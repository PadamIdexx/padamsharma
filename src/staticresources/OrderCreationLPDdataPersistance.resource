/* PersistOrderEntryClientSide.js  */
var sfdcSessionStorageNamespace = 'sfdcOrderEntryStorage',
    orderPageId = 'OrderPageId',
    uiTypeSelectList = 'selectList', uiTypeTextArea = 'textArea', uiTypeInputText = 'inputText',
    noOnChangeFire = 'noOnChangeFire', yesOnChangeFire = 'yesOnChangeFire',
    orderStorageObject = new Object(),
    arrOrderStorageObjectItems = new Array(),
    hasRunOnlyOnce = true;
var arrUIDataItems       = new Array();
var arrOnChangeListeners = new Array();

function loadArrayOfUIItems() {
    //console.log('@@@@@@ loadArrayOfUIItems...');
    //Array definition:
    //  Array Position      Array Name/Description
    //  ==================  =======================================================================================
    //  pos[0]              Id
    //  pos[1]              HTML UI Type
    //  pos[2]              Informs if a javascript onChange event needs to be wired up to trigger a write to Local Storage.
    //  pos[3]              DOM Selector ID.
    //  pos[4]              Indicator that informs the UI if loading the value of the HTML element from Local Storage 
    //                      needs to drive a refresh once the DOM is fully loaded and ready. We need to refresh the 
    //                      UI under certain scenarios, such as choosing a Shipping Partner function. The affixed "-N"
    //                      value indicates order of operation.
    //  pos[5]              UI element grouping name. Used when we re-enter the UI, and then programmatically refresh the UI, 
    //                      but then need to set a group of values AFTER the refresh takes place.
    //                                                         
    arrUIDataItems = [
        ['selectedSoldToId', uiTypeSelectList, noOnChangeFire,  'OrderPageId:formId:pbAddress:soldToPartner',                  'yes-soldTo'  , ''       ],
        ['selectedShipToId', uiTypeSelectList, noOnChangeFire,  'OrderPageId:formId:pbAddress:partnerfunction', 'yes-shipping', ''       ],
        ['shipStreet',       uiTypeTextArea,   yesOnChangeFire, 'OrderPageId:formId:pbAddress:shipStreet',      'no'          , 'shipTo' ],
        ['shipCity',         uiTypeInputText,  yesOnChangeFire, 'OrderPageId:formId:pbAddress:shipCity',        'no'          , 'shipTo' ],
        ['shipStateId',      uiTypeSelectList, yesOnChangeFire, 'OrderPageId:formId:pbAddress:shipStateId',     'no'          , 'shipTo' ],
        ['shipCountryId',    uiTypeSelectList, yesOnChangeFire, 'OrderPageId:formId:pbAddress:shipCountryId',   'no'          , 'shipTo' ],
        ['shipPostalCode',   uiTypeInputText,  yesOnChangeFire, 'OrderPageId:formId:pbAddress:shipPostalCode',  'no'          , 'shipTo' ],
        ['attentionName2',        uiTypeInputText,  yesOnChangeFire, 'OrderPageId:formId:pbAddress:attentionName2',  'no'          , 'shipTo' ]  
    ]; 
    //For the spirit of DE9220 - we are addressing only the resporation of the ship To partner selection. Other possible items to be 
    //implelemted at a later time are inventoried below:
    /*
    arrUIDataItems = [
        ['selectedOrgId',    uiTypeSelectList, noOnChangeFire,  'OrderPageId:formId:pbAddress:salesorg:selectedOrg',                    'no'     ],
        ['selectedBillToId', uiTypeSelectList, noOnChangeFire,  'OrderPageId:formId:pbAddress:address:billingAddress:partnerfunction1,  'yes-billing' ],
        ['selectedPayerId',  uiTypeSelectList, noOnChangeFire,  'OrderPageId:formId:pbAddress:address:payerAddress:partnerfunction2',   'yes-payer'],
    ];  
    */
}

function getUIBehaviorsForSelectorID(selId) {
    var arrayLength = arrUIDataItems.length;
    var uiBehavors = null;
    for (var i = 0; i < arrayLength; i++) {
        var uiType     = arrUIDataItems[i][1];
        var uiOnChange = arrUIDataItems[i][2];
        var selectorId = arrUIDataItems[i][3];
        if (selectorId === selId) {
            uiBehavors = new Object();
            uiBehavors.uiType = uiType;
            uiBehavors.uiOnChange = uiOnChange;
            break;
        }
    }
    return uiBehavors;
}

function getUIRefreshIndicatorForSelectorID(selId) {
    var arrayLength = arrUIDataItems.length;
    var uiRefreshIndicator = null;
    for (var i = 0; i < arrayLength; i++) {
        var selectorId = arrUIDataItems[i][3];
        if (selectorId === selId) {
            uiRefreshIndicator = arrUIDataItems[i][4];
            break;
        }
    }
    return uiRefreshIndicator;
}

function initializeUIDataForLocalStorage() {
    try {
        loadArrayOfUIItems();
        arrOnChangeListeners = new Array();
        var arrayLength = arrUIDataItems.length;
        for (var i = 0; i < arrayLength; i++) {
            var uiId       = arrUIDataItems[i][0];
            var uiType     = arrUIDataItems[i][1];
            var uiOnChange = arrUIDataItems[i][2];
            var selectorId = arrUIDataItems[i][3];
            //console.log('------ processing UI item ' + uiId + ' ' + uiType + ' ' + selectorId);
            var tmpObj = document.getElementById(selectorId);
            var tmpVal = '';
            switch (uiType) {
                case uiTypeSelectList:
                    tmpVal = getValForTmpObj(tmpObj);
                    break;  
                case uiTypeTextArea: 
                    tmpVal = getValForTmpObj(tmpObj);
                    break;
                case uiTypeInputText:
                    tmpVal = getValForTmpObj(tmpObj);
                    break;
            }            
            if (tmpVal) putItemInLocalStorage(selectorId, tmpVal); 
            if ((uiOnChange) && (uiOnChange === yesOnChangeFire)) {
                //only wire up event listener if we have a valid uiId AND tmpObj
                if ((uiId) && (tmpObj)) {
                	wireUpEventListener(uiId, tmpObj);
                }
            } 
            
        }
    } catch (e) {
        console.error('exception! name: ' + e.name + ' message: ' + e.message);  
    }
}

function handleSessionStorageClickEvent() { 
    var tmpVal = '';
    if (this.value) tmpVal = this.value;
    putItemInLocalStorage(this.id, tmpVal); 
    
}


function wireUpEventListener(uiId, domId) {
    //only configure if we don't yet have the item in arrOnChangeListener
	//console.log('@@@@@@ wireUpEventListener...');
    if (!doesItemAlreadyHaveAListener(uiId)) {
        var eventListener = domId.addEventListener('change', handleSessionStorageClickEvent);
        if (eventListener) {
            objChangeListener = new Object();
            objChangeListener.uiId = uiId;
            objChangeListener.eventListener = eventListener;
            arrOnChangeListeners.push(objChangeListener); 
        }
    }    
    //console.log('...arrOnChangeListeners = ' + arrOnChangeListeners);
    
}

function removeAllWiredUpEventListeners() {
    //console.log('@@@@@@ removeAllWiredUpEventListeners');
    var arrayLength = arrUIDataItems.length;
    for (var i = 0; i < arrayLength; i++) {
        var uiId       = arrUIDataItems[i][0];
        var uiType     = arrUIDataItems[i][1];
        var uiOnChange = arrUIDataItems[i][2];
        var selectorId = arrUIDataItems[i][3];
        removeWiredUpEventListener(uiId, selectorId);
    }
    
}


function removeWiredUpEventListener(uiId, selectorId) {
    //console.log('@@@@@@ removing change event listener for ' + uiId);
    var arrayLengthOfListeners = arrOnChangeListeners.length;
    for (var i = 0; i < arrayLengthOfListeners.length; i++) {
        objChangeListener = arrOnChangeListeners[i];
        if (objChangeListener.uiId === uiId) {
            //console.log('located listener for ' + uiId);
            var tmpObj = document.getElementById(selectorId);
            if (tmpObj) tmpObj.removeEventListener('change', handleSessionStorageClickEvent);
            break;
        }
    }
}

function doesItemAlreadyHaveAListener(itemId) {
    //console.log('@@@@@@ doesItemAlreadyHaveAListener...itemId = ' + itemId);
    var retVal = false; //assumes NOT yet added; unless we locate by itemId
    var arrayLengthOfListeners = arrOnChangeListeners.length;
    for (var i = 0; i < arrayLengthOfListeners.length; i++) {
        objChangeListener = arrOnChangeListeners[i];
        if (objChangeListener.uiId === itemId) {
            retVal = true;
            break;
        }
    }
    return retVal;
}

function getValForTmpObj(tmpObj) {
    var tmpVal = '';
    if ((tmpObj) && (tmpObj.value)) {
        tmpVal = tmpObj.value;
    } 
    return tmpVal;
}

function setValForTmpObj(tmpObj) {
    //console.log('in setValForTmpObj...' + tmpObj);
}

function putItemInLocalStorage(name, value) {
    var uri = window.location.host,
        keyRoot = sfdcSessionStorageNamespace + '.' + uri + ':';
    sessionStorage.setItem(keyRoot+name, value);
    
}

function getRefreshIndicatorForSoldTo() {
    var retVal = null;
    try {
        OrderCreationLPD.getRefreshIndicatorForSoldTo(function(result,event){
            if(event.status){ 
                if (event.result) {
                    retVal = result;
                }
                if (retVal === true) {
                    //do nothing
                } else {
                    setRefreshIndicatorForSoldToAndFireOnChange();
                }
            } else {
                console.log('is exception case'); 
            }    
        }); 
    } catch (e) {
        console.error('Exception! ' + e);
    }
}

function setRefreshIndicatorForSoldToAndFireOnChange() {
    try {
        OrderCreationLPD.setRefreshIndicatorForSoldTo(true, function(result,event){
            if(event.status){ 
                //console.log('>>>>>> setRefreshIndicatorForSoldTo - success from remote action.');
                if (event.result) {
                    retVal = result;
                    //need to programmatically emulate the action of manually changing "Sold To"...
                    //calls action function related to the Sold To dropdown on the vf page...
                    updateSoldToFor002(); //12.27.2016
                    //Calling updateSoldToFor002 will then call - via oncomplete - the action function updateShippingAddress. This is the "chaining" needed.
                }
            } else {
                console.log('is exception case'); 
            }    
        }); 
    } catch (e) {
        console.error('Exception! ' + e);
    }    
}


function getRefreshIndicatorForShipTo() {
    var retVal = null;
    try {
        OrderCreationLPD.getRefreshIndicatorForShipTo(function(result,event){
            if(event.status){ 
                if (event.result) {
                    retVal = result;
                }
                if (retVal === true) {
                    //do nothing
                } else {
                    setRefreshIndicatorForShipToAndFireOnChange();
                }
            } else {
                console.log('is exception case'); 
            }    
        }); 
    } catch (e) {
        console.error('Exception! ' + e);
    }
}

function setRefreshIndicatorForShipToAndFireOnChange() {
    try {
        OrderCreationLPD.setRefreshIndicatorForShipTo(true, function(result,event){
            if(event.status){ 
                //console.log('>>>>>> setRefreshIndicatorForShipTo - success from remote action.');
                if (event.result) {
                    retVal = result;
                    //calls action function related to the Ship To dropdown on the vf page...
                    updateShippingAddress(); //12.27.2016
                }
            } else {
                console.log('is exception case'); 
            }    
        }); 
    } catch (e) {
        console.error('Exception! ' + e);
    }    
}

function checkSoldToFromLocalStorage() {
    //TODO: check a boolean/flag - as this will be triggered every time we change the Ship To address...
	console.log('------> checkSoldToFromLocalStorage');
    //restoreSoldToUIItems();
    //restoreEventListenersForUIItems();     
}

function checkShipToFromLocalStorage() {
    //TODO: check a boolean/flag - as this will be triggered every time we change the Ship To address...
	console.log('------> checkShipToFromLocalStorage');
    restoreShipToUIItems();
    restoreEventListenersForUIItems(); 
    //2.25.2017 - for Order Edit situations, now that we've reconstructed the Ship To from the saved version that was
    //on the Order record, we will now need to populate the init snapshot of the partner function data:
    try {
        if (isOrderDataFromSAP) {
            console.log('isOrderDataFromSAP === ' + isOrderDataFromSAP);
            setUILockdownsBasedOnEditableState();
            populateInitialSnapshots(); //2.27.2017 -- added 
            //populateEditSnapshotsAndConfigureUIBehavior();
            console.log('just called populateInitialSnapshots which lives on mainJS!!!');
        }
    } catch (e) {
        console.error('Exception! ' + e);
    }        
}


function restoreShipToUIItems() {
    //console.log('@@@@@@ restoreShipToUIItems');
    var uri = window.location.host,
        keyRoot = sfdcSessionStorageNamespace + '.' + uri + ':', 
        i = sessionStorage.length;
    while(i--) {
        var key = sessionStorage.key(i);
        if(/sfdcOrderEntryStorage/.test(key)) {
            if (key.indexOf(keyRoot)> -1) {
                var keyName = key.replace(keyRoot, '');
                if (keyName && keyName.length > 0) {
                    var uiGroupingName = getUIGroupingForSelectorID(keyName);
                    if (uiGroupingName === 'shipTo') {
                        //found a positive match of what's in local storage in conjunction with the shipTo grouping name
                        //therefore, we will set the local (DOM) value to what was found in local storage.
                        var tmpObj = document.getElementById(keyName);
                        //console.log('processing keyName = ' + keyName);
                        tmpObj.value = sessionStorage.getItem(key);
                    }
                }
            } //keyRoot > -1
        } //end-regExp test  
    } //end-while  
}


function restoreEventListenersForUIItems() {
    //console.log('@@@@@@ restoreEventListenersForUIItems');
    var arrayLength = arrUIDataItems.length;
    for (var i = 0; i < arrayLength; i++) {
        var uiId       = arrUIDataItems[i][0];
        var uiType     = arrUIDataItems[i][1];
        var uiOnChange = arrUIDataItems[i][2];
        var selectorId = arrUIDataItems[i][3];
        var tmpObj = document.getElementById(selectorId);
        if ((uiOnChange) && (uiOnChange === yesOnChangeFire)) {
            wireUpEventListener(uiId, tmpObj);
        }  
    }
    
}

function getUIGroupingForSelectorID(selId) {
    var arrayLength = arrUIDataItems.length;
    var uiGroupingName = null;
    for (var i = 0; i < arrayLength; i++) {
        var selectorId = arrUIDataItems[i][3];
        if (selectorId === selId) {
            uiGroupingName = arrUIDataItems[i][5];
            break;
        }
    }
    return uiGroupingName;
}


function setUIItemsFromLocalStorageValues() {
    
}

function reconstituteUIItems(uiObjectItems) {
	//console.log('@@@@@@ reconstituteUIItems...uiObjectItems =' + uiObjectItems.orderStorageObjectItems);
    var uri = window.location.host,
        keyRoot = sfdcSessionStorageNamespace + '.' + uri + ':';
    try {
        var arrOrderStorageObjectItems = uiObjectItems.orderStorageObjectItems;
        if (arrOrderStorageObjectItems) {
            for (var i = 0; i < arrOrderStorageObjectItems.length; i++) {
                var orderStorageObjectItem = arrOrderStorageObjectItems[i];
				//console.log('iterating...' + orderStorageObjectItem.name + ' ' +  orderStorageObjectItem.value);
                var tmpObj = document.getElementById(orderStorageObjectItem.name);
                if (tmpObj && orderStorageObjectItem.value) {
                    putItemInLocalStorage(orderStorageObjectItem.name, orderStorageObjectItem.value);
                    ////// The statement below assigns the value from local storage to the DOM value on the UI;
                    /////  therefore, "refreshing" it visually.
                    tmpObj.value = orderStorageObjectItem.value;
                    var refreshUIIndicator = getUIRefreshIndicatorForSelectorID(orderStorageObjectItem.name);
                    //console.log('refreshUIIndicator = ' + refreshUIIndicator);
                    if (refreshUIIndicator) {
                        if (refreshUIIndicator === 'yes-soldTo') {
                        	console.log('need an intelligent way of chaining refreshes - as Sold To informs downstream Ship To...');  
                            getRefreshIndicatorForSoldTo();
                        }
                        if (refreshUIIndicator === 'yes-shipping') {
                            getRefreshIndicatorForShipTo();   
                        }
                    }  
                }
            }
        } //if arrOrderStorageObjectItems
    } catch (e) {
        console.error(e);
    }       
} //reconstituteUIItems


function removeAllWiredUpEventListeners() {
    var arrayLength = arrUIDataItems.length;
    for (var i = 0; i < arrayLength; i++) {
        var uiId       = arrUIDataItems[i][0];
        var uiType     = arrUIDataItems[i][1];
        var uiOnChange = arrUIDataItems[i][2];
        var selectorId = arrUIDataItems[i][3];
        removeWiredUpEventListener(uiId, selectorId);
    }
    
}

function findValueInJsonUIObjectByKey(key, uiObjectItems) {
    var retVal = '', keyRoot = uiObjectItems;
    try {
        var arrOrderStorageObjectItems = uiObjectItems.orderStorageObjectItems;
        if (arrOrderStorageObjectItems) {
            for (var i = 0; i < arrOrderStorageObjectItems.length; i++) {
                var orderStorageObjectItem = arrOrderStorageObjectItems[i];
                if (orderStorageObjectItem.name === key) {
                    retVal = orderStorageObjectItem.value;   
                    break;
                }
            }
        }
    } catch (e) {
        console.error(e);
    }
    return retVal;
}

function deleteAllInLocalStorage() {
    //debugger;
    var i = sessionStorage.length;
    while(i--) {
        var key = sessionStorage.key(i);
        if(/sfdcOrderEntryStorage/.test(key)) {
            sessionStorage.removeItem(key);
        }  
    } 
}

function obtainJsonFromLocalStorage() {
    var jsonObj;
    if (hasRunOnlyOnce) {
        jsonObj = convertLocalStorageToJSON();
        hasRunOnlyOnce = false;
    }
    if (jsonObj) {
        //console.log('jsonObj = ' + jsonObj);    
    }
    return jsonObj;  
}

function getAccountIdFromURI() {
    var accountId = '';
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if (pair[0].toLowerCase === 'id') {
            accountId = pair[1];
            break;
        }
    }
    return accountId;
}

function getOrderIdFromURI() {
    var orderId = '';
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if (pair[0].toLowerCase()  === 'oid') {
            orderId = pair[1];
            break;
        }
    }
    return orderId;
}

function convertLocalStorageToJSON() {
    //console.log('@@@@@@ convertLocalStorageToObject...');    
    //1. iterate through the list of items identified via sfdcSessionStorageNamespace (sfdcOrderEntryStorage).
    //   save each item - push to an array.
    //2. convert that array to a json representation for later use in Salesforce - perhaps a new custom field on the Order sObject
    var uri = window.location.host,
        keyRoot = sfdcSessionStorageNamespace + '.' + uri + ':', 
        i = sessionStorage.length,
        hasCreatedMasterStorageObject = false, 
        orderStorageObject = new Object(),
        arrOrderStorageObjectItems = new Array();
    while(i--) {
        var key = sessionStorage.key(i);
        if(/sfdcOrderEntryStorage/.test(key)) {
            //console.log('key ' + key + '; value = ' + sessionStorage.getItem(key));
            if (key.indexOf(keyRoot)> -1) {
                var keyName = key.replace(keyRoot, '');
                if (keyName && keyName.length > 0) {
                    //console.log('will be persisting ' + keyName + ', ' +  sessionStorage.getItem(key));
                    if (!hasCreatedMasterStorageObject) {
                        orderStorageObject = createOrderStorageObject(keyRoot);
                        hasCreatedMasterStorageObject = true;
                    }
                    var orderStorageObjectItem = createOrderStorageObjectItem(keyName, sessionStorage.getItem(key));
                    arrOrderStorageObjectItems.push(orderStorageObjectItem);
                }
            } //keyRoot > -1
        } //end-regExp test  
    } //end-while
    if (hasCreatedMasterStorageObject) {
        orderStorageObject.orderStorageObjectItems = arrOrderStorageObjectItems;
    }
    var orderStorageObjectToJson = JSON.stringify(orderStorageObject);
    if (orderStorageObjectToJson) {
        deleteAllInLocalStorage();
        removeAllWiredUpEventListeners();       
    }
    return orderStorageObjectToJson;
    
}

function createOrderStorageObjectItem(name, value) {
    var orderStorageObjectItem = new Object();
    orderStorageObjectItem.name = name;
    orderStorageObjectItem.value = value;
    return orderStorageObjectItem;
}

function createOrderStorageObject(keyRoot) {
    accountId = getAccountIdFromURI();
    orderStorageObject = new Object();
    orderStorageObject.keyRoot = keyRoot;
    orderStorageObject.refreshShipTo = false;
    //NOTE: there may be other refresh values here...
    //      'Refresh' vaules that indicate if upon marshalling back from JSON to Local Storage
    //      the UI needs to programmatically execute a refresh (e.g. mimics making a selection).
    //orderStorageObject.refreshSoldTo = false;
    //orderStorageObject.refreshBilling = false;
    //orderStorageObject.refreshPayer = false; 
    orderStorageObject.orderStorageObjectItems = new Array();
    return orderStorageObject;
    
}

