/* Trigger Name  : SalesviewTrigger
 * Description   : Apex Trigger to update Doctor Discount based on Price Group of Salesview
 * Created By    : Aditya Sangawar
 * Created On    : 17-Nov-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Aditya Sangawar              17-Nov-2015               Initial version.
 *  * Nagendra                                             added lines 19, 25 and 31
 *****************************************************************************************/

trigger UpdateDoctorDiscountOnAccount on Account_Salesview__c (before delete,after delete, after insert,after update) {
    
    if(trigger.isInsert){
        AccountSalesviewTriggerHandler.UpdateDoctorDiscountonPriceGroupInsert(trigger.new,null,null,trigger.oldMap);
       // AccountSalesviewTriggerHandler.UpdateSalesViewDistrictToAccount(trigger.new,null,null,null);   
        AccountSalesviewTriggerHandler.populateTheFreightTermsFieldOnTheAccount( trigger.new );
      // AccountSalesviewTriggerHandler.populateIsWaterFieldOnTheAccount( trigger.new );
    // AccountSalesviewTriggerHandler.unpopulateIsWaterFieldOnTheAccount( trigger.new );
    }
    
    else if(trigger.isUpdate){    
       AccountSalesviewTriggerHandler.UpdateDoctorDiscountonPriceGroupInsert(null,trigger.new,null,trigger.oldMap);
      // AccountSalesviewTriggerHandler.UpdateSalesViewDistrictToAccount(null,trigger.new,null,trigger.oldMap);
       AccountSalesviewTriggerHandler.populateTheFreightTermsFieldOnTheAccount( trigger.new );        
    //   AccountSalesviewTriggerHandler.populateIsWaterFieldOnTheAccount( trigger.new );
     // AccountSalesviewTriggerHandler.unpopulateIsWaterFieldOnTheAccount( trigger.new );
    }
    
       else if(trigger.isDelete){
        AccountSalesviewTriggerHandler.UpdateDoctorDiscountonPriceGroupInsert(null,null,trigger.old,trigger.oldMap); 
       // AccountSalesviewTriggerHandler.UpdateSalesViewDistrictToAccount(null,null,trigger.old,trigger.oldMap); 
        AccountSalesviewTriggerHandler.populateTheFreightTermsFieldOnTheAccount( trigger.old );
        /*if(trigger.isafter)
        {
            AccountSalesviewTriggerHandler.deleteIsWaterFieldOnTheAccount(trigger.old);  
         } */   
    }     
       
}