trigger WFM_ServiceResourceSharing on ServiceResource (before insert,before update) {
    List<ServiceResourceShare> resourceShares = new List<ServiceResourceShare>();
    Database.SaveResult[] insertResults;
    String accessLevel='Edit';
    
    for(ServiceResource res : trigger.new){
        
        System.debug('Attempting to Share Service Resource:'+res.id+', User:'+res.RelatedRecordId+', Access Level:'+accessLevel);
        ServiceResourceShare resourceShare = new  ServiceResourceShare();
        resourceShare.ParentId = res.Id;
        resourceShare.UserOrGroupId = res.RelatedRecordId;
        resourceShare.AccessLevel = accessLevel;
        resourceShares.add(resourceShare);
    }
    
    if(! resourceShares.isEmpty()){  
        insertResults = Database.insert(resourceShares,false);
    }
    
    if (insertResults==NULL){
        return;
    }
    if(insertResults[0].isSuccess()){
        System.debug('Records for ServiceResourceShare inserted successfully');
        return;
        }
    Database.Error err = insertResults[0].getErrors()[0];
    if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  && err.getMessage().contains('AccessLevel')){ 
        System.debug(' Access level of inserted service resource share record was same as natural access level ' );
        }
    else {
        System.debug(Logginglevel.ERROR, ' Unable to insert share record for service resource ' + err.getMessage());   
    }

}