trigger WFM_ServiceAppointmentSharing on ServiceAppointment (after insert,after update) {
    List<ServiceAppointmentShare> saShares = new List<ServiceAppointmentShare>();
    Id DRICGroup;
    
    // Retrieve the LOB from WorkOrder
    Set<Id> woIds=new Set<Id>();
    for(ServiceAppointment sa : trigger.new){
        if (sa.ParentRecordId!=null){
            woIds.add(sa.ParentRecordId);
        }
    }
    Map<ID,WorkOrder> woLookup = new map<ID,WorkOrder> ([select id, Product_Type__c
                                          from WorkOrder
                                          where id IN: woIds]);
                                              
    // Find Public Group Digital Implementation Coordinator
    
    for(ServiceAppointment sa : trigger.new){
    if(woLookup.get(sa.ParentRecordId).Product_Type__c=='DR'){
    List<Group> GroupList=[select id from group where Name='DR IC'];
    if (!GroupList.isEmpty()){
      DRICGroup=GroupList[0].id;
    }
    else {
        System.debug('Could not find Public Group DR IC, exiting');
        return;
    }}
    }
    
    Database.SaveResult[] insertResults;
    String accessLevel='Edit';
    
    // Process Sharing Rules for Service Appointment
    for(ServiceAppointment sa : trigger.new){
        
        if (woLookup.get(sa.ParentRecordId).Product_Type__c=='DR'){
          System.debug('Attempting to Share Service Appointment:'+sa.id+', Public Group:'+DRICGroup+', Access Level:'+accessLevel);
          ServiceAppointmentShare saShare = new  ServiceAppointmentShare();
          saShare.ParentId = sa.Id;
          saShare.UserOrGroupId = DRICGroup;
          saShare.AccessLevel = accessLevel;
          saShares.add(saShare);
        }
    }
    
    if(! saShares.isEmpty()){  
        insertResults = Database.insert(saShares,false);
    }
    
    if (insertResults==NULL){
        return;
    }
    if(insertResults[0].isSuccess()){
        System.debug('Records for ServiceAppointmentShare inserted successfully');
        return;
    }
    Database.Error err = insertResults[0].getErrors()[0];
    if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  && err.getMessage().contains('AccessLevel')){ 
        System.debug(' Access level of inserted service appointment share record was same as natural access level ' );
    }
    else {
        System.debug(Logginglevel.ERROR, ' Unable to insert share record for service appointment ' + err.getMessage());   
    }
}