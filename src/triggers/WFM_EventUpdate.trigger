trigger WFM_EventUpdate on Event (before insert,before update) {
    List<ServiceAppointment> sa = new List<ServiceAppointment>();
    List<Event> evList = new List<Event>();
    
    // Retrieve the Subject from WorkOrder
    Set<Id> evIds=new Set<Id>();
    for(Event ev : trigger.new){
        if (ev.WhatId!=null){
            evIds.add(ev.WhatId);
        }
    }
    Map<ID,ServiceAppointment> saLookup = new map<ID,ServiceAppointment> ([select id,ParentRecordId,Account.Name,Account.Id,Subject,Work_Category__c,Work_Order__c,Street,City,StateCode,PostalCode
                                          from ServiceAppointment
                                          where id IN: evIds]);
    
    List<WorkOrder> woList=new List<WorkOrder>();
    for (Id saId : saLookup.keySet()){
        WorkOrder wo=new WorkOrder(Id=saLookup.get(saId).ParentRecordId);
        woList.add(wo);
        }
                                          
    Map<ID,WorkOrder> woLookup = new Map<ID,WorkOrder> ([select Id,SAP_Account_Number__c from WorkOrder where Id in :woList]);
    
    for (Event ev: trigger.new){
        
        if (trigger.isInsert && saLookup.get(ev.WhatId)!=NULL){
            /**** Set Description ****/
            ev.Description='[WFM] \n';
            if (saLookup.get(ev.WhatId).Work_Category__c!=NULL) ev.Description+='Work Order Type: '+saLookup.get(ev.WhatId).Work_Category__c+'\n';
            if (saLookup.get(ev.WhatId).Subject!=NULL) ev.Description+='Subject: '+saLookup.get(ev.WhatId).Subject+'\n';
            if (saLookup.get(ev.WhatId).Account.Name!=NULL) ev.Description+='Account Name: '+saLookup.get(ev.WhatId).Account.Name+'\n';
            if (woLookup.get(saLookup.get(ev.WhatId).ParentRecordId)!=NULL && woLookup.get(saLookup.get(ev.WhatId).ParentRecordId).SAP_Account_Number__c!=NULL) ev.Description+='Account SAP #: '+woLookup.get(saLookup.get(ev.WhatId).ParentRecordId).SAP_Account_Number__c+'\n';
            if (saLookup.get(ev.WhatId).Street!=NULL) ev.Description+='Address: '+saLookup.get(ev.WhatId).Street+'\n'; 
            if (saLookup.get(ev.WhatId).City!=NULL) ev.Description+=saLookup.get(ev.WhatId).City;
            if (saLookup.get(ev.WhatId).StateCode!=NULL) ev.Description+=','+saLookup.get(ev.WhatId).StateCode;
            if (saLookup.get(ev.WhatId).PostalCode!=NULL) ev.Description+=+' '+saLookup.get(ev.WhatId).PostalCode+'\n';
                  
            /**** Set Interaction Origin ****/
            ev.Interaction_Origin__c='Scheduled Visit';
            
            /**** Set Work Order Id ****/
            if (saLookup.get(ev.WhatId).Work_Order__c!=NULL) ev.Work_Order__c=saLookup.get(ev.WhatId).Work_Order__c;
            
            /**** Set Subject ****/
            if (trigger.isInsert && saLookup.get(ev.WhatId).Subject!=NULL) ev.ServiceAppointmentSubject__c=saLookup.get(ev.WhatId).Subject;
            ev.Subject=''; // Subject needs to be blank for FSL to sync properly. A workflow rule updates the subject from the custom field ServiceAppointmentSubject__c after the FSL triggers execute
                        
            }
        }
}