/* Trigger Name  : AccountCurrencyUpdateTrigger
* Description : Trigger to update Account currency based on Billing Country
* Created By  : Anudeep Gopagoni
* Created On : 21-Oct-2015
*
*  Modification Log :
*  --------------------------------------------------------------------------------------
*  * Developer                    Date                    Description
*  * ------------------------------------------------------------------------------------                 
*  * Anudeep Gopagoni              21-Oct-2015               Initial version.
*  * Hemanth Vanapalli             25-Nov-2015               US-3179
*  * Lakshmi Quinn                 July-12-2016              Defect #DE8518
*  * Sonia Cook-Broen              July-15-2016              Opt out for testing/temp fix 
*                                                             for errors in test classes due 
*                                                             to AccountTriggerHandler  
*****************************************************************************************/
trigger AccountTrigger on Account (before insert,after insert, before update, after update) {
    
    
    
    AccountTriggerHandler accountHandler = new AccountTriggerHandler(); 
    if(Trigger.isBefore) { 
        if(Trigger.isInsert) { 
            //Calling Handler class when insert or update operation is performed
            AccountTriggerHandler.currencyUpdateonAccountInsert(Trigger.New);
            accountHandler.onBeforeInsert(Trigger.new);
        } 
        if(Trigger.IsUpdate){
            //Calling Handler class when insert or update operation is performed
            AccountTriggerHandler.currencyUpdateonAccountUpdate(Trigger.New,Trigger.OldMap);   
            accountHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    
    if(Trigger.isAfter) {
        if(Trigger.isUpdate){
            accountHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }
        if(Trigger.isInsert) {    
            // commenting it out due to defect DE8518. Original story was to assign all prospect accouts to P&S Queue not just ISC.
            
            // Profile p = [SELECT Id FROM Profile WHERE Name = 'Inside Sales Rep-CAG'];
            // System.debug('@@After Insert of Account' + p);
            // if(p.Id != UserInfo.getProfileId())
            //  return;
            //if(!test.isRunningTest()){
                accountHandler.onAfterInsert(Trigger.new , Trigger.newmap);
            //}
        }
        
       // Disable this trigger during data loading.
        DisableSettings__c disableSettings = DisableSettings__c.getInstance();
        if (disableSettings != null && disableSettings.Disable_Triggers__c == true) {
            return;
        }

        // bulkify trigger in case of multiple accounts
        for (Account account : trigger.new) {

            //check if Billing Address has been updated
            Boolean addressChangedFlag = false;
            if (Trigger.isUpdate) {
                Account oldAccount = Trigger.oldMap.get(account.Id);
                if ((account.ShippingStreet != oldAccount.ShippingStreet)
                    || (account.ShippingCity != oldAccount.ShippingCity)
                    || (account.ShippingCountry != oldAccount.ShippingCountry)
                    || (account.ShippingPostalCode != oldAccount.ShippingPostalCode)) {

                    addressChangedFlag = true;

                    System.debug(LoggingLevel.DEBUG, '***Address changed for - ' +
                                 oldAccount.Name);
                }
            }
            // if address is null or has been changed, geocode it
            if ((account.Location__Latitude__s == null) || (addressChangedFlag == true)) {
                System.debug(LoggingLevel.DEBUG,
                             '***Geocoding Account - ' + account.Name);
                AccountTriggerHandler.DoAddressGeocode(account.id);
            }
        }
    }
}