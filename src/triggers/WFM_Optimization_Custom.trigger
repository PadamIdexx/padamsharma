trigger WFM_Optimization_Custom on FSL__Optimization_Request__c (after update) {
  
    List<Id> serviceResource= new List<Id>();
  
    for (FSL__Optimization_Request__c optReq : trigger.new){
        if (optReq.FSL__Status__c!='Completed') return;
        serviceResource.add(optReq.FSL__Service_Resource__c); 
    }
    
    List<FSL__Optimization_Request__c> optReqList=[
         select Id from FSL__Optimization_Request__c 
         where SystemModstamp > :Datetime.now().addMinutes(-3)
            and FSL__Type__c='Fill-In Schedule'   
            and FSL__Service_Resource__c in :serviceResource];
    if (optReqList.size()>4 || optReqList.size()==0) return;
    
    FSL__Scheduling_Policy__c sP=[select Id from FSL__Scheduling_Policy__c where Name='Diagnostic' limit 1];
    
    for (FSL__Optimization_Request__c optReq : trigger.new){
        if (optReq.FSL__Type__c=='Fill-In Schedule'){
            System.debug('Custom:FillInScheduleService('+optReq.FSL__Start__c.addDays(1)+','+optReq.FSL__Service_Resource__c+','+sp.Id+')');
            if (!Test.isRunningTest()) FSL.FillInScheduleService.CallFillInBatch(optReq.FSL__Start__c.addDays(1), optReq.FSL__Service_Resource__c, sp.Id);
    
        }
    }
    
}