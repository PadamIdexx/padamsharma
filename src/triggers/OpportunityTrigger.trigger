/**
*       @author Hemanth Vanapalli
*       @date   23/09/2015   
        @description    Trigger to synchronise the parent and child opportunity amount.
        Function: Trigger which will synchronise the amount on parent and child opportunnity whenever the child amount is updated.
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                     Date                Description
        ------------------------------------------------------------------------------------
        Hemanth Vanapalli             07/10/2015          Modified Version
        Heather Kinney                1.16.2017           US24836 Implement LPD Opportunity Product Schedules.
                                                          - Added Trigger.isAfter block - calls OpportunityProductScheduleHandler.onAfterOpportunityUpdate
                                                            The logic embedded in OpportunityProductScheduleHandler takes care of inspecting the Opportunity 
                                                            record type. As of 1.16.2016, only those Opportunities that have an LPD Annuitized Opportunity 
                                                            record type will have automatic generation (or modification of existing) order item product schedules.
        Heather Kinney                1.23.2017           COMMENTED the reference to cagAnnuitizedRecType as it was no longer in use - exposed once we migrated to QA2;
                                                          - The RT_CAG_Annuitized record type no longer exists, but the Custom Label did.
**/

trigger OpportunityTrigger on Opportunity (after insert, before update, after update) {
    //H.Kinney - 1.17.2017 - US24836 Implement LPD Opportunity Product Schedules related: for the above, added "after update"...

    OpportunityTriggerHandler otHandler = new OpportunityTriggerHandler();
    
    //H.Kinney - 1.17.2017 - US24836 Implement LPD Opportunity Product Schedules related:
    //OpportunityProductScheduleHandler oppProductScheduleHandler = new OpportunityProductScheduleHandler(); 
    
    
    /*if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            otHandler.onAfterInsert(Trigger.new,Trigger.newMap);
        }
    }*/
    
    
    if(Trigger.isBefore) {
        if(CheckRecursiveTrigger.runOnce()){
            if(Trigger.isUpdate) {
                Map<Id,Opportunity> oldOppMap = new Map<Id,Opportunity>();
                Map<Id,Opportunity> newOppMap = new Map<Id,Opportunity>();
                String tenderRecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_Annuitized).getRecordTypeId();
                String lpdRecType =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_LPD_Opportunity).getRecordTypeId();
                String negativeRecType= Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_NetativeWinBack).getRecordTypeId();
                //H.Kinney - 1.23.2017 - COMMENTED the reference below to cagAnnuitizedRecType for the following reasons:
                // 1. This record type isn't even in QA2 as of 1.23.2017;
                // 2. This label - Label.RT_CAG_Annuitized - also isn't in QA2 as of 1.23.2017;
                // 3. The record type (API name) RT_CAG_Annuitized isn't in Dev3 as of 1.23.2017;
                // 4. The String cagAnnuitizedRecType isn't used elsewhere in this Trigger. 
                //String cagAnnuitizedRecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_CAG_Annuitized).getRecordTypeId();
                otHandler.onAfterUpdate(Trigger.new, Trigger.oldMap); 
                for(Opportunity opp : Trigger.newMap.values()) {
                    if( (opp.RecordTypeId == lpdRecType || opp.RecordTypeId == negativeRecType) && Trigger.oldMap.get(opp.Id).Amount != Trigger.newMap.get(opp.Id).Amount ) {
                        oldOppMap.put(opp.id,Trigger.oldMap.get(opp.Id));
                        newOppMap.put(opp.id,Trigger.newMap.get(opp.Id));
                    }
                    
                    
                } 
                if( !oldOppMap.isEmpty() && !newOppMap.isEmpty()) {
                    System.debug('actual trigger code invoked');
                    otHandler.onBeforeUpdate(oldOppMap,newOppMap);  
                }
                
                
            }
        }
    }
    
    //H.Kinney - 1.17.2017 - START BLOCK - US24836 Implement LPD Opportunity Product Schedules related:
    /*if (Trigger.isAfter) {
        System.debug('actualtrigger After');
        if (CheckRecursiveTrigger.runLPDOpportunityUpdateOnce()) {
            if(Trigger.isUpdate) {
                //oppProductScheduleHandler.onAfterOpportunityUpdate(Trigger.new, Trigger.oldMap);  
            }
        }
    }*/
    //H.Kinney - 1.17.2017 - END BLOCK - US24836 Implement LPD Opportunity Product Schedules related:
    
}