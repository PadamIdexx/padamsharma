/**
*       @author Vishal Negandhi
*       @date   15/10/2015   
        @description    Trigger which will fire when a contact is added to an account
        Function: The trigger will update the primary contact field on the account if a new contact is added to an account and will replace the existing primary contact with the newly added contact.
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Vishal Negandhi             15/10/2015          Original Version
        Heather Kinney              01-22-2016          US-3371. Added additional handler ContactCTITriggerHandler,
                                                        modified isAfter/isInsert context to also include 
                                                        call to the CTI trigger handler; added new contexts
                                                        after update and before delete, and calls to the CTI trigger
                                                        handler.
		Hemanth Kumar				03-09-2016			TKT-000595. In order to handle inactive and deleted contact
														scenarios according to the TKT.                                                        
**/

trigger ContactTrigger on Contact (after insert, after update, before delete) {
    ContactTriggerHandler handler = new ContactTriggerHandler();
    ContactCTITriggerHandler ctiHandler = new ContactCTITriggerHandler();
    
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            handler.onAfterInsert(trigger.new);
            ctiHandler.onAfterInsert(trigger.new);         
        }
        if(Trigger.isUpdate){
        	handler.onAfterUpdate(Trigger.oldMap,Trigger.newMap);
            ctiHandler.onAfterUpdate(trigger.new, Trigger.oldMap);         
        }
    }
    if(Trigger.isBefore){
    	if(Trigger.isDelete){
    		ctiHandler.onBeforeDelete(Trigger.oldMap);	
    	}
    	
    }
}