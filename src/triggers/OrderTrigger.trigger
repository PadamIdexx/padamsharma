/* Trigger Name : OrderTrigger
 * Description : Trigger opeartions on insertion,updation and deletion of Order
 * Created By : Raushan Anand
 * Created On : 07-Sep-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand              07-Sep-2015               Initial version.
 **************************************************************************************/
trigger OrderTrigger on Order (before Delete) {//before insert,after insert,before update, after update,after delete, 
    if(Trigger.IsBefore){
        /*if(Trigger.IsInsert){
            OrderTriggerUtility.buildOppCampOrderRelation(Trigger.New);
        }      
        if(Trigger.IsUpdate){
            OrderTriggerUtility.buildOppCampOrderRelation(Trigger.New);
        }*/
		if(Trigger.IsDelete){
            //OrderTriggerUtility.counterCheckAfterDelete(Trigger.Old);
            if(CheckRecursiveTrigger.runOrderDeleteOnce())
            	OrderTriggerUtility.updateCampaignOnOrderDelete(Trigger.Old);
        }
    }/*
    if(Trigger.isAfter){
        /*if(Trigger.isInsert){
            OrderTriggerUtility.updateCampaignOnOrder(null, Trigger.New);
        }
        if(Trigger.isUpdate){
           OrderTriggerUtility.updateCampaignOnOrder(null, Trigger.New);
        }
        if(Trigger.IsDelete){
            //OrderTriggerUtility.counterCheckAfterDelete(Trigger.Old);
            OrderTriggerUtility.updateCampaignOnOrderDelete(Trigger.Old);
        }
    }*/
}