trigger WFM_WorkOrderTrigger on WorkOrder (before update, after update, before insert, after insert) {
    
    WFM_WorkOrderTriggerHandler workOrderTriggerHandler = new WFM_WorkOrderTriggerHandler();
    if(Trigger.isAfter && Trigger.isUpdate)
        workOrderTriggerHandler.afterUpdate(trigger.new, trigger.oldMap);

    if(Trigger.isBefore && Trigger.isUpdate)
        workOrderTriggerHandler.beforeUpdate(trigger.new, trigger.oldMap);

    if(Trigger.isBefore && Trigger.isInsert)
        workOrderTriggerHandler.beforeInsert(trigger.new);

    if(Trigger.isAfter && Trigger.isInsert) {
        workOrderTriggerHandler.afterInsert(trigger.new);
    }
}