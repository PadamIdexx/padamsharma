trigger CPQQuoteAutoSyncTrigger on Quote (after insert) {
    Map<Id, Id> quoteMap = new Map<Id, Id>();
    
    for(Quote currentQuote : Trigger.New)
    {
        //if(currentQuote.Status == 'Approved') {
            quoteMap.put(currentQuote.Id, currentQuote.OpportunityId);
        //}
    }
    //if(quoteMap.size() > 0) {
    	CPQQuoteAutoSyncUtil.syncQuote(quoteMap);    
    //}
}