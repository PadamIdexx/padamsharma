trigger geocodeAccountAddress on Account (after insert, after update) {

    // Disable this trigger during data loading.
    DisableSettings__c disableSettings = DisableSettings__c.getInstance();
    if (disableSettings != null && disableSettings.Disable_Triggers__c == true) {
        return;
    }

    // bulkify trigger in case of multiple accounts
    for (Account account : trigger.new) {

        //check if Billing Address has been updated
        Boolean addressChangedFlag = false;
        if (Trigger.isUpdate) {
            Account oldAccount = Trigger.oldMap.get(account.Id);
            if ((account.ShippingStreet != oldAccount.ShippingStreet)
                || (account.ShippingCity != oldAccount.ShippingCity)
                || (account.ShippingCountry != oldAccount.ShippingCountry)
                || (account.ShippingPostalCode != oldAccount.ShippingPostalCode)) {

                addressChangedFlag = true;

                System.debug(LoggingLevel.DEBUG, '***Address changed for - ' +
                             oldAccount.Name);
            }
        }
        // if address is null or has been changed, geocode it
        if ((account.Location__Latitude__s == null) || (addressChangedFlag == true)) {
            System.debug(LoggingLevel.DEBUG,
                         '***Geocoding Account - ' + account.Name);
            AccountGeocodeAddress.DoAddressGeocode(account.id);
        }
    }
}