// Trigger to add Product Name from QLI to Description on Quote

trigger QliDescTrigger on QuoteLineItem ( after delete, After Update, after insert) {

  // fires after both insert and update && Trigger.isAfter || Trigger.IsBefore
  if((Trigger.isInsert || Trigger.isUpdate) && Trigger.isAfter){

    // find the ids of all Quotes that were affected
    Set<Id> QuoteIds = new Set<Id>();
    for (QuoteLineItem qli : [select Id,QuoteID,Product2.Name from QuoteLineITem 
      where Id IN :Trigger.newMap.keySet()])
      QuoteIds.add(qli.QuoteID);

    // process the Quotes
    QliDescTriggerHandler.ProcessProductNames(QuoteIds);


  // fires when records are deleted  else if(Trigger.isDelete && Trigger.isAfter)
  } else if(Trigger.isDelete && Trigger.isafter){
  // find the ids of all Quotes that were affected
   Set<Id> QuoteIds = new Set<Id>();
    for (ID id : Trigger.oldMap.keySet())
      QuoteIds.add(Trigger.oldMap.get(id).QuoteID);

    // process the Quotes
    QliDescTriggerHandler.ProcessProductNames(QuoteIds);

  }

}