trigger WFM_AssignedResource_Trigger on AssignedResource (after insert,after update,after delete) {
    Flow.Interview.WFM_Update_SA_Last_Assigned_Resource WFM_flow;
    Map<String,Object> params = new Map<String,Object>();
    List<ServiceAppointment> saList = new List<ServiceAppointment>();

    for (AssignedResource ar: (trigger.isDelete?trigger.old:trigger.new)) saList.add(new ServiceAppointment(Id=ar.ServiceAppointmentId));
    for (ServiceAppointment sa : saList){
        params.put('ServiceAppointmentId_Variable',sa.Id);    
        WFM_flow = new Flow.Interview.WFM_Update_SA_Last_Assigned_Resource(params);
        WFM_flow.start();
        params.clear();
    }
}