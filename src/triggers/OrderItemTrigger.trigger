/* Trigger Name : OrderItemTrigger
 * Description : Trigger opeartions on insertion,updation and deletion of OrderItem
 * Created By : Raushan Anand
 * Created On : 07-Sep-2015
 * 
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand              07-Sep-2015               Initial version.
 **************************************************************************************/
trigger OrderItemTrigger on OrderItem (after insert,after update,after Delete) {
    if(Trigger.IsAfter){
        if(Trigger.IsInsert){
            if(CheckRecursiveTrigger.runOrderItemInsertOnce())
                OrderTriggerUtility.updateCampaignOnOrderItem(Trigger.New);
           //OrderTriggerUtility.updateCampaignOnOrder(Trigger.New, null);
        }
        if(Trigger.IsUpdate){
            if(CheckRecursiveTrigger.runOrderItemUpdateOnce())
                OrderTriggerUtility.updateCampaignOnOrderItem(Trigger.New);
            //OrderTriggerUtility.updateCampaignOnOrder(Trigger.New, null);
        }
        if(Trigger.IsDelete){
            if(CheckRecursiveTrigger.runOrderItemDeleteOnce())
                OrderTriggerUtility.updateCampaignOnOrderItem(Trigger.old);
           //OrderTriggerUtility.updateCampaignOnOrder(Trigger.Old, null);
        }
    }
}