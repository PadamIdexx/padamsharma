/* @Class Name   : QuoteLineItemTrigger
 * @Description  : Trigger to update the standard discount field on Quote Line Items, so that the discount on Quote also gets updated. We are using a custom field discount on
 *                 Quote Line Items, and hence this does not update the discount on Quote.
 * @Created By   : Pooja Wade
 * @Created On   : 09/16/2015  
 * @Modification Log:  
 * --------------------------------------------------------------------------------------------------
 * @Developer                Date                   Description
 * --------------------------------------------------------------------------------------------------
 * @Pooja Wade              09/16/2015              Created
 * ---------------------------------------------------------------------------------------------------
*/
trigger QuoteLineItemTrigger on QuoteLineItem (before insert, before update) {

    //referencing the handler
    QLITriggerHandler handler = new QLITriggerHandler();
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            handler.onBeforeInsert(trigger.new);
        }
        if(Trigger.isUpdate){
            handler.onBeforeUpdate(trigger.new);
        }
     }
}