trigger CaseTrigger on Case (after insert,after update) {
  WFM_CaseTriggerHandler caseTriggerHandler = new WFM_CaseTriggerHandler();
    if(Trigger.isAfter && ( Trigger.isInsert || Trigger.isUpdate) ) {
        caseTriggerHandler.onAfter(trigger.new); 
    }
}