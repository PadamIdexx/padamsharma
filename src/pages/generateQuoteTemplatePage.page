<apex:page standardController="quote" extensions="generateQuoteTemplateExtension" action="{!setupPage}" standardStylesheets="false" applyBodyTag="false" applyHtmlTag="false" showHeader="false" sidebar="false" docType="html-5.0">
    <html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">   
        <head>
            <apex:includeScript value="{!URLFOR($Resource.JQuery_Lightness_1_12, 'jquery-ui-1.12.1.custom/external/jquery/jquery.js')}"/>
            <apex:includeScript value="/soap/ajax/38.0/connection.js" />
            <apex:includeScript value="/support/console/38.0/integration.js"/>
            <apex:includeScript value="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.min.js" />
            <apex:includeScript value="{!URLFOR($Resource.aljs, '/jquery/jquery.aljs-all.min.js')}" />
            <apex:includeScript value="{!URLFOR($Resource.aljs, '/jquery/jquery.aljs-init.min.js')}" />
            <apex:includeScript value="{!URLFOR($Resource.generateQuoteTemplateScript)}"/>
            <apex:stylesheet value="{!URLFOR($Resource.slds, 'assets/styles/salesforce-lightning-design-system-vf.css')}" />
            
        </head>
        <body>
            <div class="slds slds-grid">
                <div class="slds-grid slds-grid--frame" aria-hidden="false" style="background-color:#ccc">
                    <div class="slds-container--fluid" >
                        <div class="slds-page-header" role="banner">
                            <div class="slds-media slds-media--left">
                                <div class="slds-media__body">
                                    <p class="slds-page-header__title slds-truncate slds-align-left" title="Test Codes">Quote Template Viewer</p>
                                </div>
                            </div>
                        </div>
                        <apex:form >
                            <c:sldsModal ></c:sldsModal>
                            <apex:actionFunction name="preview" action="{!renderPDFwithUserSelection}" reRender="none" oncomplete="disableTabClose();redraw();">
                                <apex:param assignTo="{!temp}" value="" name="template" id="templateId"/>
                                <apex:param assignTo="{!lang}" value="" name="language" id="langId"/>
                            </apex:actionFunction>
                            <apex:actionFunction name="saveQuote" action="{!save}"  reRender="none" oncomplete="hideWait();showMessage();"/>
                            <apex:actionFunction name="sendQuote" action="{!send}"  reRender="param" oncomplete="hideWait();setId();">
                            </apex:actionFunction>
                            <apex:actionFunction name="redraw" action="{!createView}" reRender="pdf" oncomplete="hideWait();" />
                            <apex:actionFunction name="reset" action="{!resetUserLanguage}" reRender="none"/>
                            <apex:outputPanel id="param">
                                <script>
                                function setId(){
                                    var qId = '{!quoteId}';
                                    var doc = '{!docId}';
                                    var tabId = sforce.console.getEnclosingTabId();
                                    console.log('tabId ' + tabId);
                                    var url = '/_ui/core/email/author/EmailAuthor?p3_lkid=' + qId + '&doc_id=' + doc+ '&retURL=%2F' + qId;
                                    
                                    var openSubtab = function openSubtab(result) { 
                                        var primaryTabId = result.id; 
                                        sforce.console.openSubtab(primaryTabId , url, true, 
                                                                  'Send Quote', null, openSuccess, 'sendQuoteSubTab' + qId); 
                                    }; 
                                    var openSuccess = function openSuccess(result) {}; 
                                    if(sforce.console.isInConsole()){ 
                                        sforce.console.getEnclosingPrimaryTabId(openSubtab); 
                                    } 
                                    else { 
                                        window.open(url,'_blank'); 
                                    } 
                                }
                                </script>
                            </apex:outputPanel>
                            <div class="slds-col">
                                <div class="slds-box slds-theme--shade">
                                    <div class="slds-button-group  slds-align--absolute-center" role="group">
                                        <div class="slds-picklist" data-aljs="picklist1">
                                            <button class="slds-button slds-button--neutral slds-picklist__label" id="templateSelect" aria-haspopup="true">
                                                <span class="slds-truncate">Template</span>
                                                <svg aria-hidden="true" class="slds-icon">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Resource.slds)}/assets/icons/utility-sprite/svg/symbols.svg#down"></use>
                                                </svg>
                                            </button>
                                            <div class="slds-dropdown slds-dropdown--left slds-dropdown--large slds-hide">
                                                <ul class="slds-dropdown__list slds-dropdown--length-5" role="menu">
                                                    <apex:repeat value="{!Templates}" var="tem">
                                                        <li id="{!tem.value}" class="slds-dropdown__item" role="presentation">
                                                            <a href="javascript:void(0)" role="menuitemradio">
                                                                <p class="slds-truncate">
                                                                    <svg aria-hidden="true" class="slds-icon slds-icon--selected slds-icon--x-small slds-icon-text-default slds-m-right--x-small">
                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Resource.slds)}/assets/icons/utility-sprite/svg/symbols.svg#check"></use>
                                                                    </svg>
                                                                    {!tem.label}
                                                                </p>
                                                            </a>
                                                        </li>
                                                    </apex:repeat>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="slds-picklist" data-aljs="picklist2">
                                            <button class="slds-button slds-button--neutral slds-picklist__label" aria-haspopup="true">
                                                <span class="slds-truncate" id="languageSelect" >Language</span>
                                                <svg aria-hidden="true" class="slds-icon">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Resource.slds)}/assets/icons/utility-sprite/svg/symbols.svg#down"></use>
                                                </svg>
                                            </button>
                                            <div class="slds-dropdown slds-dropdown--left slds-hide">
                                                <ul class="slds-dropdown__list slds-dropdown--length-5" role="menu">
                                                    <apex:repeat value="{!Locales}" var="loc">
                                                        <li id="{!loc.value}" data-lang="{!loc.value}" class="slds-dropdown__item" role="presentation">
                                                            <a href="javascript:void(0)" role="menuitemradio">
                                                                <p class="slds-truncate">
                                                                    <svg aria-hidden="true" class="slds-icon slds-icon--selected slds-icon--x-small slds-icon-text-default slds-m-right--x-small">
                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Resource.slds)}/assets/icons/utility-sprite/svg/symbols.svg#check"></use>
                                                                    </svg>
                                                                    {!loc.label}
                                                                </p>
                                                            </a>
                                                        </li>
                                                    </apex:repeat>
                                                </ul>
                                            </div>
                                        </div>
                                        <button class="slds-button slds-button--neutral" onclick="setVals(); return false;" value="Preview">Preview</button>
                                        <button class="slds-button slds-button--neutral" onclick="showWait();enableTabClose();saveQuote(); return false;" value="Save to quote">Save</button>
                                        <button class="slds-button slds-button--neutral" onclick="showWait();enableTabClose();sendQuote(); return false;" value="Send as email">Email</button>
                                        <button class="slds-button slds-button--neutral" onclick="closeTab();return false;" value="Cancel">Cancel</button>
                                    </div>
                                </div>
                            </div>
                            <div class="slds-col">
                                <c:waitComponent ></c:waitComponent>
                                <apex:pageBlock id="pdf" >
                                    <apex:iframe height="800px" src="data:application/pdf;base64,{!pdf}" />
                                </apex:pageBlock>
                            </div>
                        </apex:form>
                    </div>
                </div>
            </div>
        </body>    
    </html>
</apex:page>