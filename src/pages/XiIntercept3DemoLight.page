<apex:page sidebar="false" showHeader="false">

<head>
<!-- 1. Paymetric integration: The following three <meta> tags are required to be in the <head>, as-is. -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="cache-control" content="no-cache, no-store, max-age=0, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    
<!-- 2. Paymetric integration: The two lines below for stylesheet and includescript needs to be placed in <head>, as-is. -->
    <apex:stylesheet value="{!URLFOR($Resource.XIeCommerce3CSS)}" />
    <apex:includescript value="{!URLFOR($Resource.XIeCommerce3JS)}" />
    
<!-- 3. Paymetric integration: the following <script> block needs to be included in <head>, as-is. -->    
    <script type="text/javascript">
        document.domain = getRelaxedDomain(location.hostname, parseInt("2"));
    </script>


    <style>
        body {
            font-size: 1.1em;
            margin: 10px 10px 10px 10px;
        }
        #info {
            font-size: 0.8em;
        }
        #wrapper {
            display: flex;
            margin: 10px 10px 10px 10px;
        }
        #left {
            flex: 0 0 30%;
        }
        #right {
            flex: 1;
        }
    </style>
    <script>
        function testApproveCreditCard() {
            //NOTE: gv_strToken is the tokenized representation of the credit card number.
            document.getElementById("tokenizedCardNo").value = gv_strToken;
            return false;
        }
    </script>
</head>
<body>
    
<!-- 4. Paymetric integration: The <div> immediately below is required, along with the child <iframe>. The ids, classes, and src needs to remain as-is.   -->
<!-- 4.a ** NOTE: The first argument passed into the InitForTokenizationCC function needs to map to the <input> on the user interface that represents the -->
<!--              credit card. Specifically, the id of the <input> needs to be passed into the function, and in this case, is 'CardNo'.                   -->              
    <div id="xiFrameDiv" class="xiFrameOverlay">
        <iframe id="xiFrame" class="xiFrameFormat" src="XiIntercept3RQ" 
            onload="return InitForTokenizationCC('CardNo', true);">
        </iframe>
    </div>
    
<!-- 5. Paymetric integration: mandatory <div> required, otherwise Paymetric integration javascript fails.  -->
<!--    This div will have no visual implact to the Order Entry integration.                                -->
    <div id="xiFadeDiv" class="xiFadeOverlay"></div>
    
    <form id="form1">
        <h2>Paymetric Tokenization Demo</h2>  
        <div id="info">
            <h3>Reminder: you **MUST** be logged into IDEXX's VPN for all Paymetric operations (Tokenization and SOAP service calls).</h3>
            <p>
            For convenience, a test credit card number (4444333322221111) is pre-populated in the Credit Card Number 
            field below. Place focus into this field, and then tab - or mouse out.
            </p>
            <p>
            After focus is lost from the Credit Card Number field (and after a brief wait as the tokenization
            process takes place), the visible Credit Card Number field will be replaced with a masked representation.
            When you see a masked representation, this is an inital indicator that tokenization was successful.
            </p>
            <p>
            For this demonstration, click on the "Pre Authorize Credit Card" button. This will invoke a simple
            Javascript function testApproveCreditCard(). In this function, the Tokenized Card Number field will 
            populate with the tokenized value of the credit card number that was initially supplied. This is the
            true confirmation that tokenization was successful.
            </p>
            <p>
            For integration within the Order Entry page, the tokenized card number should not display. Only
            the masked representation of the credit card number should display - again, acting as a visual
            confirmation that the tokenization took place. Another suggestion: make sure that "gv_strToken"
            (seen in the Javascript function testApproveCreditCard() - line 41 - is not zero-length).
            </p>
            <p>
            The tokenized value of the credit card will need to be passed (in the request) to the Paymetric Pre Authorize SOAP call.
            </p>
            <p>
            This Visualforce page documents what is required to be placed in the Order Entry Visualforce page for
            full integration. There are six (6) html comment blocks in all.
            </p>
        </div>
        <hr/>
        <div id="wrapper">
            <div id="left">
                <div>
                    Credit Card Number:    
                </div>
                <div>
<!-- 6. Paymetric integration: the credit card <input> field's id must be "CardNo", otherwise Paymetric tokenization fails.      -->
<!--    Note that for this example, a test credit card value was furnished. Please omit the initial value on the Order Entry UI. -->                     
                    <input size="24" id="CardNo" type="text" value="4444333322221111" />   
                </div> 
            </div>
            <div id="right">
                <div>
                    Tokenized Card Number (this value will be passed to the SOAP call):    
                </div>
                <div>
                    <input size="40" id="tokenizedCardNo" type="text" value="" />    
                </div> 
            </div>
        </div>
        <div>
            <input id="cmdPayment" type="button" onclick="testApproveCreditCard();" value="Pre Authorize Credit Card" />
        </div>
    </form>
</body>

</apex:page>