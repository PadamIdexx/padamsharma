/**************************************************************************************
* Apex Class Name: Test_ShipToSearch
*
* Description: Test class for the ShipToSearch callout
*
*                   
* Developer           Date              Description
* ------------------------------------------------------------------------------------                 
* Heather Kinney      11.23.2016        Original Version
*
*************************************************************************************/

@isTest
public class Test_ShipToSearch {

    
    static testMethod void testSearchShipToByAccountName() {
        User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
        system.runAs(currentUser){
            Account thisAccount = new Account();
            thisAccount = CreateTestClassData.createCustomerAccount();
            thisAccount.SAP_Customer_Number__c = '1098';
            
            ShipToSearch controller = new ShipToSearch(new ApexPages.StandardController(thisAccount));
            ShipToSearch sc = new ShipToSearch();
            
            List<Account> partnerFunctionList = ShipToSearch.searchAccounts('CP','1138');
            
            System.assertNotEquals(null, partnerFunctionList);
        }
        
    }
    
    static testMethod void testSearchShipToNoReturnInfo() {
        //negative testing
        User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
        system.runAs(currentUser){
            Account thisAccount = new Account();
            thisAccount = CreateTestClassData.createCustomerAccount();
            thisAccount.SAP_Customer_Number__c = '1098';
            
            ShipToSearch controller = new ShipToSearch(new ApexPages.StandardController(thisAccount));
            ShipToSearch sc = new ShipToSearch();
            
            List<Account> partnerFunctionList = ShipToSearch.searchAccounts('OO', '00000');
            
            System.assertNotEquals(null, partnerFunctionList);
            
        }
        
    }
    

}