/* Class Name  : Test_InlineCampaignsRelatedListTaskCtlr

 *Description : Test Class for testing that when a User Logs a Call, checks the required campaign and updates its status,
 then the required campaign & its members must be there on the account detail page
 
 *Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                  Date           Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
  Aditya Sangawar            11/5/2015       Created
*/

@isTest
public with sharing class Test_InlineCampaignsRelatedListTaskCtlr{
     static testMethod void Test_InlineCampaignsRelatedListTaskCtlr() {
        
            // Create common test accounts
            User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
            system.runAs(currentUser){
            
            Account acc = CreateTestClassData.createCustomerAccount();
            
            Contact cont = new Contact();
            cont.LastName = 'testContact1';
            cont.AccountId = acc.Id;
            insert cont;
            
            Contact cont2 = new Contact();
            cont2.LastName = 'testContact2';
            cont2.AccountId = acc.Id;
            insert cont2;
            
            
            acc.Primary_Contact__c = cont.Id;
            update acc;
            
                     
            Campaign camp = CreateTestClassData.reusableCampaign();
            insert camp ;
                 
            CampaignMember cm = CreateTestClassData.reusableCampaignMember(camp.Id,cont.Id);
            insert cm; 
            
            CampaignMember cm2 = CreateTestClassData.reusableCampaignMember(camp.Id,cont2.Id);
            insert cm2; 
                       
            Task tsk = CreateTestClassData.reusableTask();
            tsk.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Log An Interaction').getRecordTypeId();
            tsk.OwnerId = currentUser.Id;
            tsk.WhatId = camp.Id;
            tsk.selectedcampaign__c = cm.id +  ';' + cm2.id ;           
            insert tsk; 
                        
            Test.startTest();
                ApexPages.StandardController std = new ApexPages.StandardController(tsk);
                ApexPages.currentPage().getParameters().put('what_id',acc.Id);
                LogAnInteractionExtension LogCall = new LogAnInteractionExtension(std);          
                ApexPages.StandardController std2 = new ApexPages.StandardController(tsk);
                InlineCampaignsRelatedListTaskController InlineCampaignsRelatedListTask = new InlineCampaignsRelatedListTaskController(std2);
                system.assertequals(InlineCampaignsRelatedListTask.relatedCampMember.size(),2);
            Test.stopTest();   
        }  
    }   
    
        static testMethod void Test_NoCampaignMember() {
      
            // Create common test accounts
            User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
            system.runAs(currentUser){
            
            Account acc = CreateTestClassData.createCustomerAccount();
            
            Contact cont = new Contact();
            cont.LastName = 'testContact1';
            cont.AccountId = acc.Id;
            insert cont;
            
            acc.Primary_Contact__c = cont.Id;
            update acc;
            
            Campaign camp = CreateTestClassData.reusableCampaign();
            insert camp ;               
                         
            Task tsk = CreateTestClassData.reusableTask();
            tsk.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Log An Interaction').getRecordTypeId();
            tsk.OwnerId = currentUser.Id;
            tsk.WhatId = camp.Id;
            tsk.selectedcampaign__c = null;
            insert tsk;
                        
            Test.startTest();
                ApexPages.StandardController std = new ApexPages.StandardController(tsk);
                ApexPages.currentPage().getParameters().put('what_id',acc.Id);
                LogAnInteractionExtension LogCall = new LogAnInteractionExtension(std);            
                ApexPages.StandardController std2 = new ApexPages.StandardController(tsk);
                InlineCampaignsRelatedListTaskController InlineCampaignsRelatedListTask = new InlineCampaignsRelatedListTaskController(std2);
                system.assertequals(InlineCampaignsRelatedListTask.relatedCampMember.size(),0);
                system.assertequals(InlineCampaignsRelatedListTask.noRecordsFound[0],'No records found');
            Test.stopTest();
           
        }  
    } 
}