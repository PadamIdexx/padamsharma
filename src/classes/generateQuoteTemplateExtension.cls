public class generateQuoteTemplateExtension{
    
    public String url{get;set;}
    public static String pdf{get;set;}
    public String lang{get;set;}
    public String temp{get;set;}
    public String quoteContact{get;set;}
    public String docId{get;set;}
    public String quoteId{get;set;}
    private PageReference pg{get;set;}
    private User currentUserInfo{get;set;}
    private String defaultUserLanguage{get;set;}
    
    public generateQuoteTemplateExtension(ApexPages.StandardController sc){
        inititalize(sc.getId());
    }
    public void inititalize(String qt){
        quoteId = qt;
    }
    public void setupPage(){
        temp = '';
        lang='';
        docId = '';
        //get system language for user and store for reset
        currentUserInfo = [select LanguageLocaleKey from User where Id = :UserInfo.getUserId()];
        defaultUserLanguage = currentUserInfo.LanguageLocaleKey;
        //get instance of template to show initital preview
        List<String> tempList = new List<String>();
        for(QuoteTemplate__c qt : QuoteTemplate__c.getAll().values()){
            tempList.add(qt.Name);
        }   
        QuoteTemplate__c template = QuoteTemplate__c.getInstance(tempList.get(0));
        if(!Test.isRunningTest())temp = template.Template_Id__c;
        //generate initial view
        createView();
    }
    public void renderPDFwithUserSelection(){
        setTemplateId();
        changeUserLanguage();
        createPDF();
    }
    public void setTemplateId(){
        system.debug('temp ' + temp);
    }
    public void changeUserLanguage(){
        //redraw pdf with new language
        system.debug('language ' + lang);
        if(string.isNotBlank(lang)){
            currentUserInfo.LanguageLocaleKey = lang;
            update currentUserInfo;
        }
    }
    
    public void resetUserLanguage(){
        //only modify dom if there has been a change
        system.debug('lang ' + lang);
        system.debug('default ' + defaultUserLanguage);
        if(lang != defaultUserLanguage){
            currentUserInfo.LanguageLocaleKey = defaultUserLanguage;
            update currentUserInfo;
        }
    }
    
    public pageReference createView(){
        //with the language and template id, setup the pdf viewer
        url = '/quote/quoteTemplateDataViewer.apexp?id=';
        url += quoteId;
        url += '&headerHeight=190&footerHeight=188&summlid=';
        url += temp;
        url += '#toolbar=1&navpanes=0&zoom=90';
        pg = new PageReference(url);
        
        createPDF();
        
        return null;
    }
    
    private void createPDF(){
        if(Test.isRunningTest()){
            Blob b = Blob.valueOf('Unit Test');
            pdf = EncodingUtil.base64Encode(b);
        }
        else{
            Blob b = pg.getContentAsPDF();
            pdf = EncodingUtil.base64Encode(b);
        }
        
    }
    
    public List<selectOption> getLocales(){
        //access active languages from custom setting
        List<selectOption> options = new List<selectOption>();
        
        for(ActiveLanguages__c lng : ActiveLanguages__c.getAll().values()){
            options.add(new SelectOption(lng.ISO_code__c , lng.Name));
        }   
        options = ListSortHelper.selectOptionSortByLabel(options);
        return options;
    }
    
    public List<selectOption> getTemplates(){
        //using the custom setting for templates, fetch values. Need to use custom label because templates are not currently available via metadata as of API v39
        List<selectOption> options = new List<selectOption>();
        
        for(QuoteTemplate__c qt : QuoteTemplate__c.getAll().values()){
            options.add(new SelectOption(qt.Template_Id__c , qt.Name));
        }   
        options = ListSortHelper.selectOptionSortByLabel(options);
        return options;
    }
    
    public void save(){
        QuoteDocument doc = attachQuoteToDatabase();
        resetUserLanguage();
    }
    
    private QuoteDocument attachQuoteToDatabase(){
        Blob b = blob.valueOf('');
        
        if(Test.isRunningTest()){
            b = Blob.valueOf('Unit Test');
        }
        else{
            b = pg.getContent();
        }
        QuoteDocument doc = new QuoteDocument(Document = b, QuoteId=quoteId);
        
        Database.SaveResult insertResult = Database.insert(doc, false);
        System.debug('Result: ' + insertResult);
        System.debug('Quote PDF created: ' + doc.Id);  
        
        return doc;
    }
    
    public pageReference send(){
        
        QuoteDocument doc = attachQuoteToDatabase();
        
        Database.SaveResult insertResult = Database.insert(doc, false);
        System.debug('Result: ' + insertResult);
        //System.debug('Quote PDF created: ' + doc.Id);  
        docId = doc.Id;
        System.debug('Doc Id: ' + docId);  
        
        return null;
    }
    
}