/****************************************************************************
* Apex Class Name    : WebServiceUtil
* Description        : A generic utility class the
                        a. Methods to Prepare Requests(REST/SOAP?/Continuation)
                        b. Methods to invoke ContactPersonProcessing_OutBindingQSPort points, parse responses
                        c. Handle Exceptions and retries 
                        d. Invoke ServiceLogging Utility methods to format the Integration Logs 
* Modification Log   :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Sirisha Kodi              Nov 17 2015             Created
****************************************************************************/
public class WebServiceUtil {
    
    //Static Variables START---------------------------------------------------
    public static final String ORDER_SIMULATE           = 'Order Simulate';      
    public static final String ORDER_CREATE             = 'Order Create';
    public static final String ORDER_PROCESSING         = 'Order Processing';  
    public static final String SALES_PROCESSING         = 'ORDER PROCESSING';  
    public static final String CONTACT_PROCESSING       = 'CONTACT PROCESSING';
    public static final String CUSTOMER_PROCESSING      = 'CUSTOMER_PROCESSING';
    public static final String DAILY_DOMAIN_PROCESSING  = 'DAILY DOMAIN VALUE PROCESSING'; 
    public static final String WEEKLY_DOMAIN_PROCESSING = 'WEEKLY DOMAIN VALUE PROCESSING';
    public static final String PAYMETRIC_PROCESSING     = 'PAYMETRIC PROCESSING';
    public static final String POINTS_PROCESSING        = 'POINTS PROCESSING';
    public static final String OUTPUT_PROCESSING        = 'OUTPUT PROCESSING';
    public static final String MATERIAL_PROCESSING      = 'MATERIAL PROCESSING';
    public static final String WEB_IDEXX_POINTS         = 'WEB IDEXX POINTS';
    public static final String LAB_ORDERS               = 'LAB_ORDERS';
    public static final String LynxxImages              = 'LYNXX_IMAGES';
    public static final String USLabs                   = 'US_LABS';
    public static final String USLabsForm               = 'US_LABS_FORM';
    public static final String ServiceEvents            = 'SERVICE_EVENTS';
    public static final String BEACON_ENDPOINT          = 'BEACON_ENDPOINT';
    public static final String VC_PLUS                  = 'VetConnect_LOB';
    public static final String SEDIVUE_AUTOREPLENISH    = 'SEDIVUE_AUTOREPLENISH';     
    public static final String ENDPOINT                 = 'endpoint_x';
    public static final String RETRY                    = 'retry';
    public static final String DOCUMENTTYPE             = 'DocumentType';
    public static final String PURCHASEORDERTYPE        = 'PurchaseOrderType';
    public static final String PAYMENTTERMS             = 'PaymentTerms';
    public static final String LANGUAGE                 = 'Language';
    public static final String DISTRIBUTIONCHANNEL      = 'DistributionChannel';
    public static final String DISCOUNTCAP              = 'DiscountCap';
    public static final String DISCOUNTCAPVALUE         = 'DiscountCapValue';
    public static final String CREDIT_CONTROL_CHECK     = 'Credit Control Block Check';
    public static final String CREDITCONTROLBLOCKCHECK  = 'creditControlBlockCheck';
       
    
    //Static Variables END------------------------------------------------------
    
    //Contact Read response instance
    public static ContactPersonProcessingResponse.ContactPersonsQuery_Sync contactPersonQueryresponse = new ContactPersonProcessingResponse.ContactPersonsQuery_Sync();
     //Contact Update response instance
    public static ContactPersonProcessingResponse.MessagesReturn contactPersonUpdateresponse = new ContactPersonProcessingResponse.MessagesReturn();
     //Contact Add response instance
    public static ContactPersonProcessingResponse.MessagesReturn contactPersonAddresponse = new ContactPersonProcessingResponse.MessagesReturn();
    
    
    
    /*
     * @Method name  : contactPersonReadCall
     * @Description  : This method is invoked from the OrderCreation Wizard on click of the Email link.
                       This contains logic to Prepare the request, invoke the order simulate web service and parse the response. 
                       This method also handles the Request/Response/exception logs in the System Log object
     * @Param        : Request 
     */
    public ContactPersonProcessingResponse.ContactPersonsQuery_Sync contactPersonReadCall(ContactPersonProcessingRequest.ContactPersonQuery_Sync request){
        
        try{
            
            ContactPersonProcessingService.ContactPersonProcessing_OutBindingQSPort  contactService = new ContactPersonProcessingService.ContactPersonProcessing_OutBindingQSPort(); 
            //call the utility method
            if(!Test.isRunningTest()) {
                contactPersonQueryresponse = contactService.ContactPersonRead(request);
            }
            
            return contactPersonQueryresponse;
        }catch(Exception ex){
            return null;
        }
    }
    
    /*
     * @Method name  : contactPersonUpdateCall
     * @Description  : This method is invoked from the OrderCreation Wizard on click of the Email link.
                       This contains logic to Prepare the request, invoke the order simulate web service and parse the response. 
                       This method also handles the Request/Response/exception logs in the System Log object
     * @Param        : Request 
     */
    public ContactPersonProcessingResponse.MessagesReturn contactPersonUpdateCall(ContactPErsonProcessingRequest.ContactPersonUpdate_Sync request){
        try{
            ContactPersonProcessingService.ContactPersonProcessing_OutBindingQSPort contactService = new ContactPersonProcessingService.ContactPersonProcessing_OutBindingQSPort();
            //call the utility method
            if(!Test.isRunningTest()) {
                contactPersonUpdateresponse = contactService.ContactPersonUpdate(request);
            }

            return contactPersonUpdateresponse;
        }catch(Exception ex){
            return null;
        }
    }
    
    /*
     * @Method name  : contactPersonAddCall
     * @Description  : This method is invoked from the OrderCreation Wizard on click of the Email link.
                       This contains logic to Prepare the request, invoke the order simulate web service and parse the response. 
                       This method also handles the Request/Response/exception logs in the System Log object
     * @Param        : Request 
     */
    public ContactPersonProcessingResponse.MessagesReturn contactPersonAddCall(ContactPersonProcessingRequest.ContactPersonCreate_Sync request){
        
        try{
            ContactPersonProcessingService.ContactPersonProcessing_OutBindingQSPort  contactService = new ContactPersonProcessingService.ContactPersonProcessing_OutBindingQSPort();
            //call the utility method
            if(!Test.isRunningTest()) {
                contactPersonAddresponse = contactService.ContactPersonCreate(request);
            }
            return contactPersonAddresponse;
        }catch(Exception ex){
            return null;
        }
    }

      /*
      * Method name: retreiveParameters
      * @description: query custom metadata type to fetch the key-value pairs 
                      and set the endpoint and Url 
      * @param sIntegratioName     - Name of integration point -- differs based on the point
      * @returnType : Map - Key;Value pairs corresponding to the integrationName
      */
      public static map<String, String> keyValuePairs = new Map<String,String>();
      public static map<String, String> retreiveParameters(String sIntegratioName){
        keyValuePairs = new Map<String,String>();
        if(keyValuePairs.size() == 0) {
            List<Idexx_Parameter__mdt> parameterList = [Select MasterLabel, DeveloperName, Name__c, Value__c,Object_Api__c from 
                                                                Idexx_Parameter__mdt where MasterLabel=:sIntegratioName];
            if(parameterList<>null && parameterList.size()>0){
                for(Idexx_Parameter__mdt param: parameterList){
                    keyValuePairs.put(param.Name__c, param.Value__c);
                }
            }
        }
        return(keyValuePairs);
    }

    
    //TODO: The two functions that follow have been replaced with implementations in 
    //WebServiceHelper.  The ones in this class have been retrained in order to not break
    //existing code that may use them.  For now, these functions just bridge to WebServiceHelper.
    //
    //These will eventually be removed.  Use WebServiceHelper functions directly if you are 
    //implementing something new.    
    
   public static String retreiveEndpoints(String sIntegratioName){
       	return WebServiceHelper.getServiceEndpoints(sIntegratioName);
   }
        
   public static String retrieveUserId(String userId){
       return WebServiceHelper.getIDEXXNetworkIdForUser(userId);
   }
}