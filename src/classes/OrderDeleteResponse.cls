public with sharing class OrderDeleteResponse {
    //Web Service call out related constants.
    //
    // SOAP Action
    public final static String SOAPACTION = 'http://sap.com/xi/WebService/soap1.1';
    
    // XML Namespace for this object
    // Salesforce representation of Namespace
    public final static String NAMESPACE = 'http://idexx.com/sappo/sales/salesforce';
    public final static String SF_NAMESPACE = 'idexxComSappoSalesSalesforce';
    
    // XML Namespace of types    
    // Salesforce representation of Namespace   
    public final static String XMLTYPE_NAMESPACE = 'http://idexx.com/sappo/global/types';
	public final static String SF_XMLTYPE_NAMESPACE = 'idexxComSappoGlobalTypes';
     
	// Name of Class that carries the payload.  There is no generic way to get this at this 
	// from the obect at runtime at this time. 
    public final static String PAYLOAD_CLASSNAME = 'SalesOrderDeleteResponse';
    
    public class SalesOrderDeleteResponse{
    	public Return_element[] Return_x;

        private String[] Return_x_type_info = new String[]{'Return',NAMESPACE,null,'0','-1','false'};            
       
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'Return_x'};    
    }
    
    public class Return_element {
        public String MessageType;
        public String MessageID;
        public String MessageNumber;
        public String Message;
        
        private String[] MessageType_type_info = new String[]{'MessageType',NAMESPACE,null,'0','1','false'};
        private String[] MessageID_type_info = new String[]{'MessageID',NAMESPACE,null,'0','1','false'};
        private String[] MessageNumber_type_info = new String[]{'MessageNumber',NAMESPACE,null,'0','1','false'};
        private String[] Message_type_info = new String[]{'Message',NAMESPACE,null,'0','1','false'};
            
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'MessageType','MessageID','MessageNumber','Message'};
    }    

    /*
     * Transformer - Construct an SAPOrder from a response object
     */ 
    public static SAPOrder transform(SAPOrder order, OrderDeleteResponse.SalesOrderDeleteResponse response) {    
        //process return values
        if ((null <> response.return_x) && (response.return_x.size() > 0)) {
			List<SAPOrder.Return_x> returns = new List<SAPOrder.Return_x>();

            for (OrderDeleteResponse.Return_element r : response.return_x) {
                SAPOrder.Return_x return_x = new SAPOrder.Return_x();
                
                return_x.messageType = r.MessageType;
                return_x.messageId = r.MessageID;
                return_x.messageNumber = r.messageNumber;
                return_x.message = r.message;
                
                returns.add(return_x);
            }
            
            //replace whatever is there already
	        order.returns = returns;
        }
        
        return order;
    }
    
    
}