/*
 * Class implementing the service call to the SAP Customer Detail Service.
 */ 
public class CustomerDetailService {
    
    public String endpoint_x;
    public Map<String,String> inputHttpHeaders_x;
    public Map<String,String> outputHttpHeaders_x;
    public String clientCertName_x;
    public String clientCert_x;
    public String clientCertPasswd_x;
    public Integer timeout_x;
    private String[] ns_map_type_info = new String[]{'http://idexxi.com/sapxi/pocbea/customer360', 'idexxiComSapxiPocbeaCustomer360'};
        
        
    public CustomerDetailService(){
	    String endPointURL = WebServiceUtil.retreiveEndpoints(WebServiceUtil.CUSTOMER_PROCESSING);
            
        endpoint_x = endPointURL!=null ? endPointURL : '';
        inputHttpHeaders_x = new Map<String, String>();
            
        APIUser__c creds = APIUser__c.getInstance('API USER');
        if(creds !=null){
    	    String sessionId = Login.login(creds.Username__c,creds.Password__c+creds.Security_Token__c);
            inputHttpHeaders_x.put('sessionId', sessionId);
        } 
	}
    
    public CustomerDetailResponse.CUSTOMER_GETDETAIL_Response getCustomerDetail(CustomerDetailRequest.CUSTOMER_GETDETAIL_Request readRequest) {
            CustomerDetailRequest.CUSTOMER_GETDETAIL_Request request_x = new CustomerDetailRequest.CUSTOMER_GETDETAIL_Request();
            request_x= readRequest;
            CustomerDetailResponse.CUSTOMER_GETDETAIL_Response response_x;
            Map<String, CustomerDetailResponse.CUSTOMER_GETDETAIL_Response> response_map_x = new Map<String, CustomerDetailResponse.CUSTOMER_GETDETAIL_Response>();
            response_map_x.put('response_x', response_x); 
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    'http://sap.com/xi/WebService/soap1.1',
                    'http://idexxi.com/sapxi/pocbea/customer360',
                    'MT_CUSTOMER_GETDETAIL_Request',
                    'http://idexxi.com/sapxi/pocbea/customer360',
                    'MT_CUSTOMER_GETDETAIL_Response',
                    'CustomerDetailResponse.CUSTOMER_GETDETAIL_Response'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }    
    
            
}