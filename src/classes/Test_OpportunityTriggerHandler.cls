/**
*       @author Hemanth Vanapalli
*       @date   13/10/2015   
        @description   Test Class with unit test scenarios to cover the OpportunityTriggerHandler class
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Hemanth Vanapalli                13/10/2015          Original Version
**/

@isTest(seeAllData=false)
private class Test_OpportunityTriggerHandler {
    static testMethod void opportunityUnitTest() {
        Account testAccount = CreateTestClassData.createCustomerAccount();
        System.assert(testAccount.Id != null);
        Opportunity testOpportunity = CreateTestClassData.createTenderOpportunity();
        System.assert(testOpportunity.Id != null);
        List<Opportunity> childOpportunities = CreateTestClassData.insertChildOpportunities(testOpportunity,10);        
        System.assertEquals(childOpportunities.size(), 10);
        childOpportunities[0].Amount = childOpportunities[0].Amount + 100;
        update childOpportunities[0];
        System.assert(childOpportunities[0].Amount != 0);
    }
}