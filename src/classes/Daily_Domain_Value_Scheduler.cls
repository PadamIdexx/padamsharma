/* Class Name : Daily_Domain_Value_Scheduler
 * Description : Class to update Domain Values daily and Weekly
 * Created By : Raushan Anand
 * Created On : 10-Nov-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand              10-Nov-2015               Initial version.
 **************************************************************************************/
global class Daily_Domain_Value_Scheduler implements Schedulable{
    global void execute(SchedulableContext SC) {
        UpdateDomainValueService.callDailyDomainService();

    }
}