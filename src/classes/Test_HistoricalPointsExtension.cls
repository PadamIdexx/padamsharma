/**
*    @author         Pooja Wade
*    @date           01/02/2016  
     @description    This is a test class for HistoricalPointsExtension class
     Modification Log:
    ------------------------------------------------------------------------------------
    Developer                            Date                Description
    ------------------------------------------------------------------------------------
    Pooja Wade                        01/02/2016          Initial Version
**/

@isTest
public class Test_HistoricalPointsExtension {

   static testMethod void HistoricalPointsExtension_test() {
      User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
      system.runAs(adminUser){
       
      Account customerAccount = new Account();
      customerAccount = CreateTestClassData.createCustomerAccount();
      customerAccount.SAP_Customer_Number__c = '0000001494';
      update customerAccount;
      
      
     
    
      idexxiComSapxiPntsservicesCalc.DT_PNTS_DETAIL_HIST_RESPONSE response = new idexxiComSapxiPntsservicesCalc.DT_PNTS_DETAIL_HIST_RESPONSE();
      response.TRANSACTION_DETAIL = new List<idexxiComSapxiPntsservicesCalc.TRANSACTION_DETAIL_element>();
     // idexxiComSapxiPntsservicesCalc.TRANSACTION_DETAIL_element det = new idexxiComSapxiPntsservicesCalc.TRANSACTION_DETAIL_element();
      //det.AVAILABLE_BAL = '499.06';
      response.EXCLUDE_CHILD = 'yes';
      response.TRANSACTION_DETAIL = new List<idexxiComSapxiPntsservicesCalc.TRANSACTION_DETAIL_element>();
       
      for(integer i=0;i<3;i++) {
      	idexxiComSapxiPntsservicesCalc.TRANSACTION_DETAIL_element det = new idexxiComSapxiPntsservicesCalc.TRANSACTION_DETAIL_element();
      	det.AVAILABLE_BAL = '499.06';      	
      	det.TRANS_TYPE = i==0 ? 'POO' : 'CPP';
      	det.TRANS_DATE = System.today();
      	response.TRANSACTION_DETAIL.add(det);
      }
     // response.AVAILABLE_BAL = '499.06';
      Test.startTest(); 
      
     System.debug('response here is:'+response);
     
      //response.TRANSACTION_DETAIL_element[] innerRes = det[0];
      ApexPages.StandardController std = new ApexPages.StandardController(customerAccount);
      ApexPages.currentPage().getParameters().put('id',customerAccount.Id);
      HistoricalPointsExtension testHist = new HistoricalPointsExtension(std);
     for(integer j=0;j<3;j++) {
     	
      	idexxiComSapxiPntsservicesCalc.TRANSACTION_DETAIL_element det = new idexxiComSapxiPntsservicesCalc.TRANSACTION_DETAIL_element();
      	det.AVAILABLE_BAL = '578.06';
      	det.TRANS_TYPE = j==0 ? 'POO' : 'CPP';
      	det.TRANS_DATE = System.today();
      	response.TRANSACTION_DETAIL.add(det);
      }
      System.debug('inside response here is:'+response);
      testHist.getListExt();
      testHist.setExternalData();
      testHist.getHistoricalData();
      //testHist.getHistoricalData(customerAccount.SAP_Customer_Number__c);
     
      HistoricalPointsExtension.ExternalClassData wrapper = new HistoricalPointsExtension.ExternalClassData();
      wrapper.avlBal = '498.59';
       Test.stopTest();
      
      }
   
   }


}