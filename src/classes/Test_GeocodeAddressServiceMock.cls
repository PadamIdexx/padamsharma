/**
 * Created by sconnolly on 6/24/2016.
 */

@IsTest
global class Test_GeocodeAddressServiceMock implements HttpCalloutMock {

    global HTTPResponse respond(HTTPRequest req) {

        String jsonResp = '{ "results" : [ { "address_components" : [ { "long_name" : "1", "short_name" : "1", "types" : [ "street_number" ] }, { "long_name" : "Idexx Drive", "short_name" : "Idexx Dr", "types" : [ "route" ] }, { "long_name" : "Westbrook", "short_name" : "Westbrook", "types" : [ "locality", "political" ] }, { "long_name" : "Cumberland County", "short_name" : "Cumberland County", "types" : [ "administrative_area_level_2", "political" ] }, { "long_name" : "Maine", "short_name" : "ME", "types" : [ "administrative_area_level_1", "political" ] }, { "long_name" : "United States", "short_name" : "US", "types" : [ "country", "political" ] }, { "long_name" : "04092", "short_name" : "04092", "types" : [ "postal_code" ] }, { "long_name" : "2040", "short_name" : "2040", "types" : [ "postal_code_suffix" ] } ], "formatted_address" : "1 Idexx Dr, Westbrook, ME 04092, USA", "geometry" : { "bounds" : { "northeast" : { "lat" : 43.6628687, "lng" : -70.3740991 }, "southwest" : { "lat" : 43.660144, "lng" : -70.3770035 } }, "location" : { "lat" : 43.6615064, "lng" : -70.3755513 }, "location_type" : "ROOFTOP", "viewport" : { "northeast" : { "lat" : 43.6628687, "lng" : -70.3740991 }, "southwest" : { "lat" : 43.660144, "lng" : -70.3770035 } } }, "place_id" : "ChIJ2z9b-AKaskwR8EBSMmWpbNc", "types" : [ "premise" ] } ], "status" : "OK" }';
        HTTPResponse resp = new HTTPResponse();
        resp.setBody(jsonResp);
        return resp;
    }
}