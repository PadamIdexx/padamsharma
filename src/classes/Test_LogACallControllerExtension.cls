/* Class Name  : Test_LogACallControllerExtension
 *
 * Description : Test Class for the LogACallControllerExtension controller extension.
 * 
 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                  Date           Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Heather Kinney             06.09.2016     Created
 * 
*/
@isTest(SeeAllData = False)   
public with sharing class Test_LogACallControllerExtension {
 
    static testMethod void Test_LogACallControllerExtension_Save_Positive() {
        
        User currentUser = getCurrentUser();
        system.runAs(currentUser){
            test.startTest();
            
            Account acc = createCustomerAccount('Test1', 'test1', '1111111111'); 
            
            Contact cont = new Contact();
            cont.LastName = 'testContact1';
            cont.AccountId = acc.Id;
            insert cont;
            
            /*
            Contact cont2 = new Contact();
            cont2.LastName = 'testContact2';
            cont2.AccountId = acc.Id;
            insert cont2;
			*/
            
            acc.Primary_Contact__c = cont.Id;
            update acc;
            
            Campaign camp = CreateTestClassData.reusableCampaign();
            insert camp;
            
            CampaignMemberStatus cms = new CampaignMemberStatus();
            cms.CampaignId = camp.Id;
            cms.IsDefault = false;
            cms.Label = 'Participated';
            cms.SortOrder = 3;
            insert cms;
                 
            CampaignMember cm = CreateTestClassData.reusableCampaignMember(camp.Id,cont.Id);
            insert cm; 
            
            /*
            CampaignMember cm2 = CreateTestClassData.reusableCampaignMember(camp.Id,cont2.Id);
            insert cm2;
			*/

            Task tsk = CreateTestClassData.reusableTask();
            //tsk.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Inbound_Call').getRecordTypeId();
            tsk.OwnerId = currentUser.Id;
            tsk.WhatId = camp.Id;
            tsk.selectedcampaign__c = cm.id;
            tsk.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Log An Interaction').getRecordTypeId();
            System.debug('@@@ Check for Task Id' + tsk);
            insert tsk;

            ApexPages.StandardController std = new ApexPages.StandardController(tsk);
            ApexPages.currentPage().getParameters().put('what_id',acc.Id);
            LogACallControllerExtension LogCall = new LogACallControllerExtension(std);
            List<SelectOption> regionsSelectList = LogCall.regions;
            LogCall.CampaignWrapperList[0].selectCheck = true ;
            LogCall.CampaignWrapperList[0].CampStatus = 'Sent';
            LogCall.save();
            system.assertEquals(LogCall.CampaignWrapperList[0].CampStatus,'Sent');
            
            test.stopTest();
        }
        
    }
        
    
    private static Account createCustomerAccount(String name, String hin, String sapId) {     
        Account testAccount = new Account();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = name;
        testAccount.CurrencyISOCode = 'USD';
        testAccount.HIN__c = hin;
        testAccount.SAP_Customer_Number__c = sapId;
        insert testAccount;
        System.debug('just inserted account ');
        System.assertEquals(testAccount.Name, 'Test1');
        return testAccount;
    }
    
    private static User getCurrentUser() {
        //User currentUser = CreateTestClassData.reusableUser('System Administrator','admin2');
        User currentUser = CreateTestClassData.reusableUser('Inside Sales Rep-CAG','Caleb-Bi');
        return currentUser;
    }


}