@isTest(SeeAllData=true)
public class Test_OrderReadService {
    @isTest static void SalesOrderReadPositive() {
        Test.startTest();
     
        OrderReadRequest.SalesOrderReadRequest request = OrderTestDataFactory.createReadRequest();
        OrderReadResponse.SalesOrderReadResponse mockResponse = OrderTestDataFactory.createReadResponse();
      
        Test.setMock(WebServiceMock.class, new Test_SOAPReusableMockResponse(mockResponse));
       
        OrderReadService service = new OrderReadService();
 
        service.GetOrder(request);
        
        SAPOrder order = OrderTestDataFactory.createSAPOrder();
        System.debug('order = ' + order);
        System.debug('mockResponse = ' + mockResponse);
        
        order = OrderReadResponse.transform(order, mockResponse);
  
        Test.stopTest();			
    }
}