public class CPQQuoteAutoSyncUtil {
     @future
    public static void syncQuote(Map<Id, Id> quoteMap)
    {
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>();
        	for(Id currentQuote : quoteMap.keyset())
        	{
            	Opportunity opp = new Opportunity();
            	opp.Id = quoteMap.get(currentQuote);
            	opp.SyncedQuoteId = currentQuote;
            	oppMap.put(opp.Id, opp);
                for(Opportunity opportunity : [Select (Select Id, Transaction_ID__c From Quotes) From Opportunity where Id = :opp.Id]) {
                	List<Quote> quoteList = opportunity.Quotes;
                    if(quoteList.size() == 1) {
            			update oppMap.values();	
        			}
            	}
         	}
    }
}