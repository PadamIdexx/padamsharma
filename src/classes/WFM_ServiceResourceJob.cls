global class WFM_ServiceResourceJob implements Schedulable{
    
    global void execute(SchedulableContext SC) {

        List <ServiceResource> serviceResourceList = [Select Id,IsActive,WFM_SchedJobUpdate__c From ServiceResource where IsActive=True];        

        for(ServiceResource serviceResource: serviceResourceList){
             serviceResource.WFM_SchedJobUpdate__c = Datetime.now();
             }

        update(serviceResourceList);

        }
   }