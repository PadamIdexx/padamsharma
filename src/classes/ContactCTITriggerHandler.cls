/* Class Name  : ContactCTITriggerHandler
 * Description : Handler class which handles upserts and deletes on Phone_Lookup__c, based on
 *               phone number fields present on the Contact object. Phone_Lookup__c bridges between SFDC and legacy (Beacon)
 *               via sync back to Beacon to maintain consistency across systems for inbound CTI screenpops.
 * Used In     : ContactTrigger.trigger
 * Created By  : Heather Kinney
 * Created On  : 01-22-2016
 *
 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer              Date                   Modification ID       Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Heather Kinney         01-22-2016             n/a                   Initial version
 * Heather Kinney         01-31-2016             n/a                   Added logic to look at a custom setting; and only
 *                                                                     execute logic when the custom setting allows us to do so.
 *                                                                     This will allow us to be able to bypass this logic when
 *                                                                     contacts are bulk inserted (e.g. initial onboarding of contacts
 *                                                                     related to a line of business).
 *                                          
*/
public with sharing class ContactCTITriggerHandler {
	
	public SanitizedContactPhoneNumbers sanitizedNums {get; set;}
	
	public static final String NON_DIGITS = '[^0-9]';
	
    public void onAfterInsert(List<Contact> insertedContacts) {
    	if (!isConfiguredToHaltLogic()) {
    		performPhoneLookupUpsert(insertedContacts, null);
    	}
    }
    
    public void onAfterUpdate(List<Contact> updatedContacts, Map<Id,Contact> oldContactsMap) {
    	if (!isConfiguredToHaltLogic()) {
    		performPhoneLookupUpsert(updatedContacts, oldContactsMap);
    	}
    }
    
    public void onBeforeDelete(Map<Id,Contact> contactsToBeDeleted) {
    	if (!isConfiguredToHaltLogic()) {
    		performPhoneLookupSoftDelete(contactsToBeDeleted);
    	}
    }
    
    private boolean isConfiguredToHaltLogic() {
    	//leverages a hierarchical custom setting to determine if this trigger handler logic will
    	//execute or not. If we detect that we're coming in via a bulk load, we will bypass execution.
    	//However, if we are entering into this logic "interactively", logic will be allowed to continue.
    	//Bypassing during bulk loads is acceptible, as bulk-loaded Contacts and Phone_Lookup__c records 
    	//will take place during the time of bulk load. Otherwise, interactively (UI interaction) created
    	//and maintained contacts will leverage this logic.
    	boolean retVal = false;
    	
    	ContactCTItriggerExec__c customSettingForCurrentUser = ContactCTItriggerExec__c.getInstance(UserInfo.getUserId());
    	if (customSettingForCurrentUser != null) {
    		if (customSettingForCurrentUser.contactCTItriggerDisabled__c != null) {
    			retVal = customSettingForCurrentUser.contactCTItriggerDisabled__c;		
    		}
    	}
    	return retVal;
    }
    
    private void performPhoneLookupUpsert(List<Contact> contacts, Map<Id,Contact> oldContactsMap) {
    	
    	List<Phone_Lookup__c> phoneLookupsSoftDelete = null;
    	List<Phone_Lookup__c> phoneLookupsToUpsert = new List<Phone_Lookup__c>();
    	
		//Need to address if we are coming into this logic via an upsert. If we are, we will have a Map of Contacts.
		//And if we have a Map, need to comapre the old phone numbers of the Contact vs the new phone numbers of the Contact.
		//If any of the phone numbers are "removed"/deleted from the new Contact object, need to soft delete the corresponding 
		//phone entry in Phone_Lookup__c by setting the Is_Active__c flag to false.
		if (oldContactsMap != null) {
			phoneLookupsSoftDelete = generatePhoneLookupsForSoftDelete(contacts, oldContactsMap);	
			update phoneLookupsSoftDelete;	
		}
    	
    	
    	for (Contact contact : contacts) {
    		
    		//This map allows us to keep a running list of phone numbers that will be upserted.
    		//Needed to prevent duplicates from being placed in the phoneLookupsToUpsert list.
    		Map<String,String> phoneNumbersToAddMap = new Map<String,String>();
    		
			sanitizedNums = new SanitizedContactPhoneNumbers();
			
    		//Salesforce stores phone numbers on the Contact record with vanity/formatting characters.
    		//We remove vality/formatting chars as phone numbers are stored raw in the Phone_Lookup__c object:
    		sanitizeContactPhoneNumbers(contact); 
    	
 			System.debug('assistantPhone = ' + sanitizedNums.assistantPhone);
 			
 			//The logic block below covers the following scenario: what if a Contact has multiple phone numbers? 
 			//For example, a businessPhone, homePhone, and a mobilePhone? and let's say that the businessPhone was 
 			//already in the Phone Lookup table, but the homePhone and mobilePhone were not...we want a screen pop
 			//to happen for all...(screenpop is based on Contact phone numbers; however, we need to keep 
 			//Phone_Lookup__c up to date for legacy system - Beacon - bridge integration. We could have ISR 
 			//update Contact phone data; we need Beacon to be up to date for possible CS call-ins, and screenpops).
 			
 			//Function processContactAndUpsert receives the contact object and the phone number in "context" 
 			//(after sanitization), and then returns either the matching record from the Phone_Lookup__c object,
 			//or creates a new Phone_Lookup_c object for the new phone number - as it wasn't located in Phone_Lookup__c.
 			//SOQL outside of this cotaining for-loop will upsert en masse.
 
 			//NB: Length checks below are greater than 7 for sanitized phone numbers. 
 			// Accommodates North American numbers (10) digits, and International numbers. 
 			// E-164 format: Including country code, Solomon Islands is 8, and other countries are 
 			// greater than 8 (Sweden is min of 9 digits, others are greater).
 			// reference: https://stackoverflow.com/questions/14894899/what-is-the-minimum-length-of-a-valid-international-phone-number

 			if (sanitizedNums != null) {
 				if (sanitizedNums.assistantPhone.length() > 7) {				
 					if (phoneNumbersToAddMap.get(sanitizedNums.assistantPhone) == null) {
						Phone_Lookup__c phoneLookup = processContactAndUpsert(contact, sanitizedNums.assistantPhone);
 						phoneNumbersToAddMap.put(sanitizedNums.assistantPhone, sanitizedNums.assistantPhone);
 						phoneLookupsToUpsert.add(phoneLookup);
 					}
 				}
 				if (sanitizedNums.businessPhone.length() > 7) {	
 					if (phoneNumbersToAddMap.get(sanitizedNums.businessPhone) == null) {
						Phone_Lookup__c phoneLookup = processContactAndUpsert(contact, sanitizedNums.businessPhone);
						phoneNumbersToAddMap.put(sanitizedNums.businessPhone, sanitizedNums.businessPhone);
 						phoneLookupsToUpsert.add(phoneLookup);
 					}
 				}
 				if (sanitizedNums.homePhone.length() > 7) {
 					Phone_Lookup__c phoneLookup = processContactAndUpsert(contact, sanitizedNums.homePhone);
 					if (phoneNumbersToAddMap.get(sanitizedNums.homePhone) == null) {
						phoneNumbersToAddMap.put(sanitizedNums.homePhone, sanitizedNums.homePhone);
 						phoneLookupsToUpsert.add(phoneLookup);
 					}
 				}
 				if (sanitizedNums.mobilePhone.length() > 7) {
 					if (phoneNumbersToAddMap.get(sanitizedNums.mobilePhone) == null) {
 						Phone_Lookup__c phoneLookup = processContactAndUpsert(contact, sanitizedNums.mobilePhone);
						phoneNumbersToAddMap.put(sanitizedNums.mobilePhone, sanitizedNums.mobilePhone);
 						phoneLookupsToUpsert.add(phoneLookup);
 					}
 				}
 				if (sanitizedNums.otherPhone.length() > 7) {
 					if (phoneNumbersToAddMap.get(sanitizedNums.otherPhone) == null) {
 						Phone_Lookup__c phoneLookup = processContactAndUpsert(contact, sanitizedNums.otherPhone);
						phoneNumbersToAddMap.put(sanitizedNums.otherPhone, sanitizedNums.otherPhone);
 						phoneLookupsToUpsert.add(phoneLookup);
 					}
 				}
 				if (sanitizedNums.phone.length() > 7) {
 					if (phoneNumbersToAddMap.get(sanitizedNums.phone) == null) {
 						Phone_Lookup__c phoneLookup = processContactAndUpsert(contact, sanitizedNums.phone);
						phoneNumbersToAddMap.put(sanitizedNums.phone, sanitizedNums.phone);
 						phoneLookupsToUpsert.add(phoneLookup);
 					}
 				}
 				if (sanitizedNums.phoneCustom.length() > 7) {
 					if (phoneNumbersToAddMap.get(sanitizedNums.phoneCustom) == null) {
 						 Phone_Lookup__c phoneLookup = processContactAndUpsert(contact, sanitizedNums.phoneCustom);
						phoneNumbersToAddMap.put(sanitizedNums.phoneCustom, sanitizedNums.phoneCustom);
 						phoneLookupsToUpsert.add(phoneLookup);
 					}
 				}
 			}

    		
    	}
    	
    	System.debug('phoneLookupsToUpsert.size() = ' + phoneLookupsToUpsert.size());
    	//Outside of for-loop; done collecting data, now we're ready for Upsert...   			
   		if (phoneLookupsToUpsert != null && phoneLookupsToUpsert.size() > 0) {
	   		upsert phoneLookupsToUpsert;
	   	}

    }
    
    private void performPhoneLookupSoftDelete(Map<Id,Contact> contactsToBeDeleted) {   
    	List<Phone_Lookup__c> phoneLookupsSoftDelete = softDeleteDeletedContactLookups(contactsToBeDeleted);
    	update phoneLookupsSoftDelete;
    }
    
    
    private Phone_Lookup__c processContactAndUpsert(Contact contact, String searchPhoneNumber) {
    	//This method may be called multiple times, depending on if the contact has multiple phone numbers present.
		System.debug('===> in processContactAndUpsert. searchPhoneNumber is:  ' +  searchPhoneNumber);
		Phone_Lookup__c phoneLookup;    	
     	List<Phone_Lookup__c> phoneList = [SELECT Account__c, Id, Is_Active__c, IsDeleted, Match_Number__c, Phone_Number__c, SAP_Customer_Number__c 
 				 		FROM Phone_Lookup__c 
 				 		WHERE ( 
 				 			Phone_Number__C = :searchPhoneNumber
 						)]; 
 						
		boolean isNoMatchingPhoneLookupFound = true;
		
	
		//We will need to obtain the Account's SAP_Customer_Number__c via a SOQL query, 
		//as AccountId is only available on Contact.
		String contactAccountId = contact.AccountId;
		Account contactAccount = [SELECT SAP_Customer_Number__c FROM Account WHERE Id = :contactAccountId];
		String sapCustomerNumber = contactAccount.SAP_Customer_Number__c;
		System.debug('contactAccount.SAP_Customer_Number__c  = ' + sapCustomerNumber);

		if (phoneList != null) {
			if (phoneList.size() > 0) {
				//Due to Phone_Number__c being unique, we should only have one item in the list:
				for (Phone_Lookup__C returnedPhoneLookup : phoneList) {
					System.debug('located existing phoneLookup - assigning to phoneLookup...');
					//we will always ensure the Is_Active__c flag is set to true here, in case this row was
					//previously soft-deleted:
					returnedPhoneLookup.Is_Active__c = true;
					returnedPhoneLookup.Account__c = contactAccountId;
					returnedPhoneLookup.SAP_Customer_Number__c = sapCustomerNumber;
					phoneLookup = returnedPhoneLookup;	
					isNoMatchingPhoneLookupFound = false;
					break;
				}		
			} 	
    	}
    	if (isNoMatchingPhoneLookupFound) {
			System.debug('===> in processContactAndUpsert. isNoMatchingPhoneLookupFound. Constructing new obj  ');  			
			
			//Didn't find a corresponding match on the Phone Lookup object, so we need to construct a 
			//Phone_Number__c object, using data from the contact in context:

			if (contactAccount != null) {
				phoneLookup = new Phone_Lookup__c();
				String matchNumber = searchPhoneNumber.substring(0,6);
				System.debug('Phone_Lookup__c matchNumber = ' + matchNumber);
				
				phoneLookup.Account__c = contactAccountId; 
				phoneLookup.Is_Active__c = true;
				phoneLookup.Match_Number__c = matchNumber;
				phoneLookup.Phone_Number__c = searchPhoneNumber;
				phoneLookup.SAP_Customer_Number__c = sapCustomerNumber;
			}
    	}

		return phoneLookup;		
    
    }
    
    private List<Phone_Lookup__c> generatePhoneLookupsForSoftDelete(List<Contact> contacts, Map<Id,Contact> oldContactsMap) {
    	
    	List<Phone_Lookup__c> phoneLookups = new List<Phone_Lookup__c>();
		
		for (Contact newContactValue : contacts) {		
			//Attempt to locate the contact being processed in the companion map:
			Contact oldContactValue = oldContactsMap.get(newContactValue.Id);
			if (oldContactValue != null) {				
				 phoneLookups = assembleSoftDeleteRecords(oldContactValue, newContactValue);	
			}			
		}
		
		return phoneLookups;
    }
    
    private List<Phone_Lookup__c> softDeleteDeletedContactLookups(Map<Id,Contact> oldContactsMap) {
    	
    	List<Phone_Lookup__c> phoneLookups = new List<Phone_Lookup__c>();
    	
    	//This map allows us to keep a running list of phone numbers that will be upserted.
    	//Needed to prevent duplicates from being placed in the phoneLookupsToUpsert list.
    	Map<String,String> phoneNumbersToAddMap = new Map<String,String>();

		for (Contact contact: oldContactsMap.values()) {
			Phone_Lookup__c phoneLookupToSoftDelete;
			
			if (phoneNumbersToAddMap.get(contact.AssistantPhone) == null) {
				phoneLookupToSoftDelete = getPhoneLookupRecordToSoftDelete(contact.AssistantPhone);
				if (phoneLookupToSoftDelete != null) {
					phoneLookups.add(phoneLookupToSoftDelete);
					phoneNumbersToAddMap.put(contact.AssistantPhone, contact.AssistantPhone);	
				}
			}
				
			if (phoneNumbersToAddMap.get(contact.Business_Phone__c) == null) {
				phoneLookupToSoftDelete = getPhoneLookupRecordToSoftDelete(contact.Business_Phone__c);
				if (phoneLookupToSoftDelete != null) {
					phoneLookups.add(phoneLookupToSoftDelete);
					phoneNumbersToAddMap.put(contact.Business_Phone__c, contact.Business_Phone__c);	
				}
			}
			
			if (phoneNumbersToAddMap.get(contact.HomePhone) == null) {
				phoneLookupToSoftDelete = getPhoneLookupRecordToSoftDelete(contact.HomePhone);
				if (phoneLookupToSoftDelete != null) { 
					phoneLookups.add(phoneLookupToSoftDelete);
					phoneNumbersToAddMap.put(contact.HomePhone, contact.HomePhone);
				}
			}
			
			if (phoneNumbersToAddMap.get(contact.MobilePhone) == null) {
				phoneLookupToSoftDelete = getPhoneLookupRecordToSoftDelete(contact.MobilePhone);
				if (phoneLookupToSoftDelete != null) {
					phoneLookups.add(phoneLookupToSoftDelete);
					phoneNumbersToAddMap.put(contact.MobilePhone, contact.MobilePhone);
				}
			}
			
			if (phoneNumbersToAddMap.get(contact.OtherPhone) == null) {
				phoneLookupToSoftDelete = getPhoneLookupRecordToSoftDelete(contact.OtherPhone);
				if (phoneLookupToSoftDelete != null) {
					phoneLookups.add(phoneLookupToSoftDelete);
					phoneNumbersToAddMap.put(contact.OtherPhone, contact.OtherPhone);
				}
			}
			
			if (phoneNumbersToAddMap.get(contact.Phone) == null) {
				phoneLookupToSoftDelete = getPhoneLookupRecordToSoftDelete(contact.Phone);
				if (phoneLookupToSoftDelete != null) {
					phoneLookups.add(phoneLookupToSoftDelete);
					phoneNumbersToAddMap.put(contact.Phone, contact.Phone);
				}
			}
			
			if (phoneNumbersToAddMap.get(contact.Phone__c) == null) {
				phoneLookupToSoftDelete = getPhoneLookupRecordToSoftDelete(contact.Phone__c);
				if (phoneLookupToSoftDelete != null) {
					phoneLookups.add(phoneLookupToSoftDelete);
					phoneNumbersToAddMap.put(contact.Phone__c, contact.Phone__c);
				}
			}		
		}
		
		return phoneLookups;
    }
    
    
    public List<Phone_Lookup__c> assembleSoftDeleteRecords(Contact oldContactValue, Contact newContactValue) {
    
    	List<Phone_Lookup__c> phoneLookupsSoftDelete = new List<Phone_Lookup__c>();
 	
    	//we may have the situation where the incoming newContactValue object is null. For example - 
    	//if a phone type had a previous value; but now, through the UI - that incoming newContactValue is null.
    	//in order to best handle this situation - and situations where incoming old and new values are not null - 
    	//like when a phone number is changed from one value to another - we will instance the newContactValue only if it
    	//is null, and then set all of the phone values to a zero-length string...
		if (newContactValue == null) {
			newContactValue.AssistantPhone = '';
			newContactValue.Business_Phone__c = '';
			newContactValue.HomePhone = '';
			newContactValue.MobilePhone = '';
			newContactValue.OtherPhone = '';
			newContactValue.Phone = '';
			newContactValue.Phone__c = '';
		}
		
		//compare old vs new values; if we find a mis-match we will mark the old value as a soft delete.
		if (oldContactValue.AssistantPhone != newContactValue.AssistantPhone) {
			Phone_Lookup__c phoneLookupToSoftDelete = getPhoneLookupRecordToSoftDelete(oldContactValue.AssistantPhone);
			if (phoneLookupToSoftDelete != null) {
				phoneLookupsSoftDelete.add(phoneLookupToSoftDelete);
			}	
		}
		if (oldContactValue.Business_Phone__c != newContactValue.Business_Phone__c) {
			Phone_Lookup__c phoneLookupToSoftDelete = getPhoneLookupRecordToSoftDelete(oldContactValue.Business_Phone__c);
			if (phoneLookupToSoftDelete != null) {
				phoneLookupsSoftDelete.add(phoneLookupToSoftDelete);
			}	
		}
		if (oldContactValue.HomePhone != newContactValue.HomePhone) {
			Phone_Lookup__c phoneLookupToSoftDelete = getPhoneLookupRecordToSoftDelete(oldContactValue.HomePhone);
			if (phoneLookupToSoftDelete != null) {
				phoneLookupsSoftDelete.add(phoneLookupToSoftDelete);
			}	
		}
		if (oldContactValue.MobilePhone != newContactValue.MobilePhone) {
			Phone_Lookup__c phoneLookupToSoftDelete = getPhoneLookupRecordToSoftDelete(oldContactValue.MobilePhone);
			if (phoneLookupToSoftDelete != null) {
				phoneLookupsSoftDelete.add(phoneLookupToSoftDelete);
			}	
		}
		if (oldContactValue.OtherPhone != newContactValue.OtherPhone) {
			Phone_Lookup__c phoneLookupToSoftDelete = getPhoneLookupRecordToSoftDelete(oldContactValue.OtherPhone);
			if (phoneLookupToSoftDelete != null) {
				phoneLookupsSoftDelete.add(phoneLookupToSoftDelete);
			}	
		}
		if (oldContactValue.Phone != newContactValue.Phone) {
			Phone_Lookup__c phoneLookupToSoftDelete = getPhoneLookupRecordToSoftDelete(oldContactValue.Phone);
			if (phoneLookupToSoftDelete != null) {
				phoneLookupsSoftDelete.add(phoneLookupToSoftDelete);
			}	
		}
		if (oldContactValue.Phone__c != newContactValue.Phone__c) {
			Phone_Lookup__c phoneLookupToSoftDelete = getPhoneLookupRecordToSoftDelete(oldContactValue.Phone__c);
			if (phoneLookupToSoftDelete != null) {
				phoneLookupsSoftDelete.add(phoneLookupToSoftDelete);
			}	
		}
		
		return phoneLookupsSoftDelete;
		
    
    }
    
    private Phone_Lookup__c getPhoneLookupRecordToSoftDelete(String phoneNumber) {   	
    	Phone_Lookup__c phoneLookupToSoftDelete = null;
    	if (phoneNumber != null) {
	    	String sanitizedPhoneNumberToSoftDelete = sanitizePhoneNumber(phoneNumber);
			List<Phone_Lookup__c> phoneLookupsToSoftDelete = [SELECT Is_Active__c, Phone_Number__c FROM Phone_Lookup__c WHERE Phone_Number__C = :sanitizedPhoneNumberToSoftDelete];
			if (phoneLookupsToSoftDelete != null) {				
				if (phoneLookupsToSoftDelete.size() > 0) {					
					//Due to Phone_Number__c being unique, we should only have one item in the list:
					for (Phone_Lookup__C returnedPhoneLookup : phoneLookupsToSoftDelete) {					
						returnedPhoneLookup.Is_Active__c = false;
						phoneLookupToSoftDelete = returnedPhoneLookup;
						break;
					}
				}
			}
    	}
  	
    	return phoneLookupToSoftDelete;
    }
    
    public class SanitizedContactPhoneNumbers {
    	
    	public String assistantPhone {get;set;}
		public String businessPhone {get;set;}
		public String homePhone {get;set;}
		public String mobilePhone {get;set;}
		public String otherPhone {get;set;}
		public String phone {get;set;}
		public String phoneCustom {get;set;} //represents Phone__c on Contact object.
		
		public SanitizedContactPhoneNumbers() {
			assistantPhone = '';
			businessPhone = '';
			homePhone = '';
			mobilePhone = '';
			otherPhone = '';
			phone = '';
			phoneCustom = '';
		}
		
    }
    
    public void sanitizeContactPhoneNumbers(Contact contact) {
    	
    	if (sanitizedNums == null) {
    		sanitizedNums = new SanitizedContactPhoneNumbers();
    	}
    	
    	if (contact.AssistantPhone != null) {
			sanitizedNums.assistantPhone = sanitizePhoneNumber(contact.AssistantPhone);
    	}
    	if (contact.Business_Phone__c != null) {
			sanitizedNums.businessPhone = sanitizePhoneNumber(contact.Business_Phone__c);
    	}
    	if (contact.HomePhone != null) {
			sanitizedNums.homePhone = sanitizePhoneNumber(contact.HomePhone);
    	}
    	if (contact.MobilePhone != null) {
			sanitizedNums.mobilePhone = sanitizePhoneNumber(contact.MobilePhone);
    	}
    	if (contact.OtherPhone != null) {
			sanitizedNums.otherPhone = sanitizePhoneNumber(contact.OtherPhone);
    	}
    	if (contact.Phone != null) {
			sanitizedNums.phone = sanitizePhoneNumber(contact.Phone);
    	}
    	if (contact.Phone__c != null) {
			sanitizedNums.phoneCustom = sanitizePhoneNumber(contact.Phone__c);
    	}
         
     }
    
    
    public String sanitizePhoneNumber(String phoneNumberToSanitize) {	
  		//Removes all non-numeric digits to give back a sanitized value of the phone number. 
  		return phoneNumberToSanitize.replaceAll(NON_DIGITS,'');
    }
    

    
}