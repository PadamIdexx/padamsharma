public class SimulateWrapper extends SAPOrderWrapper {
	public override SAPOrder execute(SAPOrder order) {
        
        //transform SAPOrder to request
        OrderSimulateRequest.SalesOrderSimulateRequest request;
        OrderSimulateResponse.SalesOrderSimulateResponse response;
        SAPOrder simulatedOrder;
        
        //transform SAPOrder to request
        try {
        	request = OrderSimulateRequest.transform(order);
        } catch(Exception e) {
            System.debug('Error transforming SAPOrder to OrderSimulateRequest.');
            System.debug(e.getCause());
            System.debug(e.getMessage());
            
        	throw e;    
        }  
    
        //call service
        try {
            OrderSimulateService service = new OrderSimulateService();
            
            if (!Test.isRunningTest()) {
                 response = service.simulateOrder(request);            
            } else {
                response = OrderTestDataFactory.createSimulateResponse();
            }
       } catch(Exception e) {
            System.debug('Error calling Order Simulate Service.');
            System.debug(e.getCause());
            System.debug(e.getMessage());
            
        	throw e;             
        } 
            
        //transform response into new SAPOrder
        try {
	        simulatedOrder = OrderSimulateResponse.transform(order, response);  
        } catch(Exception e) {
            System.debug('Error transforming OrderSimulateResponse to SAPOrder.');
            System.debug(e.getCause());
            System.debug(e.getMessage());
            
        	throw e;              
        } 
        
        return simulatedOrder;
    }
}