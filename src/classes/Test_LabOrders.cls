@isTest
public with sharing class Test_LabOrders {

    @isTest static void getLabOrdersCollection() {
		Test.startTest();
        
            LabOrders.labReportSearchRequest_element request = LabOrderTestDataGenerator.createLabOrdersRequest();        
            LabOrders.getLabOrderCollectionsResponse_element response = LabOrderTestDataGenerator.createLabOrderCollectionResponse();
            Test.setMock(WebServiceMock.class, new Test_SOAPReusableMockResponse(response));
            
            LabOrders.LabOrderServiceSoapPort service = new LabOrders.LabOrderServiceSoapPort();
            service.getLabOrderCollections(request);
            
        Test.stopTest();
    }
    @isTest static void getLabOrdersDetail() {
		Test.startTest();
        
            LabOrders.labReportSearchRequest_element request = LabOrderTestDataGenerator.createLabOrdersRequest();        
            LabOrders.getLabOrderDetailsResponse_element response = LabOrderTestDataGenerator.createLabOrderDetailsResponse();
            Test.setMock(WebServiceMock.class, new Test_SOAPReusableMockResponse(response));
            
            LabOrders.LabOrderServiceSoapPort service = new LabOrders.LabOrderServiceSoapPort();
            service.getLabOrderDetails(request);
            
        Test.stopTest();
    }
}