@isTest
global class MockVetConnectPlusResponse implements HttpCalloutMock{
	  global HTTPResponse respond(HTTPRequest req) {
     
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"success":true,"response":"{\"practiceStatus\":{\"active\":true,\"modalitiesActive\":[\"REF_LAB\",\"RADIOLOGY\"],\"relatedPractices\":[]}}"}');
        res.setStatusCode(200);
        return res;
    }
    
    
}