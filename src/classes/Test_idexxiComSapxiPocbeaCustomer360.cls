/**
*    @author         Anudeep Gopagoni
*    @date           12/07/2015   
     @description    This is a test class for WSDL generated class idexxiComSapxiPocbeaCustomer360 written for testing the resend order/delivery confirmation emails functionality
     Modification Log:
    ------------------------------------------------------------------------------------
    Developer                            Date                Description
    ------------------------------------------------------------------------------------
    Anudeep Gopagoni                   11/01/2015          Initial Version
**/

@isTest public class Test_idexxiComSapxiPocbeaCustomer360{

    class WebSvcCalloutTest implements WebServiceMock {
        public void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response,
            String endpoint,
            String soapAction,
            String requestName,
            String responseNS,
            String responseName,
            String responseType
        ) {
            idexxiComSapxiPocbeaCustomer360.MessagesReturn response_x;
           Map<String, idexxiComSapxiPocbeaCustomer360.MessagesReturn> response_map_x = new Map<String, idexxiComSapxiPocbeaCustomer360.MessagesReturn>();
           response_map_x.put('response_x', response_x);
        }
    }

    static testMethod void orderConfirmationTest() {
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new WebSvcCalloutTest());
        idexxiComSapxiPocbeaCustomer360.HTTPS_Port  ob = new idexxiComSapxiPocbeaCustomer360.HTTPS_Port();
        idexxiComSapxiPocbeaCustomer360.MessagesReturn  output = new idexxiComSapxiPocbeaCustomer360.MessagesReturn(); 
        output = ob.OrderConfirmationResend('1438890222','EN','test');

        //System.assert(result.startsWith('herpderp'));
    }
    static testMethod void shippingConfirmationTest() {
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new WebSvcCalloutTest());
        idexxiComSapxiPocbeaCustomer360.HTTPS_Port  ob = new idexxiComSapxiPocbeaCustomer360.HTTPS_Port();
        idexxiComSapxiPocbeaCustomer360.MessagesReturn  output = new idexxiComSapxiPocbeaCustomer360.MessagesReturn(); 
        output = ob.DeliveryConfirmationResend('1438890222','EN','test');
        //MainClient.Client client = new MainClient.Client('');
        //String result = client.OrderConfirmationResend('');
        //System.assert(result.startsWith('herpderp'));
    }
}