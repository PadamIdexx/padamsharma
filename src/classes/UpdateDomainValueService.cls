/* Class Name : UpdateDomainValueService
 * Description : Class to update Domain Values daily and Weekly
 * Created By : Raushan Anand
 * Created On : 10-Nov-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand              10-Nov-2015               Initial version.
 **************************************************************************************/
public with sharing class UpdateDomainValueService{
    
    /*DailyDomainValues.FREE_GOODS_element freeGoods = new DailyDomainValues.FREE_GOODS_element();
    DailyDomainValues.COST_CENTERS_element[] costCenters = new List<DailyDomainValues.COST_CENTERS_element>();
    DailyDomainValues.SALES_ACTIVITY_element salesActivity = new DailyDomainValues.SALES_ACTIVITY_element();
    DailyDomainValues.CODE_GROUPS_element[] codeGroups =new List<DailyDomainValues.CODE_GROUPS_element>();
    DailyDomainValues.SOFTWARE_VERSIONS_element[] softwareVersion = new List<DailyDomainValues.SOFTWARE_VERSIONS_element>();
    DailyDomainValues.SWAP_element[] SWAP = new List<DailyDomainValues.SWAP_element>();
    DailyDomainValues.FAIL_element[] FAIL = new List<DailyDomainValues.FAIL_element>();*/
    
    /* Method Name : callDailyDomainService
     * Description : This method fetches daily domain values
     * Return Type : void
     * Input Parameter :
     */
    //static AsyncWeeklyDomainValues.DT_WeeklyDomains_ResponseFuture res;
    @future(callout=true)
    public static void callDailyDomainService(){
        Map<String,Map<String,String>> returnRes =  new Map<String,Map<String,String>>();
        DailyDomainValues.DomainProcessing_OutBindingQSPort daily = new DailyDomainValues.DomainProcessing_OutBindingQSPort();
        DailyDomainValues.DT_DailyDomains_Response response = new DailyDomainValues.DT_DailyDomains_Response();
        //if(!Test.isRunningTest())
        	response = daily.DailyRead('EN');
        System.debug('@@@@RA'+response);
        if(response != null){
            if(response.FREE_GOODS != null){
                List<DailyDomainValues.REPS_element> agents = new List<DailyDomainValues.REPS_element>();
                agents = response.FREE_GOODS.REPS;
                if(agents != null){
                    Map<String,String> repMap = new Map<String,String>();
                    for(DailyDomainValues.REPS_element agent : agents){
                        if(String.isNotBlank(agent.REP) && String.isNotBlank(agent.NAME)){
                            if(String.isNotBlank(agent.DEF_COST_CENTER)){
								repMap.put(agent.REP,agent.NAME+'^'+agent.DEF_COST_CENTER); 
                            }	
                            else{
								repMap.put(agent.REP,agent.NAME);
                            }
                        }
                    }
                    returnRes.put('Free Goods Agent',repMap);
                }
                if(response.FREE_GOODS.COST_CENTERS != null){
                    Map<String,String> costMap = new Map<String,String>();
                    for(DailyDomainValues.COST_CENTERS_element costC : response.FREE_GOODS.COST_CENTERS){
                        if(costC.COST_CENTER != null && costC.COST_CENTER !=''){
                            costMap.put(costC.COST_CENTER,costC.DESCRIPTION);   
                        } 
                    }
                    returnRes.put('Free Goods Cost Center',costMap);
                }
                if(response.FREE_GOODS.REASONS != null){
                    Map<String,String> reasonMap = new Map<String,String>();
                    for(DailyDomainValues.REASONS_element reason : response.FREE_GOODS.REASONS){
                        if(reason.Reason != null && reason.Reason != ''){
                            reasonMap.put(reason.Reason,reason.DESCRIPTION);   
                        }  
                    }
                    returnRes.put('Free goods Reason',reasonMap);
                }
            }
            if(!returnRes.isEmpty()){
                List<String> recordDelimitedList = new List<String>();
                for(String field : returnRes.keyset()){
                    for(String key : returnRes.get(field).keyset()){
                        String str = field+'^'+key+'^'+returnRes.get(field).get(key);  
                        recordDelimitedList.add(str);
                    }
                }
                System.debug('@@@@RA'+returnRes);
                manageDomainRecords(recordDelimitedList);
            }
        }
    }
    
    /* Method Name : callWeeklyDomainService
     * Description : This method fetches weekly domain values
     * Return Type : void
     * Input Parameter :
     */
    @future(callout=true)
    public static void callWeeklyDomainService(){
        Map<String,Map<String,String>> returnRes =  new Map<String,Map<String,String>>();
        WeeklyDomainValues.DomainProcessing_OutBindingQSPort weekly = new WeeklyDomainValues.DomainProcessing_OutBindingQSPort();
        WeeklyDomainValues.DT_WeeklyDomains_Response response = new WeeklyDomainValues.DT_WeeklyDomains_Response();
        //if(!Test.isRunningTest())
        	response = weekly.WeeklyRead('EN');
        System.debug('@@@@RA'+response);
        if(response != null){
            if(response.SALES_INFO != null){
                for(WeeklyDomainValues.SALES_INFO_element salesInfo : response.SALES_INFO){
                    if(salesInfo.INCO_TERMS != null){
                        Map<String,String> valMap = new Map<String,String>();
                        for(WeeklyDomainValues.INCO_TERMS_element shipMethod : salesInfo.INCO_TERMS){
                            if(shipMethod.PART1 != null && shipMethod.PART1 != ''){
                                valMap.put(shipMethod.PART1,shipMethod.PART1_DESCRIPTION);
                            }
                        }
                        if(returnRes.containsKey('Ship Method')){
                            Map<String,String> tempVal = returnRes.get('Ship Method');
                            tempVal.putAll(valMap);
                            returnRes.put('Ship Method',tempVal);                            
                        }
                        else{
                            returnRes.put('Ship Method',valMap);
                        }
                            
                    }
                    if(salesInfo.ORDER_REASONS != null){
                        Map<String,String> valMap = new Map<String,String>();
                        for(WeeklyDomainValues.ORDER_REASONS_element orderReason : salesInfo.ORDER_REASONS){
                            if(orderReason.REASON != null && orderReason.REASON != ''){
                                valMap.put(orderReason.REASON,orderReason.DESCRIPTION);   
                            }
                        }
                        if(returnRes.containsKey('Order Reason')){
                            Map<String,String> tempVal = returnRes.get('Order Reason');
                            tempVal.putAll(valMap);
                            returnRes.put('Order Reason',tempVal);                            
                        }
                        else{
                            returnRes.put('Order Reason',valMap);
                        }
                            
                    }
                    if(salesInfo.DELIVERY_BLOCK != null){
                        Map<String,String> valMap = new Map<String,String>();
                        for(WeeklyDomainValues.DELIVERY_BLOCK_element deliveryBlock : salesInfo.DELIVERY_BLOCK){
                        	if(deliveryBlock.DELIVERY_BLOCK != null && deliveryBlock.DELIVERY_BLOCK != ''){
                        		valMap.put(deliveryBlock.DELIVERY_BLOCK,deliveryBlock.DESCRIPTION);   
                        	}
                        }
                        if(returnRes.containsKey('Delivery Block')){
                            Map<String,String> tempVal = returnRes.get('Delivery Block');
                            tempVal.putAll(valMap);
                            returnRes.put('Delivery Block',tempVal);                            
                        }
                        else{
                            returnRes.put('Delivery Block',valMap);
                        }
                            
                    }
                    if(salesInfo.PAYMENT_TERMS != null){
                        Map<String,String> valMap = new Map<String,String>();
                        for(WeeklyDomainValues.PAYMENT_TERMS_element paymentTerms : salesInfo.PAYMENT_TERMS){
                        	if(paymentTerms.PAYMENT_TERMS != null && paymentTerms.PAYMENT_TERMS != ''){
                        		valMap.put(paymentTerms.PAYMENT_TERMS,paymentTerms.DESCRIPTION);   
                        	}
                        }
                        if(returnRes.containsKey('Payment Terms')){
                            Map<String,String> tempVal = returnRes.get('Payment Terms');
                            tempVal.putAll(valMap);
                            returnRes.put('Payment Terms',tempVal);                            
                        }
                        else{
                            returnRes.put('Payment Terms',valMap);
                        }
                            
                    }
                    if(salesInfo.BILLING_BLOCK != null){
                        Map<String,String> valMap = new Map<String,String>();
                        for(WeeklyDomainValues.BILLING_BLOCK_element billBlock : salesInfo.BILLING_BLOCK){
                            valMap.put(billBlock.BILL_BLOCK,billBlock.DESCRIPTION);
                        }
                        if(returnRes.containsKey('Billing Block')){
                            Map<String,String> tempVal = returnRes.get('Billing Block');
                            tempVal.putAll(valMap);
                            returnRes.put('Billing Block',tempVal);                            
                        }
                        else{
                            returnRes.put('Billing Block',valMap);
                        }
                    }
                        
                }
                
            }
            if(!returnRes.isEmpty()){
                List<String> recordDelimitedList = new List<String>();
                for(String field : returnRes.keyset()){
                    for(String key : returnRes.get(field).keyset()){
                        String str = field+'^'+key+'^'+returnRes.get(field).get(key);
                        recordDelimitedList.add(str);
                    }
                }
                manageDomainRecords(recordDelimitedList);
            }
        }
    }
    /* Method Name : manageDomainRecords
     * Description : This method inserts values fetched
     * Return Type : void
     * Input Parameter :List<String>
     */
    public static void manageDomainRecords(List<String> recordToHandle){
        List<IDEXX_Domain_Values__c> listDomVal = new List<IDEXX_Domain_Values__c>();
        List<IDEXX_Domain_Values__c> existingDomVal = [SELECT ID,Name,Code__c,Description__c,Default_Cost_Center_For_Rep__c FROM IDEXX_Domain_Values__c WHERE Name != null];
        Set<String> matchPattern = new Set<String>();
        if(!existingDomVal.isEmpty()){
            
            for(IDEXX_Domain_Values__c dom : existingDomVal){
                String matcher;
                if(dom.Default_Cost_Center_For_Rep__c == null || dom.Default_Cost_Center_For_Rep__c == ''){
                    matcher = dom.Name+'^'+dom.Code__c+'^'+dom.Description__c;  
                }
                else{
                	matcher = dom.Name+'^'+dom.Code__c+'^'+dom.Description__c+'^'+dom.Default_Cost_Center_For_Rep__c;
                }
                matchPattern.add(matcher); 
                
            }
        }
        if(!recordToHandle.isEmpty()){
            for(String record : recordToHandle){
                if(!matchPattern.contains(record)){
                    IDEXX_Domain_Values__c domVal = new IDEXX_Domain_Values__c();
                    List<String> strArray = record.split('\\^');
                    if(strArray.size()==3){
                        domVal.Name = strArray[0];
                        domVal.Code__c = strArray[1];
                        domVal.Description__c = strArray[2];
                        domVal.Active__c = false;
                        listDomVal.add(domVal);
                    }
					else if(strArray.size()==4){
						domVal.Name = strArray[0];
                        domVal.Code__c = strArray[1];
                        domVal.Description__c = strArray[2];
						domVal.Default_Cost_Center_For_Rep__c = strArray[3];
                        domVal.Active__c = false;
                        listDomVal.add(domVal);
					}
                }
            }
            INSERT listDomVal;
        }
    }
}