@isTest(SeeAllData = False)
public with sharing class Test_TerritoryUtils{
   

   

    @future
    @testSetup
    private static void setupTerritories() {
        List<Territory2Type> terriType = [SELECT id, DeveloperName FROM Territory2Type LIMIT 1];
        System.debug('terriType='+terriType);
        //Create two territories, can be anything.
        //Assign accounts to one, both, or neither.
        //Assign users to one, both, or neither.
        list<Territory2Model> territory1 = [SELECT id, DeveloperName,name from Territory2Model Where State = 'Active'];

      /*  Territory2Model territory1 = new Territory2Model();
        territory1.DeveloperName='ModelName1'; // required field
        territory1.Name = 'Territory1'; // required field
        MixedDMLOps.ins(territory1); */

        Territory2 objTerr = new Territory2();
        objTerr.DeveloperName = 'TestTerritory';
        objTerr.Territory2ModelId=territory1[0].Id;
        objTerr.Name='TestTerritory';
        objTerr.Territory2TypeId=terriType[0].Id;
        MixedDMLOps.ins(objTerr);


    }

    private static User alignUser(User user,
                                  String trole,
                                  Territory2 terrModel) {
        System.debug('alignUser: user='+user+', trole='+trole+', terrModel='+terrModel);
        if (terrModel != null) {
            System.debug('here1');
            UserTerritory2Association u2a = new UserTerritory2Association();
            u2a.RoleInTerritory2 = trole;
            u2a.UserId = user.Id;
            u2a.Territory2Id = terrModel.Id;
            MixedDMLOps.ins(u2a);
        } else {
            System.debug('here2');
            UserTerritory2Association[] alignments =  [select Id FROM UserTerritory2Association WHERE RoleInTerritory2 = :trole and UserId = :user.Id];
            try {
                delete alignments;
            } catch (Exception e) {
                System.debug('Exception: '+e);
            }

        }

        return user;

    }

    private static Account alignAccount(Account account,
                                        Territory2 terrModel) {
        ObjectTerritory2Association o2a = new ObjectTerritory2Association();
        o2a.ObjectId = account.Id;
        o2a.Territory2Id = terrModel.Id;
        o2a.AssociationCause = 'Territory2Manual';
        MixedDMLOps.ins(o2a);
        return account;
    }

    /*
     * Test isAccountInUserTerritory for an aligned User with a restricted Territory Role.
     */


    public static User createTestUser() {
        User testUser = new User();
        testUser.firstname = 'Sample';
        testUser.lastname = 'Test';
        testUser.username = 'Sample-Test@sample.com.xy';
        //testUser.name = 'Sample Test';
        testUser.email = 'Sample-Test@sample.com';
        testUser.alias = 'stest';
        testUser.communitynickname = 'Sample-Test';
        testUser.timezonesidkey = 'America/New_York';
        testUser.localesidkey = 'en_US';
        testUser.emailencodingkey = 'ISO-8859-1';
        testUser.LanguageLocaleKey = 'en_US';
        testUser.currencyisocode = 'USD';
        Profile pr = [SELECT ID, Name FROM Profile WHERE Name = 'Inside Sales Rep-LPD'];
        UserRole urole = [SELECT Id,Name FROM UserRole WHERE Name = 'CAG Inside Sales Rep'];
        testUser.profileid = pr.id;
        testUser.UserRoleId = urole.id;
        Sobject[] obs = new Sobject[] {testUser};
        MixedDMLOps.ins(obs);
        return testUser;
    }

    private static User getTestUser() {
        return [SELECT Id, Name FROM User WHERE email='Sample-Test@sample.com'];
    }

    private static Territory2 getTerritory(String tName) {
        return [SELECT Id, name FROM Territory2 WHERE name=:tName];
    }

    private static Account getTestAccount() {
        return [SELECT Id, Name FROM Account WHERE name='Customer Testing Account'];
    }
}