public with sharing class SmartServiceDeviceInfoController {
    
    public ApexPages.StandardController stdCntrlr {get;set;}
    public List<DeviceStatusHolder> deviceStatusHolders  {get;set;}
    private String sapId;
    private AsyncSmartServiceDeviceInfo.getDevicesByPracticeResponseFuture asyncResponse;
    public List<Asset> assets {get;set;}
    
    public SmartServiceDeviceInfoController(ApexPages.StandardController controller) {
        stdCntrlr = controller;
        sapId = ApexPages.currentPage().getParameters().get('sapId');
        try {
        	if(!String.isBlank(sapId)) {
              assets = [select Id , Name, IsCompetitorProduct, Active_Formula__c, Asset_Category__c, Equipment_Description__c, SerialNumber from Asset where SAP_Customer_Number__c = :sapId];
            }    
        } catch (Exception ex) {
            ApexPages.addMessages(ex);
        }
    }
    
    public Continuation getAllDevices() {
        if(String.isBlank(sapId)) {
            return null;
        }
        Integer TIMEOUT_INT_SECS = 120;
        Continuation continuation = new Continuation(TIMEOUT_INT_SECS);
        continuation.continuationMethod = 'processResponse';
        try {
        	SmartServiceDeviceInfo.deviceByPracticeRequest deviceByPracticeRequest = new SmartServiceDeviceInfo.deviceByPracticeRequest();
        	deviceByPracticeRequest.sapId = sapId.replaceFirst( '^0+', '');
        
        	asyncResponse = new AsyncSmartServiceDeviceInfo.AsyncEnterpriseInformationWs().beginGetDevicesByPractice(continuation, deviceByPracticeRequest);
        } catch (Exception ex) {
            ApexPages.addMessages(ex);
        }    
        return continuation;
    }
    
    public Object  processResponse() {
        try { 
           SmartServiceDeviceInfo.deviceByPracticeResponse response = asyncResponse.getValue();
           if(null != response && null != response.practice && null != response.practice.deviceInstruments) {
                if(response.practice.deviceInstruments.size() > 0) {
                    deviceStatusHolders = new List<DeviceStatusHolder>();
                    for(SmartServiceDeviceInfo.deviceInstrument ssdeviceInstrument : response.practice.deviceInstruments) {
                        DeviceStatusHolder deviceStatusHolder = new DeviceStatusHolder();
                        String serialNumber = ssdeviceInstrument.serialNumber;
                        if(!String.isBlank(serialNumber)) {
                            String SERIALNUMBER_PREFIX = SmartServiceDeviceInfoController.retreiveProperty('SMART-SERIALNUMBER_PREFIX');
                            if(!String.isBlank(SERIALNUMBER_PREFIX)) {
                                String[] prefixes = SERIALNUMBER_PREFIX.split(',');
                                for(String prefix : prefixes) {
                                    serialNumber = serialNumber.removeStart(prefix);
                                } 
                            }
                            
                            deviceStatusHolder.serialNumber = serialNumber;
                        }   
                        DateTime lastPollingDateTime = ssdeviceInstrument.lastPollingDateTime;
                        Integer seconds = Integer.valueOf((DateTime.now().getTime() - lastPollingDateTime.getTime())/(1000*60));
                        
                        TimeZone tz = UserInfo.getTimeZone();
                        deviceStatusHolder.lastPollingDateTime = lastPollingDateTime.format('yyyy-MM-dd HH:mm:ss', tz.toString());
                        
                        String LAST_POLL_OK_TIME = SmartServiceDeviceInfoController.retreiveProperty('SMART-LAST_POLL_OK_TIME');
                        if(!String.isBlank(LAST_POLL_OK_TIME) && seconds < Integer.valueOf(LAST_POLL_OK_TIME)) {
                            deviceStatusHolder.status = 'Online';
                        } else {
                            deviceStatusHolder.status = 'Offline';
                        }
                        System.debug('assets' + assets);
                        for(Asset asset: assets) {
                            if(null != asset.SerialNumber && null != deviceStatusHolder.serialNumber && asset.SerialNumber == deviceStatusHolder.serialNumber) {
                                deviceStatusHolder.asset = asset;
                            }
                        }
                        
                        deviceStatusHolders.add(deviceStatusHolder);
                    }
                }
           }
        } catch (Exception ex) {
          ApexPages.addMessages(ex);  
        }
       return null;
    }
    
    public class DeviceStatusHolder {
        public Asset asset {get;set;}
        public String serialNumber {get;set;}
        public String status {get;set;}
        public String lastPollingDateTime {get;set;}
    }
    
    private static String retreiveProperty(String sIntegratioName) {
        IDEXX_Integration_Service_Properties__c prop = IDEXX_Integration_Service_Properties__c.getInstance(sIntegratioName);
        if(prop!=null)
            return prop.TextVal__c;
        else
            return null;
    }
}