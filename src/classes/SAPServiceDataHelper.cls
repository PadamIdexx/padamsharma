/*
 * Helper class for formatting data for SAP consumption.  
 * 
 * This class could be enhanced in the future to provide for specifically
 * formatted date or numeric strings depending upon what SAP needs are.
 */
public class SAPServiceDataHelper {

    
    /*
     * SAP specific constants used with order management services.
     * 
     * These are defined in a Custom Setting called SAP_INTEGRATION__c.  While these values typically 
     * do not change, defining them this way gives us flexibility in the event that they do.  
     * 
     * These strings for access the Custom Setting need to be defined somewhere.  This is the most logical
     * places since they are ultimately used in conjunction with the methods in this class.
     */ 
    public static final String POINTS_REDEMPTION_CONDITION  ='POINTS_REDEMPTION_CONDITION';
    public static final String VALUE_DISCOUNT_CONDITION = 'VALUE_DISCOUNT_CONDITION';
    public static final String NA_VALUE_DISCOUNT_CONDITION = 'NA_VALUE_DISCOUNT_CONDITION';    
    public static final String PERCENTAGE_DISCOUNT_CONDITION = 'PERCENTAGE_DISCOUNT_CONDITION';
    public static final String FREIGHT_CONDITION = 'FREIGHT_CONDITION';
    public static final String FREIGHT_ZF10 = 'FREIGHT_ZF10';
    public static final String FREIGHT_ZF11 = 'FREIGHT_ZF11';
    public static final String VAT_CONDITION =  'VAT_CONDITION';
	public static final String CIG_TEXTID = 'CIG_TEXTID';
	public static final String CUG_TEXTID = 'CUG_TEXTID';
	public static final String GENERAL_TEXTID = 'GENERAL_TEXTID';    
	public static final String HEADER_CONDITION_ID = 'HEADER_CONDITION_ID';    
    
    public static final String FOC_DEFAULT = 'FOC_DEFAULT';
    public static final String FOC_CODE_ROOT = 'FOC_';
    
    public static final String TRUE_INDICATOR = 'TRUE_INDICATOR';
    public static final String FALSE_INDICATOR = 'FALSE_INDICATOR';
    
    public static final String UPDATE_INDICATOR = 'UPDATE_INDICATOR';
    public static final String DELETE_INDICATOR = 'DELETE_INDICATOR';
    public static final String CREATE_INDICATOR = 'CREATE_INDICATOR';
    
    //All elements in the SAP interface are defined as nullable = false.  This means that no element will be
    //generated if the value is null.  However, the change scenario needs the ability to explicitaly supply 
    //an empty element.  Delete relies on sending empty elements to with a request.  We will need to set the 
    //value of these to the empty string to have them appear in requests. 
    public static final String EMPTY_ELEMENT = '';
    
    /*
     * Helper for looking up an Orders Integration coded value based on its custom setting name.  This 
     * is used for getting values appropriate for SAP.
     */ 
    public static String getSAPCode(String settingName) {
        String settingValue = null;
        
        if (String.isBlank(settingName)) {
            System.debug('settingName cannot be null.');
            return null;
        } else { 
            try {
            	settingValue = Orders_Integration__c.getInstance(settingName).CODE__c;
            } catch (Exception e) {
                System.debug('Unabled to retrieve Orders_Integration__c value: ' + settingName);
            }
        }
        
        return settingValue;
    }
    
    /*
     * Helper for looking up an Orders Integration coded value based on its custom setting name.  If not found, it returns
     * defaultValue.  This is used for getting values appropriate for SAP.
     */ 
    public static String getSAPCode(String settingName, String defaultValue) {
        String settingValue = getSAPCode(settingName);
        
        if (String.isBlank(settingValue)) {
            settingValue = defaultValue;
        }
        
        return settingValue;
        
    }
    
    /*
     * String of a specific length.  SAP enforces size restrictions
     * on data elements in web service calls.  
     */ 
    public static String getStringOfLength(String str, Integer length) {
        if (String.isBlank(str)) {
            return null;
        }
        
        if (length > str.length()) {
            return str;
        }
        
        return str.substring(0, length);
    }
    
    /*
     * Return a String representation for a date.
     */ 
    public static String getSAPDate(Date d) {
        if (null == d) {
            return null;
        }
        
        
        return String.valueOf(d);
    }

    /*
     * Return a Date representation for a date.
     */ 
    public static Date getDateForSAPDate(String d) {
        if (null == d) {
            return null;
        }
        
        
        return Date.valueOf(d);
    }
    
	/* 
	 * Returns a String representation of a decimal value with a specific
	 * number of decimal places.
	 */     
    public static String getSAPDecimal(Decimal num, Integer scale) {
        if (null == num) {
            return null;
        }
                
        return String.valueOf(num.setScale(scale));
    }
    
    /*
     * Return the decimal value for an SAP number
     */ 
    public static Decimal getDecimalForSAPNumber(String num, Integer scale) {
        if (null == num) {
            return 0.00;
        }
        
        return Decimal.valueOf(num).setScale(scale);
    }

  	/* 
	 * Returns a String representation of a Integer value.
	 */     
    public static String getSAPInteger(Integer num) {
        if (null == num) {
            return null;
        }
                
        return String.valueOf(num);
    }

    
    /*
     * Returns an SAP Indicator value for a boolean flag.
     */ 
    public static String getSAPIndicator(Boolean b) {
        if (null == b) {
            return null;
        }
                
        return b ? getSAPCode(SAPServiceDataHelper.TRUE_INDICATOR) : ' ';
    }
    
    /*
     * Returns a Boolean for an SAP indicator.  X -> true.  Everything else -> false
     */ 
    public static Boolean getBooleanForSAPIndicator(String indicator) {
        if (String.isBlank(indicator)) {
            return false;
        }
        else {
            String trueIndicator = getSAPCode(SAPServiceDataHelper.TRUE_INDICATOR);
            if (trueIndicator.equalsIgnoreCase(indicator)) {
                return true;
            } else if (indicator.equalsIgnoreCase('true')) {	//Some services return the word 'true' some use 'X'
                return true;
            } else {
                return false;
            }
        }
    }
    
    public Static String formatItemNumber(Integer iNum) {
        
        if (null != iNum) {        
            String iNumber = String.valueOf(iNum);
            iNumber = iNumber.leftPad(6, '0');
            iNumber = iNumber.right(6);
            
            return iNumber;
        } else {
            return '000000';
        }
        
    }
    
}