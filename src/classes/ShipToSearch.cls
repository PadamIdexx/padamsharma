/**
 * @author        Heather Kinney
 * @description   Ship To Search implementation.
 *                Used on Order Entry form for LPD NA and EU, when we have the situation where 
 *                we have a lot of buying groups that constantly change the ship-to's even as 
 *                one time off and are therefore not linked.
 *           
 *
 *  Modification Log:
 * -----------------------------------------------------------------------------------------------------------------------------
 *  Developer                       Date               Description
 * -----------------------------------------------------------------------------------------------------------------------------
 *  Heather Kinney                  11/23/2016         Initial creation.
 *
 */
public class ShipToSearch {
    
    protected ApexPages.StandardController controller {get;set;}

    public ShipToSearch(ApexPages.StandardController controller) {
        this.controller = controller;
    }
    
    public ShipToSearch() {}
    
	@RemoteAction
    public static List<Account> searchAccounts(String sapId, String division) {
        System.debug('sapID: ' + sapId );
        System.debug('division: ' + division);
        List<Account> accountList = Database.query('SELECT SAP_Number_without_Leading_Zeros__c, Name, ShippingStreet, ShippingCity, ShippingState, ' +
                                       'ShippingCountry, ShippingPostalCode, ShippingStateCode, ShippingCountryCode ' +
                                       ' FROM Account ' + 
                                       ' WHERE Id IN (SELECT Account__c FROM Partner_Function__c WHERE Partner_Function__c.Division__c = \'' + division + '\') ' + //12.7.2016 added division filter
                                       '   AND SAP_Number_without_Leading_Zeros__c LIKE \'' + sapId + '%\' LIMIT 20');
        System.debug('>>>>>> accountList = ' + accountList);                             
		return accountList;        
    }
    
}