@isTest
public class Test_ListSortHelper {

    public static List<SelectOption> createTestData(Integer num){
        List<SelectOption> testList = new List<SelectOption>();
        for(integer i=0;i<num;i++){
        	testList.add(new SelectOption(String.valueOf(num + i), String.valueOf(num + i)));
            }
        return testList;
    }
    
    public static testMethod void testSortByLabel(){

        List<SelectOption> myList = createTestData(10);
        
        test.startTest();
        ListSortHelper.selectOptionSortByLabel(myList);
        System.debug('sort label ' + myList.get(0));
        
        test.stopTest();
        
    } 
  
    public static testMethod void testSortByValue(){

        List<SelectOption> myList = createTestData(10);
        
        test.startTest();
        ListSortHelper.selectOptionSortByValue(myList);
        System.debug('sort value ' + myList.get(0));
        
        test.stopTest();
        
    }
    
}