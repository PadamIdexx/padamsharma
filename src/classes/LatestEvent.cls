public with sharing class LatestEvent {

    public Event event {get;set;}
    public string taskId;
    public Set<Id> accContactIds = new Set<Id>();        
    public Set<Id> cmpgnContactIds = new Set<Id>();
    ApexPages.Standardcontroller controller;
    public List<Campaign> relatedCampaigns  {get;set;}
    public Account thisAccount {get;set;}
    public String AccName {get;set;}
    private Event currentEventRecord;
    public Account currentAccountRecord;
    public Contact currentContactRecord;
    public ID accId;
    public Account acc {get;set;}
    
    public boolean isWater{get;set;}
    
    public LatestEvent (ApexPages.StandardController controller)
    {       
        Id contactId = ApexPages.currentPage().getParameters().get('who_id');
        Id accountId = ApexPages.currentPage().getParameters().get('what_id');
        isWater = false;
        String profileName=[Select Id,Name from Profile where Id=:userinfo.getProfileId()].Name;
        /*for(WaterProfiles__c setting : WaterProfiles__c.getAll().values()) 
{
if (setting.Profile_Name__c== profileName) {
isWater=true;
}

}*/   
       
        event = new event();
        this.event = (event)controller.getrecord();
        User u = [Select Id From User Where id = :UserInfo.getUserId()];
        event.OwnerId=u.id;
        this.controller = controller;
        if( !String.isBlank(ApexPages.currentPage().getParameters().get('what_id'))){
            System.debug('ACC' + ApexPages.currentPage().getParameters().get('what_id'));
            this.currentAccountRecord = [Select Id From Account Where Id = :ApexPages.currentPage().getParameters().get('what_id')];
            accId = currentAccountRecord.Id;
        }
        else{
            
            this.currentContactRecord = [SELECT Id, AccountID from Contact WHERE ID
                                         =:ApexPages.currentPage().getParameters().get('who_id')];
            accId = currentContactRecord.AccountID;
        }
        
        event.whatId= accId;
        if(!String.isBlank(ApexPages.currentPage().getParameters().get('who_id'))){
           // event.whoId = ApexPages.currentPage().getParameters().get('who_id');
        }
        
        thisAccount = new Account();
        for(Account acc : [select id,Name,(select Id,Name from Contacts )  From Account where id = :accId ])
        {    
            thisAccount  = acc;
            
            for(Contact cnt: acc.contacts){
                accContactIds.add(cnt.Id);
            }
        }
        
    }  
    
    public PageReference cancel(){
        PageReference page;
        page = new PageReference('/'+ event.WhatId );
        page.setRedirect(true);
        return page;
    } 
  
    
    public PageReference saveEvent() 
    {
        //insert event;  
        controller.save();    
        PageReference pr = new PageReference('/'+event.WhatId);
        pr.setRedirect(true);
        return pr;      
    }
    
    public PageReference SaveNewTask()
    {   
       PageReference pr; 
        controller.save();   
          Schema.DescribeSObjectResult describeResult = controller.getRecord().getSObjectType().getDescribe(); 
            pr = new PageReference('/apex/NewTaskWater?what_id='+ event.WhatId );
            pr.setRedirect(true); 
            return pr; 
    }
    
    public PageReference saveNewEvent()
    { 
        PageReference pr; 
        try
        {
            controller.save(); 
            Schema.DescribeSObjectResult describeResult = controller.getRecord().getSObjectType().getDescribe(); 
            pr = new PageReference('/apex/NewEventWater?what_id='+ event.WhatId );
            pr.setRedirect(true); 
            return pr; 
        }
        catch(Exception e)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage())); return null; 
        } 
    }
}