@isTest
public with sharing class Test_OrderChangeService {

     @isTest static void SalesOrderReadPositive() {
        Test.startTest();
        OrderChangeRequest.SalesOrderChangeRequest request = new OrderChangeRequest.SalesOrderChangeRequest();
        OrderChangeResponse.SalesOrderChangeResponse mockResponse = OrderTestDataFactory.createChangeResponse();
        
        Test.setMock(WebServiceMock.class, new Test_SOAPReusableMockResponse(mockResponse));
        
        OrderChangeService service = new OrderChangeService();
        
        service.changeOrder(request);       

        
        Test.stopTest();			
    }
    
    @isTest(SeeAllData=true) static void SalesOrderChangeUpdateHeader() {
        Test.startTest();
        User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
        system.runAs(adminUser){        
        SAPOrder order = OrderTestDataFactory.createSAPOrderForUpdateHeaderInfo();
		OrderChangeRequest.SalesOrderChangeRequest request = OrderChangeRequest.transform(order);
            
        OrderChangeResponse.SalesOrderChangeResponse mockResponse = OrderTestDataFactory.createChangeResponse();
        
        order = OrderChangeResponse.transform(order, mockResponse);
        }
        Test.stopTest();			
    }
    
    //3.10.2017 HLK added
    @isTest(SeeAllData=true) static void SalesOrderChangeUpdateHeaderValueDisc() {
        Test.startTest();
        User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
        system.runAs(adminUser){        
        SAPOrder order = OrderTestDataFactory.createSAPOrderForUpdateHeaderInfo();
        order.discountType = 'Value';
        ProfileDisplayWrapper userProfile = new ProfileDisplayWrapper();
        order.userProfile = userProfile;
        
		OrderChangeRequest.SalesOrderChangeRequest request = OrderChangeRequest.transform(order);
            
        OrderChangeResponse.SalesOrderChangeResponse mockResponse = OrderTestDataFactory.createChangeResponse();
        
        order = OrderChangeResponse.transform(order, mockResponse);
        }
        Test.stopTest();			
    }
    

    @isTest(SeeAllData=true) static void SalesOrderChangeDeleteHeader() {
        Test.startTest();
        User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
        system.runAs(adminUser){           
        SAPOrder order = OrderTestDataFactory.createSAPOrderForDeleteHeaderInfo();
		OrderChangeRequest.SalesOrderChangeRequest request = OrderChangeRequest.transform(order);
            
        OrderChangeResponse.SalesOrderChangeResponse mockResponse = OrderTestDataFactory.createChangeResponse();

        order = OrderChangeResponse.transform(order, mockResponse);
        }
        Test.stopTest();			
    }    

    @isTest(SeeAllData=true) static void SalesOrderChangeUpdateItem() {
        Test.startTest();
        User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
        system.runAs(adminUser){           
        SAPOrder order = OrderTestDataFactory.createSAPOrderForUpdateItem();
		OrderChangeRequest.SalesOrderChangeRequest request = OrderChangeRequest.transform(order);
            
        OrderChangeResponse.SalesOrderChangeResponse mockResponse = OrderTestDataFactory.createChangeResponse();
        
        order = OrderChangeResponse.transform(order, mockResponse);
        }
        Test.stopTest();			
    }

    
    @isTest(SeeAllData=true) static void SalesOrderChangeDeleteItem() {
        Test.startTest();
        User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
        system.runAs(adminUser){           
        SAPOrder order = OrderTestDataFactory.createSAPOrderForDeleteItem();
		OrderChangeRequest.SalesOrderChangeRequest request = OrderChangeRequest.transform(order);
            
        OrderChangeResponse.SalesOrderChangeResponse mockResponse = OrderTestDataFactory.createChangeResponse();
        
        order = OrderChangeResponse.transform(order, mockResponse);
        }
        Test.stopTest();			
    }    
    
    //3.10.2017 HLK added
    @isTest(SeeAllData=true) static void SalesOrderChangeDeleteBatchOfItem() {
        Test.startTest();
        User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
        system.runAs(adminUser){           
        SAPOrder order = OrderTestDataFactory.createSAPOrderForDeleteBatchOfItem();
		OrderChangeRequest.SalesOrderChangeRequest request = OrderChangeRequest.transform(order);
            
        OrderChangeResponse.SalesOrderChangeResponse mockResponse = OrderTestDataFactory.createChangeResponse();
        
        order = OrderChangeResponse.transform(order, mockResponse);
        }
        Test.stopTest();			
    } 
    

    @isTest(SeeAllData=true) static void SalesOrderChangeInsertItem() {
        Test.startTest();
        User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
        system.runAs(adminUser){           
        SAPOrder order = OrderTestDataFactory.createSAPOrderForInsertItem();
		OrderChangeRequest.SalesOrderChangeRequest request = OrderChangeRequest.transform(order);
            
        OrderChangeResponse.SalesOrderChangeResponse mockResponse = OrderTestDataFactory.createChangeResponse();
        
        order = OrderChangeResponse.transform(order, mockResponse);
        }
        Test.stopTest();			
    }        
    
    @isTest(SeeAllData=true) static void SalesOrderChangeUpdatePartner() {
        Test.startTest();
        User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
        system.runAs(adminUser){           
        SAPOrder order = OrderTestDataFactory.createSAPOrderForUpdatePartner();
		OrderChangeRequest.SalesOrderChangeRequest request = OrderChangeRequest.transform(order);
            
        OrderChangeResponse.SalesOrderChangeResponse mockResponse = OrderTestDataFactory.createChangeResponse();
        
        order = OrderChangeResponse.transform(order, mockResponse);
        }
        Test.stopTest();			
    }   

    @isTest(SeeAllData=true) static void SalesOrderChangeDeletePartner() {
        Test.startTest();
        User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
        system.runAs(adminUser){           
        SAPOrder order = OrderTestDataFactory.createSAPOrderForDeletePartner();
		OrderChangeRequest.SalesOrderChangeRequest request = OrderChangeRequest.transform(order);
            
        OrderChangeResponse.SalesOrderChangeResponse mockResponse = OrderTestDataFactory.createChangeResponse();
        
        order = OrderChangeResponse.transform(order, mockResponse);
        }
        Test.stopTest();			
    }       
}