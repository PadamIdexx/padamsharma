/*
 * Abstract base class for all SAP Order related service calls.
 * 
 * This class is intended only for defining a common interface.  
 * 
 */ 
public abstract class SAPOrderWrapper {
    public abstract SAPOrder execute(SAPOrder ord);   
}