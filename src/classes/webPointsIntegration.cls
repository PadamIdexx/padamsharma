/* Class Name   : webPointsIntegration
 * Description  : Controller class created to access Idexx Web Points.
                  webPointsIntegration page.
                  Is invoked when Web IDEXX Points button is accessed from Account.
 * Created By   : Srinivas Kaduluri
 * Created On   : 03-17-2017
 
 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                        Date                     Modification ID              Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Srinivas Kaduluri                03-17-2017                1000                     Initial version
*/

public class webPointsIntegration {
    
    public string accId;
    public string authEndpoint;
    public string endpoint;
    public string redirectEndpoint;
    public static account acc;
    public boolean isValid;
    
    public PageReference onLoad(){
        
        accId = ApexPages.currentPage().getParameters().get('Id');
        
        //used for test class
        if (Test.isRunningTest()) {
            List<Account> testAccList = [Select Id From Account Where Name='TestWebPointsIntegration' Limit 1];
            accId = testAccList[0].Id;
        }
        acc = [Select Id,name,SAP_Number_without_Leading_Zeros__c,EMEA_Country__c from account where Id = :accId limit 1];
        
        //Check if there is SAP id associated with this Account
        if(acc.SAP_Number_without_Leading_Zeros__c == null || acc.SAP_Number_without_Leading_Zeros__c == ''){
            
            //if no SAP id
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'No SAP Id associated with this Acocount!'));
            return null;
            
        }else{
            
            //checks for response
            getHttpResponse();
            
            //Redirect to Idexx Points Page on successful response
            if(isValid){    
                Pagereference pgRef = new Pagereference(redirectEndpoint);
                pgRef.setRedirect(true);
                return pgRef;
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Bad Response from the Server. Please refresh your page!'));
                return null;
            }        
        }
        
    }
    
    public void getHttpResponse(){
        //Get user, endpoint details from Custom Settings 
        String IDEXX_WEB_POINTS;
        WebPointsUser__c wp = WebPointsUser__c.getInstance(IDEXX_WEB_POINTS);
        
        string authEndpoint         = wp.AuthServerEndpoint__c;
        string endpoint             = wp.IdexxPointEndpoint__c;
        string username             = wp.Username__c;
        string password             = wp.Password__c;
        string clientkey            = wp.ClientKey__c;
        string clientsecret         = wp.ClientSecret__c;
        Blob headerValue            = Blob.valueOf(clientkey + ':' + clientsecret);
        String authorizationHeader  = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        String jsonRes;
        
        //Http callout and response
        Http h = new Http();
        HttpRequest req  = new HttpRequest();
        HttpResponse res = new HttpResponse();
        req.setEndpoint(authEndpoint);
      req.setBody('grant_type=password&username='+username+'&password='+password+'&scope=openid+profile');
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setHeader('Authorization',authorizationHeader);
        req.setHeader('Accept-Encoding', 'gzip,deflate');
      req.setTimeout(12000);
        req.setMethod('POST');
        if (!Test.isRunningTest()) {
            res = h.send(req);
        }
        if (Test.isRunningTest()) {
         res.setBody('{"access_token": "221e4c07-c95e-31ff-8545-0b351c078c0d","refresh_token": "5c1aa81a-808a-30c6-af5d-68a9d35f63c5","scope": "openid profile","id_token": "eyJ4NXQiOiJObUptT0dVeE16WmxZak0yWkRSaE5UWmxZVEExWXpkaFpUUmlPV0UwTldJMk0ySm1PVGMxWkEiLCJraWQiOiJkMGVjNTE0YTMyYjZmODhjMGFiZDEyYTI4NDA2OTliZGQzZGViYTlkIiwiYWxnIjoiUlMyNTYifQ.eyJhdF9oYXNoIjoiMFhXZjVUX3lHUDZTOGRCSHlvQnJLUSIsImNvdW50cnkiOiJVUyIsInN1YiI6ImMzOTM0N2FiLWRjNjAtNDJjMi05OTY3LWJiNzM4NDE3MGUxMyIsInJvbGUiOlsiSW50ZXJuYWxcL2lkZW50aXR5IiwiQ3VzdG9tZXJTdXBwb3J0IiwiSW50ZXJuYWxcL2V2ZXJ5b25lIl0sImlzcyI6Imh0dHBzOlwvXC9ucC1zc28uaWRleHguY29tOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJnaXZlbl9uYW1lIjoiSmVmZnJleSIsImFjciI6InVybjptYWNlOmluY29tbW9uOmlhcDpzaWx2ZXIiLCJzY2ltX2lkIjoiYzM5MzQ3YWItZGM2MC00MmMyLTk5NjctYmI3Mzg0MTcwZTEzIiwiYXVkIjpbInhMTjE4R2lXVXFRMkF2cm5xNlIzck44clhEQWEiXSwiYXpwIjoieExOMThHaVdVcVEyQXZybnE2UjNyTjhyWERBYSIsInByaW1hcnlfZW1haWwiOiJqZWZmcmV5LWZhdWxrbmVyQGlkZXh4LmNvbSIsImV4cCI6MTQ4OTY5NzU0MCwiZmFtaWx5X25hbWUiOiJGYXVsa25lciIsImlhdCI6MTQ4OTY5Mzk0MCwicHJlZmVycmVkX2xhbmd1YWdlIjoiZW4iLCJ1c2VybmFtZSI6ImplZmZyZXktZmF1bGtuZXJAaWRleHguY29tIn0.IoT1mCzrK6qPE9TVN-uattRwGmeTkz7shYb2QCnhWRtjLTkO05ANnS5m0XNwb15bJ9Rg54Pgc6gcf69tc9pNGP5_zFqHs1s1SZ0wc8YZwtr7y28jml0xctlYD29vsJbZ6YxS8nHwvJnPRYcT9CeQ8Jw8LvBIb353NuYDiWF5CPc","token_type": "Bearer","expires_in": 3600}');
         res.setStatusCode(200);
        }
        
        //check for Valid 200 response
        if(res.getStatusCode() == 200){
            jsonRes = res.getBody();
            if(String.isNotBlank(jsonRes)){
                
            //Uses ResponseParserWebPoints wrapper class to parse Json Response 
            ResponseParserWebPoints rparser = (ResponseParserWebPoints)JSON.deserialize(jsonRes, ResponseParserWebPoints.class);
            If(acc.EMEA_Country__c == FALSE){
            redirectEndpoint = 'https://'+endpoint+'/router.html?return_url=https%3A%2F%2F'+endpoint+'%2Fsmallanimal%2Fidexxpoints.html%3FsapId%3D'+acc.SAP_Number_without_Leading_Zeros__c+'#access_token='+rparser.access_token+'&id_token='+rparser.id_token+'&state=csnull&token_type=Bearer&expires_in='+rparser.expires_in+'&session_state=csnull';
            }else {
            redirectEndpoint = 'https://'+endpoint+'/router.html?return_url=https%3A%2F%2F'+endpoint+'%2F360%2Findex.html%3FsapId%3D'+acc.SAP_Number_without_Leading_Zeros__c+'#access_token='+rparser.access_token+'&id_token='+rparser.id_token+'&state=csnull&token_type=Bearer&expires_in=3600&session_state=csnull';
                
            }
            system.debug('****Endpoint***'+redirectEndpoint);
            isValid = true;    
            
            }
        }else{
            isValid = false;
        }
    }
    
    public webPointsIntegration(ApexPages.StandardController controller) {
        isValid = false;   
    }
        

}