/*
 * OrderChangeService
 * 
 * This class contains the Web Service Callout functionality for the SAP Order Change 
 * operation in SalesProcessing_OutService as defined in Beacon OSB under
 * SAPOrderManagment.
 */ 
public class OrderChangeService {

        public String endpoint_x = '';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x = 120000;
        private String[] ns_map_type_info = new String[]{OrderReadRequest.NAMESPACE, 
            								OrderReadRequest.SF_NAMESPACE, 
            								OrderReadRequest.XMLTYPE_NAMESPACE, 
            								OrderReadRequest.SF_XMLTYPE_NAMESPACE};
       

		public OrderChangeService(){
            //Brute force approach to getting runtime classname to use for looking up the endpoint
            String thisName = String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));            
            String endPointURL = '';
            if (!Test.isRunningTest()) { //3.8.2017
                endPointURL = WebServiceHelper.getServiceEndpoints(thisName);   
            }
            endpoint_x = endPointURL!=null ? endPointURL : '';
           
            //set headers
            inputHttpHeaders_x = new Map<String, String>();
            
            String sessionId = '';
            if (!Test.isRunningTest()) { //3.8.2017
                sessionId = WebServiceHelper.getIntegrationSessionId();
            }
            inputHttpHeaders_x.put('sessionId', sessionId);
        }
    
    
        public OrderChangeResponse.SalesOrderChangeResponse ChangeOrder(OrderChangeRequest.SalesOrderChangeRequest changeRequest) {
            OrderChangeRequest.SalesOrderChangeRequest request_x = new OrderChangeRequest.SalesOrderChangeRequest();
            request_x= changeRequest;
            OrderChangeResponse.SalesOrderChangeResponse response_x;
            Map<String, OrderChangeResponse.SalesOrderChangeResponse> response_map_x = new Map<String, OrderChangeResponse.SalesOrderChangeResponse>();
            response_map_x.put('response_x', response_x); 
            if (!Test.isRunningTest()) { //3.8.2017
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        OrderChangeRequest.SOAPACTION,
                        OrderChangeRequest.NAMESPACE,
                        OrderChangeRequest.PAYLOAD_CLASSNAME,
                        OrderChangeResponse.NAMESPACE,
                        OrderChangeResponse.PAYLOAD_CLASSNAME,
                        OrderChangeResponse.SalesOrderChangeResponse.class.getName()}
                );
            }
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    
       
    
}