/**************************************************************************************
 * Create By:   Deloitte Consulting LLP.
 * CreateDate:  21-Sep-2015
 * Description: This is controller class for CampaignAccounts vf page. It displays a list of Campaigns as a dropdown and on
 *              selection of a Campaign, displays its associated Accounts in the section below. Both the Campaigns and 
 *              associated accounts are filtered as per the logged in ISR. 
 *                 
 *  
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                 Created/Modified Date         Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Vishal Negandhi           21-Sep-2015                   Initial version.
    * Vishal Negandhi           15-Dec-2015                   Modified logic to fetch data based on territory assignment instead of Account team member
    * Heather Kinney            03-May-2017                   US28777 - Filter Campaign Member Status listed in Campaign Account VF page. 
    *                                                                   Only display those Statuses associated with the given Campaign (CampaignMemberStatus), not the entire list.
 **************************************************************************************/
public With Sharing class CampaignAccountsController extends PaginationController{
    public String SelectedCampaign  {get;set;} // to store the selected Campaign
    public List<Account> AssociatedAccounts {get;set;}
    static Set<Id> SetAccountIds;
    static Set<Id> ContactIds;
    public Map<Id, CampaignMember> accountCampaignMemberMap {get;set;}
    public static Map<Id, CampaignMemberStatus> accountCampaignMemberStatusMap {get;set;}
    public static Map<Id, CampaignMemberStatusUpdateModel> campaignMemberStatusUpdateModelMap {get;set;}
    public static String CalledFromPage {get;set;}
    
    public CampaignAccountsController(){
        // initilisation of variables
        SelectedCampaign = '';
        AssociatedAccounts = new List<Account>();
        setAccountIds = new Set<Id>();
        accountCampaignMemberMap = new Map<Id, CampaignMember>();
        accountCampaignMemberStatusMap = new Map<Id, CampaignMemberStatus>();
        ContactIds = new Set<Id>();
        CalledFromPage = '';
    }
        
    //US28777 - HKinney 5.3.2017 - added this function, as this was the only reasonable way we could
    //          override the existing CampaignMembers's .Status Picklist (which displays ALL Statuses from ALL Campaigns, currently).
    @RemoteAction
    public static List<SelectOption> getStatusesForCurrentCampaignMember(String campaignId) {
        List<SelectOption> selOptCampaignMemberStatus = new List<SelectOption>();
        for (CampaignMemberStatus record: [SELECT CampaignId, Label FROM CampaignMemberStatus 
                WHERE CampaignId = :campaignId ORDER BY SortOrder ASC]) {
            selOptCampaignMemberStatus.add(new SelectOption(record.Label, record.Label));               
        }
        return selOptCampaignMemberStatus;
    }
    
    //US28777 - HKinney 5.3.2017 - added this function to store the changed Campaign Member Status (fired from an onchange wired up to the
    //          Status dropdown). This value is stored so it can be included later with the bulk Save.
    @RemoteAction
    public static Map<Id, CampaignMemberStatusUpdateModel> setCampaignStatusForCurrentCampaignMember(String changedCampaignStatusesString, String campaignMemberId, String newStatusValue) {
        //System.debug('in savePendingChanges...changedCampaignStatusesString = ' + changedCampaignStatusesString);
        //System.debug('in savePendingChanges...campaignMemberId = ' + campaignMemberId);
        //System.debug('in savePendingChanges...newStatusValue = ' + newStatusValue);
        Map<Id, CampaignMemberStatusUpdateModel> cmsUpdateMap = reconstituteCampaignMemberStatusMap(changedCampaignStatusesString);
        if (cmsUpdateMap != null) {
            campaignMemberStatusUpdateModelMap = cmsUpdateMap;    
        }
        if (campaignMemberStatusUpdateModelMap == null) { //we are coming in for the first time...
            campaignMemberStatusUpdateModelMap = new Map<Id, CampaignMemberStatusUpdateModel>();    
        } 
        //this populates the map with the new status for the given campaign member id, in addition to the ones
        //reconstituted from above...
        if ((campaignMemberId != null) && (newStatusValue != null)) {
            CampaignMemberStatusUpdateModel updatedStatusModel = new CampaignMemberStatusUpdateModel();
            updatedStatusModel.campaignMemberId = campaignMemberId;
            updatedStatusModel.newStatusValue = newStatusValue; 
            campaignMemberStatusUpdateModelMap.put(campaignMemberId, updatedStatusModel);
        }
        //System.debug('>>>> ***** campaignMemberStatusUpdateModelMap = ' + campaignMemberStatusUpdateModelMap);
        return campaignMemberStatusUpdateModelMap;
    }
    
    private static Map<Id, CampaignMemberStatusUpdateModel> reconstituteCampaignMemberStatusMap(String changedCampaignStatusesString) {
        System.debug('in reconstituteCampaignMemberStatusMap; changedCampaignStatusesString = ' + changedCampaignStatusesString);
        Map<Id, CampaignMemberStatusUpdateModel> cmsUpdateModel = new Map<Id, CampaignMemberStatusUpdateModel>(); 
        
        if (changedCampaignStatusesString.length() > 0) {
            if (changedCampaignStatusesString.contains('__')) {
                System.debug('1. in changedCampaignStatusesString.contains '); 
                //multiple ley value pairs...
                String[] arrCampStatuses = changedCampaignStatusesString.split('__');
                for (integer i=0; i<arrCampStatuses.size(); i++) {
                    String keyValPairInArr = arrCampStatuses[i];
                    if (keyValPairInArr.contains('_')) {
                        String[] arrKeyValPairInArr = keyValPairInArr.split('_');
                        String campMemberIdInArr = arrKeyValPairInArr[0];
                        String newStatusValueInArr = arrKeyValPairInArr[1];
                        CampaignMemberStatusUpdateModel updatedStatusModelInArr = new CampaignMemberStatusUpdateModel();
                        updatedStatusModelInArr.campaignMemberId = campMemberIdInArr;
                        updatedStatusModelInArr.newStatusValue = newStatusValueInArr;
                        if (campaignMemberStatusUpdateModelMap == null) {
                            campaignMemberStatusUpdateModelMap = new Map<Id, CampaignMemberStatusUpdateModel>();   
                        }
                        if (!campaignMemberStatusUpdateModelMap.containsKey(campMemberIdInArr)) {
                            campaignMemberStatusUpdateModelMap.put(campMemberIdInArr, updatedStatusModelInArr);
                        }
                    } //the otherwise should never happen as distinct key value pairs will be seprated by a '_'
                }
            } else {
                //not an inital outer string array - meaning we only have one key-val pair...
                if (changedCampaignStatusesString.contains('_')) {
                    String[] arrKeyValPairNotInArr = changedCampaignStatusesString.split('_');
                    String campMemberIdNotInArr = arrKeyValPairNotInArr[0];
                    String newStatusValueNotInArr = arrKeyValPairNotInArr[1];
                    CampaignMemberStatusUpdateModel updatedStatusModelNotInArr = new CampaignMemberStatusUpdateModel();
                    updatedStatusModelNotInArr.campaignMemberId = campMemberIdNotInArr;
                    updatedStatusModelNotInArr.newStatusValue = newStatusValueNotInArr;
                    if (campaignMemberStatusUpdateModelMap == null) {
                        campaignMemberStatusUpdateModelMap = new Map<Id, CampaignMemberStatusUpdateModel>();   
                    }
                    if (!campaignMemberStatusUpdateModelMap.containsKey(campMemberIdNotInArr)) {
                        campaignMemberStatusUpdateModelMap.put(campMemberIdNotInArr, updatedStatusModelNotInArr);
                    }
                }
            } 
        }
        cmsUpdateModel = campaignMemberStatusUpdateModelMap;
        return cmsUpdateModel;
    }
    
    
    // returns a list of Campaigns where the logged in user (ISR) is a Member of (via Contacts)
    public List<SelectOption> getCampaigns(){
        List<SelectOption> Options = new List<SelectOption>();
        Options.add(new SelectOption('', '-None-'));
        ContactIds = new Set<Id>();
        SetAccountIds= new Set<Id>(); 
        Set<Id> territoryIds = new Set<Id>();
       
        // fetch all Territories where the current user is an ISR
        // Lakshmi Removed the ISR from the where clause -- orginal quesry is :[select Territory2Id from UserTerritory2Association where RoleInTerritory2 = 'ISR' AND UserId = :UserInfo.getUserId()]
        for (UserTerritory2Association uta : [select Territory2Id from UserTerritory2Association where  UserId = :UserInfo.getUserId()]){
            territoryIds.add(uta.Territory2Id);
        }
        //system.debug('..... territory ids ' + territoryIds);
        // fetch all Accounts that belong to the territories stored in the set
        for (ObjectTerritory2Association ota : [Select ObjectId From ObjectTerritory2Association WHERE Territory2Id IN :territoryIds AND Territory2.Territory2Model.State='Active' limit 5000]){
            setAccountIds.add(ota.ObjectId);
        }
        // fetch the contact Ids from Account IDS stored above
        for (Account acc : [Select Primary_Contact__c From Account WHERE ID IN :setAccountIds AND Primary_Contact__c != NULL]){
            ContactIds.add(acc.Primary_Contact__c);
        }
        
        if (!SetAccountIds.isEmpty()){
            Set<Id> CampaignId = new Set<Id>();
            if(!ContactIds.isEmpty()){
                for(CampaignMember cm : [Select CampaignId, Campaign.Name From CampaignMember
                                         WHERE ContactId IN :ContactIds AND Campaign.IsActive = true]){
                    if(!CampaignId.contains(cm.CampaignId))
                        Options.add(new SelectOption(cm.CampaignId, cm.Campaign.Name));
                    CampaignId.add(cm.CampaignId);                         
                }
            }
        }
        return Options;
    }
    
    //Fetches the selected Campaign Details.
    public void fetchCampaignDetails(){
        AssociatedAccounts = new List<Account>();
        
        //lakshmi
        Set<Id> LabelId = new Set<Id>();
        accountCampaignMemberStatusMap = new Map<Id, CampaignMemberStatus>();
        //System.debug('SelectedCampaign = ' + SelectedCampaign);
        for(CampaignMemberStatus cms : [Select CampaignId, Label From CampaignMemberStatus 
                                 WHERE CampaignId = :SelectedCampaign]){
            //SetCMAccountId.add(cm.Contact.AccountId);
            LabelId.add(cms.CampaignId);
            accountCampaignMemberStatusMap.put(cms.CampaignId, cms);                        
        }

        Set<Id> SetCMAccountId = new Set<Id>();
        for (CampaignMember cm : [Select Contact.AccountId, Status From CampaignMember 
                                 WHERE CampaignId = :SelectedCampaign AND ContactId IN :ContactIds AND Status NOT IN ('Declined', 'Fulfilled')]){
            SetCMAccountId.add(cm.Contact.AccountId);                        
            accountCampaignMemberMap.put(cm.Contact.AccountId, cm);
        }
        
        if (!SetCMAccountId.isEmpty()) {   
            for(Account account : [Select Name, Type, Phone, Primary_Contact__r.Name, Primary_Contact__c, OwnerId
                                   FROM Account 
                                   WHERE Id IN :SetCMAccountId AND ID IN :SetAccountIds LIMIT 999]){
                AssociatedAccounts.add(account); 
            }
        }
        
        if (CalledFromPage == 'true'){
            resetPagination();
            setPagination();
        } else{
            setPagination();
        }
        
    }
    
    // resets the pagination variables
    private void resetPagination(){
        CurrentPage = 0;
        PageSize = 20;
        PageNumber = 1;
        ShowPrevious = false;   
    }
    
    // sets the pagination variables every time records are fetched
    private void setPagination(){
        TotalRecords = AssociatedAccounts.size();
        if(TotalRecords > PageSize)
            ShowNext = true;
        else
            ShowNext = false;
            
        if(TotalRecords < 1)
            PageNumber = 0;    
            
        CurrentPageInfo = 'Showing Page ' + PageNumber +   ' of ' + (Math.mod(TotalRecords, PageSize) == 0 ? TotalRecords/PageSize : (TotalRecords/PageSize) + 1);
    }
    
    //US28777 - HKinney 5.3.2017 - added this new function to save all pending changes; as we needed to set up a new way of obtaining a filtered list 
    //          of Statuses pertaining to just the given Campaign. Due to this, we needed to implement this logic in the following function block:
    @RemoteAction
    public static void savePendingChanges(String changedCampaignStatusesString) {
        System.debug('in savePendingChanges...');
        //changedCampaignStatusesString = changedCampaignStatusesString.replace('&#124;', '|');
        System.debug('changedCampaignStatusesString after replace... = ' + changedCampaignStatusesString);
        Map<Id, CampaignMemberStatusUpdateModel> cmsUpdateMap = reconstituteCampaignMemberStatusMap(changedCampaignStatusesString);
        if (cmsUpdateMap != null) {
            campaignMemberStatusUpdateModelMap = cmsUpdateMap;    
        }
        System.debug('@@@@@ >>> campaignMemberStatusUpdateModelMap = ' + campaignMemberStatusUpdateModelMap);
        

        if (!campaignMemberStatusUpdateModelMap.isEmpty()) {
            //System.debug('campaignMemberStatusUpdateModelMap = ' + campaignMemberStatusUpdateModelMap);
            //System.debug('campaignMemberStatusUpdateModelMap.keySet = ' + campaignMemberStatusUpdateModelMap.keySet());
            List<CampaignMember> reconstitutedCampaignMembersList = [SELECT Id, Status FROM CampaignMember  WHERE Id IN :campaignMemberStatusUpdateModelMap.keySet() ];
            System.debug('reconstitutedCampaignMembersList = ' + reconstitutedCampaignMembersList);
            List<CampaignMember> campaignMembersToUpdateList = null;
            if (reconstitutedCampaignMembersList != null) {
                campaignMembersToUpdateList = new List<CampaignMember>();
                for (CampaignMember cm: reconstitutedCampaignMembersList) {
                    if (campaignMemberStatusUpdateModelMap.containsKey(cm.Id)) {
                        CampaignMemberStatusUpdateModel cmsUpdateModel = campaignMemberStatusUpdateModelMap.get(cm.Id);
                        cm.Status = cmsUpdateModel.newStatusValue;
                        campaignMembersToUpdateList.add(cm);
                    }    
                } //end for...      
                if (campaignMembersToUpdateList != null) {
                    System.debug('TO be bulk updated via SOQL: campaignMembersToUpdateList = ' + campaignMembersToUpdateList);
                    UPDATE campaignMembersToUpdateList;
                }
            } //end if (campaignMembersToUpdateList != null)...
        }
    }
    
    // performs an update on Campaign members when inline changes are performed on the page
    public void saveInlineChanges(){
        try{    
            if(!accountCampaignMemberMap.isEmpty() && Schema.sObjectType.CampaignMember.isUpdateable()){   
                update accountCampaignMemberMap.values();
                CalledFromPage = '';
                fetchCampaignDetails();
            }
        } catch(DMLException e){
            // some exception while updating campaign members
        }
    }
    
    public class CampaignMemberStatusUpdateModel {
        public String campaignMemberId;
        public String newStatusValue; 
    }
}