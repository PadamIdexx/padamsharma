/*
 * OrderDeleteRequest
 * 
 * This class represents the shape the Request for the SAP Sales Order Delete 
 * operation in SalesProcessing_OutService as defined in Beacon OSB under
 * SAPOrderManagment.
 */

public with sharing class OrderDeleteRequest {
    //Web Service call out related constants.
    //
    // SOAP Action
    public final static String SOAPACTION = 'http://sap.com/xi/WebService/soap1.1';
    
    // XML Namespace for this object
    // Salesforce representation of Namespace
    public final static String NAMESPACE = 'http://idexx.com/sappo/sales/salesforce';
    public final static String SF_NAMESPACE = 'idexxComSappoSalesSalesforce';
    
    // XML Namespace of types    
    // Salesforce representation of Namespace   
    public final static String XMLTYPE_NAMESPACE = 'http://idexx.com/sappo/global/types';
	public final static String SF_XMLTYPE_NAMESPACE = 'idexxComSappoGlobalTypes';
     
	// Name of Class that carries the payload.  There is no generic way to get this at this 
	// from the obect at runtime at this time.    
    public final static String PAYLOAD_CLASSNAME = 'SalesOrderDeleteRequest';
    

    public class SalesOrderDeleteRequest{
        public String DocumentNumber;
        public String Language;
        
        private String[] DocumentNumber_type_info = new String[]{'DocumentNumber',NAMESPACE,null,'1','1','false'};            
        private String[] Language_type_info = new String[]{'Language',NAMESPACE,null,'1','1','false'};            
          
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'DocumentNumber','Language'};
        
    }
    
    /*
     * Transformer - Convert an SAPOrder into a request object.
     */ 
    public static OrderDeleteRequest.SalesOrderDeleteRequest transform(SAPOrder order) {
        OrderDeleteRequest.SalesOrderDeleteRequest request = new OrderDeleteRequest.SalesOrderDeleteRequest();

        if (null != order) {        
			request.DocumentNumber = order.documentNumber;
			request.Language = order.Language;
        }
        
        return request;
    }    
    
}