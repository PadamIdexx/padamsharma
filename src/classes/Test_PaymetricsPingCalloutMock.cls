@isTest
global class Test_PaymetricsPingCalloutMock implements WebServiceMock {
   global void doInvoke(Object stub,Object request,Map<String, Object> response,String endpoint,String soapAction,String requestName,String responseNS,String responseName,String responseType) {
        paymetricXipaysoap30Message.ITransactionHeader iHeader = new paymetricXipaysoap30Message.ITransactionHeader();
        iHeader.StatusCode=100;
        primesysXipaysoapMessage.PingResponse_element sampleResponse = new primesysXipaysoapMessage.PingResponse_element();
        sampleResponse.PingResult = iHeader;
        response.put('response_x', sampleResponse ); 
        System.debug('########'+response.get('response_x'));
    }
}