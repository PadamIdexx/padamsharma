@isTest public class Test_SAPOrder {
    @isTest static void testSAPOrderPositive() {
        Test.startTest();
        SAPOrder order = OrderTestDataFactory.createSAPOrder();
        SAPOrder.Return_x sapOrderMessage = new SAPOrder.Return_x();
        sapOrderMessage.MessageType = 'x';
        sapOrderMessage.MessageID = 'x';
        sapOrderMessage.MessageNumber = 'x';
        sapOrderMessage.updateFlag = 'x';
        sapOrderMessage.Message = 'x';
        List<SAPOrder.Return_x> sapOrderMessages = new List<SAPOrder.Return_x>();
        sapOrderMessages.add(sapOrderMessage);
        order.returns = sapOrderMessages;
        
        Test.stopTest();
    }
}