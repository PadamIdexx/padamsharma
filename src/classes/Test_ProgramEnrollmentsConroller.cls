/* Class Name   : Test_ProgramEnrollmentsController
 * Description  : Test Class with unit test scenarios to cover the ProgramEnrollmentController class
 * Created By   : Anudeep Gopagoni
 * Created On   : 12/07/2015
 
* Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                    Date                   Modification ID            Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Anudeep Gopagoni           12/07/2015                  1000                   Initial version
   Aditya Sangawar            29/01/2016                  1000                   Added PositiveTestCasePE2
   Hemanth Kumar V			  02/02/2016									     Fixed Test Failures                                        
*/
@isTest(SeeAllData = false)

public with sharing class Test_ProgramEnrollmentsConroller{
    static testMethod void Test_InlineProgramEnrollmentsPositive() {
    //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = CreateTestClassData.reusableUser('System Administrator','Test');
        system.runAs(currentUser){
            // Create common test accounts
            Account acc = CreateTestClassData.createCustomerAccount();
            acc.SAP_Customer_Number__c = '0000001003';
            update acc;
            Test.startTest();
            Test.setCurrentPage(Page.ProgramEnrollments);
            System.currentPageReference().getParameters().put('Id',acc.Id);
            ApexPages.StandardController std = new ApexPages.StandardController(acc);
            ProgramEnrollmentsController pCon = new ProgramEnrollmentsController(std);
            pCon.fetchProgramEnrollments(); 
            BRF_ENROLL__x testc = new BRF_ENROLL__x(PROG_ID__c = '00004', DisplayUrl='TEST',PROG_NAME__c='PRACTICE DEVELOPER',PROG_DESC__c='CLINIC PURCHASES FROM PROGRAM CATEGORIES AND BASED ON MATRIX, POINTS ARE AWARDED. INCREASE CUSTOMER RETNETION/SATISFACTION, PRODUCT CROSS-SELLING, NEW PRODUCT & SERVICE  AWARENESS', ENROLL_DATE__c = '20070101', ACTIVE_FLAG__c = 'Y',TERM_DATE__c='20080101');
            List<ProgramEnrollmentsController.DisplayWrapper> WrapvarList = new List<ProgramEnrollmentsController.DisplayWrapper>();
            pCon.enrollmentList = new List<BRF_ENROLL__x>();
            pCon.enrollmentList.add(testc);
            pCon.storeSAPFromQuery = '0000001003'; 
            pCon.fetchProgramEnrollments(); 
            
            Test.stopTest();
        }
    
    }
    // Give wrong SAP_Customer_Number__c

   static testMethod void NegativeTestScenario1() {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = CreateTestClassData.reusableUser('System Administrator','Test');
        system.runAs(currentUser){
            // Create common test accounts
            Account acc = CreateTestClassData.createCustomerAccount();
            acc.SAP_Customer_Number__c = '000001003';
            update acc;
            Test.startTest();
            Test.setCurrentPage(Page.ProgramEnrollments);
            System.currentPageReference().getParameters().put('Id',acc.Id);
            ApexPages.StandardController std = new ApexPages.StandardController(acc);
            ProgramEnrollmentsController pCon = new ProgramEnrollmentsController(std);
            pCon.fetchProgramEnrollments(); 
            BRF_ENROLL__x testc = new BRF_ENROLL__x(PROG_ID__c = '00004', DisplayUrl='TEST',PROG_NAME__c='PRACTICE DEVELOPER',PROG_DESC__c='CLINIC PURCHASES FROM PROGRAM CATEGORIES AND BASED ON MATRIX, POINTS ARE AWARDED. INCREASE CUSTOMER RETNETION/SATISFACTION, PRODUCT CROSS-SELLING, NEW PRODUCT & SERVICE  AWARENESS', ENROLL_DATE__c = '20070101', ACTIVE_FLAG__c = 'Y');
            ProgramEnrollmentsController.DisplayWrapper Wrapvar = new ProgramEnrollmentsController.DisplayWrapper();
            //List<DisplayWrapper> enrollmentList = new List<DisplayWrapper>();
            pCon.enrollmentList = new List<BRF_ENROLL__x>();
            pCon.enrollmentList.add(testc);
            
            Test.stopTest();
        }
    }  
    
    
 }