/* Class Name  : Test_InboundSearch_phonelookup
* Description : Test class with unit tests that covers the InboundSearch_phonelookup class.
* Created By  : Anudeep Gopagoni
* Created On  : 02-20-2016
*
* Modification Log:  
* --------------------------------------------------------------------------------------------------------------------------------------
* Developer              Date                   Modification ID       Description 
* ---------------------------------------------------------------------------------------------------------------------------------------
* Anudeep Gopagoni       02-20-2016             n/a                   Initial version
*                                          
*/
@isTest

public class Test_InboundSearch_phonelookup {
    
    static testMethod void performAllPhoneNumberSearchesFromPhoneLookup() {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = getCurrentUser();
        system.runAs(currentUser){
            PageReference pageRef = Page.InboundSearch_phonelookup;
            Test.setCurrentPage(pageRef);
            
            Account acc = createCustomerAccount();          
            Contact cont = new Contact();
            cont.LastName = 'testContact1';
            cont.AccountId = acc.Id;
            insert cont;
            
            acc.Primary_Contact__c = cont.Id;
            acc.Phone = '5463785678';
            update acc;
            test.startTest();
            ApexPages.StandardController std = new ApexPages.StandardController(acc);
            ApexPages.currentPage().getParameters().put('SF_SearchValue',acc.Phone);
            
            InboundSearch_phonelookup controller = new InboundSearch_phonelookup(std);
            controller.inboundDID = '4136522270';
            controller.phoneNumberConverted = '4136522270';
            controller.SF_SearchValue = '5463785678';
            controller.performAllPhoneNumberSearchesFromPhoneLookup();
            test.stopTest(); 
        }
    }
    
    static testMethod void performexceptionhandling() {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = getCurrentUser();
        system.runAs(currentUser){
            PageReference pageRef = Page.InboundSearch_phonelookup;
            Test.setCurrentPage(pageRef);
            
            Account acc = createCustomerAccount();          
            Contact cont = new Contact();
            cont.LastName = 'testContact1';
            cont.AccountId = acc.Id;
            insert cont;
            
            acc.Primary_Contact__c = cont.Id;
            acc.Phone = '5463785678';
            update acc;
            test.startTest();
            ApexPages.StandardController std = new ApexPages.StandardController(acc);
            ApexPages.currentPage().getParameters().put('test',acc.Phone);
            
            InboundSearch_phonelookup controller = new InboundSearch_phonelookup(std);
            controller.inboundDID = '4136522270';
            controller.phoneNumberConverted = '4136522270';
            controller.SF_SearchValue = '5463785678';
            controller.performAllPhoneNumberSearchesFromPhoneLookup();
            test.stopTest(); 
        }
    }
    
    
    static testMethod void performLocateMatchingAccountsById() {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = getCurrentUser();
        system.runAs(currentUser){
            PageReference pageRef = Page.InboundSearch_phonelookup;
            Test.setCurrentPage(pageRef);
            
            Account acc = createCustomerAccount();          
            Contact cont = new Contact();
            cont.LastName = 'testContact1';
            cont.AccountId = acc.Id;
            insert cont;
            
            acc.Primary_Contact__c = cont.Id;
            acc.Phone = '4136522270';
            acc.SAP_Customer_Number__c = '0000001003';
            update acc;
            
            test.startTest();
            ApexPages.StandardController std = new ApexPages.StandardController(acc);
            ApexPages.currentPage().getParameters().put('SF_SearchValue',acc.Phone);
            
            InboundSearch_phonelookup controller = new InboundSearch_phonelookup(std);
            Set<Id> setOfAccountIds = new Set<Id>();
            ID testId ='00300000003T2PGAA0';
            setOfAccountIds.add(testId);
            controller.InboundDID = '4136522270';
            controller.SF_SearchValue = '4136522270';
            //controller.searchstring = 'test';
            Phone_Lookup__c phoneLookup1 = new Phone_Lookup__c();
            phoneLookup1.Account__c = acc.Id;
            phoneLookup1.Match_Number__c = '41365';
            phoneLookup1.Phone_Number__c = '4136522270';
            phoneLookup1.SAP_Customer_Number__c ='0000001005';
            insert phoneLookup1;
            controller.matchedAccountId = acc.Id;
            controller.matchedAccountName = acc.Name;
            //controller.searchstring = 'abc'; 
            //controller.searchquery = 'test';
            controller.returnedPhoneLookups.add(phoneLookup1);
            controller.phoneNumberConverted = '4136522270';
           
           Id [] fixedSearchResults= new Id[1];
           fixedSearchResults[0] = phoneLookup1.id;
           Test.setFixedSearchResults(fixedSearchResults);
               
            controller.performAllPhoneNumberSearchesFromPhoneLookup();
           // controller.searchResultsBasedOnDid1.add(phoneLookup1); 
            controller.performAllPhoneNumberSearchesFromPhoneLookup();
            test.stopTest();
            
        }
    }
    
    static testMethod void performMiscValidations() {
        //this method is testing additional, misc getters and setters that the vf page uses throughout its lifecycle.
        //...code coverage...
        User currentUser = getCurrentUser(); 
        system.runAs(currentUser){
            PageReference pageRef = Page.InboundSearch_phonelookup;
            Test.setCurrentPage(pageRef);
            
            Account acc = createCustomerAccount();          
            Contact cont = new Contact();
            cont.LastName = 'testContact1';
            cont.AccountId = acc.Id;
            insert cont;
            
            acc.Primary_Contact__c = cont.Id;
            acc.Phone = '4136522270';
            update acc;
            
            test.startTest();
            ApexPages.StandardController std = new ApexPages.StandardController(acc);
            ApexPages.currentPage().getParameters().put('SF_SearchValue',acc.Phone);
            
            InboundSearch_phonelookup controller = new InboundSearch_phonelookup(std);
            controller.inboundDID = '4136522270';
            //controller.searchstring = 'abc';
            controller.phoneNumberConverted = '4136522270';
            controller.SF_SearchValue = '4136522270';            
            //controller.searchquery = 'test';
            controller.matchedAccountId = acc.Id;
            controller.matchedAccountName = acc.Name;
            
            Phone_Lookup__c phoneLookup = new Phone_Lookup__c();
            phoneLookup.Account__c = acc.Id;
            phoneLookup.Match_Number__c = '54365';
            phoneLookup.Phone_Number__c = '5463785678';
            phoneLookup.SAP_Customer_Number__c ='0000001003';
            insert phoneLookup;
            
            
       
            controller.returnedPhoneLookups.add(phoneLookup);   
            List<Phone_Lookup__c> phoneLookups = new List<Phone_Lookup__c>();
            phoneLookups.add(phoneLookup);
            controller.phoneLookups.add(phoneLookup);
            //controller.phoneLookups = phoneLookups;
            controller.performAllPhoneNumberSearchesFromPhoneLookup();
            test.stopTest(); 
            
        }
    }
    
    private static Account createCustomerAccount() {     
        Account testAccount = new Account();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = 'Inbound Search Testing Account';
        testAccount.CurrencyISOCode = 'USD';
        testAccount.HIN__c = 'Test123';
        testAccount.SAP_Customer_Number__c = '0000009999';
        insert testAccount;
        return testAccount;
    }
    
    private static User getCurrentUser() {
        //User currentUser = CreateTestClassData.reusableUser('System Administrator','admin2');
        User currentUser = CreateTestClassData.reusableUser('Inside Sales Rep-CAG','Caleb-Bi');
        return currentUser;
    }
}