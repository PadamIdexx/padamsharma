/* Class Name   : Test_QLITriggerHandler
 * Description  : Test Class with unit test scenarios to cover the QLITriggerHandler class
 * Created By   : Pooja Waade
 * Created On   : 10-06-2015 
 
* Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date                   Modification ID       Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Pooja Waade           10-06-2015                   1000               Initial version
 * Sirisha Kodi          10-13-2015                   1001               Add new test methods to improve code coverage
 */

@isTest
public with sharing class Test_QLITriggerHandler{
    
 
    static testMethod void Test_beforeUpdate() {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        
        //User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
        //system.runAs(currentUser){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
		System.runAs ( thisUser ) {

            
            //Account testAccount = CreateTestClassData.createCustomerAccount();
            Account testAccount = new Account(RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId(),
                                              Name = 'AccountForTest_QLITriggerHandler',
                                              CurrencyISOCode = 'USD'); 
            if(testAccount!=null){ 
            if(Schema.sObjectType.Account.isCreateable())  {                                
            insert testAccount;
            }
            }
            System.assert(testAccount.id!=null);
            
            Opportunity testOpty = CreateTestClassData.createLPDOpportunity();
            
            // custom priceBook
            Pricebook2 pb = new Pricebook2(Name='Custom Pricebook Test_QLITriggerHandler', isActive=true);
            if(pb!=null){
            if(Schema.sObjectType.Pricebook2.isCreateable()){
            insert pb;
            }
            }
            System.assert(pb.id!=null);
            
            //Query Standard price book
            //Pricebook2 standardPriceBook = [Select Id,IsActive from  pricebook2 where isActive=true and Name = 'IDEXX Standard Price Book'];
            Id pricebookId = Test.getStandardPricebookId();

             //Product
            Product2 newProduct = new Product2(Name = 'Test ProducTest_QLITriggerHandlert',CurrencyIsoCode = 'EUR',Kits_to_Test_Conversion__c = 20.00,
                                               SAP_MATERIAL_NUMBER__c = '9991', isActive=true  );
            if(Schema.sObjectType.Product2.isCreateable()){
            if(newProduct!=null){
            insert newProduct;
            }
            }
             System.assert(newProduct.id!=null);
            //std priceBook entry
          //  if(standardPriceBook<>null){
                PricebookEntry stdpbe = new PricebookEntry(UseStandardPrice = false,Pricebook2Id=pricebookId,Product2Id=newProduct.id,
                                                            IsActive=true, UnitPrice=99,PriceBook_SAPMatNr__c = 'test' );
                if(Schema.sObjectType.PricebookEntry.isCreateable()){                                            
                if(stdpbe!=null){                                            
                insert stdpbe;
                }
                }
                System.assert(stdpbe.id!=null);
           // }
            // pricebook entry
            PricebookEntry pbe = new PricebookEntry();
            pbe.UseStandardPrice = false;
            pbe.Pricebook2Id=pb.id;
            pbe.Product2Id=newProduct.id;
            pbe.IsActive=true;
            pbe.UnitPrice=99;
            pbe.PriceBook_SAPMatNr__c = 'test2';
             if(Schema.sObjectType.PricebookEntry.isCreateable()){                                            
            if(pbe!=null){
            insert pbe;
            }
            }
            System.assert(pbe.id!=null);
            Quote newQuote = new Quote();
            newQuote.Name = 'Test Quote';
            newQuote.Date_of_Offer_Expiration__c = date.today();
            newQuote.Account__c = testAccount.id;
            newQuote.Opportunityid = testOpty.id;
            newQuote.General_Information_BelgiumDutch__c = 'test Gen Infor';
            newQuote.Pricebook2Id = pb.id;
            //System.debug('@@@' + newQuote);
            if(Schema.sObjectType.Quote.isCreateable()){                                            
            if(newQuote!=null){
            insert newQuote;
            }
            }
            System.assert(newQuote.id!=null);
        
            QuoteLineItem QLI = new QuoteLineItem(QuoteId = newQuote.Id, PricebookEntryId = pbe.Id, Product2Id = newProduct.Id,
                                                  UnitPrice = 132.00,Quantity = 2.00);
            if(Schema.sObjectType.QuoteLineItem.isCreateable()){                                            
            if(QLI!=null){
            insert QLI;
            }
            }
            System.assert(QLI.id!=null);
            
            QLI.Discount_Type__c = 'Fixed';
            QLI.Discount__c = 20;
            update QLI;
            System.assertEquals(QLI.Discount_Type__c, 'Fixed');
            
            QLI.Discount_Type__c = 'Percent';
            QLI.Discount__c = 10;
            update QLI;
            System.assertEquals(QLI.Discount_Type__c, 'Percent');
            
        }
		
    }

     

}