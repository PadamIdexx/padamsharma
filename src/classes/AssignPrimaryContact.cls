/**************************************************************************************
 * Create By:   Deloitte Consulting LLP.
 * CreateDate:  14-Oct-2015
 * Description: This batch is designed to assign a Primary Contact to all Accounts that do not have a Primary Contact. 
 * scenario 1: If an account has related contact(s), assign any/only one contact as primary contact                 
 * scenario 2: If an account has no related contact, create a new place holder contact and assign that as its primary contact
 * 
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                 Created Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Vishal Negandhi          14-Oct-2015                   Initial version.
 **************************************************************************************/
global class AssignPrimaryContact implements Database.Batchable<sObject>, Schedulable{
	String Query; 

	global AssignPrimaryContact(){
		Query = 'Select Id FROM Account WHERE Primary_Contact__c = NULL AND RecordType.DeveloperName = \'Customer\' ORDER BY CreatedDate DESC';
	}

    global Database.QueryLocator start(Database.BatchableContext BC){
    	return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Account> scope){
   	  	Set<Id> AccountIds = new Set<Id>();		
   	  	Map<Id, Id> AccountIdToContactId = new Map<Id, Id>();
   	  	List<Account> AccountsToBeUpdated = new List<Account>();
   	  	List<Contact> PlaceHolderContactsToBeCreated = new List<Contact>();
   	  	
   	  		
      	// iterate through all accounts and fetch their id
      	for(Account acc : scope){
      		AccountIds.add(acc.Id);
      	}
      	
      	// scenario 1: fetching related contacts for accounts and storing in a map to assign primary contact to Account
      	Account accountRecord = new Account();
      	for(Contact relatedContact : [Select Id, AccountId From Contact WHERE AccountId IN :AccountIds]){
      		AccountIdToContactId.put(relatedContact.AccountId, relatedContact.Id);
      	}
      	
      	for(Account acc : [Select Id From Account WHERE Id IN :AccountIds AND Id IN :AccountIdToContactId.keyset()]){
  			accountRecord = new Account(Id = acc.Id);
  			accountRecord.Primary_Contact__c = AccountIdToContactId.get(acc.Id);
  			AccountsToBeUpdated.add(accountRecord);
      	}
      	
      	// scenario 2: for accounts not having a contact, create a new place holder contact and assign the same as primary contact
      	Contact PlaceHolderContact = new Contact();
      	for(Account acc : [Select Id, Name, SAP_Customer_Number__c, CurrencyIsoCode From Account Where Id IN :AccountIds AND Id NOT IN :AccountIdToContactId.keyset()]){
  			PlaceHolderContact = new Contact();
  			PlaceHolderContact.AccountId = acc.Id;
  			if(acc.SAP_Customer_Number__c != null)
  				PlaceHolderContact.LastName = acc.SAP_Customer_Number__c;
  			else
  				PlaceHolderContact.LastName = acc.Id;
  			PlaceHolderContact.FirstName = 'Place Holder';
  			PlaceHolderContact.CurrencyIsoCode = acc.CurrencyIsoCode;
  			PlaceHolderContactsToBeCreated.add(PlaceHolderContact);
      	}
      	
      	// for scenario 1, update accounts with primary contact
      	if(!AccountsToBeUpdated.isEmpty())
      		update AccountsToBeUpdated;
      	
      	// for scenario 2, create new Place holder contacts for Accounts and map them as Primary Contact on Accounts
      	if(!PlaceHolderContactsToBeCreated.isEmpty()){
      		insert PlaceHolderContactsToBeCreated;
      		AccountsToBeUpdated = new List<Account>();
      		
      		Account ParentAccount = new Account();
      		for(Contact newContacts : PlaceHolderContactsToBeCreated){
      			ParentAccount = new Account(Id = newContacts.AccountId);
      			ParentAccount.Primary_Contact__c = newContacts.Id;
      			AccountsToBeUpdated.add(ParentAccount);
      		}
      		update AccountsToBeUpdated;
      	}
   	}  

   	global void finish(Database.BatchableContext BC){
   	}
   	
   	global void execute (SchedulableContext SC){
         database.executeBatch(new AssignPrimaryContact());
    }
}