public with sharing class OrderReadResponse {

    //Web Service call out related constants.
    //
    // SOAP Action
    public final static String SOAPACTION = 'http://sap.com/xi/WebService/soap1.1';
    
    // XML Namespace for this object
    // Salesforce representation of Namespace
    public final static String NAMESPACE = 'http://idexx.com/sappo/sales/salesforce';
    public final static String SF_NAMESPACE = 'idexxComSappoSalesSalesforce';
    
    // XML Namespace of types    
    // Salesforce representation of Namespace   
    public final static String XMLTYPE_NAMESPACE = 'http://idexx.com/sappo/global/types';
	public final static String SF_XMLTYPE_NAMESPACE = 'idexxComSappoGlobalTypes';
     
	// Name of Class that carries the payload.  There is no generic way to get this at this 
	// from the obect at runtime at this time. 
    public final static String PAYLOAD_CLASSNAME = 'SalesOrderReadResponse';
    
    public class SalesOrderReadResponse{
        public String SalesOrganization;
        public String DistributionChannel;
        public String Division;
        public String Incoterms;
        public String PaymentTerms;
        public String DeliveryBlock;
        public String BillingBlock;
        public String PurchaseOrderNumber;
        public String SalesPerson;
        public String RequestedDeliveryDate;
        public String CompleteDelivery;
        public String CouponCode;
        public String ExternalProposalNumber;

        public Items_element[] Items;
		public Conditions_element[] Conditions;
        public Partners_element[] Partners;
        public Texts_element[] Texts;

        private String[] SalesOrganization_type_info = new String[]{'SalesOrganization',NAMESPACE,null,'1','1','false'};
        private String[] DistributionChannel_type_info = new String[]{'DistributionChannel',NAMESPACE,null,'1','1','false'};
        private String[] Division_type_info = new String[]{'Division',NAMESPACE,null,'1','1','false'};
        private String[] Incoterms_type_info = new String[]{'Incoterms',NAMESPACE,null,'0','1','false'};
        private String[] PaymentTerms_type_info = new String[]{'PaymentTerms',NAMESPACE,null,'0','1','false'};
        private String[] DeliveryBlock_type_info = new String[]{'DeliveryBlock',NAMESPACE,null,'0','1','false'};
        private String[] BillingBlock_type_info = new String[]{'BillingBlock',NAMESPACE,null,'0','1','false'};
        private String[] PurchaseOrderNumber_type_info = new String[]{'PurchaseOrderNumber',NAMESPACE,null,'0','1','false'};
        private String[] SalesPerson_type_info = new String[]{'SalesPerson',NAMESPACE,null,'0','1','false'};
        private String[] RequestedDeliveryDate_type_info = new String[]{'RequestedDeliveryDate',NAMESPACE,null,'0','1','false'};
        private String[] CompleteDelivery_type_info = new String[]{'CompleteDelivery',NAMESPACE,null,'0','1','false'};
        private String[] CouponCode_type_info = new String[]{'CouponCode',NAMESPACE,null,'0','1','false'};            
        private String[] ExternalProposalNumber_type_info = new String[]{'ExternalProposalNumber',NAMESPACE,null,'0','1','false'};            

            
        private String[] Items_type_info = new String[]{'Items',NAMESPACE,null,'0','-1','false'};            
        private String[] Conditions_type_info = new String[]{'Conditions',NAMESPACE,null,'0','-1','false'};            
        private String[] Partners_type_info = new String[]{'Partners',NAMESPACE,null,'0','-1','false'};            
        private String[] Texts_type_info = new String[]{'Texts',NAMESPACE,null,'0','-1','false'};            
            
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'SalesOrganization','DistributionChannel','Division','Incoterms','PaymentTerms','DeliveryBlock','BillingBlock','PaymentTerms',
            													'PurchaseOrderNumber','SalesPerson','RequestedDeliveryDate','CompleteDelivery','CouponCode','ExternalProposalNumber','Items','Conditions','Partners','Texts'};
    }
    
    //TODO: complete the sub objects
    public class Items_element {
        public String ItemNumber;
        public String ExternalItemNumber;
        public String Material;
        public String SpeedCode;
        public String ShortText;
        public String NetValue;
        public String Currency_x;
        public String UoM;
        public String DeliveryDate;
        public String RequestedQuantity;
		public String ConfirmedQuantity;        
        public String Plant;
        public String ShippingPoint;
        public String ItemCategory;
		public String HigherLevelItem;
        public String StorageLocation;
		public String MaterialGroup;
		public String MaterialGroup3;        
		public String MaterialGroup4;        
		public String MaterialGroup5;        
        public String Subtotal1;
        public String Subtotal2;
        public String Subtotal3;
        public String Subtotal4;
        public String Subtotal5;
        public String Subtotal6;                
        public String Batch;
        public String Incoterms;
		public String BatchedItem;
        public String BatchExpirationDate;
        public String isDelivered;
        
                
        private String[] ItemNumber_type_info = new String[]{'ItemNumber',NAMESPACE,null,'1','1','false'};
        private String[] ExternalItemNumber_type_info = new String[]{'ExternalItemNumber',NAMESPACE,null,'0','1','false'};
        private String[] Material_type_info = new String[]{'Material',NAMESPACE,null,'0','1','false'};
        private String[] SpeedCode_type_info = new String[]{'SpeedCode',NAMESPACE,null,'0','1','false'};
        private String[] ShortText_type_info = new String[]{'ShortText',NAMESPACE,null,'0','1','false'};
        private String[] NetValue_type_info = new String[]{'NetValue',NAMESPACE,null,'0','1','false'};
        private String[] Currency_x_type_info = new String[]{'Currency',NAMESPACE,null,'0','1','false'};
        private String[] UoM_type_info = new String[]{'UoM',NAMESPACE,null,'0','1','false'};
        private String[] DeliveryDate_type_info = new String[]{'DeliveryDate',NAMESPACE,null,'0','1','false'};            
        private String[] RequestedQuantity_type_info = new String[]{'RequestedQuantity',NAMESPACE,null,'0','1','false'};            
		private String[] ConfirmedQuantity_type_info = new String[]{'ConfirmedQuantity',NAMESPACE,null,'0','1','false'};                        
		private String[] Plant_type_info = new String[]{'Plant',NAMESPACE,null,'0','1','false'};                                    
		private String[] ShippingPoint_type_info = new String[]{'ShippingPoint',NAMESPACE,null,'0','1','false'};                                                
		private String[] ItemCategory_type_info = new String[]{'ItemCategory',NAMESPACE,null,'0','1','false'};                                                
		private String[] HigherLevelItem_type_info = new String[]{'HigherLevelItem',NAMESPACE,null,'0','1','false'};                                                            
		private String[] StorageLocation_type_info = new String[]{'StorageLocation',NAMESPACE,null,'0','1','false'};                                                                        
		private String[] MaterialGroup_type_info = new String[]{'MaterialGroup',NAMESPACE,null,'0','1','false'};                                                                        
		private String[] MaterialGroup3_type_info = new String[]{'MaterialGroup3',NAMESPACE,null,'0','1','false'};                                                                        
		private String[] MaterialGroup4_type_info = new String[]{'MaterialGroup4',NAMESPACE,null,'0','1','false'};                                                                        
		private String[] MaterialGroup5_type_info = new String[]{'MaterialGroup5',NAMESPACE,null,'0','1','false'};                                                                        
        private String[] Subtotal1_type_info = new String[]{'Subtotal1',NAMESPACE,null,'0','1','false'};                                                                                    
		private String[] Subtotal2_type_info = new String[]{'Subtotal2',NAMESPACE,null,'0','1','false'};                                                                                    
		private String[] Subtotal3_type_info = new String[]{'Subtotal3',NAMESPACE,null,'0','1','false'};                                                                                    
		private String[] Subtotal4_type_info = new String[]{'Subtotal4',NAMESPACE,null,'0','1','false'};                                                                                    
		private String[] Subtotal5_type_info = new String[]{'Subtotal5',NAMESPACE,null,'0','1','false'};                                                                                    
		private String[] Subtotal6_type_info = new String[]{'Subtotal6',NAMESPACE,null,'0','1','false'};                                                                                                
		private String[] Batch_type_info = new String[]{'Batch',NAMESPACE,null,'0','1','false'};                                                                                                
		private String[] Incoterms_type_info = new String[]{'Incoterms',NAMESPACE,null,'0','1','false'};                                                                                                            
		private String[] BatchedItem_type_info = new String[]{'BatchedItem',NAMESPACE,null,'0','1','false'};                                                                                                            
		private String[] BatchExpirationDate_type_info = new String[]{'BatchExpirationDate',NAMESPACE,null,'0','1','false'};                                                                                                            
		private String[] isDelivered_type_info = new String[]{'isDelivered',NAMESPACE,null,'0','1','false'};                                                                                                                        
            
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'ItemNumber','ExternalItemNumber','Material','SpeedCode','ShortText','NetValue','Currency_x',
                                                                'UoM','DeliveryDate','RequestedQuantity','ConfirmedQuantity','Plant','ShippingPoint',
                                                                'ItemCategory','HigherLevelItem','StorageLocation','MaterialGroup','Subtotal1','Subtotal2',
                                                                'Subtotal3','Subtotal4','Subtotal5','Subtotal6','MaterialGroup3','MaterialGroup4','MaterialGroup5',
            													'Batch','Incoterms','BatchedItem','BatchExpirationDate','isDelivered'};
    }    
    
     public class Conditions_element {
        public String ItemNumber;
        public String ConditionStep;
        public String ConditionCount;
        public String ConditionType;
        public String ConditionRate;
        public String CalculationType;
        public String ConditionCategory;
        public String StatisticalIndicator;
        public String Currency_x;         
        public String ConditionValue;   
        public String ConditionClass;
        public String ConditionIsInActive;
        public String Description;
    
        private String[] ItemNumber_type_info = new String[]{'ItemNumber',NAMESPACE,null,'1','1','false'};
        private String[] ConditionStep_type_info = new String[]{'ConditionStep',NAMESPACE,null,'1','1','false'};
        private String[] ConditionCount_type_info = new String[]{'ConditionCount',NAMESPACE,null,'1','1','false'};
        private String[] ConditionType_type_info = new String[]{'ConditionType',NAMESPACE,null,'1','1','false'};
		private String[] ConditionRate_type_info = new String[]{'ConditionRate',NAMESPACE,null,'0','1','false'};            
		private String[] CalculationType_type_info = new String[]{'CalculationType',NAMESPACE,null,'0','1','false'};            
		private String[] ConditionCategory_type_info = new String[]{'ConditionCategory',NAMESPACE,null,'0','1','false'};            
		private String[] StatisticalIndicator_type_info = new String[]{'StatisticalIndicator',NAMESPACE,null,'0','1','false'};                        
        private String[] Currency_x_type_info = new String[]{'Currency',NAMESPACE,null,'0','1','false'};
        private String[] ConditionValue_type_info = new String[]{'ConditionValue',NAMESPACE,null,'0','1','false'};
        private String[] ConditionClass_type_info = new String[]{'ConditionClass',NAMESPACE,null,'0','1','false'};            
        private String[] ConditionIsInActive_type_info = new String[]{'ConditionIsInActive',NAMESPACE,null,'0','1','false'};
        private String[] Description_type_info = new String[]{'Description',NAMESPACE,null,'0','1','false'};
            
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'ItemNumber','ConditionStep','ConditionCount','ConditionType','ConditionRate','CalculationType','ConditionCategory',
            													'StatisticalIndicator','Currency_x','ConditionValue','ConditionClass','ConditionIsInActive','Description'};
    }
    
    public class Partners_element {
    	public String PartnerType;
        public String Customer;
        public String Name;
        public String Name2;
        public String Street;
        public String Country;
        public String PostalCode;
        public String City;
        public String District;
        public String Region;
        public String TransportationZone;
        
        private String[] PartnerType_type_info = new String[]{'PartnerType',NAMESPACE,null,'1','1','false'};
		private String[] Customer_type_info = new String[]{'Customer',NAMESPACE,null,'1','1','false'};
		private String[] Name_type_info = new String[]{'Name',NAMESPACE,null,'0','1','false'};
		private String[] Name2_type_info = new String[]{'Name2',NAMESPACE,null,'0','1','false'};
		private String[] Street_type_info = new String[]{'Street',NAMESPACE,null,'0','1','false'};
		private String[] Country_type_info = new String[]{'Country',NAMESPACE,null,'0','1','false'};
		private String[] PostalCode_type_info = new String[]{'PostalCode',NAMESPACE,null,'0','1','false'};            
		private String[] City_type_info = new String[]{'City',NAMESPACE,null,'0','1','false'};            
		private String[] District_type_info = new String[]{'District',NAMESPACE,null,'0','1','false'};                        
		private String[] Region_type_info = new String[]{'Region',NAMESPACE,null,'0','1','false'};                        
		private String[] TransportationZone_type_info = new String[]{'TransportationZone',NAMESPACE,null,'0','1','false'};                                    
            
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'PartnerType','Customer','Name','Name2','Street','Country','PostalCode','City','District','Region','TransportationZone'};
            
    }
    
    public class Texts_element {
        public String ItemNumber;
        public String TextID;
        public String Language;
        public String TextLine;

		private String[] ItemNumber_type_info = new String[]{'ItemNumber',NAMESPACE,null,'0','1','false'};                        
        private String[] TextID_type_info = new String[]{'TextID',NAMESPACE,null,'0','1','false'};                        
		private String[] Language_type_info = new String[]{'Language',NAMESPACE,null,'0','1','false'};                                    
		private String[] TextLine_type_info = new String[]{'TextLine',NAMESPACE,null,'0','1','false'};                                                
            
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'ItemNumber','TextID','Language','TextLine'};
   }
    
    /*
     * Transformer - Construct an SAPOrder from a response object
     */ 
    public static SAPOrder transform(SAPOrder order, OrderReadResponse.SalesOrderReadResponse response) {
        
        
        //process the response
        order.salesOrg = response.SalesOrganization;
        order.distributionChannel = response.DistributionChannel;
        order.division = response.Division;
        order.incoterms = response.Incoterms;
        order.paymentTerms = response.PaymentTerms;
        order.deliveryBlock = response.DeliveryBlock;
        order.billingBlock = response.BillingBlock;
        order.poNumber = response.PurchaseOrderNumber;
        order.salesPerson = response.SalesPerson;
        order.requestedDeliveryDate = SAPServiceDataHelper.getDateForSAPDate(response.RequestedDeliveryDate);
        order.completeDelivery =  SAPServiceDataHelper.getBooleanForSAPIndicator(response.CompleteDelivery);
        order.couponCode = response.CouponCode;
        order.externalProposalNumber = response.ExternalProposalNumber;
        
        //process header conditions    
        //
        //clear values before processing
        order.pointsRedeemed = null;
        order.pointsRedeemedCurrency = null;
        order.pointsConditionStep = null;
        order.pointsConditionCount = null;	
        
        order.discountType = null;
        order.discountValue = null;
        order.discountCurrency = null;
        order.discountConditionStep = null;
        order.discountConditionCount = null; 
        
        //3.15.2017 HLK added - bypassing the setting of headerConditionItemNumber if we're in a test - to hardcode it as it's
        ///                     not yet up in prod - via Orders_Integration__c - (errors below when inspecting headerConditionItemNumber.equals...).
        String headerConditionItemNumber = '';
        if (Test.isRunningTest()) { //3.15.2017
            headerConditionItemNumber = '000000';
        } else {
            headerConditionItemNumber = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.HEADER_CONDITION_ID);
        }
        
        
        if ((null <> response.Conditions) && (response.Conditions.size() > 0)) {
            for (OrderReadResponse.Conditions_element ic : response.Conditions) {
                if (headerConditionItemNumber.equals(ic.itemNumber)) {
                    //header discounts
					//points                    
                    if (String.isNotBlank(ic.ConditionType) 
                        && ic.ConditionType.equals(SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.POINTS_REDEMPTION_CONDITION))) {

						//SAP does not currently support he remove of conditions.  Handle this by interpreting 
						//a zero value condition as no condition.                            
                        if (String.isNotBlank(ic.ConditionValue)
                           		&& (SAPServiceDataHelper.getDecimalForSAPNumber(ic.ConditionValue,2) > 0)) {
                            order.pointsRedeemed = SAPServiceDataHelper.getDecimalForSAPNumber(ic.ConditionValue,2);
                            order.pointsRedeemedCurrency = ic.Currency_x;
                            order.pointsConditionStep = ic.ConditionStep;
                            order.pointsConditionCount = ic.ConditionCount;							
                        }
                    }
                    
                    //Handle discounts
                    //We will only have one of these at a time if they exist
                    //
                    //Value discount are different for NA and Europe.  We will have only one of these conditions.
                    if (String.isNotBlank(ic.ConditionType) 
                        && ic.ConditionType.equals(SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.PERCENTAGE_DISCOUNT_CONDITION))) {

						//SAP does not currently support he remove of conditions.  Handle this by interpreting 
						//a zero value condition as no condition.                            
                        if (String.isNotBlank(ic.ConditionRate)) {
                            order.discountType = 'Percent - %';
                            //discounts come back to us negative.  turn them back into positive values for display
                            //percentage discounts also come back as a rate and not a value
                            order.discountValue = Math.abs(SAPServiceDataHelper.getDecimalForSAPNumber(ic.ConditionRate,2)) ;
                            order.discountCurrency = ic.Currency_x;
                            order.discountConditionStep = ic.ConditionStep;
                            order.discountConditionCount = ic.ConditionCount;                                   
                        }                         
                    } else if (String.isNotBlank(ic.conditionType) 
                        && ic.conditionType.equals(SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.VALUE_DISCOUNT_CONDITION))) {

                        //SAP does not currently support he remove of conditions.  Handle this by interpreting 
						//a zero value condition as no condition.                            
                        if (String.isNotBlank(ic.ConditionValue)) {
                            order.discountType = 'Value';
                            //discounts come back to us negative.  turn them back into positive values for display
                            order.discountValue = Math.abs(SAPServiceDataHelper.getDecimalForSAPNumber(ic.ConditionValue,2)) ;
                            order.discountCurrency = ic.Currency_x;
                            order.discountConditionStep = ic.ConditionStep;
                            order.discountConditionCount = ic.ConditionCount; 
                        }                            
                           
                    } else if (String.isNotBlank(ic.conditionType) 
                        && ic.conditionType.equals(SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.NA_VALUE_DISCOUNT_CONDITION))) {
                        //SAP does not currently support he remove of conditions.  Handle this by interpreting 
						//a zero value condition as no condition.                            
                        if (String.isNotBlank(ic.ConditionRate)) {
                            order.discountType = 'Value';
                            order.discountValue = Math.abs(SAPServiceDataHelper.getDecimalForSAPNumber(ic.ConditionRate,2)) ;
                            order.discountCurrency = ic.Currency_x;
                            order.discountConditionStep = ic.ConditionStep;
                            order.discountConditionCount = ic.ConditionCount;                                 
                        }                              
                    }	
                    
                    //freight 
                    if (String.isNotBlank(ic.conditionType)
                        && (ic.conditionType.equals(SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.FREIGHT_ZF10)))
                       		|| ic.conditionType.equals(SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.FREIGHT_ZF11))) {
                            order.freightType = ic.ConditionType;
                            order.freightValue = SAPServiceDataHelper.getDecimalForSAPNumber(ic.ConditionRate, 2);
                            order.freightCurrency = ic.Currency_x;
                            order.freightConditionStep = ic.ConditionStep;
                            order.freightConditionCount = ic.ConditionCount;
                        }
                } 
            }
        }        
        
        //determine how many items are delivered.  
        Integer deliveredItemCount = 0;
        
        //process items   
        if ((null <> response.Items) && (response.Items.size() > 0)){
             List<SAPOrder.Item> items = new List<SAPOrder.Item>();

            for (OrderReadResponse.Items_element i : response.items) {
                SAPOrder.Item oi = new SAPOrder.Item();
                oi.itemNumber = i.itemNumber;
                
                //set max row number for order
                //we will need this when introducing new items with change
                Integer iVal = Integer.valueOf(oi.itemNumber);                
                if (iVal > order.maxItemNumber) {
                	order.maxItemNumber = iVal; 
                }
                
                oi.externalItemNumber = i.ExternalItemNumber;
                oi.material = i.Material;
                oi.speedCode = i.SpeedCode;
                oi.shortText = i.ShortText;
                oi.netValue = SAPServiceDataHelper.getDecimalForSAPNumber(i.NetValue, 2);
                oi.currencyCode = i.Currency_x;
                oi.uom = i.UoM;
				oi.deliveryDate = SAPServiceDataHelper.getDateForSAPDate(i.DeliveryDate);
                oi.requestedQuantity = SAPServiceDataHelper.getDecimalForSAPNumber(i.RequestedQuantity,2);
                oi.confirmedQuantity = SAPServiceDataHelper.getDecimalForSAPNumber(i.confirmedQuantity,2);
				oi.plant = i.Plant;
                oi.shippingPoint = i.ShippingPoint;
				oi.itemCategory = i.ItemCategory;
                oi.higherLevelItem = i.HigherLevelItem;
				oi.storageLocation = i.StorageLocation;
                oi.materialGroup = i.MaterialGroup;

                oi.totalPrice = SAPServiceDataHelper.getDecimalForSAPNumber(i.Subtotal1,2);
                oi.netSubTotal = SAPServiceDataHelper.getDecimalForSAPNumber(i.Subtotal2,2);
                oi.netPrice = SAPServiceDataHelper.getDecimalForSAPNumber(i.Subtotal3,2);
                oi.shippingCharges = SAPServiceDataHelper.getDecimalForSAPNumber(i.Subtotal4,2);
                oi.tax = SAPServiceDataHelper.getDecimalForSAPNumber(i.Subtotal5,2);
                oi.discount = SAPServiceDataHelper.getDecimalForSAPNumber(i.Subtotal6,2);                
                
                //unit price.  Calculate if requested quantity was > 0 only
                if (oi.requestedQuantity > 0) {                
                	oi.unitPrice = oi.totalPrice / oi.requestedQuantity;                    
                    oi.unitPrice = oi.unitPrice.setScale(2);
                }
                
                oi.freeGoodsAgent = i.MaterialGroup3;
                oi.freeGoodsCostCenter = i.MaterialGroup4;                
                oi.freeGoodsReason = i.MaterialGroup5;

                if (String.isNotBlank(oi.freeGoodsAgent) 
                   && String.isNotBlank(oi.freeGoodsCostCenter)
                   && String.isNotBlank(oi.freeGoodsReason)) {
                    oi.freeGoods = true;
                   } else {
                       oi.freeGoods = false;
                   }
                
				oi.batch = i.batch;
                oi.incoterms = i.Incoterms;
                oi.batchedItem = SAPServiceDataHelper.getBooleanForSAPIndicator(i.BatchedItem);
                oi.batchExpirationDate = SAPServiceDataHelper.getDateForSAPDate(i.BatchExpirationDate);
                oi.isDelivered = SAPServiceDataHelper.getBooleanForSAPIndicator(i.isDelivered);
                
                //if this item is delivered, increment the count
                if (oi.isDelivered) {
                    deliveredItemCount++;
                }

                //process header conditions    
                //
                //clear values before processing
                oi.discountType = null;
                oi.discountValue = null;
                oi.discountCurrency = null;
                oi.discountConditionStep = null;
                oi.discountConditionCount = null;  
                oi.vat = 0.00;
                oi.points = 0.00;
                
                if ((null <> response.Conditions) && (response.Conditions.size() > 0)) {
                    for (OrderReadResponse.Conditions_element ic : response.Conditions) {
                        if (String.isNotBlank(ic.itemNumber) && ic.itemNumber.equals(oi.itemNumber)) {
                            
							//SAP does not currently support he remove of conditions.  Handle this by interpreting 
							//a zero value condition as no condition.                            
                        	if (String.isNotBlank(ic.ConditionValue)) {
                                                            
                                //discount
                                if (String.isNotBlank(ic.ConditionType) 
                                    && ic.ConditionType.equals(SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.PERCENTAGE_DISCOUNT_CONDITION))) {
                                    oi.discountType = 'Percent - %';
    		                        //discounts come back to us negative.  turn them back into positive values for display	 
    		                        //percentage discounts also come back as a rate and not a value                                       
                                    oi.discountValue = Math.abs(SAPServiceDataHelper.getDecimalForSAPNumber(ic.ConditionRate,2));
                                    oi.discountCurrency = ic.Currency_x;
                                    oi.discountConditionStep = ic.ConditionStep;
                                    oi.discountConditionCount = ic.ConditionCount;                                                                
                                } else if (String.isNotBlank(ic.ConditionType) 
                                    && ic.conditionType.equals(SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.VALUE_DISCOUNT_CONDITION))) {
                                    oi.discountType = 'Value';
    		                        //discounts come back to us negative.  turn them back into positive values for display	                                        
                                    oi.discountValue = Math.abs(SAPServiceDataHelper.getDecimalForSAPNumber(ic.ConditionValue,2));
                                    oi.discountCurrency = ic.Currency_x;
                                    oi.discountConditionStep = ic.ConditionStep;
                                    oi.discountConditionCount = ic.ConditionCount;                                                                
                                } else if (String.isNotBlank(ic.ConditionType) 
                                    && ic.conditionType.equals(SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.NA_VALUE_DISCOUNT_CONDITION))) {
                                    oi.discountType = 'Value';
                                    oi.discountValue = Math.abs(SAPServiceDataHelper.getDecimalForSAPNumber(ic.ConditionValue,2));
                                    oi.discountCurrency = ic.Currency_x;
                                    oi.discountConditionStep = ic.ConditionStep;
                                    oi.discountConditionCount = ic.ConditionCount;                                                                
                                }							                            
    
                                //freight
                                if (String.isNotBlank(ic.ConditionType)
                                   && ic.ConditionType.equals(SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.FREIGHT_CONDITION))) {
                                    oi.freightRate = SAPServiceDataHelper.getDecimalForSAPNumber(ic.ConditionRate,2) ;
                                    oi.freightCurrency = ic.Currency_x;
                                    oi.freightConditionStep = ic.ConditionStep;
                                    oi.freightConditionCount = ic.ConditionCount;
                                }
                                    
                                //VAT
                                if (String.isNotBlank(ic.ConditionType)
                                   && ic.ConditionType.equals(SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.VAT_CONDITION))) {
                                    oi.vat = SAPServiceDataHelper.getDecimalForSAPNumber(ic.ConditionValue,2) ;
                                }
                                
                                //Points
                                if (String.isNotBlank(ic.ConditionType)
                                   && ic.ConditionType.equals(SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.POINTS_REDEMPTION_CONDITION))) {
                                    oi.points = SAPServiceDataHelper.getDecimalForSAPNumber(ic.ConditionValue,2) ;
                                }
                        	}
                        }
                    }
                }
                
                //pricing rollups
                oi.totalTax = oi.tax + oi.vat;
                //oi.totalTax = Utils.formatCurrency(oi.totalTax, oi.currencyCode);
                
                oi.total = oi.totalPrice + oi.discount + oi.shippingCharges + oi.totalTax;
               // oi.total= Utils.formatCurrency(oi.total, oi.currencyCode);
				
                order.totalTax += oi.totalTax;
                //order.totalTax = Utils.formatCurrency(order.totalTax, oi.currencyCode);
                
                order.totalShipping += oi.shippingCharges;
                
                order.grandTotal += oi.total;
                //the below is blowing up our gov. limits as it runs a soql query inside this for loop do we need this? 
                //If so we need to do it more efficiently <-- scb 3/14/17
               // order.grandTotal = Utils.formatCurrency(order.grandTotal, oi.currencyCode);
                
                items.add(oi);
            }
            
        	order.items = items;
        }
        
        //determine if the entire order is free goods
        Integer focCount = 0;    
        if (order.items != null) {
            for (SAPOrder.Item oi : order.items) {
                if (oi.freeGoods) {
                    focCount++;            
                }
            }
        }
        if ((order.items != null) 
            && (order.items.size() == focCount)) {
            order.freeGoodsOrder = true;
        } else {
            order.freeGoodsOrder = false;
        }

        //set order delivery status flags
        //Are any items delivered?  Are all items delivered?
        if (deliveredItemCount > 0) { 						                    
            if (deliveredItemCount == order.items.size()) {
                //delivered items is equal to number of items. fully delivered
                order.isDelivered = true;
                order.isPartiallyDelivered = false;
            } else {
                //some items delivered but not all.  partially delivered
                order.isPartiallyDelivered = true;
                order.isDelivered = false;
            }			
        } else {
            order.isPartiallyDelivered = false;
            order.isDelivered = false;
        }
        
        if ((null <> response.Partners) && (response.Partners.size() > 0)){
            List<SAPOrder.Partner> partners = new List<SAPOrder.Partner>();
            
            for (OrderReadResponse.Partners_element ip : response.Partners) {
                SAPOrder.Partner p = new SAPOrder.Partner();

                p.partnerType = ip.PartnerType;
                p.customer = ip.Customer;
                p.name = ip.Name;
                p.name2 = ip.Name2;
                p.street = ip.Street;
                p.country = ip.Country;
                p.postalCode = ip.PostalCode;
                p.city = ip.City;
                p.district = ip.District;
                p.region = ip.Region;
                p.transportationZone = ip.TransportationZone;                
                
                partners.add(p);
            }
            
            order.partners = partners;
        }
        
        if ((null <> response.Texts) && (response.Texts.size() > 0)) {            
            for (OrderReadResponse.Texts_element it : response.Texts) {

                if (String.isNotBlank(it.TextID)) {
                    if (it.TextID.equals(SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.CIG_TEXTID))) {
                        order.CIG = it.TextLine;
                        order.CIGCUGLanguage = it.Language;
                    }
                    
                    if (it.TextID.equals(SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.CUG_TEXTID))) {
                        order.CUG = it.TextLine;
                        order.CIGCUGLanguage = it.Language;
                    }     
                    
                    //process header text
                    if (String.isBlank(it.ItemNumber)) {
                        //put header text back together 
                        if (it.TextID.equals(SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.GENERAL_TEXTID))) {
                            order.headerTextLanguage = it.Language;
                            
                            if (String.isNotBlank(order.headerText)) {
                            	order.headerText = order.headerText + it.TextLine;
                            } else {
                                order.headerText = it.TextLine;
                            }
                        }
                    } 
                }
                    
            }
        }
        
        
        return order;
    }
    
}