@isTest
public with sharing class Test_Domain_Value_Scheduler {
    @isTest
    private static void testDailyScheduler(){
        Test.startTest();
        IDEXX_Domain_Values__c domVal = new IDEXX_Domain_Values__c();
        domVal.Code__c='150';
        domVal.Description__c = 'PO Overnite AM - PREPAID';
        domVal.Name= 'Free Goods Agent';
        domVal.Default_Cost_Center_For_Rep__c='234';
        INSERT domVal;
        String CRON_EXP = '0 0 0 1 1 ? 2025';  
        Test.setMock(WebServiceMock.class, new Test_DailyDomainWebserviceCalloutMock());        
        String jobId = System.schedule('ScheduledApex', CRON_EXP, new Daily_Domain_Value_Scheduler() );
        CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression); 
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2025-01-01 00:00:00', String.valueOf(ct.NextFireTime));
        Test.stopTest();
    }
    @isTest
    private static void testWeeklyScheduler(){
        Test.startTest();
        IDEXX_Domain_Values__c domVal = new IDEXX_Domain_Values__c();
        domVal.COde__c='150';
        domVal.Description__c = 'PO Overnite AM - PREPAID';
        domVal.Name= 'Ship Method';
        INSERT domVal;
        String CRON_EXP = '0 0 0 1 1 ? 2025';  
        Test.setMock(WebServiceMock.class, new Test_WeeklyDomainWebserviceCalloutMock());        
        String jobId = System.schedule('ScheduledApex', CRON_EXP, new Weekly_Domain_Value_Scheduler() );
        CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression); 
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2025-01-01 00:00:00', String.valueOf(ct.NextFireTime));
        Test.stopTest();
        
    }
    @isTest
    private static void testActivateDomainValScheduler(){
        List<IDEXX_Domain_Values__c> listDomVal = new List<IDEXX_Domain_Values__c>();
        IDEXX_Domain_Values__c domVal = new IDEXX_Domain_Values__c();
        domVal.COde__c='150';
        domVal.Description__c = 'PO Overnite AM - PREPAID';
        domVal.Name= 'Ship Method';
        listDomVal.add(domVal);
        IDEXX_Domain_Values__c domVal1 = new IDEXX_Domain_Values__c();
        domVal1.COde__c='350';
        domVal1.Description__c = 'PO Overnite AM - PREPAID';
        domVal1.Name= 'Ship Method';
        listDomVal.add(domVal1);
        INSERT listDomVal;
        Test.startTest();
        String CRON_EXP = '0 0 0 1 1 ? 2025';        
        String jobId = System.schedule('ScheduledApex', CRON_EXP, new ActivateDomainValues_Scheduler() );
        CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression); 
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2025-01-01 00:00:00', String.valueOf(ct.NextFireTime));
        Test.stopTest();
        
    }
    @isTest
    private static void testDailyDomainValuesClass(){
        DailyDomainValues.getFromCacheIfChanged test1 = new DailyDomainValues.getFromCacheIfChanged();
        DailyDomainValues.getCacheNamesResponse test2 = new DailyDomainValues.getCacheNamesResponse();
        DailyDomainValues.getKeys test3 = new DailyDomainValues.getKeys();
        DailyDomainValues.getFromCache test4 = new DailyDomainValues.getFromCache();
        DailyDomainValues.getFromCacheIfChangedResponse test5 = new DailyDomainValues.getFromCacheIfChangedResponse();
        DailyDomainValues.cacheReturn test6 = new DailyDomainValues.cacheReturn();
        DailyDomainValues.removeFromCache test7 = new DailyDomainValues.removeFromCache();
        DailyDomainValues.putToCacheResponse test8 = new DailyDomainValues.putToCacheResponse();
        DailyDomainValues.getExpirationInformation test9 = new DailyDomainValues.getExpirationInformation();
        DailyDomainValues.expirationInformation test10 = new DailyDomainValues.expirationInformation();
        DailyDomainValues.getExpirationInformationResponse test11 = new DailyDomainValues.getExpirationInformationResponse();
        DailyDomainValues.getKeysResponse test12 = new DailyDomainValues.getKeysResponse();
        DailyDomainValues.getCacheNames test13 = new DailyDomainValues.getCacheNames();
        DailyDomainValues.putToCache test14 = new DailyDomainValues.putToCache();
        DailyDomainValues.removeFromCacheResponse test15 = new DailyDomainValues.removeFromCacheResponse();
        DailyDomainValues.getFromCacheResponse test16 = new DailyDomainValues.getFromCacheResponse();
        DailyDomainValues.SALES_AREAS_element test17 = new DailyDomainValues.SALES_AREAS_element();
        DailyDomainValues.CUSTOMER_SIZE_element test18 = new DailyDomainValues.CUSTOMER_SIZE_element();
        DailyDomainValues.PRACTICE_SPECIALITY_element test19 = new DailyDomainValues.PRACTICE_SPECIALITY_element();
        DailyDomainValues.FAIL_element test20 = new DailyDomainValues.FAIL_element();
        DailyDomainValues.SHIP_POINTS_element test21 = new DailyDomainValues.SHIP_POINTS_element();
        DailyDomainValues.CREDIT_element test22 = new DailyDomainValues.CREDIT_element();
        DailyDomainValues.CUSTOMER_CLASS_element test23 = new DailyDomainValues.CUSTOMER_CLASS_element();
        DailyDomainValues.CUSTOMER_GROUP_element test24 = new DailyDomainValues.CUSTOMER_GROUP_element();
        DailyDomainValues.CUSTOMER_DATA_element test25 = new DailyDomainValues.CUSTOMER_DATA_element();
        DailyDomainValues.REASONS_element test26 = new DailyDomainValues.REASONS_element();
        DailyDomainValues.SOFTWARE_VERSIONS_element test27 = new DailyDomainValues.SOFTWARE_VERSIONS_element();
        DailyDomainValues.INCO_TERMS_element test28 = new DailyDomainValues.INCO_TERMS_element();
        DailyDomainValues.ORDER_REASONS_element test29 = new DailyDomainValues.ORDER_REASONS_element();
        DailyDomainValues.FREE_GOODS_element test30 = new DailyDomainValues.FREE_GOODS_element();
        DailyDomainValues.ACCOUNT_GROUP_element test31 = new DailyDomainValues.ACCOUNT_GROUP_element();
        DailyDomainValues.REPS_element test32 = new DailyDomainValues.REPS_element();
        DailyDomainValues.DELIVERY_BLOCK_element test33 = new DailyDomainValues.DELIVERY_BLOCK_element();
        DailyDomainValues.BILLING_BLOCK_element test34 = new DailyDomainValues.BILLING_BLOCK_element();
        DailyDomainValues.CUSTOMER_SPECIALITY_element test35 = new DailyDomainValues.CUSTOMER_SPECIALITY_element();
        DailyDomainValues.TIME_ZONE_element test36 = new DailyDomainValues.TIME_ZONE_element();
        DailyDomainValues.SWAP_element test37 = new DailyDomainValues.SWAP_element();
        DailyDomainValues.DT_DailyDomains_Response test38 = new DailyDomainValues.DT_DailyDomains_Response();
        DailyDomainValues.PLANTS_element test39 = new DailyDomainValues.PLANTS_element();
        DailyDomainValues.SALESACTIVITY_REASONS_element test40 = new DailyDomainValues.SALESACTIVITY_REASONS_element();
        DailyDomainValues.SALESACTIVITY_KITS_element test41 = new DailyDomainValues.SALESACTIVITY_KITS_element();
        DailyDomainValues.TASK_CODES_element test42 = new DailyDomainValues.TASK_CODES_element();
        DailyDomainValues.SALESACTIVITY_OUTCOME_element test43 = new DailyDomainValues.SALESACTIVITY_OUTCOME_element();
        DailyDomainValues.PAYMENT_TERMS_element test44 = new DailyDomainValues.PAYMENT_TERMS_element();
        DailyDomainValues.SALESACTIVITY_STATUS_element test45 = new DailyDomainValues.SALESACTIVITY_STATUS_element();
        DailyDomainValues.CODE_GROUPS_element test46 = new DailyDomainValues.CODE_GROUPS_element();
        DailyDomainValues.LABS_DISCOUNTS_element test47 = new DailyDomainValues.LABS_DISCOUNTS_element();
        DailyDomainValues.OBJ_TYPE_element test48 = new DailyDomainValues.OBJ_TYPE_element();
        DailyDomainValues.LANGUAGE_element test49 = new DailyDomainValues.LANGUAGE_element();
        DailyDomainValues.SALES_ACTIVITY_element test50 = new DailyDomainValues.SALES_ACTIVITY_element();
        DailyDomainValues.BUYING_GROUP_element test51 = new DailyDomainValues.BUYING_GROUP_element();
        DailyDomainValues.REGIONS_element test52 = new DailyDomainValues.REGIONS_element();
        DailyDomainValues.COST_CENTERS_element test53 = new DailyDomainValues.COST_CENTERS_element();
        DailyDomainValues.OUTCOME_ANALYSIS_element test54 = new DailyDomainValues.OUTCOME_ANALYSIS_element();
        DailyDomainValues.CUSTOMER_SEGMENT_element test55 = new DailyDomainValues.CUSTOMER_SEGMENT_element();
        DailyDomainValues.SALES_INFO_element test56 = new DailyDomainValues.SALES_INFO_element();
        DailyDomainValues.DT_WeeklyDomains_Response test57 = new DailyDomainValues.DT_WeeklyDomains_Response();
        DailyDomainValues.DT_DailyDomains_Request test58 = new DailyDomainValues.DT_DailyDomains_Request();
        DailyDomainValues.DT_WeeklyDomains_Request test59 = new DailyDomainValues.DT_WeeklyDomains_Request();
        DailyDomainValues.CURRENCY_element test60 = new DailyDomainValues.CURRENCY_element();
        
    }
    @isTest
    private static void testWeeklyDomainValuesClass(){
        WeeklyDomainValues.getFromCacheIfChanged test1 = new WeeklyDomainValues.getFromCacheIfChanged();
        WeeklyDomainValues.getCacheNamesResponse test2 = new WeeklyDomainValues.getCacheNamesResponse();
        WeeklyDomainValues.getKeys test3 = new WeeklyDomainValues.getKeys();
        WeeklyDomainValues.getFromCache test4 = new WeeklyDomainValues.getFromCache();
        WeeklyDomainValues.getFromCacheIfChangedResponse test5 = new WeeklyDomainValues.getFromCacheIfChangedResponse();
        WeeklyDomainValues.cacheReturn test6 = new WeeklyDomainValues.cacheReturn();
        WeeklyDomainValues.removeFromCache test7 = new WeeklyDomainValues.removeFromCache();
        WeeklyDomainValues.putToCacheResponse test8 = new WeeklyDomainValues.putToCacheResponse();
        WeeklyDomainValues.getExpirationInformation test9 = new WeeklyDomainValues.getExpirationInformation();
        WeeklyDomainValues.expirationInformation test10 = new WeeklyDomainValues.expirationInformation();
        WeeklyDomainValues.getExpirationInformationResponse test11 = new WeeklyDomainValues.getExpirationInformationResponse();
        WeeklyDomainValues.getKeysResponse test12 = new WeeklyDomainValues.getKeysResponse();
        WeeklyDomainValues.getCacheNames test13 = new WeeklyDomainValues.getCacheNames();
        WeeklyDomainValues.putToCache test14 = new WeeklyDomainValues.putToCache();
        WeeklyDomainValues.removeFromCacheResponse test15 = new WeeklyDomainValues.removeFromCacheResponse();
        WeeklyDomainValues.getFromCacheResponse test16 = new WeeklyDomainValues.getFromCacheResponse();
        WeeklyDomainValues.CUSTOMER_SIZE_element test17 = new WeeklyDomainValues.CUSTOMER_SIZE_element();
        WeeklyDomainValues.PRACTICE_SPECIALITY_element test18 = new WeeklyDomainValues.PRACTICE_SPECIALITY_element();
        WeeklyDomainValues.FAIL_element test19 = new WeeklyDomainValues.FAIL_element();
        WeeklyDomainValues.SHIP_POINTS_element test20 = new WeeklyDomainValues.SHIP_POINTS_element();
        WeeklyDomainValues.CREDIT_element test21 = new WeeklyDomainValues.CREDIT_element();
        WeeklyDomainValues.CUSTOMER_CLASS_element test22 = new WeeklyDomainValues.CUSTOMER_CLASS_element();
        WeeklyDomainValues.CUSTOMER_GROUP_element test23= new WeeklyDomainValues.CUSTOMER_GROUP_element();
        WeeklyDomainValues.CUSTOMER_DATA_element test24 = new WeeklyDomainValues.CUSTOMER_DATA_element();
        WeeklyDomainValues.SOFTWARE_VERSIONS_element test25 = new WeeklyDomainValues.SOFTWARE_VERSIONS_element();
        WeeklyDomainValues.ACCOUNT_GROUP_element test26 = new WeeklyDomainValues.ACCOUNT_GROUP_element();
        WeeklyDomainValues.BILLING_BLOCK_element test27=new WeeklyDomainValues.BILLING_BLOCK_element();
        WeeklyDomainValues.CUSTOMER_SPECIALITY_element test28= new WeeklyDomainValues.CUSTOMER_SPECIALITY_element();
        WeeklyDomainValues.TIME_ZONE_element test29 = new WeeklyDomainValues.TIME_ZONE_element();
        WeeklyDomainValues.SWAP_element test30 = new WeeklyDomainValues.SWAP_element();
        WeeklyDomainValues.CURRENCY_element test31 = new WeeklyDomainValues.CURRENCY_element();
        WeeklyDomainValues.PLANTS_element test32 = new WeeklyDomainValues.PLANTS_element();
        WeeklyDomainValues.SALESACTIVITY_REASONS_element test33 = new WeeklyDomainValues.SALESACTIVITY_REASONS_element();
        WeeklyDomainValues.TASK_CODES_element test34 = new WeeklyDomainValues.TASK_CODES_element();
        WeeklyDomainValues.SALESACTIVITY_OUTCOME_element test35 = new WeeklyDomainValues.SALESACTIVITY_OUTCOME_element();
        WeeklyDomainValues.PAYMENT_TERMS_element test36 = new WeeklyDomainValues.PAYMENT_TERMS_element();
        WeeklyDomainValues.SALESACTIVITY_STATUS_element test37 = new WeeklyDomainValues.SALESACTIVITY_STATUS_element();
        WeeklyDomainValues.CODE_GROUPS_element test38 = new WeeklyDomainValues.CODE_GROUPS_element();
        WeeklyDomainValues.LABS_DISCOUNTS_element test39 = new WeeklyDomainValues.LABS_DISCOUNTS_element();
        WeeklyDomainValues.OBJ_TYPE_element test40 = new WeeklyDomainValues.OBJ_TYPE_element();
        WeeklyDomainValues.LANGUAGE_element test41 = new WeeklyDomainValues.LANGUAGE_element();
        WeeklyDomainValues.SALES_ACTIVITY_element test42 = new WeeklyDomainValues.SALES_ACTIVITY_element();
        WeeklyDomainValues.BUYING_GROUP_element test43 = new WeeklyDomainValues.BUYING_GROUP_element();
        WeeklyDomainValues.REGIONS_element test44 = new WeeklyDomainValues.REGIONS_element();
        WeeklyDomainValues.OUTCOME_ANALYSIS_element test45 = new WeeklyDomainValues.OUTCOME_ANALYSIS_element();
        WeeklyDomainValues.CUSTOMER_SEGMENT_element test46 = new WeeklyDomainValues.CUSTOMER_SEGMENT_element();
        WeeklyDomainValues.CODE_GROUPS_element test47 = new WeeklyDomainValues.CODE_GROUPS_element();
        WeeklyDomainValues.CODE_GROUPS_element test48 = new WeeklyDomainValues.CODE_GROUPS_element();
        WeeklyDomainValues.CODE_GROUPS_element test49 = new WeeklyDomainValues.CODE_GROUPS_element();
        WeeklyDomainValues.SALES_AREAS_element test50 = new WeeklyDomainValues.SALES_AREAS_element();
        WeeklyDomainValues.REASONS_element test51 = new WeeklyDomainValues.REASONS_element();
        WeeklyDomainValues.FREE_GOODS_element test52 = new WeeklyDomainValues.FREE_GOODS_element();
        WeeklyDomainValues.REPS_element test53 = new WeeklyDomainValues.REPS_element();
        WeeklyDomainValues.DT_DailyDomains_Response test54 = new WeeklyDomainValues.DT_DailyDomains_Response();
        WeeklyDomainValues.SALESACTIVITY_KITS_element test55 = new WeeklyDomainValues.SALESACTIVITY_KITS_element();
        WeeklyDomainValues.COST_CENTERS_element test56 = new WeeklyDomainValues.COST_CENTERS_element();
        WeeklyDomainValues.DT_DailyDomains_Request test57 = new WeeklyDomainValues.DT_DailyDomains_Request();
        WeeklyDomainValues.DELIVERY_BLOCK_element test58 = new WeeklyDomainValues.DELIVERY_BLOCK_element();
        
    }
     /*@isTest
    private static void testSOAPLogin(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Test_LoginWebserviceCalloutMock());   
        Login.login('test@test.com','12345');
        Test.stopTest();
    }*/
}