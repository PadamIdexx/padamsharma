/* Class Name : Weekly_Domain_Value_Scheduler
 * Description : Scheduler Class to update Weekly Domain Values
 * Created By : Raushan Anand
 * Created On : 10-Nov-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand              10-Nov-2015               Initial version.
 **************************************************************************************/
global class Weekly_Domain_Value_Scheduler implements Schedulable{
    global void execute(SchedulableContext SC) {
        UpdateDomainValueService.callWeeklyDomainService();
    }
}