/* Trigger Name  : AccountTrigger
 * Description   : Apex class to update currency based on Billing Country
 * Created By    : Raushan Anand
 * Created On    : 21-Oct-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand              21-Oct-2015               Initial version.
 *  * Hemanth Vanapalli          25-Nov-2015               US-3179. onAfterInsert()
 *  * Pooja Wade                                                              the record type name was changed and hence had to maek a quick fix.
 * * Pooja Wade                        21-Dec-2015            For TKT - 243
 *****************************************************************************************/
public with sharing class AccountTriggerHandler {
    private final string CAG_PROSPECT_RECTYPE = Label.RT_CAGProspect;
    public void onAfterInsert(List<Account> newAccounts, Map<Id,Account> newAccountsMap) {
        String CAG_PROSPECT_RECTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CAG_PROSPECT_RECTYPE).getRecordTypeId();
        List<Account> cagAccountsToConvert = new List<Account>();
        for(Account thisAccount : newAccounts) {
            if( CAG_PROSPECT_RECTYPEID.equals(thisAccount.RecordTypeId)) {
                cagAccountsToConvert.add(thisAccount);
            }
        } 
        if(cagAccountsToConvert.size() > 0 ) {
            convertCAGAccountsToBSSCases(cagAccountsToConvert);
        } 
        createPrescriptiveWorkOrdersFromRanking(newAccounts, null);      
    }

    public void onAfterUpdate(List<Account> listOfNewAccounts, Map<Id, Account> mapOldAccounts){
        createPrescriptiveWorkOrdersFromRanking(listOfNewAccounts, mapOldAccounts);
    }

    public void onBeforeInsert(List<Account> listOfNewAccounts){
        manageOperatingHours(listOfNewAccounts, null);
    }

    public void onBeforeUpdate(List<Account> listOfNewAccounts, Map<Id, Account> mapOldAccounts){
        manageOperatingHours(listOfNewAccounts, mapOldAccounts);
    }

    private void convertCAGAccountsToBSSCases(List<Account> cagAccountsToConvert) {
        string BSS_CASE_RECTYPE = Label.RT_BSSCase;
        String BSS_CASE_RECTYPEID = Schema.SObjectType.Case.getRecordTypeInfosByName().get(BSS_CASE_RECTYPE).getRecordTypeId();
        String BSS_CASE_QUEUEID = [Select QueueId,Queue.Name From QueueSobject WHERE Queue.Name=:Label.P_S_Account_Conversion_Queue limit 1].QueueId; //BSS_CASE_RECTYPE 
        List<Case> bssCasesToInsert = new List<Case>();
        
        // fetch the assignment rule
        Database.DMLOptions dmo = new Database.DMLOptions();
        List<AssignmentRule> bssRule = [Select Id From AssignmentRule WHERE Name = 'LPD Case Assignment Rule'];
        if(!bssRule.isEmpty()){
            dmo.assignmentRuleHeader.assignmentRuleId= bssRule[0].Id;
            dmo.EmailHeader.triggerUserEmail = true;
        }
        for(Account eachCAGAccount : cagAccountsToConvert) {
            Case bssCase = new Case(AccountId=eachCAGAccount.Id,RecordTypeId=BSS_CASE_RECTYPEID,Status='New',Priority='Medium',Subject='Please Create ' + eachCAGAccount.Name + ' in SAP'/*,OwnerId=BSS_CASE_QUEUEID*/);
            bssCase.setOptions(dmo);
            System.debug('@@My New Case' + bssCase);
            bssCasesToInsert.add(bssCase);
        }
        if(bssCasesToInsert.size()>0 && Schema.sObjectType.Case.isCreateable()) {
            //database.insert (bssCasesToInsert, dmlOpts);
            insert bssCasesToInsert;
        }
    }
    public static void currencyUpdateonAccountInsert(List<Account> accList){
    
     
    
        Map<String, String> regionCurrencyMap = new Map<String,String>();
        // Query for Master label and currency ISO Code from region currency custom metedata type
        List<Region_Currency_Mapping__mdt> currencyMapList= [SELECT MasterLabel,Currency_ISO_Code__c FROM Region_Currency_Mapping__mdt WHERE Currency_ISO_Code__c!=null];
        // Ensure that the map is not empty
        if(!currencyMapList.isEmpty()){
            for(Region_Currency_Mapping__mdt oCurr : currencyMapList){
                regionCurrencyMap.put(oCurr.MasterLabel.ToUpperCase(),oCurr.Currency_ISO_Code__c);
            }
        }
        if(!accList.isEmpty()){
        
            for(Account acc : accList){ 
   

               
                // Ensure that billing country code is not equal to null      
                if(acc.BillingCountryCode != null){
                    // The billing country code that we assign on the account should be present in the region currency mapping in the custom metadata type
                    if(regionCurrencyMap.containsKey(acc.BillingCountryCode.ToUpperCase())) {
                        if(acc.CurrencyIsoCode != regionCurrencyMap.get(acc.BillingCountryCode.ToUpperCase())){
                            acc.CurrencyIsoCode = regionCurrencyMap.get(acc.BillingCountryCode.ToUpperCase()); 
                        }
                    }
                    else{
                        acc.CurrencyIsoCode = 'USD';
                    }
                }
                // If billing country code that we assign on the account is not present in the region currency, default the currency to USD
                else{
                        acc.CurrencyIsoCode = 'USD';
                }
            }
        }
    }
    
    public static void currencyUpdateonAccountUpdate(List<Account> accList,Map<Id,Account> accOldMap){
        Map<String, String> regionCurrencyMap = new Map<String,String>();
        // Query for Master label and currency ISO Code from region currency custom metedata type
        List<Region_Currency_Mapping__mdt> currencyMapList= [SELECT MasterLabel,Currency_ISO_Code__c FROM Region_Currency_Mapping__mdt WHERE Currency_ISO_Code__c!=null];
        // Ensure that the map is not empty
        if(!currencyMapList.isEmpty()){
            for(Region_Currency_Mapping__mdt oCurr : currencyMapList){
                regionCurrencyMap.put(oCurr.MasterLabel.ToUpperCase(),oCurr.Currency_ISO_Code__c);
            }
        }
        if(!accList.isEmpty()){
            for(Account acc : accList){    
                if(acc.CurrencyIsoCode == accOldMap.get(acc.Id).CurrencyIsoCode){
                    // Ensure that billing country code is not equal to null      
                    if(acc.BillingCountryCode != null){
                        // The billing country code that we assign on the account should be present in the region currency mapping in the custom metadata type
                        if(regionCurrencyMap.containsKey(acc.BillingCountryCode.ToUpperCase())) {
                            if(acc.CurrencyIsoCode != regionCurrencyMap.get(acc.BillingCountryCode.ToUpperCase())){
                                acc.CurrencyIsoCode = regionCurrencyMap.get(acc.BillingCountryCode.ToUpperCase()); 
                            }
                        }
                        else{
                            acc.CurrencyIsoCode = 'USD';
                        }
                    }
                   
                    // If billing country code that we assign on the account is not present in the region currency, default the currency to USD
                    else{
                            acc.CurrencyIsoCode = 'USD';
                    }
                }
            }
        }
    }
    // static variable to determine if geocoding has already occurred
    private static Boolean geocodingCalled = false;
    // wrapper method to prevent calling future methods from an existing future context
    public static void DoAddressGeocode(Id accountId) {
        if ( geocodingCalled || System.isFuture() || System.isBatch() ) 
        {
            System.debug(LoggingLevel.WARN, '***Address Geocoding Future Method Already Called - Aborting...');
            return;
        }
        // if not being called from future context, geocode the address
        geocodingCalled = true;
        geocodeAddress(accountId);
    }
    public static boolean runGeocoding = true;
    
    // we need a future method to call Google Geocoding API from Salesforce
    @future (callout=true)
    static private void geocodeAddress(id accountId) {
        if(runGeocoding)
        {
            // Key for Google Maps Geocoding API
            String geocodingKey = 'AIzaSyA_mcRUJHc7zlKH_z6kZNtEr8i57Qc5XDg';
            // get the passed in address
            
            List<Account> ListgeoAccount = [SELECT ShippingStreet, ShippingCity, ShippingState, ShippingCountry,
                                  ShippingPostalCode
                                  FROM
                                  Account
                                  WHERE
                                  id = :accountId];
    
            if(ListgeoAccount.size() >0 )
            {
                Account geoAccount = ListgeoAccount[0];
                
                // check that we have enough information to geocode the address
                if ((geoAccount.ShippingStreet == null) || (geoAccount.ShippingCity == null)) 
                {
                    System.debug(LoggingLevel.WARN, 'Insufficient Data to Geocode Address');
                    return;
                }
                // create a string for the address to pass to Google Geocoding API
                String geoAddress = Utils.formatAddress(geoAccount.ShippingStreet,
                                                        geoAccount.ShippingCity,
                                                        geoAccount.ShippingState,
                                                        geoAccount.ShippingCountry,
                                                        geoAccount.ShippingPostalCode);
            
                // encode the string so we can pass it as part of URL
                geoAddress = EncodingUtil.urlEncode(geoAddress, 'UTF-8');
                // build and make the callout to the Geocoding API
                Http http = new Http();
                HttpRequest request = new HttpRequest();
                request.setEndpoint('https://maps.googleapis.com/maps/api/geocode/json?address='
                                    + geoAddress + '&key=' + geocodingKey
                                    + '&sensor=false');
                request.setMethod('GET');
                request.setTimeout(60000);
                try 
                {
                    // make the http callout
                    HttpResponse response = http.send(request);
                    System.debug('response.body='+response.getBody());
                    // parse JSON to extract co-ordinates
                    JSONParser responseParser = JSON.createParser(response.getBody());
                    // initialize co-ordinates
                    double latitude = null;
                    double longitude = null;
                    while (responseParser.nextToken() != null) 
                    {
                        if ((responseParser.getCurrentToken() == JSONToken.FIELD_NAME)&& (responseParser.getText() == 'location')) 
                        {
                            responseParser.nextToken();
                            while (responseParser.nextToken() != JSONToken.END_OBJECT) 
                            {
                                String locationText = responseParser.getText();
                                responseParser.nextToken();
                                if (locationText == 'lat') 
                                {
                                    latitude = responseParser.getDoubleValue();
                                } 
                                else if (locationText == 'lng') 
                                {
                                    longitude = responseParser.getDoubleValue();
                                }
                            }
                        }
                    }
                    // update co-ordinates on address if we get them back
                    if (latitude != null) 
                    {
                        geoAccount.Location__Latitude__s = latitude;
                        geoAccount.Location__Longitude__s = longitude;
                        update geoAccount;
                    }
                }
                catch (Exception e) 
                {
                    System.debug(LoggingLevel.ERROR,
                                 'Error Geocoding Address - ' + e.getMessage());
                }
            }   
        }
    }

    public void createPrescriptiveWorkOrdersFromRanking(List<Account> listOfNewAccounts, Map<Id, Account> mapOfOldAccounts){
        if(!WFM_Constants.ENABLE_WORK_ORDERS && !Test.isRunningTest())
            return;
            
        // Collect accounts with updated Ranking ie FSR_Ranking_Last_Update is changed and among the top 100
        Map<Id, Account> mapOfUpdatedAccounts = new Map<Id, Account>();
        List<Id> listAccountIds = new List<Id>();

        for(Account account : listOfNewAccounts){
            if(account.FSR_Ranking__c <= WFM_Constants.ACCOUNT_MIN_RANKING_FOR_PRESCRIPTIVE){
                listAccountIds.add(account.id);
                // If the account is being inserted, automatically add it to the list of changed accounts
                if(mapOfOldAccounts == null)
                    mapOfUpdatedAccounts.put(account.Id, account);
                else if(account.FSR_Ranking_Last_Update__c != mapOfOldAccounts.get(account.Id).FSR_Ranking_Last_Update__c){
                    mapOfUpdatedAccounts.put(account.Id, account);
                }
            }
        }

        // Retrieve work orders for the current semester for the changed accounts
        Datetime startOfCurrentSemester = WFM_Utils.getStartCurrentSemester();
        Datetime endOfCurrentSemester = WFM_Utils.getEndCurrentSemester();

        Map<Id, List<WorkOrder>> existingPrescriptiveWorkOrdersByAccount = new Map<Id, List<WorkOrder>>();
        for(WorkOrder workOrder : [
                SELECT      id, StartDate, EndDate, RecordTypeId, AccountId, Due_Date__c 
                FROM        WorkOrder
                WHERE       RecordTypeId = :WFM_Constants.WORK_ORDER_PRESCRIPTIVE_RECORDTYPE_ID
                            AND
                            AccountId IN :mapOfUpdatedAccounts.keySet()
                            AND Due_Date__c >= :startOfCurrentSemester
                            AND Due_Date__c <= :endOfCurrentSemester
                ORDER BY    Due_Date__c

            ]){
            if(existingPrescriptiveWorkOrdersByAccount.containsKey(workOrder.AccountId))
                existingPrescriptiveWorkOrdersByAccount.get(workOrder.AccountId).add(workOrder);
            else
                existingPrescriptiveWorkOrdersByAccount.put(workOrder.AccountId, new List<WorkOrder>{workOrder});
        }
    
   


        // Create work orders when missing
        List<WorkOrder> listPrescriptiveWorkOrdersToCreate = new List<WorkOrder>();
        Map<Id, List<Id>> mapFSRIdByAccountId = WFM_Utils.retrieveAssociatedFSRs(listAccountIds, new List<String>{WFM_Constants.USER_TERRITORY_ROLE_FSR_DX});
        for(Account account : mapOfUpdatedAccounts.values()){
            Id fsrUserId;
            if(mapFSRIdByAccountId.containsKey(account.Id))
                fsrUserId = mapFSRIdByAccountId.get(account.Id).get(0);
                system.debug ('FSR ID' + fsrUserId );

            if(account.FSR_Ranking__c > 0 && account.FSR_Ranking__c <= 15)
                listPrescriptiveWorkOrdersToCreate.addAll(WFM_Utils.getWorkOrdersForTop15(account, existingPrescriptiveWorkOrdersByAccount.get(account.Id), fsrUserId));
            else if(account.FSR_Ranking__c > 15 && account.FSR_Ranking__c <= WFM_Constants.ACCOUNT_MIN_RANKING_FOR_PRESCRIPTIVE){
                listPrescriptiveWorkOrdersToCreate.addAll(WFM_Utils.getWorkOrdersForTop100(account, existingPrescriptiveWorkOrdersByAccount.get(account.Id), fsrUserId));
            }
        }
        insert listPrescriptiveWorkOrdersToCreate;
    }

    public void manageOperatingHours(List<Account> listNewAccounts, Map<Id, Account> mapOldAccounts){
        if(!WFM_Constants.ENABLE_WORK_ORDERS && !Test.isRunningTest())
            return;
        WFM_Utils.createOperatingHoursForAccounts(listNewAccounts, mapOldAccounts);
    }
}