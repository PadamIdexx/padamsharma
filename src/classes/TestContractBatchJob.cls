@isTest
private class TestContractBatchJob {

    static testmethod void test() {
    Account acc =  new Account();       
        acc.Name = 'Cedarburg';          
        acc.ShippingStreet = '1 Idexx Dr';
        Insert acc;
        // The query used by the batch job.
        String query = 'SELECT Id,Name,Status,Contract_End_Date__c,Account FROM Contract where Contract_End_Date__c != null AND Contract_End_Date__c < TODAY()';


       Contract[] cont = new List<Contract>();
       for (Integer i=0;i<10;i++) {
           Contract m = new Contract(
               Name='Contract ' + i,
               Contract_End_Date__c = Date.today()-1,
               Status = 'Inactive',
              AccountID = acc.id
               
             );
           cont.add(m);
       }
       insert cont;

Test.startTest();
        ContractBatchJob con = new  ContractBatchJob();
        Database.executeBatch(con);
Test.stopTest();

         }
}