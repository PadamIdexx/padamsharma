@isTest
public class TestVetConnectplusIntegrationBasic{
    static testMethod void testVetConnect() {
        String vetJson = '{"sapId" : "12345", "active" : true, "activeUserCount" : 6, "relatedSapIds" : []}';
        Account acc1 = new Account(Name='TestVetConnectAccount', Phone='2222222222', SAP_Customer_Number__c ='12345');
        Account acc2 = new Account(Name='TestVetConnectAccount', Phone='2222222222');
        
        Test.startTest();
            insert acc1;
            insert acc2;
        Test.stopTest();
        
        //system.assertEquals(acc1.Id, acc1.Id);
        //system.assertEquals(acc2.Id, acc2.Id);
        
        pageRefMethod(acc1);
        pageRefMethod(acc2);
        
        VetconnectJsonResponse vetJsonResp = new VetconnectJsonResponse();
        vetJsonResp = VetconnectJsonResponse.parse(vetJson);
    }
    
    public static void pageRefMethod(Account acc) {
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        vetConnectplusIntegrationBasic vcplus = new vetConnectplusIntegrationBasic(sc);
        vcplus.onload();        
    }
}