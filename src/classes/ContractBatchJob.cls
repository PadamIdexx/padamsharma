global class ContractBatchJob implements Database.Batchable<sObject> 
{
date d = system.today().addDays(-1);
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
        String query = 'SELECT Id,Name,Status,Contract_End_Date__c FROM Contract where Contract_End_Date__c = :d ' ;
        
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Contract> scope) 
    {
        for(Contract a : scope)
         {
           a.Status= 'Inactive';
         }

        update scope;
    }
    global void finish(Database.BatchableContext BC) {
    }
    
}