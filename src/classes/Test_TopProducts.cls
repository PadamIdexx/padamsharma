@isTest
private class Test_TopProducts {

	static testMethod void testRetrieveTopProductsForAccount() {         
       	User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
       	system.runAs(currentUser){

        	Account thisAccount = new Account();
            thisAccount = CreateTestClassData.createAccountTypeWithCurrency(Label.RT_Customer,Label.USD);

            Order theOrder = CreateTestClassData.createReusableOrder(thisAccount);
            
            Test.startTest();
            TopProductSelector s = new TopProductSelector();
                
            Integer topRows = Integer.valueOf(Label.CAG_Top_N_Products_Value);
                
            List<TopProduct> products = s.retrieveTopProductsForAccount(thisAccount.id, topRows);

            System.debug(products);
            
            for (TopProduct p : products) {
            	System.debug(p);
            }
            Test.stopTest();
    	}
    }

    /*
     * Since this test method ultimately attempts to access a Custom Metadata Type via a SOQL query, we need
     * SeeAllData.
     */ 
    @isTest(SeeAllData=true)
    static void testRetrieveDefaultTopProducts() {         
       	User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
       	system.runAs(currentUser){
                
            Test.startTest();
            TopProductSelector s = new TopProductSelector();
                
            Integer topRows = Integer.valueOf(Label.CAG_Top_N_Products_Value);
                
            List<TopProduct> defaults = s.retrieveDefaultTopProducts('CAG', 'USD', topRows);
            
            System.debug(defaults);
            
            for (TopProduct p : defaults) {
                System.debug(p);
            }
            Test.stopTest();
    	}
    }
    
    static testMethod void testTopProduct() {
        
        TopProduct p = new TopProduct();

        System.assert(p.qty == 1);
    }

}