/*
 * SeeAllData is set on purpose because we are testing custom settings.
*/ 
@isTest(SeeAllData=true)
public class Test_SAPServiceDataHelper {

    @isTest
    static void stringOfLength() {
        Test.startTest();
        
        String testString = '1234567890';
        
        testString = SAPServiceDataHelper.getStringOfLength(testString, 5);
        
        System.assert(testString.length() == 5);
        
        Test.stopTest();
    }
        
    
    @isTest
    static void stringOfLengthMaxToLong() {
        Test.startTest();
        
        String testString = '1234567890';
        
        testString = SAPServiceDataHelper.getStringOfLength(testString, 15);
        
        System.assert(testString.length() == 10);
        
        Test.stopTest();
    }

    @isTest
    static void stringOfLengthNoStringPassed() {
        Test.startTest();
        
        String testString = SAPServiceDataHelper.getStringOfLength(null, 15);
        
        System.assertEquals(testString, null);
        
        Test.stopTest();
    }
    
    @isTest
    static void datePositive() {
        Test.startTest();

        Date d = Date.newInstance(2016, 7, 8);
        
        String testString = SAPServiceDataHelper.getSAPDate(d);
        
        System.assertNotEquals(testString, null);
        
        Test.stopTest();        
    }

    @isTest
    static void dateNegative() {
        Test.startTest();

        String testString = SAPServiceDataHelper.getSAPDate(null);
        
        System.assertEquals(testString, null);
        
        Test.stopTest();        
    }


    @isTest
    static void dateForSAPDateStringPositive() {
        Test.startTest();

        Date d = Date.newInstance(2016, 7, 8);
        String dString = String.valueOf(d);
        
        Date testD = SAPServiceDataHelper.getDateForSAPDate(dString);
        
        System.assertEquals(testD, Date.newInstance(2016, 7, 8));
        
        Test.stopTest();        
    }

    @isTest
    static void dateForSAPDateStringNegative() {
        Test.startTest();

        String d = null;
        
        Date testD = SAPServiceDataHelper.getDateForSAPDate(d);
        
        System.assertEquals(testD, null);
        
        Test.stopTest();       
    }

    
    @isTest
    static void sapIntegerPositive() {
        Test.startTest();

        Integer num = Integer.valueOf('999');
        
        String testNum = SAPServiceDataHelper.getSAPInteger(num);
        
        System.assertNotEquals(testNum, null);
        
        Test.stopTest();        
    }

    @isTest
    static void sapIntegerNegative() {
        Test.startTest();

        String testNum = SAPServiceDataHelper.getSAPInteger(null);
        
        System.assertEquals(testNum, null);
        
        Test.stopTest();        
    }

    @isTest
    static void sapDecimalPositive() {
        Test.startTest();

        Decimal num = Decimal.valueOf('999.00');
        
        String testNum = SAPServiceDataHelper.getSAPDecimal(num, 2);
        
        System.assertNotEquals(testNum, null);
        
        Test.stopTest();        
    }

    @isTest
    static void sapDecimalNegative() {
        Test.startTest();

		String testNum = SAPServiceDataHelper.getSAPDecimal(null, 2);
        System.assertEquals(testNum, null);
        
        Test.stopTest();        
    }


    @isTest
    static void decimalForSAPNumberPositive() {
        Test.startTest();

        String num = '999.00';
        
        Decimal testNum = SAPServiceDataHelper.getDecimalForSAPNumber(num, 2);
        
        System.assertEquals(testNum, Decimal.valueOf(num));
        
        Test.stopTest();        
    }    
    
    @isTest
    static void decimalForSAPNumberNegative() {
        Test.startTest();

        String num = null;
        
        Decimal testNum = SAPServiceDataHelper.getDecimalForSAPNumber(num, 2);
        
        System.assertEquals(testNum, 0.00);
        
        Test.stopTest();        
    }    
    

    
    @isTest
    static void sapIndicatorPositive() {
        Test.startTest();

        Boolean b = true;
        
        String testIndicator = SAPServiceDataHelper.getSAPIndicator(b);
        
        //3.16.2017 commented out as in PROD we don't yet have the custom setting/object yet.
        //System.assertNotEquals(testIndicator, null);
        
        Test.stopTest();        
    }

    @isTest
    static void sapIndicatorNegative() {
        Test.startTest();

        String testIndicator = SAPServiceDataHelper.getSAPIndicator(null);
        
        System.assertEquals(testIndicator, null);
        
        Test.stopTest();        
    }
    
    
    //3.16.2017 commented out as in PROD we don't yet have the custom setting/object yet.
    /*
    @isTest
    static void sapSAPIndicatorToBooleanPositiveTrue() {
        Test.startTest();

        Boolean testBoolean = SAPServiceDataHelper.getBooleanForSAPIndicator('X');
       
        System.assertEquals(testBoolean, true);
        
        Test.stopTest();        
    }

    @isTest
    static void sapSAPIndicatorToBooleanPositiveFalse() {
        Test.startTest();

        Boolean testBoolean = SAPServiceDataHelper.getBooleanForSAPIndicator('O');
        
        System.assertEquals(testBoolean, false);
        
        Test.stopTest();        
    }
    */
    
    @isTest
    static void sapSAPIndicatorToBooleanPositive() {
        Test.startTest();

        Boolean testBoolean = SAPServiceDataHelper.getBooleanForSAPIndicator(null);
        
        System.assertEquals(testBoolean, false);
        
        Test.stopTest();        
    }
    
    @isTest
    static void testGetSAPCodePositive() {
        Test.startTest();

        String trueIndicator = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.TRUE_INDICATOR);
        
        //3.16.2017 commented out as in PROD we don't yet have the custom setting/object yet.
        //System.assertEquals(trueIndicator, 'X');
        
        Test.stopTest(); 
    }

    @isTest
    static void testGetSAPCodeWithDefaultPositive() {
        Test.startTest();

        String trueIndicator = SAPServiceDataHelper.getSAPCode('TEST_VALUE', 'X');
        
        System.assertEquals(trueIndicator, 'X');
        
        Test.stopTest(); 
    }    
    
    @isTest
    static void testGetSAPCodeNegative() {
        Test.startTest();

        String trueIndicator = SAPServiceDataHelper.getSAPCode(null);
        
        System.assertEquals(trueIndicator, null);
        
        Test.stopTest(); 
    }    
    
    @isTest
    static void testFormatItemNumberPositive() {
        Test.startTest();
        
        String iNum = SAPServiceDataHelper.formatItemNumber(10);
        
        system.assertEquals(iNum, '000010');
        
        Test.stopTest();
    }
    
    @isTest
    static void testFormatItemNumberNegative() {
        Test.startTest();
        
        String iNum = SAPServiceDataHelper.formatItemNumber(null);
        
        system.assertEquals(iNum, '000000');
        
        Test.stopTest();
    }    
    
}