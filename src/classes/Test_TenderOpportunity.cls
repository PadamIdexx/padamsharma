/**************************************************************************************
 * Create By:   Deloitte Consulting LLP.
 * CreateDate:  03-Sep-2015
 * Description: This Class does below processing
 *              1. To provide code coverage for class "TenderOpportunity"
 *              
 *                 
 *  
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                 Created Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Hemanth Kumar V           03-Sep-2015                   Initial version.
      Aditya Sangawar           19-Feb-2016                   Added method to cover Start & Close Date Validation Rules.
 **************************************************************************************/
@isTest(seeAllData=false)
private class Test_TenderOpportunity {
    
    static testMethod void tenderUnitTest() {
        Opportunity testOpportunity = new Opportunity();
        Account testAccount = new Account();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = 'Test Account';
        testAccount.CurrencyIsoCode = 'USD';
        if(Schema.sObjectType.Account.isCreateable() && testAccount != null) {
          insert testAccount;     
          System.assert(testAccount.Id != null);
        }
        
        String tenderRecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_Annuitized).getRecordTypeId();
        String lpdRecType =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_LPD_Opportunity).getRecordTypeId();
        String negativeRecType= Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_NetativeWinBack).getRecordTypeId();
        System.assert(tenderRecType != null);
        System.assert(lpdRecType != null);
        System.assert(negativeRecType != null);
        Test.setCurrentPage(Page.TenderOpportunities);      
        system.currentPageReference().getParameters().put('RecordType', tenderRecType);    
        System.currentPageReference().getParameters().put('accid',testAccount.Id);
        System.currentPageReference().getParameters().put('opp16',testAccount.CurrencyIsoCode);
        Test.startTest();
        TenderOpportunity controller = new TenderOpportunity(new ApexPages.StandardController(testOpportunity));
        controller.getControllerRecordId();
        controller.getPeriodTypes();
        testOpportunity.Name = 'Test Opportunity';
        testOpportunity.Start_Date__c = System.today();
        testOpportunity.CloseDate = System.today().addMonths(10);
        testOpportunity.stageName = '1 Prospect Tender';
        testOpportunity.AccountId = testAccount.Id;
        testOpportunity.Amount_In__c = 10000.0;
        testOpportunity.Period_Type__c = 'Monthly';
        testOpportunity.Opportunity_Type__c = 'Annuitized Opportunity';
        testOpportunity.RecordTypeId = tenderRecType;
        controller.changeStageName();
        controller.syncChildTenders();
        testOpportunity.CloseDate = System.today().addMonths(22);
        controller.syncChildTenders();
        testOpportunity.CloseDate = System.today().addMonths(34);
        controller.syncChildTenders();
        testOpportunity.CloseDate = System.today().addMonths(46);
        controller.syncChildTenders();
        testOpportunity.CloseDate = System.today().addMonths(58);
        controller.syncChildTenders();
        testOpportunity.Period_Type__c = 'Quarterly';
        controller.syncChildTenders();
        controller.save();
        Test.stopTest();
    }
    
    static testMethod void tenderProductsUnitTest() {
        Opportunity testOpportunity = new Opportunity();
        Account testAccount = new Account();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = 'Test Account';
        testAccount.CurrencyIsoCode = 'USD';
        if(Schema.sObjectType.Account.isCreateable() && testAccount != null) {
            insert testAccount;     
            System.assert(testAccount.Id != null);
        }
        String tenderRecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_Annuitized).getRecordTypeId();
        String lpdRecType =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_LPD_Opportunity).getRecordTypeId();
        String negativeRecType= Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_NetativeWinBack).getRecordTypeId();
        Test.setCurrentPage(Page.TenderOpportunities);      
        system.currentPageReference().getParameters().put('RecordType', tenderRecType);    
        System.currentPageReference().getParameters().put('accid',testAccount.Id);
        System.currentPageReference().getParameters().put('opp16',testAccount.CurrencyIsoCode);
        TenderOpportunity controller = new TenderOpportunity(new ApexPages.StandardController(testOpportunity));
        controller.getControllerRecordId();
        controller.getPeriodTypes();
        testOpportunity.Name = 'Test Opportunity';
        testOpportunity.Start_Date__c = System.today();
        testOpportunity.CloseDate = System.today().addMonths(10);
        testOpportunity.stageName = '1 Prospect Tender';
        testOpportunity.AccountId = testAccount.Id;
        testOpportunity.Amount_In__c = 10000.0;
        testOpportunity.Opportunity_Type__c = 'Annuitized Opportunity';
        testOpportunity.Period_Type__c = 'Monthly';
        testOpportunity.RecordTypeId = tenderRecType;
        controller.changeStageName();
        controller.syncChildTenders();
        testOpportunity.CloseDate = System.today().addMonths(22);
        controller.syncChildTenders();
        testOpportunity.CloseDate = System.today().addMonths(34);
        controller.syncChildTenders();
        testOpportunity.CloseDate = System.today().addMonths(46);
        controller.syncChildTenders();
        testOpportunity.CloseDate = System.today().addMonths(58);
        controller.syncChildTenders();
        testOpportunity.Period_Type__c = 'Quarterly';
        controller.syncChildTenders();
        controller.saveAndAddProducts();
    }
    
    static testMethod void editTenderUnitTest() {
        Opportunity testOpportunity = new Opportunity();
        List<Opportunity> childOpportunities = new List<Opportunity>();
        Account testAccount = new Account();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = 'Test Account';
        testAccount.CurrencyIsoCode = 'USD';
        if(Schema.sObjectType.Account.isCreateable() && testAccount != null) {
            insert testAccount;     
            System.assert(testAccount.Id != null);
        }
        String tenderRecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_Annuitized).getRecordTypeId();
        String lpdRecType =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_LPD_Opportunity).getRecordTypeId();
        testOpportunity.Name = 'Test Opportunity';
        testOpportunity.Start_Date__c = System.today();
        testOpportunity.CloseDate = System.today().addMonths(10);
        testOpportunity.stageName = '1 Prospect Tender';
        testOpportunity.Probability = 25.0;
        testOpportunity.AccountId = testAccount.Id;
        testOpportunity.Amount_In__c = 10000.0;
        testOpportunity.Period_Type__c = 'Monthly';
        testOpportunity.Opportunity_Type__c = 'Annuitized Opportunity';
        testOpportunity.RecordTypeId = tenderRecType;
        if(Schema.sObjectType.Opportunity.isCreateable() && testOpportunity != null) {
            insert testOpportunity;
            System.assert(testOpportunity.Id != null);
        }
        testOpportunity.StageName = '2 Discovery Tender';
        testOpportunity.Probability = 50;
        if(Schema.sObjectType.Opportunity.isCreateable()) {
            childOpportunities = CreateTestClassData.insertChildOpportunities(testOpportunity,10);
        }
        Test.setCurrentPage(Page.TenderOpportunities);             
        TenderOpportunity controller = new TenderOpportunity(new ApexPages.StandardController(testOpportunity));
        childOpportunities.addAll(CreateTestClassData.insertChildOpportunities(testOpportunity,12));
        controller = new TenderOpportunity(new ApexPages.StandardController(testOpportunity));
        childOpportunities.addAll(CreateTestClassData.insertChildOpportunities(testOpportunity,12));
        controller = new TenderOpportunity(new ApexPages.StandardController(testOpportunity));
        childOpportunities.addAll(CreateTestClassData.insertChildOpportunities(testOpportunity,12));
        controller = new TenderOpportunity(new ApexPages.StandardController(testOpportunity));
        childOpportunities.addAll(CreateTestClassData.insertChildOpportunities(testOpportunity,12));
        controller = new TenderOpportunity(new ApexPages.StandardController(testOpportunity));
        controller.getControllerRecordId();
        for(Opportunity opp : controller.childOpportunities) {
            opp.Amount = (Double) testOpportunity.Amount_in__c /controller.childOpportunities.size(); 
        }
        if(Schema.sObjectType.Opportunity.isUpdateable()) {
            controller.updateRecords();
        }
    }
    
    static testMethod void lpdUnitTest() {
        Opportunity testOpportunity = CreateTestClassData.createLPDOpportunity(); 
        System.assert(testOpportunity.Id != null);
        Account testAccount = CreateTestClassData.createCustomerAccount();
        System.assert(testAccount.Id != null);
        String lpdRecType =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_LPD_Opportunity).getRecordTypeId();
        Test.setCurrentPage(Page.TenderOpportunities);
        system.currentPageReference().getParameters().put('RecordType', lpdRecType);    
        System.currentPageReference().getParameters().put('accid',testAccount.Id);
        System.currentPageReference().getParameters().put('opp16',testAccount.CurrencyIsoCode);
        testOpportunity.AccountId = testAccount.Id;
        TenderOpportunity controller = new TenderOpportunity(new ApexPages.StandardController(testOpportunity));
        controller.getControllerRecordId();
        controller.getPeriodTypes();
        controller.negativeWinBackSave();
        controller.isEdit = true;
        testOpportunity.StageName = '5 Closed Lost';
        testOpportunity.Probability = 0;
        controller.negativeWinBackSave();
    }
    static testMethod void negativeWinBackUnitTest() {
        Account testAccount = CreateTestClassData.createCustomerAccount();
        Opportunity testOpportunity = CreateTestClassData.createNegativeWinBackOpportunity();
        System.assert(testAccount.Id != null);
        System.assert(testOpportunity.Id != null);
        //testOpportunity.AccountId = testAccount.Id;
        TenderOpportunity controller = new TenderOpportunity(new ApexPages.StandardController(testOpportunity));
        controller.negativeWinBackSave();
    }
    //**********
        static testMethod void negativeWinBackUnitTest_InsertCondition() {
        Account testAccount = CreateTestClassData.createCustomerAccount();
        Opportunity testOpportunity = CreateTestClassData.createNegativeWinBackOpportunity();
        System.assert(testAccount.Id != null);
        System.assert(testOpportunity.Id != null);
        //testOpportunity.AccountId = testAccount.Id;
        TenderOpportunity controller = new TenderOpportunity(new ApexPages.StandardController(testOpportunity));
        controller.isEdit = false;
        controller.negativeWinBackSave();
    }
    static testMethod void negativeWinBackProductsUnitTest() {
        Account testAccount = CreateTestClassData.createCustomerAccount();
        Opportunity testOpportunity = CreateTestClassData.createNegativeWinBackOpportunity();
        System.assert(testAccount.Id != null);
        System.assert(testOpportunity.Id != null);
        //testOpportunity.AccountId = testAccount.Id;
        TenderOpportunity controller = new TenderOpportunity(new ApexPages.StandardController(testOpportunity));
        controller.negativeWinBackSaveAndAddProducts();     
    }
    static testMethod void validateTenderUnitTest() {
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_Annuitized).getRecordTypeId();
        Account testAccount = CreateTestClassData.createCustomerAccount();
        System.assert(testAccount.Id != null);        
        TenderOpportunity controller = new TenderOpportunity(new ApexPages.StandardController(testOpportunity));
        controller.validateParentOpportunity();     
        testOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_LPD_Opportunity).getRecordTypeId();
        controller.validateParentOpportunity();
        testOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_NetativeWinBack).getRecordTypeId();
        testOpportunity.Amount = 100;
        controller.validateParentOpportunity();
        testOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_Annuitized).getRecordTypeId();
        testOpportunity.Opportunity_Type__c = '';
        testOpportunity.Period_Type__c = '';
        testOpportunity.Amount_In__c = null;
        controller.validateParentOpportunity();
        testOpportunity.Start_Date__c = null;
        controller.validateParentOpportunity();
        
    }
    
    static testMethod void TestValidationErrorsforStartandCloseDate() {
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_Annuitized).getRecordTypeId();
        Account testAccount = CreateTestClassData.createCustomerAccount();
        System.assert(testAccount.Id != null);        
        TenderOpportunity controller = new TenderOpportunity(new ApexPages.StandardController(testOpportunity));
        controller.validateParentOpportunity();     
        testOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_LPD_Opportunity).getRecordTypeId();
        controller.validateParentOpportunity();
        testOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_NetativeWinBack).getRecordTypeId();
        testOpportunity.Amount = 100;
        controller.validateParentOpportunity();
        testOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_Annuitized).getRecordTypeId();
        testOpportunity.Opportunity_Type__c = '';
        testOpportunity.Period_Type__c = '';
        testOpportunity.Amount_In__c = null;
        controller.validateParentOpportunity();
        testOpportunity.Start_Date__c = System.today().addMonths(20);
        testOpportunity.CloseDate = System.today();
        controller.validateParentOpportunity();
        
    }
    
        static testMethod void TestValidationErrorsforStartandCloseDate2() {
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_Annuitized).getRecordTypeId();
        Account testAccount = CreateTestClassData.createCustomerAccount();
        System.assert(testAccount.Id != null);        
        TenderOpportunity controller = new TenderOpportunity(new ApexPages.StandardController(testOpportunity));
        controller.validateParentOpportunity();     
        testOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_LPD_Opportunity).getRecordTypeId();
        controller.validateParentOpportunity();
        testOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_NetativeWinBack).getRecordTypeId();
        testOpportunity.Amount = 100;
        controller.validateParentOpportunity();
        testOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_Annuitized).getRecordTypeId();
        testOpportunity.Opportunity_Type__c = '';
        testOpportunity.Period_Type__c = '';
        testOpportunity.Amount_In__c = null;
        controller.validateParentOpportunity();
        testOpportunity.Start_Date__c = System.today();
        testOpportunity.CloseDate = System.today().addMonths(75);
        controller.validateParentOpportunity();
        
    }
    
    
    static testMethod void miscellaneousNegTestCoverage() {
        Opportunity testOpportunity = new Opportunity();
        Account testAccount = CreateTestClassData.createCustomerAccount();
        String tenderRecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_Annuitized).getRecordTypeId();
        String lpdRecType =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_LPD_Opportunity).getRecordTypeId();
        testOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_NetativeWinBack).getRecordTypeId();
        testOpportunity.Name = 'Test Opportunity';
        //testOpportunity.Start_Date__c = System.today();
        testOpportunity.CloseDate = System.today().addMonths(10);
        testOpportunity.stageName = '1 Prospect Tender';
        testOpportunity.Probability = 25.0;
        testOpportunity.AccountId = testAccount.Id;
        testOpportunity.Amount_In__c = -12000.0;
        
        //testOpportunity.Period_Type__c = 'Monthly';
        //testOpportunity.Opportunity_Type__c = 'Annuitized Opportunity';
        //testOpportunity.RecordTypeId = tenderRecType;
        Test.setCurrentPage(Page.TenderOpportunities);      
        system.currentPageReference().getParameters().put('RecordType', tenderRecType);    
        System.currentPageReference().getParameters().put('accid',testAccount.Id);
        System.currentPageReference().getParameters().put('opp16',testAccount.CurrencyIsoCode);
        Test.startTest();
        TenderOpportunity controller = new TenderOpportunity(new ApexPages.StandardController(testOpportunity));
        controller.negativeWinBackSaveAndAddProducts(); 
        testOpportunity.Start_Date__c = System.today();
        controller.negativeWinBackSaveAndAddProducts();
        Test.stopTest(); 
    }
    static testMethod void miscellaneousNegTestCoverage1() {
        Opportunity testOpportunity = new Opportunity();
        Account testAccount = CreateTestClassData.createCustomerAccount();
        String tenderRecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_Annuitized).getRecordTypeId();
        String lpdRecType =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_LPD_Opportunity).getRecordTypeId();
        testOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_NetativeWinBack).getRecordTypeId();
        testOpportunity.Name = 'Test Opportunity';
        //testOpportunity.Start_Date__c = System.today();
        testOpportunity.CloseDate = System.today().addMonths(10);
        testOpportunity.stageName = '1 Prospect Tender';
        testOpportunity.Probability = 25.0;
        testOpportunity.AccountId = testAccount.Id;
        testOpportunity.Amount_In__c = -12000.0;

        Test.setCurrentPage(Page.TenderOpportunities);      
        system.currentPageReference().getParameters().put('RecordType', tenderRecType);    
        System.currentPageReference().getParameters().put('accid',testAccount.Id);
        System.currentPageReference().getParameters().put('opp16',testAccount.CurrencyIsoCode);
        Test.startTest();
        TenderOpportunity controller = new TenderOpportunity(new ApexPages.StandardController(testOpportunity));
        controller.negativeWinBackSaveAndAddProducts(); 
        testOpportunity.Start_Date__c = System.today();
        controller.negativeWinBackSave();
        Test.stopTest(); 
    }
    
}