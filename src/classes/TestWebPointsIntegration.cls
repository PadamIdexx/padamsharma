@isTest

public class TestWebPointsIntegration{
    
    static testMethod void webPointsIntegrationTest() {
        Account acc1=new Account(Name='TestWebPointsIntegration',SAP_Customer_Number__c ='12345');
        acc1.ShippingCountryCode= 'US';
        acc1.ShippingStateCode = 'CA';
        string jsonRes = '{"access_token": "221e4c07-c95e-31ff-8545-0b351c078c0d","refresh_token": "5c1aa81a-808a-30c6-af5d-68a9d35f63c5","scope": "openid profile","id_token": "eyJ4NXQiOiJObUptT0dVeE16WmxZak0yWkRSaE5UWmxZVEExWXpkaFpUUmlPV0UwTldJMk0ySm1PVGMxWkEiLCJraWQiOiJkMGVjNTE0YTMyYjZmODhjMGFiZDEyYTI4NDA2OTliZGQzZGViYTlkIiwiYWxnIjoiUlMyNTYifQ.eyJhdF9oYXNoIjoiMFhXZjVUX3lHUDZTOGRCSHlvQnJLUSIsImNvdW50cnkiOiJVUyIsInN1YiI6ImMzOTM0N2FiLWRjNjAtNDJjMi05OTY3LWJiNzM4NDE3MGUxMyIsInJvbGUiOlsiSW50ZXJuYWxcL2lkZW50aXR5IiwiQ3VzdG9tZXJTdXBwb3J0IiwiSW50ZXJuYWxcL2V2ZXJ5b25lIl0sImlzcyI6Imh0dHBzOlwvXC9ucC1zc28uaWRleHguY29tOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJnaXZlbl9uYW1lIjoiSmVmZnJleSIsImFjciI6InVybjptYWNlOmluY29tbW9uOmlhcDpzaWx2ZXIiLCJzY2ltX2lkIjoiYzM5MzQ3YWItZGM2MC00MmMyLTk5NjctYmI3Mzg0MTcwZTEzIiwiYXVkIjpbInhMTjE4R2lXVXFRMkF2cm5xNlIzck44clhEQWEiXSwiYXpwIjoieExOMThHaVdVcVEyQXZybnE2UjNyTjhyWERBYSIsInByaW1hcnlfZW1haWwiOiJqZWZmcmV5LWZhdWxrbmVyQGlkZXh4LmNvbSIsImV4cCI6MTQ4OTY5NzU0MCwiZmFtaWx5X25hbWUiOiJGYXVsa25lciIsImlhdCI6MTQ4OTY5Mzk0MCwicHJlZmVycmVkX2xhbmd1YWdlIjoiZW4iLCJ1c2VybmFtZSI6ImplZmZyZXktZmF1bGtuZXJAaWRleHguY29tIn0.IoT1mCzrK6qPE9TVN-uattRwGmeTkz7shYb2QCnhWRtjLTkO05ANnS5m0XNwb15bJ9Rg54Pgc6gcf69tc9pNGP5_zFqHs1s1SZ0wc8YZwtr7y28jml0xctlYD29vsJbZ6YxS8nHwvJnPRYcT9CeQ8Jw8LvBIb353NuYDiWF5CPc","token_type": "Bearer","expires_in": 3600}';
         WebPointsUser__c wp= new WebPointsUser__c ();
         wp.name = 'IDEXX WEB POINTS';
         wp.AuthServerEndpoint__c = 'https://np-sso.idexx.com/oauth2/token';
         wp.Username__c='test@idexx.com';
         wp.Password__c='test1234';
         wp.ClientKey__c='testClientKey';
         wp.ClientSecret__c='testClientSecret';

        Test.startTest();
          insert acc1;
          insert wp; 
        Test.stopTest();
        
        system.assertEquals(acc1.id,acc1.id);
        
        pageRefMethod(acc1);
        ResponseParserWebPoints resParser = new ResponseParserWebPoints();
        resParser = ResponseParserWebPoints.parse(jsonRes);
    }    
        
     public static void pageRefMethod(Account acc){
        ApexPages.StandardController sc= new ApexPages.StandardController(acc);
        webPointsIntegration webPointsInt = new webPointsIntegration(sc);
        webPointsInt.onLoad();
    }
    
}