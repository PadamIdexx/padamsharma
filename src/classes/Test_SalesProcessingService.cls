@isTest
public with sharing class Test_SalesProcessingService {

		@isTest static void SalesOrderSimulatePositive() {
			 Test.startTest();
            	 SalesProcessingResponse responseServiceClass = new SalesProcessingResponse();
	             SalesProcessingResponse.SalesOrderSimulateResponse_Sync dummyResponseObject = createTestClassData.setSimulateResponse();
                 Test.setMock(WebServiceMock.class, new Test_SOAPReusableMockResponse(dummyResponseObject));
                 SalesProcessingRequest.SalesOrderSimulateRequest_Sync request = new SalesProcessingRequest.SalesOrderSimulateRequest_Sync();
                 SalesProcessingService utility = new SalesProcessingService();
                 utility.SalesOrderSimulate(request);
            Test.stopTest();			
		}
		
		@isTest static void SalesBeginOrderSimulatePositive() {
			 Test.startTest();
            	 SalesProcessingResponse responseServiceClass = new SalesProcessingResponse();
	             SalesProcessingResponse.SalesOrderSimulateResponse_Sync dummyResponseObject = createTestClassData.setSimulateResponse();
                 Test.setMock(WebServiceMock.class, new Test_SOAPReusableMockResponse(dummyResponseObject));
                 SalesProcessingRequest.SalesOrderSimulateRequest_Sync request = new SalesProcessingRequest.SalesOrderSimulateRequest_Sync();
                 SalesProcessingService utility = new SalesProcessingService();
                 Integer TIMEOUT_INT_SECS = 60;  
                 Continuation cont = new Continuation(TIMEOUT_INT_SECS);
                // utility.beginSalesOrderSimulate(cont, request);
            Test.stopTest();			
		}
		
		@isTest static void SalesOrderCreatePositive() {
			 Test.startTest();
            	 SalesProcessingResponse responseServiceClass = new SalesProcessingResponse();
	             SalesProcessingResponse.SalesOrderCreateResponse_Sync dummyResponseObject = createTestClassData.setCreateResponse();
                 Test.setMock(WebServiceMock.class, new Test_SOAPReusableMockResponse(dummyResponseObject));
                 SalesProcessingRequest.SalesOrderCreateRequest_Sync request = new SalesProcessingRequest.SalesOrderCreateRequest_Sync();
                 SalesProcessingService utility = new SalesProcessingService();
                 utility.SalesOrderCreate(request);
            Test.stopTest();			
		}
		
		@isTest static void SalesBeginOrderCreatePositive() {
			 Test.startTest();
            	 SalesProcessingResponse responseServiceClass = new SalesProcessingResponse();
	             SalesProcessingResponse.SalesOrderCreateResponse_Sync dummyResponseObject = createTestClassData.setCreateResponse();
                 Test.setMock(WebServiceMock.class, new Test_SOAPReusableMockResponse(dummyResponseObject));
                 SalesProcessingRequest.SalesOrderCreateRequest_Sync request = new SalesProcessingRequest.SalesOrderCreateRequest_Sync();
                 SalesProcessingService utility = new SalesProcessingService();
                 Integer TIMEOUT_INT_SECS = 60;  
                 Continuation cont = new Continuation(TIMEOUT_INT_SECS);
                // utility.beginSalesOrderCreate(cont, request);
            Test.stopTest();			
		}
		
}