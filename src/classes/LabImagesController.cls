public class LabImagesController {
    public static String result{get;set;}
    public static String resultEnd{get;set;}
    public static String resultStart{get;set;}
    public static blob imgBlob{get;set;}
    public static String Id{get;set;}
    public static String reqId{get;set;}
    public static String sType{get;set;}
    
    @testVisible private static String endpoint1 =
        WebServiceUtil.retreiveEndpoints(WebServiceUtil.LynxxImages);
    
   @testVisible private static String endpoint2 =
        WebServiceUtil.retreiveEndpoints(WebServiceUtil.USLabs);
    
    @testVisible private static String envelopeEndpoint =
        WebServiceUtil.retreiveEndpoints(WebServiceUtil.BEACON_ENDPOINT);
    
    public static void request(){
        if(test.isRunningTest()) endpoint2 = '';
        
        Id = ApexPages.currentPage().getParameters().get('guid');
        reqId = ApexPages.currentPage().getParameters().get('reqId');
        sType = ApexPages.currentPage().getParameters().get('type');
        
        Http http = new Http();
        
        String SoapXMLBody;

        if(sType == 'Antrim'){
            SoapXMLBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cre="ld:logical/CreateCoreImage_ws">'   +
                '<soapenv:Header/>    <soapenv:Body>       <cre:findByGuid>          <cre:guid>'+ Id +'</cre:guid>'+
                '</cre:findByGuid>    </soapenv:Body> </soapenv:Envelope>';
            
            
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setEndpoint(endpoint2);
            req.setHeader('SOAPAction', '"findByGuid"');
            req.setTimeout(120000);
            req.setHeader('sessionId', UserInfo.getSessionId());
            req.setBody(SoapXMLBody);
            
            HttpResponse resp = http.send(req);
            
            result = '';
            
            boolean next = true;
            XmlStreamReader xsr = resp.getXmlStreamReader();
            while(next){
                String parse = '';
                if(xsr.getEventType() == XmlTag.START_ELEMENT){
                    if('Image' == xsr.getLocalName()){
                        parse = ParseImg(xsr);    
                    }
                    
                }
                if(xsr.hasNext() && !test.isRunningTest()){
                    xsr.next();
                } else {
                    next = false;
                    break;
                }
                
            }
            
            returnResp('');
        }
        else{
            SoapXMLBody ='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:imag="ld:Image/logical/Image_ws"><soapenv:Header/>'+
                '<soapenv:Body><imag:findByImageId><imag:imageId>'+Id+'</imag:imageId></imag:findByImageId></soapenv:Body></soapenv:Envelope>';
            if(test.isRunningTest()) endpoint1 = '';
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setEndpoint(endpoint1);
            //req.setEndpoint('https://stbeacon.idexx.com/beacon/sfdc/ApplicationServicesLABS/ProxyServices/LynxxImageService');
            req.setHeader('SOAPAction', '"ld:Image/logical/Image_ws/findByImageId"');
            req.setTimeout(120000);
            req.setHeader('sessionId', UserInfo.getSessionId());
            req.setBody(SoapXMLBody);
            HttpResponse resp = http.send(req);
            
            result = '';
            
            boolean next = true;
            XmlStreamReader xsr = resp.getXmlStreamReader();
            while(next){
                String parse = '';
                if(xsr.getEventType() == XmlTag.START_ELEMENT){
                    
                    if('Image' == xsr.getLocalName()){
                        
                        parse = ParseImg(xsr);
                    }
                    
                }
                if(xsr.hasNext() && !test.isRunningTest()){
                    xsr.next();
                } else {
                    next = false;
                    break;
                }
                
            }
            
            returnResp(result);
            
        }
        
    }
    public static String ParseImg(XmlStreamReader xsr){
        String readerString;
        boolean next = true;
        while(next){
            readerString = '';
            if(xsr.getEventType() == XmlTag.END_ELEMENT){
                break;
                
            }else if(xsr.getEventType() == XmlTag.CHARACTERS){

                String len = '';
                len = xsr.getText();
                if(len.length() >20){
                     //System.debug('e '  + len + ' , ' + xsr.getText());
                    readerString+= xsr.getText();
                    result+= xsr.getText();
                }
                
            }
            if(xsr.hasNext()){
                xsr.next();
                
            }else{
                next = false;
                break;
                
            }
            
        }
        return readerString;
        
    } 
    public static Object returnResp(String res){

            imgBlob = EncodingUtil.convertFromHex(result);
            result = EncodingUtil.base64Encode(imgBlob);
            return null;
        
        
    }    
    
}