public class vetConnectplusIntegrationBasic {
    public string Sapextid;
    public static account act {get; set;}
    public static User user {get; set;}
    public Boolean optLnk {get; set;}
    public String activeSapId;
    public String activeUseToken;
    public boolean isError{get;set;}
    public boolean isSAP{get;set;}
    public integer getStatus;
    public string phone{get;set;}
    public string phoneNumber{get;set;}
    
    public Pagereference  onload() 
    { 
        Sapextid = Apexpages.currentpage().getParameters().get('Id');
        phone = Apexpages.currentpage().getParameters().get('Phone');
        if (Test.isRunningTest()) {
            List<Account> testAccList = [Select Id From Account Where Name='TestVetConnectAccount' Limit 1];
            sapextid = testAccList[0].Id;
        }
        act = [Select Id,name,VetConnect_Support_Number__c,SAP_Number_without_Leading_Zeros__c from account where Id = :Sapextid limit 1];
        activeSapId = act.SAP_Number_without_Leading_Zeros__c; 
        phoneNumber = act.VetConnect_Support_Number__c;
        if( isError == false && activeSapId == null){
            return null;
        }
        else {               
            basicRequest();
            if(isError == false){
                VetConnect_Plus__c GoodClick= new VetConnect_Plus__c(User_Name__c=UserInfo.getUserId(),Name='Practice is Active',Account_Name__c= act.id,Button_Link__c='VetConnect Button',Clicked_Date_and_Time__c=datetime.now());
                insert GoodClick;
                string endpointurl3='https://www.vetconnectplus.com/integration/impersonation/home?sapId='+activeSapId+'&singleUseLoginToken='+activeUseToken;
                Pagereference pgRef = new Pagereference(endpointurl3);
                pgRef.setRedirect(true);
                return pgRef;
            }
            else
            {
                VetConnect_Plus__c BadClick= new VetConnect_Plus__c(User_Name__c=UserInfo.getUserId(),Name='Practice is not Active',Account_Name__c= act.id,Button_Link__c='VetConnect Button',Clicked_Date_and_Time__c=datetime.now());
                insert BadClick;
                return null;
            }
        }
    }
    
    public vetConnectplusIntegrationBasic(ApexPages.StandardController controller) {
        isError = false;
    }
    
    public PageReference basicRequest() {
        act = [Select Id,name,SAP_Number_without_Leading_Zeros__c from account where Id = :Sapextid limit 1];
        system.debug('Account SAP number'+act.SAP_Number_without_Leading_Zeros__c);
        // activeSapId = act.SAP_Number_without_Leading_Zeros__c;
        string endpointUrl = 'https://www.vetconnectplus.com/integration/impersonation/practices?sapId='+act.SAP_Number_without_Leading_Zeros__c;        
        string username = 'salesforce';
        string password = 'HJzCq4HENxqV';
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        
        System.debug('*********' + authorizationHeader);
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        req.setMethod('GET');
        req.setEndpoint(endpointUrl);
        req.setHeader('Accept', 'application/json');
        //req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setTimeout(12000);
        req.setHeader('Authorization',authorizationHeader);
        system.debug('********Request**********'+req);
        if (!Test.isRunningTest()) {
            res = h.send(req);
        }
        if (Test.isRunningTest()) {
            res.setBody('{"sapId" : "12345", "active" : true, "activeUserCount" : 6, "relatedSapIds" : []}');
            res.setStatusCode(200);
        }
        system.debug('******Response*****'+res+'*******ResponseCode*******'+res.getStatusCode()+'********ResponseBody*******'+res.getBody());
        JSONParser parser = JSON.createParser(res.getBody());
        getStatus = res.getStatusCode();
        system.debug('Status Code'+getStatus);
        String jsonResp = res.getBody();
        
        if (getStatus == 200)
        {
            if (String.isNotBlank(jsonResp)) {
                VetconnectJsonResponse jsonParse = (VetconnectJsonResponse)JSON.deserialize(jsonResp, VetconnectJsonResponse.class);
                // VetconnectJsonResponse jsonParse = (VetconnectJsonResponse)parser.readValueAs(VetconnectJsonResponse.class);
                System.debug('************' + jsonParse.active);
                
                if (jsonParse.active) {
                    string endpointUrl1 = 'https://www.vetconnectplus.com/integration/impersonation/login';        
                    string jsonReqBody='{"username":"salesforce","password":"HJzCq4HENxqV"}';       
                    System.debug('*********' + authorizationHeader);   
                    Http h1 = new Http();
                    HttpRequest req1 = new HttpRequest();
                    HttpResponse res1 = new HttpResponse();
                    req1.setMethod('POST');
                    req1.setEndpoint(endpointUrl1);
                    req1.setHeader('Content-Type', 'application/json');
                    req1.setTimeout(12000);
                    req1.setHeader('Authorization',authorizationHeader);
                    req1.setBody(jsonReqBody);
                    if (!Test.isRunningTest()) {
                        res1 = h1.send(req1);
                    }
                    if (Test.isRunningTest()) {
                        activeUseToken = 'adhkbwmiemce2saf5hjd';
                    }
                    system.debug('******Response*****'+res1+'*******ResponseCode*******'+res1.getStatusCode()+'********ResponseBody*******'+res1.getBody());
                    JSONParser parser1 = JSON.createParser(res1.getBody());
                    parser1.nexttoken();
                    parser1.nextvalue();
                    string restoken = parser1.getText();
                    activeUseToken=restoken;
                    system.debug('token value'+restoken);
                    system.debug('Active Use Token:'+activeUseToken);
                    string endpointurl2='https://www.vetconnectplus.com/integration/impersonation/home?sapId='+act.SAP_Number_without_Leading_Zeros__c+'&singleUseLoginToken='+restoken;
                    return null;   
                }else{
                   // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Practice Not Active!'));
                    isError = true;
                    isSAP= true;
                    return null;            
                } 
            }
            else{
               // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Not body found in VetConnect Plus!'));
                return null;
            }
        }
        else{
            isError = true;
            isSAP= false;
            return null;
        }
        return null;
    }
    
}