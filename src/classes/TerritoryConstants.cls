public abstract class TerritoryConstants {

    //Territory Roles Picklist values
    public static final String AREA_MANAGER = 'Area Manager';
    public static final String CCP = 'CCP';
    public static final String CORPORATE_ACCOUNT_MANAGER = 'Corporate Account Manager';
    public static final String CUSTOMER_SUPPORT_MANAGER = 'Customer Support Manager';
    public static final String CUSTOMER_SUPPORT_REP = 'Customer Support Rep';
    public static final String CUSTOMER_SUPPORT_SUPERVISOR = 'Customer Support Supervisor';
    public static final String DR = 'DR';
    public static final String DR_MANAGER = 'DR Manager';
    public static final String EQUINE_DR = 'Equine DR';
    public static final String EXECUTIVE = 'Executive';
    public static final String FSR_DX = 'FSR DX';
    public static final String FSR_DX_SUPERVISOR = 'FSR DX Supervisor';
    public static final String FINANCE = 'Finance';
    public static final String ICSC = 'ICSC';
    public static final String IDSC = 'IDSC';
    public static final String IM_MANAGER = 'IM Manager';
    public static final String IMSS = 'IMSS';
    public static final String IMTSS = 'IMTSS';
    public static final String ISCC = 'ISCC';
    public static final String ISR = 'ISR';
    public static final String IVDC = 'IVDC';
    public static final String IVDC_MANAGER = 'IVDC Manager';
    public static final String INSIDE_NEO = 'Inside Neo';
    public static final String INSIDE_SALES_SUPERVISOR = 'Inside Sales Supervisor';
    public static final String LKAM = 'LKAM';
    public static final String LPD_CUSTOMER_SERVICE = 'LPD Customer Service';
    public static final String LPD_FIELD_SALES_REP = 'LPD Field Sales Rep';
    public static final String LPD_GENERAL_MANAGER = 'LPD General Manager';
    public static final String LPD_INSIDE_SALES_REP = 'LPD Inside Sales Rep';
    public static final String LPD_REGIONAL_MANAGER = 'LPD Regional Manager';
    public static final String LPD_SALES_MANAGER = 'LPD Sales Manager';
    public static final String MARKETING = 'Marketing';
    public static final String PHNSS = 'PHNSS';
    public static final String PSR = 'PSR';
    public static final String PSV = 'PSV';
    public static final String REGIONAL_MANAGER = 'Regional Manager';
    public static final String SOP_TEAM = 'SOP Team';
    public static final String SST_ANALYST = 'SST Analyst';
    public static final String SALES_SUPPORT_MANAGER = 'Sales Support Manager';
    public static final String SALES_SUPPORT_REP = 'Sales Support Rep';
    public static final String TECHICAL_SERVICE_REP = 'Techical Service Rep';
    public static final String TECHNICAL_SERVICE_MANAGER = 'Technical Service Manager';
    public static final String TECHNICAL_SERVICE_SUPERVISOR = 'Technical Service Supervisor';
    public static final String VDC = 'VDC';
    public static final String VDS = 'VDS';
    public static final String VSS = 'VSS';

}