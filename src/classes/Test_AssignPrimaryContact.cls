/**
*       @author Vishal Negandhi
*       @date   16/10/2015   
        @description    Test class for AssignPrimaryContact
        Function: This test class will test the functionality of AssignPrimaryContact.
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Vishal Negandhi               16/10/2015          Original Version
**/

@isTest(SeeAllData = False)    
public With Sharing class Test_AssignPrimaryContact {
    static testMethod void testPrimaryContactAssignment(){
        // Create new Account
        Account acc = CreateTestClassData.createCustomerAccount();
        
        Test.startTest();
        database.executeBatch(new AssignPrimaryContact());
        Test.stopTest();
        acc = [Select Id, Primary_Contact__c, Primary_Contact__r.FirstName FROM Account WHERE Id = :acc.Id];
        // Adding assert based on the expectation from functionality
        System.Assert(acc.Primary_Contact__c != NULL, 'Primary Contact should be linked for this Account');
        System.Assert(acc.Primary_Contact__r.FirstName == 'Place Holder', 'Place holder contact should be the primary contact');
        
    }
    static testMethod void testPrimaryContactAssignment2(){
        // Create new Account
        Account acc = CreateTestClassData.createCustomerAccount();
        
        //Populate Contact details 
        Contact con           = new Contact(); 
        con.AccountId         = acc.Id; 
        con.LastName          = 'Place Holder'; 
        con.CurrencyIsoCode   = 'USD'; 
        // checking for permissions before creating/modifying records
        if(Schema.sObjectType.Contact.isCreateable() && con != null)
            insert con; 
        
        
        Test.startTest();
        database.executeBatch(new AssignPrimaryContact());
        Test.stopTest();
        acc = [Select Id, Primary_Contact__c FROM Account WHERE Id = :acc.Id];
        // Adding assert based on the expectation from functionality
        System.Assert(acc.Primary_Contact__c != NULL, 'Primary contact should be linked for this Account');
    }
    
    static testMethod void testPrimaryContactAssignmentScheduler(){
        String strCRON_EXP = '0 0 0 15 3 ? 2022';
        
        test.startTest();
        String jobId = System.schedule('Assign Primary Contact',
                        strCRON_EXP,
                        new AssignPrimaryContact());
        
        test.stopTest();
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        // Verify the expressions are the same
        System.assertEquals(strCRON_EXP, ct.CronExpression);
        
    }

}