public class WFM_Constants {

    public static final Schema.DescribeSObjectResult workOrderSObjectResult = WorkOrder.SObjectType.getDescribe();
    public static final Schema.DescribeSObjectResult workOrderLineItemSObjectResult = WorkOrderLineItem.SObjectType.getDescribe();
    public static final Schema.DescribeSObjectResult caseSObjectResult = Case.SObjectType.getDescribe();

    // Work Order Status
    public static final String WORK_ORDER_STATUS_COMPLETED = 'Completed';
    public static final String WORK_ORDER_STATUS_CANCELED = 'Canceled';
    public static final String WORK_ORDER_STATUS_CLOSED = 'Closed';
    public static final String WORK_ORDER_STATUS_NEW = 'New';
    public static final String WORK_ORDER_STATUS_CANCELLATION_REQUESTED = 'Cancellation Requested';

    //Work Order Line Item Status
    public static final String WORK_ORDER_LINE_ITEM_COMPLETED = 'Completed';
    public static final String WORK_ORDER_LINE_ITEM_NEW = 'New';

    // Work Order Priority
    public static final String WORK_ORDER_PRIORITY_LOW = 'Low';
    public static final String WORK_ORDER_PRIORITY_MEDIUM = 'Medium';
    public static final String WORK_ORDER_PRIORITY_HIGH = 'High';
    public static final String WORK_ORDER_PRIORITY_CRITICAL = 'Critical';

    //US29611 - 5.24.2017 - HKinney - Work Order Product Type Names - changed to store this information as Labels.
    //                      Allows us to reference by value so we can dynamically parse Work Order durations that are
    //                      stored via WFM_WorkOrder_Duration__mdt.
    // Work Order Product Type
    public static final String WORK_ORDER_PRODUCT_TYPE_DX = Label.WFM_ProductType_DX;
    public static final String WORK_ORDER_PRODUCT_TYPE_DR = Label.WFM_ProductType_DR;

    //US29611 - 5.24.2017 - HKinney - Work Order Record Type Names - changed to store this information as Labels in
    //                      case we change the Record Type Name, we can still function without a code change:
    //Work Order RecordTypes
    public static final  Id WORK_ORDER_INSTALL_RECORDTYPE_ID        = workOrderSObjectResult.getRecordTypeInfosByName().get(Label.WFM_RecordType_Install).getRecordTypeId();
    public static final  Id WORK_ORDER_CRITICAL_RECORDTYPE_ID       = workOrderSObjectResult.getRecordTypeInfosByName().get(Label.WFM_RecordType_Critical).getRecordTypeId();
    public static final  Id WORK_ORDER_PRESCRIPTIVE_RECORDTYPE_ID   = workOrderSObjectResult.getRecordTypeInfosByName().get(Label.WFM_RecordType_Prescriptive).getRecordTypeId();
    public static final  Id WORK_ORDER_PROACTIVE_RECORDTYPE_ID      = workOrderSObjectResult.getRecordTypeInfosByName().get(Label.WFM_RecordType_Proactive).getRecordTypeId();
    public static final  Id WORK_ORDER_VISIT_REQUEST_RECORDTYPE_ID  = workOrderSObjectResult.getRecordTypeInfosByName().get(Label.WFM_RecordType_VisitRequest).getRecordTypeId();
    public static final  Id WORK_ORDER_ONBOARDING_RECORDTYPE_ID     = workOrderSObjectResult.getRecordTypeInfosByName().get(Label.WFM_RecordType_Onboarding).getRecordTypeId();

    public static final  Id CASE_CAG_DEAL_SHAPING_REQUEST_RECORDTYPE_ID  = caseSObjectResult.getRecordTypeInfosByName().get('CAG Deal Shaping Request').getRecordTypeId();

    //Work Order Line Item RecordTypes
    public static final  Id WORK_ORDER_LINE_ITEM_INSTALL_RECORDTYPE_ID        = workOrderLineItemSObjectResult.getRecordTypeInfosByName().get('Install').getRecordTypeId();
    public static final  Id WORK_ORDER_LINE_ITEM_NON_INSTALL_RECORDTYPE_ID    = workOrderLineItemSObjectResult.getRecordTypeInfosByName().get('Non-Install').getRecordTypeId();

    // Work Order Line Item Activity
    public static final String WORK_ORDER_LINE_ITEM_ACTIVITY_INSTALL_TRAINING = 'Install_Set_Up_Training';

    //Work Order Reason for Visit
    public static final String WORK_ORDER_REASON_NEW_SYSTEM = 'New System';

    //Work Order Payment Type
    public static final String WORK_ORDER_PAYMENT_TYPE_CASH = 'Cash';

    //Service Appointment Status
    public static final String SERVICE_APPOINTMENT_STATUS_IN_PROGRESS = 'In Progress';
    public static final String SERVICE_APPOINTMENT_STATUS_NEW = 'None';
    public static final String SERVICE_APPOINTMENT_STATUS_COMPLETED = 'Completed';
    public static final String SERVICE_APPOINTMENT_STATUS_CANCELED = 'Canceled';
    public static final String SERVICE_APPOINTMENT_STATUS_SCHEDULED = 'Scheduled';
    public static final String SERVICE_APPOINTMENT_STATUS_DISPATCHED = 'Dispatched';

    // Account
    public static final Integer ACCOUNT_MIN_RANKING_FOR_PRESCRIPTIVE = 100;

    //
    public static Boolean WORKORDER_TRIGGER_HANDLER_HAS_RAN = false;
    public static Boolean WORKORDER_TRIGGER_CANCELLATION_REQUEST_EMAIL_DELIVERED=false;

    // User Territory Role
    public static final String USER_TERRITORY_ROLE_FSR_DX = 'FSR DX';

    // Added a new Constant USER_TERRITORY_ROLE_FSR_DX_SUP = 'FSR DX Supervisor' for US29710  by Lakshmi.
    public static final String USER_TERRITORY_ROLE_FSR_DX_SUP = 'FSR DX Supervisor';

    public static final String USER_TERRITORY_ROLE_FSR_DR = 'FSR DR';
    public static final String USER_TERRITORY_ROLE_DR = 'DR';
    public static final String USER_TERRITORY_ROLE_VDC = 'VDC';
    public static final String USER_TERRITORY_ROLE_VDS = 'VDS';

    // Elevation form email titles - Diagnostic
    public static final String ELEVATION_FORM_SAP_NUMBER = 'SAP_Number';
    public static final String ELEVATION_FORM_LINE_OF_BUSINESS = 'Line_of_Business';
    public static final String ELEVATION_FORM_ESTIMATED_INSTALLATION_DATE = 'Estimated Installation Date';

    // Elevation form email titles - Digital
    public static final String ELEVATION_FORM_CLINIC_SAP = 'Clinic SAP';

    // Email Service Emails
    public static final String ELEVATION_FORM_EMAIL_ADDRESS_DX = 'elevation_form_dx';
    public static final String ELEVATION_FORM_EMAIL_ADDRESS_DR = 'elevation_form_dr';

    // Operating Hours Days Of the week
    public static final String OPERATING_HOURS_MONDAY = 'Monday';
    public static final String OPERATING_HOURS_TUESDAY = 'Tuesday';
    public static final String OPERATING_HOURS_WEDNESDAY = 'Wednesday';
    public static final String OPERATING_HOURS_THURSDAY = 'Thursday';
    public static final String OPERATING_HOURS_FRIDAY = 'Friday';
    public static final String OPERATING_HOURS_SATURDAY = 'Saturday';
    public static final String OPERATING_HOURS_SUNDAY = 'Sunday';
    public static final Set<String> WEEKDAYS = new Set<String>{OPERATING_HOURS_MONDAY, OPERATING_HOURS_TUESDAY, OPERATING_HOURS_WEDNESDAY,
            OPERATING_HOURS_THURSDAY, OPERATING_HOURS_FRIDAY, OPERATING_HOURS_SATURDAY, OPERATING_HOURS_SUNDAY};

    public static final Set<String> WEEKEND_DAYS = new Set<String>{OPERATING_HOURS_SATURDAY, OPERATING_HOURS_SUNDAY};

    // Opportunity's Stages
    public static final String OPPORTUNITY_STAGE_COMMITTED = '4 Committed';
    public static final String OPPORTUNITY_STAGE_PROSPECT = '1 Prospect';

    public static final Map<String, String> MAP_PRODUCT_TYPES = new Map<String, String>{
            'DR' => 'DR',
            'IHD' => 'DX',
            'ICS' => 'VSS'
    };

    public static final Map<String, String> MAP_PRODUCT_TYPE_NAMES = new Map<String, String>{
            'DR' => 'Digital',
            'DX' => 'Diagnostic'
    };

    // QUote status
    public static final String QUOTE_STATUS_ACCEPTED = 'Accepted';

    //Service Resource Type
    public static final String SERVICE_RESOURCE_TYPE_TECHNICIAN = 'T';

    //Resource Preference Type
    public static final String RESOURCE_PREFERENCE_TYPE_REQUIRED = 'Required';

    //Public Group
    public static final String PUBLIC_GROUP_CAG_FSR_DX_ADMIN = 'CAG_FSR_DX_Admin';

    //Service Territory
    public static final String SERVICE_TERRITORY_TYPE_PRIMARY = 'P';

    public static final Boolean ENABLE_WORK_ORDERS = WFM_Settings__c.getInstance().Enable_Work_Order_Creation__c;

}