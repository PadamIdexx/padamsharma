global class WFM_ElevationFormEmailService implements Messaging.InboundEmailHandler {
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email,
                                                                Messaging.InboundEnvelope env) {

        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        Map<String, String> mapEmailData = new Map<String, String>();
        String emailBody = email.plainTextBody;
        String subject = email.subject;
        List<String> emailRows = emailBody.split('\n');
        
        System.debug('Email_Body = ' + emailBody);
        System.debug('Rows Count = ' + emailRows.size());
        System.debug('Rows  = ' + emailRows);

        // Set Subject in map
        mapEmailData.put('Subject', subject);

        // Set the product type based on the email
        mapEmailData.put('Line_of_Business', getLineOfBusiness(email.toAddresses, email.ccAddresses));

        for(String row : emailRows){
            List<String> rowData = row.split(':');

            if(row.containsIgnoreCase(WFM_Constants.ELEVATION_FORM_SAP_NUMBER) && rowData.size() >= 2){
                mapEmailData.put('SAP_Account_Number', rowData.get(1).trim());
                if(String.isBlank(mapEmailData.get('Line_of_Business')))
                    mapEmailData.put('Line_of_Business', WFM_Constants.WORK_ORDER_PRODUCT_TYPE_DX);
            }else if(row.containsIgnoreCase(WFM_Constants.ELEVATION_FORM_CLINIC_SAP) && rowData.size() >= 2){
                mapEmailData.put('SAP_Account_Number', rowData.get(1).trim());
                if(String.isBlank(mapEmailData.get('Line_of_Business')))
                    mapEmailData.put('Line_of_Business', WFM_Constants.WORK_ORDER_PRODUCT_TYPE_DR);
            }else if(row.containsIgnoreCase(WFM_Constants.ELEVATION_FORM_ESTIMATED_INSTALLATION_DATE) && rowData.size() >= 2 && String.isNotBlank(rowData.get(1))){
                mapEmailData.put('Estimated_Installation_Date', rowData.get(1).trim());
            }
        }
        mapEmailData.put('Body', emailBody);

        if(WFM_Constants.ENABLE_WORK_ORDERS || Test.isRunningTest()){
            WorkOrder workOrder = WFM_Utils.createServiceWorkOrderFromEmailValuesMap(mapEmailData);
            insert workOrder;
        }

        result.success = true;
        return result;
    }

    public static String getLineOfBusiness(List<String> toEmails, List<String> ccEmails){
        String lineOfBusiness;
        Set<String> setEmails = new Set<String>();

        setEmails.addAll(toEmails);

        if(ccEmails != null && !ccEmails.isEmpty())
            setEmails.addAll(ccEmails);

        for(String email : setEmails){
            if(String.isNotBlank(email)){
                if(email.containsIgnoreCase(WFM_Constants.ELEVATION_FORM_EMAIL_ADDRESS_DX))
                    lineOfBusiness = WFM_Constants.WORK_ORDER_PRODUCT_TYPE_DX;
                if(email.containsIgnoreCase(WFM_Constants.ELEVATION_FORM_EMAIL_ADDRESS_DR))
                    lineOfBusiness = WFM_Constants.WORK_ORDER_PRODUCT_TYPE_DR;
            }
        }
        
        return lineOfBusiness;
    }
}