/**
*       @author Hemanth Vanapalli
*       @date   18/09/2015   
        @description   For record type selection on opportunity
        Function:      Redirects the user to a VF page to select the opportunity record type when the user wants to create a new opportunity.
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Hemanth Vanapalli               07/10/2015          Modified Version
**/

public with sharing class RecordtypeSelectionController {
    public ApexPages.StandardController controller {get;set;}   
    public Opportunity thisOpportunity {get;set;}
    public Boolean isRedirectValid {get;set;}
    public Boolean isSplitSelected {get;set;}
    public Account thisAccount {get;set;}
    public Boolean displaySplitSection {get;set;}
    public String userProfileName {get;set;}
    public List<SelectOption> recTypes {get;set;}
    public Map<Id,Profile> profileMap = new Map<Id,Profile>();
    public Boolean isTenderType {get;set;}
    public RecordtypeSelectionController(ApexPages.StandardController controller) {
        System.debug('controller record is:'+controller);
         this.controller = controller; 
         thisAccount = (Account)controller.getRecord();
         profileMap = new Map<Id,Profile>([SELECT Id,Name From Profile where usertype = 'Standard' ]);
         thisOpportunity = new Opportunity();
         isRedirectValid = true;
         userProfileName = profileMap.get(UserInfo.getProfileId()) != null ? profileMap.get(UserInfo.getProfileId()).Name: null;
         //thisOpportunity = (Opportunity) controller.getRecord();
         isSplitSelected = false;
         isTenderType = false;
         displaySplitSection = false; 
         System.debug('controller record is:1'+controller.getId());
         
         setRecordTypes();
         if(userProfileName.contains('LPD')) {
            thisOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('LPD Opportunity').getRecordTypeId();
         }
         System.debug('page Parameters are:'+System.currentPageReference().getParameters());
    }
    public PageReference redirectToPage() {
        System.debug('page Parameters are1:'+System.currentPageReference().getParameters());
        System.debug('controller for action is:'+controller.getId());
        return (System.currentPageReference().getParameters().get('accid') == null )? Page.Disable_Opportunity_Creation : null;
        /* 
        if(System.currentPageReference().getParameters().get('accid') == null) {
            PageReference pr = Page.Disable_Opportunity_Creation;
            pr.setRedirect(true);
            return pr;
        }
        return null;
        */
    }
    public void setRecordTypes() {
        
        recTypes = getAllRecordTypesPicklistValues(thisOpportunity);
        System.debug('rec Types are:'+recTypes);
//      return null;

    }
    public list<Schema.RecordTypeInfo> getRecordTypes(sObject thisSObject) {
        return thisSObject.getsObjectType().getDescribe().getRecordTypeInfos();
    }
    public list<Selectoption> getAllRecordTypesPicklistValues(sobject thisSObject) {
        list<Selectoption> recordTypePickList = new list<Selectoption>();
        for(Schema.RecordTypeInfo rType: getRecordTypes( thisSObject )) {
             if( rType.isAvailable() && rType.getName() != 'Master' )
                recordTypePickList.add(new Selectoption(rType.getRecordTypeId() ,rType.getName()));
        }
        return recordTypePickList;
    }
    public PageReference redirectToThisRecordType() {
        isRedirectValid = true;
        System.debug('selected Rec Type ID:'+controller.getRecord());
        System.debug('selected Rec thisOpportunity ID:'+thisOpportunity);
        if(String.isBlank(thisOpportunity.RecordTypeId)) {
            isRedirectValid = false;
            ApexPages.addMessage(new ApexPages.Message( ApexPages.Severity.Error,  'Please Select RecordType' ));
            return null;
        }
        PageReference nextPage = Page.TenderOpportunities;
        nextPage.getParameters().put('RecordType', thisOpportunity.RecordTypeId);
        if(isSplitSelected == true || isSplitSelected == false || String.isBlank(String.valueOf(isSplitSelected))) {
            nextPage.getParameters().put('isSplitSelected', String.valueOf(isSplitSelected));
        }
        if(isSplitSelected == null ) {
            nextPage.getParameters().put('isSplitSelected', 'false');
        }
        if( !System.currentPageReference().getParameters().keySet().isEmpty()) {
            for(String key : System.currentPageReference().getParameters().keySet()) {
                if(key.equalsIgnoreCase('accid') || key.equalsIgnoreCase('opp16')) {
                    nextPage.getParameters().put(key, System.currentPageReference().getParameters().get(key)); 
                }
            }
        }
        System.debug('next page parameters are:'+nextPage.getParameters());
        //nextPage.getParameters().put('Id', thisOpportunity.Id);
        return nextPage;
        //return null;
    }
    
}