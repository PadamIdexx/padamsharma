/****************************************************************************
* Apex Class Name    : OrderCreationUtil
* Description        : Utility Class for the Order Create Wizard
                       
* Modification Log   : 
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Raushan Anand             Feb 19 2016                 Created    
                                                   
****************************************************************************/
public with sharing class OrderCreationUtil {
    //Properties for Idexx Domain Value
    // CAG NA Ship method values
    public static List<IDEXX_Domain_Values__c> domValList {
          get {
               if (domValList == null) {
                    domValList = [SELECT Name,Code__c,Description__c,Default_Cost_Center_For_Rep__c, EMEA_LPD__c, NA_LPD__c 
                    FROM IDEXX_Domain_Values__c WHERE Name!= null and NA_CAG__c = true and Active__c = True ORDER BY Description__c ASC]; //and Active__c = True  
                    // Added NA_CAG__c clause to meet the requirement TKT-000728 
               }

               return domValList;
          }
          set;
     }
     
     public static List<IDEXX_Domain_Values__c> domValListCAGEMEA {
          get {
               if (domValList == null) {
                    domValList = [SELECT Name,Code__c,Description__c,Default_Cost_Center_For_Rep__c, EMEA_LPD__c, NA_LPD__c 
                    FROM IDEXX_Domain_Values__c WHERE Name!= null and EMEA_CAG__c = true  and Active__c = True  ORDER BY Description__c ASC]; // Active__c = True
               }

               return domValList;
          }
          set;
     }
     
     // LPD NA (NA_LPD__c) Ship method values
     public static List<IDEXX_Domain_Values__c> domValListLPD {
          get {
               if (domValListLPD == null) {
                    domValListLPD = [SELECT Name,Code__c,Description__c,Default_Cost_Center_For_Rep__c, EMEA_LPD__c, NA_LPD__c  
                    FROM IDEXX_Domain_Values__c WHERE Name!= null and NA_LPD__c = true and Active__c = True  ORDER BY NAME ASC];
                    // Active__c = True
                    // Added NA_CAG__c clause to meet the requirement TKT-000728 
               }

               return domValListLPD;
          }
          set;
     }
     // LPD EMEA Ship method values
     public static List<IDEXX_Domain_Values__c> domValListLPDEMEA {
          get {
               if (domValListLPDEMEA == null) {
                    domValListLPDEMEA = [SELECT Name,Code__c,Description__c,Default_Cost_Center_For_Rep__c, EMEA_LPD__c, NA_LPD__c  
                    FROM IDEXX_Domain_Values__c WHERE Name!= null and EMEA_LPD__c = true and Active__c = True ORDER BY NAME ASC];
                    // Active__c = True
                    // Added EMEA_LPD__c clause to meet the requirement TKT-000728 
               }

               return domValListLPDEMEA;
          }
          set;
     }
     
    //Properties for Credit Card Validation
    public static List<Credit_Card_Type_Number_Validation__mdt> ccValidationList {
          get {
               if (ccValidationList == null) {
                    ccValidationList = [SELECT Label, Minimum_Charecter__c,Maximum_Charecter__c,Starts_With__c FROM Credit_Card_Type_Number_Validation__mdt LIMIT 10];
               }
               return ccValidationList;
          }
          set;
     }
}