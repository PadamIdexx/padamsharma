/**
*    @author         Anudeep Gopagoni
*    @date           12/07/2015   
     @description    This class is used to display the Program Enrollments data in an inline VF page. 
     Function:       Class used to display the ProgramEnrollment VF page that will appear in the related list of an account. 
     Modification Log:
    ------------------------------------------------------------------------------------
    Developer                            Date                Description
    ------------------------------------------------------------------------------------
    Anudeep Gopagoni                   12/07/2015          Initial Version
    Anudeep Gopagoni                   1/05/2016           Made changes based on Code Review generated on 1/03/2016
    Anudeep Gopagoni                   1/06/2016           Made changes to address TKT-000278
    Aditya Sangawar                    29/01/2016          Added enrollmentList to constructor
    Anudeep Gopagoni                   1/02/2016           Added logic to add leading zeros to SAP Customer Number
    Anudeep Gopagoni                   1/02/2016           Added condition TermDate>Today to SOQL query

**/
public with sharing class ProgramEnrollmentsController{
    public List<Account> accountSAPNumber;
    public String storeSAPFromQuery;
    public    List<DisplayWrapper> enrolWrapList               {get;set;}//List used in Wrapper Class
    public    boolean isexception                              {get;set;}//boolean flag to set isexception if there is any exception
    public    boolean isSapNoNull                              {get;set;}//boolean flag to set isSapNoNull to capture error message when isSapNoNull is null      
    public List<BRF_ENROLL__x> enrollmentList;
    String SAPNumberWithLeadingZeros;
    
    //remote action is invoked from constructor
    public ProgramEnrollmentsController(ApexPages.StandardController std){
        SAPNumberWithLeadingZeros  = '';
        //Set both the flags to false initially
        isexception = false; 
        isSapNoNull = false;
        enrollmentList = new List<BRF_ENROLL__x>();
        accountSAPNumber  = [SELECT SAP_Customer_Number__c FROM Account where id =:ApexPages.currentPage().getParameters().get('id') LIMIT 1];
        if(accountSAPNumber.size()>0)
        storeSAPFromQuery = accountSAPNumber[0].SAP_Customer_Number__c; 
        SAPNumberWithLeadingZeros= ('0000000000' + storeSAPFromQuery).substring(storeSAPFromQuery.length()); 
        enrolWrapList = new List<DisplayWrapper>(); 
    }
    
    /*
* @Method name  : fetchProgramEnrollments
* @Description  : Returns List of WrapperClass Elements
* @Param        : SapCustomerNo
*/
    //Implemented visualforce remoting to improve the page performance
    public void fetchProgramEnrollments(){ 
        Date d = system.today();
        String st1 = String.valueOf(d); 
        String monthExtracted= st1.substring(5,7);
        String DateExtracted = st1.substring(8,10); 
        String formattedDt = String.valueOf(d.Year())+String.valueOf(monthExtracted)+String.valueOf(DateExtracted);
        enrolWrapList = new List<DisplayWrapper>(); 
        if(storeSAPFromQuery != null) { 
            try{
            if(Test.isRunningTest()) {
            BRF_ENROLL__x testc = new BRF_ENROLL__x(PROG_ID__c = '00004', DisplayUrl='TEST',PROG_NAME__c='PRACTICE DEVELOPER',PROG_DESC__c='CLINIC PURCHASES FROM PROGRAM CATEGORIES AND BASED ON MATRIX, POINTS ARE AWARDED. INCREASE CUSTOMER RETNETION/SATISFACTION, PRODUCT CROSS-SELLING, NEW PRODUCT & SERVICE  AWARENESS', ENROLL_DATE__c = '20070101', ACTIVE_FLAG__c = 'Y');
            enrollmentList.add(testc);
            }
            else {
            //KLafortune 2/5/2016.  Modified query to match Beacon behavior.  It only shows:  VL,CO,EQ,PD,RA,RL,SU[01078].  
            enrollmentList = [SELECT PROG_ID__c, PROG_NAME__c, ENROLL_DATE__c , TERM_DATE__c from BRF_ENROLL__x WHERE KUNNR__c =:SAPNumberWithLeadingZeros AND ACTIVE_FLAG__c = 'Y' AND (PROG_CAT__c = 'RA' OR PROG_CAT__c = 'RL' OR PROG_CAT__c = 'VL' OR PROG_CAT__c = 'CO' OR PROG_CAT__c = 'EQ' OR PROG_CAT__c = 'PD' OR (PROG_CAT__c = 'SU' AND PROG_ID__c = '01078')) AND TERM_DATE__c>=: formattedDt];
            }
            isexception = enrollmentlist.size() == 0 ? true : false; 
            }
            catch(Exception e) {
            isexception = true;
            }
            if(enrollmentList!=null) {
            //Initialize Wrapper instance 
                for(BRF_ENROLL__x brf : enrollmentList){
                    DisplayWrapper disWrap = new DisplayWrapper();
                    disWrap.progId = brf.PROG_ID__c;
                    disWrap.progName = brf.PROG_NAME__c;
                    
                    if(brf.ENROLL_DATE__c != null){
                        String year = brf.ENROLL_DATE__c.left(4);
                        string day = brf.ENROLL_DATE__c.right(2);
                        string month = brf.ENROLL_DATE__c.substring(4,6);
                        disWrap.enrollDate = Date.NewInstance(Integer.Valueof(year),Integer.Valueof(month),Integer.Valueof(day));
                    }
                    if(brf.TERM_DATE__c != null){
                        String year = brf.TERM_DATE__c.left(4);
                        string day = brf.TERM_DATE__c.right(2);
                        string month = brf.TERM_DATE__c.substring(4,6);
                        disWrap.termDate = Date.NewInstance(Integer.Valueof(year),Integer.Valueof(month),Integer.Valueof(day));
                    }
                    if(disWrap.termDate>= Date.Today())
                        enrolWrapList.add(disWrap);
                }
            
           }else {
           isexception = true;
           }
        }else {
        isSapNoNull = true;
        }
       
    }
    //Wrapper class to convert the date format from Number to String
    public class DisplayWrapper{ 
        public string progId{get;set;}
        public string progName{get;set;}
        public Date enrollDate{get;set;}
        public Date termDate{get;set;}
       
     public DisplayWrapper() {
     
     progId = ''; 
     progName = ''; 
     enrollDate = System.today(); 
     termDate = System.today();
     
     }
        
    }
}