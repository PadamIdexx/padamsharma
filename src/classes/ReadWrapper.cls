public class ReadWrapper extends SAPOrderWrapper {
    
    public override SAPOrder execute(SAPOrder order) {
        OrderReadRequest.SalesOrderReadRequest request;
        OrderReadResponse.SalesOrderReadResponse response;
		SAPOrder readOrder;
        
        //transform SAPOrder to request
        try {
			request = OrderReadRequest.transform(order);                
        } catch(Exception e) {
            System.debug('Error transforming SAPOrder to OrderReadRequest.');
            System.debug(e.getCause());
            System.debug(e.getMessage());
            
        	throw e;    
        }     
        
        //call service
        try {
            OrderReadService service = new OrderReadService();
            
            if (!Test.isRunningTest()) {
                response = service.getOrder(request);
            } else {
                response = OrderTestDataFactory.createReadResponse();
            }    
       } catch(Exception e) {
            System.debug('Error calling Order Read Service.');
            System.debug(e.getCause());
            System.debug(e.getMessage());
            
        	throw e;             
        }      
        
        //transform response into new SAPOrder
        try {
        	readOrder = OrderReadResponse.transform(order, response);        
        } catch(Exception e) {
            System.debug('Error transforming OrderReadResponse to SAPOrder.');
            System.debug(e.getCause());
            System.debug(e.getMessage());
            
        	throw e;              
        }        
        
        return readOrder;
        
	}


}