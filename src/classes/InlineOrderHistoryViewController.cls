/**
*       @author         Sirisha Kodi
*       @date           21/10/2015
        @description    This class is used to display order history records in an inline VF page.
        Function:       Display Order History records in an inline VF page and in expanded view which will be launched on click of a button
                        in the Account detail page.
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Sirisha Kodi                 19/11/2015          Modified the class to fix the view state issue TKT-000166
        Anudeep Gopagoni             07/01/2015          Replaced V_ORDER_HISTORY__X with ORDER_HISTORY__X as per kevin's comments in US-0344
        Anudeep Gopagoni             08/01/2015          Added wrapper logic to format date/time
        Anudeep Gopagoni             11/01/2015          sorted created on date from newest to oldest
        Anudeep Gopagoni             12/01/2015          Excluded Lab Services (CAS7, USS3) from Sales Org as per maria's request
        Anudeep Gopagoni             15/01/2015          Added Logic to Show/Hide hyperlinks for Order#
        Anudeep Gopagoni             20/01/2015          Added a button 'View Labs History' and a List View as per TKT-00355
        Anudeep Gopagoni             03/02/2016          commented unused variables, removed unnecessary fields in SOQL and indented code
        Anudeep Gopagoni             09/02/2016          Fixed order grouping related issue reported by melanie on 08/02/2016
        Anudeep Gopagoni             09/02/2016          Removed debug statements from the code
        Robin Fortney                06/27/2016          Added "DESC" to ORDER BY statements to the SOQL statements in viewLabServices1 per DE8436
**/
public class InlineOrderHistoryViewController{
    //properties

    public ProfileDisplayWrapper					 profileHelper			{get;set;}

    public List<ORDER_HISTORY__x>                    OrderHistLst          {get;set;} // List of order history records
    public String                                    errorMsg              {get;set;} // String to display user friendly message where there is no data
    public String                                    displayLength         {get;set;}
    public List<String>                              ordersSorted          {get;set;} // List to sort order history records
    public ORDER_HISTORY__x                          tmpOrder              {get;set;} // used to convert map to Object
    public transient List<ORDER_HISTORY__x>          LabsLst               {get;set;} // List to store order history for specific lab services (USS3, CAS7)
    public List<String>                              insertStyleToOrder    {get;set;} // List to highlight duplicate order history records
    public String                                    selectedDate          {get;set;} // String to store selected value from date filter
    public String                                    enableLabsHistListView{get;set;} // String to show/hide buttons
    public String                                    formattedDt1          {get;set;} // String to convert date format used in Beacon to SFDC date format
    public static Date                                  d                  {get;set;} // Used for date conversion
    public List<SelectOption>                        options;                         // Used to display values in date dropdown
    public List<SelectOption>                        wateroptions;                    // Used to display values in date dropdown
    public List<SelectOption>                        optionsForLabs;                  // Used to display values in date dropdown
    public transient List<DisplayWrapper>            styleOrdersWrapper    {get;set;} // List to fetch external object data
    public boolean                                   wrapperSizeCheck      {get;set;} // boolean used to perform a size check to show/hide the order confirmation buttons in the VF Page
    public boolean                                   LabsSizeCheck         {get;set;}
    public boolean                                   isNotUnique;
    public set<String>                               checkIfUnique;
    //local variables
    private ApexPages.StandardController            controller;
    public static Map<String, sObject>              styleMap                {get; set;}// Map used to sort order history records from newest to oldest
    public Account                                  currentAccountRecord = new Account();
    public static final String                      TECHNICAL_DETAILS = 'Technical Details : ';
    private final static String SUBSTITUTION_TEXT = '{0}';
    public   static List<DisplayWrapper>  ordWrapperList {get;set;}//List to fetch external object data
    public String SalesOrg1;
    public String SalesOrg2;
    public Integer limitOrdRecords;
    public static final String SOLD_TO_CODE = Label.Sold_To_Code;
    public static final String SHIP_TO_CODE = Label.Ship_To_Code;
    List<Idexx_Parameter__mdt> excludeSalesOrg = [SELECT Value__c,MasterLabel FROM Idexx_Parameter__mdt WHERE (MasterLabel = 'SalesOrg1') OR (MasterLabel = 'SalesOrg2') OR (MasterLabel = 'LIMIT ORDER HISTORY RECORDS')];

    Map<String, String> trackingURL;
    //As per TKT-000568
    public String selectedOrderByType {get;set;}
    public String truncatedSAPNO {get;set;}
    public string accountName {get;set;}
    public List<SelectOption> getOrderbyTypes() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Sold To','Sold To'));
        options.add(new SelectOption('Ship To','Ship To'));
        return options;
    }
    public List<Partner_Function__c> partnerAccountsList {get;set;}
    public String selectedPartnerType {get;set;}
    /*
    public List<SelectOption> getPartnerAccounts() {
        List<SelectOption> options = new List<SelectOption>();
        if(partnerAccountsList.size() >0) {
            for(Partner_Function__c prtAccount : partnerAccountsList) {
                options.add(new SelectOption(String.valueOf(Integer.valueOf(prtAccount.Partner_Account__r.SAP_Customer_Number__c))+'-'+prtAccount.Partner_Account__r.Name+'-'+prtAccount.Division__c,prtAccount.Partner_Account__r.SAP_Customer_Number__c+'-'+prtAccount.Partner_Account__r.Name));
            }
        }

        return options;
    }*/
    public String selectedType {get;set;}
    Map<String,Partner_Function__c> parterAccountMap = new Map<String,Partner_Function__c>();
    
   public List<SelectOption> typeOptions = new List<SelectOption>();
    public List<SelectOption> getTypes() {
        //typeOptions = new List<SelectOption>();
        if(partnerAccountsList.size() >0) {
        for(Partner_Function__c prtAccount : partnerAccountsList) {
               checkIfUnique.add(prtAccount.SAP_Customer_Number__c);
               if(checkIfUnique.size()>1) {
               isNotUnique = true;
               }
           }
         }

        if(partnerAccountsList.size() >0) {

            for(Partner_Function__c prtAccount : partnerAccountsList) {
                if(String.isNotBlank(String.valueOf(prtAccount.Partner_Account__c))) {
                    if( !parterAccountMap.containsKey(prtAccount.Partner_Account__c+':'+prtAccount.Partner_Function__c)&& prtAccount.Partner_Function__c == SOLD_TO_CODE ) {
                        parterAccountMap.put(prtAccount.Partner_Account__c+':'+prtAccount.Partner_Function__c,prtAccount);
                        if(String.isNotBlank(prtAccount.partner_Account__r.SAP_Customer_Number__c )){
                            typeOptions.add(new SelectOption(prtAccount.Partner_Account__c +':'+prtAccount.Partner_Function__c,'SOLD TO:'+  String.valueOf(Integer.valueOf(prtAccount.partner_Account__r.SAP_Customer_Number__c))+'-'+prtAccount.Partner_Account__r.Name));
                        }
                    }
                    else if(!parterAccountMap.containsKey(prtAccount.Partner_Account__c+':'+prtAccount.Partner_Function__c)&& prtAccount.Partner_Function__c == SHIP_TO_CODE ) {
                        parterAccountMap.put(prtAccount.Partner_Account__c+':'+prtAccount.Partner_Function__c,prtAccount);
                        if( String.isNotBlank(prtAccount.partner_Account__r.SAP_Customer_Number__c ) && prtAccount.partner_Account__r.SAP_Customer_Number__c !=prtAccount.SAP_Customer_Number__c && isNotUnique == true) {
                        typeOptions.add(new SelectOption(prtAccount.Partner_Account__c +':'+prtAccount.Partner_Function__c,'SOLD TO:'+  String.valueOf(Integer.valueOf(prtAccount.SAP_Customer_Number__c))+'-'+prtAccount.Partner_Account__r.Name));
                        }
                     parterAccountMap.put(prtAccount.Partner_Account__c+':'+prtAccount.Partner_Function__c,prtAccount);
                     typeOptions.add(new SelectOption(prtAccount.Partner_Account__c +':'+prtAccount.Partner_Function__c,'SHIP TO:'+  String.valueOf(Integer.valueOf(prtAccount.partner_Account__r.SAP_Customer_Number__c))+'-'+prtAccount.Partner_Account__r.Name));
                    }
                }
                //options.add(new SelectOption(prtAccount.Partner_Account__r.SAP_Customer_Number__c+'-'+prtAccount.Partner_Account__r.Name+'-'+prtAccount.Division__c,prtAccount.Partner_Account__r.SAP_Customer_Number__c+'-'+prtAccount.Partner_Account__r.Name+'-'+prtAccount.Division__c));
            }
        }
        
        return typeOptions;
    }
    public void setPartnerAccounts(String customer) {

        String soqlQuery = 'Select Id,Name,Account__c, Division__c, Partner_Account__c,Partner_Account__r.Name,Partner_Account__r.SAP_Customer_Number__c,Partner_Function__c,Salesview__c,Sales_Organization__c,SAP_Customer_Number__c From Partner_Function__c ';
        String conditions = '';
        conditions = ' WHERE (SAP_Customer_Number__c =:customer OR Business_Partner_Custom_Number__c=:customer) AND (Partner_Function__c = \'WE\' OR Partner_Function__c = ' +'\'AG\') ORDER BY Partner_Function__c ';

        soqlQuery += conditions;

        partnerAccountsList = (List<Partner_Function__c>) Database.query(soqlQuery);

    }
    //drop down to display date values, which allows users to select from the Inline Order History view
    public List<SelectOption> getOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('30','Last 30 days'));
        options.add(new SelectOption('90','90 days'));
        options.add(new SelectOption('183','6 months'));
        options.add(new SelectOption('365','1 Year'));
        return options;
    }
    //drop down to display date values, which allows users to select from the expanded Order History view
    public List<SelectOption> getOptionsForLabs() {
        List<SelectOption> options = new List<SelectOption>();
         options.add(new SelectOption('30','Last 30 days'));
        options.add(new SelectOption('90','90 days'));
        options.add(new SelectOption('183','6 months'));
        options.add(new SelectOption('365','1 Year'));
        return options;
    }
    
      public List<SelectOption> getOptionsForwater() {
        List<SelectOption> wateroptions = new List<SelectOption>();
        wateroptions.add(new SelectOption('30','Last 30 days'));
        wateroptions.add(new SelectOption('90','90 days'));
        wateroptions.add(new SelectOption('183','6 months'));
        wateroptions.add(new SelectOption('365','1 Year'));
        wateroptions.add(new SelectOption('730','2 Years'));
        wateroptions.add(new SelectOption('1095','3 Years'));
        
        return wateroptions;
    }

    public InlineOrderHistoryViewController(ApexPages.StandardController controller){    
		//setup the profile helper so that we can determine what kind of user this is
		//for various display enabling/disabling in the VF page
		profileHelper = new ProfileDisplayWrapper();
        
        checkIfUnique = new Set<String>();
        isNotUnique = false;
        styleOrdersWrapper = new List<DisplayWrapper>();
        wrapperSizeCheck   = false;
        LabsSizeCheck      = false;
        selectedDate       = '30';
        selectedOrderByType = 'Sold To';
        selectedType = '';
        d = system.today().adddays(-(Integer.valueOf(selectedDate)));
        String st1 = String.valueOf(d);
        String monthExtracted= st1.substring(5,7);
        String dateExtracted = st1.substring(8,10);
        formattedDt1 = String.valueOf(d.year())+String.valueOf(monthExtracted)+String.valueOf(dateExtracted);
        enableLabsHistListView = ApexPages.currentPage().getParameters().get('dropDown');

        LabsLst            = new List<ORDER_HISTORY__x>();
        styleMap           = new Map<String, sObject>();
        ordersSorted       = new List<String>();
        insertStyleToOrder = new List<String>();
        // Get 'USS3' and 'CAS7' Lab Services values from custom metadata type and store it in the strings 'SalesOrg1' and 'SalesOrg2'
        if(!excludeSalesOrg.isEmpty()){
            for(Idexx_Parameter__mdt oParameter : excludeSalesOrg ){
                if(oParameter.MasterLabel == 'SalesOrg1'){
                    SalesOrg1 = oParameter.Value__c;
                }
                else if(oParameter.MasterLabel == 'SalesOrg2'){
                    SalesOrg2 = oParameter.Value__c;
                }
                else if(oParameter.MasterLabel == 'LIMIT ORDER HISTORY RECORDS'){
                    limitOrdRecords = Integer.valueOf(oParameter.Value__c);
                }
            }
        }
        ordWrapperList = new List<DisplayWrapper>();
        //Initializations
        if(!Test.isRunningTest()){
            controller.addFields(new List<string> {'SAP_CUSTOMER_NUMBER__c'});
       }
        this.controller           = controller;
        currentAccountRecord      = (Account)controller.getRecord();
        if(enableLabsHistListView == 'true') {
            LabsLst = [SELECT SalesOrder__c, SalesOrderItem__c,PONumber__c, Material__c, CreatedBy__c, MaterialDesc__c,OrderQty__c, BatchNum__c , Price__c,Plant__c,ShipTo__c, SalesOrg__c, DeliveryItem__c, CarrierType__c,IncoTerm1__c,POTypeDesc__c,POType__c,SalesDocTyp__c
             FROM ORDER_HISTORY__x WHERE SoldTo__c=:currentAccountRecord.SAP_CUSTOMER_NUMBER__c AND (SalesOrg__c=:SalesOrg1  OR SalesOrg__c=:SalesOrg2 ) AND (Material__c LIKE '%UC%' OR Material__c LIKE '%CAB%') AND CreatedOn__c>=:formattedDt1 ORDER BY CreatedOn__c DESC LIMIT :limitOrdRecords];
        }
        trackingURL = new Map<String, String>();
        if(System.currentPageReference().getParameters().get('dropDown')!= null) {
            displayLength = 'true';
        }
        else {
            displayLength = 'false';
        }
        if(this.currentAccountRecord<>null && currentAccountRecord.Id<>null){
            try{
                trackingURL = new Map<String, String>();
                // fetch URLs for tracking numbers from custom metadata
                for(ORDER_TRACKING_URL__mdt cUrl : [Select DeveloperName, MasterLabel, URL__c FROM ORDER_TRACKING_URL__mdt WHERE URL__c != NULL]){
                    trackingURL.put(cUrl.MasterLabel, cUrl.URL__c);
                }
                // query for the SAP customer number on Account
                this.currentAccountRecord = [Select Id, SAP_CUSTOMER_NUMBER__c,Name from Account where Id=:currentAccountRecord.Id];
                truncatedSAPNO = currentAccountRecord.SAP_CUSTOMER_NUMBER__c;
                accountName = currentAccountRecord.Name;
                //Initialize the List
                OrderHistLst = new List<ORDER_HISTORY__x>();
                partnerAccountsList = new List<Partner_Function__c>();
                setPartnerAccounts(currentAccountRecord.SAP_Customer_Number__c);
                if(!ApexPages.currentPage().getParameters().containsKey('dropDown') || ApexPages.currentPage().getParameters().get('dropDown') =='false') {
                    if(currentAccountRecord.SAP_CUSTOMER_NUMBER__c<> null){
                        OrderHistLst = [SELECT SalesOrder__c, SalesOrderItem__c, Delivery__c,Division__c,CreatedOn__c, DelvCreatedOn__c, TrackNum__c,
                                    PONumber__c, Material__c, CreatedBy__c, MaterialDesc__c,OrderQty__c,
                                    BatchNum__c , CarrierType__c, POType__c, Price__c,Plant__c,ShipTo__c, PGIDate__c,
                                    IncoTerm1__c,POTypeDesc__c,SalesDocTyp__c,SDocTypDesc__c FROM ORDER_HISTORY__x WHERE SoldTo__c=:currentAccountRecord.SAP_CUSTOMER_NUMBER__c AND SalesOrg__c!=:SalesOrg1 AND SalesOrg__c!=:SalesOrg2 AND CreatedOn__c>=:formattedDt1 ORDER BY CreatedOn__c DESC
                                    LIMIT :limitOrdRecords];
                                    
                        reGroupOrders(OrderHistLst);
                    }
                }
                else {
                    errorMsg = TECHNICAL_DETAILS+system.Label.Order_History_Custom_Message;
                }

            } catch(Exception ex){
                errorMsg = TECHNICAL_DETAILS+ex.getMessage();
            }
        }

    }

    public DateTime getDate(String dt) {
        return Date.NewInstance(Integer.Valueof(dt.left(4)),Integer.Valueof(dt.subString(4,6)),Integer.Valueof(dt.right(2)));
    }
    public DisplayWrapper formatThisOrderHistory(ORDER_HISTORY__x oHistory) {
        DisplayWrapper disWrap = new DisplayWrapper();
        String year                         =  '';
        String month                        = '';
        String day                          = '';
        disWrap.deliveryCreatedOn           = oHistory.DelvCreatedOn__c != null ? getDate(oHistory.DelvCreatedOn__c) : null;
        disWrap.createdDate                 = oHistory.CreatedOn__c != null ? getDate(oHistory.CreatedOn__c) : null;
        disWrap.ordershippingdate           = oHistory.PGIDate__c != null ? getDate(oHistory.PGIDate__c) : null;
        disWrap.salesOrder                  = oHistory.SalesOrder__c;
        disWrap.salesOrderItem              = oHistory.SalesOrderItem__c;
        disWrap.delivery                    = oHistory.Delivery__c;
        disWrap.trackingNum                 = oHistory.TrackNum__c;
        disWrap.poNumber                    = oHistory.PONumber__c;
        disWrap.material                    = oHistory.Material__c;
        disWrap.materialDesc                = oHistory.MaterialDesc__c;
        disWrap.orderQty                    = oHistory.OrderQty__c;
        disWrap.batchNum                    = oHistory.BatchNum__c ;
        disWrap.price                       = oHistory.Price__c;
        disWrap.plant                       = oHistory.Plant__c;
        disWrap.ShipToTrimmed               = String.valueOf(Integer.valueOf(oHistory.ShipTo__c));
        disWrap.createdBY                   = oHistory.CreatedBy__c;
        disWrap.IncoTerm1                   = oHistory.IncoTerm1__c;
        disWrap.POTypeDesc                  = oHistory.POTypeDesc__c;
        disWrap.POType                      = oHistory.POType__c;
        disWrap.SalesDocTyp                 = oHistory.SalesDocTyp__c;
        disWrap.SDocTypDesc                 = oHistory.SDocTypDesc__c;
        return disWrap;
    }
    // Method that contains logic to sort data from newest to oldest and to hightlight rows when there is duplicate data
    public void reGroupOrders(List<ORDER_HISTORY__x> OrderHistLst){
        try{
            List<DisplayWrapper> tempWrapper = new List<DisplayWrapper>();
            styleOrdersWrapper = new List<DisplayWrapper>();
            ordWrapperList = new List<DisplayWrapper>();
            DisplayWrapper disWrap = new DisplayWrapper();
            List<String> uniqVal = new List<String>();
            List<String> sortedOrdersList = new List<String>();
            styleMap = new Map<String,sObject>();
            Integer inc = 0;
            for(ORDER_HISTORY__x oh: OrderHistLst ){
                if(oh.CreatedOn__c != null){
                    String year = oh.CreatedOn__c .left(4);
                    string day = oh.CreatedOn__c .right(2);
                    string month = oh.CreatedOn__c .substring(4,6);
                    disWrap = formatThisOrderHistory(oh);
                    ordWrapperList.add(disWrap);
                    uniqVal.add(oh.CreatedOn__c +':'+ oh.SalesOrder__c +':'+inc);
                    styleMap.put(oh.CreatedOn__c +':'+ oh.SalesOrder__c +':'+inc , oh);
                    inc++;
                }
            }
            uniqVal.sort();
            for(Integer k = uniqVal.size()-1; k>=0;k--) {
                sortedOrdersList.add(uniqVal.get(k));
            }
            //sortedOrdersList.sort();
            for(String eachOrderedHistory : sortedOrdersList) {
                tempWrapper.add(processThisOrder(eachOrderedHistory));
            }
            //logic to hightlight row if there is duplicate data
            String prevOrderNumber = '';
            String prevOrderType = '';
            Integer j=1;
            for(DisplayWrapper dw : tempWrapper ) {

                if(j==1){
                    dw.rowType = 'Odd';
                    dw.style = 'background-color:#fff;';
                    prevOrderNumber = dw.salesOrder;
                    prevOrderType = 'Odd';
                }
                else if(prevOrderNumber == dw.salesOrder  && prevOrderType == 'Odd'){
                    dw.rowType = 'Odd';
                    dw.style = 'background-color:#fff;';
                    prevOrderNumber = dw.salesOrder;
                    prevOrderType = 'Odd';
                }
                else if(prevOrderNumber == dw.salesOrder  && prevOrderType == 'Even'){
                    dw.rowType = 'Even';
                    dw.style = 'background-color:#FFC;';
                    prevOrderNumber = dw.salesOrder;
                    prevOrderType = 'Even';
                }
                else if(prevOrderNumber != dw.salesOrder && prevOrderType =='Odd'  ) {
                    dw.rowType = 'Even';
                    dw.style = 'background-color:#FFC;';
                    prevOrderNumber = dw.salesOrder;
                    prevOrderType = 'Even';
                }
                else if(prevOrderNumber != dw.salesOrder && prevOrderType =='Even'  ) {
                    dw.rowType = 'Odd';
                    dw.style = 'background-color:#fff;';
                    prevOrderNumber = dw.salesOrder;
                    prevOrderType = 'Odd';
                }
                styleOrdersWrapper.add(dw);
                if(styleOrdersWrapper.size()>0&&!styleOrdersWrapper.isEmpty()) {
                WrappersizeCheck = true;
                }
                j++;
            }
        } catch(Exception ex){
            errorMsg = TECHNICAL_DETAILS+ex.getMessage();
        }
    }
    public DisplayWrapper processThisOrder(String uniq) {
        tmpOrder = new Order_History__x();
        String oNo = uniq.subString(0,10);
        String dDate = uniq.subString(10,uniq.length());
        tmpOrder = (ORDER_HISTORY__x) styleMap.get(uniq);
        DisplayWrapper disWrap1 = new DisplayWrapper();
        disWrap1.salesOrder       = tmpOrder.SalesOrder__c;
        disWrap1.salesOrderItem   = tmpOrder.SalesOrderItem__c;
        disWrap1.delivery         = tmpOrder.Delivery__c;
        disWrap1.trackingNum      = tmpOrder.TrackNum__c;
        disWrap1.poNumber         = tmpOrder.PONumber__c;
        disWrap1.material         = tmpOrder.Material__c;
        disWrap1.materialDesc     = tmpOrder.MaterialDesc__c;
        disWrap1.orderQty         = tmpOrder.OrderQty__c;
        disWrap1.batchNum         = tmpOrder.BatchNum__c ;
        disWrap1.price            = tmpOrder.Price__c;
        disWrap1.plant            = tmpOrder.Plant__c;
        disWrap1.ShipToTrimmed    = String.valueOf(Integer.valueOf(tmpOrder.ShipTo__c));
        disWrap1.createdBY        = tmpOrder.CreatedBy__c;
        disWrap1.sendEmail        = '';
        disWrap1.IncoTerm1        = tmpOrder.IncoTerm1__c;
        disWrap1.carrierURL       = getTrackingUrl(tmpOrder.CarrierType__c, tmpOrder.TrackNum__c);
        if(tmpOrder.POType__c == 'BEA'){
            disWrap1.POTypeDesc = 'Beacon';
        } else if(tmpOrder.POType__c  == 'B2B'){
            disWrap1.POTypeDesc = 'eCommerce';
        }
        else if(tmpOrder.POType__c  == 'B2B1'){
            disWrap1.POTypeDesc = 'ScheduledOrder';
        }
        else if(tmpOrder.POType__c  == 'CRM'){
            disWrap1.POTypeDesc = 'Salesforce';
        }else if(tmpOrder.POType__c  == null || tmpOrder.POType__c == ''){
            disWrap1.POTypeDesc = 'Other';}
        else {
            disWrap1.POTypeDesc = 'Other';
        }
        disWrap1.POType = tmpOrder.POType__c;
        disWrap1.SalesDocTyp = tmpOrder.SalesDocTyp__c;
        disWrap1.SDocTypDesc = tmpOrder.SDocTypDesc__c;

        // convert date to MM-DD-YYYY format
        if(tmpOrder.CreatedOn__c != null){
            String year          = tmpOrder.CreatedOn__c .left(4);
            string day           = tmpOrder.CreatedOn__c .right(2);
            string month         = tmpOrder.CreatedOn__c .substring(4,6);
            disWrap1.createdDate = Date.NewInstance(Integer.Valueof(year),Integer.Valueof(month),Integer.Valueof(day));
        }
        if(tmpOrder.DelvCreatedOn__c!= null){
            String year                = tmpOrder.DelvCreatedOn__c.left(4);
            string day                 = tmpOrder.DelvCreatedOn__c.right(2);
            string month               = tmpOrder.DelvCreatedOn__c.substring(4,6);
            disWrap1.deliveryCreatedOn = Date.NewInstance(Integer.Valueof(year),Integer.Valueof(month),Integer.Valueof(day));
        }
        if(tmpOrder.PGIDate__c!= null){
            String year                = tmpOrder.PGIDate__c.left(4);
            string day                 = tmpOrder.PGIDate__c.right(2);
            string month               = tmpOrder.PGIDate__c.substring(4,6);
            disWrap1.ordershippingdate = Date.NewInstance(Integer.Valueof(year),Integer.Valueof(month),Integer.Valueof(day));
        }
        return disWrap1;
    }

    //Constuctor

    // Order History with labservices (USS3, CAS7)
    public void viewLabServices(){
        styleMap = new Map<String, sObject>();
        d = system.today().adddays(-(Integer.valueOf(selectedDate)));
        String st1 = String.valueOf(d);
        String monthExtracted= st1.substring(5,7);
        String dateExtracted = st1.substring(8,10);
        formattedDt1= String.valueOf(d.year())+String.valueOf(monthExtracted)+String.valueOf(dateExtracted);
        styleOrdersWrapper = new List<DisplayWrapper>();
        LabsLst = new List<ORDER_HISTORY__X>();
        LabsLst = [SELECT SalesOrder__c,SalesOrderItem__c,Division__c,PGIDate__c,Delivery__c,DelvCreatedOn__c,CreatedOn__c ,TrackNum__c,CarrierType__c,PONumber__c, Material__c, CreatedBy__c, MaterialDesc__c,OrderQty__c, BatchNum__c , Price__c,Plant__c,ShipTo__c,IncoTerm1__c,POTypeDesc__c,POType__c,SalesDocTyp__c, SDocTypDesc__c FROM ORDER_HISTORY__x WHERE SoldTo__c=:currentAccountRecord.SAP_CUSTOMER_NUMBER__c AND (SalesOrg__c=:SalesOrg1  OR SalesOrg__c=:SalesOrg2 ) AND (Material__c LIKE '%UC%' OR Material__c LIKE '%CAB%') AND CreatedOn__c>=:formattedDt1 LIMIT :limitOrdRecords];
        if(LabsLst.isEmpty()) {
        labsSizeCheck = true;
        }
        reGroupOrders(LabsLst);
    }
      // Order History without labservices (USS3, CAS7)
    public void viewLabServices1(){
        styleOrdersWrapper = new List<DisplayWrapper>();
        d = system.today().adddays(-(Integer.valueOf(selectedDate)));
        String st1 = String.valueOf(d);
        String monthExtracted= st1.substring(5,7);
        String dateExtracted = st1.substring(8,10);
        formattedDt1= String.valueOf(d.year())+String.valueOf(monthExtracted)+String.valueOf(dateExtracted);
        styleMap = new Map<String, sObject>();
        styleOrdersWrapper.clear();
        wrapperSizeCheck = false;
        
        setPartnerAccounts(currentAccountRecord.SAP_Customer_Number__c);
        getTypes();
        List<ORDER_HISTORY__x> LabsLst1 = new List<ORDER_HISTORY__x>();
        String soqlQuery = 'SELECT SalesOrder__c,SalesOrderItem__c,Delivery__c,Division__c,CreatedOn__c, DelvCreatedOn__c, CarrierType__c, TrackNum__c,PONumber__c, Material__c, CreatedBy__c, MaterialDesc__c,OrderQty__c, BatchNum__c , Price__c,Plant__c,ShipTo__c, PGIDate__c,IncoTerm1__c,POTypeDesc__c,POType__c,SalesDocTyp__c, SDocTypDesc__c FROM ORDER_HISTORY__x ';
        String conditions = '';
        if( String.isNotBlank(selectedType) && selectedType.endsWith(SOLD_TO_CODE) ) {
            Partner_Function__c pf = parterAccountMap.get(selectedType);
            truncatedSAPNO = String.valueOf(Integer.valueOf(pf.Partner_Account__r.SAP_Customer_Number__c));
            accountName = pf.Partner_Account__r.Name;
            conditions += ' WHERE (SoldTo__c=\'' + pf.Partner_Account__r.SAP_CUSTOMER_NUMBER__c + '\' OR ShipTo__c=\'' + pf.Partner_Account__r.SAP_Customer_Number__c + '\') AND SalesOrg__c !=\'' + SalesOrg2 + '\' AND CreatedOn__c >= \'' +formattedDt1+'\' ORDER BY CreatedOn__c DESC LIMIT ' +limitOrdRecords ;
        }
        else if(  String.isNotBlank(selectedType) && selectedType.endsWith(SHIP_TO_CODE) ){
            Partner_Function__c pf = parterAccountMap.get(selectedType);
            truncatedSAPNO = String.valueOf(Integer.valueOf(pf.Partner_Account__r.SAP_Customer_Number__c));
            accountName = pf.Partner_Account__r.Name;
            conditions += ' WHERE (SoldTo__c=\'' + pf.Partner_Account__r.SAP_CUSTOMER_NUMBER__c + '\' OR ShipTo__c=\'' + pf.Partner_Account__r.SAP_Customer_Number__c + '\') AND SalesOrg__c !=\'' +SalesOrg1 +'\' AND SalesOrg__c !=\'' + SalesOrg2 + '\' AND CreatedOn__c >= \'' +formattedDt1+'\' ORDER BY CreatedOn__c DESC LIMIT ' +limitOrdRecords ;
        }
        else{
            if(currentAccountRecord.SAP_Customer_Number__c!=null)
            {
                truncatedSAPNO = String.valueOf(Integer.valueOf(currentAccountRecord.SAP_Customer_Number__c));
            }
            accountName = currentAccountRecord.Name;
            conditions += ' WHERE SoldTo__c=\'' + currentAccountRecord.SAP_CUSTOMER_NUMBER__c + '\' AND SalesOrg__c !=\'' +SalesOrg1 +'\' AND SalesOrg__c !=\'' + SalesOrg2 + '\' AND CreatedOn__c >= \'' +formattedDt1+'\' ORDER BY CreatedOn__c DESC LIMIT ' +limitOrdRecords ;
        }
        /*
        if( String.isNotBlank(selectedType) == 'Sold To') {
            conditions += ' WHERE SoldTo__c=\'' + currentAccountRecord.SAP_CUSTOMER_NUMBER__c + '\' AND SalesOrg__c !=\'' +SalesOrg1 +'\' AND SalesOrg__c !=\'' + SalesOrg2 + '\' AND CreatedOn__c >= \'' +formattedDt1+'\' ORDER BY CreatedOn__c LIMIT ' ;
            //SoldTo__c=:currentAccountRecord.SAP_CUSTOMER_NUMBER__c AND SalesOrg__c!=:SalesOrg1 AND SalesOrg__c!=:SalesOrg2 AND CreatedOn__c>=:formattedDt1 ORDER BY CreatedOn__c LIMIT :limitOrdRecords
        }
        else{
            conditions += ' WHERE ShipTo__c=\'' + currentAccountRecord.SAP_CUSTOMER_NUMBER__c + '\' AND SalesOrg__c !=\'' +SalesOrg1 +'\' AND SalesOrg__c !=\'' + SalesOrg2 + '\' AND CreatedOn__c >= \'' +formattedDt1+'\' ORDER BY CreatedOn__c LIMIT '  ;
        }
        */
        conditions = conditions == '' ? ' limit '+limitOrdRecords  : conditions;
        soqlQuery += conditions;
        //LabsLst1 = [SELECT SalesOrder__c,Delivery__c,CreatedOn__c , DelvCreatedOn__c, CarrierType__c, TrackNum__c,PONumber__c, Material__c, CreatedBy__c, MaterialDesc__c,OrderQty__c, BatchNum__c , Price__c,Plant__c,ShipTo__c, PGIDate__c,IncoTerm1__c,POTypeDesc__c,POType__c,SalesDocTyp__c, SDocTypDesc__c FROM ORDER_HISTORY__x WHERE SoldTo__c=:currentAccountRecord.SAP_CUSTOMER_NUMBER__c AND SalesOrg__c!=:SalesOrg1 AND SalesOrg__c!=:SalesOrg2 AND CreatedOn__c>=:formattedDt1 ORDER BY CreatedOn__c LIMIT :limitOrdRecords];
        LabsLst1 = (List<ORDER_HISTORY__X>) Database.query(soqlQuery);
        reGroupOrders(LabsLst1);
        //setPartnerAccounts();
    }
    // method to fetch tracking url
    private String getTrackingUrl(String carrier, String trackingNum) {

        String url = null;

        if (String.isEmpty(carrier)) {

            return url;
        }
        if (String.isEmpty(trackingNum)) {

            return url;
        }
        if (null != trackingURL) {
            if(!trackingURL.containsKey(carrier)){
                url = null;
                return url;
            }else{
                url = trackingURL.get(carrier);
                url = url.replace(SUBSTITUTION_TEXT, trackingNum);
            }
        }
        else {

        }
        return url;
    }
    
    // Wrapper class
    public class DisplayWrapper{
    // Wrapper class constructor
        public DisplayWrapper() {
            rowselected = '';
            rowType     = '';
            style       = '';
            style1      = 'background-color:#FFF;';
            style2      = 'background-color:#FFC;';
            carrierURL  = '';
            sendEmail   = '';
            carrierURL  = '';
            delivery    = '';
            trackingNum = '';
            poNumber    = '';
            material    = '';
            materialDesc= '';
            orderQty    = 0;
            batchNum    = '';
            price       = 0;
            plant       ='';
            ShipToTrimmed ='';
            salesOrder  = '';
            CreatedBy   = '';
            IncoTerm1   = '';
            POTypeDesc  = '';
            POType      = '';
            SalesDocTyp = '';
            SDocTypDesc= '';
            salesOrderItem = '';

        }
        public String     sendEmail             {get;set;}
        public String     carrierURL            {get;set;}
        public string     rowType               {get;set;}
        public string     delivery              {get;set;}
        public string     trackingNum           {get;set;}
        public string     poNumber              {get;set;}
        public string     material              {get;set;}
        public string     materialDesc          {get;set;}
        public DateTime   deliveryCreatedOn     {get;set;}
        public Decimal    orderQty              {get;set;}
        public string     batchNum              {get;set;}
        public Decimal    price                 {get;set;}
        public string     plant                 {get;set;}
        public string     ShipToTrimmed         {get;set;}
        public string     salesOrder            {get;set;}
        public DateTime   createdDate           {get;set;}
        public string     CreatedBy             {get;set;}
        public string     IncoTerm1             {get;set;}
        public string     POTypeDesc            {get;set;}
        public string     POType                {get;set;}
        public string     SalesDocTyp           {get;set;}
        public string     SDocTypDesc           {get;set;}
        public DateTime   ordershippingdate     {get;set;}
        public String     style1                {get;set;}
        public String     style2                {get;set;}
        public String     style                 {get;set;}
        public String     rowselected           {get;set;}
        public String     salesOrderItem        {get;set;}
    }
}