/**
 */
@isTest
private class WFM_WorkOrderTriggerHandlerTest {


    @testSetup
    private static void createWorkOrderDataWithTerritories() {
        List<Territory2Type> listTerritoryTypes = [
                SELECT Id, MasterLabel
                FROM    Territory2Type
                WHERE  DeveloperName = 'CAG_Territory_Type'
        ];

        List<User> listOfUsers = new List<User>();
        Territory2Model terrModel;
        Territory2 newTerritory;
        System.runAs(new User(Id = UserInfo.getUserId())) {
            for(Integer i=0; i<5; i++) {
                listOfUsers.add(CreateTestClassData.reusableUser('System Administrator', 'tusr' + i));
            }
            insert listOfUsers;

          terrModel = [Select id, DeveloperName, Name from Territory2Model where State = 'Active'];


            newTerritory = new Territory2(
                    DeveloperName = 'Test_Territory',
                    Name = 'Test Territory',
                    Territory2ModelId = terrModel.Id,
                    Territory2TypeId = listTerritoryTypes[0].Id
            );
            insert newTerritory;

            List<UserTerritory2Association> listUserTerritory = new List<UserTerritory2Association>();
            for(User usr : listOfUsers){
                UserTerritory2Association usrTerritory = new UserTerritory2Association(
                        UserId = usr.Id,
                        Territory2Id = newTerritory.Id,
                        RoleInTerritory2 = WFM_Constants.USER_TERRITORY_ROLE_FSR_DR
                );
                if(usr == listOfUsers[0]){
                    usrTerritory.RoleInTerritory2 = WFM_Constants.USER_TERRITORY_ROLE_FSR_DX;
                }

                listUserTerritory.add(usrTerritory);
            }
            insert listUserTerritory;
        }

        // Create 200 accounts.
        //US29611
        //Create 10 accounts
        List<Account> listOfAccounts = createAccounts(10);
        insert listOfAccounts;

        // Assign all accounts to territory
        List<ObjectTerritory2Association> listAccountTerritory = new List<ObjectTerritory2Association>();
        for(Account acct : listOfAccounts){
            ObjectTerritory2Association acctTerritory = new ObjectTerritory2Association(
                    ObjectId = acct.Id,
                    Territory2Id = newTerritory.Id,
                    AssociationCause = 'Territory2Manual'
            );

            listAccountTerritory.add(acctTerritory);
        }
        insert listAccountTerritory;

        //US29611 - 5.24.2017 - HKinney - amended for increased test code coverage for calculateDuration() which is
        //                      now data driven.
        ////US29611
        // Create 8 work orders:
        List<WorkOrder> listOfWorkOrders = createWorkOrders(8, listOfAccounts);
        Id recordType;
        for (Integer i = 0; i<8; i++) {
            String prodType;
            if (i==0) {
                prodType = WFM_Constants.WORK_ORDER_PRODUCT_TYPE_DX;
                recordType = WFM_Constants.WORK_ORDER_VISIT_REQUEST_RECORDTYPE_ID;
            }
            if (i==1) {
                prodType = WFM_Constants.WORK_ORDER_PRODUCT_TYPE_DX;
                recordType = WFM_Constants.WORK_ORDER_INSTALL_RECORDTYPE_ID;
            }
            if (i==2) {
                prodType = WFM_Constants.WORK_ORDER_PRODUCT_TYPE_DR;
                recordType = WFM_Constants.WORK_ORDER_VISIT_REQUEST_RECORDTYPE_ID;
            }
            if (i==3) {
                prodType = WFM_Constants.WORK_ORDER_PRODUCT_TYPE_DR;
                recordType = WFM_Constants.WORK_ORDER_INSTALL_RECORDTYPE_ID;
            }
            //the remainder tests don't involve prodType; just recType:
            if (i==4) {
                recordType = WFM_Constants.WORK_ORDER_PRESCRIPTIVE_RECORDTYPE_ID;
            }
            if (i==5) {
                recordType = WFM_Constants.WORK_ORDER_PROACTIVE_RECORDTYPE_ID;
            }
            if (i==6) {
                recordType = WFM_Constants.WORK_ORDER_ONBOARDING_RECORDTYPE_ID;
            }
            if (i==7) {
                recordType = WFM_Constants.WORK_ORDER_CRITICAL_RECORDTYPE_ID;
            }
            listOfWorkOrders.get(i).Product_Type__c = prodType;
            listOfWorkOrders.get(i).RecordTypeId = recordType;
        }
        /*
        // Create 200 work orders: 100 of DX (50 Install/Visit Request) and 100 of DR (Install/Visit Request).
        for(Integer i = 0; i<200; i++){
            String prodType;
            if (i<=100) {
                prodType = WFM_Constants.WORK_ORDER_PRODUCT_TYPE_DX;
                recordType = Math.mod(i, 2) == 0 ? WFM_Constants.WORK_ORDER_INSTALL_RECORDTYPE_ID : WFM_Constants.WORK_ORDER_VISIT_REQUEST_RECORDTYPE_ID;
            }
            if ((i>100) && (i<=195)) {
                prodType = WFM_Constants.WORK_ORDER_PRODUCT_TYPE_DR;
                recordType = Math.mod(i, 2) == 0 ? WFM_Constants.WORK_ORDER_INSTALL_RECORDTYPE_ID : WFM_Constants.WORK_ORDER_VISIT_REQUEST_RECORDTYPE_ID;
                //recordType = WFM_Constants.WORK_ORDER_INSTALL_RECORDTYPE_ID;
            }
            if ((i>=196) && (i<200)) { //no prod types; just record types in this case...
                if (i == 196)
                    recordType = WFM_Constants.WORK_ORDER_PRESCRIPTIVE_RECORDTYPE_ID;
                if (i == 197)
                    recordType = WFM_Constants.WORK_ORDER_PROACTIVE_RECORDTYPE_ID;
                if (i == 198)
                    recordType = WFM_Constants.WORK_ORDER_ONBOARDING_RECORDTYPE_ID;
                if (i == 199)
                    recordType = WFM_Constants.WORK_ORDER_CRITICAL_RECORDTYPE_ID;
            }
            listOfWorkOrders.get(i).Product_Type__c = prodType;
            listOfWorkOrders.get(i).RecordTypeId = recordType;
        }
        */
        insert listOfWorkOrders;
    }

    private static List<Account> createAccounts(Integer numberOfAccounts) {
        List<Account> listOfAccounts = new List<Account>();
        for(Integer i=0; i<numberOfAccounts; i++){
            listOfAccounts.add(
                    new Account(
                            Name = 'TestAccount_' + i, ShippingCountry = 'UNITED STATES', ShippingCity = 'New York',
                            ShippingPostalCode = '001', ShippingState = 'NEW YORK',SAP_Customer_Number__c = '1234'+i
                    )
            );
        }
        return listOfAccounts;
    }

    private static List<WorkOrder> createWorkOrders(Integer numberOfWorkOrders, List<Account> accs) {
        List<WorkOrder> listOfWorkOrders = new List<WorkOrder>();
        for(Integer i=0; i<numberOfWorkOrders; i++){
            WorkOrder wo = WFM_WorkOrderModel.createWorkOrder(accs.get(i).Id, 'Test Class Subject',
                    WFM_Constants.WORK_ORDER_PROACTIVE_RECORDTYPE_ID, WFM_Constants.WORK_ORDER_PRIORITY_MEDIUM, WFM_Constants.WORK_ORDER_STATUS_NEW);
            wo.Product_Type__c = WFM_Constants.WORK_ORDER_PRODUCT_TYPE_DR;
            wo.Due_Date__c = DateTime.newInstance(System.today().year()+1, 3, 1);
            listOfWorkOrders.add(wo);
        }
        return listOfWorkOrders;
    }

    private static List<WorkOrderLineItem> createWorkOrderLineItems(List<WorkOrder> workOrders) {
        List<WorkOrderLineItem> woLineItems = new List<WorkOrderLineItem>();
        for(WorkOrder wo : workOrders) {
            WorkOrderLineItem woli = new WorkOrderLineItem(WorkOrderId = wo.Id, Status = 'New');
            woLineItems.add(woli);
        }
        return woLineItems;
    }

    static testMethod void test_createFollowUpWorkOrdersForCompletedInstallTest() {
        List<WorkOrder> workOrders = [SELECT Id, OwnerId FROM WorkOrder WHERE RecordTypeId = :WFM_Constants.WORK_ORDER_INSTALL_RECORDTYPE_ID and Product_Type__c=:WFM_Constants.WORK_ORDER_PRODUCT_TYPE_DX];

        System.assertEquals(1, workOrders.size(),'Install Work Orders created'); //US29611 - 5.24.2017 - HKinney

        List<ServiceAppointment> serviceAppointments = [SELECT Id, Status FROM ServiceAppointment WHERE ParentRecordId IN :workOrders];

        System.assertEquals(workOrders.size(), serviceAppointments.size(),'Service Appointments created');
        for(Integer i=0; i<workOrders.size(); i++){
            workOrders.get(i).Payment_Type__c = WFM_Constants.WORK_ORDER_PAYMENT_TYPE_CASH;
            workOrders.get(i).Install_Time__c = 2;
            workOrders.get(i).Status = WFM_Constants.WORK_ORDER_STATUS_COMPLETED;
        }
        WFM_Constants.WORKORDER_TRIGGER_HANDLER_HAS_RAN = false;

        Test.startTest();
        update workOrders;
        Test.stopTest();

        // Check that 100 follow up proactive work orders were created
        List<WorkOrder> listProactiveWorkOrders = new List<WorkOrder>([
                SELECT      Id, RecordTypeId, Subject, AccountId, Priority, Status
                FROM        WorkOrder
                WHERE       RecordTypeId = :WFM_Constants.WORK_ORDER_PROACTIVE_RECORDTYPE_ID
                AND         ParentWorkOrderId IN : workOrders
        ]);

        System.assertEquals(workOrders.size(), listProactiveWorkOrders.size(),'Followup Proactives Created');
    }

    static testMethod void test_sendNotificationsForOwnerChange() {
        User usr;
        System.runAs(new User(Id = UserInfo.getUserId())) {
            usr = CreateTestClassData.reusableUser('System Administrator', 'tusr');
            insert usr;
        }

        List<WorkOrder> workOrders = [SELECT Id, OwnerId FROM WorkOrder];
        for(WorkOrder wo : workOrders) {
            //wo.Product_Type__c = WFM_Constants.WORK_ORDER_PRODUCT_TYPE_DR;
            wo.OwnerId = usr.Id;
        }
        WFM_Constants.WORKORDER_TRIGGER_HANDLER_HAS_RAN = false;

        Integer emailNumber = Limits.getEmailInvocations();
        Test.startTest();
        update workOrders;
        Integer emailNumberAfter = Limits.getEmailInvocations();
        Test.stopTest();

        //System.assertNotEquals(emailNumber, emailNumberAfter, 'Email invocations number should increase');
    }

    static testMethod void test_sendNotificationsForInstallCompletionOrCancellation() {
        List<WorkOrder> workOrders = [SELECT Id, OwnerId FROM WorkOrder WHERE RecordTypeId = :WFM_Constants.WORK_ORDER_INSTALL_RECORDTYPE_ID];

        for(Integer i = 0; i <workOrders.size(); i++){
            String status = Math.mod(i, 2) == 0 ? WFM_Constants.WORK_ORDER_STATUS_COMPLETED :
                    WFM_Constants.WORK_ORDER_STATUS_CANCELED;
            WorkOrder wo = workOrders[i];
            wo.Reason_for_Visit__c = WFM_Constants.WORK_ORDER_REASON_NEW_SYSTEM;
            wo.Install_Time__c = 2;
            wo.Status = status;
            wo.Payment_Type__c = WFM_Constants.WORK_ORDER_PAYMENT_TYPE_CASH;
            wo.Cancellation_Notes__c = 'Test cancel';
            wo.Cancellation_Reason__c = 'Credit';
        }

        Integer emailNumber = Limits.getEmailInvocations();
        Test.startTest();
        update workOrders;
        Integer emailNumberAfter = Limits.getEmailInvocations();
        Test.stopTest();

    }


    static testMethod void test_fillAddressAndDuration() {
        Map<Id, Account> id2Account = new Map<Id, Account>([SELECT Id, ShippingCountry, ShippingCity, ShippingPostalCode, ShippingState FROM Account where Name like 'TestAccount_%' LIMIT 100]);
        List<WorkOrder> workOrders = [SELECT Id, Country, City, PostalCode, State, Duration, AccountId, OwnerId, RecordTypeId,RecordType.Name FROM WorkOrder WHERE AccountId IN :id2Account.keySet()];
        workOrders = workOrders.deepClone(false);
        Test.startTest();
        insert workOrders;
        Test.stopTest();

        workOrders = [SELECT Id, Country, City, PostalCode, State, Duration, AccountId FROM WorkOrder WHERE Id IN :workOrders];

        for(WorkOrder wo : workOrders) {
            Account acc = id2Account.get(wo.AccountId);
            System.assertEquals(acc.ShippingCountry, wo.Country, 'Country should be taken from shipping country on Account');
            System.assertEquals(acc.ShippingCity, wo.City, 'City should be taken from shipping city on Account');
            System.assertEquals(acc.ShippingPostalCode, wo.PostalCode, 'Postal code should be taken from shipping postal code on Account');
            System.assertEquals(acc.ShippingState, wo.State, 'State should be taken from shipping state on Account');
        }

    }

    static testMethod void test_createServiceAppointment() {
        List<WorkOrder> workOrders = [SELECT Id, Country, City, PostalCode, State, Duration, OwnerId, Account_FSR_Ranking_At_Creation__c, Due_Date__c,RecordTypeId,RecordType.Name FROM WorkOrder WHERE Subject='Test Class Subject' LIMIT 100];
        workOrders = workOrders.deepClone(false);

        WorkOrder woWithLowerFSRrank = workOrders[0];
        WorkOrder woWithHigherFSRrank = workOrders[1];
        WorkOrder woProactive = workOrders[2];
        woWithLowerFSRrank.RecordTypeId = WFM_Constants.WORK_ORDER_PRESCRIPTIVE_RECORDTYPE_ID;
        woWithHigherFSRrank.RecordTypeId = WFM_Constants.WORK_ORDER_PRESCRIPTIVE_RECORDTYPE_ID;
        woWithLowerFSRrank.Account_FSR_Ranking_At_Creation__c = 15;
        woWithHigherFSRrank.Account_FSR_Ranking_At_Creation__c = 18;
        woProactive.RecordTypeId = WFM_Constants.WORK_ORDER_PROACTIVE_RECORDTYPE_ID;

        Test.startTest();
        insert workOrders;
        Test.stopTest();

        Map<Id, WorkOrder> id2WorkOrder = new Map<Id, WorkOrder>([
                SELECT Id, City, Country, Description, PostalCode, State, Street, Subject, RecordTypeId, Due_Date__c,RecordType.Name
                FROM WorkOrder
                WHERE Id IN :workOrders
        ]);

        List<ServiceAppointment> serviceAppointments = [
                SELECT Id, City, Country, Description, PostalCode, SchedStartTime, State,
                        Street, DueDate, Subject, EarliestStartTime, ParentRecordId
                FROM ServiceAppointment
                WHERE ParentRecordId IN :workOrders
        ];

        for(ServiceAppointment sa : serviceAppointments) {
            WorkOrder wo = id2WorkOrder.get(sa.ParentRecordId);
            System.assertEquals(wo.City, sa.City, 'City should be taken from WorkOrder');
            System.assertEquals(wo.Country, sa.Country, 'Country should be taken from WorkOrder');
            System.assertEquals(wo.Description, sa.Description, 'Description shuld be taken from WorkOrder');
            System.assertEquals(wo.PostalCode, sa.PostalCode, 'PostalCode should be taken from WorkOrder');
            System.assertEquals(wo.State, sa.State, 'State should be taken from WorkOrder');
            System.assertEquals(wo.Street, sa.Street, 'Street should be takedn from WorkOrder');
            //System.assertEquals(wo.Subject, sa.Subject, 'Subject should be taken from WorkOrder');
            Boolean isFslTrue = wo.RecordTypeId == WFM_Constants.WORK_ORDER_INSTALL_RECORDTYPE_ID || wo.RecordTypeId == WFM_Constants.WORK_ORDER_CRITICAL_RECORDTYPE_ID;
            //System.assertEquals(isFslTrue, sa.FSL__Pinned__c, 'FSL Pinned should be true if Work Order is Install or Critical');
            System.assertNotEquals(null, sa.EarliestStartTime, 'EarliestStartTime should be set');

            if(sa.ParentRecordId == woWithLowerFSRrank.Id || sa.ParentRecordId == woWithHigherFSRrank.Id) {
                System.assertEquals(sa.EarliestStartTime.month(), 1, 'First quarter, month should be set to Jan');
            }

            if(sa.ParentRecordId == woProactive.Id) {
                DateTime dt = wo.Due_Date__c.addDays(-10);
                System.assertEquals(sa.EarliestStartTime, dt, 'EarliestStartTime should be ted days earlier than WorkOrder DueDate');
            }

        }
    }


    static testMethod void test_verifyWorkOrderLineItemsBeforeCompletion_Error() {
        List<WorkOrder> workOrdersSerialRequired = [SELECT Id, Country, City, PostalCode, State, Duration, OwnerId,RecordTypeId FROM WorkOrder WHERE Subject='Test Class Subject' and RecordTypeId = :WFM_Constants.WORK_ORDER_INSTALL_RECORDTYPE_ID Limit 1];

        System.assertNotEquals(0,workOrdersSerialRequired.size(),'Should have queried 1 or more work orders');
        for(WorkOrder wo : workOrdersSerialRequired) {
            wo.Reason_for_Visit__c = WFM_Constants.WORK_ORDER_REASON_NEW_SYSTEM;
            wo.Install_Time__c = 2;
            wo.Payment_Type__c = WFM_Constants.WORK_ORDER_PAYMENT_TYPE_CASH;
            wo.Status = WFM_Constants.WORK_ORDER_STATUS_COMPLETED;
        }

        List<WorkOrderLineItem> woLineItems = createWorkOrderLineItems(workOrdersSerialRequired);
        for(WorkOrderLineItem woli : woLineItems) {
            woli.Requires_Serial_Number_on_Install__c = true;
        }
        insert woLineItems;

        String error;
        Test.startTest();
        try {
            update workOrdersSerialRequired;
        } catch(Exception e) {
            error = e.getMessage();
        }
        Test.stopTest();

        System.assert(error.contains(Label.Work_Order_Line_Item_incomplete), 'Should throw line items not completed error:'+error);
        System.assert(error.contains(Label.Work_Order_Line_Item_Serial_Number_missing), 'Should throw line items missing serial number error'+error);
    }

    static testMethod void test_verifyWorkOrderLineItemsBeforeCompletion() {
        List<WorkOrder> workOrdersSerialRequired = [SELECT Id, Country, City, PostalCode, State, Duration, OwnerId FROM WorkOrder LIMIT 1];

        List<ServiceAppointment> serviceAppointments = [SELECT Id, Status FROM ServiceAppointment WHERE ParentRecordId IN :workOrdersSerialRequired];

        for(WorkOrder wo : workOrdersSerialRequired) {
            wo.Reason_for_Visit__c = WFM_Constants.WORK_ORDER_REASON_NEW_SYSTEM;
            wo.Install_Time__c = 2;
            wo.Status = WFM_Constants.WORK_ORDER_STATUS_COMPLETED;
            wo.Payment_Type__c = WFM_Constants.WORK_ORDER_PAYMENT_TYPE_CASH;
        }

        List<WorkOrderLineItem> woLineItems = createWorkOrderLineItems(workOrdersSerialRequired);
        for(WorkOrderLineItem woli : woLineItems) {
            woli.Requires_Serial_Number_on_Install__c = true;
            woli.Serial_Number__c = '1234';
            woli.Status = WFM_Constants.WORK_ORDER_LINE_ITEM_COMPLETED;
        }
        insert woLineItems;

        String error;
        Test.startTest();
        try {
            update workOrdersSerialRequired;
        } catch(Exception e) {
            error = e.getMessage();
        }
        Test.stopTest();

        System.assertEquals(null, error, 'There should be no error');
    }

    static testMethod void test_assignPreferredResourceAndTerritory_No_ResourcePreference() {
        List<WorkOrder> workOrders = [SELECT Id, Country, City, PostalCode, State, Duration, OwnerId,RecordTypeId,RecordType.Name FROM WorkOrder where Subject='Test Class Subject' LIMIT 100];
        workOrders = workOrders.deepClone(false);
        for(WorkOrder wo : workOrders) {
            wo.Product_Type__c = WFM_Constants.WORK_ORDER_PRODUCT_TYPE_DX;
        }
        Integer emailNumber = Limits.getEmailInvocations();
        Test.startTest();
        insert workOrders;
        Integer emailNumberAfter = Limits.getEmailInvocations();
        Test.stopTest();

        //System.assertNotEquals(emailNumber, emailNumberAfter, 'Email invocations number should increase');
        List<ResourcePreference> resourcePrefs = [SELECT Id FROM ResourcePreference WHERE RelatedRecordId IN :workOrders];
        System.assertEquals(0, resourcePrefs.size(), 'There should be no Resource Preference records created');
        List<WorkOrder> workOrdersWithTerritory = [SELECT Id, ServiceTerritoryId FROM WorkOrder WHERE Id IN : workOrders AND ServiceTerritoryId != null];
        System.assertEquals(0, workOrdersWithTerritory.size(), 'Service Territory should not be added to work orders');
    }

    static testMethod void test_assignPreferredResourceAndTerritory() {
        WFM_Utils.setUserPermissions([select id from User where id = :UserInfo.getUserId()]);

        System.runAs(new User(Id = UserInfo.getUserId())){
            OperatingHours oh = new OperatingHours();
            oh.Name = 'Test Operating Hours';
            insert oh;

            ServiceResource sr = new ServiceResource();
            sr.Name = 'Test Service Resource';
            sr.IsActive = true;
            sr.RelatedRecordId = UserInfo.getUserId();
            sr.ResourceType = WFM_Constants.SERVICE_RESOURCE_TYPE_TECHNICIAN;
            insert sr;

            ServiceTerritory st = new ServiceTerritory();
            st.IsActive = true;
            st.Name = 'Test Service Territoryregtrg';
            st.OperatingHoursId = oh.Id;
            insert st;

            ServiceTerritoryMember stm = new ServiceTerritoryMember();
            stm.ServiceResourceId = sr.Id;
            stm.TerritoryType = WFM_Constants.SERVICE_TERRITORY_TYPE_PRIMARY;
            stm.EffectiveStartDate = System.today().addDays(-1);
            stm.ServiceTerritoryId = st.Id;
            insert stm;

            WFM_ServiceResourceJob srj=new WFM_ServiceResourceJob();
            srj.execute(null);

            List<WorkOrder> workOrders = [SELECT Id, Product_Type__c, Country, City, PostalCode, State, Duration, OwnerId
                    , RecordTypeId, RecordType.Name FROM WorkOrder WHERE Product_Type__c = :WFM_Constants.WORK_ORDER_PRODUCT_TYPE_DX];
            workOrders = workOrders.deepClone(false);
            for(WorkOrder wo : workOrders) {
                wo.OwnerId = UserInfo.getUserId();
            }



            Test.startTest();
            insert workOrders;
            List<ServiceAppointment> serviceAppointmentList = [ SELECT Id,Status,DueDate,SchedStartTime,SchedEndTime from ServiceAppointment limit 1];
            ServiceAppointment sa = serviceAppointmentList[0];
            sa.SchedStartTime = sa.DueDate;
            sa.SchedEndTime = sa.DueDate.addHours(2);
            sa.Status = WFM_Constants.SERVICE_APPOINTMENT_STATUS_SCHEDULED;
            update sa;


            AssignedResource assignedResource = new AssignedResource();
            assignedResource.ServiceAppointmentId=sa.Id;
            assignedResource.ServiceResourceId=sr.Id;
            insert assignedResource;

            Event ev=new Event();
            ev.WhatId=sa.Id;
            ev.OwnerId=UserInfo.getUserId();
            ev.DurationInMinutes=120;
            ev.ActivityDate=sa.SchedStartTime.date();
            ev.StartDateTime=sa.SchedStartTime;
            ev.EndDateTime=sa.SchedEndTime;
            insert ev;

            Test.stopTest();
        }
    }

    //US29611 - 5.24.2017 - HKinney - to increase code coverage.
    static testMethod void test_updateSAPSalesOrderNumberIfBlank() {
        List<WorkOrder> workOrders = [SELECT Id, Country, City, PostalCode, State,
                Duration, OwnerId, RecordTypeId, RecordType.Name,
                Sales_Order_Number__c, Proposal_Number__c
        FROM WorkOrder where Subject='Test Class Subject' LIMIT 1];
        workOrders[0].Sales_Order_Number__c = '';
        workOrders[0].Proposal_Number__c = '12345';

        WFM_WorkOrderTriggerHandler wfmWorkOrderTriggerHandler = new WFM_WorkOrderTriggerHandler();
        wfmWorkOrderTriggerHandler.updateSAPSalesOrderNumberIfBlank(workOrders);

    }
    //US29611 - 5.24.2017 - HKinney - to increase code coverage.
    static testMethod void test_updateServiceAppointmentsOnWorkOrderChange() {
        List<WorkOrder> newWorkOrdersList = [SELECT Id, Country, City, PostalCode, State,
                Duration, OwnerId, RecordTypeId, RecordType.Name,
                Sales_Order_Number__c, Proposal_Number__c, Status
        FROM WorkOrder where Subject='Test Class Subject' LIMIT 2];
        newWorkOrdersList[0].Sales_Order_Number__c = '';
        newWorkOrdersList[0].Proposal_Number__c = '12345';
        newWorkOrdersList[0].Status = WFM_Constants.WORK_ORDER_STATUS_CANCELED;

        newWorkOrdersList[1].Sales_Order_Number__c = '';
        newWorkOrdersList[1].Proposal_Number__c = '6789';
        newWorkOrdersList[1].Status = WFM_Constants.WORK_ORDER_STATUS_COMPLETED;

        WorkOrder oldWorkOrder = newWorkOrdersList[0];
        oldWorkOrder.Status = WFM_Constants.WORK_ORDER_STATUS_NEW;

        WorkOrder secondOldWorkOrder = newWorkOrdersList[1];
        secondOldWorkOrder.Status = WFM_Constants.WORK_ORDER_STATUS_NEW;

        Map<Id, WorkOrder> mapOldWorkOrders = new Map<Id, WorkOrder>();
        mapOldWorkOrders.put(oldWorkOrder.Id, oldWorkOrder);
        mapOldWorkOrders.put(secondOldWorkOrder.Id, secondOldWorkOrder);

        WFM_WorkOrderTriggerHandler wfmWorkOrderTriggerHandler = new WFM_WorkOrderTriggerHandler();
        wfmWorkOrderTriggerHandler.updateServiceAppointmentsOnWorkOrderChange(newWorkOrdersList, mapOldWorkOrders);

    }



}