@isTest
public class Test_VCPlusLOBSchedule {
    
    
    static testMethod void testExecute() {
        
  
        String CRONstr = '0 0 0 L MAR,JUN,SEP,DEC ? *';
        
        
        User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
        
        
        system.runAs(currentUser){
            
            Test.startTest();
            VCPlus_lob_callout.session = system.UserInfo.getSessionId();
            
            Account testAccount = CreateTestClassData.createCustomerAccount();
            
        }
        
        
        
        String jobId = System.schedule('SchedulerToUbdateVCPlusLOB', CRONstr ,  new Scheduler_VCPlus());
        
        
        if(jobId!=null){
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                              FROM CronTrigger WHERE id = :jobId];
            
        }
        Test.stopTest();
        
        
    }
}