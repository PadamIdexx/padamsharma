@isTest
public class Test_GenerateQuoteAction {
    
    public static Account testAcct{get;set;}
    public static List<Order> orderList{get;set;}
    
    @testSetup
    public static void createTestData(){
        Test_Data_Utility.createTestAccounts(0, 0);
        testAcct = [Select Id, Name, CurrencyIsoCode From Account WHERE Name =: 'TestAccount0' LIMIT 1];
        system.debug('account ' + testAcct);
        Test_Data_Utility.createTestOpps(0, testAcct);
        Test_Data_Utility.createTestOrders(0, testAcct);
        orderList = new List<Order>([SELECT Name, Id, AccountId, Delivery_Block__c, billingStreet, BillingCity, BillingCountry, BillingPostalCode, BillingCountryCode, BillingState, VAT__c, Payment_Terms__c, Ship_Method__c,
                                     Pricebook2Id, ShippingStreet, ShippingPostalCode, ShippingCountry, ShippingState, ShippingCountryCode, name2__c, Tax_Charges__c, Shipping_Charges__c, BillToContactId, Order_Email__c,
                                     OpportunityId, CurrencyIsoCode
                                     FROM Order WHERE AccountId =: testAcct.Id LIMIT 1]);
    }
    
    public static testMethod void createQuote(){
        
        createTestData();
        AccountTriggerHandler.runGeocoding = false;
        generateQuoteAction.details.put('VAT', '0.0');
        generateQuoteAction.details.put('billTo', 'Test Street');
        generateQuoteAction.details.put('shipTo',  'Test ShipStreet');
        generateQuoteAction.details.put('incoterm', 'FXG — FEDEX Ground');
        generateQuoteAction.details.put('shipToName', 'Test ShipTo Name');
        generateQuoteAction.details.put('billToName', 'Test BillTo Name');
        generateQuoteAction.CreateQuote(orderList);
        //String quoteName =  'Quote for Order# ' + orderList.get(0).orderNumber;
        
        //List<Quote> q = new List<Quote>([SELECT Name, Account__c FROM Quote WHERE Name =: quoteName]);
        
        //SYSTEM.assertEquals(1, q.size());
        
    }

}