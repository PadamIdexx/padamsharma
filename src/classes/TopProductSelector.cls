/*
 * Selector class for retrieving top products.
 */ 
public class TopProductSelector {
    
    public TopProductSelector() {

    }
    
    /*
     * Retrieval method for getting top products for the specified practice.     
     */ 
    public List<TopProduct> retrieveTopProductsForAccount(String accountId, Integer topRows) {

        List<TopProduct> myProducts = new List<TopProduct>();
        
        List<AggregateResult> items = [SELECT PricebookEntry.Product2.SAP_Material_Number__c mat, count(PricebookEntry.Product2.Id) cnt from OrderItem where Order.AccountID=:accountId and PricebookEntry.Product2.SAP_Material_Number__c != '' group by rollup( PricebookEntry.Product2.SAP_Material_Number__c) order by count(PricebookEntry.Product2.Id) DESC Limit :topRows];

        if (!items.isEmpty()) {        
            for (AggregateResult ar : items) {
                String material = (String)ar.get('mat');
               
                if (String.isNotBlank(material)) {               
                    OrderItem theItem = [SELECT ID,PricebookEntryId,PricebookEntry.Product2Id,PricebookEntry.CurrencyIsoCode,PricebookEntry.UnitPrice,PricebookEntry.Product2.Name,PricebookEntry.Product2.SAP_Material_Number__c,UnitPrice,Shipping_Method__c,Discount_Reason__c,PriceBookEntry.Product2.Item_Category_Group_Code__c,Quantity, PricebookEntry.Product2.CMS_Unit_of_Measure__c from OrderItem where Order.AccountID=:accountId and PricebookEntry.Product2.SAP_Material_Number__c=:material Limit 1];
    
                    TopProduct p = new TopProduct();                
                    
                    p.isSelected = false;
                    p.oItem = theItem;   
                    p.qty = 1;
                    p.prodName = theItem.PricebookEntry.Product2.Name;
                    p.materialNumber = theItem.PricebookEntry.Product2.SAP_Material_Number__c;
                    p.itemCategory = theItem.PricebookEntry.Product2.Item_Category_Group_Code__c;  
                    p.unitOfMeasure = theItem.PricebookEntry.Product2.CMS_Unit_of_Measure__c;
                    
                    myProducts.add(p);
                }
            } 
    	}

        return myProducts;        
    }
    
    
    /*
     * Retrieval method for getting default top products for an LOB.
     */ 
    public List<TopProduct> retrieveDefaultTopProducts(String lob, String curr, Integer topRows) {
    	List<TopProduct> myProducts = new List<TopProduct>();

        //KRL TODO: Figure out a better way to handle this
        String pBook = (curr == 'CAD') ? 'IDEXX_PriceBook_CA' : 'IDEXX Standard Price Book' ;
        
        List<IdexxTop10Products__mdt> defaultTopN = [SELECT Currency__c,Product_code__c,DeveloperName,Id,isActive__c,Label,Language,NamespacePrefix,ProductName__c,QualifiedApiName FROM IdexxTop10Products__mdt WHERE Currency__c =:curr and isActive__c = true AND LOB__c =:lob];
        
        Set<String> productCodes = new Set<String>(); 
        
        for(IdexxTop10Products__mdt top10 : defaultTopN) { 
        	productCodes.add(top10.Product_code__c);
        }
       
        List<PricebookEntry> pbEntries = [SELECT Id, CurrencyIsoCode, PricebookEntry.Product2.Id, PricebookEntry.UnitPrice, PricebookEntry.Product2.SAP_Material_Number__c,PriceBookEntry.Product2.Item_Category_Group_Code__c, PricebookEntry.Product2.Name, PricebookEntry.Product2.CMS_Unit_of_Measure__c FROM PricebookEntry WHERE Pricebook2.Name = :pBook and PricebookEntry.Product2.ProductCode in :productCodes Limit :topRows];
        
        for(PricebookEntry pb : pbEntries) {
        	TopProduct p = new TopProduct();
            
            p.isSelected = false;
            p.pbEntry = pb;
            p.qty = 1;
            p.prodName = pb.Product2.Name;
            p.materialNumber = pb.Product2.SAP_Material_Number__c;
            p.itemCategory = pb.Product2.Item_Category_Group_Code__c; 
            p.unitOfMeasure = pb.Product2.CMS_Unit_of_Measure__c;

            myProducts.add(p);

          }             
        
        return myProducts;
    }
    
}