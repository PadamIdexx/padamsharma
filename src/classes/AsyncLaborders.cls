public class AsyncLaborders {
    public class getLabOrderCollectionsResponse_elementFuture extends System.WebServiceCalloutFuture {
        public labOrders.labOrderReport_element getValue() {
           laborders.getLabOrderCollectionsResponse_element response = (laborders.getLabOrderCollectionsResponse_element)System.WebServiceCallout.endInvoke(this);
            if(Test.isRunningTest()){response = LabOrderTestDataGenerator.createLabOrderCollectionResponse();}
            return response.labOrderReport;
        }
    }
    public class getLabOrderDetailsResponse_elementFuture extends System.WebServiceCalloutFuture {
        public laborders.labOrderReport_element getValue() {
            laborders.getLabOrderDetailsResponse_element response = (laborders.getLabOrderDetailsResponse_element)System.WebServiceCallout.endInvoke(this);
            return response.labOrderReport;
        }
    }

    public class AsyncLabOrderServiceSoapPort {
        
        String endPointURL = WebServiceUtil.retreiveEndpoints(WebServiceUtil.LAB_ORDERS);
        public String endpoint_x = endPointURL!=null ? endPointURL : '';
        
        public Map<String,String> inputHttpHeaders_x;
        public AsyncLabOrderServiceSoapPort(){
            inputHttpHeaders_x = new Map<String, String>();
            APIUser__c creds = APIUser__c.getInstance('API USER');
            if(creds !=null){
                String sessionId = Login.login(creds.Username__c,creds.Password__c+creds.Security_Token__c);
                inputHttpHeaders_x.put('sessionId', sessionId);
            }
        }
        public String clientCertName_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://idexx.com/services/lde/laborder', 'LabOrders'};
            public AsyncLabOrders.getLabOrderCollectionsResponse_elementFuture beginGetLabOrderCollections(System.Continuation continuation,LabOrders.labReportSearchRequest_element labReportSearchRequest) {
                LabOrders.getLabOrderCollections_element request_x = new LabOrders.getLabOrderCollections_element();
                request_x.labReportSearchRequest = labReportSearchRequest;
                return (AsyncLabOrders.getLabOrderCollectionsResponse_elementFuture) System.WebServiceCallout.beginInvoke(
                    this,
                    request_x,
                    AsyncLabOrders.getLabOrderCollectionsResponse_elementFuture.class,
                    continuation,
                    new String[]{endpoint_x,
                        '',
                        'http://idexx.com/services/lde/laborder',
                        'getLabOrderCollections',
                        'http://idexx.com/services/lde/laborder',
                        'getLabOrderCollectionsResponse',
                        'LabOrders.getLabOrderCollectionsResponse_element'}
                );
            }

                public AsyncLaborders.getLabOrderDetailsResponse_elementFuture beginGetLabOrderDetails(System.Continuation continuation,Laborders.labReportSearchRequest_element labReportSearchRequest) {
                    Laborders.getLabOrderDetails_element request_x = new Laborders.getLabOrderDetails_element();
                    request_x.labReportSearchRequest = labReportSearchRequest;
                    return (AsyncLaborders.getLabOrderDetailsResponse_elementFuture) System.WebServiceCallout.beginInvoke(
                        this,
                        request_x,
                        AsyncLaborders.getLabOrderDetailsResponse_elementFuture.class,
                        continuation,
                        new String[]{endpoint_x,
                            '',
                            'http://idexx.com/services/lde/laborder',
                            'getLabOrderDetails',
                            'http://idexx.com/services/lde/laborder',
                            'getLabOrderDetailsResponse',
                            'Laborders.getLabOrderDetailsResponse_element'}
                    );
                }
            }
    }