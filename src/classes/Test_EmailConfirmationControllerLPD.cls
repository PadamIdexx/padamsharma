@isTest
public with sharing class Test_EmailConfirmationControllerLPD {
		
	@isTest
	private static void Emailcontroller1(){
        
		Test.startTest();
        Test.setMock(WebServiceMock.class, new ContactWebServiceMockImpl());
        Test.stopTest();
        EmailConfirmationControllerLPD controller1 = new EmailConfirmationControllerLPD('127895');
		controller1.fetchDetailsFromSAP();  
	}
	
	@isTest
	private static void EmailController2(){
		
		Test.startTest();
		// This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new ContactWebServiceMockImpl());
        // Call the method that invokes a callout
        Test.stopTest();
        
		
		Account testAccount = new Account();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = 'Customer Testing Account';
        testAccount.CurrencyISOCode = 'USD';
        testAccount.HIN__c = 'Test123';
        testAccount.SAP_Customer_Number__c = '0'+String.valueOf(Math.random()).subString(12); 
        insert testAccount;
        
        //Account testAccount = [Select Id From Account LIMIT 1];
		
        ApexPages.StandardController std = new ApexPages.StandardController(testAccount);
		EmailConfirmationControllerLPD controller1 = new EmailConfirmationControllerLPD(std);
		
		EmailConfirmationControllerLPD.ContactDetails contactDtls = new EmailConfirmationControllerLPD.ContactDetails();
		contactDtls.FirstName = 'fname';
		contactDtls.LastName = 'lname';
		contactDtls.Email = 'email@test.com';
        contactDtls.ContactType = 'CD';
        controller1.shipTo = '12345';
        controller1.soldTo = '12345';
		
		controller1.selectedContact = contactDtls;
		
		controller1.add();
	}
	
	@isTest
	private static void EmailController3(){
		Test.startTest();
		// This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new ContactWebServiceMockImpl());
        // Call the method that invokes a callout
        Test.stopTest();
        
		
		Account testAccount = new Account();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = 'Customer Testing Account';
        testAccount.CurrencyISOCode = 'USD';
        testAccount.HIN__c = 'Test123';
        testAccount.SAP_Customer_Number__c = '0'+String.valueOf(Math.random()).subString(12); 
        insert testAccount;
       
        //Account testAccount = [Select Id From Account LIMIT 1];
		
        ApexPages.StandardController std = new ApexPages.StandardController(testAccount);
		EmailConfirmationControllerLPD controller1 = new EmailConfirmationControllerLPD(std);
		
		controller1.deleteContact();
	}
	
	@isTest
	private static void EmailController4(){
		Test.startTest();
		// This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new ContactWebServiceMockImpl());
        // Call the method that invokes a callout
        Test.stopTest();
        
		
		Account testAccount = new Account();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = 'Customer Testing Account';
        testAccount.CurrencyISOCode = 'USD';
        testAccount.HIN__c = 'Test123';
        testAccount.SAP_Customer_Number__c = '0'+String.valueOf(Math.random()).subString(12); 
        insert testAccount;
        
        //Account testAccount = [Select Id From Account LIMIT 1];
        ApexPages.StandardController std = new ApexPages.StandardController(testAccount);
		EmailConfirmationControllerLPD controller1 = new EmailConfirmationControllerLPD(std);
		
		EmailConfirmationControllerLPD.ContactDetails contactDtls = new EmailConfirmationControllerLPD.ContactDetails();
		contactDtls.FirstName = 'fname';
		contactDtls.LastName = 'lname';
		contactDtls.Email = 'email@test.com';
		
		controller1.selectedContact = contactDtls;
		
		controller1.save();
	}
	
	@isTest
	private static void EmailController5(){
		Account testAccount = new Account();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = 'Customer Testing Account';
        testAccount.CurrencyISOCode = 'USD';
        testAccount.HIN__c = 'Test123';
        testAccount.SAP_Customer_Number__c = '0'+String.valueOf(Math.random()).subString(12); 
        insert testAccount;	
        
        ApexPages.StandardController std = new ApexPages.StandardController(testAccount);
        ApexPages.CurrentPage().getParameters().put('pf', 'FRS5:CL:0000012345' ); // was testAccount.Id);
        ApexPages.CurrentPage().getParameters().put('lob','LPDEMEA');
        ApexPages.CurrentPage().getParameters().put('partnerId', '12345');
        ApexPages.CurrentPage().getParameters().put('soldTo', 'FRS5:CL:0000012345' ); // was testAccount.Id);
		EmailConfirmationControllerLPD controller1 = new EmailConfirmationControllerLPD(std);
        
        EmailConfirmationControllerLPD.ContactDetails contactDtls = new EmailConfirmationControllerLPD.ContactDetails();
		contactDtls.FirstName = 'fname1';
		contactDtls.LastName = 'lname2';
		contactDtls.Email = 'email123@test.com';
        contactDtls.PartnerID = '12345';
        List<EmailConfirmationControllerLPD.ContactDetails> conList = new List<EmailConfirmationControllerLPD.ContactDetails>();
        conList.add(contactDtls);
        controller1.contacts = conList;
        controller1.selectContact();
        //controller1.deleteContact();
        //
        
        ContactPersonProcessingResponse.ContactPersonsQuery_Sync fetchresponse = new ContactPersonProcessingResponse.ContactPersonsQuery_Sync();
        ContactPersonProcessingResponse.ContactPerson contactPerson1 = new ContactPersonProcessingResponse.ContactPerson();
        contactPerson1.CustomerID = '12345';
        contactPerson1.ContactType = 'CO';
        contactPerson1.Department = 'CL';
        ContactPersonProcessingResponse.CommunicationsFull contactPersonCommsFull1 = new ContactPersonProcessingResponse.CommunicationsFull();
        ContactPersonProcessingResponse.Emails_element contactPersonEmails1 = new ContactPersonProcessingResponse.Emails_element();
        ContactPersonProcessingResponse.Default_element contactPersonEmailsDefault1 = new ContactPersonProcessingResponse.Default_element();
        contactPersonEmailsDefault1.Email = 'test1@test.net';
        contactPersonEmailsDefault1.DoNotUse = 'false';
        contactPersonEmails1.Default_x = contactPersonEmailsDefault1;
        contactPersonCommsFull1.Emails = contactPersonEmails1;
        contactPerson1.Communications = contactPersonCommsFull1;
        ContactPersonProcessingResponse.ContactPerson[] contactPersons1 = new ContactPersonProcessingResponse.ContactPerson[]{};
        contactPersons1.add(contactPerson1); 
        controller1.fetchresponseShipTo = fetchresponse;
        controller1.fetchDetailsFromSAP();
        
        //2
		ContactPersonProcessingResponse.ContactPersonsQuery_Sync fetchresponse2 = new ContactPersonProcessingResponse.ContactPersonsQuery_Sync();
        ContactPersonProcessingResponse.ContactPerson contactPerson2 = new ContactPersonProcessingResponse.ContactPerson();
        contactPerson2.CustomerID = '12345';
        contactPerson2.ContactType = 'CD';
        contactPerson2.Department = 'CL';
        ContactPersonProcessingResponse.CommunicationsFull contactPersonCommsFull2 = new ContactPersonProcessingResponse.CommunicationsFull();
        ContactPersonProcessingResponse.Emails_element contactPersonEmails2 = new ContactPersonProcessingResponse.Emails_element();
        ContactPersonProcessingResponse.Default_element contactPersonEmailsDefault2 = new ContactPersonProcessingResponse.Default_element();
        contactPersonEmailsDefault2.Email = 'test1@test.net';
        contactPersonEmailsDefault2.DoNotUse = 'false';
        contactPersonEmails2.Default_x = contactPersonEmailsDefault2;
        contactPersonCommsFull2.Emails = contactPersonEmails2;
        contactPerson2.Communications = contactPersonCommsFull2;
        ContactPersonProcessingResponse.ContactPerson[] contactPersons2 = new ContactPersonProcessingResponse.ContactPerson[]{};
        contactPersons2.add(contactPerson2);
       	fetchresponse2.ContactPerson = contactPersons2;

        ApexPages.CurrentPage().getParameters().put('partnerId', '12345'); 
		EmailConfirmationControllerLPD controller2 = new EmailConfirmationControllerLPD(std);
        ApexPages.CurrentPage().getParameters().put('lob','LPDNA');
        controller2.fetchresponseSoldTo = fetchresponse2;
     	controller2.fetchDetailsFromSAP();
        

	}
	
	@isTest
	private static void EmailController6(){
		Test.startTest();
		// This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new ContactWebServiceNoEmailMockImpl());
        // Call the method that invokes a callout
        Test.stopTest();

		EmailConfirmationControllerLPD controller1 = new EmailConfirmationControllerLPD('127895');
		controller1.fetchDetailsFromSAP();
        controller1.isAddContactMethod();
        controller1.selectContact();
        
	}
    @isTest
    private static void TestContactPersonResponse(){
        ContactPersonProcessingResponse.Messages message1= new ContactPersonProcessingResponse.Messages();
        ContactPersonProcessingResponse.Telephones_element telElemenet1 = new ContactPersonProcessingResponse.Telephones_element();
        telElemenet1.Default_x = new ContactPersonProcessingResponse.Default_element();
        ContactPersonProcessingResponse.Telephone_element tel2 = new ContactPersonProcessingResponse.Telephone_element();
        tel2.Default_x = new ContactPersonProcessingResponse.Default_element();
        ContactPersonProcessingResponse.Faxes_element faxElement = new ContactPersonProcessingResponse.Faxes_element();
        ContactPersonProcessingResponse.Additional_element addtionalElement = new ContactPersonProcessingResponse.Additional_element();
        
    }
}