/*
 * OrderSimulateRequest
 * 
 * This class represents the shape the Request for the SAP Sales Order Simulate 
 * operation in SalesProcessing_OutService as defined in Beacon OSB under
 * SAPOrderManagment.
 */ 
public with sharing class OrderSimulateRequest {
    //Web Service call out related constants.
    //
    // SOAP Action
    public final static String SOAPACTION = 'http://sap.com/xi/WebService/soap1.1';
    
    // XML Namespace for this object
    // Salesforce representation of Namespace
    public final static String NAMESPACE = 'http://idexx.com/sappo/sales/salesforce';
    public final static String SF_NAMESPACE = 'idexxComSappoSalesSalesforce';
    
    // XML Namespace of types    
    // Salesforce representation of Namespace   
    public final static String XMLTYPE_NAMESPACE = 'http://idexx.com/sappo/global/types';
	public final static String SF_XMLTYPE_NAMESPACE = 'idexxComSappoGlobalTypes';
     
	// Name of Class that carries the payload.  There is no generic way to get this at this 
	// from the obect at runtime at this time.    
    public final static String PAYLOAD_CLASSNAME = 'SalesOrderSimulateRequest';

    public class SalesOrderSimulateRequest {
    	public String DocumentType;
        public String SalesOrganization;
        public String DistributionChannel;
        public String Division;
        public String PurchaseOrderType;
        public String SalesPerson;        
        public String Incoterms;
        public String PaymentTerms;        
        public String DeliveryBlock;
        public String BillingBlock;
        public String OrderReason;
        public String CustomerGroup3;
        public String CustomerGroup4;
        public String CustomerGroup5;        
        public String PurchaseOrderNumber;        
        public String ConditionType1;
        public String ConditionValue1;
        public String ConditionCurrency1;
        public String ConditionType2;
        public String ConditionValue2;
        public String ConditionCurrency2;
        public String ConditionType3;
        public String ConditionValue3;
        public String ConditionCurrency3;        
        public String ConditionType4;
        public String ConditionValue4;
        public String ConditionCurrency4;                
        public String ReferenceDocument;
        public String ReferenceDocumentCategory;
        public String Customer;
        public String CouponCode;
        public String Language;
        
        public Items_element[] Items;
        public Partners_element[] Partners;

        private String[] DocumentType_type_info = new String[]{'DocumentType',NAMESPACE,null,'0','1','false'};
        private String[] SalesOrganization_type_info = new String[]{'SalesOrganization',NAMESPACE,null,'1','1','false'};
        private String[] DistributionChannel_type_info = new String[]{'DistributionChannel',NAMESPACE,null,'1','1','false'};
        private String[] Division_type_info = new String[]{'Division',NAMESPACE,null,'1','1','false'};
        private String[] PurchaseOrderType_type_info = new String[]{'PurchaseOrderType',NAMESPACE,null,'1','1','false'};
        private String[] SalesPerson_type_info = new String[]{'SalesPerson',NAMESPACE,null,'0','1','false'};            
        private String[] Incoterms_type_info = new String[]{'Incoterms',NAMESPACE,null,'0','1','false'};
        private String[] PaymentTerms_type_info = new String[]{'PaymentTerms',NAMESPACE,null,'0','1','false'};
        private String[] DeliveryBlock_type_info = new String[]{'DeliveryBlock',NAMESPACE,null,'0','1','false'};            
        private String[] BillingBlock_type_info = new String[]{'BillingBlock',NAMESPACE,null,'0','1','false'};
        private String[] OrderReason_type_info = new String[]{'OrderReason',NAMESPACE,null,'0','1','false'};
		private String[] CustomerGroup3_type_info = new String[]{'CustomerGroup3',NAMESPACE,null,'0','1','false'};
        private String[] CustomerGroup4_type_info = new String[]{'CustomerGroup4',NAMESPACE,null,'0','1','false'};
        private String[] CustomerGroup5_type_info = new String[]{'CustomerGroup5',NAMESPACE,null,'0','1','false'};            
        private String[] PurchaseOrderNumber_type_info = new String[]{'PurchaseOrderNumber',NAMESPACE,null,'0','1','false'};
        private String[] ConditionType1_type_info = new String[]{'ConditionType1',NAMESPACE,null,'0','1','false'};
        private String[] ConditionValue1_type_info = new String[]{'ConditionValue1',NAMESPACE,null,'0','1','false'};
        private String[] ConditionCurrency1_type_info = new String[]{'ConditionCurrency1',NAMESPACE,null,'0','1','false'};
        private String[] ConditionType2_type_info = new String[]{'ConditionType2',NAMESPACE,null,'0','1','false'};
        private String[] ConditionValue2_type_info = new String[]{'ConditionValue2',NAMESPACE,null,'0','1','false'};
        private String[] ConditionCurrency2_type_info = new String[]{'ConditionCurrency2',NAMESPACE,null,'0','1','false'};
        private String[] ConditionType3_type_info = new String[]{'ConditionType3',NAMESPACE,null,'0','1','false'};
        private String[] ConditionValue3_type_info = new String[]{'ConditionValue3',NAMESPACE,null,'0','1','false'};
        private String[] ConditionCurrency3_type_info = new String[]{'ConditionCurrency3',NAMESPACE,null,'0','1','false'};            
        private String[] ConditionType4_type_info = new String[]{'ConditionType4',NAMESPACE,null,'0','1','false'};
        private String[] ConditionValue4_type_info = new String[]{'ConditionValue4',NAMESPACE,null,'0','1','false'};
        private String[] ConditionCurrency4_type_info = new String[]{'ConditionCurrency4',NAMESPACE,null,'0','1','false'};            
        private String[] ReferenceDocument_type_info = new String[]{'ReferenceDocument',NAMESPACE,null,'0','1','false'};
        private String[] ReferenceDocumentCategory_type_info = new String[]{'ReferenceDocumentCategory',NAMESPACE,null,'0','1','false'};                        
        private String[] Language_type_info = new String[]{'Language',NAMESPACE,null,'0','1','false'};            
        private String[] CouponCode_type_info = new String[]{'CouponCode',NAMESPACE,null,'0','1','false'};            
        private String[] Items_type_info = new String[]{'Items',NAMESPACE,null,'0','-1','false'};            
        private String[] Conditions_type_info = new String[]{'Conditions',NAMESPACE,null,'0','-1','false'};            
        private String[] Partners_type_info = new String[]{'Partners',NAMESPACE,null,'0','-1','false'};            
          
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'DocumentType','SalesOrganization','DistributionChannel','Division','PurchaseOrderType','SalesPerson','Incoterms',
            													'PaymentTerms','DeliveryBlock','BillingBlock','OrderReason','CustomerGroup3','CustomerGroup4','CustomerGroup3','PurchaseOrderNumber',
            													'ConditionType1','ConditionValue1','ConditionCurrency1','ConditionType2','ConditionValue2','ConditionCurrency2',
            													'ConditionType3','ConditionValue3','ConditionCurrency3','ConditionType4','ConditionValue4','ConditionCurrency4',            
            													'ReferenceDocument','ReferenceDocumentCategory','CouponCode','Customer','Language','Items','Partners'};
    }
    

    public class Items_element {
        public String ExternalItemNumber;
        public String Material;
        public String Batch;
        public String Plant;
        public String StorageLocation;
        public String RequestedQuantity;
        public String ItemCategory;
        public String RequestedDeliveryDate;
        public String ConditionType1;
        public String MaterialGroup3;
        public String MaterialGroup4;
        public String MaterialGroup5;
        public String ConditionValue1;
        public String ConditionCurrency1;        
        public String Incoterms;
        public String ReferenceDocument;
        public String ReferenceDocumentItem;
        public String ReferenceDocumentCategory;
        public String ConditionType2;
        public String ConditionValue2;
        public String ConditionCurrency2;
        public String ConditionType3;
        public String ConditionValue3;
        public String ConditionCurrency3;        
        public String ConditionType4;
        public String ConditionValue4;
        public String ConditionCurrency4; 
        public String ShippingPoint;
        public String UseCustomerIncoterms;
        
        private String[] ExternalItemNumber_type_info = new String[]{'ExternalItemNumber',NAMESPACE,null,'1','1','false'};
        private String[] Material_type_info = new String[]{'Material',NAMESPACE,null,'1','1','false'};
        private String[] Batch_type_info = new String[]{'Batch',NAMESPACE,null,'0','1','false'};
        private String[] Plant_type_info = new String[]{'Plant',NAMESPACE,null,'0','1','false'};
        private String[] StorageLocation_type_info = new String[]{'StorageLocation',NAMESPACE,null,'0','1','false'};
        private String[] RequestedQuantity_type_info = new String[]{'RequestedQuantity',NAMESPACE,null,'0','1','false'};
        private String[] ItemCategory_type_info = new String[]{'ItemCategory',NAMESPACE,null,'0','1','false'};
        private String[] RequestedDeliveryDate_type_info = new String[]{'RequestedDeliveryDate',NAMESPACE,null,'0','1','false'};
        private String[] ConditionType1_type_info = new String[]{'ConditionType1',NAMESPACE,null,'0','1','false'};
        private String[] MaterialGroup3_type_info = new String[]{'MaterialGroup3',NAMESPACE,null,'0','1','false'};
        private String[] MaterialGroup4_type_info = new String[]{'MaterialGroup4',NAMESPACE,null,'0','1','false'};
        private String[] MaterialGroup5_type_info = new String[]{'MaterialGroup5',NAMESPACE,null,'0','1','false'};
        private String[] ConditionValue1_type_info = new String[]{'ConditionValue1',NAMESPACE,null,'0','1','false'};
        private String[] ConditionCurrency1_type_info = new String[]{'ConditionCurrency1',NAMESPACE,null,'0','1','false'};
        private String[] Incoterms_type_info = new String[]{'Incoterms',NAMESPACE,null,'0','1','false'};
        private String[] ReferenceDocument_type_info = new String[]{'ReferenceDocument',NAMESPACE,null,'0','1','false'};
        private String[] ReferenceDocumentItem_type_info = new String[]{'ReferenceDocumentItem',NAMESPACE,null,'0','1','false'};
        private String[] ReferenceDocumentCategory_type_info = new String[]{'ReferenceDocumentCategory',NAMESPACE,null,'0','1','false'};
        private String[] ConditionType2_type_info = new String[]{'ConditionType2',NAMESPACE,null,'0','1','false'};
        private String[] ConditionValue2_type_info = new String[]{'ConditionValue2',NAMESPACE,null,'0','1','false'};
        private String[] ConditionCurrency2_type_info = new String[]{'ConditionCurrency2',NAMESPACE,null,'0','1','false'};
        private String[] ConditionType3_type_info = new String[]{'ConditionType3',NAMESPACE,null,'0','1','false'};
        private String[] ConditionValue3_type_info = new String[]{'ConditionValue3',NAMESPACE,null,'0','1','false'};
        private String[] ConditionCurrency3_type_info = new String[]{'ConditionCurrency3',NAMESPACE,null,'0','1','false'};            
        private String[] ConditionType4_type_info = new String[]{'ConditionType4',NAMESPACE,null,'0','1','false'};
        private String[] ConditionValue4_type_info = new String[]{'ConditionValue4',NAMESPACE,null,'0','1','false'};
        private String[] ConditionCurrency4_type_info = new String[]{'ConditionCurrency4',NAMESPACE,null,'0','1','false'};            
        private String[] ShippingPoint_type_info = new String[]{'ShippingPoint',NAMESPACE,null,'0','1','false'};            
      	private String[] UseCustomerIncoterms_type_info = new String[]{'UseCustomerIncoterms',NAMESPACE,null,'0','1','false'};

            
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'ExternalItemNumber','Material','Batch', 'Plant','StorageLocation','RequestedQuantity',
            													'ItemCategory','RequestedDeliveryDate','ConditionType1','MaterialGroup3','MaterialGroup4','MaterialGroup5',
            													'ConditionValue1','ConditionCurrency1','Incoterms','ReferenceDocument','ReferenceDocumentItem','ReferenceDocumentCategory',
            													'ConditionType2','ConditionValue2','ConditionCurrency2','ConditionType3','ConditionValue3','ConditionCurrency3',
            													'ConditionType4','ConditionValue4','ConditionCurrency4','ShippingPoint','UseCustomerIncoterms'};
    }    
    
    
    public class Partners_element {
    	public String PartnerType;
        public String Customer;
        public String Name;
        public String Name2;
        public String Street;
        public String Country;
        public String PostalCode;
        public String City;
        public String District;
        public String Region;
        
        private String[] PartnerType_type_info = new String[]{'PartnerType',NAMESPACE,null,'0','1','false'};
		private String[] Customer_type_info = new String[]{'Customer',NAMESPACE,null,'0','1','false'};
		private String[] Name_type_info = new String[]{'Name',NAMESPACE,null,'0','1','false'};
		private String[] Name2_type_info = new String[]{'Name2',NAMESPACE,null,'0','1','false'};
		private String[] Street_type_info = new String[]{'Street',NAMESPACE,null,'0','1','false'};
		private String[] Country_type_info = new String[]{'Country',NAMESPACE,null,'0','1','false'};
		private String[] PostalCode_type_info = new String[]{'PostalCode',NAMESPACE,null,'0','1','false'};            
		private String[] City_type_info = new String[]{'City',NAMESPACE,null,'0','1','false'};            
		private String[] District_type_info = new String[]{'District',NAMESPACE,null,'0','1','false'};                        
		private String[] Region_type_info = new String[]{'Region',NAMESPACE,null,'0','1','false'};                        
            
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'PartnerType','Customer','Name','Name2','Street','Country','PostalCode','City','District','Region'};
            
    }
    
        /*
     * Transformer - Convert an SAPOrder into a request object.
     */ 
    public static OrderSimulateRequest.SalesOrderSimulateRequest transform(SAPOrder order) {
		
        OrderSimulateRequest.SalesOrderSimulateRequest request = new OrderSimulateRequest.SalesOrderSimulateRequest();
        
        request.DocumentType = order.documentType;
        request.SalesOrganization = order.salesOrg;
        request.DistributionChannel = order.distributionChannel;
        request.Division = order.division;
        request.PurchaseOrdertype = order.poType;

        //optional field.  don't set it if it wasn't provided
        if (String.isNotBlank(order.salesPerson)) {
            request.SalesPerson = order.salesPerson;
        }
        
        //optional field.  don't set it if it wasn't provided
        if (String.isNotBlank(order.incoterms)) {
            request.Incoterms = order.incoterms;
        }

        //optional field.  don't set it if it wasn't provided
        if (String.isNotBlank(order.paymentTerms)) {
            request.PaymentTerms = order.paymentTerms;
        }
        
        //optional field.  don't set it if it wasn't provided
        if (String.isNotBlank(order.deliveryBlock)) {
            request.DeliveryBlock = order.deliveryBlock;
        }        

        //optional field.  don't set it if it wasn't provided
        if (String.isNotBlank(order.billingBlock)) {
            request.BillingBlock = order.billingBlock;
        }        

        //optional field.  don't set it if it wasn't provided
        if (String.isNotBlank(order.orderReason)) {
            request.OrderReason = order.orderReason;
        }
        
		//customerGroup3, customerGroup4, and CustomerGroup5 intentionally excluded 
		//since they are not used in Salesforce at this time.      

		request.PurchaseOrderNumber = order.poNumber;
        
		//handle header conditions 1-4.  these are optional values
		//condition 1 --> points redemption
		//condition 2 --> discounts
		//condition 3 --> freight
		//condition 4 --> not used yet
		
        if ((order.pointsRedeemed != null) && (order.pointsRedeemed > 0)) {
            request.ConditionType1 = SAPServiceDataHelper.POINTS_REDEMPTION_CONDITION;
            request.ConditionValue1 = SAPServiceDataHelper.getSAPDecimal(order.pointsRedeemed, 2);
            request.ConditionCurrency1 = order.pointsRedeemedCurrency;
        }

        if (String.isNotBlank(order.discountType)) {
            if (order.discountType.equals('Percent - %')) {
                request.ConditionType2 = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.PERCENTAGE_DISCOUNT_CONDITION);
                request.ConditionValue2 = SAPServiceDataHelper.getSAPDecimal(order.discountValue,2) ;
                request.ConditionCurrency2 = order.discountCurrency;
            } else if (order.discountType.equals('Value')) {
                //Condition for value discount is different for North America
                if (order.userProfile.isNA) {
                	request.ConditionType2 = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.NA_VALUE_DISCOUNT_CONDITION);
                } else {
                    request.ConditionType2 = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.VALUE_DISCOUNT_CONDITION);
                }
                
                request.ConditionValue2 = SAPServiceDataHelper.getSAPDecimal(order.discountValue,2) ;
                request.ConditionCurrency2 = order.discountCurrency;
            }          
        }
        
        //freight
        if (String.isNotBlank(order.freightType)) {
            request.ConditionType3 = order.freightType;
            request.ConditionValue3 = SAPServiceDataHelper.getSAPDecimal(order.freightValue,2) ;
            request.ConditionCurrency3 = order.freightCurrency;
        }
        
       	//referenceDocument and referenceDocumentCategory intentionally excluded 
		//since they are not used in Salesforce at this time.
        

        //optional field.  don't set it if it wasn't provided
        if (String.isNotBlank(order.couponCode)) {
            request.CouponCode = order.couponCode;
        }        
        
        request.Customer = order.customerId;
		request.Language = order.language;
        
        //handle items
        //optional field.  don't set it if it wasn't provided
        if ((null <> order.items) && (order.items.size() > 0)) {
            List<OrderSimulateRequest.Items_element> items = new List<OrderSimulateRequest.Items_element>();
                    
            for (SAPOrder.Item i : order.items) {                  
                OrderSimulateRequest.Items_element item = new OrderSimulateRequest.Items_element();
                
                item.ExternalItemNumber = i.externalItemNumber;
                item.Material = i.material;                
                
                //optional field.  don't set it if it wasn't provided
                if (String.isNotBlank(i.batch)) {
                    item.Batch = i.batch;                               
                    item.Plant = i.plant;                
                    item.StorageLocation = i.storageLocation;                
                }
    
                item.RequestedQuantity = SAPServiceDataHelper.getSAPDecimal(i.requestedQuantity, 3);                
                
                item.RequestedDeliveryDate = SAPServiceDataHelper.getSAPDate(i.deliveryDate);                
               
                //handle free goods                
                if ((null != i.freeGoods) && i.freeGoods) {
                    
                    //map item category
                    //Look up the item category in the SAP_INTEGRATION custom setting to see if there is a mapping for
                    //this item category.  If so, you it.  Otherwise, use the default
                    String mappedItemCategory = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.FOC_CODE_ROOT + i.itemCategory);

                    if (null != mappedItemCategory) {
                    	item.ItemCategory = mappedItemCategory;   
                    } else {
                        item.ItemCategory = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.FOC_DEFAULT);
                    }
                    
                    item.MaterialGroup3 = i.freeGoodsAgent;                
                    item.MaterialGroup4 = i.freeGoodsCostCenter;                
                    item.MaterialGroup5 = i.freeGoodsReason;                
                }
                
                //optional field.  don't set it if it wasn't provided
                if (String.isNotBlank(i.incoterms)) {
                    item.incoTerms = i.incoterms;                
                }
                
                //optional field.  don't set it if it wasn't provided
                if (String.isNotBlank(i.shippingPoint)) {
                    item.ShippingPoint = i.shippingPoint;                
                }
    
                //referenceDocument, referenceDocumentItem and referenceDocumentCategory intentionally excluded 
				//since they are not used in Salesforce at this time.
                
                //optional field.  don't set it if it wasn't provided
                if (null <> i.useCustomerIncoterms) {
                    item.UseCustomerIncoterms = SAPServiceDataHelper.getSAPIndicator(i.useCustomerIncoterms);
                }      
                
                //handle line item conditions 1-4.  these are optional values
                //condition 1 --> discounts
                //condition 2 --> not used yet
                //condition 3 --> freight
                //condition 4 --> not used yet
                
                if (String.isNotBlank(i.discountType)) {
                    if (order.discountType.equals('Percent - %')) {
                        item.ConditionType1 = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.PERCENTAGE_DISCOUNT_CONDITION);
                        item.ConditionValue1 = SAPServiceDataHelper.getSAPDecimal(i.discountValue,2) ;
                        item.ConditionCurrency1 = i.discountCurrency;
                    } else if (i.discountType.equals('Value')) {
                        //Condition for value discount is different for North America
                        if (order.userProfile.isNA) {
                            item.ConditionType1 = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.NA_VALUE_DISCOUNT_CONDITION);
                        } else {
                            item.ConditionType1 = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.VALUE_DISCOUNT_CONDITION);
                        }
                        
                        item.ConditionValue1 = SAPServiceDataHelper.getSAPDecimal(i.discountValue,2) ;
                        item.ConditionCurrency1 = i.discountCurrency;
                    }          
                }
                
                //freight condition
                if (i.freightChanged && (null != i.freightRate)) {
                    item.ConditionType3 = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.FREIGHT_CONDITION);
                    item.ConditionValue3 = SAPServiceDataHelper.getSAPDecimal(i.freightRate, 2);
                    item.ConditionCurrency3 = i.freightCurrency;
                }
                
                items.add(item);
            }
            
            request.items = items;
        }
        
        
        //handle partners
        if ((order.partners != null) && (order.partners.size() > 0)) {
            
            List<OrderSimulateRequest.Partners_element> partners = new List<OrderSimulateRequest.Partners_element>();
            
            for (SAPOrder.Partner p : order.partners) {
                OrderSimulateRequest.Partners_element partner = new OrderSimulateRequest.Partners_element();
                
                partner.PartnerType = p.partnerType;
                partner.Customer = p.customer;

                //TODO: add WE to custom setting
                if (String.isNotBlank(p.partnerType) && p.partnerType.equals('WE')) {                
                    if ((null != p.onetimeShippingChange) && p.onetimeShippingChange) {
                        
                        //do we need to do anything special with name?
                        partner.Name = p.name;
						
                        //do we need to handle 'Attn:' here or does that come in from elsewhere?
                        partner.Name2 = p.name2;

                        partner.Street = p.street;
                        partner.Country = p.country;
                        partner.PostalCode = p.postalCode;
                        partner.City = p.city;
                        partner.District = p.district;
                        partner.Region = p.region;
                    }
                }
                    
                partners.add(partner);
            }
            
            request.Partners = partners;
        }
       
        
        return request;
    }
    
}