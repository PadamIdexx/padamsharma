/**
*       @author Vishal Negandhi
*       @date   06/10/2015   
        @description    Test class for CampaignAccountsController
        Function: This test class will test the functionality of CampaignAccountsController.
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Vishal Negandhi               16/10/2015          Modified Version
        Heather Kinney                03-May-2017         US28777 - Filter Campaign Member Status listed in Campaign Account VF page. 
    *                                                               Only display those Statuses associated with the given Campaign (CampaignMemberStatus), not the entire list.
**/

@isTest
public With Sharing class Test_CampaignAccountsController {

    @testSetup static void setupTerritories(){
        list<Territory2Type> terriType   = [SELECT id, DeveloperName from Territory2Type LIMIT 1];
        list<Territory2Model> terrModel   = [SELECT id, DeveloperName,name from Territory2Model Where State = 'Active'];

       /* Territory2Model terrModel = new Territory2Model();
        terrModel.DeveloperName='ModelName1'; // required field
        terrModel.Name = 'Name'; // required field
        terrModel.ID = '0MAj0000000PBQt';
        insert terrModel ;*/

        Territory2 objTerr = new Territory2();
        objTerr.DeveloperName = 'TestTerritory';
        objTerr.Territory2ModelId=terrModel[0].Id;
        objTerr.Name='TestTerritory'; 
        objTerr.Territory2TypeId=terriType[0].Id;
        insert objTerr;
        
        
        UserTerritory2Association u2a = new UserTerritory2Association();
        u2a.RoleInTerritory2 = 'ISR';
        u2a.UserId = UserInfo.getUserId();
        u2a.Territory2Id = objTerr.Id;
        insert u2a;
    }
    
    @testSetup static void setupData(){
        Account testAccount = new Account();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = 'Customer Testing Account';
        testAccount.CurrencyISOCode = 'USD';
        testAccount.HIN__c = 'Test123';
        testAccount.SAP_Customer_Number__c = '0'+String.valueOf(Math.random()).subString(12);
        insert testAccount;
        
        Account testAccount2 = new Account();
        testAccount2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount2.Name = 'Customer Testing Account2';
        testAccount2.CurrencyISOCode = 'USD';
        testAccount2.HIN__c = 'Test123';
        testAccount2.SAP_Customer_Number__c = '0'+String.valueOf(Math.random()).subString(12);
        insert testAccount2;
        
        Contact cont = new Contact();
        cont.LastName = 'testContact1';
        cont.AccountId = testAccount.Id;
        if(Schema.sObjectType.Contact.isCreateable() && cont != null)
            insert cont;
        
        Contact cont2 = new Contact();
        cont2.LastName = 'testContact2';
        cont2.AccountId = testAccount2.Id;
        if(Schema.sObjectType.Contact.isCreateable() && cont2 != null)
            insert cont2;
       
        System.debug('###RA'+cont);
        Campaign camp1 = CreateTestClassData.createCampaign();
        CampaignMember cm = new CampaignMember();
        System.debug('###RA'+camp1);
        cm.ContactId = cont.Id;
        cm.CampaignId = camp1.Id;
        cm.Status = 'Sent';
        if(Schema.sObjectType.CampaignMember.isCreateable() && cm != null)
        insert cm;
    }
    
    static testMethod void testCampaignAccountsController() {
        ObjectTerritory2Association o2a = new ObjectTerritory2Association();
        Account account = [Select Id From Account WHERE Name = 'Customer Testing Account'];
        Account account2 = [Select Id From Account WHERE Name = 'Customer Testing Account2'];
        Contact con = CreateTestClassData.reusableContact(account.id);
        insert con;
        
        account.primary_contact__c = con.id;
        update account;
        o2a.ObjectId = account.Id;
        o2a.AssociationCause = 'Territory2Manual';
        Territory2 t2 = [Select Id From Territory2 WHERE Name = 'TestTerritory'];
        o2a.Territory2Id = t2.Id;
        insert o2a;   
        
        test.startTest();
        CampaignAccountsController cac = new CampaignAccountsController();
        Campaign camp = [Select Id From Campaign WHERE Name = 'TestCampaign'];
        CampaignMember cmpmem = CreateTestClassData.reusableCampaignMember(camp.id, con.id);
        insert cmpmem;
        //adding another campaign member to this campaign - 5.3.2017 HKinney
        Contact con2 = CreateTestClassData.reusableContact(account2.id);
        insert con2;
        CampaignMember cmpmem2 = CreateTestClassData.reusableCampaignMember(camp.id, con2.id);
        insert cmpmem2;
        cac.getCampaigns();
        
        cac.SelectedCampaign = camp.Id;

        cac.SaveInlineChanges();  
        
        cac.fetchCampaignDetails();
        
        CampaignAccountsController.campaignMemberStatusUpdateModelMap = new Map<Id, CampaignAccountsController.CampaignMemberStatusUpdateModel>();
        CampaignAccountsController.CalledFromPage = 'true';
        
        CampaignAccountsController.getStatusesForCurrentCampaignMember(camp.Id);
        //test the changing of just one campaign status - 5.2.2017 HKinney
        CampaignAccountsController.setCampaignStatusForCurrentCampaignMember(cmpmem.id + '_sent', cmpmem.id, 'sent');
        
        //test the changing of one campaign status with two existing campaigns in the mix (first inarg below) - 5.2.2017 HKinney
        CampaignAccountsController.setCampaignStatusForCurrentCampaignMember(cmpmem.id + '_sent__' + cmpmem2.id + '_sent', cmpmem.id, 'sent');
        
        CampaignAccountsController.savePendingChanges(cmpmem.id + '_sent__' + cmpmem2.id + '_sent');
 
        //public static Map<Id, CampaignMemberStatusUpdateModel> 
        //     setCampaignStatusForCurrentCampaignMember(String changedCampaignStatusesString, String campaignMemberId, String newStatusValue) {
    
        
        //cac.FetchAccounts(); 
        //
        //System.Assert(cac.AssociatedAccounts.size() == 1, 'Campaign should be associated with Account created in test case');
        test.stopTest();           
    }  
}