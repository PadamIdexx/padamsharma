public class AccountGeocodeAddress {
    
    // static variable to determine if geocoding has already occurred
    private static Boolean geocodingCalled = false;
    // wrapper method to prevent calling future methods from an existing future context
    public static void DoAddressGeocode(Id accountId) {
        if (geocodingCalled || System.isFuture()) {
            System.debug(LoggingLevel.WARN,
                         '***Address Geocoding Future Method Already Called - Aborting...');
            return;
        }
        // if not being called from future context, geocode the address
        geocodingCalled = true;
        geocodeAddress(accountId);
    }
    public static boolean runGeocoding = true;
    
    // we need a future method to call Google Geocoding API from Salesforce
    @future (callout=true)
    static private void geocodeAddress(id accountId) {
        if(runGeocoding){
            // Key for Google Maps Geocoding API
            String geocodingKey = 'AIzaSyA_mcRUJHc7zlKH_z6kZNtEr8i57Qc5XDg';
            // get the passed in address
            Account geoAccount = [SELECT ShippingStreet, ShippingCity, ShippingState, ShippingCountry,
                                  ShippingPostalCode
                                  FROM
                                  Account
                                  WHERE
                                  id = :accountId];
            
            // check that we have enough information to geocode the address
            if ((geoAccount.ShippingStreet == null) || (geoAccount.ShippingCity == null)) {
                System.debug(LoggingLevel.WARN,
                             'Insufficient Data to Geocode Address');
                return;
            }
            // create a string for the address to pass to Google Geocoding API
            String geoAddress = Utils.formatAddress(geoAccount.ShippingStreet,
                                                    geoAccount.ShippingCity,
                                                    geoAccount.ShippingState,
                                                    geoAccount.ShippingCountry,
                                                    geoAccount.ShippingPostalCode);
            
            // encode the string so we can pass it as part of URL
            geoAddress = EncodingUtil.urlEncode(geoAddress, 'UTF-8');
            // build and make the callout to the Geocoding API
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint('https://maps.googleapis.com/maps/api/geocode/json?address='
                                + geoAddress + '&key=' + geocodingKey
                                + '&sensor=false');
            request.setMethod('GET');
            request.setTimeout(60000);
            try {
                // make the http callout
                HttpResponse response = http.send(request);
                System.debug('response.body='+response.getBody());
                // parse JSON to extract co-ordinates
                JSONParser responseParser = JSON.createParser(response.getBody());
                // initialize co-ordinates
                double latitude = null;
                double longitude = null;
                while (responseParser.nextToken() != null) {
                    if ((responseParser.getCurrentToken() == JSONToken.FIELD_NAME)
                        && (responseParser.getText() == 'location')) {
                            responseParser.nextToken();
                            while (responseParser.nextToken() != JSONToken.END_OBJECT) {
                                String locationText = responseParser.getText();
                                responseParser.nextToken();
                                if (locationText == 'lat') {
                                    latitude = responseParser.getDoubleValue();
                                } else if (locationText == 'lng') {
                                    longitude = responseParser.getDoubleValue();
                                }
                            }
                        }
                }
                // update co-ordinates on address if we get them back
                if (latitude != null) {
                    geoAccount.Location__Latitude__s = latitude;
                    geoAccount.Location__Longitude__s = longitude;
                    update geoAccount;
                }
            } catch (Exception e) {
                System.debug(LoggingLevel.ERROR,
                             'Error Geocoding Address - ' + e.getMessage());
            }
        }
    }
}