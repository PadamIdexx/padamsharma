/*
 * OrderCreateRequest
 * 
 * This class represents the shape the Request for the SAP Sales Order Create 
 * operation in SalesProcessing_OutService as defined in Beacon OSB under
 * SAPOrderManagment.
 */ 
public with sharing class OrderCreateRequest {
    //Web Service call out related constants.
    //
    // SOAP Action
    public final static String SOAPACTION = 'http://sap.com/xi/WebService/soap1.1';
    
    // XML Namespace for this object
    // Salesforce representation of Namespace
    public final static String NAMESPACE = 'http://idexx.com/sappo/sales/salesforce';
    public final static String SF_NAMESPACE = 'idexxComSappoSalesSalesforce';
    
    // XML Namespace of types    
    // Salesforce representation of Namespace   
    public final static String XMLTYPE_NAMESPACE = 'http://idexx.com/sappo/global/types';
	public final static String SF_XMLTYPE_NAMESPACE = 'idexxComSappoGlobalTypes';
     
	// Name of Class that carries the payload.  There is no generic way to get this at this 
	// from the obect at runtime at this time.    
    public final static String PAYLOAD_CLASSNAME = 'SalesOrderCreateRequest';
 
    public class SalesOrderCreateRequest {
        
    	public String DocumentType;
        public String SalesOrganization;
        public String DistributionChannel;
        public String Division;
        public String PurchaseOrderType;
        public String SalesPerson;        
        public String Incoterms;
        public String PaymentTerms;        
        public String DeliveryBlock;
        public String BillingBlock;
        public String OrderReason;
        public String CompleteDelivery;
        public String CustomerGroup3;
        public String CustomerGroup4;
        public String CustomerGroup5;        
        public String PurchaseOrderNumber;        
		public String ExternalDocumentNumber;              
        public String ReferenceDocument;
        public String ReferenceDocumentCategory;
        public String Customer;
        public String CouponCode;
        public String Language;
        public String UseRewardPoints;
        
        public Items_element[] Items;
		public Conditions_element[] Conditions;
        public Partners_element[] Partners;
        public CreditCard_element[] CreditCard;
        public Texts_element[] Texts;

        private String[] DocumentType_type_info = new String[]{'DocumentType',NAMESPACE,null,'1','1','false'};
        private String[] SalesOrganization_type_info = new String[]{'SalesOrganization',NAMESPACE,null,'1','1','false'};
        private String[] DistributionChannel_type_info = new String[]{'DistributionChannel',NAMESPACE,null,'1','1','false'};
        private String[] Division_type_info = new String[]{'Division',NAMESPACE,null,'1','1','false'};
        private String[] PurchaseOrderType_type_info = new String[]{'PurchaseOrderType',NAMESPACE,null,'1','1','false'};
        private String[] SalesPerson_type_info = new String[]{'SalesPerson',NAMESPACE,null,'1','1','false'};            
        private String[] Incoterms_type_info = new String[]{'Incoterms',NAMESPACE,null,'0','1','false'};
        private String[] PaymentTerms_type_info = new String[]{'PaymentTerms',NAMESPACE,null,'0','1','false'};
        private String[] DeliveryBlock_type_info = new String[]{'DeliveryBlock',NAMESPACE,null,'0','1','false'};            
        private String[] BillingBlock_type_info = new String[]{'BillingBlock',NAMESPACE,null,'0','1','false'};
        private String[] OrderReason_type_info = new String[]{'OrderReason',NAMESPACE,null,'0','1','false'};
		private String[] CompleteDelivery_type_info = new String[]{'CompleteDelivery',NAMESPACE,null,'0','1','false'};
		private String[] CustomerGroup3_type_info = new String[]{'CustomerGroup3',NAMESPACE,null,'0','1','false'};
        private String[] CustomerGroup4_type_info = new String[]{'CustomerGroup4',NAMESPACE,null,'0','1','false'};
        private String[] CustomerGroup5_type_info = new String[]{'CustomerGroup5',NAMESPACE,null,'0','1','false'};            
        private String[] PurchaseOrderNumber_type_info = new String[]{'PurchaseOrderNumber',NAMESPACE,null,'0','1','false'};
        private String[] ExternalDocumentNumber_type_info = new String[]{'ExternalDocumentNumber',NAMESPACE,null,'0','1','false'};            

        private String[] ReferenceDocument_type_info = new String[]{'ReferenceDocument',NAMESPACE,null,'0','1','false'};
        private String[] ReferenceDocumentCategory_type_info = new String[]{'ReferenceDocumentCategory',NAMESPACE,null,'0','1','false'};                        
        private String[] Customer_type_info = new String[]{'Customer',NAMESPACE,null,'1','1','false'};            
        private String[] Language_type_info = new String[]{'Language',NAMESPACE,null,'1','1','false'};            
        private String[] CouponCode_type_info = new String[]{'CouponCode',NAMESPACE,null,'0','1','false'};            
        private String[] UseRewardPoints_type_info = new String[]{'UseRewardPoints',NAMESPACE,null,'0','1','false'};            
            
            
        private String[] Items_type_info = new String[]{'Items',NAMESPACE,null,'0','-1','false'};            
        private String[] Conditions_type_info = new String[]{'Conditions',NAMESPACE,null,'0','-1','false'};            
        private String[] CreditCard_type_info = new String[]{'CreditCard',NAMESPACE,null,'0','-1','false'};            
        private String[] Partners_type_info = new String[]{'Partners',NAMESPACE,null,'0','-1','false'};               
        private String[] Texts_type_info = new String[]{'Texts',NAMESPACE,null,'0','-1','false'};               

            
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'DocumentType','SalesOrganization','DistributionChannel','Division','PurchaseOrderType','SalesPerson','Incoterms',
            													'PaymentTerms','DeliveryBlock','BillingBlock','OrderReason','CompleteDelivery','CustomerGroup3','CustomerGroup4','CustomerGroup3',
            													'PurchaseOrderNumber','ExternalDocumentNumber','ReferenceDocument','ReferenceDocumentCategory',
            													'Customer','Language','UseRewardPoints','CouponCode','Items','Conditions','Partners','CreditCard','Texts'};
    }
    

    public class Items_element {
        public String ItemNumber;
        public String Material;
        public String Batch;
        public String Plant;
        public String StorageLocation;
        public String RequestedQuantity;
        public String ItemCategory;
        public String RequestedDeliveryDate;
        public String MaterialGroup3;
        public String MaterialGroup4;
        public String MaterialGroup5;
        public String Incoterms;
        public String ReferenceDocument;
        public String ReferenceDocumentItem;
        public String ReferenceDocumentCategory;
        public String ShippingPoint;
        public String UseCustomerIncoterms;
        public String DesignatedBatch;

        private String[] ItemNumber_type_info = new String[]{'ItemNumber',NAMESPACE,null,'0','1','false'};
        private String[] Material_type_info = new String[]{'Material',NAMESPACE,null,'1','1','false'};
        private String[] Batch_type_info = new String[]{'Batch',NAMESPACE,null,'0','1','false'};
        private String[] Plant_type_info = new String[]{'Plant',NAMESPACE,null,'0','1','false'};
        private String[] StorageLocation_type_info = new String[]{'StorageLocation',NAMESPACE,null,'0','1','false'};
        private String[] RequestedQuantity_type_info = new String[]{'RequestedQuantity',NAMESPACE,null,'1','1','false'};
        private String[] ItemCategory_type_info = new String[]{'ItemCategory',NAMESPACE,null,'0','1','false'};
        private String[] RequestedDeliveryDate_type_info = new String[]{'RequestedDeliveryDate',NAMESPACE,null,'1','1','false'};
        private String[] MaterialGroup3_type_info = new String[]{'MaterialGroup3',NAMESPACE,null,'0','1','false'};
        private String[] MaterialGroup4_type_info = new String[]{'MaterialGroup4',NAMESPACE,null,'0','1','false'};
        private String[] MaterialGroup5_type_info = new String[]{'MaterialGroup5',NAMESPACE,null,'0','1','false'};
        private String[] Incoterms_type_info = new String[]{'Incoterms',NAMESPACE,null,'0','1','false'};
        private String[] ReferenceDocument_type_info = new String[]{'ReferenceDocument',NAMESPACE,null,'0','1','false'};
        private String[] ReferenceDocumentItem_type_info = new String[]{'ReferenceDocumentItem',NAMESPACE,null,'0','1','false'};
        private String[] ReferenceDocumentCategory_type_info = new String[]{'ReferenceDocumentCategory',NAMESPACE,null,'0','1','false'};
        private String[] ShippingPoint_type_info = new String[]{'ShippingPoint',NAMESPACE,null,'0','1','false'};            
      	private String[] UseCustomerIncoterms_type_info = new String[]{'UseCustomerIncoterms',NAMESPACE,null,'0','1','false'};
      	private String[] DesignatedBatch_type_info = new String[]{'DesignatedBatch',NAMESPACE,null,'0','1','false'};
            
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'ItemNumber','Material','Batch', 'Plant','StorageLocation','RequestedQuantity',
            													'ItemCategory','RequestedDeliveryDate','MaterialGroup3','MaterialGroup4','MaterialGroup5',
            													'Incoterms','ShippingPoint','ReferenceDocument','ReferenceDocumentItem','ReferenceDocumentCategory',
            													'DesignatedBatch','UseCustomerIncoterms'};
    }    
    
     public class Conditions_element {
        public String ItemNumber;
		public String ConditionStep;
        public String ConditionCount;         
        public String ConditionType;
        public String UpdateFlag;         
        public String ConditionValue;         
        public String Currency_x;
    
        private String[] ItemNumber_type_info = new String[]{'ItemNumber',NAMESPACE,null,'0','1','false'};
        private String[] ConditionStep_type_Info = new String[]{'ConditionStep',NAMESPACE,null,'0','1','false'};
        private String[] ConditionCount_type_Info = new String[]{'ConditionCount',NAMESPACE,null,'0','1','false'};            
        private String[] ConditionType_type_Info = new String[]{'ConditionType',NAMESPACE,null,'1','1','false'};
        private String[] UpdateFlag_type_Info = new String[]{'UpdateFlage',NAMESPACE,null,'1','1','false'};            
        private String[] ConditionValue_type_info = new String[]{'ConditionValue',NAMESPACE,null,'0','1','false'};
        private String[] Currency_x_type_info = new String[]{'Currency',NAMESPACE,null,'0','1','false'};
        
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'ItemNumber','ConditionStep','ConditionCount','ConditionType','UpdateFlag','ConditionValue','Currency_x'};
    }    
    
    public class Partners_element {
    	public String PartnerType;
        public String Customer;
        public String Name;
        public String Name2;
        public String Street;
        public String Country;
        public String PostalCode;
        public String City;
        public String District;
        public String Region;
        public String TransportationZone;
        
        private String[] PartnerType_type_info = new String[]{'PartnerType',NAMESPACE,null,'1','1','false'};
		private String[] Customer_type_info = new String[]{'Customer',NAMESPACE,null,'1','1','false'};
		private String[] Name_type_info = new String[]{'Name',NAMESPACE,null,'0','1','false'};
		private String[] Name2_type_info = new String[]{'Name2',NAMESPACE,null,'0','1','false'};
		private String[] Street_type_info = new String[]{'Street',NAMESPACE,null,'0','1','false'};
		private String[] Country_type_info = new String[]{'Country',NAMESPACE,null,'0','1','false'};
		private String[] PostalCode_type_info = new String[]{'PostalCode',NAMESPACE,null,'0','1','false'};            
		private String[] City_type_info = new String[]{'City',NAMESPACE,null,'0','1','false'};            
		private String[] District_type_info = new String[]{'District',NAMESPACE,null,'0','1','false'};                        
		private String[] Region_type_info = new String[]{'Region',NAMESPACE,null,'0','1','false'};                        
   		private String[] TransportationZone_type_info = new String[]{'TransportationZone',NAMESPACE,null,'0','1','false'};                        

        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'PartnerType','Customer','Name','Name2','Street','Country','PostalCode','City','District','Region','TransportationZone'};
            
    }

    public class Texts_element {
        public String TextID;
        public String Language;
        public String TextLine;

        private String[] TextID_type_info = new String[]{'TextID',NAMESPACE,null,'0','1','false'};                        
		private String[] Language_type_info = new String[]{'Language',NAMESPACE,null,'0','1','false'};                                    
		private String[] TextLine_type_info = new String[]{'TextLine',NAMESPACE,null,'0','1','false'};                                                
            
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'TextID','Language','TextLine'};
   }    

    public class CreditCard_element {
        public String CreditCardType;
        public String CreditCardNumber;
        public String CreditCardDate;
        public String CreditCardName;

		private String[] CreditCardType_type_info = new String[]{'CreditCardType',NAMESPACE,null,'0','1','false'};                        
        private String[] CreditCardNumber_type_info = new String[]{'CreditCardNumber',NAMESPACE,null,'0','1','false'};                        
		private String[] CreditCardDate_type_info = new String[]{'CreditCardDate',NAMESPACE,null,'0','1','false'};                                    
		private String[] CreditCardName_type_info = new String[]{'CreditCardName',NAMESPACE,null,'0','1','false'};                                                
            
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'CreditCardType','CreditCardNumber','CreditCardDate','CreditCardName'};
   }       
    
    /*
     * Transformer - Convert an SAPOrder into a request object.
     */ 
    public static OrderCreateRequest.SalesOrderCreateRequest transform(SAPOrder order) {
        
        OrderCreateRequest.SalesOrderCreateRequest request = new OrderCreateRequest.SalesOrderCreateRequest();
        
        request.DocumentType = order.documentType;
        request.SalesOrganization = order.salesOrg;
        request.DistributionChannel = order.distributionChannel;
        request.Division = order.division;
        request.PurchaseOrdertype = order.poType;
        request.SalesPerson = order.salesPerson;
        
        //optional field.  don't set it if it wasn't provided
        if (String.isNotBlank(order.incoterms)) {
            request.Incoterms = order.incoterms;
        }

        //optional field.  don't set it if it wasn't provided
        if (String.isNotBlank(order.paymentTerms)) {
            request.PaymentTerms = order.paymentTerms;
        }
        
        //optional field.  don't set it if it wasn't provided
        if (String.isNotBlank(order.deliveryBlock)) {
            request.DeliveryBlock = order.deliveryBlock;
        }        

        //optional field.  don't set it if it wasn't provided
        if (String.isNotBlank(order.billingBlock)) {
            request.BillingBlock = order.billingBlock;
        }        

        //optional field.  don't set it if it wasn't provided
        if (String.isNotBlank(order.orderReason)) {
            request.OrderReason = order.orderReason;
        }

        //optional field.  don't set it if it wasn't provided
        if (null != order.completeDelivery) {
            request.CompleteDelivery = SAPServiceDataHelper.getSAPIndicator(order.completeDelivery);
        }        
        
		//customerGroup3, customerGroup4, and CustomerGroup5 intentionally excluded 
		//since they are not used in Salesforce at this time.       

		request.PurchaseOrderNumber = order.poNumber;
		request.ExternalDocumentNumber = order.externalDocumentNumber;
        
        
		//referenceDocument and referenceDocumentCategory intentionally excluded 
		//since they are not used in Salesforce at this time.      

        request.Customer = order.customerId;
		request.Language = order.language;

        //optional field.  don't set it if it wasn't provided
        if (null != order.useRewardPoints) {
            request.UseRewardPoints = SAPServiceDataHelper.getSAPIndicator(order.useRewardPoints);
        }        
                
        
        //optional field.  don't set it if it wasn't provided
        if (String.isNotBlank(order.couponCode)) {
            request.CouponCode = order.couponCode;
        }        
        
        //Conditions
        List<OrderCreateRequest.Conditions_element> conditions = new List<OrderCreateRequest.Conditions_element>();
            
        //process header conditions            
        if ((order.pointsRedeemed != null) && (order.pointsRedeemed > 0)) {
            OrderCreateRequest.Conditions_element condition = new OrderCreateRequest.Conditions_element();
            
            condition.conditionType = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.POINTS_REDEMPTION_CONDITION);
            condition.conditionValue = SAPServiceDataHelper.getSAPDecimal(order.discountValue,2) ;
            condition.currency_x = order.discountCurrency;
            condition.updateFlag = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.CREATE_INDICATOR);
            conditions.add(condition);
        }
        
        if (String.isNotBlank(order.discountType)) {
            if (order.discountType.equals('Percent - %')) {
                OrderCreateRequest.Conditions_element condition = new OrderCreateRequest.Conditions_element();
                
                condition.conditionType = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.PERCENTAGE_DISCOUNT_CONDITION);
                condition.conditionValue = SAPServiceDataHelper.getSAPDecimal(order.discountValue,2) ;
                condition.currency_x = order.discountCurrency;
	            condition.updateFlag = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.CREATE_INDICATOR);
                
                conditions.add(condition);
            } else if (order.discountType.equals('Value')) {
                OrderCreateRequest.Conditions_element condition = new OrderCreateRequest.Conditions_element();
                
                //Condition for value discount is different for North America
                if (order.userProfile.isNA) {
                	condition.conditionType = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.NA_VALUE_DISCOUNT_CONDITION);
                } else {
                    condition.conditionType = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.VALUE_DISCOUNT_CONDITION);
                }
                
                condition.conditionValue = SAPServiceDataHelper.getSAPDecimal(order.discountValue,2) ;
                condition.currency_x = order.discountCurrency;
	            condition.updateFlag = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.CREATE_INDICATOR);
                
                conditions.add(condition);                    
            }
        }       
        
        //freight
        if (String.isNotBlank(order.freightType)) {
	        OrderCreateRequest.Conditions_element condition = new OrderCreateRequest.Conditions_element();
            condition.ConditionType = order.freightType;
			condition.ConditionValue = SAPServiceDataHelper.getSAPDecimal(order.freightValue, 2);
            condition.Currency_x = order.freightCurrency;
        }
        
        request.conditions = conditions;
        
        //handle items
        //optional field.  don't set it if it wasn't provided
        if ((null <> order.items) && (order.items.size() > 0)) {
            List<OrderCreateRequest.Items_element> items = new List<OrderCreateRequest.Items_element>();
                    
            for (SAPOrder.Item i : order.items) {                  
                OrderCreateRequest.Items_element item = new OrderCreateRequest.Items_element();

                //optional field.  don't set it if it wasn't provided
                if (String.isNotBlank(i.ItemNumber)) {                
                	item.ItemNumber = i.ItemNumber;
                }
                 
                item.Material = i.material;                
                
                //optional field.  don't set it if it wasn't provided
                if (String.isNotBlank(i.batch)) {
                    item.Batch = i.batch;                               
                    item.Plant = i.plant;                
                    item.StorageLocation = i.storageLocation;   

                    //optional field.  don't set it if it wasn't provided
                    if (null <> i.designatedBatch) {
                        item.DesignatedBatch = SAPServiceDataHelper.getSAPIndicator(i.designatedBatch);
                    }                        
                }
    
                item.RequestedQuantity = SAPServiceDataHelper.getSAPDecimal(i.requestedQuantity, 3);                

                //optional field.  don't set it if it wasn't provided
                if (String.isNotBlank(i.itemCategory)) {
                    item.ItemCategory = i.itemCategory;                
                }                
                
                item.RequestedDeliveryDate = SAPServiceDataHelper.getSAPDate(i.deliveryDate);       
                
                //handle free goods
                
                if ((null != i.freeGoods) && i.freeGoods) {
                    
                    //map item category
                    //Look up the item category in the SAP_INTEGRATION custom setting to see if there is a mapping for
                    //this item category.  If so, you it.  Otherwise, use the default
                    String mappedItemCategory = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.FOC_CODE_ROOT + i.itemCategory);

                    if (null != mappedItemCategory) {
                    	item.ItemCategory = mappedItemCategory;   
                    } else {
                        item.ItemCategory = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.FOC_DEFAULT);
                    }
                    
                    item.MaterialGroup3 = i.freeGoodsAgent;                
                    item.MaterialGroup4 = i.freeGoodsCostCenter;                
                    item.MaterialGroup5 = i.freeGoodsReason;                                    
                }
                
                //optional field.  don't set it if it wasn't provided
                if (String.isNotBlank(i.incoterms)) {
                    item.incoTerms = i.incoterms;                
                }
                
                //optional field.  don't set it if it wasn't provided
                if (String.isNotBlank(i.shippingPoint)) {
                    item.ShippingPoint = i.shippingPoint;                
                }

                //ReferenceDocument, ReferenceDocumentItem, and ReferenceDocumentCategory 
                //omitted on purposes because they are not used in salesforce at this time. 
              
                
                //optional field.  don't set it if it wasn't provided
                if (null <> i.useCustomerIncoterms) {
                    item.UseCustomerIncoterms = SAPServiceDataHelper.getSAPIndicator(i.useCustomerIncoterms);
                }      
                

                //add conditions
                if (String.isNotBlank(i.discountType)) {
                    if (i.discountType.equals('Percent - %')) {
                        OrderCreateRequest.Conditions_element condition = new OrderCreateRequest.Conditions_element();
                        
                        condition.itemNumber = i.itemNumber;
                        condition.conditionType = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.PERCENTAGE_DISCOUNT_CONDITION);
                        condition.conditionValue = SAPServiceDataHelper.getSAPDecimal(i.discountValue,2) ;
                        condition.currency_x = order.discountCurrency;
                        condition.updateFlag = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.CREATE_INDICATOR);

                        conditions.add(condition);
                    } else if (i.discountType.equals('Value')) {
                        OrderCreateRequest.Conditions_element condition = new OrderCreateRequest.Conditions_element();
                        
                        condition.itemNumber = i.itemNumber;
                        
                        //Condition for value discount is different for North America
                        if (order.userProfile.isNA) {
                        	condition.conditionType = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.NA_VALUE_DISCOUNT_CONDITION);
                        } else {
                            condition.conditionType = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.VALUE_DISCOUNT_CONDITION);
                        }
                        
                        condition.conditionValue = SAPServiceDataHelper.getSAPDecimal(i.discountValue,2) ;
                        condition.currency_x = order.discountCurrency;
			            condition.updateFlag = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.CREATE_INDICATOR);

                        conditions.add(condition);                    
                    }
                }                  
                
                //freight condition
                if (i.freightChanged && (null != i.freightRate)) {
                    OrderCreateRequest.Conditions_element condition = new OrderCreateRequest.Conditions_element();
                    
                    condition.itemNumber = i.itemNumber;
                    
                    condition.conditionType = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.FREIGHT_CONDITION);
                    condition.conditionValue = SAPServiceDataHelper.getSAPDecimal(i.freightRate, 2);
                    condition.currency_x = i.currencyCode;
                    //We are updating a generated condition so this will be an update
		            condition.updateFlag = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.UPDATE_INDICATOR);

                    conditions.add(condition);
                }
                
                items.add(item);
            }
            
            request.items = items;
        }
        
        
        //handle partners
        if ((order.partners != null) && (order.partners.size() > 0)) {
            
            List<OrderCreateRequest.Partners_element> partners = new List<OrderCreateRequest.Partners_element>();
            
            for (SAPOrder.Partner p : order.partners) {
                OrderCreateRequest.Partners_element partner = new OrderCreateRequest.Partners_element();
                
                partner.PartnerType = p.partnerType;
                partner.Customer = p.customer;

                //TODO: add WE to custom setting
                if (String.isNotBlank(p.partnerType) && p.partnerType.equals('WE')) {                
                    if ((null != p.onetimeShippingChange) && p.onetimeShippingChange) {
                        
                        //do we need to do anything special with name?
                        partner.Name = p.name;
						
                        //do we need to handle 'Attn:' here or does that come in from elsewhere?
                        partner.Name2 = p.name2;

                        partner.Street = p.street;
                        partner.Country = p.country;
                        partner.PostalCode = p.postalCode;
                        partner.City = p.city;
                        partner.District = p.district;
                        partner.Region = p.region;
                    }
                }
                    
                partners.add(partner);
            }
            
            request.Partners = partners;
        }
       
        //Credit cards
        if ((null != order.creditcards) && (order.creditcards.size() > 0)) {
            List<OrderCreateRequest.CreditCard_element> cards = new List<OrderCreateRequest.CreditCard_element>();
            
            for (SAPOrder.CreditCard c : order.creditcards) {
                OrderCreateRequest.CreditCard_element card = new OrderCreateRequest.CreditCard_element();
                card.CreditCardType = c.creditCardType;
                card.CreditCardNumber = c.creditCardNumber;
                card.CreditCardDate = SAPServiceDataHelper.getSAPDate(c.creditCardDate);
                card.CreditCardName = c.creditCardName;
                
                cards.add(card);
            }
            
            request.CreditCard = cards;
        }
        
        //Texts - header text, CIG and CUG
        //optional field.  don't set it if it wasn't provided		
        if (String.isnotBlank(order.headerText)
           	|| String.isNotBlank(order.CIG) 
           	|| String.isNotBlank(order.CUG)) {
            
			List<OrderCreateRequest.Texts_element> texts = new List<OrderCreateRequest.Texts_element>();
            
            if (String.isNotBlank(order.CIG)) {
                OrderCreateRequest.Texts_element t = new OrderCreateRequest.Texts_element();
                t.TextID = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.CIG_TEXTID);
                t.Language = order.CIGCUGLanguage;
                t.TextLine = order.CIG;
                texts.add(t);
            }
            
            if (String.isNotBlank(order.CUG)) {
                OrderCreateRequest.Texts_element t = new OrderCreateRequest.Texts_element();
                t.TextID = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.CUG_TEXTID);
                t.Language = order.CIGCUGLanguage;
                t.TextLine = order.CUG;                
                texts.add(t);                
            }

             if (String.isNotBlank(order.headerText)) {
                List<String> chunkedText = Utils.chunkStringAtWordBoundaries(order.headerText, ' ', 132);
                
                for (String s : chunkedText) {
                    OrderCreateRequest.Texts_element t = new OrderCreateRequest.Texts_element();
                    t.TextID = SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.GENERAL_TEXTID);
                    t.Language = order.headerTextLanguage;
                    t.TextLine = s;                
                    texts.add(t);                    
                }               
            }
                
                        
            request.texts = texts;
        }    
        
        
        return request;
    }
}