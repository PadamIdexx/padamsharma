@isTest
public class Test_Data_Utility {
    
    public static void createTestAccounts(Integer numAccts, Integer numContactsPerAcct){
        List<Account> accts = new List<Account>();
        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Account' LIMIT 1];
        for(Integer i=0;i<=numAccts;i++) {
            Account a = new Account(Name='TestAccount' + i, SAP_Customer_Number__c = '1003' + i);
            a.BillingStreet = '123 test road';
            a.BillingCity = 'Testville';
            a.BillingPostalCode = '04101';
            a.BillingState = 'Maine';
            a.BillingCountry = 'United States';
            a.CurrencyIsoCode = 'USD';
            a.TIN__c = '123445';
            a.ShippingCity = 'Maine';
            a.ShippingStreet = '123 Test St';
            a.ShippingCountry = 'United States'; //3.24.2017 HKinney added so Test_GenerateQuoteAction could fire.
            
            a.RecordTypeId = rt.Id;
            accts.add(a);
        }
        try{
            insert accts;
        }
        catch(dmlException ex){
            SYSTEM.debug('error inserting account ' + ex.getCause() + ex.getDmlMessage(0));
        }
        if(numContactsPerAcct != 0){
            List<Contact> cons = new List<Contact>();
            for (Integer j=0;j<numAccts;j++) {
                Account acct = accts[j];            
                // For each account just inserted, add contacts
                for (Integer k=numContactsPerAcct*j;k<numContactsPerAcct*(j+1);k++) {
                    cons.add(new Contact(firstname='Test'+k,
                                         lastname='Test'+k,
                                         AccountId=acct.Id, Contact_Role__c = 'Other'));
                    
                }
            }
            // Insert all contacts for all accounts
            try{
                insert cons;
            }
            catch(dmlException ex){
                SYSTEM.debug('error inserting contact ' + ex.getCause() + ex.getDmlMessage(0));
            }
        }
    }
    
    public static void createTestAssets(Integer numAccts, Integer numAssets){
        List<Account> accts = new List<Account>();
        
        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Account' LIMIT 1];
        
        
        for(Integer i=0;i<numAccts;i++) {
            Account a = new Account(Name='TestAccount' + i, SAP_Customer_Number__c = '1003' + i);
            a.BillingStreet = '123 test road';
            a.BillingCity = 'Testville';
            a.BillingPostalCode = '04101';
            a.BillingState = 'Maine';
            a.BillingCountry = 'United States';
            a.CurrencyIsoCode = 'USD';
            a.TIN__c = '123445';
            a.ShippingCountry = 'United States'; //4.12.2017 added
            a.RecordTypeId = rt.Id;
            accts.add(a);
        }
        insert accts;
        
        List<Asset> asst = new List<Asset>();
        for (Integer j=0;j<numAccts;j++) {
            Account acct = accts[j];   
            for(Integer k=numAssets*j;k<numAssets*(j+1);k++){
                asst.add(new Asset(Name='TestAsset'+k,
                                   AccountId = acct.Id));
                
                
            }
        }
        insert asst;
    }
    
    public static void createTestOpps(Integer numOpps, Account acct){
        List<Opportunity> opps = new List<Opportunity>();
        
        for(Integer i=0;i<=numOpps;i++){
            Opportunity opp = new Opportunity();
            Opp.AccountId = acct.Id;
            Opp.CurrencyIsoCode = acct.CurrencyIsoCode;
            Opp.StageName = '4 commitment';
            Opp.Name = 'Test Opp ' + i;
            Opp.CloseDate = date.today();
            
            opps.add(opp);
            
        }
        try{
            insert opps;
        }
        catch(dmlException ex){
            SYSTEM.debug('error inserting Opp ' + ex.getCause() + ex.getDmlMessage(0));
        }
    }
    
    public static void createTestOrders(Integer numOrders, Account acct){
        List<Order> orders = new List<Order>();
        
        for(Integer i=0;i<=numOrders; i++){
            Order ord = new Order();
            ord.AccountId = acct.id;
            ord.EffectiveDate = date.today();
            ord.Status = 'Draft';
            
            orders.add(ord);
            
        }
        system.debug('orders ' + orders);
        try{
            insert orders;
        }
        catch(dmlException ex){
            SYSTEM.debug('error inserting orders ' + ex.getCause() + ex.getDmlMessage(0));
        }
        
    }
    public static void createTestQuote(Integer numQuotes){
        List<Quote> qts = new List<Quote>();
        List<Account> accts = new List<Account>();
        List<Opportunity> opps = new List<Opportunity>();
        
        createTestAccounts(1, 1);
        accts = ([SELECT Id, Name, CurrencyIsoCode FROM Account WHERE Name LIKE '%TestAccount%' ]);
        createTestOpps(1,accts.get(0));
		opps = ([SELECT Id, Name FROM Opportunity WHERE Name LIKE '%Test Opp%' ]);
        
        for(Integer i=0;i<numQuotes;i++) {
            Quote q = new Quote(Name='Test Quote' + i);
            q.Date_of_Offer_Expiration__c = date.today();
            q.OpportunityId = opps.get(0).id;
            qts.add(q);
        }
        insert qts;
        
    }
    public static void createTestLims(Integer numLims, Integer numComp){
        List<LIMS_Test__c> lims = new List<LIMS_Test__c >();
        
        for(Integer i=0;i<numLims;i++) {
            LIMS_Test__c  l = new LIMS_Test__c (Name='TestLims' + i,  Test_Code__c = '1003' + i);
            
            l.LIMS_Test_Name__c = 'Test Test' + i;
            l.LIMS__c = 'Antrim';
            l.Species__c = 'Dog';
            l.Test_Type__c = '';
            l.Turnaround_Time__c = '24 hrs';
            l.Interferences__c = '';
            l.Storage_Stability__c = '';
            l.Submission_Requirements__c = '';
            l.Min_Specimen_Requirements__c = '';
            l.Acceptable_Specimens__c = '';
            l.Internal_Comments__c = '';
            l.Interferences__c = '';
            l.Interpretation_Comments__c = '';
            
            lims.add(l);
        }
        insert lims;
        
        List<LIMS_Test_Component__c> comps = new List<LIMS_Test_Component__c>();
        for (Integer j=0;j<numLims;j++) {
            LIMS_Test__c lim = lims[j];   
            for(Integer k=numComp*j;k<numComp*(j+1);k++){
                comps.add(new LIMS_Test_Component__c(Component_Test_Name__c='TestComp'+k,
                                                     Component_Test_Code__c = '0123'+k,LIMS_Test_Component__c = lim.Test_Code__c, LIMS_Test__c = lim.Id));
                
                
            }
        }
        insert comps;
        
        
        
        
    }
    
    
    
}