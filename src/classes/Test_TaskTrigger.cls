/************************************************************************************
*      @author         Raushan Anand
*      @date           12-Dec-2015
*      @description    Test class for Task Trigger              
*       
*       Modification Log:
*       ---------------------------------------------------------------------
*       Developer                  Date                Description
*       ---------------------------------------------------------------------
*       Raushan Anand             12-Dec-2015           Initial Version   
*************************************************************************************/
@isTest 
public with sharing class Test_TaskTrigger {
    static testmethod void testSuccessfulonTask(){
        Account acc = CreateTestClassData.createCustomerAccount(); // Insert Account
        Contact cont = CreateTestClassData.createContact(acc.Id);
        Task tsk = CreateTestClassData.reusableTask();
        tsk.WhoId =cont.Id; 
        tsk.WhatId=acc.Id; // Associate Account to Task
        tsk.Successful__c = true;
        INSERT tsk;   // Insert Task
        tsk.Successful__c = true;
        UPDATE tsk;
        List<Account> accList = [SELECT Id, Successful__c FROM Account WHERE ID=: acc.Id LIMIT 1];
        System.assertEquals(false, accList[0].Successful__c);
    }
    static testmethod void testLastUpdateOnTask(){
        Account acc = CreateTestClassData.createCustomerAccount(); // Insert Account
        Contact cont = CreateTestClassData.createContact(acc.Id);
        Task tsk = CreateTestClassData.reusableTask();
        tsk.WhoId =cont.Id; 
        tsk.RecordTypeId=Schema.SObjectType.Task.getRecordTypeInfosByName().get('Log An Interaction').getRecordTypeId();
        tsk.WhatId=acc.Id; // Associate Account to Task
        acc.My_Last_Touch_Date__c = System.now();
        List<Task> taskList = new List<Task>();
        taskList.add(tsk);
        TaskTriggerUtility.updateLastTouchDate(taskList);
        System.assertEquals(Date.today(), acc.My_Last_Touch_Date__c.date());
        }
    
}