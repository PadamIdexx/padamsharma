@isTest
public class Test_SmartServiceDeviceInfoController {
    public static testmethod void testWebServiceAync() {
        
        IDEXX_Integration_Service_Properties__c customsettings = new IDEXX_Integration_Service_Properties__c();
        customsettings.Name = 'SMART-LAST_POLL_OK_TIME';
		customsettings.TextVal__c = '300';
        insert customsettings;
        
        customsettings = new IDEXX_Integration_Service_Properties__c();
        customsettings.Name = 'SMART-SERIALNUMBER_PREFIX';
		customsettings.TextVal__c = 'IPU,VS';
        insert customsettings;
        
       //create account
       Account testAccount = new Account();
       testAccount.SAP_Customer_Number__c = '0000016242';
       PageReference pageRef = Page.SmartServiceDeviceInfo;
       pageRef.getParameters().put('sapId',testAccount.SAP_Customer_Number__c);
       Test.setCurrentPage(pageRef);
        Test.startTest();
        
        
       ApexPages.StandardController stdController = new ApexPages.StandardController(new Asset()); 
       SmartServiceDeviceInfoController controller = new SmartServiceDeviceInfoController(stdController);
        // Invoke the continuation by calling the action method
        Continuation conti = (Continuation)controller.getAllDevices();
        
        // Verify that the continuation has the proper requests
        Map<String, HttpRequest> requests = conti.getRequests();
        system.assert(requests.size() == 1);
        
        // Perform mock callout 
        // (i.e. skip the callout and call the callback method)
        HttpResponse response = new HttpResponse();
        response.setBody('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">   <S:Body xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">      <smar:getDevicesByPracticeResponse xmlns:smar="http://smartservice2.idexx.com">         <deviceByPracticeResponse>            <isPracticeSmartService2012>true</isPracticeSmartService2012>            <status>SUCCESS</status>            <practice>               <practiceName>TURNER VETERINARY SERVICE, PA</practiceName>               <sapId>16242</sapId>               <deviceInstruments>                  <deviceInstrumentName>IVLS_VS2D111L1</deviceInstrumentName>                  <deviceInstrumentType>DEVICE</deviceInstrumentType>                  <deviceSeqId>VS2D111L1</deviceSeqId>                  <lastPollingDateTime>2016-03-03T22:35:29Z</lastPollingDateTime>                  <parentDeviceSeqId>VS2D111L1</parentDeviceSeqId>                  <softwareVersion>3.41.481</softwareVersion>                  <serialNumber>VS2D111L1</serialNumber>                  <lastRefreshedOn>2016-03-03T20:47:26.675Z</lastRefreshedOn>                  <errorCode></errorCode>                  <errorDescription></errorDescription>               </deviceInstruments>               <deviceInstruments>                  <deviceInstrumentName>IVLS_VS3T7DV12</deviceInstrumentName>                  <deviceInstrumentType>DEVICE</deviceInstrumentType>                  <deviceSeqId>VS3T7DV12</deviceSeqId>                  <lastPollingDateTime>2015-08-13T14:42:39Z</lastPollingDateTime>                  <parentDeviceSeqId>VS3T7DV12</parentDeviceSeqId>                  <softwareVersion>4.20.54</softwareVersion>                  <serialNumber>VS3T7DV12</serialNumber>                  <lastRefreshedOn>2015-06-06T19:59:00.027Z</lastRefreshedOn>                  <errorCode></errorCode>                  <errorDescription></errorDescription>               </deviceInstruments>            </practice>         </deviceByPracticeResponse>      </smar:getDevicesByPracticeResponse>   </S:Body></soapenv:Envelope>');   
        
        // Set the fake response for the continuation
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        
        // Invoke callback method
        Object result = Test.invokeContinuationMethod(controller, conti);
        System.debug(controller);
 
    }

}