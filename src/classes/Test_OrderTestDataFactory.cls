/**************************************************************************************
* Apex Class Name: Test_OrderTestDataFactory
*
* Description: Test class for the OrderTestDataFactory class.
*
*                   
* Developer           Date              Description
* ------------------------------------------------------------------------------------                 
* Heather Kinney      3.8.2017          initial creation.
*
*************************************************************************************/

@isTest
private class Test_OrderTestDataFactory {
    
    @isTest static void getCreateReadRequestPositive() {
        OrderTestDataFactory.createReadRequest();    
    }
     
    @isTest static void getCreateChangeRequestPositive() {
        OrderTestDataFactory.createChangeRequest();    
    }
    
    @isTest static void getCreateChangeResponsePositive() {
        OrderTestDataFactory.createChangeResponse();    
    }

    @isTest static void getCreateReadResponsePositive() {
        OrderTestDataFactory.createReadResponse();    
    }
     
    @isTest static void getCreateDeleteRequestPositive() {
        OrderTestDataFactory.createDeleteRequest();    
    }
     
    @isTest static void getCreateDeleteResponsePositive() {
        OrderTestDataFactory.createDeleteResponse();    
    }
    
    @isTest static void getCreateSimulateRequestPositive() {
        OrderTestDataFactory.createSimulateRequest();    
    }
    
    @isTest static void getCreateSimulateResponsePositive() {
        OrderTestDataFactory.createSimulateResponse();    
    }
    
    @isTest static void getCreateCreateRequestPositive() {
        OrderTestDataFactory.createCreateRequest();    
    }

    @isTest static void getCreateCreateResponsePositive() {
        OrderTestDataFactory.createCreateResponse();    
    }
    
    @isTest static void getCreateSAPOrderForUpdateHeaderInfoPositive() {
        OrderTestDataFactory.createSAPOrderForUpdateHeaderInfo();    
    }

    @isTest static void getCreateSAPOrderForDeleteHeaderInfoPositive() {
        OrderTestDataFactory.createSAPOrderForDeleteHeaderInfo();    
    } 

    @isTest static void getCreateSAPOrderForUpdateItemPositive() {
        OrderTestDataFactory.createSAPOrderForUpdateItem();    
    }  

    @isTest static void getCreateSAPOrderForInsertItemPositive() {
        OrderTestDataFactory.createSAPOrderForInsertItem();    
    } 

    @isTest static void getCreateSAPOrderForDeleteItemPositive() {
        OrderTestDataFactory.createSAPOrderForDeleteItem();    
    } 
    
    @isTest static void getCreateSAPOrderForUpdatePartnerPositive() {
        OrderTestDataFactory.createSAPOrderForUpdatePartner();    
    }

    @isTest static void getCreateSAPOrderForDeletePartnerPositive() {
        OrderTestDataFactory.createSAPOrderForDeletePartner();    
    }
    
    @isTest static void getCreateSAPOrderForDeleteBatchOfItem() {
        OrderTestDataFactory.createSAPOrderForDeleteBatchOfItem();
    }
    
  

}