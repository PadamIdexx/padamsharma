@isTest
public class Test_ChangeWrapper {
    @isTest static void testPositive() {
        Test.startTest();
        try {
            SAPOrder order = OrderTestDataFactory.createSAPOrder();
            ChangeWrapper w = new ChangeWrapper();
            w.execute(order);
        } catch (Exception e) {
            System.debug('Got Exception which was expected.');
        }          
      
        Test.stopTest();			
    } 

    @isTest static void testBadRequest() {
        Test.startTest();
        try {
            ChangeWrapper w = new ChangeWrapper();
            w.execute(null);
        } catch (Exception e) {
            System.debug('Got Exception which was expected.');
        }          
      
        Test.stopTest();			
    }   
}