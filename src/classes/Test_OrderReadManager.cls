/**************************************************************************************
* Apex Class Name: Test_OrderReadManager
*
* Description: Test class for the Test_OrderReadManager class.
*
*                   
* Developer           Date              Description
* ------------------------------------------------------------------------------------                 
* Heather Kinney      3.9.2017          initial creation.
*
*************************************************************************************/

@isTest
private class Test_OrderReadManager {
    
    @isTest static void getOrderReadDetailsPositive() {
        
        User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
        system.runAs(adminUser){
            // Insert Customer Account
            Account customerAccount = new Account();
            customerAccount = CreateTestClassData.createCustomerAccount();
            
            // Insert Products
            Product2 testProduct = new Product2(Name='SNAP PRO STARTER KIT', Family='test', SAP_MATERIAL_NUMBER__c = String.valueOf(Math.random()).subString(15));
            insert testProduct;
            
            // Insert PriceBook
            Pricebook2 priceBook = CreateTestClassData.reusablePricebook2('IDEXX Standard Price Book');
            insert priceBook;
            
            // Fetch Standard PriceBookId
            Id pricebookId = Test.getStandardPricebookId();
            System.debug('PB2 Id:'+pricebookId);
            // Insert PriceBookEntry
            PricebookEntry stdpriceBookEntry = CreateTestClassData.reusablePriceBookEntry(testProduct.Id, pricebookId,1000);
            stdpriceBookEntry.UseStandardPrice = false;
            insert stdpriceBookEntry;
            
            //Create Order and associate it to Account and Standard Pricebook
            Order thisOrder = CreateTestClassData.createOrder(customerAccount,pricebookId, customerAccount.CurrencyISOCode);
            thisOrder.OrderReferenceNumber = '123456789';
            
            Test.startTest();
            OrderReadManager orderReadManager = new OrderReadManager();
System.debug('thisOrder >>>>>>>>>' + thisOrder);
            orderReadManager.sapOrderForTest = OrderTestDataFactory.createSAPOrder();
            
            SAPOrder.Partner p = new SAPOrder.Partner();
        
            p.partnerType = 'WE';
            p.customer = '0000016242';
            p.name = 'test name';
            p.name2 = 'test name2';
            p.street = '123 Main St.';
            p.country = 'US';
            p.postalCode = '04106';
            p.city = 'Portland';
            p.district = 'x';
            p.region = 'ME';
            p.transportationZone = 'z';
            
            orderReadManager.sapOrderForTest.partners.add(p); 
            
            SAPOrder.Item i2 = new SAPOrder.Item();           
    		i2.itemNumber = '000030';
    		i2.externalItemNumber = '3'; 
            i2.material = '11-11111-11';
            i2.speedCode = 'ABC';
            i2.shortText = 'test test test';
            i2.batch = '1234';
            i2.storageLocation = 'x';
            i2.requestedQuantity = 1.00;
            i2.itemCategory = 'x';
            i2.netValue = Decimal.valueOf('1.23');
            i2.currencyCode = 'USD';
            i2.uom = 'x';
            i2.deliveryDate = Date.newInstance(2017, 7, 8);
            i2.requestedQuantity = 1.00;
            i2.confirmedQuantity = 1.00;
            i2.plant = 'x';
            i2.shippingPoint = 'x';
            i2.itemCategory = 'x';
            i2.storageLocation = 'x';
    		i2.materialGroup = 'x';
            i2.totalPrice = 1.00;
            i2.netSubTotal = 1.00;
            i2.netPrice = 1.00;
            i2.shippingCharges = 1.00;
            i2.freightChanged = false;        
            i2.tax = 1.00;
            i2.discount = 1.00;
            i2.batch = 'x';
            i2.incoterms = 'x';
            i2.batchedItem = true;
            i2.batchExpirationDate = Date.newInstance(2017, 7, 8);
            i2.isDelivered = false;
            i2.freeGoods = true;
            i2.freeGoodsAgent = 'xxxx';
            i2.freeGoodsReason = 'xxxx';
            i2.freeGoodsCostCenter = 'xxxx';
            
            orderReadManager.sapOrderForTest.items.add(i2);
            
System.debug('orderReadManager.sapOrderForTest >>>>>>>>>' + orderReadManager.sapOrderForTest);            
            OrderReadManager.OrderReadManagerWrapper orderReadManagerWrapper = orderReadManager.getOrderReadDetails(thisOrder);
            
            Test.stopTest();	

        
        }
        
        		
    }

}