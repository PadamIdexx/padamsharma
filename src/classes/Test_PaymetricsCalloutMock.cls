@isTest
global class Test_PaymetricsCalloutMock implements WebServiceMock {
    public static Integer indicatorVar;
    global void doInvoke(Object stub,Object request,Map<String, Object> response,String endpoint,String soapAction,String requestName,String responseNS,String responseName,String responseType) {
        paymetricXipaysoap30Message.ITransactionHeader iHeader = new paymetricXipaysoap30Message.ITransactionHeader();
        iHeader.StatusCode=indicatorVar;
        primesysXipaysoapMessage.PreAuthorizeResponse_element sampleResponse = new primesysXipaysoapMessage.PreAuthorizeResponse_element();
        sampleResponse.PreAuthorizeResult = iHeader;
        response.put('response_x', sampleResponse ); 
        System.debug('########'+response.get('response_x'));
    }
}