public class WFM_WOLITriggerHandler {  
    
    /***** Only Used by CAG EMEA for reporting Purposes ****/   
    public static void UpdateProductOrService(WorkOrderLineItem[] WorkOrderLineItems) {       
        for (WorkOrderLineItem WOLI :WorkOrderLineItems){
            If(WOLI.EMEA_Country__c == TRUE && WOLI.RecordTypeId == WFM_Constants.WORK_ORDER_LINE_ITEM_INSTALL_RECORDTYPE_ID ){     
                /* Update from Custom Product Field . We need Product Name as in Apex for some reason Name is not accessible, so have to create a formula Field */
                WOLI.Product_OR_Product_and_Service__c= WOLI.Product_Name__c;
            } else if(WOLI.EMEA_Country__c == TRUE && WOLI.RecordTypeId == WFM_Constants.WORK_ORDER_LINE_ITEM_NON_INSTALL_RECORDTYPE_ID){
                /* Update from MultiSelect PickList Field .*/
                WOLI.Product_OR_Product_and_Service__c= WOLI.Products_and_Services_EMEA__c;
            }
        }    
    }
}