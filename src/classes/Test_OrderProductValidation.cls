@isTest
public with Sharing class Test_OrderProductValidation {
    private static testmethod void testProductValidation(){
        // Insert Customer Account
            Account customerAccount = new Account();
            customerAccount = CreateTestClassData.createCustomerAccount();
                        
            // Insert Products
            Product2 testProduct = new Product2(Name='SNAP PRO STARTER KIT', Family='test', SAP_MATERIAL_NUMBER__c = '11235');
            insert testProduct;
            
            // Insert PriceBook
            Pricebook2 priceBook = CreateTestClassData.reusablePricebook2('IDEXX Standard Price Book');
            insert priceBook;
            
            // Fetch Standard PriceBookId
            Id pricebookId = Test.getStandardPricebookId();
            System.debug('PB2 Id:'+pricebookId);
            // Insert PriceBookEntry
            PricebookEntry stdpriceBookEntry = CreateTestClassData.reusablePriceBookEntry(testProduct.Id, pricebookId,1000);
            stdpriceBookEntry.UseStandardPrice = false;
            insert stdpriceBookEntry;
                        
            //Create Order and associate it to Account and Standard Pricebook
            Order thisOrder = CreateTestClassData.createOrder(customerAccount,pricebookId, customerAccount.CurrencyISOCode);
            
            List<Product2> prodLst = New List<Product2>(); 
            Product2 prod = new Product2(Name = 'Laptop X200', 
                                         Family = 'Hardware',SAP_MATERIAL_NUMBER__c='124234');
            prodLst.add(prod); 
            if(!prodLst.isEmpty()) {
                insert prodLst; 
            }
        
            // 1. Insert a price book entry or the standard price book.
            // Standard price book entries require the standard price book ID we got earlier.
            List<PricebookEntry> pricebookentryLst = new List<PricebookEntry>(); 
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, Product2Id = prod.Id,
                UnitPrice = 10000, IsActive = true,currencyIsoCode = 'USD', PriceBook_SAPMatNr__c = '3');
            pricebookentryLst .add(standardprice); 
            if(!pricebookentryLst.isEmpty()) {
                insert pricebookentryLst;
            }
            
            OrderItem ordItm = new OrderItem(); 
            ordItm.OrderId = thisOrder.Id; 
            ordItm.PricebookEntryId = standardPrice.Id; 
            ordItm.Quantity = 12; 
            ordItm.UnitPrice = 20;
            insert ordItm; 
            OrderProductValidationController ordP = new OrderProductValidationController(new ApexPages.StandardController(ordItm));
            ordP.backToOriginal();
            System.assertEquals(thisOrder.Id, ordItm.OrderId);
    }
}