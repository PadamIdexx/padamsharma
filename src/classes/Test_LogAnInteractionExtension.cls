@isTest
public with sharing class Test_LogAnInteractionExtension{
   /* public static testMethod void Test_LogAnInteractionExtension() {
        
        User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
        system.runAs(currentUser){
            
            Account acc = CreateTestClassData.createCustomerAccount();
            
            Contact cont = new Contact();
            cont.LastName = 'testContact1';
            cont.AccountId = acc.Id;
            insert cont;
            
            Contact cont2 = new Contact();
            cont2.LastName = 'testContact1';
            cont2.AccountId = acc.Id;
            insert cont2;
            
            acc.Primary_Contact__c = cont.Id;
            update acc;
            
            Campaign camp = CreateTestClassData.reusableCampaign();
            insert camp ;
            
            CampaignMember cm = CreateTestClassData.reusableCampaignMember(camp.Id,cont.Id);
            insert cm; 
            
            Task tsk = CreateTestClassData.reusableTask();
            
            tsk.RecordTypeId =[Select Id,SobjectType,Name From RecordType WHERE Name ='Water Task' and SobjectType ='Task'].Id;
            tsk.OwnerId = currentUser.Id;
            tsk.WhatId = camp.Id;
            tsk.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Log An Interaction').getRecordTypeId();
            tsk.selectedcampaign__c = cm.id;
            insert tsk;
            
            //Call the VF page
            Test.startTest();
            ApexPages.StandardController std = new ApexPages.StandardController(tsk);
            ApexPages.currentPage().getParameters().put('what_id',acc.Id);
            LogAnInteractionExtension LogCall = new LogAnInteractionExtension(std);
            LogCall.isWater = false;
            
            Test.stopTest();
            
        }  
    } */
    
     public static testMethod void Test_LogAnInteractionExtension_Save_Positive() {
        
        // Create common test accounts
        User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
        system.runAs(currentUser){
            
            Account acc = CreateTestClassData.createCustomerAccount();
            
            Contact cont = new Contact();
            cont.LastName = 'testContact1';
            cont.AccountId = acc.Id;
            insert cont;
            system.debug('Count---'+cont);
            /*Contact cont2 = new Contact();
            cont2.LastName = 'testContact1';
            cont2.AccountId = acc.Id;
            insert cont2;
            system.debug('Count---'+cont2);*/
            acc.Primary_Contact__c = cont.Id;
            update acc;
            
            Campaign camp = CreateTestClassData.reusableCampaign();
            insert camp;
            
            CampaignMemberStatus cms = new CampaignMemberStatus();
            cms.CampaignId = camp.Id;
            cms.IsDefault = false;
            cms.Label = 'Participated';
            cms.SortOrder = 3;
            insert cms;
            
            CampaignMember cm = CreateTestClassData.reusableCampaignMember(camp.Id,cont.Id);
            insert cm; 
            
            //CampaignMember cm2 = CreateTestClassData.reusableCampaignMember(camp.Id,cont2.Id);
            //insert cm2;
            
            Task tsk = CreateTestClassData.reusableTask();
            //tsk.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Inbound_Call').getRecordTypeId();
            tsk.OwnerId = currentUser.Id;
            tsk.WhatId = camp.Id;
            tsk.selectedcampaign__c = cm.id;
            tsk.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Log An Interaction').getRecordTypeId();
            System.debug('@@@ Check for Task Id' + tsk);
            insert tsk;
            
            Test.startTest();
            ApexPages.StandardController std = new ApexPages.StandardController(tsk);
            ApexPages.currentPage().getParameters().put('what_id',acc.Id);
            LogAnInteractionExtension LogCall = new LogAnInteractionExtension(std);
            LogCall.CampaignWrapperList[0].selectCheck = true ;
            LogCall.CampaignWrapperList[0].CampStatus = 'Sent';
            LogCall.save();
            LogCall.cancel();
            LogCall.saveNew();
            system.assertEquals(LogCall.CampaignWrapperList[0].CampStatus,'Sent');
            Test.stopTest();
            
        }  
    }
    
}