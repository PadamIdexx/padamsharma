public with sharing class OrderSimulateService {

        public String endpoint_x = '';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x = 120000;

		//Namespace information for service all.
        private String[] ns_map_type_info = new String[]{OrderSimulateRequest.NAMESPACE, 
            								OrderSimulateRequest.SF_NAMESPACE, 
            								OrderSimulateRequest.XMLTYPE_NAMESPACE, 
            								OrderSimulateRequest.SF_XMLTYPE_NAMESPACE};
                                                
        public OrderSimulateService(){
            //Brute force approach to getting runtime classname to use for looking up the endpoint
            String thisName = String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));            
            String endPointURL = '';
            if (!Test.isRunningTest()) {
                endPointURL = WebServiceHelper.getServiceEndpoints(thisName);   
            }
            endpoint_x = endPointURL!=null ? endPointURL : '';
           
            //set headers
            inputHttpHeaders_x = new Map<String, String>();
            
            String sessionId = '';
            if (!Test.isRunningTest()) {
               sessionId = WebServiceHelper.getIntegrationSessionId();
            }
            inputHttpHeaders_x.put('sessionId', sessionId);
            
        }

    
        
        public OrderSimulateResponse.SalesOrderSimulateResponse simulateOrder(OrderSimulateRequest.SalesOrderSimulateRequest simulateRequest) {
            OrderSimulateRequest.SalesOrderSimulateRequest request_x = new OrderSimulateRequest.SalesOrderSimulateRequest();
            request_x= simulateRequest;
            OrderSimulateResponse.SalesOrderSimulateResponse response_x;
            Map<String, OrderSimulateResponse.SalesOrderSimulateResponse> response_map_x = new Map<String, OrderSimulateResponse.SalesOrderSimulateResponse>();
            response_map_x.put('response_x', response_x); 
            if (!Test.isRunningTest()) {
                WebServiceCallout.invoke(
                  this,
                  request_x,
                  response_map_x,
                  new String[]{endpoint_x,
                        OrderSimulateRequest.SOAPACTION,
                        OrderSimulateRequest.NAMESPACE,
                        OrderSimulateRequest.PAYLOAD_CLASSNAME,
                        OrderSimulateResponse.NAMESPACE,
                        OrderSimulateResponse.PAYLOAD_CLASSNAME,
                        OrderSimulateResponse.SalesOrderSimulateResponse.class.getName()}
                );
            }
            response_x = response_map_x.get('response_x');
            return response_x;
        }

}