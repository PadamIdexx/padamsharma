public with sharing class OrderManager {

    /*
     * SAP Change Order operation.  This operation delegates to ChangeWrapper for processing.          
     */ 
    public static SAPOrder changeOrder(SAPOrder order) {
        try {                       
            ChangeWrapper w = new ChangeWrapper();
            
            SAPOrder updatedOrder = w.execute(order);
       
            updatedOrder = processStatus(updatedOrder);        
            
        	return updatedOrder;
        } catch (Exception e) {
        	System.debug('Error occured processing change request: ' + e);
            System.debug('Message: ' + e.getMessage());
            
            StatusEmailHandler.sendException(e);
            
            //rethrow error so it can be handled by caller
            throw e;
        }
        
        return null;
        
    }  
    
    public static SAPOrder getOrder(SAPOrder order) {
        
        try {                       
            ReadWrapper w = new ReadWrapper();
            
            SAPOrder updatedOrder = w.execute(order);
                  
            //read operation does not contain a returns object
            
        	return updatedOrder;
        } catch (Exception e) {
        	System.debug('Error occured processing retrieving order: ' + e);
            System.debug('Message: ' + e.getMessage());

            StatusEmailHandler.sendException(e);
            
            //rethrow error so it can be handled by caller
            throw e;
        }
        
        return null;
    }

    public static SAPOrder deleteOrder(SAPOrder order) {

		try {                       
            DeleteWrapper w = new DeleteWrapper();
            
            SAPOrder deletedOrder = w.execute(order);
            
            deletedOrder = processStatus(deletedOrder);        
            
        	return deletedOrder;
        } catch (Exception e) {
        	System.debug('Error occurred deleting order: ' + e);
            System.debug('Message: ' + e.getMessage());

            StatusEmailHandler.sendException(e);
            
            //rethrow error so it can be handled by caller
            throw e;
        }
       
        return null;
    }    
    
    public static SAPOrder simulateOrder(SAPOrder order) {

       try {                       
            SimulateWrapper w = new SimulateWrapper();
            
            SAPOrder simulatedOrder = w.execute(order);

            simulatedOrder = processStatus(simulatedOrder);
           
        	return simulatedOrder;
        } catch (Exception e) {
        	System.debug('Error occurred simulating order: ' + e);
            System.debug('Message: ' + e.getMessage());

            StatusEmailHandler.sendException(e);
            
            //rethrow error so it can be handled by caller
            throw e;
        }
       
        return null;
    }
    
    public static SAPOrder createOrder(SAPOrder order) {

       try {                       
            CreateWrapper w = new CreateWrapper();
            
            SAPOrder createdOrder = w.execute(order);
        
           	createdOrder = processStatus(createdOrder);
           
        	return createdOrder;
        } catch (Exception e) {
        	System.debug('Error occurred creating order: ' + e);
            System.debug('Message: ' + e.getMessage());

            StatusEmailHandler.sendException(e);
            
            //rethrow error so it can be handled by caller
            throw e;
        }
       
        return null;
    }
    

    /*
     * Common status processor.  This is a helper that interogates the returns object
     * from an SAPOrder into a set of top level flags for determining things about
     * the order 
     * 
     * Does this really belong here or should it live somewhere else?
     */
    private static SAPOrder processStatus(SAPOrder order) {
        for (SAPOrder.Return_x r : order.returns) {
            
            System.debug(r);
            
            if (String.isNotBlank(r.MessageType)
                && r.MessageType.equalsIgnoreCase('E')) {
                    order.hasErrors = true;
            }
            
            if (String.isNotBlank(r.MessageType)
                && r.MessageType.equalsIgnoreCase('W')) {
                    order.hasWarnings = true;
            }       
            
            if (String.isNotBlank(r.MessageType)
                && r.MessageType.equalsIgnoreCase('W')
                && String.isNotBlank(r.MessageID)
                && r.MessageID.equalsIgnoreCase('ZPOC')) {
                    order.hasCreditLimitBlock = true;
            }
            
            if (String.isNotBlank(r.MessageType)
                && r.MessageType.equalsIgnoreCase('I')
                && String.isNotBlank(r.Message)
                && r.Message.equalsIgnoreCase('Short dated batch')) {
                    order.hasShortDatedBatches = true;         
            }
        }
        
        return order;
    }
    
}