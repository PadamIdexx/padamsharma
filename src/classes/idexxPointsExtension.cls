public with sharing class idexxPointsExtension {
    
     public Id accId;
     public String sapCustNo {get;set;}
     public List<Account> acc;
     public String response {get;set;}
     public String loc {get;set;}
     public String endpointURL {get;set;}

    public idexxPointsExtension(ApexPages.StandardController controller) {
     
    }
    
    public void fetchData() {
    
        acc = new List<Account>();
        accId =ApexPages.currentPage().getParameters().get('id');
        acc = [SELECT Id, SAP_Number_without_Leading_Zeros__c FROM Account WHERE ID =:accId];
        if(!acc.isEmpty()){
            for(Account a : acc){
             sapCustNo = a.SAP_Number_without_Leading_Zeros__c;
            }
        }
       getAndParse( sapCustNo);
        
    }
    
    
    public void getAndParse(String sapCustNo) {
        String setbody = 'Username=beaconpoints&Password=beacon123&sapId=' + sapCustNo;
        System.debug('@@Set'+ setbody);
        if(!Test.isRunningTest()){
        endpointURL = WebServiceUtil.retreiveEndpoints(WebServiceUtil.WEB_IDEXX_POINTS);
        }
        System.debug('endpoint' + endpointURL );
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        // do your initial http call here
        req.setMethod('POST');
        req.setBody(setbody);
        req.setEndpoint(endpointURL);
        res = http.send(req);
        system.debug('>>>>>>>>>>>>>>>>> res  ' + res);
        system.debug('>>>>>>>>>>>>>>>>> res.body ' + res.getBody());
         system.debug('>>>>>>>>>>>>>>>>> res.body ' + res.getStatusCode());
        // redirection checking
        boolean redirect = false;
        if(res.getStatusCode() >=300 && res.getStatusCode() <= 307 && res.getStatusCode() != 306) {
            do {
                system.debug('>>>>>>>>>>>>>>   ' + res.getHeader('Location'));
                redirect = false;
                  loc = res.getHeader('Location');
                 // reset the value each time
               // get location of the redirect
                System.debug('@@Resulting loc' + loc);
                if(loc == null) {
                    redirect = false;
                    continue;
                }
             /*   req = new HttpRequest();
                req.setEndpoint(loc);
                req.setMethod('GET');
                res = http.send(req);
                if(res.getStatusCode() != 500) { // 500 = fail
                    if(res.getStatusCode() >=300 && res.getStatusCode() <= 307 && res.getStatusCode() != 306) {
                        redirect= true;
                    }
                    // I do special handling here with cookies
                    // if you need to bring a session cookie over to the
                    // redirected page, this is the place to grab that info
                } */
            } while (redirect && Limits.getCallouts() != Limits.getLimitCallouts());
        }
        //congratulations you're outside of the redirects now
        //read what you need from the res object
        system.debug(res.getBody());
         
       this.Response =res.getBody();
     
    }
  
}