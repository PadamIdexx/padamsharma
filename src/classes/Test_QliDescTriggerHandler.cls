@isTest

public with sharing class Test_QliDescTriggerHandler {

  static List<Product2> Products = new List<Product2>();

  static {

    // insert some Products
    Product2 r1   = new Product2(name='Descp 1',SAP_MATERIAL_NUMBER__c='1');
    Product2 r2 = new Product2(name='Descp 2',SAP_MATERIAL_NUMBER__c='2');
    Product2 r3 = new Product2(name='Descp 3',SAP_MATERIAL_NUMBER__c='3');
    Product2 r4 = new Product2(name='Descp 4',SAP_MATERIAL_NUMBER__c='4');
    Products.add(r1);
    Products.add(r2);
    Products.add(r3);
    Products.add(r4);
    insert Products;

  }

  private static testMethod void testInsertRecords()  {
  
           // Prepare data for Quote and Quote Line Item
  
           // Insert Oppty from CreateTestClassData
           Opportunity testOpty = CreateTestClassData.createLPDOpportunity();
           
           // Create PriceBook and Extension
            Pricebook2 pb = new Pricebook2(Name='Custom Pricebook Test_QLIDescTriggerHandler', isActive=true);
            if(pb!=null){
            if(Schema.sObjectType.Pricebook2.isCreateable()){
            insert pb;
            }
            }
            System.assert(pb.id!=null);
            
           
            Id pricebookId = Test.getStandardPricebookId();

            //Add new Product
            Product2 newProduct = new Product2(Name = 'Test ProducTest_QLIDescTriggerHandler',CurrencyIsoCode = 'USD',Quantity_Per_Package__c = '20.00',
                                               SAP_MATERIAL_NUMBER__c = '9991', isActive=true  );
            if(Schema.sObjectType.Product2.isCreateable()){
            if(newProduct!=null){
            insert newProduct;
            }
            }
             System.assert(newProduct.id!=null);

                PricebookEntry stdpbe = new PricebookEntry(UseStandardPrice = false,Pricebook2Id=pricebookId,Product2Id=newProduct.id,
                                                            IsActive=true, UnitPrice=99,PriceBook_SAPMatNr__c = 'test' );
                if(Schema.sObjectType.PricebookEntry.isCreateable()){                                            
                if(stdpbe!=null){                                            
                insert stdpbe;
                }
                }
                System.assert(stdpbe.id!=null);
           
          
            // pricebook entry
            PricebookEntry pbe = new PricebookEntry();
            pbe.UseStandardPrice = false;
            pbe.Pricebook2Id=pb.id;
            pbe.Product2Id=newProduct.id;
            pbe.IsActive=true;
            pbe.UnitPrice=99;
            pbe.PriceBook_SAPMatNr__c = 'test2';
             if(Schema.sObjectType.PricebookEntry.isCreateable()){                                            
            if(pbe!=null){
            insert pbe;
            }
            }
            
            
            

    List<Quote> Quotes = new List<Quote>();
    List<QuoteLineItem> QuoteLineItems = new List<QuoteLineItem>();

    // insert some quotes
    Quote a1 = new Quote(name='Quote 1',OpportunityID=testOpty.ID,Pricebook2Id = pb.id);
    Quote a2 = new Quote(name='Quote 2',OpportunityID=testOpty.ID,Pricebook2Id = pb.id);
    Quotes.add(a1);
    Quotes.add(a2);
    insert Quotes;

    Test.startTest();

      QuoteLineItems.add(new QuoteLineItem(QuoteID=a1.Id, Product2ID=Products.get(0).Id,Unitprice=100.00,Quantity=2.00,PricebookEntryId=pbe.id));
      QuoteLineItems.add(new QuoteLineItem(QuoteID=a1.Id, Product2ID=Products.get(1).Id,Unitprice=100.00,Quantity=2.00,PricebookEntryId=pbe.id));
      QuoteLineItems.add(new QuoteLineItem(QuoteID=a2.Id, Product2ID=Products.get(2).Id,Unitprice=100.00,Quantity=2.00,PricebookEntryId=pbe.id));
      QuoteLineItems.add(new QuoteLineItem(QuoteID=a2.Id, Product2ID=Products.get(3).Id,Unitprice=100.00,Quantity=2.00,PricebookEntryId=pbe.id));

      insert QuoteLineItems;

    Test.stopTest();

    // since async, check for the accounts AFTER tests stop
    List<Quote> updatedAccounts = [select id, name, Description from Quote where id IN :Quotes];
  //  System.assertEquals('Descp 1, Descp 2',updatedAccounts.get(0).description);
  //  System.assertEquals('Descp 3, Descp 4',updatedAccounts.get(1).description);

  }
  
 // prepare data for Delete
  private static testMethod void testDeleteRecords() {
    Opportunity testOpty = CreateTestClassData.createLPDOpportunity();

            Pricebook2 pb = new Pricebook2(Name='Custom Pricebook Test_QLIDescTriggerHandler', isActive=true);
            if(pb!=null){
            if(Schema.sObjectType.Pricebook2.isCreateable()){
            insert pb;
            }
            }
            System.assert(pb.id!=null);
            
           
            Id pricebookId = Test.getStandardPricebookId();

             //Product
            Product2 newProduct = new Product2(Name = 'Test ProducTest_QLIDescTriggerHandler',CurrencyIsoCode = 'USD',Quantity_Per_Package__c = '20.00',
                                               SAP_MATERIAL_NUMBER__c = '9991', isActive=true  );
            if(Schema.sObjectType.Product2.isCreateable()){
            if(newProduct!=null){
            insert newProduct;
            }
            }
             System.assert(newProduct.id!=null);

                PricebookEntry stdpbe = new PricebookEntry(UseStandardPrice = false,Pricebook2Id=pricebookId,Product2Id=newProduct.id,
                                                            IsActive=true, UnitPrice=99,PriceBook_SAPMatNr__c = 'test' );
                if(Schema.sObjectType.PricebookEntry.isCreateable()){                                            
                if(stdpbe!=null){                                            
                insert stdpbe;
                }
                }
                System.assert(stdpbe.id!=null);
           
            // pricebook entry
            PricebookEntry pbe = new PricebookEntry();
            pbe.UseStandardPrice = false;
            pbe.Pricebook2Id=pb.id;
            pbe.Product2Id=newProduct.id;
            pbe.IsActive=true;
            pbe.UnitPrice=99;
            pbe.PriceBook_SAPMatNr__c = 'test2';
             if(Schema.sObjectType.PricebookEntry.isCreateable()){                                            
            if(pbe!=null){
            insert pbe;
            }
            }
    List<quote> quotes = new List<quote>();
    List<quotelineitem> quotelineitems = new List<quotelineitem>();

    // insert an Quote
    quote a1 = new quote(name='quote 1',OpportunityID=testOpty.ID,Pricebook2Id = pb.id);
    quotes.add(a1);
    insert quotes;

    Test.startTest();

      Quotelineitems.add(new Quotelineitem(QuoteID=a1.Id, Product2ID=Products.get(0).Id,Unitprice=100.00,Quantity=2.00,PricebookEntryId=pbe.id)); 
      Quotelineitems.add(new Quotelineitem(QuoteID=a1.Id, Product2ID=Products.get(1).Id,Unitprice=100.00,Quantity=2.00,PricebookEntryId=pbe.id));
      Quotelineitems.add(new Quotelineitem(QuoteID=a1.Id, Product2ID=Products.get(2).Id,Unitprice=100.00,Quantity=2.00,PricebookEntryId=pbe.id));
      Quotelineitems.add(new Quotelineitem(QuoteID=a1.Id, Product2ID=Products.get(3).Id,Unitprice=100.00,Quantity=2.00,PricebookEntryId=pbe.id));

      insert Quotelineitems;

     //  now delete a record
      delete Quotelineitems.get(3);
    Test.stopTest();

    List<quote> updatedquotes = [select id, name, description from quote where id IN :quotes];
 //   System.assertEquals('Descp 1,Descp 2,Descp 3,Descp 4',updatedquotes.get(0).description);
    SYSTEM.debug(updatedquotes.size());
    

  }

}