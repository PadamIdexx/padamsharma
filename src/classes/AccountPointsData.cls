public with sharing class AccountPointsData {
    public Decimal availableBalance {get;set;}
    public String expirationDate{get;set;}
    public Decimal expiringBalance {get;set;}
    public Double pointsToLease {get;set;}
    public List<String> expiryDateSet {get;set;}
    public Map<String,BANK_EXP__x> expiryDateMap = new Map<String,BANK_EXP__x>();
    String sapCustomerNumber;
    public List<PARENT_CHILD__x> parentRecord;
    public List<BANK_EXP__x> expirydate;
    
    public AccountPointsData(String custNo){
        availableBalance = 0;
        expirationDate = '';
        expiringBalance = 0;
       pointsToLease = 0;
        sapCustomerNumber = custNo;
        expiryDateSet = new List<String>();
         parentRecord = new List<PARENT_CHILD__x>();
         expirydate = new List<BANK_EXP__x>();
    }
    
    public AccountPointsData(ApexPages.StandardController std){
        availableBalance = 0;
        expirationDate = '';
        expiringBalance = 0;
       pointsToLease = 0;
        expiryDateSet = new List<String>();
         parentRecord = new List<PARENT_CHILD__x>();
       PointsToLeaseService svc = new PointsToLeaseService();
       List<Account> thisAccount = [Select SAP_Customer_Number__c FROM Account WHERE Id =:std.getId()];
       sapCustomerNumber = '';
       
       if(thisAccount[0].SAP_Customer_Number__c != NULL)
        sapCustomerNumber = thisAccount[0].SAP_Customer_Number__c;
    }
    
    public void fetchAccountPoints(){ 
        availableBalance = 0;
        expirationDate = '';
        expiringBalance =0;
        pointsToLease = 0;
        expiryDateSet = new List<String>();
        
        if(sapCustomerNumber != null && sapCustomerNumber != ''){
        
        //For Points-To-Lease Service
        
            PointsToLeaseService svc = new PointsToLeaseService();
            List<PointsToLeaseService.PointsToLease> l = svc.getPointsToLeaseForCustomer(sapCustomerNumber);
            Iterator<PointsToLeaseService.PointsToLease> i = l.iterator();
            
            while (i.hasNext()) {
             PointsToLeaseService.PointsToLease lease = (PointsToLeaseService.PointsToLease)i.next();
             pointsToLease = pointsToLease + Double.valueOf(lease.points);
            }
            
             String currentYear = String.valueOf(Date.Today().Year());
             String expiryDateString = currentYear + '1201';
               
            // check if this is a parent account. If yes, no calculations needed.
            
            if(!Test.isRunningTest())
            parentRecord = [select ParentCustomer__c FROM PARENT_CHILD__x 
                            WHERE ChildCustomer__c = :sapCustomerNumber AND ChildExclusion__c != 'X' AND ProgActiveFlag__c = 'Y'];
            System.debug('@@Parent Rec'+parentRecord );
            // this is a child record, add points from parent
            if(!parentRecord.isEmpty()){
            
            // has a parent where points don't have to be excluded
            String parentCustomerId = parentRecord[0].ParentCustomer__c;
            
            
            if(!Test.isRunningTest())
            expirydate  = [SELECT AvailBalance__c,Customer__c, ExpiredBal__c, ExpiryDate__c from BANK_EXP__x where Customer__c =:parentCustomerId ]; //and expirydate__c =:currentYear
            
            if(!expirydate.isEmpty()){
                 for(BANK_EXP__x be : expirydate) {
                        expiryDateSet.add(String.valueOf(be.ExpiryDate__c)+':'+String.valueOf(be.AvailBalance__c));  
                        expiryDateMap.put(String.valueOf(be.ExpiryDate__c)+':'+String.valueOf(be.AvailBalance__c) , be);
                    }
                    
                   
                    expiryDateSet.sort();
                    
                 for(BANK_EXP__x be : expirydate) {
                     if (( String.valueOf(be.AvailBalance__c) != '0' &&   Double.valueOf(be.AvailBalance__c) > 0 )
                        	&& (String.valueOf(be.ExpiryDate__c) == expiryDateString)) {
                         System.debug('came here:'+expirationDate);
                         String expYear = String.valueOf(be.ExpiryDate__c).left(4);
                         expirationDate = '11/30/' +expYear;
                         expiringBalance = be.AvailBalance__c;
                         break;
                     }
                 }
                 
                if (expirationDate == '') {
                    expirationDate = '11/30/' + currentYear;
                }
               
            }
            
              
                
                // available balance would be balance on parent account + balance on this account
                for(BANK_EXP__x points : [select AvailBalance__c from BANK_EXP__x where Customer__c = :parentCustomerId]){
                    if(points.AvailBalance__c != NULL)
                        availableBalance += points.AvailBalance__c; 
                }               
            }
            else{
             if(!Test.isRunningTest())
                expirydate = [SELECT AvailBalance__c,Customer__c, ExpiredBal__c, ExpiryDate__c from BANK_EXP__x where Customer__c =:sapCustomerNumber ]; //and expirydate__c =:currentYear
              if(!expirydate.isEmpty()){
                  for(BANK_EXP__x be : expirydate) {
                        expiryDateSet.add(String.valueOf(be.ExpiryDate__c));                        
                    }
                    
                    expiryDateSet.sort();
                    for(BANK_EXP__x be : expirydate) {
                     if (( String.valueOf(be.AvailBalance__c) != '0' &&   Double.valueOf(be.AvailBalance__c) > 0 )
                        	&& (String.valueOf(be.ExpiryDate__c) == expiryDateString)) {
                         System.debug('came here:'+expirationDate);
                         String expYear = String.valueOf(be.ExpiryDate__c).left(4);
                         expirationDate = '11/30/' +expYear;
                         expiringBalance = be.AvailBalance__c;
                         break;
                         
                     }
                 }
                                                  
                 if (expirationDate == '') {
                    expirationDate = '11/30/' + currentYear;
                }
            }
                // has no parent, itself is a parent. Simply show this Account's points on the screen
                List<BANK_EXP__x> points = new List<BANK_EXP__x>();
                points = [select AvailBalance__c from BANK_EXP__x where Customer__c = :sapCustomerNumber];
                if(!points.isEmpty() && points[0].AvailBalance__c != NULL){
                    availableBalance = points[0].AvailBalance__c;
                }
            }
              
        }
    } 
}