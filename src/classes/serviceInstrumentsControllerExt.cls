public class serviceInstrumentsControllerExt {
    
    private final Asset asst;
    public String custId{get;set;}
    private List<Asset> assets{get;set;}
    public String errorMsg{get;set;}
    public List<Map<String,String>> equip{get;set;}
    public List<Map<String,String>> details{get;set;}
    public List<Map<String,String>> notes{get;set;}
    public Set<String> eqIds{get;set;}

    
    public serviceInstrumentsControllerExt(ApexPages.StandardController stdController){
        this.asst = (Asset)stdController.getRecord();
        
    }
    
    public PageReference fetchEvents(){
        
        Account acc = new Account();
        try{
            String sapId = ApexPages.currentPage().getParameters().get('sapId');
            if(sapId != null)custId = sapId;
            
        }
        catch(exception e){
            errorMsg = 'No SAP id for this Account';
        }
        equip = new List<Map<String, String>>();
        details = new List<Map<String, String>>();
        notes = new List<Map<String, String>>();
        eqIds = new Set<String>();
        try{
            serviceInstrumentCallout.getEvents(custId);
            if(serviceInstrumentCallout.eqIds != null)eqIds.addAll(serviceInstrumentCallout.eqIds);
            if(serviceInstrumentCallout.notes != null)notes.addAll(serviceInstrumentCallout.notes);
            if(serviceInstrumentCallout.details != null)details.addAll(serviceInstrumentCallout.details);
            if(serviceInstrumentCallout.equip != null)equip.addAll(serviceInstrumentCallout.equip);
            
            
            return null;
        }
        catch(exception e){
            errorMsg = e.getMessage() +  ' ' + e.getStackTraceString() + ' ' + e.getLineNumber();
            return null;
            
        }
        
        
    }
    
    
}