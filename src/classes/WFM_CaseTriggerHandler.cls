public class WFM_CaseTriggerHandler {
    
  public void onAfter(List<Case> caseList) {
        createCaseOnboardingWorkOrders(caseList); 
    }
    
    public void createCaseOnboardingWorkOrders(List<Case> listCases){
        WFM_Utils.createOnboardingWorkOrders(listCases);
    }
}