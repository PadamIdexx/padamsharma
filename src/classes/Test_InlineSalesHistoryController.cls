/* Class Name   : Test_InlineSalesHistoryController
 * Description  : Test Class with unit test scenarios to cover the InlineSalesHistoryViewController class
 * Created By   : Sirisha Kodi
 * Created On   : 10-07-2015
 
* Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date                   Modification ID       Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Sirisha Kodi             10-22-2015             1000                   Initial version
                                                                                
*/
@isTest(seeAllData=false)
public with sharing class Test_InlineSalesHistoryController {
    

   
    static testMethod void ViewSalesHistoryPositiveTestCase() {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
        system.runAs(currentUser){
            // test data creation - test accounts 
              Account testAccount = CreateTestClassData.createCustomerAccount();
              Contact testContact = CreateTestClassData.reusableContact(testAccount.Id);
              insert testContact;
              testAccount.Primary_Contact__c = testContact.Id;
              testAccount.SAP_CUSTOMER_NUMBER__c = '0000001003';
              update testAccount;
              
                Test.startTest();
                //set the standard Controller to the test Account created 
                ApexPages.StandardController std = new ApexPages.StandardController(testAccount);
                InlineSalesHistoryController controllerInstance = new InlineSalesHistoryController(std);
                MV_CUSTOMER_SALES_HISTORY__x testc = new MV_CUSTOMER_SALES_HISTORY__x(DisplayUrl= 'TEST',ExternalId='TEST',SAP_CUSTOMER_NUMBER__c = '0000001003', PRODUCT_LINE__c  = 'LINEA', PRODUCT_NAME__c ='PRD1', PRODUCT_TYPE__c = 'TYPEA');
                MV_CUSTOMER_SALES_HISTORY__x testc2 = new MV_CUSTOMER_SALES_HISTORY__x(DisplayUrl= 'TEST2',ExternalId='TEST2',SAP_CUSTOMER_NUMBER__c = '0000001003', PRODUCT_LINE__c  = 'LINEB', PRODUCT_NAME__c ='PRD2', PRODUCT_TYPE__c = 'TYPEB');
                MV_CUSTOMER_SALES_HISTORY__x testc3 = new MV_CUSTOMER_SALES_HISTORY__x(DisplayUrl= 'TEST3',ExternalId='TEST3',SAP_CUSTOMER_NUMBER__c = '0000001003', PRODUCT_LINE__c  = 'LINEB', PRODUCT_NAME__c ='PRD2', PRODUCT_TYPE__c = 'TYPEC');
                MV_CUSTOMER_SALES_HISTORY__x testc4 = new MV_CUSTOMER_SALES_HISTORY__x(DisplayUrl= 'TEST4',ExternalId='TEST4',SAP_CUSTOMER_NUMBER__c = '0000001003', PRODUCT_LINE__c  = 'LINEB', PRODUCT_NAME__c ='PRD3', PRODUCT_TYPE__c = 'LPD TOTAL');
                MV_CUSTOMER_SALES_HISTORY__x testc5 = new MV_CUSTOMER_SALES_HISTORY__x(ExternalId ='0000066423-LPD TESTS-POULTRY TESTS-REO-N/A-USD-EUR-EUR', SAP_CUSTOMER_NUMBER__c ='0000066423',PRODUCT_TYPE__c ='LPD TOTAL',PRODUCT_LINE__c ='RUMINANT TESTS' , PRODUCT_FAMILY__c ='BVDV' , PRODUCT_NAME__c='N/A' , REV3_CURR_QUARTER__c =696.87,REV3_DIFF_QUARTER__c =8585.89,
                REV3_CURR_YEAR__c = 65738.00, REV3_CURR_QUARTER_YAGO__c = 84748.72, REV3_YOY_YEAR__c =734740.73 , HA_QTY_DIFF_QUARTER__c = 749473.00, REV3_DIFF_YEAR__c = 88493.849 , REV3_CURR_YEAR_YAGO__c = 1879.38, REV3_YOY_QUARTER__c= 849484.849);
                //controllerInstance.currentAccountRecord.SAP_CUSTOMER_NUMBER__c = '0000001003';
                //controllerInstance.GroupProducts();
                //controllerInstance.ProductTypeSet.add('TYPEA;LINEA');
                //controllerInstance.GroupProducts();
                controllerInstance.rowSpanProductTypeMap.put('TYPEA',3);
                controllerInstance.ProductTypeSet.add('TYPEB');
                controllerInstance.GroupProducts();
                controllerInstance.ProductTypeLineSet.add('TYPEA;LINEA');
                controllerInstance.GroupProducts();
                controllerInstance.externalSalesHistoryList.add(testc);
                controllerInstance.externalSalesHistoryList.add(testc2);
                controllerInstance.externalSalesHistoryList.add(testc3);
                controllerInstance.externalSalesHistoryList.add(testc5);
                controllerInstance.externalSalesHistoryList.add(testc4);
                controllerInstance.GroupProducts();
            Test.stopTest();
        }
    }
    static testMethod void ViewSalesHistoryNegativeTestCase() {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
        system.runAs(currentUser){
            // test data creation - test accounts 
              Account testAccount = CreateTestClassData.createCustomerAccount();
              Contact testContact = CreateTestClassData.reusableContact(testAccount.Id);
              insert testContact;
              testAccount.Primary_Contact__c = testContact.Id;
              testAccount.SAP_CUSTOMER_NUMBER__c = '0000001003';
              update testAccount;
              
                Test.startTest();
                //set the standard Controller to the test Account created 
                ApexPages.StandardController std = new ApexPages.StandardController(testAccount);
                InlineSalesHistoryController controllerInstance = new InlineSalesHistoryController(std);
                MV_CUSTOMER_SALES_HISTORY__x testc = new MV_CUSTOMER_SALES_HISTORY__x(DisplayUrl= 'TEST',ExternalId='TEST',SAP_CUSTOMER_NUMBER__c = '0000001003', PRODUCT_LINE__c  = 'LINEA', PRODUCT_NAME__c ='PRD1', PRODUCT_TYPE__c = 'TYPEA');
                MV_CUSTOMER_SALES_HISTORY__x testc2 = new MV_CUSTOMER_SALES_HISTORY__x(DisplayUrl= 'TEST2',ExternalId='TEST2',SAP_CUSTOMER_NUMBER__c = '0000001003', PRODUCT_LINE__c  = 'LINEB', PRODUCT_NAME__c ='PRD2', PRODUCT_TYPE__c = 'TYPEB');
                MV_CUSTOMER_SALES_HISTORY__x testc3 = new MV_CUSTOMER_SALES_HISTORY__x(DisplayUrl= 'TEST3',ExternalId='TEST3',SAP_CUSTOMER_NUMBER__c = '0000001003', PRODUCT_LINE__c  = 'LINEB', PRODUCT_NAME__c ='PRD2', PRODUCT_TYPE__c = 'TYPEC');
                MV_CUSTOMER_SALES_HISTORY__x testc4 = new MV_CUSTOMER_SALES_HISTORY__x(DisplayUrl= 'TEST4',ExternalId='TEST4',SAP_CUSTOMER_NUMBER__c = '0000001003', PRODUCT_LINE__c  = 'LINEB', PRODUCT_NAME__c ='PRD3', PRODUCT_TYPE__c = 'LPD TOTAL');
                MV_CUSTOMER_SALES_HISTORY__x testc5 = new MV_CUSTOMER_SALES_HISTORY__x();
                MV_CUSTOMER_SALES_HISTORY__x testc6 = new MV_CUSTOMER_SALES_HISTORY__x(ExternalId ='0000066423-LPD TESTS-POULTRY TESTS-REO-N/A-USD-EUR-EUR', SAP_CUSTOMER_NUMBER__c ='0000066423',PRODUCT_TYPE__c ='LPD TOTAL',PRODUCT_LINE__c ='RUMINANT TESTS' , PRODUCT_FAMILY__c ='BVDV' , PRODUCT_NAME__c='N/A' , REV3_CURR_QUARTER__c =696.87,REV3_DIFF_QUARTER__c =8585.89,
                REV3_CURR_YEAR__c = 65738.00, REV3_CURR_QUARTER_YAGO__c = 84748.72, REV3_YOY_YEAR__c =734740.73 , HA_QTY_DIFF_QUARTER__c = 749473.00, REV3_DIFF_YEAR__c = 88493.849 , REV3_CURR_YEAR_YAGO__c = 1879.38, REV3_YOY_QUARTER__c= 849484.849);
                controllerInstance.currentAccountRecord.SAP_CUSTOMER_NUMBER__c = '0000001003';
                controllerInstance.rowSpanProductTypeMap.put('TYPEA',3);
                controllerInstance.ProductTypeSet.add('TYPEB');
                controllerInstance.GroupProducts();
                controllerInstance.ProductTypeLineSet.add('TYPEA;LINEA');
                controllerInstance.GroupProducts();
                controllerInstance.externalSalesHistoryList.add(testc5);
                controllerInstance.GroupWrappers();
                controllerInstance.externalSalesHistoryList.add(testc6);
                controllerInstance.externalSalesHistoryList.add(testc5);
                controllerInstance.externalSalesHistoryList.add(testc);
                controllerInstance.externalSalesHistoryList.add(testc2);
                controllerInstance.externalSalesHistoryList.add(testc3);
                controllerInstance.externalSalesHistoryList.add(testc4);
                controllerInstance.GroupProducts();
            Test.stopTest();
        }
    }
}