/**************************************************************************************
* Apex Class Name: Test_MaterialProcessingService
*
* Description: Test class for the MaterialProcessingService callout
*
*                   
* Developer           Date              Description
* ------------------------------------------------------------------------------------                 
* Heather Kinney      8.5.2016         Original Version
*
*************************************************************************************/

@isTest
public class Test_MaterialProcessingService {
    
     static testMethod void testGetData1() {
    	 User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
            system.runAs(currentUser){
                
                Account thisAccount = new Account();
                thisAccount = CreateTestClassData.createCustomerAccount();
                thisAccount.SAP_Customer_Number__c = '0000008649';
            
                Test.startTest();
                
                MaterialProcessingService.DT_STOCK_OVERVIEW_Request request = new MaterialProcessingService.DT_STOCK_OVERVIEW_Request();
                MaterialProcessingService.ITEMS_element[] ITEMS = new MaterialProcessingService.ITEMS_element[]{};     
                MaterialProcessingService.ITEMS_element itemElement = new MaterialProcessingService.ITEMS_element();
                
                itemElement.CUSTOMER = '0000013014';
                itemElement.COUNTRY = 'US';
                itemElement.SALES_ORG = 'USS1';
                itemElement.DIST_CHAN = '00';
                itemElement.DIVISION = 'CE';
                itemElement.MATERIAL = '98-14074-00';
                //itemElement.BATCH = 'xxxxxx';
                //itemElement.PLANT = 'xxxxxx';
                
				ITEMS.add(itemElement);
                
                request.ITEMS = ITEMS;

                MaterialProcessingService.HTTPS_Port service = new MaterialProcessingService.HTTPS_Port();
                service.StockQuantityQuery(ITEMS);
                
                Test.stopTest();
                
            }
     }
    
    static testMethod void testResponseData() {
    	 User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
            system.runAs(currentUser){
                Test.startTest();

    			MaterialProcessingService.DT_STOCK_OVERVIEW_Response response_x = CreateTestClassData.createMaterialProcessingTestResponse();

              
                Test.stopTest();
            }
    }
    
    

}