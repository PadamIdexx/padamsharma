@isTest
public class Test_ServiceInstrumentController {
    public static List<Account> accts = new List<Account>();
    public static List<Asset> asts = new List<Asset>();
    
    
    @testSetup 
    static void createTestData(){
        
        accts = ([SELECT Id, Name, SAP_Number_without_Leading_Zeros__c FROM Account WHERE Name LIKE '%TestAccount%' ]);
        asts = ([SELECT Id, Name FROM Asset WHERE Name LIKE '%TestAsset%']);
        
    }
    static testMethod void testServiceInstrumentPage(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'andt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='testSystem@idexx.com');
        System.runAs(u) {
        
            Test_Data_Utility.createTestAssets(1, 1);
        system.debug('sess ' + UserInfo.getSessionId());
        createTestData();
        System.debug('account ' + accts.get(0) + 'Asset ' + asts.get(0));
            if(accts.get(0) != null && asts.get(0) != null){    
        Account act = accts.get(0);
        Asset stdAsset = asts.get(0);
        
        PageReference page = new PageReference('/apex/serviceInstruments/?id=' + act.SAP_Number_without_Leading_Zeros__c);
        
        Test.setCurrentPage(page);
        
        ApexPages.StandardController stdCont = new
            ApexPages.StandardController(stdAsset);
        
        serviceInstrumentsControllerExt testCont =
            new serviceInstrumentsControllerExt(stdCont);
        
        Test_genericMock mock = new Test_genericMock();
        mock.callType = 'service';
                
        test.startTest();
        
        Test.setMock(HttpCalloutMock.class, mock);

        testCont.eqIds = new Set<String>();
        testCont.equip = new List<Map<String,String>>();
        testCont.notes = new List<Map<String,String>>();
        testCont.details = new List<Map<String, String>>();
        testCont.fetchEvents();   
        serviceInstrumentCallout.getEvents(stdAsset.Id);
        
        test.stopTest();
            }
            else{
                system.debug('no account or asset');
            }
        }
            
        
    }
    
    
    
}