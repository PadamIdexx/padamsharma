@IsTest
private class Test_UserHelper {

    static testMethod void testIsSystemAdministator() {
        User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
        
        system.runAs(adminUser){
            UserHelper u = new UserHelper();
            
            Test.startTest();
            //test the primary profile for this test case
            u.isSystemAdministrator();
            
            //test other profiles
            u.isCAG();
            u.isLPD();
            u.isWater();

            u.isEMEA();
            u.isNA();
      
            Test.stopTest();            
        }
    }

    static testMethod void testCheckProfile() {
        User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
        
        system.runAs(adminUser){
            UserHelper u = new UserHelper();
            
            Test.startTest();
      
            u.doesUserHaveProfile('System Administrator');
            u.doesUserHaveProfile('Negative Test');       
            
            Test.stopTest();            
        }        
    }
    
    static testMethod void testIsCAG() {
        User adminUser = CreateTestClassData.reusableUser('Inside Sales Manager-CAG','Idexx');
        
        system.runAs(adminUser){
            UserHelper u = new UserHelper();
            
            Test.startTest();
            //test the primary profile for this test case
            u.isCAG();
            
            //test other profiles
            u.isSystemAdministrator();
            u.isLPD();
            u.isWater();

            u.isEMEA();
            u.isNA();
      
            Test.stopTest();            
        }
    }    
    
    static testMethod void testIsLPD() {
        User adminUser = CreateTestClassData.reusableUser('Inside Sales Manager-LPD','Idexx');
        
        system.runAs(adminUser){
            UserHelper u = new UserHelper();
            
            Test.startTest();
            //test the primary profile for this test case
            u.isLPD();
            
            //test other profiles
            u.isSystemAdministrator();
            u.isCAG();
            u.isWater();

            u.isEMEA();
            u.isNA();
      
            Test.stopTest();            
        }
    }    
    
    static testMethod void testIsWater() {
        User adminUser = CreateTestClassData.reusableUser('Water EMEA Customer Support','Idexx');
        
        system.runAs(adminUser){
            UserHelper u = new UserHelper();
            
            Test.startTest();
            //test the primary profile for this test case
            u.isLPD();
            
            //test other profiles
            u.isSystemAdministrator();
            u.isCAG();
            u.isWater();

            u.isEMEA();
            u.isNA();
      
            Test.stopTest();            
        }
    }    
    
    static testMethod void testProfileDisplayWrapper() {
                User adminUser = CreateTestClassData.reusableUser('Inside Sales Manager-CAG','Idexx');
        
        system.runAs(adminUser){
             
            Test.startTest(); 
            
            ProfileDisplayWrapper wrap = new ProfileDisplayWrapper();
            
            System.debug(wrap);
            
            Test.stopTest();            
        }
    }
    
}