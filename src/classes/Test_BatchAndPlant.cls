/**************************************************************************************
* Apex Class Name: Test_BatchAndPlant
*
* Description: Test class for the Test_BatchAndPlant class.
*
*                   
* Developer           Date              Description
* ------------------------------------------------------------------------------------                 
* Heather Kinney      8.17.2016         Original Version
*
*************************************************************************************/

@isTest
public class Test_BatchAndPlant {
    
    static testMethod void testBatchAndPlant() {
    	 User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
            system.runAs(currentUser){
                
                Account thisAccount = new Account();
                thisAccount = CreateTestClassData.createCustomerAccount();
                thisAccount.SAP_Customer_Number__c = '0000008649';
                
                BatchAndPlant batchAndPlant = new BatchAndPlant();
                BatchAndPlant.WrapperPlantAndBatch plantAndBatchInfo = new BatchAndPlant.WrapperPlantAndBatch();
                
                String vcustomer = '0000013014';
                String vcountry = 'US';
                String vsOrg = 'USS1';
                String vChannel = '00';
                String vSalesDiv = 'CL';
                String vPlant = 'USP1';
                String vProdMat = '98-14074-00';
                String vBatch = '019089';
                String ExternalItemNum = '1';
                
                MaterialProcessingService.DT_STOCK_OVERVIEW_Response response_x = CreateTestClassData.createMaterialProcessingTestResponse();
                System.debug('response_x = ' + response_x);
                batchAndPlant.setResponse(response_x);
                
                batchAndPlant.setBatchReservationsList(getBatchReservations());
                plantAndBatchInfo = batchAndPlant.loadStockInfo(vcustomer, vcountry, vsOrg, vChannel, vSalesDiv, vPlant, vProdMat, ExternalItemNum, vBatch);
                
                List<BatchAndPlant.WrapperBatch> materialItems = new List<BatchAndPlant.WrapperBatch>();
                BatchAndPlant.WrapperBatch materialItem = new BatchAndPlant.WrapperBatch();
                materialItem.Batch = vBatch;
                materialItem.Plant = vPlant;
                materialItem.productMatNo = vProdMat;
                
                Map<String, BatchAndPlant.WrapperPlant> stockInfoMap = batchAndPlant.loadStockInfoForMultipleMaterials(vcustomer, vcountry, vsOrg, vChannel, vSalesDiv, materialItems);
                
                batchAndPlant.setNotFoundDataVariables();
                
                System.assertNotEquals(null, plantAndBatchInfo);
            
                Test.startTest();
            }
    }
    
    
    private static List<BatchReservationService.BatchReservation> getBatchReservations() {
        
        List<BatchReservationService.BatchReservation> batchReservations = new List<BatchReservationService.BatchReservation>();
        BatchReservationService.BatchReservation batchReservation = new BatchReservationService.BatchReservation();
        
        batchReservation.Sold_To = '0000013014';
        batchReservation.Ship_To = '0000013014';
        batchReservation.Consumed_Qty = null;
        batchReservation.Dist_Channel = '00';
        batchReservation.End_Date = '20161219';
        batchReservation.Line_Item = '000020';
        batchReservation.Reserved_Qty = null;
        batchReservation.Sales_Org = 'USS1';
        batchReservation.Start_Date = '20141219';
        batchReservation.batch = '019089';
        batchReservation.division = 'CE';
        batchReservation.material = '98-14074-00';
        batchReservation.plant = 'USP1';
        batchReservation.reservation = '0040095126';
        batchReservation.status = 'A';
        
        batchReservations.add(batchReservation);
        
		//BatchReservation:[Consumed_Qty=null, Dist_Channel=00, End_Date=20161022, Line_Item=000040, Reserved_Qty=1, Sales_Org=USS1, Ship_To=0000013014, 
		//                  Sold_To=0000013014, Start_Date=20151023, batch=019089, division=CE, material=98-14074-00, plant=USP1, reservation=0040095126, status=A], 

        return batchReservations; 
    }

}