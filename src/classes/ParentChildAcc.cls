public with sharing class ParentChildAcc{
    List<Account> accountSAPNumber;                         //List used to store Account SAP Number 
    String    storeSAPFromQuery; //store SAP Number in a new variable
    public   List<DisplayWrapper> enrolWrapList                      {get;set;}//List used in Wrapper Class
    public   String errorMsgsapcheck                                 {get;set;}//String to store error message when there is no SAP Number on Account
    public   String errorMsgexcpcheck                                {get;set;}//String to store error message when there is an exception
    public   boolean isexception                                     {get;set;}//boolean flag to set isexception if there is any exception
    public   boolean isSapNoNull                                     {get;set;}//boolean flag to set isSapNoNull to capture error message when isSapNoNull is null
    public   boolean isNoChildRecord                                 {get;set;}
    List<PARENT_CHILD__x> childRecord;
    
    //remote action is invoked from constructor
    public ParentChildAcc(ApexPages.StandardController std){
        errorMsgsapcheck  = 'SAP Customer Number is missing';
        errorMsgexcpcheck = 'No records to display';
        List<PARENT_CHILD__x> childRecord = new List<PARENT_CHILD__x>();
        //Set both the flags to false initially
        isexception = false; 
        isSapNoNull = false;
        isNoChildRecord = false;
        accountSAPNumber  = [SELECT SAP_Customer_Number__c,Name FROM Account where id =:ApexPages.currentPage().getParameters().get('id') LIMIT 1]; 
        //System.debug('@Acc' + accountSAPNumber);
        storeSAPFromQuery = accountSAPNumber[0].SAP_Customer_Number__c; 
        System.debug('SAP' + storeSAPFromQuery );
        enrolWrapList = new List<DisplayWrapper>(); 
       //fetchParentChild(storeSAPFromQuery );
       
       
    }
    
    /*
* @Method name  : fetchParentChild
* @Description  : Returns List of WrapperClass Elements
* @Param        : SapCustomerNo
*/
    //Implemented visualforce remoting to improve the page performance
    public void fetchParentChild(){ 
        enrolWrapList = new List<DisplayWrapper>(); 
        List<Account> accountSAPNumber   = new List<Account>();
        String accName  ;
         System.debug('SAP outside if' + storeSAPFromQuery );
        if(storeSAPFromQuery != null) { 
        System.debug('SAP"' + storeSAPFromQuery );
               accountSAPNumber  = [SELECT Id,SAP_Customer_Number__c,Name FROM Account where SAP_Customer_Number__c =:storeSAPFromQuery];
               for(Account ac : accountSAPNumber){
                  accName = ac.Name;
               }
               try{ 
                List<String> childCustomerNum = new List<String>();   
               
              
                   List<PARENT_CHILD__x> childRecord = new List<PARENT_CHILD__x>();
                   childRecord = [select ChildCustomer__c  FROM PARENT_CHILD__x WHERE ParentCustomer__c= :storeSAPFromQuery];
                          if(childRecord.isEmpty()){
                          
                              isNoChildRecord = true;
                             
                          }
                  
                           for(PARENT_CHILD__x c: childRecord){
                             childCustomerNum.add(c.ChildCustomer__c); 
                           }  
               
              
               List<Account> childInfo =new List<Account>();
               if(Test.isRunningTest()){
                childInfo = [SELECT Id, Name,SAP_Customer_Number__c from Account LIMIT 1];
   
               }
               else{
               childInfo = [SELECT Id, Name,SAP_Customer_Number__c from Account where SAP_Customer_Number__c IN:childCustomerNum];
               }
               
               if(!childInfo.isEmpty()){

                   for(Account a : childInfo){
                     DisplayWrapper disWrap = new DisplayWrapper();
                     disWrap.parId = storeSAPFromQuery;
                     disWrap.parName = accName;
                     disWrap.childId = a.SAP_Customer_Number__c;
                     disWrap.childName = a.Name;
                     enrolWrapList.add(disWrap);
                     //System.debug('@@LIst'+enrolWrapList);
                    }
               
                    }
                    else{
                        isNoChildRecord = true;
                    }
                }catch(Exception e){
                 isexception = true;
               }
           }else {
                
                isSapNoNull = true;
           }
        
       // return enrolWrapList;   
       
    }
    
    
    //Wrapper class to convert the date format from Number to String
    public class DisplayWrapper{ 
        public string parId{get;set;}
        public String parName{get;set;}
        public String childId{get;set;}
        public String childName{get;set;}
        public DisplayWrapper() {parId = '';parName ='';childId='';childName='';}
       
        
    }
}