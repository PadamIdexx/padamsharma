/**************************************************************************************
 * Create By:   Deloitte Consulting LLP.
 * CreateDate:  15-Oct-2015
 * Description: This is handler class for trigger ContactTrigger    
 *  
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                 Created Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Vishal Negandhi           15-Oct-2015                   Initial version.
 *	* Hemanth Kumar				09-Mar-2016					  As per TKT-000595.
 **************************************************************************************/
public class ContactTriggerHandler{
	 
    public void onAfterInsert(List<Contact> InsertedContacts){
    	SetPlaceHolderCheckbox(InsertedContacts);
        reAlignPrimaryContacts(InsertedContacts);
    }
    public void onAfterUpdate(Map<Id,Contact> oldContactsMap , Map<Id,Contact> newContactsMap) {
    	reAlignPrimaryContacts(oldContactsMap , newContactsMap);    	
    }
    private void reAlignPrimaryContacts(List<Contact> newContacts) {
        Set<Id> accIds = new Set<Id>();
        for(Contact eachContact : newContacts) {
            if(String.isNotBlank(eachContact.AccountId)) {
                accIds.add(eachContact.AccountId);
            }
        }
        List<Account> accountsToBeUpdated = new List<Account>();
        for(Account acc : [select Id,Primary_Contact__c,(Select Id,Name,IsActive__c,Place_Holder_To_Be_Deleted__c From Contacts  where IsActive__c != 'N'  ORDER BY IsActive__c Desc NULLS LAST ) From Account WHERE ID IN:accIds]) {
            for(Contact primaryContact : acc.Contacts) {
                System.debug('pm is:'+primaryContact);
				acc.Primary_Contact__c = primaryContact.Id;
				accountsToBeUpdated.add(acc);
				break;
			}
        }
        if(accountsToBeUpdated.size()> 0){
            update accountsToBeUpdated;
        }
    }
    private void reAlignPrimaryContacts(Map<Id,Contact> oldContactsMap , Map<Id,Contact> newContactsMap) {
    	Set<Id> contactIds = new Set<Id>();
    	List<Account> accountsToBeUpdated = new List<Account>();
    	List<Contact> PlaceHolderContactsToBeCreated = new List<Contact>();
	    for(Contact eachContact : newContactsMap.values()) {
			if( oldContactsMap.get(eachContact.Id).isActive__c != newContactsMap.get(eachContact.Id).isActive__c && eachContact.isActive__c == 'N') {
				contactIds.add(eachContact.Id);
			}
		}
		//accountsToBeUpdated = [SELECT Id,Primary_Contact__c,(Select Id,Name From Contacts where IsActive__c = 'Y' ORDER BY LastModifiedDate Desc) From Account WHERE Primary_Contact__c IN:contactIds];
		for(Account acc :[SELECT Id,Primary_Contact__c,SAP_Customer_Number__c ,CurrencyIsoCode,(Select Id,Name From Contacts  where IsActive__c != 'N'  ORDER BY IsActive__c Desc NULLS LAST ) From Account WHERE Primary_Contact__c IN:contactIds]) {
			System.debug('acc is:'+acc);
            if(acc.contacts == null || acc.contacts.size() == 0) {
				Contact PlaceHolderContact = new Contact();
				PlaceHolderContact = new Contact();
  				PlaceHolderContact.AccountId = acc.Id;
                PlaceHolderContact.LastName = String.isNotBlank(acc.SAP_Customer_Number__c) ?  acc.SAP_Customer_Number__c : acc.id;
  				PlaceHolderContact.FirstName = 'Place Holder';
	  			PlaceHolderContact.CurrencyIsoCode = acc.CurrencyIsoCode;
	  			PlaceHolderContactsToBeCreated.add(PlaceHolderContact);
			}
			for(Contact primaryContact : acc.Contacts) {
                System.debug('pm is:'+primaryContact);
				acc.Primary_Contact__c = primaryContact.Id;
				accountsToBeUpdated.add(acc);
				break;
			}
		}
		if(PlaceHolderContactsToBeCreated.size()>0) {
            System.debug('place holder contacts:'+PlaceHolderContactsToBeCreated);
			insert PlaceHolderContactsToBeCreated;
            System.debug('after place holder contacts:'+PlaceHolderContactsToBeCreated);
		}
		if(accountsToBeUpdated.size()>0) {
            System.debug('Account to be updated:'+accountsToBeUpdated);
			update accountsToBeUpdated;
            System.debug('after Account to be updated:'+accountsToBeUpdated);
		}
		
	}
    private void SetPlaceHolderCheckbox(List<Contact> ContactRecords){
    	Map<Id, Id> AccountIdsToPrimaryContactIds = new Map<Id, Id>();
    	List<Contact> ContactsToBeUpdated = new List<Contact>(); 
    	List<Account> AccountsToBeUpdated = new List<Account>(); 
    	
    	for(Contact contRecord : ContactRecords){
    		if(contRecord.LastName != 'Place Holder' && contRecord.FirstName != 'Place Holder' )
    			AccountIdsToPrimaryContactIds.put(contRecord.AccountId, contRecord.Id);
            System.debug('map is:'+AccountIdsToPrimaryContactIds);
    	}
    	
    	for(Contact phContact : [Select Id, Place_Holder_To_Be_Deleted__c From Contact
    							  WHERE AccountId IN :AccountIdsToPrimaryContactIds.keyset() 
    							  AND (FirstName = 'Place Holder' OR LastName = 'Place Holder')]){
    		phContact.Place_Holder_To_Be_Deleted__c = true;
    		ContactsToBeUpdated.add(phContact);						  	
    	}
    	//fetch existing primary contact
    	Map<Id, Id> existingPrimaryContactIds = new Map<Id, Id>();
    	if(!AccountIdsToPrimaryContactIds.isEmpty()){
	    	for(Account accRecord : [Select Id, Primary_Contact__c FROM Account 
	    	WHERE Id IN :AccountIdsToPrimaryContactIds.keyset() AND RecordType.DeveloperName = 'Customer'
	    	AND  (Primary_Contact__r.FirstName = 'Place Holder' OR Primary_Contact__r.LastName = 'Place Holder')]){
	    		existingPrimaryContactIds.put(accRecord.Primary_Contact__c, AccountIdsToPrimaryContactIds.get(accRecord.Id));
	    		accRecord.Primary_Contact__c = AccountIdsToPrimaryContactIds.get(accRecord.Id);	
	    		AccountsToBeUpdated.add(accRecord);
	    	}
    	}
    	List<CampaignMember> updateCampaignMembers = new List<CampaignMember>();
    	List<CampaignMember> deleteCampaignMembers = new List<CampaignMember>();
    	if(!existingPrimaryContactIds.isEmpty()){
	    	for(CampaignMember cms :[Select Status, HasResponded, FirstRespondedDate, CurrencyIsoCode, ContactId, CampaignId
	    							From CampaignMember c WHERE ContactId IN :existingPrimaryContactIds.keySet()]){
	    		CampaignMember cm = cms.clone(false, false);
	    		cm.ContactId = existingPrimaryContactIds.get(cms.ContactId);
	    		updateCampaignMembers.add(cm);
	    		
	    		deleteCampaignMembers.add(cms);
	    	}
    	}
    	if(!ContactsToBeUpdated.isEmpty())
    		update ContactsToBeUpdated;
    	
    	if(!AccountsToBeUpdated.isEmpty())
    		update AccountsToBeUpdated;
    	
    	if(!updateCampaignMembers.isEmpty())
    		insert updateCampaignMembers;
    		
    	if(!deleteCampaignMembers.isEmpty())	
    		delete deleteCampaignMembers;
    }
    
}