/**
*       @author Sirisha Kodi
*       @date   28/09/2015   
        @description   This class is used to display the sales history view as an inline VF page. 
        Function:      Class used to display the Sales History VF page that will appear in the related list of an account. 
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Sirisha Kodi                   19/10/2015          Modified to replace the custom object code with the external object references.
        Sirisha Kodi                   06/11/2015          Updated the code to refer to new External Object - US-3121 and TKT-000041
        
**/
public with sharing class InlineSalesHistoryController {
    
    
    private  ApexPages.StandardController                   controller                 {get;set;}//added an instance varaible for the standard controller
    public   list<productSalesViewWrapper>                  salesHistoryList           {get;set;}// List used to show the final grouped records
    public   String                                         errorMsg                   {get;set;}// List to store the custom message
    public  Account                                         currentAccountRecord;// local variable that points to the account record 
    public  set<String>                                     productTypeSet;   //Set to store the unique Product Types of the exisisting sales history records         
    public  set<String>                                     productTypeLineSet;  //Set to store the unique Product Type and the Product Line combination of the exisisting sales history records             
    public  map<String, Integer>                            rowSpanProductTypeLineMap;//Map to store the count of Product Type and Product Line -> Count of the sales history records with the same Key
    public  map<String, Integer>                            rowSpanProductTypeMap;//Map to store the count of Product Type -> Count of the sales history records with the product type
    public list<MV_CUSTOMER_SALES_HISTORY__x>               externalSalesHistoryList;
    public   static final String                            SEMICOLON = ';';
    public   static final String                            TECHNICAL_DETAILS = 'Technical Details :';
    //wrapper class 
    public class productSalesViewWrapper{
        
        public integer rowCount                             {get;set;} // integer variable used to identify the unique Product Type count - used as a rowspan attribute for product column
        public Boolean display                              {get;set;} //Boolean variable to determine if the row with a product Type is already displayed in UI or not
        public integer rowCountPL                           {get;set;} // integer variable used to identify the unique Product Line count - used as a rowspan attribute for product column
        public Boolean displayPL                            {get;set;} //Boolean variable to determine if the row with a productLine is already displayed in UI or not
        public MV_CUSTOMER_SALES_HISTORY__x salesHistory       {get;set;}
        
        //Parameterized constructor 
        public productSalesViewWrapper(Integer rowCount, boolean display, MV_CUSTOMER_SALES_HISTORY__x salesHistory, boolean displayPL, Integer rowCountPL){
            this.rowCount       = rowCount;
            this.display        = display;
            this.SalesHistory   = salesHistory;
            this.rowCountPL     = rowCountPL;
            this.displayPL      = displayPL;
          }
    }
    //constuctor 
    public InlineSalesHistoryController(ApexPages.StandardController controller){
        
        //Initializations of the local variables
        this.controller           = controller;  
        errorMsg                  = '';
        rowSpanProductTypeMap     = new map<String, Integer>();
        rowSpanProductTypeLineMap = new map<String, Integer>();
        salesHistoryList          = new List<productSalesViewWrapper>();
        productTypeSet            = new Set<String>();
        productTypeLineSet        = new Set<String>();
        externalSalesHistoryList  = new List<MV_CUSTOMER_SALES_HISTORY__x>();
         // fetch the account details 
        this.currentAccountRecord = (Account)controller.getRecord();
        if(this.currentAccountRecord<>null && currentAccountRecord.Id<>null)
            this.currentAccountRecord = [Select Id, SAP_CUSTOMER_NUMBER__c from Account where Id=:currentAccountRecord.Id];
        if(!Test.isRunningTest())
            GroupProducts();
    }
    
     /*
    * Method name     : GroupProducts
    * Description     : This method pulls all the sales history records related to the current account and populates the rowSpanProductTypeMap, rowSpanProductTypeLineMap
                        which are furtther used to populate the wrapper class records.
    * Return Type     : void
    * Input Parameter : nil
    */
   public void GroupProducts(){
    try{
           if(currentAccountRecord.SAP_CUSTOMER_NUMBER__c <> null && !Test.isRunningTest())
           //Query for all the MV_CUSTOMER_SALES_HISTORY__x records related to the current account object, order these records based on product type and product line so that these records are ordered and stored in the list
           externalSalesHistoryList = [select   ExternalId,SAP_CUSTOMER_NUMBER__c, PRODUCT_TYPE__c ,REV3_CURR_YEAR_YAGO__c,PRODUCT_LINE__c , PRODUCT_FAMILY__c , PRODUCT_NAME__c , REV3_CURR_QUARTER__c ,REV3_DIFF_QUARTER__c,
                                                REV3_CURR_YEAR__c , REV3_CURR_QUARTER_YAGO__c , REV3_YOY_YEAR__c , HA_QTY_DIFF_QUARTER__c , REV3_DIFF_YEAR__c , REV3_YOY_QUARTER__c
                                                from MV_CUSTOMER_SALES_HISTORY__x where SAP_CUSTOMER_NUMBER__c =:currentAccountRecord.SAP_CUSTOMER_NUMBER__c
                                                order by PRODUCT_TYPE__c, PRODUCT_LINE__c
                                                LIMIT : Limits.getLimitQueryRows()- Limits.getQueryRows()];
            if(!externalSalesHistoryList.isEmpty()){
                for( MV_CUSTOMER_SALES_HISTORY__x srvar : externalSalesHistoryList){
                    // populate the rowSpanProductTypeMap (ProductType -> count )  
                    if(srvar.PRODUCT_TYPE__c<> null){
                        if(rowSpanProductTypeMap.containsKey(srvar.PRODUCT_TYPE__c)){
                            integer updatedCount = rowSpanProductTypeMap.get(srvar.PRODUCT_TYPE__c)+1;
                            rowSpanProductTypeMap.put(srvar.PRODUCT_TYPE__c,updatedCount);
                         }else
                            rowSpanProductTypeMap.put(srvar.PRODUCT_TYPE__c,1);
                    }
                    // populate the rowSpanProductTypeLineMap (ProductType+';'ProductLine -> count )  
                    if(srvar.PRODUCT_TYPE__c<> null &&srvar.PRODUCT_LINE__c<> null){
                            if(rowSpanProductTypeLineMap.containsKey(srvar.PRODUCT_TYPE__c+SEMICOLON+srvar.PRODUCT_LINE__c))
                                rowSpanProductTypeLineMap.put(srvar.PRODUCT_TYPE__c+SEMICOLON+srvar.PRODUCT_LINE__c,  rowSpanProductTypeLineMap.get(srvar.PRODUCT_TYPE__c+SEMICOLON+srvar.PRODUCT_LINE__c)+1);
                            else
                                rowSpanProductTypeLineMap.put(srvar.PRODUCT_TYPE__c+SEMICOLON+srvar.PRODUCT_LINE__c,1);
                        }
                    }
                    GroupWrappers();
                }else
                    errorMsg = TECHNICAL_DETAILS+ system.Label.Sales_History_Custom_Message;
            }catch(Exception ex){
                system.debug('*** exception details'+ex.getStackTraceString());
                errorMsg = TECHNICAL_DETAILS+ex.getMessage();
            }
        }
     /*
    * Method name     : GroupWrappers
    * Description     : based on the maps populated in GroupProducts, this method populates the records in wrapper format productSalesViewWrapper.
    * Return Type     : void
    * Input Parameter : nil
    */
    public void GroupWrappers(){
        
        if(!externalSalesHistoryList.isEmpty() && rowSpanProductTypeMap.size()>0 ){
            //loop over the sales history  records and create the wrappers 
            for(MV_CUSTOMER_SALES_HISTORY__x srvar : externalSalesHistoryList){
                
                //check to populate the wrapper 
                if(rowSpanProductTypeMap.containskey(srvar.PRODUCT_TYPE__c) || 
                    rowSpanProductTypeLineMap.containsKey(srVar.PRODUCT_TYPE__c+SEMICOLON+srVar.PRODUCT_LINE__c)){
                    if(productTypeSet.contains(srVar.PRODUCT_TYPE__c)){
                        if(productTypeLineSet.contains(srVar.PRODUCT_TYPE__c+SEMICOLON+srVar.PRODUCT_LINE__c))
                            salesHistoryList.add(new productSalesViewWrapper(rowSpanProductTypeMap.get(srvar.PRODUCT_TYPE__c),false,srvar,false,rowSpanProductTypeLineMap.get(srVar.PRODUCT_TYPE__c+SEMICOLON+srVar.PRODUCT_LINE__c)));
                        else{
                            salesHistoryList.add(new productSalesViewWrapper(rowSpanProductTypeMap.get(srvar.PRODUCT_TYPE__c),false,srvar,true,rowSpanProductTypeLineMap.get(srVar.PRODUCT_TYPE__c+SEMICOLON+srVar.PRODUCT_LINE__c)));
                            productTypeLineSet.add(srVar.PRODUCT_TYPE__c+SEMICOLON+srVar.PRODUCT_LINE__c);
                        }
                    }
                    else{
                        productTypeSet.add(srVar.PRODUCT_TYPE__c);
                        if(productTypeLineSet.contains(srVar.PRODUCT_TYPE__c+SEMICOLON+srVar.PRODUCT_LINE__c))
                            salesHistoryList.add(new productSalesViewWrapper(rowSpanProductTypeMap.get(srvar.PRODUCT_TYPE__c),true,srvar,false,rowSpanProductTypeLineMap.get(srVar.PRODUCT_TYPE__c+SEMICOLON+srVar.PRODUCT_LINE__c)));
                        else{
                            productTypeLineSet.add(srVar.PRODUCT_TYPE__c+SEMICOLON+srVar.PRODUCT_LINE__c);
                            salesHistoryList.add(new productSalesViewWrapper(rowSpanProductTypeMap.get(srvar.PRODUCT_TYPE__c),true,srvar,true,rowSpanProductTypeLineMap.get(srVar.PRODUCT_TYPE__c+SEMICOLON+srVar.PRODUCT_LINE__c)));
                        }
                    }
                }
            }
        }
    }
}