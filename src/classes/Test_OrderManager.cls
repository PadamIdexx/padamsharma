@isTest(SeeAllData=true)
private class Test_OrderManager {
    
    @isTest static void changeOrderPositive() {
        Test.startTest();
        
        SAPOrder order = OrderTestDataFactory.createSAPOrder();
        
        SAPOrder updatedOrder = OrderManager.changeOrder(order);
        
        Test.stopTest();
    }

    @isTest static void simulateOrderPositive() {
        Test.startTest();
        
        SAPOrder order = OrderTestDataFactory.createSAPOrder();
        
        SAPOrder updatedOrder = OrderManager.simulateOrder(order);
        
        Test.stopTest();
    }    
    
    @isTest static void createOrderPositive() {
        Test.startTest();
        
        SAPOrder order = OrderTestDataFactory.createSAPOrder();
        
        SAPOrder updatedOrder = OrderManager.createOrder(order);
        
        Test.stopTest();
    }
    
    @isTest static void readOrderPositive() {
        Test.startTest();
        
        SAPOrder order = OrderTestDataFactory.createSAPOrder();
        
        SAPOrder updatedOrder = OrderManager.getOrder(order);
        
        Test.stopTest();
    }    
    
    @isTest static void deleteOrderPositive() {
        Test.startTest();
        
        SAPOrder order = OrderTestDataFactory.createSAPOrder();
        
        SAPOrder updatedOrder = OrderManager.deleteOrder(order);
        
        Test.stopTest();
    }        
}