/**************************************************************************************
* Apex Class Name: Test_SediVueAutoReplenService
*
* Description: Test class for the SediVueAutoReplenishmentService callout
*
*                   
* Developer           Date              Description
* ------------------------------------------------------------------------------------                 
* Heather Kinney      7.8.2016         Original Version
*
*************************************************************************************/

@isTest
public class Test_SediVueAutoReplenService {
    
      static testMethod void testGetSubscriptionDetailsNoReturnInfo() {
    	 User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
            system.runAs(currentUser){
                Account thisAccount = new Account();
                thisAccount = CreateTestClassData.createCustomerAccount();
                thisAccount.SAP_Customer_Number__c = '1098';
                
                SediVueControllerExtension controller = new SediVueControllerExtension(new ApexPages.StandardController(thisAccount));
                Test.setCurrentPage(Page.OrderCreation);
                
                system.currentPageReference().getParameters().put('Id', thisAccount.Id);  
                system.currentPageReference().getParameters().put('hideBanner', 'true');
                controller.hideBanner = true;
                controller.errorMsg = 'Test Error Message';
    
                String fakeSapID ='1003';
                String fakeAccountId = '0017A00000D7IPQ';
                
                //This response was obtained by inserting a debugger immediately after the setting of the "response_x" variable in the 
                //GetDetails function in SediVueAutoReplenishmentService: response_x = response_map_x.get('response_x');...:
                
                String sampleResponse = 'SubscriptionResponse:[Return_x=Return_element:[Id=ZSB, Id_type_info=(Id, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), Message=Unable to find any subscription details, Message_type_info=(Message, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), Number_x=010, Number_x_type_info=(Number, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), Type_x=E, Type_x_type_info=(Type, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), apex_schema_type_info=(http://idexx.com/sappo/sales/autoreplenishment, false, false), field_order_type_info=(Type_x, Id, Number_x, Message)], Return_x_type_info=(Return, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), SubscriptionRespDetails=null, SubscriptionRespDetails_type_info=(SubscriptionRespDetails, http://idexx.com/sappo/sales/autoreplenishment, null, 0, -1, false), apex_schema_type_info=(http://idexx.com/sappo/sales/autoreplenishment, false, false), field_order_type_info=(SubscriptionRespDetails, Return_x)]';
                
                Test_ReusableMockResponse fakeResponse = new Test_ReusableMockResponse(200,'SUCCESS',sampleResponse, new map<String,String>{'Content-Type'=>'application/json'});
    
                Test.setMock(HttpCalloutMock.class, fakeResponse);
                Test.startTest();

                //the individual detail when we don't have a response
                SediVueAutoReplenishmentService.Return_element Return_x = new SediVueAutoReplenishmentService.Return_element(); 
                
                Return_x.Type_x = 'E';
                Return_x.Id = 'ZSB';
                Return_x.Number_x = '010';
                Return_x.Message = 'Unable to find any subscription details';

                SediVueAutoReplenishmentService.SubscriptionResponse response = new SediVueAutoReplenishmentService.SubscriptionResponse();
                
                response.Return_x = Return_x;
                
                controller.setResponse(response);
                
                SediVueControllerExtension.getResultsSync(fakeSapID);
                
                SediVueControllerExtension.processGetDetailsResponse(response, fakeSapID);
                
                Test.stopTest();    
        }    
        
    }
    
  
    static testMethod void testGetSubscriptionDetails() {
    	 User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
            system.runAs(currentUser){
                Account thisAccount = new Account();
                thisAccount = CreateTestClassData.createCustomerAccount();
                thisAccount.SAP_Customer_Number__c = '1098';
                
                SediVueControllerExtension controller = new SediVueControllerExtension(new ApexPages.StandardController(thisAccount));
                Test.setCurrentPage(Page.OrderCreation);
                
                system.currentPageReference().getParameters().put('Id', thisAccount.Id);
                system.currentPageReference().getParameters().put('hideBanner', 'true');
    
                String fakeSapID ='1098';
                String fakeAccountId = '0017A00000D7IPQ';
                
                //This response was obtained by inserting a debugger immediately after the setting of the "response_x" variable in the 
                //GetDetails function in SediVueAutoReplenishmentService: response_x = response_map_x.get('response_x');...:
                String sampleResponse = 'SubscriptionResponse:[Return_x=null, Return_x_type_info=(Return, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), SubscriptionRespDetails=(SubscriptionRespDetails_element:[ActiveFlag=X, ActiveFlag_type_info=(ActiveFlag, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), ChangedAt=15:39:43, ChangedAt_type_info=(ChangedAt, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), ChangedBy=PTIRUMALASET, ChangedBy_type_info=(ChangedBy, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), ChangedOn=2016-05-06, ChangedOn_type_info=(ChangedOn, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), CreatedAt=null, CreatedAt_type_info=(CreatedAt, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), CreatedBy=null, CreatedBy_type_info=(CreatedBy, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), CreatedOn=null, CreatedOn_type_info=(CreatedOn, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), DIvision=CP, DIvision_type_info=(DIvision, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), DistrChanl=00, DistrChanl_type_info=(DistrChanl, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), InTransitQty=0.000 , InTransitQty_type_info=(InTransitQty, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), InTransitUoM=null, InTransitUoM_type_info=(InTransitUoM, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), InventoryTot=3.000 , InventoryTot_type_info=(InventoryTot, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), InventoryUoM=null, InventoryUoM_type_info=(InventoryUoM, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), LastDelivery=null, LastDelivery_type_info=(LastDelivery, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), LastDelvCarrier=null, LastDelvCarrier_type_info=(LastDelvCarrier, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), LastDelvDate=null, LastDelvDate_type_info=(LastDelvDate, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), LastDelvItem=null, LastDelvItem_type_info=(LastDelvItem, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), LastDelvTrack=null, LastDelvTrack_type_info=(LastDelvTrack, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), LastOrder=null, LastOrder_type_info=(LastOrder, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), OpenOrdQty=100.000 , OpenOrdQty_type_info=(OpenOrdQty, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), OpenOrdUoM=null, OpenOrdUoM_type_info=(OpenOrdUoM, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), PendDelivery=null, PendDelivery_type_info=(PendDelivery, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), PendDelvCarrier=null, PendDelvCarrier_type_info=(PendDelvCarrier, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), PendDelvDate=null, PendDelvDate_type_info=(PendDelvDate, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), PendDelvItem=null, PendDelvItem_type_info=(PendDelvItem, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), PendDelvTrack=null, PendDelvTrack_type_info=(PendDelvTrack, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), PendOrder=null, PendOrder_type_info=(PendOrder, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), ProdDescription=null, ProdDescription_type_info=(ProdDescription, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), ProdReference=98-0005077-00, ProdReference_type_info=(ProdReference, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), ReOrderQty=5.000 , ReOrderQty_type_info=(ReOrderQty, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), ReOrderUoM=null, ReOrderUoM_type_info=(ReOrderUoM, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), ReplenishQty=2.000 , ReplenishQty_type_info=(ReplenishQty, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), ReplenishUoM=BOX, ReplenishUoM_type_info=(ReplenishUoM, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), SalesOrg=USS1, SalesOrg_type_info=(SalesOrg, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), ShipTo=0000001098, ShipToName=null, ShipToName_type_info=(ShipToName, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), ShipTo_type_info=(ShipTo, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), SoldTo=0000001098, SoldToName=null, SoldToName_type_info=(SoldToName, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), SoldTo_type_info=(SoldTo, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), apex_schema_type_info=(http://idexx.com/sappo/sales/autoreplenishment, false, false), field_order_type_info=(ShipTo, ShipToName, SoldTo, SoldToName, ProdReference, ProdDescription, SalesOrg, DistrChanl, DIvision, InventoryTot, ...)]), SubscriptionRespDetails_type_info=(SubscriptionRespDetails, http://idexx.com/sappo/sales/autoreplenishment, null, 0, -1, false), apex_schema_type_info=(http://idexx.com/sappo/sales/autoreplenishment, false, false), field_order_type_info=(SubscriptionRespDetails, Return_x)]';    
                Test_ReusableMockResponse fakeResponse = new Test_ReusableMockResponse(200,'SUCCESS',sampleResponse, new map<String,String>{'Content-Type'=>'application/json'});
    
                Test.setMock(HttpCalloutMock.class, fakeResponse);
                Test.startTest();

                //a list of details:
                SediVueAutoReplenishmentService.SubscriptionRespDetails_element[] SubscriptionRespDetails = new SediVueAutoReplenishmentService.SubscriptionRespDetails_element[]{};
                
                //one individual item detail:
                SediVueAutoReplenishmentService.SubscriptionRespDetails_element respDetail_Cuvettes = new SediVueAutoReplenishmentService.SubscriptionRespDetails_element();
                respDetail_Cuvettes.ProdReference = '98-0005077-00';      //Material Number
                respDetail_Cuvettes.ProdReference = 'Cuvettes, SediVue';  //Material Description
                respDetail_Cuvettes.InventoryTot = '10.000';              //Current Inventory ?
                respDetail_Cuvettes.ReOrderQty = '22.000';                //Re-Order Point ?
                respDetail_Cuvettes.PendDelvDate = '05-24-2016';          //Pending Delivery Date
                respDetail_Cuvettes.PendOrder = '143901975';              //Pending Order ID?
                respDetail_Cuvettes.PendDelvItem = '800004345';           //Pending Delivery Number? //NB: there's also PendDelivery
                respDetail_Cuvettes.PendDelvCarrier = 'FEDEX';            //Pending Delivery Carrier
                respDetail_Cuvettes.PendDelvTrack = '999999999999999';    //Pending Delivery Tracking Number
                respDetail_Cuvettes.LastDelvDate = '01-15-2016';          //Last Delivery Date
                respDetail_Cuvettes.LastOrder = '140123434';              //Last Order ID?
                respDetail_Cuvettes.LastDelvItem = '800002523';           //Last Delivery Number? //NB: there's also LastDelivery
                respDetail_Cuvettes.LastDelvCarrier = 'UPS';              //Last Delivery Carrier
                respDetail_Cuvettes.LastDelvTrack = '1Z9999999999999999'; //Last Delivery Tracking Number
                
                SubscriptionRespDetails.add(respDetail_Cuvettes);     
                
                SediVueAutoReplenishmentService.SubscriptionResponse response = new SediVueAutoReplenishmentService.SubscriptionResponse();
                
                response.SubscriptionRespDetails = SubscriptionRespDetails;
                
                controller.setResponse(response);
                
                SediVueControllerExtension.getResultsSync(fakeSapID);
                
                SediVueControllerExtension.processGetDetailsResponse(response, fakeSapID);
                
                Test.stopTest();
                
            }
    }
    
    static testMethod void testUpdateSubscriptionDetails() {
    	 User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
            system.runAs(currentUser){
                Account thisAccount = new Account();
                thisAccount = CreateTestClassData.createCustomerAccount();
                thisAccount.SAP_Customer_Number__c = '19907';
                
                SediVueControllerExtension controller = new SediVueControllerExtension(new ApexPages.StandardController(thisAccount));
                Test.setCurrentPage(Page.OrderCreation);
                
                system.currentPageReference().getParameters().put('Id', thisAccount.Id);
                system.currentPageReference().getParameters().put('hideBanner', 'true');
    
                String fakeSapID ='19907';
                String fakeAccountId = '0017A00000D7IPQ';
                
                //This response was obtained by inserting a debugger immediately after the setting of the "response_x" variable in the 
                //UpdSubsc function in SediVueAutoReplenishmentService: response_x = response_map_x.get('response_x');...:
				String sampleResponse = '(SubUpdResp_element:[ProdReference=98-0035203-00, ProdReference_type_info=(ProdReference, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), Return_x=Return_element:[Id=ZSB, Id_type_info=(Id, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), Message=Subscription details updated successfully., Message_type_info=(Message, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), Number_x=013, Number_x_type_info=(Number, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), Type_x=S, Type_x_type_info=(Type, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), apex_schema_type_info=(http://idexx.com/sappo/sales/autoreplenishment, false, false), field_order_type_info=(Type_x, Id, Number_x, Message)], Return_x_type_info=(Return, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), ShipTo=0000019907, ShipTo_type_info=(ShipTo, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), apex_schema_type_info=(http://idexx.com/sappo/sales/autoreplenishment, false, false), field_order_type_info=(ShipTo, ProdReference, Return_x)])';
                Test_ReusableMockResponse fakeResponse = new Test_ReusableMockResponse(200,'SUCCESS',sampleResponse, new map<String,String>{'Content-Type'=>'application/json'});
    
                Test.setMock(HttpCalloutMock.class, fakeResponse);
                Test.startTest();

                //an Update request payload
                SediVueAutoReplenishmentService.SubUpdReq_element cuvettesUpdate = new SediVueAutoReplenishmentService.SubUpdReq_element();
                
                cuvettesUpdate.ShipTo = '19907';
                cuvettesUpdate.ProdReference = '98-0005077-00';
                cuvettesUpdate.InventoryTot = '100';
                cuvettesUpdate.ReOrderQty = '50';
                cuvettesUpdate.ReplQuantity = '25';
                cuvettesUpdate.Active = 'X';
                cuvettesUpdate.ChangedBy = 'testsfdc';
                cuvettesUpdate.ChangedOn = '2016-07-12';
                cuvettesUpdate.ChangedAt = '13:16:25';
                
                //a list of update details:
                SediVueAutoReplenishmentService.SubUpdReq_element[] SubUpdReq = new SediVueAutoReplenishmentService.SubUpdReq_element[]{}; 
                SubUpdReq.add(cuvettesUpdate);
                
                //SediVueAutoReplenishmentService.SubscriptionUpdResponse response = new SediVueAutoReplenishmentService.SubscriptionUpdResponse();
                SediVueAutoReplenishmentService.SubUpdResp_element[] response = new SediVueAutoReplenishmentService.SubUpdResp_element[]{};
                
                SediVueAutoReplenishmentService.SubUpdResp_element subscriptionUpdateElementDetail = new SediVueAutoReplenishmentService.SubUpdResp_element();
                
                SediVueAutoReplenishmentService.Return_element Return_x = new SediVueAutoReplenishmentService.Return_element();
                Return_x.Type_x = '0';
                Return_x.Id = '000';
                Return_x.Number_x = '000';
                Return_x.Message = 'Dummy return message';
                subscriptionUpdateElementDetail.ShipTo = '19907';
                subscriptionUpdateElementDetail.ProdReference = '98-0005077-00';
                subscriptionUpdateElementDetail.Return_x = Return_x;
                
                SediVueAutoReplenishmentService.SubscriptionUpdRequest request = new SediVueAutoReplenishmentService.SubscriptionUpdRequest();
                request.SubUpdReq = SubUpdReq;
     
                controller.setUpdateResponse(response);

                SediVueControllerExtension.getResultsSync(fakeSapID);

                //SediVueControllerExtension.processGetDetailsResponse(response);

                Test.stopTest();
                
            }
    }
    
    static testMethod void UpdateSubscriptionDetailsCurrentInventory() {
    	User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
            system.runAs(currentUser){
                Account thisAccount = new Account();
                thisAccount = CreateTestClassData.createCustomerAccount();
                thisAccount.SAP_Customer_Number__c = '19907';
                
                SediVueControllerExtension controller = new SediVueControllerExtension(new ApexPages.StandardController(thisAccount));
                Test.setCurrentPage(Page.OrderCreation);
                
                system.currentPageReference().getParameters().put('Id', thisAccount.Id);
                system.currentPageReference().getParameters().put('hideBanner', 'true');
    
                String fakeSapID ='0000000001';
                String fakeAccountId = '0017A00000D7IPQ';

                String sampleResponse = '(SubUpdResp_element:[ProdReference=98-0035203-00, ProdReference_type_info=(ProdReference, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), Return_x=Return_element:[Id=ZSB, Id_type_info=(Id, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), Message=Subscription details updated successfully., Message_type_info=(Message, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), Number_x=013, Number_x_type_info=(Number, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), Type_x=S, Type_x_type_info=(Type, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), apex_schema_type_info=(http://idexx.com/sappo/sales/autoreplenishment, false, false), field_order_type_info=(Type_x, Id, Number_x, Message)], Return_x_type_info=(Return, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), ShipTo=0000019907, ShipTo_type_info=(ShipTo, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), apex_schema_type_info=(http://idexx.com/sappo/sales/autoreplenishment, false, false), field_order_type_info=(ShipTo, ProdReference, Return_x)])';
                Test_ReusableMockResponse fakeResponse = new Test_ReusableMockResponse(200,'SUCCESS',sampleResponse, new map<String,String>{'Content-Type'=>'application/json'});
    
                Test.setMock(HttpCalloutMock.class, fakeResponse);
                
                Test.startTest();
                
                String jsonUpdatePayload = '[{"shipTo":"16242","materialNumber":"98-0035203-00","currentInventory":"10","reOrderPoint":"51","newCurrentInventory":"","newReOrderPoint":"33"}]';

                SediVueAutoReplenishmentService.SubUpdResp_element[] updResp = new SediVueAutoReplenishmentService.SubUpdResp_element[]{};
                    
                SediVueAutoReplenishmentService.SubUpdResp_element updRespElement = new SediVueAutoReplenishmentService.SubUpdResp_element();
                updRespElement.ShipTo = '99999';
        		updRespElement.prodReference = '99-0000-11';
        		SediVueAutoReplenishmentService.Return_element Return_x = new SediVueAutoReplenishmentService.Return_element();
                Return_x.Type_x = 'A';
                Return_x.Id = '100';
        		Return_x.Number_x = '999';
        		Return_x.Message = 'Test Message';             
                updRespElement.Return_x = Return_x;
                
                controller.setUpdateResponse(updResp);

                SediVueControllerExtension.setData(jsonUpdatePayload, fakeSapID);
                
                SediVueControllerExtension.processUpdateSubscriptionResponse(updResp, fakeSAPID);

                Test.stopTest(); 
                
            }    
    }
    
        static testMethod void UpdateSubscriptionDetailsReOrderPoint() {
    	User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
            system.runAs(currentUser){
                Account thisAccount = new Account();
                thisAccount = CreateTestClassData.createCustomerAccount();
                thisAccount.SAP_Customer_Number__c = '19907';
                
                SediVueControllerExtension controller = new SediVueControllerExtension(new ApexPages.StandardController(thisAccount));
                Test.setCurrentPage(Page.OrderCreation);
                
                system.currentPageReference().getParameters().put('Id', thisAccount.Id);
                system.currentPageReference().getParameters().put('hideBanner', 'true');
    
                String fakeSapID ='0000000001';
                String fakeAccountId = '0017A00000D7IPQ';

				String sampleResponse = '(SubUpdResp_element:[ProdReference=98-0035203-00, ProdReference_type_info=(ProdReference, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), Return_x=Return_element:[Id=ZSB, Id_type_info=(Id, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), Message=Subscription details updated successfully., Message_type_info=(Message, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), Number_x=013, Number_x_type_info=(Number, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), Type_x=S, Type_x_type_info=(Type, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), apex_schema_type_info=(http://idexx.com/sappo/sales/autoreplenishment, false, false), field_order_type_info=(Type_x, Id, Number_x, Message)], Return_x_type_info=(Return, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), ShipTo=0000019907, ShipTo_type_info=(ShipTo, http://idexx.com/sappo/sales/autoreplenishment, null, 0, 1, false), apex_schema_type_info=(http://idexx.com/sappo/sales/autoreplenishment, false, false), field_order_type_info=(ShipTo, ProdReference, Return_x)])';
                Test_ReusableMockResponse fakeResponse = new Test_ReusableMockResponse(200,'SUCCESS',sampleResponse, new map<String,String>{'Content-Type'=>'application/json'});
    
                Test.setMock(HttpCalloutMock.class, fakeResponse);
                
                Test.startTest();
                
                String jsonUpdatePayload = '[{"shipTo":"16242","materialNumber":"98-0035203-00","currentInventory":"10","reOrderPoint":"51","newCurrentInventory":"","newReOrderPoint":"33"}]';

                SediVueControllerExtension.setData(jsonUpdatePayload, fakeSapID);
                
                Test.stopTest(); 

                
            }    
    }
    
	static testMethod void testCreateResultWrapper() {
    	 User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
            system.runAs(currentUser){
                Test.startTest();
                SediVueControllerExtension.SediVueResultWrapper result1 = new SediVueControllerExtension.SediVueResultWrapper();
                result1.shipTo = '000001';
                result1.materialNumber = '98-0004875-00';
                result1.description = 'SEDIVUE 1-200UL PIPETTE TIP';
                result1.currentInventory = '0';
                result1.newCurrentInventory = '1';
                result1.reOrderPoint = '75';
                result1.newReOrderPoint = '85';
                result1.pendingDeliveryDate = '05-24-2016';
                result1.pendingOrderId = '143901975';
                result1.pendingDeliveryNumber = '800004345';
                result1.pendingTrackingCarrier = 'FEDEX';
                result1.pendingTrackingNumber = '999999999999999';
                
                result1.lastDeliveryDate = '01-15-2016';
                result1.lastOrderId = '140123434';
                result1.lastDeliveryNumber = '800002523';
                result1.lastTrackingCarrier = 'UPS';
                result1.lastTrackingNumber = '1Z9999999999999999';

                Test.stopTest();
            }
    }
    
}