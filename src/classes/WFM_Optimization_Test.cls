@isTest
public class WFM_Optimization_Test {
        
    @isTest(SeeAllData=false)
    static void testFillInSchedule(){
         
        WFM_Utils.setUserPermissions([select id from User where id = :UserInfo.getUserId()]);
        System.runAs(new User(Id = UserInfo.getUserId())){
            Test.startTest();
            //FSL__Scheduling_Policy__c spIn=[select Id from FSL__Scheduling_Policy__c where Name='Diagnostic'];
            FSL__Scheduling_Policy__c spIn=new FSL__Scheduling_Policy__c (Name='Diagnostic');
            insert spIn;
            RecordType rT=[select Id from RecordType where DeveloperName='Gap_Rule_Service' limit 1];
            
            FSL__Work_Rule__c workRule= new FSL__Work_Rule__c(
                Name='Active Resources',
                FSL__Working_Location_Enable_Primary__c=False,
                FSL__Service_Time_Property__c='EarliestStartTime',
                FSL__Service_Time_Operator__c='Before',
                FSL__Service_Schedule_Time_Property__c='SchedStartTime',
                FSL__Service_Property__c='ServiceTerritory', 
                FSL__Resource_Property__c ='IsActive',
                FSL__Maximum_Travel_From_Home_Type__c='Distance',
                FSL__Pass_Empty_Values__c=False,
                FSL__Match_Skill_Level__c=True,
                CurrencyIsoCode='USD',
                FSL__Enable_Overtime__c=False,
                FSL__Is_Fixed_Gap__c =False,
                FSL__Match_Constant_Boolean_Value__c =True,
                FSL__Boolean_Operator__c='=',
                FSL__Start_of_Day__c='00:00',
                FSL__Active__c=True,
                RecordTypeId=rT.id);
            insert workRule;
            
            FSL__Scheduling_Policy_Work_Rule__c spWR=new FSL__Scheduling_Policy_Work_Rule__c(FSL__Scheduling_Policy__c=spIn.id,FSL__Work_Rule__c=workRule.id);
            insert spWR;
            
            FSL__Service_Goal__c sG = new FSL__Service_Goal__c(Name='Minimize Travel',FSL__Prioritize_Resource__c='Most Qualified');
            insert sG;
            
            FSL__Scheduling_Policy_Goal__c spSG=new FSL__Scheduling_Policy_Goal__c(FSL__Scheduling_Policy__c=spIn.id,FSL__Service_Goal__c=sG.id,FSL__Weight__c=1);
            insert spSG;
            
            
            List<ServiceResource> srList=[select Id,IsActive from ServiceResource where RelatedRecordId=:UserInfo.getUserId() limit 1];
            ServiceResource sr;
            if (srList.size()==0){
                sr = new ServiceResource();
                sr.Name = UserInfo.getUserName();
                sr.IsActive = true;
                sr.RelatedRecordId = UserInfo.getUserId();
                sr.ResourceType = WFM_Constants.SERVICE_RESOURCE_TYPE_TECHNICIAN;
                sr.FSL__Travel_Speed__c=15;
                sr.IsOptimizationCapable=True;
                insert sr;
            } else {
                sr=srList[0];
                sr.isActive=True;
                sr.ResourceType = WFM_Constants.SERVICE_RESOURCE_TYPE_TECHNICIAN;
                sr.FSL__Travel_Speed__c=15;
                sr.IsOptimizationCapable=True;
                update sr;
                
            }
            
                 
            Datetime dt=Datetime.now().addMonths(-1);
            OperatingHours oH=new OperatingHours(Name='BaseTest', TimeZone='America/New_York');
            insert oH;
            Time current = Time.newInstance(18,0,0,0);
            TimeSlot tS=new TimeSlot(DayOfWeek='Monday',EndTime=current,OperatingHoursId=oH.id,StartTime=current.addHours(-9),Type='Normal');
            insert tS;
            
            List<ServiceTerritoryMember> stmList=[select Id from ServiceTerritoryMember where ServiceResourceId=:sr.Id];
            ServiceTerritoryMember stm;
            
            if (stmList.size()==0){
                ServiceTerritory st;//[select Id from ServiceTerritory where Name='Diagnostic - Home Base'];
                if (st==NULL) {
                    st=new ServiceTerritory(Name='Diagnostic - Home Base',OperatingHoursId=oH.id,IsActive=True);
                    insert st;
                }
                stm=new ServiceTerritoryMember();
                stm.ServiceResourceId=sr.Id;
                stm.EffectiveStartDate=dt.addMonths(-1).addDays(-10);
                stm.ServiceTerritoryId=st.Id;
                insert stm;
            }
            else {
                stm=stmList[0];
            }
            
            
            System.debug('FILLINTEST:'+dt+','+sr.Id+','+spIn.Id);
            FSL.FillInScheduleService.CallFillInBatch(dt, sr.Id, spIn.Id);
            
            Test.stopTest();
        }
    }
    
    
}