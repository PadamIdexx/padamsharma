/* Class Name   : Test_CreateTestData
 * Description  : Test Class with unit test scenarios to cover the Test_CreateTestData class
 * Created By   :  Hemanth Vanapalli
 * Created On   : 09-25-2015 
 
* Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date                   Modification ID       Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Hemanth Vanapalli         09-25-2015            1000                   Initial version
 */
@isTest(seeAllData=false)
private class Test_CreateTestData {
    static testMethod void CreateTestData() {
        Test.startTest();
            CreateTestClassData.createTenderOpportunity();
            Opportunity testOpp = CreateTestClassData.createLPDOpportunity();
            CreateTestClassData.createNegativeWinBackOpportunity();
            CreateTestClassData.insertChildOpportunities(CreateTestClassData.createTenderOpportunity(),10);
            CreateTestClassData.reusableUser('System Administrator', 'Test');
            Account testAccount = CreateTestClassData.createCustomerAccount();
           // CreateTestClassData.reusableOrderHistory(testAccount.Id);
            Contact testContact = CreateTestClassData.reusableContact(testAccount.Id);
            insert testContact;
            Campaign testCampaign = CreateTestClassData.reusableCampaign();
            insert testCampaign;
            CreateTestClassData.reusableCampaignMember(testCampaign.Id, testContact.Id);
            CreateTestClassData.reusableOrder(testOpp.Id, testAccount.Id);
            CreateTestClassData.createAccountType('Customer');
            CreateTestClassData.createAccountTypeWithCurrency('Customer','USD');
            CreateTestClassData.createPartnerFunctions(testAccount,2);
           // CreateTestClassData.createPartnerFunction(testAccount,'CP');
            CreateTestClassData.createMaterialProcessingTestResponse();

        Test.stopTest();
        
    } 
    
    static testMethod void CreateTestData1() {
        Test.startTest();
        CreateTestClassData.createTestUser();    
    }    
    
    
   
}