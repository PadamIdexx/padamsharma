@isTest(SeeAllData=true)
public class Test_StatusEmailHandler {

   @isTest static void testSendObjectContents() {
       Test.startTest();
       
       SAPOrder order = OrderTestDataFactory.createSAPOrder();
       
       //unfortunately, there is no generic way to do this so serializing the object to string by hand
       String jsonOrder = JSON.serialize(order);
       StatusEmailHandler.sendObjectContents('SAPOrder', jsonOrder);
       
       Test.stopTest();			
    }
    
   @isTest static void testSendException() {
       Test.startTest();
       
       try {
           //trigger an exception on purposes
           Decimal num = null;           
           num.setScale(2);
           
       } catch (Exception e) {
           StatusEmailHandler.sendException(e);
       }
       
       Test.stopTest();			
    }    

   @isTest static void testSendExceptionWithObject() {
       Test.startTest();
       
       SAPOrder order = OrderTestDataFactory.createSAPOrder();
       
       try {
           //trigger an exception on purposes
           Decimal num = null;           
           num.setScale(2);
           
       } catch (Exception e) {
	       //unfortunately, there is no generic way to do this so serializing the object to string by hand
    	   String jsonOrder = JSON.serialize(order);           
           StatusEmailHandler.sendExceptionWithObject(e, jsonOrder);
       }
       
       Test.stopTest();			
    }
}