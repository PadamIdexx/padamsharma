global class RefLabTestCodeLookupControllerExtension 
{
    private Account acct{get;set;}
    public Product2 product2 {get;set;} 
    public String LIMS{get;set;}
    public String limsId{get;set;}
    public String repId{get;set;}
    public static List<LIMS_Test__c> labRes{get;set;}
    public static List<LIMS_Test_Component__c> compList{get;set;}
    ApexPages.StandardController con;
    public String errorMsg{get;set;}
    
    public String getLIMS() {
        return LIMS; }
    
    public RefLabTestCodeLookupControllerExtension(ApexPages.StandardController controller) 
    {
        this.con = controller;
        acct = (account)controller.getRecord();  
        Id accId=controller.getId();
        errorMsg = '';
        repId = '';
        
        Reports__c rep = Reports__c.getValues('TestMix');
        try{
            repId = rep.Report_Id__c;
        }
        catch(exception ex){
            system.debug('error fetching report ' + ex.getCause());
            repId = '';
        }
        
    }
    
    
    /* ************************* new pagination mathod create by Rama as part of DE8584************************************ */
    global static WrapperSobject FinalResult {get;set;}
   
    // This method is used for Display Name Filter as we need to use pagination for same
    // So we are sending offset from VF page also
    // Rama:- Added same code for all three filter
    @RemoteAction
    global static RefLabTestCodeLookupControllerExtension.WrapperSobject getResultDisplayName(Integer offsetValue ,String term1, String term2, String term3)
    {
        if (ApexPages.hasMessages()) 
        {
            ApexPages.getMessages().clear();
        }
        
        FinalResult = new WrapperSobject();
        
        if(term1 == 'Display Name')
        {
            LabRes = new List<Lims_Test__c>();
            LabRes = DoSearch(1,term1,term2,term3);
            List<String> TestCodeSplit = term3.split(',');
             
            String wildCard = '%'+term2+'%';
            Integer totalcount = [ select count() from LIMS_Test__c where Test_Name__c like :wildCard  and LIMS__c in :TestCodeSplit  ] ;
            if(totalcount != null && totalcount >0)
            {
                Decimal d = totalcount * 0.10;
                totalcount= Integer.valueOf(d.round(System.RoundingMode.CEILING));
            }
            
            FinalResult.listLabRes = LabRes;
            FinalResult.TotalRowCount = totalcount;
            return FinalResult;
        }
        else if(term1 == 'Test Code')
        {
            List<String> TestCodeSplit = term3.split(',');
            LabRes = new List<Lims_Test__c>();
            LabRes = DoSearch(1,term1,term2,term3);
            Integer totalcount = [ select count() from  LIMS_Test__c  where ( Native_Test_Code__c =:term2 or Test_Code__c =:term2 ) and LIMS__c in :TestCodeSplit  ] ;
            if(totalcount != null && totalcount >0)
            {
                Decimal d = totalcount * 0.10;
                
                totalcount= Integer.valueOf(d.round(System.RoundingMode.CEILING));
            }
            
            FinalResult.listLabRes = LabRes;
            FinalResult.TotalRowCount = totalcount;
            return FinalResult;
        }
        else if(term1 == 'Component Code')
        {
            List<String> CompCodeNew = term2.split(',');
            LabRes = new List<Lims_Test__c>();
            LabRes = DoSearch(1,term1,term2,term3);
            
            
            Integer totalcount = compCount;
                                                    
            if(totalcount != null && totalcount >0)
            {
                Decimal d = totalcount * 0.10;
                
                totalcount= Integer.valueOf(d.round(System.RoundingMode.CEILING));
            }
            
            FinalResult.listLabRes = LabRes;
            FinalResult.TotalRowCount = totalcount;
            return FinalResult;
        }
        return null;
    }

    // Common method for Query 
       
    // Rama:- Added same code for all three filter
    public static List<sObject> DoSearch( Integer offsetValue  , String term1, String term2, String term3)
    {
        Integer offsetCount ;
        LabRes =  new List<Lims_Test__c> ();
        
        if(offsetValue > 1)
        {
              offsetCount = (offsetValue -1 ) * 10;
        }        
        
        if(term1 == 'Display Name')
        {
            String wildCard = '%'+term2+'%';
            List<String> TestCodeSplit = term3.split(',');
            LabRes = new List<Lims_Test__c>();
            String Soql = 'select  LIMS__c, Native_Test_Code__c, Product__r.SAP_MATERIAL_NUMBER__c, Species__c, Product__c, Test_Name__c, Turnaround_Time__c, '+
                                            ' Test_Type__c, Interferences__c, Storage_Stability__c, Submission_Requirements__c, Min_Specimen_Requirements__c, '+
                                            ' Acceptable_Specimens__c, Spec_Protocol__c, Comments__c, Internal_Comments__c, Interpretation_Comments__c, '+
                                            '  Test_Category__c, (select Component_Test_Code__c, Component_Test_Name__c from LIMS_Test_Components__r ) '+
                                            '  from LIMS_Test__c where Test_Name__c like : wildCard  and LIMS__c in:TestCodeSplit  Order by Native_Test_Code__c  ' ;
            if(offsetValue == 1)
            {
                strSoql= Soql + ' Limit 10 ' ;
            }
            else
            {
                strSoql= Soql + ' Limit 10 OFFSET '+string.valueOf(offsetCount);
            }
            LabRes = ( List<Lims_Test__c> ) database.query(strSoql);
        }
        else if(term1 == 'Test Code')
        {
            List<String> TestCodeSplit = term3.split(',');
            String Soql =   'select  LIMS__c, Id,Native_Test_Code__c, Species__c, Product__c,  Product__r.SAP_MATERIAL_NUMBER__c,'+
                            'Test_Name__c, Turnaround_Time__c, Test_Type__c, Interferences__c, Storage_Stability__c, Submission_Requirements__c,'+
                            ' Min_Specimen_Requirements__c, Acceptable_Specimens__c, Spec_Protocol__c, Comments__c, '+
                            'Internal_Comments__c, Interpretation_Comments__c, Test_Category__c,'+
                            '(select Component_Test_Code__c, Component_Test_Name__c from LIMS_Test_Components__r) '+
                            ' from LIMS_Test__c  where ( Native_Test_Code__c =:term2 or Test_Code__c =:term2 ) and LIMS__c in :TestCodeSplit  Order by Native_Test_Code__c ';
        
            if(offsetValue == 1)
            {
                strSoql= Soql + ' Limit 10 ' ;
            }
            else
            {
                strSoql= Soql + ' Limit 10 OFFSET '+string.valueOf(offsetCount);
            }
            LabRes = ( List<Lims_Test__c> ) database.query(strSoql);
        }
        else if ( term1 == 'Component Code' )
        {
            List<String> TestCodeSplit = term3.split(',');
            List<String> CompCodeNew = term2.split(',');
            
            // Query all LIMS_Test_Component__c which code contain search value
            
            compList =  new List<LIMS_Test_Component__c> ([SELECT LIMS_Test__c FROM LIMS_Test_Component__c WHERE  Component_Test_code__c  IN :CompCodeNew and LIMS_Test__r.LIMS__c in :TestCodeSplit Order by LIMS_Test__r.Native_Test_Code__c  ]) ; 
            
            Set<Id> setLIMSTestId = new Set<Id>();
            for(LIMS_Test_Component__c l : compList)
            {
                if(l.LIMS_Test__c != null)
                {
                    setLIMSTestId.add(l.LIMS_Test__c); // get LimsTest id
                }
            }
            
            String Soql =   'select  LIMS__c, Id,Native_Test_Code__c, Species__c, Product__c,  Product__r.SAP_MATERIAL_NUMBER__c,'+
                            'Test_Name__c, Turnaround_Time__c, Test_Type__c, Interferences__c, Storage_Stability__c, Submission_Requirements__c,'+
                            ' Min_Specimen_Requirements__c, Acceptable_Specimens__c, Spec_Protocol__c, Comments__c, '+
                            'Internal_Comments__c, Interpretation_Comments__c, Test_Category__c,'+
                            '(select Component_Test_Code__c, Component_Test_Name__c from LIMS_Test_Components__r WHERE  Component_Test_code__c  IN :CompCodeNew ) '+
                            ' from LIMS_Test__c where Id IN :setLIMSTestId  and LIMS__c in:TestCodeSplit Order by Native_Test_Code__c ';
            
            /*
            if(offsetValue == 1)
            {
                strSoql = Soql + ' Limit 10 ' ;
            }
            else
            {
                strSoql= Soql + ' Limit 10 OFFSET '+string.valueOf(offsetCount);
            }
            */
                
            // Query all    Lims_Test__c record
            
            List<Lims_Test__c> TempLabRes =  ( List<Lims_Test__c> ) database.query(Soql );
            Set<ID> setLimsID = new Set<ID>();
            for( Lims_Test__c lt : TempLabRes )
            {
                List<LIMS_Test_Component__c> lstComp = lt.LIMS_Test_Components__r ;
                // Check LIMS Test have all searched value
                if( lstComp.size() == CompCodeNew.size() )
                {
                    setLimsID.add(lt.id);
                }
            }

            // Check LIMS Test have all searched value the fetch record base on setLimsID
            
            String SoqlFinal =   'select  LIMS__c, Id,Native_Test_Code__c, Species__c, Product__c,  Product__r.SAP_MATERIAL_NUMBER__c,'+
                            'Test_Name__c, Turnaround_Time__c, Test_Type__c, Interferences__c, Storage_Stability__c, Submission_Requirements__c,'+
                            ' Min_Specimen_Requirements__c, Acceptable_Specimens__c, Spec_Protocol__c, Comments__c, '+
                            'Internal_Comments__c, Interpretation_Comments__c, Test_Category__c,'+
                            '( select Component_Test_Code__c, Component_Test_Name__c from LIMS_Test_Components__r ) '+
                            ' from LIMS_Test__c where Id IN :setLimsID  and LIMS__c in:TestCodeSplit Order by Native_Test_Code__c ';

            if(offsetValue == 1)
            {
                strSoql = SoqlFinal + ' Limit 10 ' ;
            }
            else
            {
                strSoql= SoqlFinal + ' Limit 10 OFFSET '+string.valueOf(offsetCount);
            }
            
            LabRes = ( List<Lims_Test__c> ) database.query(strSoql);
            List<Lims_Test__c> compCountLabRes = ( List<Lims_Test__c> ) database.query(SoqlFinal);
            compCount = compCountLabRes.size();
        }
        return LabRes;
    }
    
    Global Static String strSoql ;
    Global Static Integer compCount ;
    
    // Pagination method for Next and previous method with offset
    @RemoteAction
    public Static List<sObject> do_pagination(Integer offsetValue  ,String term1, String term2, String term3) 
    {
        LabRes = new List<Lims_Test__c>();
        LabRes = DoSearch(offsetValue,term1,term2,term3);
        return LabRes;
    }

    // Below Wrapper class used to send total record and result
    global Class WrapperSobject
    {
        public List<sObject> listLabRes {get;set;}
        public Integer TotalRowCount {get;set;} 
    }

/************************************ new pagination END *************************************/
  
    
}