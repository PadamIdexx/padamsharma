@isTest
global class Test_DailyDomainWebserviceCalloutMock implements WebServiceMock {
    
   global void doInvoke(Object stub,Object request,Map<String, Object> response,String endpoint,String soapAction,String requestName,String responseNS,String responseName,String responseType) {
       DailyDomainValues.DT_DailyDomains_Response sampleResponse = new DailyDomainValues.DT_DailyDomains_Response();
       DailyDomainValues.FREE_GOODS_element sampleFreeGoods = new DailyDomainValues.FREE_GOODS_element();
       
       DailyDomainValues.REPS_element[] REPS = new List<DailyDomainValues.REPS_element>();
       DailyDomainValues.COST_CENTERS_element[] COST_CENTERS = new List<DailyDomainValues.COST_CENTERS_element>();
       DailyDomainValues.REASONS_element[] REASONS = new List<DailyDomainValues.REASONS_element>();
       DailyDomainValues.REPS_element agentRep = new DailyDomainValues.REPS_element();
       agentRep.Rep='Test';
       agentRep.Name='Test Name';
       agentRep.DEF_COST_CENTER='Test CC';
       REPS.add(agentRep);
       DailyDomainValues.COST_CENTERS_element ccAgent = new DailyDomainValues.COST_CENTERS_element();
       ccAgent.COST_CENTER = '234';
       ccAgent.Description = 'Test Desc';
       COST_CENTERS.add(ccAgent);
       DailyDomainValues.REASONS_element reason = new DailyDomainValues.REASONS_element();
       reason.REASON= 'NONE';
       reason.DESCRIPTION='TEST DESC';
       REASONS.add(reason);
       sampleFreeGoods.REPS = REPS;
       sampleFreeGoods.COST_CENTERS = COST_CENTERS ;
       sampleFreeGoods.REASONS = REASONS;
       sampleResponse.FREE_GOODS= sampleFreeGoods;
       response.put('response_x', sampleResponse ); 
       System.debug('########'+response.get('response_x'));
   }
}