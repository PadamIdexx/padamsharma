public with sharing class EmailConfirmationControllerLPD {
        
    private Account currentAccountRecord;
    
    public String   shippingSAPId;
    public String 	shippingCountry;
    public String 	shippingStreet;
    public String 	shippingCity;
    public String 	shippingState;
    public String 	shippingPostalCode;
    
    public String 	shippingAdd 			{get;set;}
    public String   soldToAdd               {get;set;}
    public String message    				{get;set;}
    public List<ContactDetails> contacts	{get;set;}
    public ContactDetails selectedContact	{get;set;}
    public boolean isAddContact				{get;set;}
    public String lobString					{get;set;}
    public String soldToString              {get;set;}
    public String initialDepartment         {get;set;}
    public String department                {get;set;}
    
    public String shipTo					{get;set;}
    public String soldTo					{get;set;}
    
    public ContactPersonProcessingResponse.ContactPersonsQuery_Sync fetchresponseShipTo;
    public ContactPersonProcessingResponse.ContactPersonsQuery_Sync fetchresponseSoldTo;
    
    WebServiceUtil callOutsUtil;
    String partnerFunctionId;
    
    public EmailConfirmationControllerLPD(String sapNumber){
        currentAccountRecord= new Account();
        callOutsUtil = new WebServiceUtil();
        currentAccountRecord.SAP_Customer_Number__c=sapNumber;
        fetchresponseShipTo = new ContactPersonProcessingResponse.ContactPersonsQuery_Sync();
        fetchresponseSoldTo = new ContactPersonProcessingResponse.ContactPersonsQuery_Sync();
    }
    
    public EmailConfirmationControllerLPD(ApexPages.StandardController controller) { 
        this.currentAccountRecord = [SELECT id, SAP_Customer_Number__c, SAP_Number_without_Leading_Zeros__c, ShippingCountry, 
                                     ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode
                                     FROM Account 
                                     WHERE id=:controller.getId()];
        callOutsUtil = new WebServiceUtil();
        partnerFunctionId = '';
        message = '';
        selectedContact = new ContactDetails();
        contacts = new List<ContactDetails>();
        fetchresponseShipTo = new ContactPersonProcessingResponse.ContactPersonsQuery_Sync();
        fetchresponseSoldTo = new ContactPersonProcessingResponse.ContactPersonsQuery_Sync();
        
        //Obtain the Partner Function id
        if(ApexPages.CurrentPage().getParameters().containsKey('pf')){
            partnerFunctionId = ApexPages.CurrentPage().getParameters().get('pf');
        }
        //Obtain the Line of Business. Values will be: LPDNA, LPDEMEA, CAG 
        if(ApexPages.CurrentPage().getParameters().containsKey('lob')){
            lobString = ApexPages.CurrentPage().getParameters().get('lob');
        }
        //Obtain the Sold To id, if there is one...
        if(ApexPages.CurrentPage().getParameters().containsKey('soldTo')){
            soldToString = ApexPages.CurrentPage().getParameters().get('soldTo');
        } 
        
 
        if ((currentAccountRecord !=null) && ((partnerFunctionId == '') || (partnerFunctionId == null))) {
            System.debug('We have a currentAccountRecord; but the partnerFunctionId is either a zero-length string or it is null.');
            shippingSAPId       = currentAccountRecord.SAP_Number_without_Leading_Zeros__c;
            shippingCountry     = currentAccountRecord.ShippingCountry;
            shippingStreet      = currentAccountRecord.shippingStreet;
            shippingCity        = currentAccountRecord.shippingCity;
            shippingState       = currentAccountRecord.shippingState;
            shippingPostalCode  = currentAccountRecord.shippingPostalCode;
            shippingAdd         = ' SAP ID  ' + shippingSAPId + ' - ' + shippingStreet + ' ' + shippingCity + ' ' + shippingState + ' ' + shippingCountry + ' ' + shippingPostalCode;
        } else if(currentAccountRecord != null && partnerFunctionId != null && partnerFunctionId != ''){
            System.debug('We have a currentAccountRecord; and the partnerFunctionId is either not a zero length string or it is not null.');
            String pfId = '';
            System.debug('>>>>>> partnerFunctionId = ' + partnerFunctionId); //>>>>>> partnerFunctionId = FRS5:CL:0000060483
            List<String> shipToPartnerFunctionId = partnerFunctionId.split(':');
            if (!shipToPartnerFunctionId.isEmpty()) {
                System.debug('>>>>>> shipToPartnerFunctionId = ' + shipToPartnerFunctionId);
                initialDepartment = shipToPartnerFunctionId[1];
                pfId = shipToPartnerFunctionId[2]; //was referencing [0] position in the array
                shipTo = pfId;
            }
            System.debug('>>>>>> department = ' + department);
            if(pfId == '' || pfId == null) {
                pfId = partnerFunctionId;
            }
            List<Account> partnerAccountSelected = [SELECT Name, SAP_Customer_Number__c, SAP_Number_without_Leading_Zeros__c, ShippingStreet, ShippingCity, ShippingState, ShippingCountry, ShippingPostalCode
                                                    FROM Account 
                                                    WHERE SAP_Customer_Number__c = :pfId];
            if (!partnerAccountSelected.isEmpty()){
                if (partnerAccountSelected[0] != null) {
                    shippingSAPId      = partnerAccountSelected[0].SAP_Number_without_Leading_Zeros__c;
                    shippingCountry    = partnerAccountSelected[0].ShippingCountry;
                    shippingStreet     = partnerAccountSelected[0].shippingStreet;
                    shippingCity       = partnerAccountSelected[0].shippingCity;
                    shippingState      = partnerAccountSelected[0].shippingState;
                    shippingPostalCode = partnerAccountSelected[0].shippingPostalCode;
                    currentAccountRecord.SAP_Customer_Number__c = partnerAccountSelected[0].SAP_Customer_Number__c;
            		currentAccountRecord.ShippingCountry = partnerAccountSelected[0].ShippingCountry;
            		currentAccountRecord.shippingStreet = partnerAccountSelected[0].shippingStreet;
            		currentAccountRecord.shippingCity = partnerAccountSelected[0].shippingCity;
            		currentAccountRecord.shippingState = partnerAccountSelected[0].shippingState;
            		currentAccountRecord.shippingPostalCode = partnerAccountSelected[0].shippingPostalCode;
                	shippingAdd        = ' SAP ID ' + shippingSAPId + ' - ' + partnerAccountSelected[0].Name;
                }
            }
            System.debug('>>>>>> soldToString = ' + soldToString);
            List<Account> soldToAccounts;
            if (soldToString.contains(':')) { //002 Account - obtained from a dropdown list
            	List<String> soldToPartnerFunctionId = soldToString.split(':');
                if (!soldToPartnerFunctionId.isEmpty()) { 
                    soldToString = soldToPartnerFunctionId[2];
                }
                soldToAccounts = [SELECT Name, SAP_Customer_Number__c, SAP_Number_without_Leading_Zeros__c
                                            FROM Account
                                            WHERE SAP_Customer_Number__c = :soldToString];    
            } else { //001 Account - what will be incoming is the Id of the Account
            	soldToAccounts = [SELECT Name, SAP_Customer_Number__c, SAP_Number_without_Leading_Zeros__c
                                            FROM Account
                                            WHERE Id = :soldToString];       
            }
            System.debug('>>>>>> soldToString = ' + soldToString);
            if(!Test.isRunningTest()){
                if ((soldToAccounts != null) && (soldToAccounts[0] != null)) {
                    soldToAdd         = ' SAP ID ' + soldToAccounts[0].SAP_Number_without_Leading_Zeros__c + ' - ' +  soldToAccounts[0].Name;
                    soldTo = soldToAccounts[0].SAP_Customer_Number__c; 
                }
            }

        }
        
    }
    
    public void fetchDetailsFromSAP(){
        try {
            boolean wroteErrors = false; 
            message = '';
            contacts = new List<ContactDetails>();
            List<String> testCP = new List<String>();
           
            //*** First time processing - SHIP TO account:
            ContactPersonProcessingRequest.ContactPersonQuery_Sync requestShipTo = new ContactPersonProcessingRequest.ContactPersonQuery_Sync();
            requestShipTo.CustomerID = shipTo; //was currentAccountRecord.SAP_Customer_Number__c;
            requestShipTo.ContactTypes = testCP;
            if(!Test.isRunningTest()){
            	fetchresponseShipTo = callOutsUtil.contactPersonReadCall(requestShipTo);   
            } 
            system.debug('in fetchDetailsFromSAP...fetchresponseShipTo = ' + fetchresponseShipTo);  
            if (fetchresponseShipTo != null) {
                if (fetchresponseShipTo.ContactPerson != null) { 
                    processContactDetailsFromSAP(initialDepartment, shipTo, 'SHIPTO', fetchresponseShipTo);
                } else { // no contact person found in SAP
                    message += 'No Contact details found for this Customer in SAP';
                    wroteErrors = true;
                }
            } else { // no response from SAP - might be due to an exception/error
                message += 'No response received from SAP';
                wroteErrors = true;
            } 
            
            //*** Second time processing - SOLD TO account:
            ContactPersonProcessingRequest.ContactPersonQuery_Sync requestSoldTo = new ContactPersonProcessingRequest.ContactPersonQuery_Sync();
            requestSoldTo.CustomerID = soldTo;
            requestSoldTo.ContactTypes = testCP;
            if(!Test.isRunningTest()){
            	fetchresponseSoldTo = callOutsUtil.contactPersonReadCall(requestSoldTo);   
            }              
            if (fetchresponseSoldTo != null) {
                if (fetchresponseSoldTo.ContactPerson != null) { 
                    processContactDetailsFromSAP(initialDepartment, soldTo, 'SOLDTO', fetchresponseSoldTo);
                } else { // no contact person found in SAP
                    if (!wroteErrors) {
                        message += 'No Contact details found for this Customer in SAP';
                        wroteErrors = true;
                    }
                }
            } else { // no response from SAP - might be due to an exception/error
                if (!wroteErrors) {
                    message += 'No response received from SAP';
                    wroteErrors = true;
                }
            }
            
        } catch(Exception ex) {
           message += 'Exception: ' + ex.getMessage();
        }
    }
    
    private void processContactDetailsFromSAP(String department, String filterSAPId, String mode, ContactPersonProcessingResponse.ContactPersonsQuery_Sync response) {
    
        try {
            for (ContactPersonProcessingResponse.ContactPerson cp : response.ContactPerson) {
                ContactDetails cd = new ContactDetails();
                cd.FirstName = cp.FirstName;
                cd.LastName = cp.LastName;
                cd.Department = cp.Department;
                cd.PartnerID = cp.PartnerID;
                cd.CustomerID = cp.CustomerID;
                cd.ContactType = cp.ContactType;
                cd.isEdit = false;
                if (cp.Communications.Emails == null) { // if there is no existing email found from SAP
                    ContactPersonProcessingResponse.Emails_element emailElement = new ContactPersonProcessingResponse.Emails_element();
                    cp.Communications.Emails = emailElement;
                    ContactPersonProcessingResponse.Default_element defaultElement = new ContactPersonProcessingResponse.Default_element();
                    cp.Communications.Emails.Default_x = defaultElement;
                    cp.Communications.Emails.Default_x.Email = '';
                    cd.Email = '';
                } else { // when details are retreived
                    cd.Email = cp.Communications.Emails.Default_x.Email;
                    cd.isDeleted = Boolean.ValueOf(cp.Communications.Emails.Default_x.donotuse); 
                }
                if (!cd.isDeleted) {    
                    if (lobString != null) { 
                        if (lobString.equalsIgnoreCase('CAG')) {
                            if ((cd.Department != null) && 
                                ((cd.Department == 'OWNR') || (cd.Department == 'PTNR') || (cd.Department == 'PMGR') || 
                                 (cd.Department == 'CP')   || (cd.Department == 'VC')   || (cd.Department == 'VR')   || (cd.Department == 'VS'))) {
                                     contacts.add(cd);   
                                 }
                        }
                        if (lobString.equalsIgnoreCase('LPDNA')) {
                            if ((cd.Department != null) && 
                                ((cd.Department == 'OWNR') || (cd.Department == 'PTNR') || (cd.Department == 'PMGR') || 
                                 (cd.Department == 'CE')   || (cd.Department == 'CF')   || (cd.Department == 'CL'))) {                                   
                                     //if ((cd.ContactType != null) && ((cd.ContactType == 'CO') || (cd.ContactType == 'CD'))) {
                                     if (cd.ContactType != null) {    
                                         if (mode.equalsIgnoreCase('SHIPTO')) {   
                                             if ((cd.ContactType == 'CD') && (cp.CustomerId == filterSAPId)) {
                                                 cd.EmailPreferenceDesc = 'Shipping Confirmation';
                                                 contacts.add(cd); 
                                             }
                                         }
                                         if (mode.equalsIgnoreCase('SOLDTO')) {    
                                             if ((cd.ContactType == 'CO') && (cp.CustomerId == filterSAPId)) {
                                                 cd.EmailPreferenceDesc = 'Order Confirmation';
                                                 contacts.add(cd); 
                                             }
                                         }
                                     } //LPDNA - CD/CO processing  
                                 } //LPDNA - Department display rules
                        } //LPDNA
                        if (lobString.equalsIgnoreCase('LPDEMEA')) {
                            if ((cd.Department != null) && 
                                ((cd.Department == 'OWNR') || (cd.Department == 'PTNR') || (cd.Department == 'PMGR') || 
                                 (cd.Department == 'CE')   || (cd.Department == 'CF')   || (cd.Department == 'CL')   || (cd.Department == 'CP'))) {
                                 	if (cd.ContactType != null) {    
                                         if (mode.equalsIgnoreCase('SHIPTO')) {   
                                             if ((cd.ContactType == 'CD') && (cp.CustomerId == filterSAPId)) {
                                                 cd.EmailPreferenceDesc = 'Shipping Confirmation';
                                                 contacts.add(cd); 
                                             }
                                         }
                                         if (mode.equalsIgnoreCase('SOLDTO')) {    
                                             if ((cd.ContactType == 'CO') && (cp.CustomerId == filterSAPId)) {
                                                 cd.EmailPreferenceDesc = 'Order Confirmation';
                                                 contacts.add(cd); 
                                             }
                                         }
                                     } //LPDNA - CD/CO processing      
                                 } //LPDEMEA - Department display rules
                        } //LPDEMEA 
                        
                    } //lobString != null check...  
                } //!cd.isDeleted check...
            } //for - iteration of ContactPerson...
            
    
        } catch(Exception ex) {
           message += 'Exception - in processContactDetailsFromSAP: ' + ex.getMessage();
        }
        
    }
    
    public void add(){
		//System.debug('>>> in add. department = ' + department);        
        message = '';
        if ((selectedContact.FirstName != null) && (selectedContact.LastName != null) && (selectedContact.Email != null) &&
            (selectedContact.FirstName != '')   && (selectedContact.LastName != '')   && (selectedContact.Email != '')) {
               ContactPersonProcessingResponse.CommunicationsLite cLite = new ContactPersonProcessingResponse.CommunicationsLite();
               ContactPersonProcessingResponse.Emails_element emailElement = new ContactPersonProcessingResponse.Emails_element();
               ContactPersonProcessingResponse.Default_element defaultElement = new ContactPersonProcessingResponse.Default_element();
               defaultElement.Email = selectedContact.Email;
               emailElement.Default_x = defaultElement;
               cLite.Emails = emailElement;
   
               ContactPersonProcessingRequest.ContactPersonCreate_Sync request = new ContactPersonProcessingRequest.ContactPersonCreate_Sync();
               
               request.ContactType = selectedContact.ContactType;
               request.Department = department; //selectedContact.Department;
               if (selectedContact.ContactType.equalsIgnoreCase('CD')) { //Shipping Confirmation
               		request.CustomerID = shipTo;    
               }
               if (selectedContact.ContactType.equalsIgnoreCase('CO')) { //Order Confirmation
               		request.CustomerID = soldTo;     
               }
               //request.CustomerID = currentAccountRecord.SAP_Customer_Number__c;
               request.Lastname = selectedContact.LastName;
               request.Firstname = selectedContact.FirstName;
               request.Sex = '';
               request.Title = '';
               request.Language = '';
               request.LanguageISO = '';
               request.Communications = cLite;
               
               ContactPersonProcessingResponse.MessagesReturn response = new ContactPersonProcessingResponse.MessagesReturn();
               //ContactPersonProcessingService.ContactPersonProcessing_OutBindingQSPort   service = new ContactPersonProcessingService.ContactPersonProcessing_OutBindingQSPort();
               //response = service.ContactPersonCreate(request);
			   //System.debug('>>>>>> add method - request ' + request);               
               response = callOutsUtil.contactPersonAddCall(request);
               System.debug('&&& add response '+response);
               if (response != null) {
                   if (response.isError == false) { // update saved successfully
                       message = 'Contact added successfully in SAP';
                       contacts.add(selectedContact);
                       isAddContact = false;
                   } else {
                       message = 'Contact add Failed';
                   }
               } else {
                   message = 'No response received from SAP';
               }
           } else {
           	  message = 'Please enter First Name, Last Name and Email for the Contact to be added. ';
           }      
    }

    
    public void save() {
        message = '';
        List<ContactDetails> dts = new List<ContactDetails>();
        dts.add(selectedContact);
        if(selectedContact.Email != null && selectedContact.Email != ''){
            ContactPersonProcessingResponse.ContactPerson cp = new ContactPersonProcessingResponse.ContactPerson();
            cp.PartnerID   = selectedContact.PartnerID;
            cp.CustomerID  = selectedContact.CustomerID;
            cp.LastName    = selectedContact.LastName;
            cp.FirstName   = selectedContact.FirstName;
            cp.ContactType = selectedContact.ContactType;
            cp.Department  = selectedContact.Department;
            
            ContactPersonProcessingResponse.CommunicationsFull cFull = new ContactPersonProcessingResponse.CommunicationsFull();
            ContactPersonProcessingResponse.Emails_element emailElement = new ContactPersonProcessingResponse.Emails_element();
            ContactPersonProcessingResponse.Default_element defaultElement = new ContactPersonProcessingResponse.Default_element();
            defaultElement.Email = selectedContact.Email;
            emailElement.Default_x = defaultElement;
            cFull.Emails = emailElement;
            
            cp.Communications = cFull;
            
            ContactPErsonProcessingRequest.ContactPersonUpdate_Sync request = new ContactPErsonProcessingRequest.ContactPersonUpdate_Sync();
            request.PartnerID   = cp.PartnerID;
            request.CustomerID  = cp.CustomerID;
            request.ContactType = cp.ContactType;
            request.Department  = cp.Department;
            request.Lastname    = cp.LastName;
            request.Firstname   = cp.FirstName;
            request.Sex = '';
            request.Title = '';
            request.Language = '';
            request.LanguageISO = '';
            request.Communications = cp.Communications;
            
            ContactPersonProcessingResponse.MessagesReturn response = new ContactPersonProcessingResponse.MessagesReturn();    
            response = callOutsUtil.contactPersonUpdateCall(request);
            System.debug('&&& Update response '+response);
            if (response != null) {    
                if (response.isError == false) { // update saved successfully
                    message = 'Contact updated successfully in SAP';
                    selectedContact.isEdit = false;
                } else {
                    message = 'Contact update Failed';
                }
            } else {
                message = 'No response received from SAP';
            }
        } else {
            message = 'Please enter an Email address';
        }
    }
    
    public void deleteContact() {
        String partId =  ApexPages.currentPage().getParameters().get('partnerId');
        if(partId != null){
            for(ContactDetails cd : contacts){
                if(cd.PartnerId == partId){
                    selectedContact = cd;
                }
            }
        }
        message = '';
        ContactPersonProcessingResponse.ContactPerson cp = new ContactPersonProcessingResponse.ContactPerson();
        cp.PartnerID = partId;
        cp.CustomerID = currentAccountRecord.SAP_Customer_Number__c;
        cp.LastName = selectedContact.LastName;
        cp.FirstName = selectedContact.FirstName;
        cp.ContactType = '';
        
        ContactPersonProcessingResponse.CommunicationsFull cFull = new ContactPersonProcessingResponse.CommunicationsFull();
        ContactPersonProcessingResponse.Emails_element emailElement = new ContactPersonProcessingResponse.Emails_element();
        ContactPersonProcessingResponse.Default_element defaultElement = new ContactPersonProcessingResponse.Default_element();
        defaultElement.Email = selectedContact.Email==null? '' : selectedContact.Email;
        defaultElement.donotUse = 'true';
        emailElement.Default_x = defaultElement;
        cFull.Emails = emailElement;
        
        cp.Communications = cFull;
        
        ContactPersonProcessingRequest.ContactPersonUpdate_Sync request = new ContactPersonProcessingRequest.ContactPersonUpdate_Sync();
        request.PartnerID = cp.PartnerID;
        request.CustomerID = cp.CustomerID;
        request.ContactType = cp.ContactType;
        request.ContactType = '';
        request.Department = cp.Department==null ? '':cp.Department;
        request.Lastname = cp.LastName;
        request.Firstname = cp.FirstName;
        request.Sex = '';
        request.Title = '';
        request.Language = '';
        request.LanguageISO = '';
        request.Communications = cp.Communications;
        
        ContactPersonProcessingResponse.MessagesReturn response = new ContactPersonProcessingResponse.MessagesReturn();    
        response = callOutsUtil.contactPersonUpdateCall(request);
        System.debug('>>>>>> Update response - in deleteContact: '+ response);
        if (response != null) {    
            if (response.isError == false) { // update saved successfully
                message = 'Contact deleted successfully in SAP';
                for(INTEGER i = 0 ; i<contacts.size();i++){
                    if(partId == contacts[i].PartnerID){
                        contacts.remove(i);
                    }  
                }   
            } else {
                message = 'Contact deletion Failed';
            } 
        } else {
            message = 'No response received from SAP';
        }
    } 
        
    
    public class ContactDetails {
        public String FirstName				{get;set;}
        public String LastName				{get;set;}
        public String Department			{get;set;}
        public String Email					{get;set;}
        public String PartnerID				{get;set;}
        public String CustomerID			{get;set;}
        public String ContactType			{get;set;}
        public String EmailPreferenceDesc	{get;set;}
        public boolean isEdit				{get;set;}
        public boolean isDeleted			{get;set;}
        
        public ContactDetails(){
            isDeleted = false;
        }
    }
    
    public void selectContact() {
        String partId =  ApexPages.currentPage().getParameters().get('partnerId');
        if (partId != null) {
            for (ContactDetails cd : contacts) {
                if (cd.PartnerId == partId) {
                    cd.IsEdit = true;
                    selectedContact = cd;
                    isAddContact = false;
                }
            }
        }
    }
    
    public void isAddContactMethod() {
        isAddContact = true;
        selectedContact = new ContactDetails();
        //selectedContact.Department = 'CP';
        if (selectedContact != null) {
        	selectedContact.IsEdit = false;
        }
        message ='';
    }
    

}