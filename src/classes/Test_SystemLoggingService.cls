@isTest
public with sharing class Test_SystemLoggingService {
	
	static testMethod void logRequestResponsePositive() {
		User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
		
		system.runAs(adminUser){
			CreateTestClassData.insertSwitch(userinfo.getProfileId(),true);
			List<String> integrationLogs = new List<String>();
			Test.startTest();
				 integrationLogs.add(SystemLoggingService.concatenateFields('Integration_Request', 'Test_SystemLoggingService', 'logRequestResponsePositive', 'Test','Test'));
            	 integrationLogs.add(SystemLoggingService.concatenateFields('Integration_Response', 'Test_SystemLoggingService', 'logRequestResponsePositive', 'Test', 'Test'));
            	 SystemLoggingService.logRequestResponse(integrationLogs);
            	 SystemLoggingService.logRequestResponse(integrationLogs,true);
			Test.stopTest();
		}
	}
	
	static testMethod void logRequestResponseNegative() {
		User adminUser = CreateTestClassData.reusableUser('Marketing','Idexx');
		Profile adminProfile = [Select id, name from Profile where Name = :'System Administrator'];
		system.runAs(adminUser){
			List<String> integrationLogs = new List<String>();
			CreateTestClassData.insertSwitch(adminProfile.Id,true);
			Test.startTest();
				 integrationLogs.add(SystemLoggingService.concatenateFields('Integration_Request', 'Test_SystemLoggingService', 'logRequestResponsePositive', 'Test','Test'));
            	 integrationLogs.add(SystemLoggingService.concatenateFields('Integration_Response', 'Test_SystemLoggingService', 'logRequestResponsePositive', 'Test', 'Test'));
            	 SystemLoggingService.logRequestResponse(integrationLogs);
			Test.stopTest();
		}
	}
	static testMethod void attachRequestBodyTest(){
		User adminUser = CreateTestClassData.reusableUser('Marketing','Idexx');
		Profile adminProfile = [Select id, name from Profile where Name = :'System Administrator'];
		system.runAs(adminUser){
			List<String> integrationLogs = new List<String>();
			CreateTestClassData.insertSwitch(adminProfile.Id,true);
			Test.startTest();
				Integration_Log__c log = CreateTestClassData.reusableLogs();
				insert log;
				SystemLoggingService.attachRequestBody('Test Request Body', log.Id);
				SystemLoggingService.ValidateInsertion();
			Test.stopTest();
		}
	}
	static testMethod void logRequestResponseForBatchTest(){
		User adminUser = CreateTestClassData.reusableUser('Marketing','Idexx');
		Profile adminProfile = [Select id, name from Profile where Name = :'System Administrator'];
		system.runAs(adminUser){
			List<String> integrationLogs = new List<String>();
			integrationLogs.add(SystemLoggingService.concatenateFields('Integration_Request', 'Test_SystemLoggingService', 'logRequestResponsePositive', 'Test','Test'));
			CreateTestClassData.insertSwitch(adminProfile.Id,true);
			Test.startTest();
				SystemLoggingService.logRequestResponseForBatch(integrationLogs);
			Test.stopTest();
		}
	}
	
	/*static testMethod void ExceptionLogPostive(){
		User adminUser = CreateTestClassData.reusableUser('Marketing','Idexx');
		Profile adminProfile = [Select id, name from Profile where Name = :'System Administrator'];
		system.runAs(adminUser){
			Test.startTest();
			try{
				Account testAcc = [Select Id, Name from Account Limit 1];
			}catch(Exception ex){
				SystemLoggingService.log(ex);
				SystemLoggingService.FlushLogCache();
				SystemLoggingService.flushable = true;
				SystemLoggingService.log(ex);
			}
			Test.StopTest();
		}
	}*/
}