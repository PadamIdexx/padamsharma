/**************************************************************************************
* Apex Class Name: Test_ProductSearchLPD
*
* Description: Test class for the ProductSearchLPD callout
*
*                   
* Developer           Date              Description
* ------------------------------------------------------------------------------------                 
* Heather Kinney      8.9.2016         Original Version
*
*************************************************************************************/

@isTest
public class Test_ProductSearchLPD {
    
    static testMethod void testGetSubscriptionDetailsNoReturnInfo() {
        User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
        system.runAs(currentUser){
            Account thisAccount = new Account();
            thisAccount = CreateTestClassData.createCustomerAccount();
            thisAccount.SAP_Customer_Number__c = '1098';
            
            ProductSearch controller = new ProductSearch(new ApexPages.StandardController(thisAccount));
            ProductSearch ps = new ProductSearch();
            
            String searchTerm = 'SNAP';
            Id pricebookId = Test.getStandardPricebookId();
            String accountISOCode = 'USD';
            
            Productsearch.constructQuery(pricebookId, searchTerm, accountISOCode);
            List<ProductSearch.ProductSearchResultsUIData> listResults = ProductSearch.getCMSSearchResults(searchTerm, pricebookId, accountISOCode);
     
            List<Product2> productList = new List<Product2>();
            for (Integer i = 1; i <= 10; i++) {
                Product2 tempProduct = new Product2();
                tempProduct.Name = 'SNAP PRO STARTER KIT';
                tempProduct.CMS_Product_Keywords__c = 'keyword test ' + tempProduct.Name;
                tempProduct.CMS_Product_Description_Long__c = 'Long CMS product description test.';
                tempProduct.Family = 'test' + i;
                tempProduct.SAP_MATERIAL_NUMBER__c = '22345' + i;
                tempProduct.Include_In_Search__c = true;
                tempProduct.IsActive = true;   
                productList.add(tempProduct);
            }
            insert productList;
            List<PricebookEntry> standardPricebookEntryList = new List<PricebookEntry> ();
            for (Product2 product2Item: productList) {
                System.debug('Processing product2Item = ' + product2Item);
                standardPricebookEntryList.add(
                    new PricebookEntry(Product2Id = product2Item.Id, //was TempProd.Id
                                       Pricebook2Id = priceBookId, 
                                       UnitPrice = 1000,
                                       IsActive = true,
                                       CurrencyIsoCode = accountISOCode,
                                       PriceBook_SAPMatNr__c=String.valueOf(Math.random()).subString(10)));
            }
            insert standardPricebookEntryList;
            ProductSearch.generateProductSearchResults(standardPricebookEntryList); 
        }
    }
    
    static testMethod void testSearchLPDEMEA() {
        User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
        system.runAs(currentUser){
            Account thisAccount = new Account();
            thisAccount = CreateTestClassData.createCustomerAccount();
            thisAccount.SAP_Customer_Number__c = '1098';
            
            ProductSearch controller = new ProductSearch(new ApexPages.StandardController(thisAccount));
            ProductSearch ps = new ProductSearch();
            
            String searchTerm = 'SNAP';
            Id pricebookId = Test.getStandardPricebookId();
            String accountISOCode = 'USD';
            
            Productsearch.constructQuery(pricebookId, searchTerm, accountISOCode);
            List<ProductSearch.ProductSearchResultsUIData> listResults = ProductSearch.getCMSSearchResultsLPDEMEA(searchTerm, pricebookId, accountISOCode);
     
            List<Product2> productList = new List<Product2>();
            for (Integer i = 1; i <= 10; i++) {
                Product2 tempProduct = new Product2();
                tempProduct.Name = 'SNAP PRO STARTER KIT';
                tempProduct.CMS_Product_Keywords__c = 'keyword test ' + tempProduct.Name;
                tempProduct.CMS_Product_Description_Long__c = 'Long CMS product description test.';
                tempProduct.Family = 'test' + i;
                tempProduct.SAP_MATERIAL_NUMBER__c = '22345' + i;
                tempProduct.Include_In_Search__c = true;
                tempProduct.IsActive = true;   
                productList.add(tempProduct);
            }
            insert productList;
            List<PricebookEntry> standardPricebookEntryList = new List<PricebookEntry> ();
            for (Product2 product2Item: productList) {
                System.debug('Processing product2Item = ' + product2Item);
                standardPricebookEntryList.add(
                    new PricebookEntry(Product2Id = product2Item.Id, //was TempProd.Id
                                       Pricebook2Id = priceBookId, 
                                       UnitPrice = 1000,
                                       IsActive = true,
                                       CurrencyIsoCode = accountISOCode,
                                       PriceBook_SAPMatNr__c=String.valueOf(Math.random()).subString(10)));
            }
            insert standardPricebookEntryList;
            ProductSearch.generateProductSearchResults(standardPricebookEntryList);    
        }
    }

    static testMethod void testSearchLPDNA() {
        User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
        system.runAs(currentUser){
            Account thisAccount = new Account();
            thisAccount = CreateTestClassData.createCustomerAccount();
            thisAccount.SAP_Customer_Number__c = '1098';
            
            ProductSearch controller = new ProductSearch(new ApexPages.StandardController(thisAccount));
            ProductSearch ps = new ProductSearch();
            
            String searchTerm = 'SNAP';
            Id pricebookId = Test.getStandardPricebookId();
            String accountISOCode = 'USD';
            
            Productsearch.constructQuery(pricebookId, searchTerm, accountISOCode);
            List<ProductSearch.ProductSearchResultsUIData> listResults = ProductSearch.getCMSSearchResultsLPDNA(searchTerm, pricebookId, accountISOCode);
     
            List<Product2> productList = new List<Product2>();
            for (Integer i = 1; i <= 10; i++) {
                Product2 tempProduct = new Product2();
                tempProduct.Name = 'SNAP PRO STARTER KIT';
                tempProduct.CMS_Product_Keywords__c = 'keyword test ' + tempProduct.Name;
                tempProduct.CMS_Product_Description_Long__c = 'Long CMS product description test.';
                tempProduct.Family = 'test' + i;
                tempProduct.SAP_MATERIAL_NUMBER__c = '22345' + i;
                tempProduct.Include_In_Search__c = true;
                tempProduct.IsActive = true;   
                productList.add(tempProduct);
            }
            insert productList;
            List<PricebookEntry> standardPricebookEntryList = new List<PricebookEntry> ();
            for (Product2 product2Item: productList) {
                System.debug('Processing product2Item = ' + product2Item);
                standardPricebookEntryList.add(
                    new PricebookEntry(Product2Id = product2Item.Id, //was TempProd.Id
                                       Pricebook2Id = priceBookId, 
                                       UnitPrice = 1000,
                                       IsActive = true,
                                       CurrencyIsoCode = accountISOCode,
                                       PriceBook_SAPMatNr__c=String.valueOf(Math.random()).subString(10)));
            }
            insert standardPricebookEntryList;
            ProductSearch.generateProductSearchResults(standardPricebookEntryList);    
        }
    }
    
        static testMethod void testSearchCAG() {
        User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
        system.runAs(currentUser){
            Account thisAccount = new Account();
            thisAccount = CreateTestClassData.createCustomerAccount();
            thisAccount.SAP_Customer_Number__c = '16242';
            
            ProductSearch controller = new ProductSearch(new ApexPages.StandardController(thisAccount));
            ProductSearch ps = new ProductSearch();
            
            String searchTerm = 'SNAP';
            Id pricebookId = Test.getStandardPricebookId();
            String accountISOCode = 'USD';
            
            Productsearch.constructQuery(pricebookId, searchTerm, accountISOCode);
            List<ProductSearch.ProductSearchResultsUIData> listResults = ProductSearch.getCMSSearchResultsCAG(searchTerm, pricebookId, accountISOCode);
     
            List<Product2> productList = new List<Product2>();
            for (Integer i = 1; i <= 10; i++) {
                Product2 tempProduct = new Product2();
                tempProduct.Name = 'SNAP PRO STARTER KIT';
                tempProduct.CMS_Product_Keywords__c = 'keyword test ' + tempProduct.Name;
                tempProduct.CMS_Product_Description_Long__c = 'Long CMS product description test.';
                tempProduct.Family = 'test' + i;
                tempProduct.SAP_MATERIAL_NUMBER__c = '22345' + i;
                tempProduct.Include_In_Search__c = true;
                tempProduct.IsActive = true;   
                productList.add(tempProduct);
            }
            insert productList;
            List<PricebookEntry> standardPricebookEntryList = new List<PricebookEntry> ();
            for (Product2 product2Item: productList) {
                System.debug('Processing product2Item = ' + product2Item);
                standardPricebookEntryList.add(
                    new PricebookEntry(Product2Id = product2Item.Id, //was TempProd.Id
                                       Pricebook2Id = priceBookId, 
                                       UnitPrice = 1000,
                                       IsActive = true,
                                       CurrencyIsoCode = accountISOCode,
                                       PriceBook_SAPMatNr__c=String.valueOf(Math.random()).subString(10)));
            }
            insert standardPricebookEntryList;
            ProductSearch.generateProductSearchResults(standardPricebookEntryList);    
        }
    }
    
}