/* Class Name  : SalesOrderDelete
* Description : Controller class to trigger order deletion based on order reference number available on Order
* Created By  : Anudeep Gopagoni
* Created On  : 11-17-2015
*
* Modification Log:  
* --------------------------------------------------------------------------------------------------------------------------------------
* Developer              Date                   Modification ID       Description 
* ---------------------------------------------------------------------------------------------------------------------------------------
* Anudeep Gopagoni      11-17-2015               n/a                   Initial Version
* Anudeep Gopagoni      15-02-2016               10001                 Removed debug statements and added comments
*                                          
*/
global class SalesOrderDelete {
    
    
    Public static List<Order> ordList = new List<Order>(); 
    Public static List<Order> ordId = new List<Order>(); 
    Public static List<Order> ordId1 = new List<Order>();
    
    //just for grins - trying a @RemoteAction
    //H.Kinney 9.9.2016
    @RemoteAction
    public static String deleteSalesOrder(String odId) {
    	return calloutMethod(odId);    
    }
    
    
    //Declaring this as WebService as this method will be invoked from Delete Order button
    webService static String calloutMethod(String odId) {
        String MessageType = ''; 
        
        ordId1 = [Select OrderReferenceNumber FROM Order where Id=:odId LIMIT 1];         
        String orderRefNumber = ordId1[0].OrderReferenceNumber;
        
        //This is likey a draft or failed to submit to SAP order.   Therefore, it cannot be deleted.
        //Return 'Failure'
        if (String.isBlank(orderRefNumber)) {
            messageType = 'Failure';
            System.debug('Order does not have an SAP order Id and cannot be deleted.');
            return messageType;
        }

		SAPOrder order = new SAPOrder();
        order.documentNumber = orderRefNumber;
        order.language = 'EN'; 
        
        try{        
            //check for delivery
            OrderDataAccessService dao = new OrderDataAccessService();
            OrderStatus status = dao.getStatusOfOrder(orderRefNumber);
            
            if (!Test.isRunningTest()) { //3.10.2017 HLK added to improve code coverage
                if (status.delivered) {
                    messageType = 'Delivered';
                    System.debug('Order has a delivery and cannot be deleted.');
                    return messageType;                
                }
            }
            
            if (!status.submittedToSAP) {
                messageType = 'NotFound';
                System.debug('Order not found in SAP and cannot be deleted.');
                return messageType;                
            }
            
            //attempt delete                                    
            order = OrderManager.deleteOrder(order);
            
            if (Test.isRunningTest()) {  //3.10.2017 - HLK added: mock up a returns list while in test...
                SAPOrder.Return_x retMsg = new SAPOrder.Return_x();
                retMsg.messageType = 'S';
                List<SAPOrder.Return_x> retMsgs = new List<SAPOrder.Return_x>();
                retMsgs.add(retMsg);
                order.returns = retMsgs;
            }
            
                                                
        }catch(Exception e) {
            messageType = 'Failure';
            SystemLoggingService.log(e);
			return messageType;
        }
       
        if (null != order.returns) {
            //parse the response for a success
            for (SAPOrder.Return_x ret : order.returns) {
                if (ret.messageType.equals('S')) {
                    messageType = 'Success'; 
                    break;
                } else {
                    messageType = 'Failure';
                }
            }
        } else {
            //this should never happen because we would have hit another failure scenario first
            //adding it just in case something really weird happens.
            messageType = 'Failure';
        }
               
        if (String.isNotBlank(messageType) 
            && messageType.equals('Success')) {
            updateOrder(odId);
        }
               
        return MessageType;
    }
    //Updates the 'Status' of order to 'Deleted' once the order is deleted in SAP
    @TestVisible
    private static void UpdateOrder(String oId)
    { 
        ordList = [SELECT Status, Id FROM Order Where Id =: oId];
        for(Order ord :ordList)
        {  
            ord.Status = 'Deleted'; 
            ordId.add(ord); 
        }
        update ordId; 
        
    }
}