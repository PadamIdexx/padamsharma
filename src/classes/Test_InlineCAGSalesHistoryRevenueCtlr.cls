/**
 * @author Aditya Sangawar
 * @date   22/12/2015
 * @description   Test Class for covering the Customer Sales History 'REVENUE' Visual force controller.
 Modification Log:
 ------------------------------------------------------------------------------------
 Developer                       Date                Description
 ------------------------------------------------------------------------------------
 Aditya Sangawar                 06/01/2015          Created

**/

@isTest(SeeAllData = False)
public With Sharing class Test_InlineCAGSalesHistoryRevenueCtlr {

    // works as create scenario
    static testMethod void Test_CreateSalesHistoryRevenue() {

        Account acc2 = CreateTestClassData.createCustomerAccount();
        acc2.SAP_Customer_Number__c = 'SAP1234567';
        update acc2;

        Integer i = 1;
        List<Customer_Sales_History__c> lstCSH =
            new List<Customer_Sales_History__c>();
        Customer_Sales_History__c item =
            CreateTestClassData.CreateSalesHistory(acc2.Id,
                                                   'SAP1234567',
                                                   'IDEXXKEY1234' + (i++),
                                                   AbstractSalesHistoryController.PF_CAG_SUMMARY,
                                                   AbstractSalesHistoryController.PG_RAPID_ASSAY,
                                                   'IDEXXProductLine',
                                                   'IDEXXProductName',
                                                   'IDEXXProductType',
                                                   false);
        lstCSH.add(item);

        item =
            CreateTestClassData.CreateSalesHistory(acc2.Id,
                                                   'SAP1234567',
                                                   'IDEXXKEY1234' + (i++),
                                                   AbstractSalesHistoryController.PF_CAG_SUMMARY,
                                                   AbstractSalesHistoryController.PG_CHEMISTRY,
                                                   'IDEXXProductLine',
                                                   'IDEXXProductName',
                                                   'IDEXXProductType',
                                                   false);
        lstCSH.add(item);

        item =
            CreateTestClassData.CreateSalesHistory(acc2.Id,
                                                   'SAP1234567',
                                                   'IDEXXKEY1234' + (i++),
                                                   AbstractSalesHistoryController.PF_CAG_SUMMARY,
                                                   AbstractSalesHistoryController.PG_REFERENCE_LAB_SUPPLIES,
                                                   'IDEXXProductLine',
                                                   'IDEXXProductName',
                                                   'IDEXXProductType',
                                                   false);
        lstCSH.add(item);

        item =
            CreateTestClassData.CreateSalesHistory(acc2.Id,
                                                   'SAP1234567',
                                                   'IDEXXKEY1234' + (i++),
                                                   AbstractSalesHistoryController.PF_CAG_SUMMARY,
                                                   AbstractSalesHistoryController.PG_PRACTICE_SUPPLIES,
                                                   'IDEXXProductLine',
                                                   'IDEXXProductName',
                                                   'IDEXXProductType',
                                                   false);
        lstCSH.add(item);

        insert lstCSH;

        Account acc = [select id,name from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryRevenueController testcontructor = new InlineCAGSalesHistoryRevenueController(controller);
        System.debug('chemistryQuery='+testcontructor.chemistryQuery);
        System.assertEquals(acc.name, 'Customer Testing Account');

        AbstractSalesHistoryController.SalesHistoryWrapper val = testcontructor.Chemistry.get(0);
        System.assertEquals('', val.getQtdGrowthStyle());
        System.assertEquals('', val.getCurrentMonthGrowthStyle());
        System.assertEquals('', val.getLastMonthGrowthStyle());
        System.assertEquals('', val.getTwoMonthsAgoGrowthStyle());
        System.assertEquals('', val.getThreeMonthsAgoGrowthStyle());
        System.assertEquals('', val.getFourMonthsAgoGrowthStyle());
        System.assertEquals('', val.getFiveMonthsAgoGrowthStyle());
        System.assertEquals('', val.getSixMonthsAgoGrowthStyle());
        System.assertEquals('', val.getSevenMonthsAgoGrowthStyle());
        System.assertEquals('', val.getEightMonthsAgoGrowthStyle());
        System.assertEquals('', val.getNineMonthsAgoGrowthStyle());
        System.assertEquals('', val.getTenMonthsAgoGrowthStyle());
        System.assertEquals('', val.getElevenMonthsAgoGrowthStyle());
        // System.debug('val.getQtyYOYGrowthStyle(): '+val.getQtyYOYGrowthStyle());
        //System.debug('val.getUnitsYOYGrowthStyle(): '+val.getUnitsYOYGrowthStyle());
        //System.debug('val.getQtdLastDiff(): '+val.getQtdLastDiff());
    }

    static testMethod void Test_CreateCAGKits() {

        // works as update scenario
        List<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
        for(Customer_Sales_History__c tempCSH : lstCSH) {
            tempCSH.PRODUCT_GROUP__c = 'RAPID ASSAY';
            tempCSH.PRODUCT_LINE__c = 'SNAP';
            tempCSH.PRODUCT_FAMILY__c = 'CAG SUMMARY2';
        }
        update lstCSH;

        Test.startTest();
        Account acc = [select id from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryRevenueController testcontructor = new InlineCAGSalesHistoryRevenueController(controller);
        List<Customer_Sales_History__c> CSH = [select Product_Line__c from Customer_Sales_History__c where Product_Line__c =: lstCSH[0].Product_Line__c];
        System.assertEquals(CSH[0].Product_Line__c, 'SNAP');
        Test.stopTest();
    }

    static testMethod void Test_CreatKitsTotalrow() {

        // works as update scenario
        List<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
        for(Customer_Sales_History__c tempCSH : lstCSH) {
            tempCSH.PRODUCT_GROUP__c = 'RAPID ASSAY';
            tempCSH.PRODUCT_LINE__c = 'SNAP';
            tempCSH.PRODUCT_FAMILY__c = 'CAG KITS TOTAL';
        }
        update lstCSH;

        Test.startTest();
        Account acc = [select id from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryRevenueController testcontructor = new InlineCAGSalesHistoryRevenueController(controller);
        List<Customer_Sales_History__c> CSH = [select Product_Line__c from Customer_Sales_History__c where Product_Line__c =: lstCSH[0].Product_Line__c];
        System.assertEquals(CSH[0].Product_Line__c, 'SNAP');
        Test.stopTest();
    }

    static testMethod void Test_ConsumablesTotal() {

        // works as update scenario
        List<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
        for(Customer_Sales_History__c tempCSH : lstCSH) {
            tempCSH.PRODUCT_GROUP__c = 'RAPID ASSAY';
            tempCSH.PRODUCT_LINE__c = 'SNAP';
            tempCSH.PRODUCT_FAMILY__c = 'CAG CONSUMABLES TOTAL';
        }
        update lstCSH;

        Test.startTest();
        Account acc = [select id from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryRevenueController testcontructor = new InlineCAGSalesHistoryRevenueController(controller);
        list<Customer_Sales_History__c> CSH = [select Product_Line__c from Customer_Sales_History__c where Product_Line__c =: lstCSH[0].Product_Line__c];
        System.assertEquals(CSH[0].Product_Line__c, 'SNAP');
        Test.stopTest();
    }

    static testMethod void Test_LabServicesTotals() {

        // works as update scenario
        list<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
        for(Customer_Sales_History__c tempCSH : lstCSH) {
            tempCSH.PRODUCT_GROUP__c = 'RAPID ASSAY';
            tempCSH.PRODUCT_LINE__c = 'SNAP';
            tempCSH.PRODUCT_FAMILY__c = 'CAG LABS TOTAL';
        }
        update lstCSH;

        Test.startTest();
        Account acc = [select id from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryRevenueController testcontructor = new InlineCAGSalesHistoryRevenueController(controller);
        list<Customer_Sales_History__c> CSH = [select PRODUCT_FAMILY__c from Customer_Sales_History__c where PRODUCT_FAMILY__c =: lstCSH[0].PRODUCT_FAMILY__c];
        System.assertEquals(CSH[0].PRODUCT_FAMILY__c, 'CAG LABS TOTAL');
        Test.stopTest();
    }

    static testMethod void Test_CAGTotal() {

        // works as update scenario
        list<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
        for(Customer_Sales_History__c tempCSH : lstCSH) {
            tempCSH.PRODUCT_GROUP__c = 'RAPID ASSAY';
            tempCSH.PRODUCT_LINE__c = 'SNAP';
            tempCSH.PRODUCT_FAMILY__c = 'CAG TOTAL';
        }
        update lstCSH;

        Test.startTest();
        Account acc = [select id from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryRevenueController testcontructor = new InlineCAGSalesHistoryRevenueController(controller);
        list<Customer_Sales_History__c> CSH = [select PRODUCT_GROUP__c from Customer_Sales_History__c where PRODUCT_GROUP__c =: lstCSH[0].PRODUCT_GROUP__c];
        System.assertEquals(CSH[0].PRODUCT_GROUP__c, 'RAPID ASSAY');
        Test.stopTest();
    }

    //******* Newly added Methods ***************

    static testMethod void Test_Consumables() {

        // works as update scenario
        list<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
        for(Customer_Sales_History__c tempCSH : lstCSH) {
            tempCSH.PRODUCT_GROUP__c = 'PROCYTE CONSUMABLES';
            tempCSH.PRODUCT_LINE__c =  'CHEMISTRY';
            tempCSH.PRODUCT_FAMILY__c = 'CAG TOTAL';
        }
        update lstCSH;

        Test.startTest();
        Account acc = [select id from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryRevenueController testcontructor = new InlineCAGSalesHistoryRevenueController(controller);
        list<Customer_Sales_History__c> CSH = [select PRODUCT_GROUP__c from Customer_Sales_History__c where PRODUCT_GROUP__c =: lstCSH[0].PRODUCT_GROUP__c];
        System.assertEquals(CSH[0].PRODUCT_GROUP__c, 'PROCYTE CONSUMABLES');
        Test.stopTest();

    }

    static testMethod void Test_LabServices() {

        // works as update scenario
        list<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
        for(Customer_Sales_History__c tempCSH : lstCSH) {
            tempCSH.PRODUCT_GROUP__c = 'ENDOCRINOLOGY';
            //tempCSH.PRODUCT_LINE__c =  'CHEMISTRY';
            //tempCSH.PRODUCT_FAMILY__c = 'CAG TOTAL';
        }
        update lstCSH;

        Test.startTest();
        Account acc = [select id from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryRevenueController testcontructor = new InlineCAGSalesHistoryRevenueController(controller);
        list<Customer_Sales_History__c> CSH = [select PRODUCT_GROUP__c from Customer_Sales_History__c where PRODUCT_GROUP__c =: lstCSH[0].PRODUCT_GROUP__c];
        System.assertEquals(CSH[0].PRODUCT_GROUP__c, 'ENDOCRINOLOGY');
        Test.stopTest();

    }

    static testMethod void Test_TeleMed() {

        // works as update scenario
        list<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
        for(Customer_Sales_History__c tempCSH : lstCSH) {
            //tempCSH.PRODUCT_GROUP__c = 'ENDOCRINOLOGY';
            tempCSH.PRODUCT_LINE__c =  'TELEMED';
            //tempCSH.PRODUCT_FAMILY__c = 'CAG TOTAL';
        }
        update lstCSH;

        Test.startTest();
        Account acc = [select id from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryRevenueController testcontructor = new InlineCAGSalesHistoryRevenueController(controller);
        list<Customer_Sales_History__c> CSH = [select PRODUCT_LINE__c from Customer_Sales_History__c where PRODUCT_LINE__c =: lstCSH[0].PRODUCT_LINE__c];
        System.assertEquals(CSH[0].PRODUCT_LINE__c, 'TELEMED');
        Test.stopTest();

    }

    static testMethod void Test_SerologyImmunology() {

        // works as update scenario
        list<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
        for(Customer_Sales_History__c tempCSH : lstCSH) {
            tempCSH.PRODUCT_GROUP__c = 'SEROLOGY/IMMUNOLOGY';
            //tempCSH.PRODUCT_LINE__c =  'CHEMISTRY';
            tempCSH.PRODUCT_FAMILY__c = 'Allergy';
        }
        update lstCSH;

        Test.startTest();
        Account acc = [select id from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryRevenueController testcontructor = new InlineCAGSalesHistoryRevenueController(controller);
        list<Customer_Sales_History__c> CSH = [select PRODUCT_GROUP__c from Customer_Sales_History__c where PRODUCT_GROUP__c =: lstCSH[0].PRODUCT_GROUP__c];
        System.assertEquals(CSH[0].PRODUCT_GROUP__c, 'SEROLOGY/IMMUNOLOGY');
        Test.stopTest();

    }

    static testMethod void Test_CAGTeleMedTotal() {

        // works as update scenario
        list<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
        for(Customer_Sales_History__c tempCSH : lstCSH) {

            tempCSH.PRODUCT_FAMILY__c = 'CAG TELEMED TOTAL';
        }
        update lstCSH;

        Test.startTest();
        Account acc = [select id from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryRevenueController testcontructor = new InlineCAGSalesHistoryRevenueController(controller);
        list<Customer_Sales_History__c> CSH = [select PRODUCT_FAMILY__c from Customer_Sales_History__c where PRODUCT_FAMILY__c =: lstCSH[0].PRODUCT_FAMILY__c];
        System.assertEquals(CSH[0].PRODUCT_FAMILY__c, 'CAG TELEMED TOTAL');
        Test.stopTest();

    }

    //*********
}