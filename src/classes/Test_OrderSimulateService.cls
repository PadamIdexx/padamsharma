@isTest
public with sharing class Test_OrderSimulateService {
    @isTest static void SalesOrderSimulatePositive() {
        Test.startTest();
        OrderSimulateRequest.SalesOrderSimulateRequest request = OrderTestDataFactory.createSimulateRequest();
        OrdersimulateResponse.SalesOrderSimulateResponse mockResponse = OrderTestDataFactory.createSimulateResponse();
        
        Test.setMock(WebServiceMock.class, new Test_SOAPReusableMockResponse(mockResponse));
        
        OrderSimulateService service = new OrderSimulateService();
        
        service.simulateOrder(request);
        
        SAPOrder order = OrderTestDataFactory.createSAPOrder();
        order = OrderSimulateResponse.transform(order, mockResponse);

        Test.stopTest();
        
    }
}