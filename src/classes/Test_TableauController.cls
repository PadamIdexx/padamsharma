/**************************************************************************************
 * Create By:   Pooja Wade
 * CreateDate:  23-Nov-2015
 * Description: This Class does below processing
 *              1. To provide code coverage for class "TableauController "
 *              
 *                 
 *  
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                 Created Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Pooja Wade                23-Nov-2015                   Initial version.
 *  * Anudeep Gopagoni          15-Feb-2016                   Added Negative test scenario to increase code coverage
 **************************************************************************************/
 @isTest(seeAllData=false)
public class Test_TableauController{

      static testMethod void tableauUnitTest() {
      Test.startTest(); //start Test 
          
          Account testAccount = new Account();
          testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
          testAccount.Name = 'Test Account';
          testAccount.CurrencyIsoCode = 'USD';
          if(Schema.sObjectType.Account.isCreateable() && testAccount != null) {
                  insert testAccount;     
                  System.assert(testAccount.Id != null);
          }
          
          Test.setCurrentPage(Page.Tableau);  //set Page to Tableau
          System.currentPageReference().getParameters().put('acctid',testAccount.id);
         TableauController controller = new TableauController(new ApexPages.StandardController(testAccount)); //passing the Account record as a parameter to the controller
          Test.stopTest(); //Stop Test
      }
      static testMethod void tableauNegativeTest() {
      Test.startTest(); //start Test 
          
          Account testAccount = new Account();
          testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
          testAccount.Name = 'Test Account';
          testAccount.CurrencyIsoCode = 'USD';
          if(Schema.sObjectType.Account.isCreateable() && testAccount != null) {
                  insert testAccount;     
                  System.assert(testAccount.Id != null);
          }
          
          Test.setCurrentPage(Page.Tableau);  //set Page to Tableau
          System.currentPageReference().getParameters().put('acctid',null);
         TableauController controller = new TableauController(new ApexPages.StandardController(testAccount)); //passing the Account record as a parameter to the controller
          Test.stopTest(); //Stop Test
      }

}