@isTest
private class AccountTriggerHandlerTest {
    
    static testMethod void createPrescriptiveWorkOrdersFromRankingTest() {
        // Create 200 accounts
        List<Account> listOfAccounts = new List<Account>();
        for(Integer i=0; i<200; i++){
            Account account = new Account(
                Name = 'Account Name ' + i,
                ShippingCountryCode = 'US',
                ShippingStateCode = 'PA'
            );
            listOfAccounts.add(account);
        }
        insert listOfAccounts;

        // Check that there is no work orders.
        List<WorkOrder> listWorkOrders = new List<WorkOrder>(
            [
                SELECT  Id
                FROM    WorkOrder
                WHERE   RecordTypeId = :WFM_Constants.WORK_ORDER_PRESCRIPTIVE_RECORDTYPE_ID
            ]
        );
        System.assertEquals(listWorkOrders.size(), 0);

        Test.startTest();
        // Update rankings and update dates
        Integer i=1;
        for(Account account : listOfAccounts){
            account.FSR_Ranking__c = i;
            account.FSR_Ranking_Last_Update__c = Datetime.now().addDays(1);
            i++;
        }

        update listOfAccounts;
        Test.stopTest();

        // Check that we have 115 work orders (30 for the top 15 and 85 for the ones from 15 to 100)
        listWorkOrders = [
            SELECT      Id
            FROM        WorkOrder
            WHERE       RecordTypeId = :WFM_Constants.WORK_ORDER_PRESCRIPTIVE_RECORDTYPE_ID
        ];
        System.assertEquals(listWorkOrders.size(), 115);
    }

}