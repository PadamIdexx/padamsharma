@isTest
public with sharing class Test_Paymetrics {
    private static testmethod void testVISACCSuccess(){
          
        primesysXipaysoapMessage.BasicHttpBinding_IXiPay test1= new primesysXipaysoapMessage.BasicHttpBinding_IXiPay ();
        primesysXipaysoapMessage.Ping_element test2 = new primesysXipaysoapMessage.Ping_element();
        primesysXipaysoapMessage.PreAuthorize_element test3 = new primesysXipaysoapMessage.PreAuthorize_element();
        primesysXipaysoapMessage.PingResponse_element test4 = new primesysXipaysoapMessage.PingResponse_element();
        paymetricXipaysoap30Message.BlobItem test5 =  new paymetricXipaysoap30Message.BlobItem();
        paymetricXipaysoap30Message.CheckImage test6 = new paymetricXipaysoap30Message.CheckImage();
        paymetricXipaysoap30Message.InfoItem test7 = new paymetricXipaysoap30Message.InfoItem();
        paymetricXipaysoap30Message.LineItem  test8 = new paymetricXipaysoap30Message.LineItem();
        paymetricXipaysoap30Message.ArrayOfBlobItem test9 = new paymetricXipaysoap30Message.ArrayOfBlobItem ();
        paymetricXipaysoap30Message.ArrayOfCheckImage test10 = new paymetricXipaysoap30Message.ArrayOfCheckImage();
        paymetricXipaysoap30Message.ArrayOfInfoItem test11 = new paymetricXipaysoap30Message.ArrayOfInfoItem();
        paymetricXipaysoap30Message.ArrayOfLineItem test12 = new paymetricXipaysoap30Message.ArrayOfLineItem();

        OrderCreation.creditCardReqResponse ccReq = new OrderCreation.creditCardReqResponse();
        ccReq.nameOnCard= 'RYTGH';
        ccReq.creditCardType = 'VISA';
        ccReq.creditCardNumber='4012000077777777';
        ccReq.expMonth=12;
        ccReq.expYear=19;
        ccReq.cvv='123';
        OrderCreation ord = new OrderCreation('USD');
        ord.ccReq=ccReq;
        ord.tokenizedCardNo='123456789087654';
        ord.enteredCardNo='4012000077777777';
        Test.setMock(WebServiceMock.class, new Test_PaymetricsCalloutMock());
        Test_PaymetricsCalloutMock.indicatorVar = 100;
        Test.startTest();  
        ord.preAuthorizeCreditCard();
        Test_PaymetricsCalloutMock.indicatorVar = -1;
        ord.preAuthorizeCreditCard();
        Test_PaymetricsCalloutMock.indicatorVar = -2;
        ord.preAuthorizeCreditCard();
        Test_PaymetricsCalloutMock.indicatorVar = -50;
        ord.preAuthorizeCreditCard();
        Test_PaymetricsCalloutMock.indicatorVar = -100;
        ord.preAuthorizeCreditCard();
        Test_PaymetricsCalloutMock.indicatorVar = -101;
        ord.preAuthorizeCreditCard();
        Test_PaymetricsCalloutMock.indicatorVar = -111;
        ord.preAuthorizeCreditCard();
        Test_PaymetricsCalloutMock.indicatorVar = -200;
        ord.preAuthorizeCreditCard();
        Test_PaymetricsCalloutMock.indicatorVar = -201;
        ord.preAuthorizeCreditCard();
        Test_PaymetricsCalloutMock.indicatorVar = -211;
        ord.preAuthorizeCreditCard();
        Test_PaymetricsCalloutMock.indicatorVar = -300;
        ord.preAuthorizeCreditCard();
        Test_PaymetricsCalloutMock.indicatorVar = -301;
        ord.preAuthorizeCreditCard();
        Test_PaymetricsCalloutMock.indicatorVar = -302;
        ord.preAuthorizeCreditCard();
        Test_PaymetricsCalloutMock.indicatorVar = -311;
        ord.preAuthorizeCreditCard();
        Test_PaymetricsCalloutMock.indicatorVar = -400;
        ord.preAuthorizeCreditCard();
        Test_PaymetricsCalloutMock.indicatorVar = -500;
        ord.preAuthorizeCreditCard();
        Test_PaymetricsCalloutMock.indicatorVar = -501;
        ord.preAuthorizeCreditCard();
        Test_PaymetricsCalloutMock.indicatorVar = -600;
        ord.preAuthorizeCreditCard();
        Test_PaymetricsCalloutMock.indicatorVar = -601;
        ord.preAuthorizeCreditCard();
        Test.stopTest();
            
    }    
    
   
    private static testmethod void testMasterCCSuccess(){
        
        OrderCreation.creditCardReqResponse ccReq = new OrderCreation.creditCardReqResponse();
        ccReq.nameOnCard= 'RYTGH';
        ccReq.creditCardType = 'MC';
        ccReq.creditCardNumber='4012000077777777';
        ccReq.expMonth=12;
        ccReq.expYear=19;
        ccReq.cvv='123';
        OrderCreation ord = new OrderCreation('USD');
        ord.ccReq=ccReq;
        ord.tokenizedCardNo='123456789087654';
        ord.enteredCardNo='5424180279791773';
        Test.setMock(WebServiceMock.class, new Test_PaymetricsCalloutMock());
        Test.startTest();  
        ord.preAuthorizeCreditCard();
        Test.stopTest();
    }   
    private static testmethod void testAMEXCCSuccess(){

        OrderCreation.creditCardReqResponse ccReq = new OrderCreation.creditCardReqResponse();
        ccReq.nameOnCard= 'RYTGH';
        ccReq.creditCardType = 'AMEX';
        ccReq.creditCardNumber='373235387881015';
        ccReq.expMonth=12;
        ccReq.expYear=19;
        ccReq.cvv='1234';
        OrderCreation ord = new OrderCreation('USD');
        ord.ccReq=ccReq;
        ord.enteredCardNo='373235387881015';
        ord.tokenizedCardNo='123454321341234567';
        Test.setMock(WebServiceMock.class, new Test_PaymetricsCalloutMock());
        Test.startTest();  
        ord.preAuthorizeCreditCard();
        Test.stopTest();
    }   
    private static testmethod void testVISACADCCSuccess(){

        OrderCreation.creditCardReqResponse ccReq = new OrderCreation.creditCardReqResponse();
        ccReq.nameOnCard= 'RYTGH';
        ccReq.creditCardType = 'VISA';
        ccReq.creditCardNumber='4012000077777777';
        ccReq.expMonth=12;
        ccReq.expYear=19;
        ccReq.cvv='123';
        OrderCreation ord = new OrderCreation('CAD');
        ord.ccReq=ccReq;
        ord.tokenizedCardNo='123456789087654';
        ord.enteredCardNo='4012000077777777';
        Test.setMock(WebServiceMock.class, new Test_PaymetricsCalloutMock());
        Test.startTest();  
        ord.preAuthorizeCreditCard();
        Test.stopTest();
        //System.assertEquals('CAD', acc.CurrencyIsoCode);
    }  
    private static testmethod void testMasterCADCCSuccess(){

        OrderCreation.creditCardReqResponse ccReq = new OrderCreation.creditCardReqResponse();
        ccReq.nameOnCard= 'RYTGH';
        ccReq.creditCardType = 'MC';
        ccReq.creditCardNumber='5424180279791773';
        ccReq.expMonth=12;
        ccReq.expYear=19;
        ccReq.cvv='123';
        OrderCreation ord = new OrderCreation('CAD');
        ord.ccReq=ccReq;
        ord.tokenizedCardNo='123456789087654';
        ord.enteredCardNo='5424180279791773';
        Test.setMock(WebServiceMock.class, new Test_PaymetricsCalloutMock());
        Test.startTest();  
        ord.preAuthorizeCreditCard();
        Test.stopTest();
        
    }   
    private static testmethod void testAMEXCADCCSuccess(){
                         
        OrderCreation.creditCardReqResponse ccReq = new OrderCreation.creditCardReqResponse();
        ccReq.nameOnCard= 'RYTGH';
        ccReq.creditCardType = 'AMEX';
        ccReq.creditCardNumber='373235387881015';
        ccReq.expMonth=12;
        ccReq.expYear=19;
        ccReq.cvv='1234';
        OrderCreation ord = new OrderCreation('CAD');
        ord.ccReq=ccReq;
        ord.enteredCardNo='373235387881015';
        ord.tokenizedCardNo='123454321341234567';
        Test.setMock(WebServiceMock.class, new Test_PaymetricsCalloutMock());
        Test.startTest();  
        ord.preAuthorizeCreditCard();
        Test.stopTest();
      
    }   
    private static testmethod void testVISACCFailure1(){

        OrderCreation.creditCardReqResponse ccReq = new OrderCreation.creditCardReqResponse();
        ccReq.nameOnCard= 'RYTGH';
        ccReq.creditCardType = 'VISA';
        ccReq.creditCardNumber='373235387881015';
        ccReq.expMonth=12;
        ccReq.expYear=19;
        ccReq.cvv='123';
        OrderCreation ord = new OrderCreation('USD');
        ord.ccReq=ccReq;
        ord.enteredCardNo='373235387881015';
        ord.tokenizedCardNo='123454321341234567';
        Test.setMock(WebServiceMock.class, new Test_PaymetricsCalloutMock());
        Test.startTest();  
        ord.preAuthorizeCreditCard();
        Test.stopTest();
    }   
    private static testmethod void testVISACCFailure2(){

        OrderCreation.creditCardReqResponse ccReq = new OrderCreation.creditCardReqResponse();
        ccReq.nameOnCard= 'RYTGH';
        ccReq.creditCardType = 'VISA';
        ccReq.creditCardNumber='123456432112345';
        ccReq.expMonth=12;
        ccReq.expYear=19;
        ccReq.cvv='123';
        OrderCreation ord = new OrderCreation('USD');
        ord.ccReq=ccReq;
        ord.enteredCardNo='12345654321123';
        ord.tokenizedCardNo='123454321341234567';
        Test.setMock(WebServiceMock.class, new Test_PaymetricsCalloutMock());
        Test.startTest();  
        ord.preAuthorizeCreditCard();
        Test.stopTest();
    }   
    private static testmethod void testVISACCFailure3(){

        OrderCreation.creditCardReqResponse ccReq = new OrderCreation.creditCardReqResponse();
        ccReq.creditCardType = 'VISA';
        ccReq.creditCardNumber='123456432112345';
        ccReq.expMonth=12;
        ccReq.expYear=19;
        ccReq.cvv='123';
        OrderCreation ord = new OrderCreation('USD');
        ord.ccReq=ccReq;
        ord.enteredCardNo='12345654321123';
        ord.tokenizedCardNo='123454321341234567';
        Test.setMock(WebServiceMock.class, new Test_PaymetricsCalloutMock());
        Test.startTest();  
        ord.preAuthorizeCreditCard();
        Test.stopTest();
        
    }   
        private static testmethod void testVISACCFailure4(){

        OrderCreation.creditCardReqResponse ccReq = new OrderCreation.creditCardReqResponse();
        ccReq.creditCardType = 'VISA';
        ccReq.creditCardNumber='123456432112345';
        ccReq.expMonth=12;
        ccReq.expYear=19;
        ccReq.cvv='123';
        OrderCreation ord = new OrderCreation('USD');
        ord.ccReq=ccReq;
        ord.enteredCardNo='12345654321123';
        ord.tokenizedCardNo='123454321341234567';
        Test.setMock(WebServiceMock.class, new Test_PaymetricsCalloutMock());
        Test.startTest();  
        ord.getPaymentTerms();
        ord.preAuthorizeCreditCard();
        Test.stopTest();
   
    } 
    private static testmethod void TestPing(){
        
        Test.setMock(WebServiceMock.class, new Test_PaymetricsPingCalloutMock());
        Test.startTest();
        primesysXipaysoapMessage.BasicHttpBinding_IXiPay test1 =new primesysXipaysoapMessage.BasicHttpBinding_IXiPay();
        test1.Ping();
        Test.StopTest();
        
    }
}