public with sharing class TerritoryUtils {

    public static boolean isAccountInUserTerritory(String accountId) {
        return TerritoryUtils.isAccountInUserTerritory(accountId,
                                                       null);
    }

    public static boolean isAccountInUserTerritory(String accountId,
                                                   String role) {
        System.debug('isAccountInUserTerritory: '+accountId+', '+role);
        Id userId = UserInfo.getUserId();
        Set<Id> territoryIds = new Set<Id>();
        boolean isAligned = false;
        for (UserTerritory2Association uta : [select Territory2Id, RoleInTerritory2 from UserTerritory2Association where UserId = :userId]) {
            isAligned = true;
            if (role == null || uta.RoleInTerritory2 == role) {
                territoryIds.add(uta.Territory2Id);
            }
        }
        System.debug('territoryIds='+territoryIds);
                                                       
        boolean result = false;
        if (!isAligned) {
            result = true;
        } else {
            integer count = [SELECT count() FROM ObjectTerritory2Association WHERE Territory2Id IN :territoryIds and ObjectId = :accountId];
            result = count > 0;
        }

        return result;
    }

    public static Set<Id> getTerritoriesForAccount(String accountId) {
        Set<Id> territoriesList = new Set<Id>();

        for (ObjectTerritory2Association ota : [select Id, Territory2Id from ObjectTerritory2Association WHERE ObjectId = :accountId]) {
            System.debug('ota='+ota);
            territoriesList.add(ota.Territory2Id);
        }
        System.debug('territoriesList='+territoriesList);
        System.debug('territoriesList.size='+territoriesList.size());

        return territoriesList;
    }

    public static Set<String> getUserRolesInAccount(String accountId) {
        String userId = UserInfo.getUserId();
        System.debug('userId='+userId);
        Set<String> roleList = new Set<String>();
        Set<Id> territoriesList = getTerritoriesForAccount(accountId);

        List<UserTerritory2Association> testUtaList = [select RoleInTerritory2 from UserTerritory2Association where Territory2Id IN :territoriesList ];
        System.debug('testUtaList='+testUtaList);
        System.debug('testUtaList.size()='+testUtaList.size());
        for (UserTerritory2Association uta : testUtaList) {
            System.debug('uta='+uta);
        }
        
        List<UserTerritory2Association> utaList = [select RoleInTerritory2 from UserTerritory2Association where Territory2Id IN :territoriesList AND UserId = :userId ];
        System.debug('utaList='+utaList);
        for (UserTerritory2Association uta : utaList) {
            roleList.add(uta.RoleInTerritory2);
        }

        return roleList;
    }

    public static Set<String> getUserRoles() {
        String userId = UserInfo.getUserId();
        System.debug('userId='+userId);
        Set<String> roleList = new Set<String>();

        List<UserTerritory2Association> utaList = [select RoleInTerritory2 from UserTerritory2Association where UserId = :userId ];
        System.debug('utaList='+utaList);
        for (UserTerritory2Association uta : utaList) {
            roleList.add(uta.RoleInTerritory2);
        }

        return roleList;
    }

    public static Boolean isUserAligned(String userId) {
        integer terrCount = [select count() from UserTerritory2Association where UserId = :userId];
        return terrCount > 0;
    }

    public static Boolean shouldRestrictAccess(Id accountId) {
        System.debug('shouldRestrictAccess');
        Boolean restrictAccess = false;

        String userId = UserInfo.getUserId();

        if (isUserAligned(userId)) {
            restrictAccess = true;
        }

        if (restrictAccess == true) {
            Set<String> roleList = getUserRoles();
            System.debug('roleList='+roleList);
            if (roleList.contains(TerritoryConstants.VDC)
                || roleList.contains(TerritoryConstants.IVDC)
                || roleList.contains(TerritoryConstants.REGIONAL_MANAGER)
                || roleList.contains(TerritoryConstants.AREA_MANAGER)
                || roleList.contains(TerritoryConstants.VDS)) {
                restrictAccess = true;
            } else {
                restrictAccess = false;
            }

        }

        if (restrictAccess == true) {
            // Check if user has AccessToPrivateAccountInfo permission set assigned.
            List<PermissionSetAssignment> permList = [SELECT PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId= :userId AND PermissionSet.Name = 'Access_to_Territory_restricted_Account_Information'];

            if (!permList.isEmpty()) {
                // If so, then we don't need to check for territory.
                restrictAccess = false;
            } else if (TerritoryUtils.isAccountInUserTerritory(accountId)) {
                // Specified account is in user's assigned territory.
                restrictAccess = false;
            }
        }

        return restrictAccess;
    }

}