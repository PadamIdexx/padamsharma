@isTest 
public class Test_ReadWrapper {
    @isTest static void testPositive() {
        Test.startTest();
        try {
            SAPOrder order = OrderTestDataFactory.createSAPOrder();
            ReadWrapper w = new ReadWrapper();
            w.execute(order);
        } catch (Exception e) {
            System.debug('Got Exception which was expected.');
        }          
      
        Test.stopTest();			
    } 

    @isTest static void testBadRequest() {
        Test.startTest();
        try {
            ReadWrapper w = new ReadWrapper();
            w.execute(null);
        } catch (Exception e) {
            System.debug('Got Exception which was expected.');
        }          
      
        Test.stopTest();			
    }   
}