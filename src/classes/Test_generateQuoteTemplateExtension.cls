@isTest
public class Test_generateQuoteTemplateExtension {
    
    public static List<Quote> quotes{get;set;}
    public static User u{get;set;}
    @testSetup 
    static void createTestData(){

        quotes = [SELECT Name, Id FROM Quote WHERE Name LIKE '%Test Quote%'];
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        u = new User(Alias = 'standt', Email='randomstandarduser@IDEXX.com', 
                     EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                     LocaleSidKey='en_US', ProfileId = p.Id, 
                     TimeZoneSidKey='America/Los_Angeles', UserName='reallyrandomstandarduser1234@IDEXX.com');
        
    }    
    
   @isTest static void  testTemplateCreationPositive(){
         AccountTriggerHandler.runGeocoding = false;
        Test_Data_Utility.createTestQuote(1);
        createTestData();
        insert new QuoteTemplate__c(Name = 'Test', Template_Id__c = 'Value');
        generateQuoteTemplateExtension ext = new generateQuoteTemplateExtension(new ApexPages.StandardController(quotes.get(0)));
        PageReference pageRef = Page.generateQuoteTemplatePage;
        Test.setCurrentPage(pageRef);

        System.runAs(u) {
            Test.startTest();
            ext.inititalize(quotes.get(0).id);
            ext.setupPage();
            ext.createView();
            ext.lang = 'de';
            ext.changeUserLanguage();
            List<selectOption> options = new List<selectOption>();
            options = ext.getLocales();
            options = ext.getTemplates();
           // system.assertEquals('de', u.LanguageLocaleKey, 'User language is: ' + u.LanguageLocaleKey);
            ext.resetUserLanguage();
            system.assertEquals('en_US', u.LanguageLocaleKey, 'User language is: ' + u.LanguageLocaleKey);
			ext.save();
			ext.send();            
            Test.stopTest();
        }
        
    }
    @isTest static void testTemplateCreationNegative(){
        AccountTriggerHandler.runGeocoding = false;
        Test_Data_Utility.createTestQuote(1);
        createTestData();
        insert new QuoteTemplate__c(Name = 'Test', Template_Id__c = 'Value');
        generateQuoteTemplateExtension ext = new generateQuoteTemplateExtension(new ApexPages.StandardController(quotes.get(0)));
                PageReference pageRef = Page.generateQuoteTemplatePage;
        Test.setCurrentPage(pageRef);

        System.runAs(u) {
            Test.startTest();
            ext.inititalize(quotes.get(0).id);
            ext.setupPage();
            ext.createView();
            ext.lang = 'de';
            ext.changeUserLanguage();
            ext.quoteContact = '12344555';
            List<selectOption> options = new List<selectOption>();
            options = ext.getLocales();
            options = ext.getTemplates();
            system.assertEquals('de', ext.lang, 'User language is: ' + ext.lang);
            ext.resetUserLanguage();
            system.assertEquals('en_US', u.LanguageLocaleKey, 'User language is: ' + u.LanguageLocaleKey);
			ext.save();
			ext.send();            
            Test.stopTest();
        }
    }
    
}