/**
 * 
 */
@isTest
private class Test_idexxiComSapxiPntsservicesCalc {

    static testMethod void idexxiComSapxiPntsservicesCalcUnitTest() {
        idexxiComSapxiPntsservicesCalc idexxPoints = new idexxiComSapxiPntsservicesCalc();
        idexxiComSapxiPntsservicesCalc.DT_PNTS_DETAIL_HIST_REQ pntDetailRef = new idexxiComSapxiPntsservicesCalc.DT_PNTS_DETAIL_HIST_REQ();
        idexxiComSapxiPntsservicesCalc.DT_REWARDS_POINT_CALC rwdPointRef = new idexxiComSapxiPntsservicesCalc.DT_REWARDS_POINT_CALC();
        idexxiComSapxiPntsservicesCalc.TRANSACTION_DETAIL_element trDetRef = new idexxiComSapxiPntsservicesCalc.TRANSACTION_DETAIL_element(); 
        idexxiComSapxiPntsservicesCalc.DT_PNTS_SUMMARY_REQ dtPntSummRef = new idexxiComSapxiPntsservicesCalc.DT_PNTS_SUMMARY_REQ(); 
        idexxiComSapxiPntsservicesCalc.BANK_SUMMARY_element  bnkSummRef = new idexxiComSapxiPntsservicesCalc.BANK_SUMMARY_element(); 
        idexxiComSapxiPntsservicesCalc.CURRENT_element currEleRef = new idexxiComSapxiPntsservicesCalc.CURRENT_element(); 
        idexxiComSapxiPntsservicesCalc.DT_PNTS_DETAIL_HIST_RESPONSE  dtPntDetRef = new idexxiComSapxiPntsservicesCalc.DT_PNTS_DETAIL_HIST_RESPONSE(); 
        idexxiComSapxiPntsservicesCalc.DT_REWARDS_POINT_GATE  dtRwdPntRef = new idexxiComSapxiPntsservicesCalc.DT_REWARDS_POINT_GATE(); 
        idexxiComSapxiPntsservicesCalc.DT_PNTS_WEBCALC_REQUEST  dtPntWebReqRef = new idexxiComSapxiPntsservicesCalc.DT_PNTS_WEBCALC_REQUEST(); 
        idexxiComSapxiPntsservicesCalc.DT_PNTS_WEBCALC_RESPONSE  dtPntWebResRef = new idexxiComSapxiPntsservicesCalc.DT_PNTS_WEBCALC_RESPONSE(); 
        idexxiComSapxiPntsservicesCalc.DT_PNTS_SUMMARY_RESPONSE dtPntSumRes =  new idexxiComSapxiPntsservicesCalc.DT_PNTS_SUMMARY_RESPONSE(); 
        idexxiComSapxiPntsservicesCalc.PROGRAMS_element prgEleRef = new idexxiComSapxiPntsservicesCalc.PROGRAMS_element(); 
        idexxiComSapxiPntsservicesCalc.PREVIOUS_element prevEleRef = new idexxiComSapxiPntsservicesCalc.PREVIOUS_element(); 
        idexxiComSapxiPntsservicesCalc.HTTPS_Port httpPortRef = new idexxiComSapxiPntsservicesCalc.HTTPS_Port();
        try{
        httpPortRef.PointsHistoryRead('0000010255');        
        }catch(Exception e) {
        	System.debug('is Callout Exception:'+e.getMessage());
        	System.debug('is Callout ExceptionString:'+e.getStackTraceString());
        	System.debug('Cause ExceptionString:'+e.getCause());
        }
        try{
        	httpPortRef.PointsSummaryRead('0000010255');	
        }catch(Exception e) {
        	System.debug('is Callout Exception:'+e.getMessage());
        	System.debug('is Callout ExceptionString:'+e.getStackTraceString());
        	System.debug('Cause ExceptionString:'+e.getCause());
        }
        try{
        	httpPortRef.PointsCalculate('','',System.today());
        }catch(Exception e) {
        	System.debug('is Callout Exception:'+e.getMessage());
        	System.debug('is Callout ExceptionString:'+e.getStackTraceString());
        	System.debug('Cause ExceptionString:'+e.getCause());
        }
        idexxiComSapxiPntsservicesCalc.HTTPS_Port httpPortRef1 = new idexxiComSapxiPntsservicesCalc.HTTPS_Port();
        idexxiComSapxiPntsservicesCalc.HTTPS_Port httpPortRef2 = new idexxiComSapxiPntsservicesCalc.HTTPS_Port();         
    }
}