@isTest
public class Test_CreateWrapper {
    @isTest static void testPositive() {
        Test.startTest();
        try {
            SAPOrder order = OrderTestDataFactory.createSAPOrder();
            CreateWrapper w = new CreateWrapper();
            w.execute(order);
        } catch (Exception e) {
            System.debug('Got Exception which was expected.');
        }          
      
        Test.stopTest();			
    } 

    @isTest static void testBadRequest() {
        Test.startTest();
        try {
            CreateWrapper w = new CreateWrapper();
            w.execute(null);
        } catch (Exception e) {
            System.debug('Got Exception which was expected.');
        }          
      
        Test.stopTest();			
    } 
}