@isTest
public class Test_OrderCreateService {
    @isTest static void SalesOrderCreatePositive() {
        Test.startTest();
        OrderCreateRequest.SalesOrderCreateRequest request = OrderTestDataFactory.createCreateRequest();
        OrderCreateResponse.SalesOrderCreateResponse mockResponse = OrderTestDataFactory.createCreateResponse();
        
        Test.setMock(WebServiceMock.class, new Test_SOAPReusableMockResponse(mockResponse));
        
        OrderCreateService service = new OrderCreateService();
        
        service.createOrder(request);
        
        SAPOrder order = OrderTestDataFactory.createSAPOrder();
        order = OrderCreateResponse.transform(order, mockResponse);

        Test.stopTest();
    }
}