/* Class Name   : Test_ParentChildAcc
 * Description  : Test Class with unit test scenarios to cover the ParentChildAcc class
 * Created By   : Pooja Wade
 * Created On   : 01-18-2015
 
* Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date                   Modification ID       Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Pooja Wade             01-18-2015             1000                   Initial version
 * Heather Kinney         3.15.2017                                     Fixed apex test class execution error upon validation - dupe contact.
                                                                                
*/
@isTest(seeAllData=false)
public with sharing class Test_ParentChildAcc {
    

   
    static testMethod void ViewParentChildPositiveTestCase() {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
        system.runAs(currentUser){
            // test data creation - test accounts 
              Account testAccount = CreateTestClassData.createCustomerAccount();
              Contact testContact = CreateTestClassData.reusableContact(testAccount.Id);
              insert testContact;
              testAccount.Primary_Contact__c = testContact.Id;
              testAccount.SAP_CUSTOMER_NUMBER__c = '0000001003';
              update testAccount;
              
              
              Account testAccount1 = CreateTestClassData.createCustomerAccount();
              //3.15.2017 commented out the insert of testContact1 to avoid apex test execution failure when validating in PROD: 
              //        System.DmlException: Insert failed. First exception on row 0; first error: DUPLICATES_DETECTED, 
              //        You're creating a duplicate record. We recommend you use an existing record instead.: [] 
              //        Stack Trace: Class.Test_ParentChildAcc.ViewParentChildPositiveTestCase: line 33, column 1
              Contact testContact1 = CreateTestClassData.reusableContact(testAccount1.Id);
              //insert testContact1;
              testAccount1.Primary_Contact__c = testContact1.Id;
              testAccount1.SAP_CUSTOMER_NUMBER__c = '0000001494';
              update testAccount1;
              
              
            Test.startTest();
                PageReference pageRef = Page.ParentChildAcc;//Observe how we set the Page here
                Test.setCurrentPage(pageRef);//Applying page context here

                ApexPages.currentPage().getParameters().put('Id',testAccount.id);

                ApexPages.StandardController std = new ApexPages.StandardController(testAccount);
                ParentChildAcc controllerInstance = new ParentChildAcc(std);
                controllerInstance.fetchParentChild();
            Test.stopTest();
        }
    }

}

      //controllerInstance.childCustomerNum.add('0000005880');
        
             /*   PARENT_CHILD__x testc = new PARENT_CHILD__x (DisplayUrl= 'test',ExternalId='TEST',ParentCustomer__c = '0000001003', ChildCustomer__c ='0000001494' );
                PARENT_CHILD__x testc2 = new PARENT_CHILD__x (DisplayUrl= 'test2',ExternalId='TEST2',ParentCustomer__c = '0000001003', ChildCustomer__c ='0000005880' );

                controllerInstance.childRecord = new List<PARENT_CHILD__x>();
                controllerInstance.childRecord.add(testc);
                controllerInstance.childRecord.add(testc2); */