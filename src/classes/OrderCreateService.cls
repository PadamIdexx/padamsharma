public with sharing class OrderCreateService {

        public String endpoint_x = '';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x = 120000;

		//Namespace information for service all.
        private String[] ns_map_type_info = new String[]{OrderCreateRequest.NAMESPACE, 
            								OrderCreateRequest.SF_NAMESPACE, 
            								OrderCreateRequest.XMLTYPE_NAMESPACE, 
            								OrderCreateRequest.SF_XMLTYPE_NAMESPACE};
                                                
        public OrderCreateService(){
            //Brute force approach to getting runtime classname to use for looking up the endpoint
            String thisName = String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));            
            String endPointURL = '';
            if (!Test.isRunningTest()) { //3.8.2017
                endPointURL = WebServiceHelper.getServiceEndpoints(thisName);   
            }
            endpoint_x = endPointURL!=null ? endPointURL : '';
           
            //set headers
            inputHttpHeaders_x = new Map<String, String>();
            
            String sessionId = '';
            if (!Test.isRunningTest()) { //3.8.2017
                sessionId = WebServiceHelper.getIntegrationSessionId();
            }
            inputHttpHeaders_x.put('sessionId', sessionId);
            
        }

    
        
        public OrderCreateResponse.SalesOrderCreateResponse createOrder(OrderCreateRequest.SalesOrderCreateRequest simulateRequest) {
            OrderCreateRequest.SalesOrderCreateRequest request_x = new OrderCreateRequest.SalesOrderCreateRequest();
            request_x= simulateRequest;
            OrderCreateResponse.SalesOrderCreateResponse response_x;
            Map<String, OrderCreateResponse.SalesOrderCreateResponse> response_map_x = new Map<String, OrderCreateResponse.SalesOrderCreateResponse>();
            response_map_x.put('response_x', response_x); 
            if (!Test.isRunningTest()) { //3.8.2017
                WebServiceCallout.invoke(
                  this,
                  request_x,
                  response_map_x,
                  new String[]{endpoint_x,
                        OrderCreateRequest.SOAPACTION,
                        OrderCreateRequest.NAMESPACE,
                        OrderCreateRequest.PAYLOAD_CLASSNAME,
                        OrderCreateResponse.NAMESPACE,
                        OrderCreateResponse.PAYLOAD_CLASSNAME,
                        OrderCreateResponse.SalesOrderCreateResponse.class.getName()}
                );
            }
            response_x = response_map_x.get('response_x');
            return response_x;
        }

}