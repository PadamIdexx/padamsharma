public with sharing class OrderLineItems {
    /*
    * @Class name:  OliResult 
    * @description: Wrapper class to represent the Order List Item
    */ 
    public class OliResult{
       
        public OrderItem orderItem                                                  {get;set;} //Order Item 
        public Boolean remove                                                       {get;set;} //property corresponding to the checkbox         
        public Integer quantityConfirmed                                            {get;set;} //Confirmed Quantity
        public Decimal sapPrice                                                     {get;set;} //
        public Decimal savings                                                      {get;set;}
        public boolean isBackOrdered                                                {get;set;} //is a Backorderd Product?
        public boolean simulateCheck                                                {get;set;} //boolean variable that indicates if the Order Line Item is submitted for simulate
        public String  prodMatNo                                                    {get;set;} //Product Material Number
        //public String  prodMatNoCopy                                              {get;set;} //Product Material Number copy  
        public integer externalItemNumber                                           {get;set;} //external Item number is the key that's used to map the items from response to the respective item
        public String itemNumber                                                    {get;set;} //returned by SAP
        public String itemCategory                                                  {get;set;} // this is to capture item category based on which free goods code is sent to SAP
        public String materialText                                                  {get;set;}
        public Decimal totalPrice                                                   {get;set;} 
        public Decimal netPrice                                                     {get;set;} 
        public Decimal unitPrice                                                    {get;set;}
        public Decimal points                                                       {get;set;}
        public Decimal discount                                                     {get;set;}
        public Decimal shippingCharges                                              {get;set;}

        public Decimal freightRate													{get;set;}
        public Boolean freightChanged												{get;set;}
        //updating the default freight condition requires passing the step and count
        //from the prior simulate
        public String freightConditionStep											{get;set;}
        public String freightConditionCount											{get;set;}
        
        public Decimal tax                                                          {get;set;}
        public Decimal vat                                                          {get;set;} //This is a condition but needs to be tracked at the line item level
        public Decimal netSubtotal3                                                 {get;set;}
        public Decimal netValue														{get;set;}        

        //pricing information displayed on the order screen.
        public List<Pricing> prices													{get;set;} //currently implemented as OrderCreationLPD.dispwrap3
        public List<Pricing> discounts												{get;set;} //currently implemented as OrderCreationLPD.dispwrap1       
        public List<Pricing> taxes													{get;set;} //currently implemented as OrderCreationLPD.dispwrap2
        
        public String style                                                         {get;set;}
        public String className                                                        {get;set;}
        public String currencyISOCode                                               {get;set;}
        public String shipDate                                                         {get;set;}
        public String expirationDate                                                {get;set;}
        public String materialGroup                                                 {get;set;}
        public String ReferenceDocument                                             {get;set;}
        public String ReferenceDocumentItem                                         {get;set;}
        public String ReferenceDocumentCategory                                     {get;set;}
        public String Batch                                                         {get;set;}
        public String Plant                                                         {get;set;}
        public String commodity														{get;set;}
        public String countryOfOrigin												{get;set;}
        public boolean designatedBatch												{get;set;}
        public String higherLevelItem												{get;set;}
        public boolean BatchedItem                                                  {get;set;}
        public boolean showSelectBatchLink											{get;set;}
        public boolean showSelectPlantLink											{get;set;}
        public String shipMethod													{get;set;}
        public Boolean overrideShipMethod											{get;set;}

        public String freeGoodsAgent												{get;set;}
        public String freeGoodsCostCenter											{get;set;}
		public String freeGoodsReason												{get;set;} 
        
        public String discountType													{get;set;} //Order Edit related - added by HKinney 2.16.2017
        public Decimal discountValue												{get;set;} //Order Edit related - added by HKinney 2.16.2017
        public String discountCurrency												{get;set;} //Order Edit related - added by HKinney 2.16.2017
		
		public boolean isItemFromSAP                                                {get;set;} //Order Edit related - added by HKinney 2.16.2017
		public boolean isDelivered                                                  {get;set;} //Order Edit related - added by HKinney 2.16.2017

		public Boolean isParentBOMItem												{get;set;} //Added by HKinney 4.3.2017
        public String  parentBOMMaterialNumber                                      {get;set;} //Added by HKinney 4.6.2017 to aid in deletion/re-add of BOMs and children
        
        //default constructor 
        public OliResult(){
            // default values 
            orderItem                 = new OrderItem();
            remove                    = false;
            isBackOrdered             = false;
            simulateCheck             = false;
            quantityConfirmed         = 0;
            sapPrice                  = 0;
            savings                   = 0;
            externalItemNumber        = 0;
            totalPrice                = 0;
            unitPrice                 = 0;
            netPrice                  = 0;
            points                    = 0;
            discount                  = 0;
            shippingCharges           = 0;
            freightRate 			  = 0;
            freightChanged 			  = false;
            tax                       = 0;
            vat						  = 0;
            netSubtotal3              = 0;
            netValue				  = 0;
            
            prices = new List<Pricing>();
            discounts = new List<Pricing>();
            taxes = new List<Pricing>();
            
            style                     = 'background-color:#ffc;';
            className				='';
            currencyISOCode           = '';
            itemNumber                = '';
            itemCategory              = '';
            materialText              = '';            
            expirationDate            = '';
            materialGroup             = '';
            ReferenceDocument         = '';
            ReferenceDocumentItem     = '';
            ReferenceDocumentCategory = '';
            Batch                     = ''; 
            Plant                     = '';
            commodity				  = '';
            countryOfOrigin			  = '';
            designatedBatch 		  = false;
            higherLevelItem			  = '';
            BatchedItem               = false;
            showSelectBatchLink       = false;
            showSelectPlantLink       = false;
            shipMethod			= '';
            overrideShipMethod  = false;

            freeGoodsAgent			 = '';
            freeGoodsCostCenter		 = '';
            freeGoodsReason	 = '';
            
            discountType			  = '';    //Order Edit related - added by HKinney 2.16.2017
            discountValue			  = null;  //Order Edit related - added by HKinney 2.16.2017
            discountCurrency		  = '';    //Order Edit related - added by HKinney 2.16.2017
            
            isItemFromSAP             = false; //Order Edit related - added by HKinney 2.16.2017
            isDelivered               = false; //Order Edit related - added by HKinney 2.16.2017

            isParentBOMItem           = false; //Added by HKinney 4.3.2017
            parentBOMMaterialNumber   = '';    //Added by HKinney 4.6.2017
            
        }
    }
    
    public class Pricing {
        public String description {get;set;}
        public Decimal rate {get;set;}
        public Decimal value {get;set;}
        
    }
}