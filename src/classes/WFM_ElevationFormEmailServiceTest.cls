@isTest
private class WFM_ElevationFormEmailServiceTest {
    
    static testMethod void handleInboundEmailTest() {

        // Create 10 Accounts
        List<Account> listOfAccounts = new List<Account>();
        for(Integer i=0; i<10; i++){
            listOfAccounts.add(
                new Account(
                    Name = 'TestAccount_' + i,
                    SAP_Customer_Number__c = '10000' + i,
                    ShippingCountryCode = 'US',
                    ShippingStateCode = 'PA'
                )
            );
        }
        insert listOfAccounts;

        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        // setup the data for the email
        email.subject = 'Test DX Elevation Form';
        email.plainTextBody = 'Hello, here is a test for DX Elevation Form' + '\r\n'
            + 'SAP_Number : 100005' + '\r\n'
            + WFM_Constants.ELEVATION_FORM_ESTIMATED_INSTALLATION_DATE + ': 05/20/1972 \r\n'
            + 'Other Content'
        ;
        email.fromname = 'GOB Bluth';
        env.fromAddress = 'gob@bluth.com';
        email.toAddresses = new List<String>{WFM_Constants.ELEVATION_FORM_EMAIL_ADDRESS_DX + '@bluth.com'};

        // call the email service class and test it with the data in the testMethod
        WFM_ElevationFormEmailService emailService = new WFM_ElevationFormEmailService();
        emailService.handleInboundEmail(email, env);

        // query for the work order the email service created
        List<WorkOrder> listVisitWOs = [
            SELECT      Id, AccountId, Account.SAP_Customer_Number__c, Product_Type__c, Subject, Due_Date__c
            FROM        WorkOrder
            WHERE       RecordTypeId = :WFM_Constants.WORK_ORDER_VISIT_REQUEST_RECORDTYPE_ID
        ];

      System.assertEquals(listVisitWOs.size(), 1);
      System.assertEquals(listVisitWOs.get(0).Account.SAP_Customer_Number__c, '100005');
      System.assertEquals(listVisitWOs.get(0).Subject, 'Test DX Elevation Form');
      System.assertEquals(listVisitWOs.get(0).Due_Date__c, Datetime.newInstance(1972, 5, 20));
      System.assertEquals(listVisitWOs.get(0).Product_Type__c, WFM_Constants.WORK_ORDER_PRODUCT_TYPE_DX);

    }
    
}