public with sharing class LogAnInteractionExtension {
    
    public Task task;
    public string taskId;
    public Set<Id> accContactIds = new Set<Id>();        
    public Set<Id> cmpgnContactIds = new Set<Id>();
    public List<Campaign> relatedCampaigns  {get;set;}
    public Account thisAccount {get;set;}
    public String AccName {get;set;}
    public list<CampaignWrapper> CampaignWrapperList {get;set;}
    private Task currentTaskRecord;
    public Account currentAccountRecord;
    public Contact currentContactRecord;
    public string LeadId;
    public ID accId;
    public Account acc {get;set;}
    Set<Id> selectedIdCamp = new Set<Id>();
    public List<CampaignMember> campMem= new List<CampaignMember>();
    public List<CampaignMember> updateConMem= new List<CampaignMember>(); 
    public static final String DECLINED_CM_STATUS =  'Declined'; 
    public static final String FULFILLED_CM_STATUS = 'Fulfilled';
    private final string Campaign_Activity_RecType  = Label.RT_CampaignActivity;
    public boolean isWater{get;set;}
    @testVisible private boolean testRun{get;set;}
    
    ApexPages.Standardcontroller controller;
    
    String CAMPAIGN_ACTIVITY_RECTYPEID = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Campaign_Activity_RecType).getRecordTypeId();
    
    //Controller
    public LogAnInteractionExtension(ApexPages.StandardController controller) {
        //check to see if profile is water
        isWater = false;
        testRun = false;
        
        String profileName=[Select Id,Name from Profile where Id=:userinfo.getProfileId()].Name;
            for(WaterProfiles__c setting : WaterProfiles__c.getAll().values()){
                if (setting.Profile_Name__c == profileName) {
                    isWater=true;
                } 
            }
        
        this.task = (Task)controller.getRecord(); 
        this.task.Status = 'Completed';
        this.task.ActivityDate = system.today();
        this.task.Interaction_Origin__c = ''; 
        this.task.RecordTypeId = ApexPages.currentPage().getParameters().get('RecordType');
        //System.debug('@@Record Type' + this.task.RecordTypeId);
        this.task.Date_of_Activity__c = date.today();
        this.task.order__c = ApexPages.currentPage().getParameters().get('orderId');
        User u = [Select Id From User Where id = :UserInfo.getUserId()];
        task.OwnerId = u.Id;    
        this.controller = controller;
        
        if(isWater || testRun){
            LeadId = ApexPages.currentPage().getParameters().get('who_id');
            if(!String.isBlank(ApexPages.currentPage().getParameters().get('what_id'))){
                System.debug('ACC' + ApexPages.currentPage().getParameters().get('what_id'));
                this.currentAccountRecord = [Select Id From Account Where Id = :ApexPages.currentPage().getParameters().get('what_id')];
                accId = currentAccountRecord.Id;
            }
            else if(!String.isBlank(ApexPages.currentPage().getParameters().get('who_id')))
            {
                task.whoId = LeadId; 
            }
            
            else{
                
                this.currentContactRecord = [SELECT Id, AccountID from Contact WHERE ID =:ApexPages.currentPage().getParameters().get('who_id')];
                accId = currentContactRecord.AccountID;
            }
            
        }
        else{
            //fetch all the contacts present on the Account
            if(!String.isBlank(ApexPages.currentPage().getParameters().get('what_id'))){
                this.currentAccountRecord = [Select Id From Account Where Id = :ApexPages.currentPage().getParameters().get('what_id')];
                accId = currentAccountRecord.Id;
            }
            else if(!String.isBlank(ApexPages.currentPage().getParameters().get('who_id'))){
                this.currentContactRecord = [SELECT Id, AccountID from Contact WHERE ID =:ApexPages.currentPage().getParameters().get('who_id')];
                accId = currentContactRecord.AccountID;
                task.whoId = ApexPages.currentPage().getParameters().get('who_id');
                
            }
            
        }
        if(accId != null){
            task.WhatId = accId;
        }
        thisAccount = new Account();
        for(Account acc : [select id,Name,Top_200_Ranking__c,(select Id,Name from Contacts )  From Account where id = :accId ])
        {    
            thisAccount  = acc;
            
            for(Contact cnt: acc.contacts){
                accContactIds.add(cnt.Id);
            }
        }
        //fetch all the campaigns related to the Account
        relatedCampaigns = new List<Campaign>();
        getrelatedcampains();
        CampaignWrapperList = new List<CampaignWrapper>(); 
        
        Map<Id, List<SelectOption>> campaignRelatedStatus = new Map<Id, List<SelectOption>>();
        for(CampaignMemberStatus cms : [Select Id, CampaignId, Label From CampaignMemberStatus WHERE CampaignId IN :relatedCampaigns ORDER BY CampaignId]){
            SelectOption option = new SelectOption(cms.Label, cms.Label);
            if(!campaignRelatedStatus.containsKey(cms.CampaignId)){
                campaignRelatedStatus.put(cms.CampaignId, new List<SelectOption>{option});
            }
            else{
                List<SelectOption> existingValues = new List<SelectOption>();
                existingValues = campaignRelatedStatus.get(cms.CampaignId);
                existingValues.add(option);
                campaignRelatedStatus.put(cms.CampaignId,existingValues);
            }   
        }
        
        for(Campaign cmp : relatedCampaigns)
        { 
            for(CampaignMember cmpmem : cmp.CampaignMembers)
            { 
                CampaignWrapperList.add(new CampaignWrapper (cmp, cmpmem.Status, cmpmem.Name, cmpmem, campaignRelatedStatus.get(cmp.Id))); 
            }
        } 
    }
    
        public void getrelatedcampains()
        {
          //   Rama:- US26573 New logic for  only ISR profiles
          
            User usrObj = [select profile.Name from User where id =:UserInfo.getUserId() ];
            Map<String,Profile_Name__c> mapProfileName= new Map<String,Profile_Name__c>();
            
            For (Profile_Name__c profileName : Profile_Name__c.getall().Values() )
            {
                mapProfileName.put(profileName.name ,profileName);
            }
            
            if( mapProfileName.containsKey(usrObj.profile.Name) )
            {
                String businessType = Label.CampaignBusinessUnit;
                List<String> lstBusinessType = businessType.split(',');

            
                for(Campaign c : [Select Name, status, StartDate, Type, Id, 
                                    (Select Name, CampaignId, ContactId, Status, HasResponded, Contact.AccountId
                                        from CampaignMembers 
                                        where Contact.AccountId  =:accId 
                                        and status!=: DECLINED_CM_STATUS 
                                        and status!=: FULFILLED_CM_STATUS 
                                        ORDER By LastModifiedDate DESC ) 
                                        from Campaign where isActive = true 
                                        and Business_Unit__c in :lstBusinessType
                                        ORDER BY Campaign.Type,Campaign.StartDate DESC
                                        ])
                {
                    relatedCampaigns.add(c);
                }
            }        
            else
            { // Old logic for all  profile
                for(Campaign c : [Select Name, status, StartDate, Type, Id, 
                                    (Select Name, CampaignId, ContactId, Status, HasResponded, Contact.AccountId
                                        from CampaignMembers 
                                        where Contact.AccountId  =:accId 
                                        and status!=: DECLINED_CM_STATUS 
                                        and status!=: FULFILLED_CM_STATUS 
                                        ORDER By LastModifiedDate DESC ) 
                                        from Campaign where isActive = true ])
                {
                    relatedCampaigns.add(c);
                }
            }                 
                
        }
    public class CampaignWrapper {
        //Fetch Campaign details
        public Campaign campgn {get; set;}
        public String CampStatus {get; set;} 
        public List<SelectOption> statusValues   {get;set;}
        public String campMemName {get; set;}
        public CampaignMember campgnMember {get; set;}
        //select checkbox
        public Boolean selectCheck  {get;set;}
        public CampaignWrapper(Campaign c, String CampStatus, String campMemName, CampaignMember campgnMember, List<SelectOption> statuses) {
            campgn = c;
            selectCheck= false;
            this.CampStatus = CampStatus; 
            this.campMemName = campMemName;
            this.campgnMember = campgnMember;
            statusValues = statuses;
        }
    }
    
    public PageReference save() {
        
        String selectedcampId  = ''; 
        task.Top_200_Account__c = String.valueOf(thisAccount.Top_200_Ranking__c);
        
        List<Task> newtaskList = new List<Task>();
        Task tempTask;
        if(!CampaignWrapperList.isEmpty()) { 
            for(CampaignWrapper cm : CampaignWrapperList){ 
                
                if(cm.selectCheck == true) {
                    
                tempTask = new Task(Subject = task.Subject,RecordTypeId = CAMPAIGN_ACTIVITY_RECTYPEID,Status =task.Status,
                        Top_200_Account__c = task.Top_200_Account__c,Interaction_Origin__c = task.Interaction_Origin__c,
                        Activity_Account__c = task.WhatId,Activity_Descriptions__c = task.Activity_Descriptions__c,WhatId = cm.campgn.id, 
                        Campaign_Member_Name__c = cm.campMemName , Campaign_Member_Status__c = cm.CampStatus );
                newtaskList.add(tempTask);
                    
                    cm.campgnMember.Status = cm.CampStatus; 
                    updateConMem.add(cm.campgnMember);
                }
                if(cm.selectCheck) { 
                    selectedIdCamp.add(cm.campgnMember.Id); 
                    if(selectedcampId =='') 
                        selectedcampId = cm.campgnMember.Id;
                    else { 
                        selectedcampId = selectedcampId + ';'+cm.campgnMember.Id; 
                        
                    } 
                } 
            }
            
            Set<CampaignMember> setCampMembers = new Set<CampaignMember>(updateConMem);
            updateConMem = new List<CampaignMember>(setCampMembers );
             update updateConMem;
            string selectcmpIds= selectedcampId ;
            this.task.selectedcampaign__c= selectcmpIds;
        } 
        try{
            if(!newtaskList.isEmpty()){
                insert newtaskList;
            }
            upsert task;
            PageReference pr = new PageReference('/'+task.Id);
            return pr;
            
        }catch(DMLException e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Log_An_Interaction_Error));
            SystemLoggingService.log(e);
            
            return null;
            
        }catch(Exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addmessage(myMsg);
            
            return null;
        }
        
    }
    public PageReference cancel(){
        PageReference page;
        if( ApexPages.currentPage().getParameters().get('what_id') != null &&
           ApexPages.currentPage().getParameters().get('what_id') ==task.WhatId ){
               page = new PageReference('/'+task.WhatId );
           }
        else{
            page = new PageReference('/'+task.whoId);
            
        }
        page.setRedirect(true);
        return page;
    }
    
    public PageReference saveNew()
    { PageReference pr; 
     try{
         controller.save(); 
         Schema.DescribeSObjectResult describeResult = controller.getRecord().getSObjectType().getDescribe(); 
         //pr = new PageReference('/' + describeResult.getKeyPrefix() + '/e');
         pr = new PageReference('/setup/ui/recordtypeselect.jsp?ent=Task' );
         
         pr.setRedirect(true); return pr; 
     }catch(Exception e)
     {
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage())); return null; 
     }//try 
    }//saveNew 
    
    
}