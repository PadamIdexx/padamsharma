/**************************************************************************************
* Apex Class Name: Test_OrderUpdateManager
*
* Description: Test class for the OrderUpdateManager class.
*
*                   
* Developer           Date              Description
* ------------------------------------------------------------------------------------                 
* Heather Kinney      3.9.2017          initial creation.
*
*************************************************************************************/

@isTest
private class Test_OrderUpdateManager {
    
    @isTest static void getOrderReadDetailsPositive() {
        
        User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
        system.runAs(adminUser){
            // Insert Customer Account
            Account customerAccount = new Account();
            customerAccount = CreateTestClassData.createCustomerAccount();
            
            // Insert Products
            Product2 testProduct = new Product2(Name='SNAP PRO STARTER KIT', Family='test', SAP_MATERIAL_NUMBER__c = String.valueOf(Math.random()).subString(15));
            insert testProduct;
            
            // Insert PriceBook
            Pricebook2 priceBook = CreateTestClassData.reusablePricebook2('IDEXX Standard Price Book');
            insert priceBook;
            
            // Fetch Standard PriceBookId
            Id pricebookId = Test.getStandardPricebookId();
            System.debug('PB2 Id:'+pricebookId);
            // Insert PriceBookEntry
            PricebookEntry stdpriceBookEntry = CreateTestClassData.reusablePriceBookEntry(testProduct.Id, pricebookId,1000);
            stdpriceBookEntry.UseStandardPrice = false;
            insert stdpriceBookEntry;
            
            //Create Order and associate it to Account and Standard Pricebook
            Order originalOrderBeforeModifications = CreateTestClassData.createOrder(customerAccount,pricebookId, customerAccount.CurrencyISOCode);
            originalOrderBeforeModifications.OrderReferenceNumber = '0040095126';
            
            Test.startTest();
            
            Order updatedOrder = originalOrderBeforeModifications;
            Order updatedOrder2 = originalOrderBeforeModifications;
            
            /*
            String snapshotValuesInit = '{"snapshotFieldList": [[{"sapOrderField":"customer","domId":"OrderPageId:formId:pbAddress:partnerfunction","value":"USS1:CE:0000019791","uiGroup":"isHeaderItem"},{"sapOrderField":"street","domId":"OrderPageId:formId:pbAddress:shipStreet","value":"4457 OAKWOOD ROAD","uiGroup":"isHeaderItem"},{"sapOrderField":"city","domId":"OrderPageId:formId:pbAddress:shipCity","value":"OAKWOOD","uiGroup":"isHeaderItem"},{"sapOrderField":"region","domId":"OrderPageId:formId:pbAddress:shipStateId","value":"GA","uiGroup":"isHeaderItem"},{"sapOrderField":"country","domId":"OrderPageId:formId:pbAddress:shipCountryId","value":"US","uiGroup":"isHeaderItem"},{"sapOrderField":"postalCode","domId":"OrderPageId:formId:pbAddress:shipPostalCode","value":"30566","uiGroup":"isHeaderItem"},{"sapOrderField":"name2","domId":"OrderPageId:formId:pbAddress:attentionName2","value":"BLANKVALUE","uiGroup":"isHeaderItem"},{"sapOrderField":"poNumber","domId":"OrderPageId:formId:pbAddress:ponumber","value":"TestOrderWithDisc22222","uiGroup":"isHeaderItem"},{"sapOrderField":"headerText","domId":"OrderPageId:formId:pbAddress:customerFacingHeaderText","value":"test header upd- line item upd and ins - 333......","uiGroup":"isHeaderItem"},{"sapOrderField":"CIG","domId":"OrderPageId:formId:pbAddress:cigId","value":"BLANKVALUE","uiGroup":"isHeaderItem"},{"sapOrderField":"CUG","domId":"OrderPageId:formId:pbAddress:cugId","value":"BLANKVALUE","uiGroup":"isHeaderItem"},{"sapOrderField":"deliveryBlock","domId":"OrderPageId:formId:pbAddress:deliveryBlock","value":"BLANKVALUE","uiGroup":"isHeaderItem"},{"sapOrderField":"billingBlock","domId":"OrderPageId:formId:pbAddress:billingBlock","value":"BLANKVALUE","uiGroup":"isHeaderItem"},{"sapOrderField":"discountType","domId":"OrderPageId:formId:pbAddress:discountType","value":"BLANKVALUE","uiGroup":"isHeaderItem"},{"sapOrderField":"discountValue","domId":"OrderPageId:formId:pbAddress:discountValue","value":"BLANKVALUE","uiGroup":"isHeaderItem"}]]}';
            String snapshotValuesModified = '{"snapshotFieldList": [[{"sapOrderField":"customer","domId":"OrderPageId:formId:pbAddress:partnerfunction","value":"USS1:CE:0000019791","uiGroup":"isHeaderItem"},{"sapOrderField":"street","domId":"OrderPageId:formId:pbAddress:shipStreet","value":"4457 OAKWOOD ROAD","uiGroup":"isHeaderItem"},{"sapOrderField":"city","domId":"OrderPageId:formId:pbAddress:shipCity","value":"OAKWOOD","uiGroup":"isHeaderItem"},{"sapOrderField":"region","domId":"OrderPageId:formId:pbAddress:shipStateId","value":"GA","uiGroup":"isHeaderItem"},{"sapOrderField":"country","domId":"OrderPageId:formId:pbAddress:shipCountryId","value":"US","uiGroup":"isHeaderItem"},{"sapOrderField":"postalCode","domId":"OrderPageId:formId:pbAddress:shipPostalCode","value":"30566","uiGroup":"isHeaderItem"},{"sapOrderField":"name2","domId":"OrderPageId:formId:pbAddress:attentionName2","value":"BLANKVALUE","uiGroup":"isHeaderItem"},{"sapOrderField":"poNumber","domId":"OrderPageId:formId:pbAddress:ponumber","value":"TestOrderWithDisc22222","uiGroup":"isHeaderItem"},{"sapOrderField":"headerText","domId":"OrderPageId:formId:pbAddress:customerFacingHeaderText","value":"test header upd- line item upd and ins - 333......","uiGroup":"isHeaderItem"},{"sapOrderField":"CIG","domId":"OrderPageId:formId:pbAddress:cigId","value":"BLANKVALUE","uiGroup":"isHeaderItem"},{"sapOrderField":"CUG","domId":"OrderPageId:formId:pbAddress:cugId","value":"BLANKVALUE","uiGroup":"isHeaderItem"},{"sapOrderField":"deliveryBlock","domId":"OrderPageId:formId:pbAddress:deliveryBlock","value":"04","uiGroup":"isHeaderItem"},{"sapOrderField":"billingBlock","domId":"OrderPageId:formId:pbAddress:billingBlock","value":"13","uiGroup":"isHeaderItem"},{"sapOrderField":"discountType","domId":"OrderPageId:formId:pbAddress:discountType","value":"BLANKVALUE","uiGroup":"isHeaderItem"},{"sapOrderField":"discountValue","domId":"OrderPageId:formId:pbAddress:discountValue","value":"BLANKVALUE","uiGroup":"isHeaderItem"}]]}';
            String snapshotValuesLineItemInit = '';
            String snapshotValuesLineItemsModified = '';
            */
            SAPOrder sapOrder = OrderTestDataFactory.createSAPOrder();
            
            SAPOrder.Partner p = new SAPOrder.Partner();
        
            p.partnerType = 'WE';
            p.customer = '0000016242';
            p.name = 'test name';
            p.name2 = 'test name2';
            p.street = '123 Main St.';
            p.country = 'US';
            p.postalCode = '04106';
            p.city = 'Portland';
            p.district = 'x';
            p.region = 'ME';
            p.transportationZone = 'z';
            
            sapOrder.partners.add(p); 
            
            SAPOrder.Item i1 = new SAPOrder.Item();           
    		i1.itemNumber = '00010';
    		i1.externalItemNumber = '1'; 
            i1.material = '11-11111-11';
            i1.speedCode = 'ABC';
            i1.shortText = 'test test test';
            i1.batch = '1234';
            i1.storageLocation = 'x';
            i1.requestedQuantity = 1.00;
            i1.itemCategory = 'x';
            i1.netValue = Decimal.valueOf('1.23');
            i1.currencyCode = 'USD';
            i1.uom = 'x';
            i1.deliveryDate = Date.newInstance(2017, 7, 8);
            i1.requestedQuantity = 1.00;
            i1.confirmedQuantity = 1.00;
            i1.plant = 'x';
            i1.shippingPoint = 'x';
            i1.itemCategory = 'x';
            i1.storageLocation = 'x';
    		i1.materialGroup = 'x';
            i1.totalPrice = 1.00;
            i1.netSubTotal = 1.00;
            i1.netPrice = 1.00;
            i1.shippingCharges = 1.00;
            i1.freightChanged = false;        
            i1.tax = 1.00;
            i1.discount = 1.00;
            i1.discountType = 'x';
            i1.discountValue = 1.0;
            i1.batch = 'x';
            i1.incoterms = 'x';
            i1.batchedItem = true;
            i1.batchExpirationDate = Date.newInstance(2017, 7, 8);
            i1.isDelivered = false;
            i1.freeGoods = true;
            i1.freeGoodsAgent = 'xxxx';
            i1.freeGoodsReason = 'xxxx';
            i1.freeGoodsCostCenter = 'xxxx';
            
            sapOrder.items.add(i1);
            
            List<OrderLineItems.OliResult> uiOriginalFromSAPOrderItemsList = new List<OrderLineItems.OliResult>();
            //UPDATE scenario item
            OrderLineItems.OliResult olItem = new OrderLineItems.OliResult();
            olItem.prodMatNo = '11-11111-11';
            olItem.externalItemNumber = 1;
            olItem.quantityConfirmed = 1;
            olItem.ReferenceDocument = '0040095126';
            olItem.freeGoodsCostCenter = 'testcc';
            olItem.ReferenceDocumentItem = '1';
            olItem.ReferenceDocumentCategory = '1';
            olItem.itemNumber = '00010';
            olItem.Batch = '019089';
            olItem.Plant = 'USP1';
            olItem.higherLevelItem = '11-11111-11';
            
            
            olItem.orderItem.Quantity = 2; //trigger Quantity update
            olItem.orderItem.Material_Number__c = '11-11111-11';
            olItem.orderItem.Ship_Method__c = 'z'; //Trigger shipmethod update
            olItem.orderItem.Ship_Date__c = Date.newInstance(2017, 8, 8); //trigger ShipDate update
            olItem.orderItem.Batch__c = 'TESTB'; //Trigger Batch update
            olItem.orderItem.Plant__c = 'TESTP'; //Trigger Plant update
            olItem.orderItem.Discount_Type__c = 'z'; //Trigger discount type update
            olItem.orderItem.Discount_Value__c = 2.0; //Trigger discount value update
            olItem.orderItem.Free_Goods_Agent__c = 'zzzz'; //Trigger free goods agent update
            olItem.orderItem.Free_Goods_Cost_Center__c = 'zzzz'; //Trigger free goods cost center update
            olItem.orderItem.Free_Goods_Reason__c = 'zzzz'; //Trigger free goods reason update
            olItem.orderItem.Freight_Charge__c = 2.0; //Trigger freight charge update

			uiOriginalFromSAPOrderItemsList.add(olItem);
			
			
		/*	
			//INSERT scenario item
			OrderLineItems.OliResult olItem2 = new OrderLineItems.OliResult();
            olItem2.prodMatNo = '99-99999-99';
            olItem2.externalItemNumber = 1;
            olItem2.quantityConfirmed = 1;
            olItem2.ReferenceDocument = '0040095126';
            olItem2.freeGoodsCostCenter = 'xxxx';
            olItem2.ReferenceDocumentItem = '1';
            olItem2.ReferenceDocumentCategory = '1';
            olItem2.itemNumber = '00010';
            olItem2.Batch = '019089';
            olItem2.Plant = 'USP1';
            olItem2.higherLevelItem = '99-99999-99';
            
            olItem2.orderItem.Quantity = 1;
            //olItem.orderItem.Material_Number__c = '98-14074-00'; //3.13.2017 3.04pm
            olItem2.orderItem.Ship_Method__c = 'x';
            olItem2.orderItem.Ship_Date__c = Date.newInstance(2017, 7, 8);
            olItem2.orderItem.Discount_Type__c = 'x';
            olItem2.orderItem.Free_Goods_Agent__c = 'xxxx';
            olItem2.orderItem.Free_Goods_Cost_Center__c = 'xxxx';
            olItem2.orderItem.Free_Goods_Reason__c = 'xxxx';

			uiOriginalFromSAPOrderItemsList.add(olItem2);
			
		*/	
			
            List<OrderLineItems.OliResult> uiCurrentOrderItemsList = uiOriginalFromSAPOrderItemsList;
            
            //INSERT scenario line item item
			OrderLineItems.OliResult olItem2 = new OrderLineItems.OliResult();
            olItem2.prodMatNo = '99-99999-99';
            olItem2.externalItemNumber = 1;
            olItem2.quantityConfirmed = 1;
            olItem2.ReferenceDocument = '0040095126';
            olItem2.freeGoodsCostCenter = 'xxxx';
            olItem2.ReferenceDocumentItem = '1';
            olItem2.ReferenceDocumentCategory = '1';
            olItem2.itemNumber = '00010';
            olItem2.Batch = '019089';
            olItem2.Plant = 'USP1';
            olItem2.higherLevelItem = '99-99999-99';
            
            olItem2.orderItem.Quantity = 1;
            //olItem.orderItem.Material_Number__c = '98-14074-00'; //3.13.2017 3.04pm
            olItem2.orderItem.Ship_Method__c = 'x';
            olItem2.orderItem.Ship_Date__c = Date.newInstance(2017, 7, 8);
            olItem2.orderItem.Discount_Type__c = 'x';
            olItem2.orderItem.Free_Goods_Agent__c = 'xxxx';
            olItem2.orderItem.Free_Goods_Cost_Center__c = 'xxxx';
            olItem2.orderItem.Free_Goods_Reason__c = 'xxxx';

			uiCurrentOrderItemsList.add(olItem2);            
            
            
            
            
            String languageKey = 'EN';
            String currencyCode = 'USD';
            
            SAPOrder sapOrderToChange2 = sapOrder;
            SAPOrder sapOrderForDeleteTesting = sapOrder;
            SAPOrder sapOrderForNoUpdatesTesting = sapOrder;
            SAPOrder sapOrderForLineItemTesting = sapOrder;
            
            Map<String,String> deletedLineItemMaterialsNotYetEditedInSAP = new Map<String,String>();
            
            boolean hasUpdatedShipTo = true;
            
            OrderUpdateManager orderUpdManager = new OrderUpdateManager();
            
            
            //setting these specific fields below to exercise the private function processOrderHeader in OrderUpdateManager...:
            
            //**************************************************************************************************************************************************            
            //Begin HEADER update testing - UPDATE flags for HEADER - AND - will also trigger Partner Function one time change of address 
            updatedOrder.PoNumber = 'Z';
            updatedOrder.Billing_Block__c = 'Z';
            updatedOrder.DeliveryBlock__c = 'Z';
            updatedOrder.CIG__c = 'Z';
            updatedOrder.CUG__c = 'Z';
            updatedOrder.Shipping_Label_Text__c = 'Z';
            updatedOrder.DiscountType__c = 'Z';
            updatedOrder.Freight_Condition__c = 'Z';
            
            String retValHeaderUpdate = orderUpdManager.processUpdateRequest(sapOrder, originalOrderBeforeModifications, uiOriginalFromSAPOrderItemsList, 
                                               updatedOrder, uiCurrentOrderItemsList, 
                                               languageKey, currencyCode, sapOrderToChange2, deletedLineItemMaterialsNotYetEditedInSAP, hasUpdatedShipTo);
            //End HEADER update testing
            hasUpdatedShipTo = false;                                   
               
            //**************************************************************************************************************************************************      
            //Begin HEADER INSERT/CREATE testing - CREATE flags on HEADER:: blank strings/null fields on sapOrder
            sapOrder.billingBlock = '';
            sapOrder.deliveryBlock = '';
            sapOrder.CIG = '';
            sapOrder.CUG = '';
            sapOrder.headerText = '';     
            sapOrder.discountType = '';
            sapOrder.freightType = '';

            String retValHeaderCreate = orderUpdManager.processUpdateRequest(sapOrder, originalOrderBeforeModifications, uiOriginalFromSAPOrderItemsList, 
                                               updatedOrder, uiCurrentOrderItemsList, 
                                               languageKey, currencyCode, sapOrderToChange2, deletedLineItemMaterialsNotYetEditedInSAP, hasUpdatedShipTo);  
            //End HEADER INSERT/CREATE testing   
            
            
            //**************************************************************************************************************************************************
            //Begin HEADER DELETE testing
            
            sapOrder = sapOrderForDeleteTesting;
            updatedOrder.Billing_Block__c = '';
            updatedOrder.DeliveryBlock__c = '';
            updatedOrder.CIG__c = '';
            updatedOrder.CUG__c = '';
            updatedOrder.Shipping_Label_Text__c = '';
            updatedOrder.DiscountType__c = '';
            updatedOrder.Freight_Condition__c = 'Remove Condition';
            
            deletedLineItemMaterialsNotYetEditedInSAP.put('11-11111-11', '00010');

            String retValHeaderDelete = orderUpdManager.processUpdateRequest(sapOrder, originalOrderBeforeModifications, uiOriginalFromSAPOrderItemsList, 
                                               updatedOrder, uiCurrentOrderItemsList, 
                                               languageKey, currencyCode, sapOrderToChange2, deletedLineItemMaterialsNotYetEditedInSAP, hasUpdatedShipTo);                                                   
                                               
            //End HEADER DELETE testing
            
            
            
            Test.stopTest();
        }
        
    }

}