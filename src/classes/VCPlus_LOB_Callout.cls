global class VCPlus_LOB_Callout implements Database.Batchable<sObject>,
Database.AllowsCallouts{
    
    global final string query;
    global final Set<ID> assetIds;
    
    public static String custId{get;set;}
    public static String session{get;set;}
    public static Map<String, Asset> results{get;set;}
    
    @testVisible private static String endpoint1 =
        WebServiceUtil.retreiveEndpoints(WebServiceUtil.VC_PLUS);
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        String query = 'SELECT SAP_Number_Without_Leading_Zeros__c, '+
            '(SELECT Name, AccountId, Digital_Enabled__c, Telemedicine_Enabled__c, Labs_Enabled__c, IHD_Enabled__c '+
            'From Assets WHERE Name = \'Vet Connect Plus\') ' +
            'FROM Account WHERE SAP_Number_Without_Leading_Zeros__c != null';
        
        return Database.getQueryLocator(query);
        
    }
    
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        Id MyRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('VetConnectPlus').getRecordTypeId();
        List<Account> accts= (List<Account>) scope;
        Map<Id, Account> qNext = 
            new Map<Id, Account>(accts);
        
        List<Asset> toUpdate = new List<Asset>();
        List<Asset> toDelete = new List<Asset>();
        
        results = new Map<String, Asset>();
        
        for(Account a : accts){
            
            Asset ast = new Asset();
            
            if(a.assets.size() > 0){
                ast.RecordTypeId = MyRecTypeId;
                results.put(a.SAP_Number_without_Leading_Zeros__c, a.assets);
                
            }
            else{
                ast.RecordTypeId = MyRecTypeId ;
                ast.AccountId =  a.Id;
                results.put(a.SAP_Number_without_Leading_Zeros__c, ast);
            }
            
        }
        
        if(session == null)getSession();
        
        System.debug('account list ' + results);       
        // insert results.values();
        System.debug('session ' + session);
        
        for(String s : results.keySet()){
            
            Asset ast = new Asset();  
            ast = results.get(s);
            
            Http http = new Http();
            
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setEndpoint(endpoint1+'?sapid=' + s);
            req.setTimeout(120000);
            req.setHeader('sessionId', session);
            
            try{
                HttpResponse resp = http.send(req);
                JSONParser parser = JSON.createParser(resp.getBody());
                System.debug('req ' + req);
                while(parser.nextToken() != null){
                    
                    if(parser.getCurrentToken() == JSONToken.VALUE_STRING){
                        boolean hasVal = false;
                        
                        String jsonString = parser.getText();
                        
                        //possible results are:
                        //REF_LAB, IVLS, DIG, RADIOLOGY, TEL
                        
                        ast.Name = 'Vet Connect Plus';
                        ast.IHD_Enabled__c = boolean.valueOf(jsonString.Contains('IVLS'));
                        hasVal = boolean.valueOf(jsonString.Contains('IVLS'));
                        ast.Labs_Enabled__c = boolean.valueOf(jsonString.Contains('DIG'));
                        hasVal = boolean.valueOf(jsonString.Contains('DIG'));
                        if(!boolean.valueOf(jsonString.Contains('DIG')))ast.Digital_Enabled__c = boolean.valueOf(jsonString.Contains('RADIOLOGY'));
                        hasVal = boolean.valueOf(jsonString.Contains('RADIOLOGY'));
                        ast.Telemedicine_Enabled__c = boolean.valueOf(jsonString.Contains('TEL'));
                        hasVal = boolean.valueOf(jsonString.Contains('TEL'));
                        ast.Labs_Enabled__c = boolean.valueOf(jsonString.Contains('REF_LAB'));
                        hasVal = boolean.valueOf(jsonString.Contains('REF_LAB'));
						
						if(hasVal)toUpdate.add(ast);                     
                       
                        else toDelete.add(ast);
                    }
                        
                }

            }
            catch(exception ex){
                System.debug('Problem with callout ' + ex.getMessage() +' '+ ex.getLineNumber() +' '+ ex.getStackTraceString());
                
            }

            
        }
        
        System.debug('results ' + toUpdate);
        upsert toUpdate;
      //  delete toDelete;
    }
    global void finish(Database.BatchableContext BC){
        
        
    }
    
    
    public static void getSession(){
        APIUser__c creds = APIUser__c.getInstance('API USER');
        if(creds !=null){
            String sessionId = Login.login(creds.Username__c,creds.Password__c+creds.Security_Token__c);
            session = sessionId;
        }  
    }
    
    
    
}