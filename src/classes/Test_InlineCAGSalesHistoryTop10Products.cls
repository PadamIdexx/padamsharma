/**
*       @author Mansi Maini
*       @date   18/01/2016
        @description   This is a test class for covering InlineCAGSalesHistryTop10ProductsQtyCtlr 
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Mansi Maini                     18/01/2016         Created
        Aditya Sangawar                 28/01/2016         Modified - Added StartTest,StopTest & Assert
**/

@isTest (SeeAllData=False)
public With Sharing class Test_InlineCAGSalesHistoryTop10Products {
    static testMethod void Test_CreateSalesTop10(){
        
        list<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);  
        for(Customer_Sales_History__c tempCSH : lstCSH){
                tempCSH.Quantity_Current_Month__c = 123;
                tempCSH.Quantity_1_Month_Ago__c = 245;
                tempCSH.Quantity_2_Months_Ago__c = 1456;
                tempCSH.Quantity_3_Months_Ago__c = 1516;
                tempCSH.Product_Line__c = 'CHEMISTRY';
                tempCSH.Product_Group__c = 'CATALYST SLIDES';
             
        }
        update lstCSH;
            
            Test.startTest();           
                Account acc = [select id,name from Account where id =: lstCSH[0].Account__c];           
                ApexPages.StandardController controller = new ApexPages.StandardController(acc);
                InlineCAGSalesHistryTop10ProductsQtyCtlr testcontructor = new InlineCAGSalesHistryTop10ProductsQtyCtlr(controller);
                
                list<Customer_Sales_History__c> CSH = [select Product_Line__c from Customer_Sales_History__c where Product_Line__c =: lstCSH[0].Product_Line__c]; 
                System.assertEquals(CSH[0].Product_Line__c, 'CHEMISTRY');

            Test.stopTest();
         }
         
}