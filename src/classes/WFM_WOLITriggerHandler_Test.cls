@isTest
private class WFM_WOLITriggerHandler_Test{
  @testSetup
  static void setupTestData(){
    test.startTest();
    Account AccObj= new Account(Name= 'EMEA Account', ShippingCountryCode = 'ES');
    Insert AccObj; 
    
    // Insert a WO and 2 relatred WOLIs, 1 Intall and other Non Install
    WorkOrder workOrderObj = new WorkOrder(Product_Type__c = 'DX', AccountId = AccObj.Id);
    Insert workOrderObj; 
    WorkOrderLineItem workorderlineitem_Obj = new WorkOrderLineItem(WorkOrderId = workOrderObj.id,RecordTypeId = WFM_Constants.WORK_ORDER_LINE_ITEM_NON_INSTALL_RECORDTYPE_ID );
    Insert workorderlineitem_Obj; 
    WorkOrderLineItem workorderlineitem_Obj2 = new WorkOrderLineItem(WorkOrderId = workOrderObj.id,RecordTypeId = WFM_Constants.WORK_ORDER_LINE_ITEM_INSTALL_RECORDTYPE_ID );
    Insert workorderlineitem_Obj2;
    test.stopTest();
  }
  static testMethod void test_UpdateProductOrService(){
    List<WorkOrder> workOrderObj  =  [SELECT EMEA_Country__c from WorkOrder];
    System.assertEquals(true,workOrderObj.size()>0);
    List<WorkOrderLineItem> workorderlineitem_Obj  =  [SELECT WorkOrderId,EMEA_Country__c from WorkOrderLineItem];
    System.assertEquals(true,workorderlineitem_Obj.size()>0);
     List<WorkOrderLineItem> workorderlineitem_Obj2  =  [SELECT WorkOrderId,EMEA_Country__c from WorkOrderLineItem];
    System.assertEquals(true,workorderlineitem_Obj2.size()>0);
    WFM_WOLITriggerHandler obj01 = new WFM_WOLITriggerHandler();
    WFM_WOLITriggerHandler.UpdateProductOrService(new List<WorkOrderLineItem>());
  }
     
}