public class LabImagesCallout {
    
    public List<String> result{get;set;}
    public String image{get;set;}
    public  String Id{get;set;}
    public Map<String, String> guidMap{get;set;}
    public static Blob hexRes{get;set;}
    public integer getStatus;

    
    private String endpoint1 =
        WebServiceUtil.retreiveEndpoints(WebServiceUtil.LynxxImages);
    
    private String endpoint2 =
        WebServiceUtil.retreiveEndpoints(WebServiceUtil.USLabsForm);
    
    private static String endpoint3 =
        WebServiceUtil.retreiveEndpoints(WebServiceUtil.USLabs);
    
    private static String envelopeEndpoint =
        WebServiceUtil.retreiveEndpoints(WebServiceUtil.BEACON_ENDPOINT);
    
    
    public Map<String, String> fetchGuids(List<String> lims, String d1, String d2){
        
        if(Test.isRunningTest()){
            endpoint1 = 'Mock1';
            endpoint2 = 'Mock2';
            endpoint3 = 'Mock3';
        }
        result = new List<String>();
        guidMap = new Map<String, String>();
        String ns = 'http://schemas.xmlsoap.org/soap/envelope/';
        String wsa = 'http://idexx.com/uslabs/types';
        Http http = new Http();
        
        String SoapXMLBody;
        for(String s : lims){
         
            SoapXMLBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:usl="ld:logical/USLabsForm_ws">'   +
                '<soapenv:Header/>    <soapenv:Body>       <usl:findByCustomerAndDate>          <usl:customerId>'+ s +'</usl:customerId>'+
                '<usl:startDate>'+ d2 +'</usl:startDate> <usl:endDate>'+ d1 +'</usl:endDate>'+
                '</usl:findByCustomerAndDate>    </soapenv:Body> </soapenv:Envelope>';
            
            
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setEndpoint(endpoint2);
            req.setTimeout(120000);
            req.setHeader('sessionId', UserInfo.getSessionId());
            req.setBody(SoapXMLBody);
            HttpResponse resp = http.send(req);
            getStatus = resp.getStatusCode();
            if (getStatus == 200){
            Dom.Document body = resp.getBodyDocument();
            Dom.XmlNode root = body.getRootElement();
            system.debug('*******Http RESPONSE Antrim***'+resp);
            system.debug('*****response body Antrim***'+resp.getbody());
            system.debug('*****response get body Antrim document***'+resp.getBodyDocument());
            system.debug('*****response get root Antrim***'+root);  
            render(root);
            }
        }
        
        return guidMap;
        
    }
    public Map<String, String> fetchLynx(List<String> acc){
        if(Test.isRunningTest()){
            endpoint1 = 'Mock1';
            endpoint2 = 'Mock2';
            endpoint3 = 'Mock3';
        }
        result = new List<String>();
        guidMap = new Map<String, String>();
        Http http = new Http();
        
        String SoapXMLBody;
        for(String a : acc){
          
            SoapXMLBody ='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:imag="ld:Image/logical/Image_ws"><soapenv:Header/><soapenv:Body><imag:getImageIdsByAccessionId><imag:accessionId>'+a+'</imag:accessionId></imag:getImageIdsByAccessionId></soapenv:Body></soapenv:Envelope>';
            
            
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setEndpoint(endpoint1);
            //req.setEndpoint('https://qabeacon.idexx.com/beacon/sfdc/ApplicationServicesLABS/ProxyServices/LynxxImageService');
            //req.setEndpoint('https://stbeacon.idexx.com/beacon/sfdc/ApplicationServicesLABS/ProxyServices/LynxxImageService');
            req.setHeader('SOAPAction', '"ld:Image/logical/Image_ws/getImageIdsByAccessionId"');
            //req.setHeader('Content-Type','text/xml;charset=UTF-8');
            //req.setHeader('Accept-Encoding', 'gzip,deflate');
            req.setTimeout(120000);
            req.setHeader('sessionId', UserInfo.getSessionId());
            req.setBody(SoapXMLBody);
            system.debug('*******SOAP XML BODY***'+SoapXMLBody);
            system.debug('******Accession ID loop***'+a);
            HttpResponse resp = http.send(req);
            System.debug(logginglevel.INFO, resp);
            getStatus = resp.getStatusCode();
            if (getStatus == 200){
            Dom.Document body = resp.getBodyDocument();
            Dom.XmlNode root = body.getRootElement();
            system.debug('*******Http RESPONSE***'+resp);
            system.debug('*****response body lynxx***'+resp.getbody());
            system.debug('*****response get body lynxxdocument***'+resp.getBodyDocument());
            system.debug('*****response get root Lynxx***'+root);
            renderL(a,root);
            }
            
            
        }        
        return guidMap;
        
    }
    public void render(Dom.XmlNode root){    
        
        Dom.XMLNode Body= root.getChildElements()[1];        
        for(Dom.XMLNode child : Body.getChildElements()) {
            for(Dom.XMLNode subchild : child.getChildElements()) {
            
            if(subchild.getChildElement('RequisitionId','http://idexx.com/uslabs/types') != null)
                guidMap.put(subchild.getChildElement('RequisitionId','http://idexx.com/uslabs/types').getText(),
                            subchild.getChildElement('ImageGuid','http://idexx.com/uslabs/types').getText());
                
            }
        }                
    }
    public void renderL(String a, Dom.XmlNode root){    
        system.debug('******Values for Accession id in DOMXML NOde****'+a);
        system.debug('******Values for root in DOMXML NOde****'+root);
        
        Dom.XMLNode Body= root.getChildElements()[1];        
        for(Dom.XMLNode child : Body.getChildElements()) {
            for(Dom.XMLNode subchild : child.getChildElements()) {
                System.debug('child ' + subchild);
                if(subchild.getChildElement('ImageId','http://idexx.com/lims/domain/imageId') != null)
                if(!test.isRunningTest()) {guidMap.put(a,subchild.getChildElement('ImageId','http://idexx.com/lims/domain/imageId').getText());}
              }
        }                
    }
    
    
}