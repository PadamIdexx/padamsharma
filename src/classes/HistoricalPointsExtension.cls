/**************************************************************************************
Apex Class Name  : HistoricalPointsExtension

                   
* Developer         Date                   Description
* ----------------------------------------------------------------------------                 
* Pooja Wade       23-Dec-2015        Original Version
 Modified By:  Lakshmi.Quinn  01/31/2017  For DE8355 
 NOTES: Certain Transaction Types were coming null and by adding the bellow ‘IF’ took care of the DESCRIPTION issue.   
        SAP holds the Transaction Description in two different filed – ‘TRANS_TEXT’ for DEPOSITS Type
        and ‘MATERIAL_DESCRIPTION’ for ACTUAL POINTS REDEMPTION Type.
*************************************************************************************/
public with sharing class HistoricalPointsExtension {
     Account acct;
     String SapCustomerNo;
    public static boolean isResponseNull {get;set;}   
    public static String errorMsg {get;set;}   
    List<ExternalClassData> listExt;
    idexxiComSapxiPntsservicesCalc.DT_PNTS_DETAIL_HIST_RESPONSE response;
    
    
    public HistoricalPointsExtension(ApexPages.StandardController controller) {
     
      //System.debug(controller.getRecord().get('SAP_Customer_Number__c'));
      
      Id acctid = controller.getId();
      acct = [SELECT Id, SAP_Customer_Number__c FROM Account WHERE Id = :acctid];  
      SapCustomerNo = acct.SAP_Customer_Number__c;
      isResponseNull = false;
      errorMsg = System.Label.ProgramEnrollmentsErrorMessageNoRecords;
      listExt  = new List<ExternalClassData>();
      response = new idexxiComSapxiPntsservicesCalc.DT_PNTS_DETAIL_HIST_RESPONSE();
      //getHistoricalData(SapCustomerNo);
      //setExternalData();
      
    }
    
    public List<ExternalClassData> getListExt(){
        return listExt;
    }
    
    public void setExternalData(){
        listExt  = new List<ExternalClassData>();
    }
    
    public void getHistoricalData(){
    try{
     
    /*  Date dDate = system.today().adddays(-(365*5));
      System.debug('Date' + dDate);
      String Yyear = String.valueOf(dDate.year());
      System.debug( '@@Year ' + Yyear); */
        //idexxiComSapxiPntsservicesCalc.DT_PNTS_DETAIL_HIST_RESPONSE response = new idexxiComSapxiPntsservicesCalc.DT_PNTS_DETAIL_HIST_RESPONSE();
        idexxiComSapxiPntsservicesCalc.HTTPS_Port request= new idexxiComSapxiPntsservicesCalc.HTTPS_Port();
        listExt  = new List<ExternalClassData>();
        //System.debug('SAP' + SapCustomerNo);
        // response= request.PointsHistoryRead(SapCustomerNo);
         
        if(!Test.isRunningTest()){
          response= request.PointsHistoryRead(SapCustomerNo);
        }
        else{
            //System.debug('is test coming');
            response = new idexxiComSapxiPntsservicesCalc.DT_PNTS_DETAIL_HIST_RESPONSE();
            response.TRANSACTION_DETAIL = new List<idexxiComSapxiPntsservicesCalc.TRANSACTION_DETAIL_element>();
            for(integer j=0;j<3;j++) {
                idexxiComSapxiPntsservicesCalc.TRANSACTION_DETAIL_element det = new idexxiComSapxiPntsservicesCalc.TRANSACTION_DETAIL_element();
                det.AVAILABLE_BAL = '578.06';
                det.TRANS_DATE = System.today();
                det.TRANS_TYPE = j==0 ? 'POO' : 'CPP';
                response.TRANSACTION_DETAIL.add(det);
            }
        }
        
         System.debug('Response'+ response);
         if(response.TRANSACTION_DETAIL <> null){
     
         for(idexxiComSapxiPntsservicesCalc.TRANSACTION_DETAIL_element a : response.TRANSACTION_DETAIL){
      
         //System.debug('element is:'+a);
        
           ExternalClassData ext = new ExternalClassData();
           if(a.TRANS_TYPE != 'CPP' && a.TRANS_TYPE != 'PPR'){
           ext.avlbal = a.available_bal;
           System.debug('@@DATE'+ a.TRANS_DATE);
           String transDate = String.isNotBlank(String.valueOf(a.TRANS_DATE)) ? String.valueOf(a.TRANS_DATE) : '';
           
           String month = transDate.substring(5,7);
           String day= transDate.substring(8,10);
           String year = transDate.substring(0,4);
           String fDate = month + '/'+day+ '/'+year;
           Date pDate =  Date.NewInstance(Integer.Valueof(year),Integer.Valueof(month),Integer.Valueof(day));
           //System.debug('@@month' + month);
           //System.debug('@@date' + day);
           //System.debug('@@year' + year); 
           ext.transDate = pDate ;
           ext.transAmt  = a.TRANS_AMT;
           ext.transType = a.TYPE_DESC;
           ext.docNumber = a.DOCUMENT_NUMBER;

           //ext.transDesc = a.TRANS_TEXT;
           
           
    //Lakshmi added this for DE8355 01/31/2017 
    //NOTES: Certain Transaction Types were coming null and by adding the bellow ‘IF’ took care of the DESCRIPTION issue.   
    //       SAP holds the Transaction Description in two different filed – ‘TRANS_TEXT’ for DEPOSITS Type
    //       and ‘MATERIAL_DESCRIPTION’ for ACTUAL POINTS REDEMPTION Type.
    
    
           If (a.TRANS_TEXT == null){
            ext.transDesc = a.MATERIAL_DESCRIPTION;}
           Else {
             ext.transDesc = a.TRANS_TEXT;}
           
           listExt.add(ext); 
           //System.debug('@@TRANSACTION DAte' + a.TRANS_DATE);
           //System.debug('@@AVAILABLE' + a.AVAILABLE_BAL);
           //System.debug('@@Doc Number' + a.DOCUMENT_NUMBER); 
           System.debug('******Transaction Text***'+a.TRANS_TEXT);
           system.debug('*****Material Desc***'+a.MATERIAL_DESCRIPTION);
            
            

       }
              }
              response = new idexxiComSapxiPntsservicesCalc.DT_PNTS_DETAIL_HIST_RESPONSE();
            }else{
              //  System.debug('is else it here:');            
                isResponseNull = true;
                errorMsg = System.Label.ProgramEnrollmentsErrorMessageNoRecords;
                //System.debug('@@CHeck' + isResponseNull );
             }
             
             }catch(Exception e){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
                ApexPages.addmessage(myMsg);
                 errorMsg = System.Label.ProgramEnrollmentsErrorMessageNoRecords;
                //System.debug('exception is:'+e.getMessage());
                //System.debug('exception root is:'+e.getStackTraceString());
                //System.debug('exception line is:'+e.getLineNumber());
                //System.debug('exception cause is:'+e.getCause());
                isResponseNull = true;
              //  System.debug('is it here:');
            }
           
            
}

public class ExternalClassData {
        public String avlBal{get;set;}
        public Date transDate{get;set;}
        public String transAmt {get;set;}
        public String transType{get;set;}
        public String docNumber {get;set;}
        public String transDesc {get;set;}
        public ExternalClassData(){
            avlBal = '';
            transDate = system.today();
            transAmt ='';
            transType = '';
            docNumber = '';
            transDesc = '';
        }
    }
    }