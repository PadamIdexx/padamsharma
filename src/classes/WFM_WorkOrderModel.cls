public class WFM_WorkOrderModel {
    
    public static WorkOrder createFollowUpWorkOrder(WorkOrder workOrder){
        WorkOrder followUpWorkOrder = new WorkOrder(
            AccountId = workOrder.AccountId,
            OwnerId = workOrder.OwnerId,
            Priority = WFM_Constants.WORK_ORDER_PRIORITY_MEDIUM,
            Status = WFM_Constants.WORK_ORDER_STATUS_NEW,
            Subject = 'Installation Follow-Up',
            RecordTypeId = WFM_Constants.WORK_ORDER_PROACTIVE_RECORDTYPE_ID,
            ParentWorkOrderId = workOrder.Id,
            Product_Type__c = workOrder.Product_Type__c,
            Due_Date__c = (workOrder.Due_Date__c != null) ? workOrder.Due_Date__c.addDays(35) : workOrder.Due_Date__c
        );
        return followUpWorkOrder;
    }
   
    public static WorkOrder createWorkOrder(Id accountId, String subject, Id recordTypeId, String priority, String status){
        WorkOrder workOrder = new WorkOrder(
            AccountId = accountId,
            Priority = priority,
            Status = status,
            Subject = subject,
            RecordTypeId = recordTypeId

        );
        return workOrder;
    }
    
}