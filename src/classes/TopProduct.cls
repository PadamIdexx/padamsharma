/*
 * Object presennting a top product.  
 */ 
public class TopProduct {
        public OrderItem oItem    {get;set;}
        public PriceBookEntry pbEntry    {get;set;}
        public String prodName {get;set;}
    	public String materialNumber {get;set;}
        public Integer qty    {get;set;}
        public Boolean isSelected {get;set;}
        public String itemCategory {get;set;}
    public String unitOfMeasure {get;set;}
    
        public TopProduct() {
            isSelected = false;
            oItem = new OrderItem();
            pbEntry = new PriceBookEntry();
            prodName = '';
            materialNumber = '';
            qty = 1; 
            itemCategory = '';
            unitOfMeasure = '';
        }    
}