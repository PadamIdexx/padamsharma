/* Class Name : OrderTriggerUtility
 * Description : Utility class for Order and OrderItem trigger. It manages opportunity creation/deletion and summarizes order number and amount on Campaign
 * Created By : Raushan Anand
 * Created On : 07-Sep-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand              07-Sep-2015               Initial version.
 *  * Anudeep Gopagoni           09-Nov-2015               commented lines 42- 46 as the order update operation is taken care in OpportunityTriggerHandler class
 **************************************************************************************/
public with sharing class OrderTriggerUtility {
    
    /* Method Name : buildOppCampOrderRelation
     * Description : This method populates campaign based upon Opportunity if campaign not populated
     * Return Type : void
     * Input Parameter : List of Order - List<Order>
     */
    /*
    public static void buildOppCampOrderRelation(List<Order> orderList){
        List<Id> campaignIdList= new List<Id>();
        Set<Id> opptyIdList = new Set<Id>();
        Map<Id, Id> opptyCampMap = new Map<Id, Id>();
        
        for(Order ord: orderList){
            if(ord.OpportunityId != null){
                opptyIdList.add(ord.OpportunityId);    
            }
        }
        
        for(Opportunity oOppty : [SELECT Id, Name, CampaignId FROM Opportunity WHERE ID IN :opptyIdList]){
            OpptyCampMap.put(oOppty.Id, oOppty.CampaignId);
        }
        
        for(Order ord : orderList){
            if(Trigger.IsInsert){
                if(ord.OpportunityId != NULL && ord.POC_Campaign__c == NULL){
                    if(OpptyCampMap.containsKey(ord.OpportunityId))
                        ord.POC_Campaign__c = OpptyCampMap.get(ord.OpportunityId);     
                }
            }
            else if(Trigger.IsUpdate){
                 if(OpptyCampMap.containsKey(ord.OpportunityId) && OpptyCampMap.get(ord.OpportunityId) != null)
                        ord.POC_Campaign__c = OpptyCampMap.get(ord.OpportunityId);     
                
            }
        }
    }
    */
    /* Method Name : updateCampaignOnOrder
     * Description : Updates total amount on campaign after insertion/updation of any related orderitem.
     * Return Type : void
     * Input Parameter : List of OrderItem - List<OrderItem>
     */
   /* public static void updateCampaignOnOrder(List<OrderItem> orderItemList, List<Order> lstOrders){
        List<Id> campaignIdList= new List<Id>();
        List<Id> orderIdList = new List<Id>();
        List<Campaign> campaignUpdateList= new List<Campaign>();
        if(orderItemList != NULL){
            for(OrderItem ordItem : orderItemList){
                orderIdList.add(ordItem.OrderId);
            }
            List<Order> orderList = [SELECT Id,TotalAmount,POC_Campaign__c FROM Order where Id IN : orderIdList];
            for(Order ord : orderList){
                campaignIdList.add(ord.POC_Campaign__c);
            }
        }
        else if(lstOrders != NULL){
            for(Order ord : lstOrders){
                campaignIdList.add(ord.POC_Campaign__c);
            }
        }
        Map<Id,Double> orderAmountMap = new Map<Id,Double>();
        Map<Id,Integer> orderCount = new Map<Id,Integer>();
        if(campaignIdList != null && (!campaignIdList.isEmpty())){
            Map<Id,Campaign> campaignMap = new Map<Id,campaign>([SELECT Id, Total_Order_Amount__c,Num_Total_Order__c FROM Campaign where Id IN: campaignIdList]);
            List<Order> orderAllList = [SELECT Id,TotalAmount,POC_Campaign__c FROM Order where POC_Campaign__c IN : campaignMap.keySet()];
            for(Order oOrder : orderAllList){
                if(orderAmountMap.containsKey(oOrder.POC_Campaign__c)){   
                    orderAmountMap.put(oOrder.POC_Campaign__c,orderAmountMap.get(oOrder.POC_Campaign__c)+oOrder.TotalAmount);
                }
                else{
                    orderAmountMap.put(oOrder.POC_Campaign__c,oOrder.TotalAmount);
                }
                
                if(orderCount.containsKey(oOrder.POC_Campaign__c)){
                    orderCount.put(oOrder.POC_Campaign__c, orderCount.get(oOrder.POC_Campaign__c) + 1);
                }
                else{
                    orderCount.put(oOrder.POC_Campaign__c, 1);
                }
            }
            for(Id campId : campaignMap.Keyset()){
                if(orderAmountMap.containsKey(campId)){
                    campaignMap.get(campId).Total_Order_Amount__c = orderAmountMap.get(campId);
                }
                
                if(ordercount.containsKey(campId)){
                    campaignMap.get(campId).Num_Total_Order__c = orderCount.get(campId);
                }
                campaignUpdateList.add(campaignMap.values());
            }
            if(Campaign.sObjectType.getDescribe().isUpdateable() && (!campaignUpdateList.isEmpty()))
                UPDATE campaignUpdateList;
        }
    }
    */
    /* Method Name : counterCheckAfterDelete
     * Description : Deletes respective opportunity for each order deleted.Also recalculate the number of order and total amount of order to Campaign.
     * Return Type : void
     * Input Parameter : List of Order - List<Order>
     */
    /*
    public static void counterCheckAfterDelete(List<Order> orderList){
        List<Campaign> campaignUpdateList= new List<Campaign>();
        List<Id> campaignIdList= new List<Id>();
        List<Id> oppIdList= new List<Id>();
        for(Order oOrder : orderList){
            campaignIdList.add(oOrder.POC_Campaign__c);
            oppIdList.add(oOrder.OpportunityId);
        }
        List<Opportunity> oppList = [SELECT Id FROM Opportunity WHERE Id IN: oppIdList];
        Map<Id,Campaign> campaignMap = new Map<Id,campaign>([SELECT Id, Total_Order_Amount__c,Num_Total_Order__c FROM Campaign where Id IN: campaignIdList]);
        for(Id cId : campaignMap.keySet()){
            for(Order oOrder :OrderList){
                if(cId == oOrder.POC_Campaign__c){
                    Campaign camp = campaignMap.get(cId);
                    if(camp.Num_Total_Order__c == 1){
                        camp.Num_Total_Order__c = 0;
                        camp.Total_Order_Amount__c = 0;
                        
                    }
                    else{
                        camp.Num_Total_Order__c -= 1;
                        camp.Total_Order_Amount__c -= oOrder.TotalAmount;
                    }
                    campaignUpdateList.add(camp);
                }
            }
        }
        if(Opportunity.sObjectType.getDescribe().isDeletable() && (!oppList.isEmpty()))
            DELETE oppList;
        if(Campaign.sObjectType.getDescribe().isUpdateable() && (!campaignUpdateList.isEmpty()))
            UPDATE campaignUpdateList;
    }
    */
    /* Method Name : updateCampaignOnOrderItem
     * Description : Rolls up OrderItem count and total price on Campaign
     * Return Type : void
     * Input Parameter : List of OrderItem - List<OrderItem>
     * Date : 11/19/2015
     */
    public static void updateCampaignOnOrderItem(List<OrderItem> orderItemList){
        List<Id> campaignIdList= new List<Id>();
        List<Campaign> campaignUpdateList= new List<Campaign>();
        Map<Id,Double> orderAmountMap = new Map<Id,Double>();
        Map<Id,Set<Id>> orderCountMap = new Map<Id,Set<Id>>();
        Map<Id,Double> orderQuantityMap = new Map<Id,Double>();
        if(!orderItemList.isEmpty()){
            for(OrderItem ordItem : orderItemList){
                if (ordItem.Campaign__c != null) {
                	campaignIdList.add(ordItem.Campaign__c);
                }
            }
        }
        if(!campaignIdList.isEmpty()){
            
            Map<Id,Campaign> campaignMap = new Map<Id,campaign>([SELECT Id, Total_Order_Amount__c,Num_Total_Order__c,Total_Order_Quantity__c FROM Campaign where Id IN: campaignIdList]);
            List<OrderItem> orderIList = [SELECT Id,UnitPrice,Quantity, Campaign__c,OrderId FROM OrderItem WHERE Campaign__c IN:  campaignIdList];
            for(OrderItem oOrder : orderIList){
                if(orderAmountMap.containsKey(oOrder.Campaign__c)){
                    orderAmountMap.put(oOrder.Campaign__c,orderAmountMap.get(oOrder.Campaign__c)+(oOrder.UnitPrice*oOrder.Quantity));
                }
                else{
                    orderAmountMap.put(oOrder.Campaign__c,(oOrder.UnitPrice*oOrder.Quantity));
                }
                
                if(orderCountMap.containsKey(oOrder.Campaign__c)){
                    Set<Id> idSet = orderCountMap.get(oOrder.Campaign__c);
                    idSet.add(oOrder.OrderId);
                    orderCountMap.put(oOrder.Campaign__c, idSet);
                }
                else{
                    Set<Id> idSet = new Set<Id>();
                    idSet.add(oOrder.OrderId);
                    orderCountMap.put(oOrder.Campaign__c, idSet);
                }
                if(orderQuantityMap.containsKey(oOrder.Campaign__c)){
                    orderQuantityMap.put(oOrder.Campaign__c,(orderQuantityMap.get(oOrder.Campaign__c) + oOrder.Quantity));
                }
                else{
                    orderQuantityMap.put(oOrder.Campaign__c,Double.valueOf(oOrder.Quantity));
                }
            }
            for(Id campId : campaignMap.Keyset()){
                if(orderAmountMap.containsKey(campId)){
                    campaignMap.get(campId).Total_Order_Amount__c = orderAmountMap.get(campId);
                }
                else{
                    campaignMap.get(campId).Total_Order_Amount__c = 0;
                }
                
                if(ordercountMap.containsKey(campId)){
                    campaignMap.get(campId).Num_Total_Order__c = orderCountMap.get(campId).size();
                }
                else{
                    campaignMap.get(campId).Num_Total_Order__c = 0;
                }
                if(orderQuantityMap.containsKey(campId)){
                    campaignMap.get(campId).Total_Order_Quantity__c = orderQuantityMap.get(campId);
                }
                else{
                    campaignMap.get(campId).Total_Order_Quantity__c = 0;
                }
                
            }
            campaignUpdateList.addAll(campaignMap.values());
            if(Campaign.sObjectType.getDescribe().isUpdateable() && (!campaignUpdateList.isEmpty()))
                UPDATE campaignUpdateList;
        }
        
    }
    
    /* Method Name : updateCampaignOnOrderDelete
     * Description : Update Total Number and Amount whenever an Order is deleted.
     * Return Type : void
     * Input Parameter : List of Order - List<Order>
     * Date : 11/19/2015
     */
    public static void updateCampaignOnOrderDelete(List<Order> orderList){
        List<Campaign> campaignUpdateList= new List<Campaign>();
        Set<Id> campaignIdSet = new Set<Id>();
        Map<Id,Double> orderAmountMap = new Map<Id,Double>();
        Map<Id,Set<Id>> orderCountMap = new Map<Id,Set<Id>>();
        Map<Id,Double> orderQuantityMap = new Map<Id,Double>();
        
        List<OrderItem> orderIList = [SELECT Id,UnitPrice,Quantity, Campaign__c,OrderId FROM OrderItem WHERE OrderId IN:  OrderList];
        System.debug('OItems is:'+orderIList);
        if(!orderIList.isEmpty()){
            for(OrderItem ordItem : orderIList){
                if(String.isNotBlank(ordItem.Campaign__c)) {
                    campaignIdSet.add(ordItem.Campaign__c);
                }
            }
        }
        System.debug('campaignIdSet is:'+campaignIdSet);
        System.debug('orderAmountMap1 is:'+orderAmountMap);
        if(!campaignIdSet.isEmpty()){ 
            Map<Id,Campaign> campaignMap = new Map<Id,campaign>([SELECT Id, Total_Order_Amount__c,Num_Total_Order__c,Total_Order_Quantity__c FROM Campaign where Id IN: campaignIdSet]);
            for(OrderItem oOrder : orderIList){
                if(orderAmountMap.containsKey(oOrder.Campaign__c) ){   
                    orderAmountMap.put(oOrder.Campaign__c,orderAmountMap.get(oOrder.Campaign__c)+(oOrder.UnitPrice*oOrder.Quantity));
                }
                else if(!orderAmountMap.containsKey(oOrder.Campaign__c) && String.isNotBlank(oOrder.Campaign__c)){
                    orderAmountMap.put(oOrder.Campaign__c,(oOrder.UnitPrice*oOrder.Quantity));
                }
                
                if(orderCountMap.containsKey(oOrder.Campaign__c)){
                    Set<Id> idSet = orderCountMap.get(oOrder.Campaign__c);
                    idSet.add(oOrder.OrderId);
                    orderCountMap.put(oOrder.Campaign__c, idSet);
                }
                else if( !orderCountMap.containsKey(oOrder.Campaign__c) && String.isNotBlank(oOrder.Campaign__c)){
                    Set<Id> idSet = new Set<Id>();
                    idSet.add(oOrder.OrderId);
                    orderCountMap.put(oOrder.Campaign__c, idSet);
                }
                if(orderQuantityMap.containsKey(oOrder.Campaign__c)){   
                    orderQuantityMap.put(oOrder.Campaign__c,(orderQuantityMap.get(oOrder.Campaign__c)+oOrder.Quantity));
                }
                else if( !orderQuantityMap.containsKey(oOrder.Campaign__c) &&  String.isNotBlank(oOrder.Campaign__c)){
                    orderQuantityMap.put(oOrder.Campaign__c,oOrder.Quantity);
                }
            }
            System.debug('orderAmountMap is:'+orderAmountMap);
            System.debug('orderCountMap is:'+orderCountMap);
            System.debug('orderQuantityMap is:'+orderQuantityMap);
            for(Id campId : campaignMap.Keyset()){
                if(orderAmountMap.containsKey(campId)){
                    System.debug('camp record:'+campaignMap.get(campId));
                    System.debug('Order record:'+orderAmountMap.get(campId));
                    campaignMap.get(campId).Total_Order_Amount__c -= orderAmountMap.get(campId);
                }
                else{
                    System.debug('camp record:first'+campaignMap.get(campId));
                    campaignMap.get(campId).Total_Order_Amount__c = 0;
                }
                
                if(ordercountMap.containsKey(campId)){
                    campaignMap.get(campId).Num_Total_Order__c -= orderCountMap.get(campId).size();
                }
                else{
                    campaignMap.get(campId).Num_Total_Order__c = 0;
                } 
                if(orderQuantityMap.containsKey(campId)){
                    campaignMap.get(campId).Total_Order_Quantity__c -= orderQuantityMap.get(campId);
                }
                else{
                    campaignMap.get(campId).Total_Order_Quantity__c = 0;
                } 
            }
            campaignUpdateList.addAll(campaignMap.values());
            if(Campaign.sObjectType.getDescribe().isUpdateable() && (!campaignUpdateList.isEmpty()))
                UPDATE campaignUpdateList;
        }
    }
}