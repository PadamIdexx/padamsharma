/* Helper for sending email for exceptions and other scenarios.  This is similar the Beacon "phone home" 
 * mechanism that sends notifications to a central inbox.
 */ 
public class StatusEmailHandler {
	public final static String ERRORSANDWARNINGS_EMAIL = 'ErrorsAndWarnings';
    public final static String INFO_EMAIL = 'Info';

    public static void sendException(Exception e) {
         System_Email__c email = getEmailAddress(ERRORSANDWARNINGS_EMAIL);

         if (null != email) {
            if (email.Enabled__c) {
                //get error email recipient(s)
                String toEmail = email.Address__c;
                
                String subject = 'Salesforce exception';
        		String body = getEnvironmentInfo() + '<br/>' 
            			+ 'Type: ' + e.getTypeName() + '<br/>' 
            			+ 'Message: ' + e.getMessage() + '<br/>' 
            			+ 'StackTrace: ' + e.getStackTraceString() + '<br/>';
				                
                send('Salesforce', toEmail, subject, body);                
            } else {
                System.debug('Exception email not enabled.');
            }
        }
        
    }

    public static void sendExceptionWithObject(Exception e, String sObjectValues) {
         System_Email__c email = getEmailAddress(ERRORSANDWARNINGS_EMAIL);

         if (null != email) {
            if (email.Enabled__c) {
                //get error email recipient(s)
                String toEmail = email.Address__c;
                
                String subject = 'Salesforce exception';
        		String body = getEnvironmentInfo() + '<br/>' 
            			+ 'Type: ' + e.getTypeName() + '<br/>' 
            			+ 'Message: ' + e.getMessage() + '<br/>' 
            			+ 'StackTrace: ' + e.getStackTraceString() + '<br/><br/>' 
                    	+ 'Object : ' + sObjectValues;
				                
                send('Salesforce', toEmail, subject, body);                
            } else {
                System.debug('Exception email not enabled.');
            }
        }
        
    }
    
    public static void sendObjectContents(String name, String sObjectValues) {
        
        System_Email__c email = getEmailAddress(INFO_EMAIL);
        
        if (null != email) {
            if (email.Enabled__c) {
                //get error email recipient(s)
                String toEmail = email.Address__c;
                
                String subject = 'Salesforce object contents';
		        String body = getEnvironmentInfo() 
            			+ '<br/>' + name + ': ' + sObjectValues;
                
                send('Salesforce', toEmail, subject, body);                
            } else {
                System.debug('Info email not enabled.');
            }
        }
        

    }

    private static String getEnvironmentInfo() {

        String env = 'Organization: ' + UserInfo.getOrganizationName() + ' [' + UserInfo.getOrganizationId() + ']' + '<br/>'
            			+ 'User: ' + UserInfo.getUserName() + '<br/><br/>';
        
        return env;
    }
    
    private static System_Email__c getEmailAddress(String name) {		
        System_Email__c cs = System_Email__c.getValues(name);        
        return cs;
    }
    
    private static void send(String sender, String toEmail, String subject, String body) {
        Messaging.reserveSingleEmailCapacity(2);
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                
        String[] toAddresses = new String[] {toEmail}; 
                
        mail.setToAddresses(toAddresses);
        mail.setBccSender(false);
        
        mail.setSenderDisplayName(sender);
        
        mail.setSubject(subject);
        mail.setPlainTextBody(body);        
        mail.setHtmlBody(body);
        
        if (!Test.isRunningTest()) {        
        	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
}