/**
 * @author        Heather Kinney
 * @description   OrderReadManager implementation.
 *                - Facilitates Order Manager read operations. 
 *                - Used when entering into an existing Order. Retreives SAPOrder information.
 *                - Determines Order Edit status (based on Delivery Status) which is derrived in this class after the SAPOrder 
 *                  is obtained.
 *                - Componentized logic exists here, as opposed to residing in a monolithic controller.
 *           
 *
 *  Modification Log:
 * -----------------------------------------------------------------------------------------------------------------------------
 *  Developer                       Date               Description
 * -----------------------------------------------------------------------------------------------------------------------------
 *  Heather Kinney                  2.2.2017           Initial creation.
 *
 */
public class OrderReadManager {
    
    public static final String DELIVERY_STATUS_NOT_DELIVERED        = 'isNotDelivered';
    public static final String DELIVERY_STATUS_PARTIALLY_DELIVERED  = 'isPartiallyDelivered';
    public static final String DELIVERY_STATUS_COMPLETELY_DELIVERED = 'isCompletelyDelivered';
    
    private boolean showPricing;
    private String orderDeliveryStatus                       {get;set;}
    private List<OrderLineItems.OliResult> uiOrderItemsList;
    private List<OrderItem> orderItemsList;
    private Map<String,String> incotermsMap;
    
    @TestVisible private SAPOrder sapOrderForTest = null; //3.8.2017
    
    private Integer externalItemCount = 0;
    
    public OrderReadManagerWrapper getOrderReadDetails(Order currentOrder) {
		//System.debug(LoggingLevel.ERROR, 'OrderReadManager - getOrderReadDetails. currentOrder = ' + currentOrder);        
        OrderReadManagerWrapper returnWrapper = null;
        SAPOrder readInOrder = null;
        try {
            SAPOrder sapOrderRequest = new SAPOrder(); 
            sapOrderRequest.documentNumber = currentOrder.OrderReferenceNumber;
            sapOrderRequest.language = 'EN'; //TODO: this is hardcoded currently in returnOrderSimulateRequestLPD.
            if (Test.isRunningTest()) { //3.8.2017
                readInOrder = sapOrderForTest;
            } else {
            	readInOrder = OrderManager.getOrder(sapOrderRequest);
            }
            if (readInOrder != null) {
                //Obtain the overall editable state for this Order:
                setEditableStateforOrder(readInOrder);
                
                //set up header info on currentOrder with what we read in from the call to OrderManager.getOrder:
                currentOrder.CIG__c = readInOrder.CIG;
                currentOrder.CUG__c = readInOrder.CUG;
                currentOrder.Shipping_Label_Text__c = readInOrder.headerText;
                currentOrder.PoNumber = readInOrder.poNumber;
                currentOrder.Ship_Method__c = readInOrder.incoterms;
                currentOrder.Billing_Block__c = readInOrder.billingBlock;
                currentOrder.DeliveryBlock__c = readInOrder.deliveryBlock;
                currentOrder.DiscountType__c = readInOrder.discountType;
                currentOrder.Discount__c = readInOrder.discountValue;
                
                currentOrder.Freight_Condition__c = readInOrder.freightType;
                currentOrder.Shipping_Charges__c = readInOrder.freightValue;
                
                //get shipto partner
                List<SAPOrder.Partner> partners = readInOrder.partners;
                for(SAPOrder.partner p: partners){
                	if(p.partnerType == 'WE'){
                		currentOrder.shippingStreet = p.street;
                		currentOrder.shippingCity = p.city;
                		currentOrder.shippingStateCode = p.region;
                		currentOrder.shippingPostalCode = p.postalCode;
                		currentOrder.shippingCountryCode = p.country;
                		currentOrder.name2__c = p.name2;
                		currentOrder.Shipping_Partner_SAP__c = p.customer;
                	}
                }
                
                currentOrder.Payment_Terms__c = readInOrder.paymentTerms; 
                currentOrder.EffectiveDate = readInOrder.requestedDeliveryDate;
              
                setOrderItems(readInOrder);
               
                returnWrapper = new OrderReadManagerWrapper();
                returnWrapper.orderDeliveryStatus = orderDeliveryStatus;
                returnWrapper.sapOrder = readInOrder;
                returnWrapper.currentOrder = currentOrder;
                returnWrapper.uiOrderItems = uiOrderItemsList;
                returnWrapper.currentOrderItems = orderItemsList;
                returnWrapper.incotermsFromSAP = incotermsMap;
                returnWrapper.showPricing = showPricing;
              
                
                return returnWrapper;
                
                //*** NOTE *********************************************************************************
                //The following UI Header Fields/Values are NOT updatable once the order has been submitted to SAP:
                //   - SalesOrg/Division - 001 and 002 Sold To only Partner.
                //   - Bill To
                //   - Payer 
                //   - Order Type Id (Standard, Free Goods)
                //   - Order Origin (email, phone, fax, etc.)
                //Other fields not editable once order has been submitted to SAP:
                //   - Payment Terms   - eg ZB30, credit card
                
            } //if we have a non NULL readInOrder...
        } catch (Exception e) {
        	System.debug('Error occured in OrderReadManager.getOrderReadDetails: ' + e);
        	if (currentOrder != null) {
        	    System.debug('currentOrder = ' + currentOrder);
        	}
            System.debug('Cause: '+ e.getCause());
            System.debug('Message: ' + e.getMessage());

            //rethrow error so it can be handled by caller
            throw e;
        }
        return null;
    }
    
    public void setOrderItems(SAPOrder sapOrder) {
        //Mapper from raw SAPOrder to OrderItem, which is what the user interface leverages for display.
        List<OrderLineItems.OliResult> lstOlItems = null;
        List<OrderItem> currentOrderItems = null;
        Map<String, String> incotermsFromSAP = new Map<String, String>();
        try {

			Map<String, OrderLineItems.OliResult> determineBOMMap = new Map<String, OrderLineItems.OliResult>();
  
            if (sapOrder.Items != null) {
                lstOlItems = new List<OrderLineItems.OliResult>();
                currentOrderItems = new List<OrderItem>();

                for (SAPOrder.Item sapOrderItem: sapOrder.Items) {
                    
                    OrderLineItems.OliResult oResult = new OrderLineItems.OliResult();
                    OrderItem oItem = new OrderItem();
                    
                    oItem.Ship_Date__c = sapOrderItem.deliveryDate;
                    
					//System.debug(LoggingLevel.ERROR, 'READ scenario - order items sapOrderItem.deliveryDate = ' + sapOrderItem.deliveryDate);  
					//System.debug(LoggingLevel.ERROR, 'READ scenario - order items oItem.Ship_Date__c = ' + oItem.Ship_Date__c   );  
                    
                    oItem.Material_Number__c = sapOrderItem.material;
                    oItem.Quantity = sapOrderItem.requestedQuantity;
                    oItem.Number_Confirmed__c = String.valueOf(sapOrderItem.confirmedQuantity);
                    oItem.UnitPrice = sapOrderItem.unitPrice;
                    oItem.Net_Price__c = sapOrderItem.netPrice;
                    
                    oItem.Description = sapOrderItem.shortText;
                    oItem.Batch__c = String.isNotBlank(sapOrderItem.batch) ? sapOrderItem.batch : 'Select Batch';
                    oItem.Plant__c = String.isNotBlank(sapOrderItem.plant) ? sapOrderItem.plant : 'Select Plant';
                    
                    oItem.Discount_Type__c = sapOrderItem.discountType;
                    oItem.Discount_Value__c = sapOrderItem.discountValue;

                    oItem.Free_Goods__c = sapOrderItem.freeGoods;
                    oitem.Free_Goods_Agent__c = sapOrderItem.freeGoodsAgent;
                    oItem.Free_Goods_Cost_Center__c = sapOrderItem.freeGoodsCostCenter;
                    oItem.Free_Goods_Reason__c = sapOrderItem.freeGoodsReason;
                    
                    //If the order was originally submitted with no user specified incoterms on the line item, 
                    //then we should return the header value
                    if (String.isBlank(sapOrderItem.incoterms)) {
                        oItem.Ship_Method__c = sapOrder.incoterms;
                    } else {
                    	oItem.Ship_Method__c = sapOrderItem.incoterms;
                    }
                    
                    if (sapOrderItem.freightRate != null ) {
                    	oItem.Freight_Charge__c = sapOrderItem.freightRate;
                    }
                    
					//System.debug(LoggingLevel.ERROR, 'READ scenario - order items Ship Method sapOrderItem.incoterms = ' + sapOrderItem.incoterms);  
					//System.debug(LoggingLevel.ERROR, 'READ scenario - order items Ship Method oItem.Ship_Method__c = ' +  oItem.Ship_Method__c);  
                  
                    
                    //We are dynamatically populating the lineItemShipMethodCode dropdown if we detect that the incoming incoterms is NOT on the Ship Method UI
                    //dropdown. Not a likely case, but could exist if the domain values are out of sync. In this case, SAP is the true gatekeeper.
                    //As we process/iterate through the order items, we populate a map of the incoterms read in from SAP. Later on, when control is returned
                    //to upstream controller logic, a check is performed to see if what's in the incotermsFromSAP map below needs to be added to the Ship Method
                    //dropdown list.
                    String lineItemShipMethodCode = oItem.Ship_Method__c;
                    if ((lineItemShipMethodCode != null) && (lineItemShipMethodCode.length() > 0)) {
                        if (incotermsFromSAP.containsKey(lineItemShipMethodCode)) {
                            //do nothing - don't repeat the addition to the map...
                        } else {
                			//System.debug(LoggingLevel.ERROR,'>>>>>> adding lineItemShipMethodCode ' + lineItemShipMethodCode + ' to incotermsFromSAP');
                            incotermsFromSAP.put(lineItemShipMethodCode, lineItemShipMethodCode);
                        }    
                    }
                    
                    oResult.isItemFromSAP = true;
                    oResult.isDelivered = sapOrderItem.isDelivered;
                    
                    oResult.orderItem = oItem;
                    //There are fields on oResult that the UI directly uses for rendering that may not exist in the oItem populated above:
                    
                    oResult.higherLevelItem = sapOrderItem.higherLevelItem; //used in conjunction with BOMs
					                    
                    oResult.itemNumber = sapOrderItem.itemNumber;
                    oResult.prodMatNo = sapOrderItem.material;
                    oResult.unitPrice = sapOrderItem.unitPrice;
                    if (sapOrderItem.netPrice != null) {
                        oResult.netPrice = sapOrderItem.netPrice;
                        showPricing = true;
                    }
					//System.debug(LoggingLevel.ERROR,'>>>>>> oResult.netPrice = ' + oResult.netPrice);                     
                    //item number reconstitution: oResult.externalItemNumber was originally: externalItemCount+1;
                    if (sapOrderItem.externalItemNumber != null) {
                        oResult.externalItemNumber = Integer.valueOf(sapOrderItem.externalItemNumber); 
                    }
                    
				//TODO: 2.8.2017 showBatchLink - may need additional (re)work. If the line item can't be ordered, don't allow editing of batch, but still need to display.   
                    oResult.showSelectBatchLink = false;
                    if (sapOrderItem.batch != null) {
                        oResult.showSelectBatchLink = true;
                    }
                    
                    if (sapOrderItem.batchExpirationDate != null) {
                        Datetime batchExpDT = datetime.newInstance(sapOrderItem.batchExpirationDate.year(), sapOrderItem.batchExpirationDate.month(),sapOrderItem.batchExpirationDate.day());
                        //need to open this format for non NA use cases
                        //String stringBatchExpDate = batchExpDT.format('MM/dd/yyyy');
                        String stringBatchExpDate = batchExpDT.format();
                        stringBatchExpDate = stringBatchExpDate.remove(' 0:00');
                        oResult.expirationDate = stringBatchExpDate;
                    }
                    if(sapOrderItem.plant != null){
                    	oResult.showSelectPlantLink = true;
                    }

                    //NB: 2.8.2017 -- We're not mapping MaterialText (mapped from Simulate response - MaterialSalesText) on purpose when reconstituting an order line item from READ; as per
                    //    SAP (Ryan Crosby) MaterialSalesText is custom on the backend and only comes back with Simulate. This would require lots of extra work - new WSDL, mapping, OSB;
                    //    and would only proceed if the Business asked for it. Checking with Melanie Francis on 2.8.2017, we don't need this populated; and we'll get it back anyway
                    //    with Simulate. This is informational.

                    oResult.itemCategory = sapOrderItem.itemCategory;
                    oResult.FreeGoodsAgent = sapOrderItem.freeGoodsAgent;
                    oResult.FreeGoodsCostCenter = sapOrderItem.freeGoodsCostCenter;
                    oResult.FreeGoodsReason = sapOrderItem.freeGoodsReason;
                    
                    //Discounts
                    if (sapOrderItem.discountType != null) {
                        oResult.discountType = sapOrderItem.discountType;
                    }
                    if (sapOrderItem.discountValue != null) {
                        oResult.discountValue = sapOrderItem.discountValue;
                    }
                    if (sapOrderItem.discountCurrency != null) {
                        oResult.discountCurrency = sapOrderItem.discountCurrency;
                    }
                    
                    //line item freight
                    if ((sapOrderItem.freightRate != null) && (sapOrderItem.freightRate > 0)) {
                    	oResult.freightRate = sapOrderItem.freightRate;
                        oResult.freightChanged = true;
                    }
                    
                    if (oItem.Quantity != null) {
                        oResult.orderItem.Quantity = String.ValueOf(oItem.Quantity).contains('.')? Double.valueOf(String.ValueOf(oItem.Quantity).Substring(0,String.ValueOf(oItem.Quantity).indexOf('.'))):0;
                    }
                    oResult.shippingCharges = sapOrderItem.shippingCharges;
                    
                    currentOrderItems.add(oItem);

                    //placement of an item in this map will signify that a parent line item (identified by itemNumber) is a BOM header/parent item;
					if (oResult.HigherLevelItem != null) {
						determineBOMMap.put(oResult.HigherLevelItem, oResult);
					}
                    
                    lstOlItems.add(oResult);
                    
                } //for
            } //end - if

            //4.3.2017 now that we've initially iterated through the line items being returned,
            //spin through again to see if any of these items are BOMs. If we detect a parent BOM, indicate as 
            //such so we can lock things down on the UI; or vice-versa
            List<OrderLineItems.OliResult> lstOrdersBOMProcessingTemp = new List<OrderLineItems.OliResult>();
            for(OrderLineItems.OliResult lineItem : lstOlItems){
            	if (determineBOMMap.containsKey(lineItem.itemNumber)) {
            		lineItem.isParentBOMItem = true;
            		lineItem.showSelectBatchLink = false;
            		lineItem.showSelectPlantLink = false;
            		if (lineItem.OrderItem.Batch__c == 'Select Batch') {
            			lineItem.OrderItem.Batch__c = ''; //so we don't render "Select Batch" in this case.
            		}
            		if (lineItem.OrderItem.Plant__c == 'Select Plant') {
            			lineItem.OrderItem.Plant__c = ''; //so we don't render "Select Plant" in this case.
            		}
            	}
            	lstOrdersBOMProcessingTemp.add(lineItem);
            }
            lstOlItems = lstOrdersBOMProcessingTemp;
			//System.debug(LoggingLevel.ERROR,'>>>>>> lstOlItems = ' + lstOlItems);
            //4.3.2017 end
            
            uiOrderItemsList = lstOlItems;
            orderItemsList = currentOrderItems;
            incotermsMap = incotermsFromSAP;

        } catch (Exception e) {
        	System.debug('Error occured in OrderReadManager.setOrderItems: ' + e + ' ' + e.getMessage() + ' ' + e.getStackTraceString() );
        	if (sapOrder.Items != null) {
        	    System.debug('sapOrder.Items value = ' + sapOrder.Items);
        	}
            System.debug('Cause: '+ e.getCause());
            System.debug('Message: ' + e.getMessage());

            //rethrow error so it can be handled by caller
            throw e;
        }
        
    }

    
    public void setEditableStateforOrder(SAPOrder sapOrder) {
        if (null != sapOrder) {
            if (sapOrder.isDelivered) {
                orderDeliveryStatus = DELIVERY_STATUS_COMPLETELY_DELIVERED;    
            } else if (sapOrder.isPartiallyDelivered)           {
                orderDeliveryStatus = DELIVERY_STATUS_PARTIALLY_DELIVERED;
            } else {
                orderDeliveryStatus = DELIVERY_STATUS_NOT_DELIVERED;  
            }
        } else {
            orderDeliveryStatus = DELIVERY_STATUS_NOT_DELIVERED;  
        }

    }
    
    
    //This inner class will allow the return of both the raw SAPOrder object from a call to OrderManager.getOrder(),
    //but will also return a modified currentOrder object with updates from the read-in SAPOrder object.
    //We require the currentOrder object to maintain connectivity with the front-end UI, so in this case, currentOrder
    //can be thought of as a Data Transformation Object (DTO) to the front-end.
    public class OrderReadManagerWrapper {
        public String orderDeliveryStatus                   {get;set;}
        public boolean showPricing                          {get;set;} //for visibility of netPrice on line items
        public SAPOrder sapOrder                            {get;set;}
        public Order currentOrder                           {get;set;}
        public List<OrderLineItems.OliResult> uiOrderItems  {get;set;} //the UI requires order line items to be in this format.
        public List<OrderItem> currentOrderItems            {get;set;} //will be populated with order items that are built using SAP data, for a later bulk SOQL insert. 
        public Map<String,String> incotermsFromSAP          {get;set;} //needed to determine if we need to add an incoterm/Ship Method dynamicall based on value from SAP.
    }
    

}