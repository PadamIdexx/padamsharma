/**
*       @author Hemanth Vanapalli
*       @date   14/10/2015   
        @description   This is class works on opportunities
        Function:      This class has two functions:
               1. It prohibits the deletion of opportunity 
               2. Sums the amount of all child opportunities and displays it in a field called Child Rollup Amount
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Hemanth Vanapalli               13/10/2015          Original Version
**/

public with sharing class OpportunityTriggerHandler {
    public static boolean doNotRun{get;set;}
    public Set<Id> opportunityIds = new Set<Id>();
    public List<Order> orderList = new List<Order>();
    public List<Order> orderUpdateList = new List<Order>();
    public Set<Id> parentOpportunityIds = new Set<Id>();
    Map<Id, Id> opptyCampMap = new Map<Id, Id>();
    private static final String OPPORTUNITY_DEL_VALIDATION = 'The Opportunity cannot be deleted';
    String tenderRecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_Annuitized).getRecordTypeId();
    String lpdRecType =Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_LPD_Opportunity).getRecordTypeId();
    String negativeRecType= Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_NetativeWinBack).getRecordTypeId();
    public OpportunityTriggerHandler() {
          
    }
    public void onAfterInsert(List<Opportunity> oppNewList , Map<Id,Opportunity> opportunityNewMap) {
        for(Opportunity opp :opportunityNewMap.values()) {
            opportunityIds.add(opp.Id); 
            if( String.isNotBlank(opp.Opportunity__c)) {
                opportunityIds.add(opp.Opportunity__c);
                parentOpportunityIds.add(opp.Opportunity__c);
            }
        }
        OpportunityTriggerHandler.updateRollupOnParentOpportunityForInsert(opportunityIds);
    }
    public void onBeforeUpdate( Map<Id,Opportunity> opportunityOldMap , Map<Id,Opportunity> opportunityNewMap) {
        System.debug('opportunityNewMap map1:'+opportunityNewMap);
        for(Opportunity opp :opportunityNewMap.values()) {
            opportunityIds.add(opp.Id); 
            if( String.isNotBlank(opp.Opportunity__c)) {
                opportunityIds.add(opp.Opportunity__c);
                parentOpportunityIds.add(opp.Opportunity__c);
            }
        }
        updateRollupOnParentOpportunityForUpdate(opportunityNewMap,opportunityIds);
    }
    /*
    public void onAfterUpdate(Map<Id,Opportunity> oldOppMap, Map<Id,Opportunity> newOppMap) {
        Set<Id> parentOppIds = new Set<Id>();
        Map<Id,Opportunity> oldParentMap = new Map<Id,Opportunity>();
        Map<Id,Opportunity> newParentMap = new Map<Id,Opportunity>();
        for(Opportunity opp : newOppMap.values()) {
            if( opp.RecordTypeId == tenderRecType && oldOppMap.get(opp.Id).Opportunity_Type__c != newOppMap.get(opp.Id).Opportunity_Type__c ) {
                parentOppIds.add(opp.Id);         
            }
        }   
        if(parentOppIds.size() > 0 ) {
            for(Id opId : parentOppIds) {
                oldParentMap.put(opId,oldOppMap.get(opId));
                newParentMap.put(opId,newOppMap.get(opId));
            }
            updateChildOpportunityType(oldParentMap,newParentMap);
        }     
    }
    
    public void updateChildOpportunityType( Map<Id,Opportunity> oldOppMap, Map<Id,Opportunity> newOppMap ) {
        List<Opportunity> childOpportunitiesToBeUpdated = new List<Opportunity>();
        childOpportunitiesToBeUpdated = [select Id,Opportunity_Type__c From Opportunity where Opportunity__c in:newOppMap.keySet()];
        for(Opportunity opp: [select Id,Opportunity_Type__c,(select Id, Opportunity_Type__c From Opportunities__r )From Opportunity where Id IN:newOppMap.keySet()]) {
            for(Opportunity childOpp : opp.Opportunities__r) {
                childOpp.Opportunity_Type__c = String.valueOf(opp.Opportunity_Type__c).equalsIgnoreCase(Label.Annuized_Tender)? 'Annuitized Opportunity' :'LPD Opportunity';
                childOpportunitiesToBeUpdated.add(childOpp);
            }
        }       
    }
    */
    public void onAfterUpdate(List<Opportunity> oppUpdateList, Map<Id, Opportunity> mapOldOpportunities) {
        if(doNotRun != null && doNotRun != true){
        System.debug('@@@@' + oppUpdateList); 
        System.debug(Trigger.old); 
        for(Opportunity opp :oppUpdateList) {
            if(opp.CampaignId!=null) {
            OpptyCampMap.put(opp.Id, opp.CampaignId);    
            opportunityIds.add(opp.Id); 
            }
           } 
        for(Order oOrder : [SELECT Id, OpportunityId, Name, POC_Campaign__c FROM Order WHERE OpportunityId IN :opportunityIds]) {
            //orderList.add(oOrder.POC_Campaign__c); 
            if(opptyCampMap.containskey(oOrder.OpportunityId)) { 
                
                oOrder.POC_Campaign__c = opptyCampMap.get(oOrder.OpportunityId);
                orderList.add(oOrder); 
            }
        }
        System.debug ('&&&&&' + orderList); 
        update orderList; 
        
        }
        
        // Create Install work orders for Opportunity where Stage = '4 - Commitment'
        createInstallWorkOrders(oppUpdateList, mapOldOpportunities);  
    }
    public void updateRollupOnParentOpportunityForUpdate(Map<Id,Opportunity> opportunityNewMap,Set<Id> Opportunity_IDs) {
        
        Map<Id,Opportunity> oppNewMap = new Map<Id,Opportunity>();
        oppNewMap = opportunityNewMap;
        System.debug('new map:'+oppNewMap);
        System.debug('opportunityNewMap map:'+opportunityNewMap);
        System.debug('new map size :'+oppNewMap.size());
        String tenderRecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_Annuitized).getRecordTypeId();
        
        Double oppRollupAmount = 0.0;
        Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>([Select Id,Amount,Amount_In__c,Child_Rollup_Amount__c,Opportunity__c,Is_Child_Opportunity__c,RecordTypeId,RecordType.Name From Opportunity where Id in:Opportunity_IDs  OR Opportunity__c in:Opportunity_IDs]); // OR Opportunity__c in:parentOpportunityIds
        System.debug('oppMap is:'+oppMap);
        System.debug('oppMap size is:'+oppMap.size());
        Map<Id,List<Opportunity>> parentOpportunityMap = new Map<Id,List<Opportunity>>();
        Map<Id,Opportunity> updateThisOpportunityMap = new Map<Id,Opportunity>();
        for(Opportunity eachOpportunity : oppMap.values()) {
            if( eachOpportunity.Is_Child_Opportunity__c ) {
                if( !parentOpportunityMap.containsKey(eachOpportunity.Opportunity__c)) {
                    parentOpportunityMap.put(eachOpportunity.Opportunity__c,new List<Opportunity>{eachOpportunity});    
                }
                else {
                    parentOpportunityMap.get(eachOpportunity.Opportunity__c).add(eachOpportunity);
                }   
            }                           
        }
        System.debug('parentOpportunityMap:'+parentOpportunityMap);
        System.debug('parentOpportunityMap:size'+parentOpportunityMap.size());
        System.debug('opportunityNewMap:size'+opportunityNewMap);
        for(Id parentOppId : parentOpportunityMap.keySet()) {
            oppRollupAmount = 0.0;
            
            for(Opportunity opp : parentOpportunityMap.get(parentOppId)){
                if(opportunityNewMap.containsKey(opp.id) && opp.Opportunity__c == parentOppId){
                    System.debug('each id:'+opportunityNewMap.get(opp.id));
                    opp.Amount =  opportunityNewMap.get(opp.id).Amount;
                }
                System.debug('each child opp:'+opp);
                oppRollupAmount += opp.Amount;
                System.debug('each time rolled up:'+oppRollupAmount);
            }
            
            Opportunity tempOpp = oppMap.get(parentOppId);
            tempOpp.Child_Rollup_Amount__c = oppRollupAmount;
            updateThisOpportunityMap.put(parentOppId,tempOpp);
        }
        if( !updateThisOpportunityMap.isEmpty() ) {
            System.debug('updateThisOpportunityMap are:'+updateThisOpportunityMap);
            update updateThisOpportunityMap.values();
        }
    }
    /*
    public void onBeforeDelete(Map<Id,Opportunity> opportunityOldMap) {
        for(Opportunity opp:opportunityOldMap.values()) {          
            opp.addError(OPPORTUNITY_DEL_VALIDATION);          
        }
    }
    */
    @future
    public static void updateRollupOnParentOpportunityForInsert(Set<Id> Opportunity_IDs) {
        
        Double oppRollupAmount = 0.0;
        Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>([Select Id,Amount,Amount_In__c,Child_Rollup_Amount__c,Opportunity__c,Is_Child_Opportunity__c,RecordTypeId,RecordType.Name From Opportunity where Id in:Opportunity_IDs ]); // OR Opportunity__c in:parentOpportunityIds
        System.debug('oppMap is:'+oppMap);
        System.debug('oppMap size is:'+oppMap.size());
        Map<Id,List<Opportunity>> parentOpportunityMap = new Map<Id,List<Opportunity>>();
        Map<Id,Opportunity> updateThisOpportunityMap = new Map<Id,Opportunity>();
        for(Opportunity eachOpportunity : oppMap.values()) {
            if( eachOpportunity.Is_Child_Opportunity__c ) {
                if( !parentOpportunityMap.containsKey(eachOpportunity.Opportunity__c)) {
                    parentOpportunityMap.put(eachOpportunity.Opportunity__c,new List<Opportunity>{eachOpportunity});    
                }
                else {
                    parentOpportunityMap.get(eachOpportunity.Opportunity__c).add(eachOpportunity);
                }   
            }
                        
        }
        System.debug('parentOpportunityMap:'+parentOpportunityMap);
        System.debug('parentOpportunityMap:size'+parentOpportunityMap.size());
        for(Id parentOppId : parentOpportunityMap.keySet()) {
            oppRollupAmount = 0.0;          
            for(Opportunity opp : parentOpportunityMap.get(parentOppId)){
                System.debug('each child opp:'+opp);
                oppRollupAmount += opp.Amount;
                System.debug('each time rolled up:'+oppRollupAmount);
            }           
            Opportunity tempOpp = oppMap.get(parentOppId);
            tempOpp.Child_Rollup_Amount__c = oppRollupAmount;
            updateThisOpportunityMap.put(parentOppId,tempOpp);
        }
        if( !updateThisOpportunityMap.isEmpty() ) {
            System.debug('updateThisOpportunityMap are:'+updateThisOpportunityMap);
            update updateThisOpportunityMap.values();
        }
    }
    
    public void createInstallWorkOrders(List<Opportunity> listOpportunities, Map<Id, Opportunity> mapOldOpportunities){
        WFM_Utils.createInstallWorkOrders(listOpportunities, mapOldOpportunities);
    }
}