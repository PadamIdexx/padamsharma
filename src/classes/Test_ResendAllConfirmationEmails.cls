/**
*    @author         Anudeep Gopagoni
*    @date           12/07/2015   
     @description    This is a test class for WSDL generated class idexxiComSapxiPocbeaCustomer360 written for testing the resend order/delivery confirmation emails functionality
     Modification Log:
    ------------------------------------------------------------------------------------
    Developer                            Date                Description
    ------------------------------------------------------------------------------------
    Anudeep Gopagoni                   11/01/2015          Initial Version
**/

@isTest public class Test_ResendAllConfirmationEmails {

 static testMethod void resendAllConfirmationEmailsPositiveTest() {
    User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
    system.runAs(adminUser){
    
      
          
                Test.startTest();
          
                    
                    Test.setMock(WebServiceMock.class, new ResendOrderConfirmationNoError());
                    
                    
                    //SimulateCallout Method
                    //ResendAllConfirmationEmail.resendconfirmation_order(thisOrder.OrderReferenceNumber);
                    ResendAllConfirmationEmail.resendconfirmation_delivery('8000004770');
                   // ResendAllConfirmationEmail.resendconfirmation_delivery('8000003749');
                    ResendAllConfirmationEmail.resendconfirmation_order('8000004770');
                   // ResendAllConfirmationEmail.resendconfirmation_delivery('8000003748');
                    //ResendAllConfirmationEmail.resendconfirmation_order('8000004770');
                    ResendAllConfirmationEmail.resendDelConfFromOrderDetail('8000004770');
                    ResendAllConfirmationEmail.resendOrdConfFromOrderDetail('8000004770');
                     
                Test.stopTest();
            }
        
    }
    
    static testMethod void resendAllConfirmationEmailsOutputPending() {
    User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
    system.runAs(adminUser){
    
      
          
                Test.startTest();
           
                    
                    Test.setMock(WebServiceMock.class, new ResendOrderConfirmationPendingOutput());
                    
                    
                    //SimulateCallout Method
                    ResendAllConfirmationEmail.resendconfirmation_order('8000004770');
                    ResendAllConfirmationEmail.resendconfirmation_delivery('8000004770');
                   // ResendAllConfirmationEmail.resendconfirmation_delivery('8000003749');
                    //ResendAllConfirmationEmail.resendconfirmation_order('1438916353');
                    //ResendAllConfirmationEmail.resendconfirmation_delivery('8000003748');
                    //ResendAllConfirmationEmail.resendconfirmation_order('1438916352');
                    ResendAllConfirmationEmail.resendDelConfFromOrderDetail('8000004770');
                    ResendAllConfirmationEmail.resendOrdConfFromOrderDetail('8000004770');
                     
                Test.stopTest();
            }
        
    }
    
    static testMethod void resendAllConfirmationEmailsNoOutput() {
    User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
    system.runAs(adminUser){
    
      
          
                Test.startTest();
           
                    
                    Test.setMock(WebServiceMock.class, new ResendOrderConfirmationNoOutput());
                    
                    
                    //SimulateCallout Method
                    ResendAllConfirmationEmail.resendconfirmation_order('8000004770');
                    //ResendAllConfirmationEmail.resendconfirmation_delivery('8000004770');
                   // ResendAllConfirmationEmail.resendconfirmation_delivery('8000003749');
                    //ResendAllConfirmationEmail.resendconfirmation_order('1438916353');
                    //ResendAllConfirmationEmail.resendconfirmation_delivery('8000003748');
                    //ResendAllConfirmationEmail.resendconfirmation_order('1438916352');
                    //ResendAllConfirmationEmail.resendDelConfFromOrderDetail(thisOrder.id);
                    ResendAllConfirmationEmail.resendOrdConfFromOrderDetail('8000004770');
                     
                Test.stopTest();
            }
        
    }
    
    static testMethod void resendAllConfirmationEmailsTAOrders() {
    User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
    system.runAs(adminUser){
    
      
          
                Test.startTest();
           
                    Test.setMock(WebServiceMock.class, new ResendOrderConfirmationTAOrder());
                    
                    
                    //SimulateCallout Method
                    ResendAllConfirmationEmail.resendconfirmation_order('8000004770');
                    //ResendAllConfirmationEmail.resendconfirmation_delivery('8000004770');
                   // ResendAllConfirmationEmail.resendconfirmation_delivery('8000003749');
                    //ResendAllConfirmationEmail.resendconfirmation_order('1438916353');
                    //ResendAllConfirmationEmail.resendconfirmation_delivery('8000003748');
                    //ResendAllConfirmationEmail.resendconfirmation_order('1438916352');
                    //ResendAllConfirmationEmail.resendDelConfFromOrderDetail(thisOrder.id);
                    ResendAllConfirmationEmail.resendOrdConfFromOrderDetail('8000004770');
                     
                Test.stopTest();
            }
        
    }
    
    static testMethod void resendAllConfirmationEmailsNoObject() {
    User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
    system.runAs(adminUser){
    
      
          
                Test.startTest();
           
                    
                    Test.setMock(WebServiceMock.class, new ResendOrderConfirmationNoObject());
                    
                    
                    //SimulateCallout Method
                    ResendAllConfirmationEmail.resendconfirmation_order('8000004770');
                    ResendAllConfirmationEmail.resendconfirmation_delivery('8000004770');
                   // ResendAllConfirmationEmail.resendconfirmation_delivery('8000003749');
                    //ResendAllConfirmationEmail.resendconfirmation_order('1438916353');
                    //ResendAllConfirmationEmail.resendconfirmation_delivery('8000003748');
                    //ResendAllConfirmationEmail.resendconfirmation_order('1438916352');
                    ResendAllConfirmationEmail.resendDelConfFromOrderDetail('8000004770');
                    ResendAllConfirmationEmail.resendOrdConfFromOrderDetail('8000004770');
                     
                Test.stopTest();
            }
        
    }
    
    static testMethod void resendAllConfirmationEmailsNoDnum() {
    User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
    system.runAs(adminUser){
    
      
          
                Test.startTest();
           
                    
                    Test.setMock(WebServiceMock.class, new ResendOrderConfirmationNoObject());
                    
                    
                    //SimulateCallout Method
                    //ResendAllConfirmationEmail.resendconfirmation_order('8000004770');
                    ResendAllConfirmationEmail.resendconfirmation_delivery('');
                   // ResendAllConfirmationEmail.resendconfirmation_delivery('8000003749');
                    //ResendAllConfirmationEmail.resendconfirmation_order('1438916353');
                    //ResendAllConfirmationEmail.resendconfirmation_delivery('8000003748');
                    //ResendAllConfirmationEmail.resendconfirmation_order('1438916352');
                   // ResendAllConfirmationEmail.resendDelConfFromOrderDetail(thisOrder.id);
                   // ResendAllConfirmationEmail.resendOrdConfFromOrderDetail('8000004770');
                     
                Test.stopTest();
            }
        
    }
    
    static testMethod void resendAllConfirmationEmailsNoObjectOnly() {
    User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
    system.runAs(adminUser){
    
      
          
                Test.startTest();
           
                    
                    Test.setMock(WebServiceMock.class, new ResendOrderConfirmationNoObjectOnly());
                    
                    
                    //SimulateCallout Method
                    //ResendAllConfirmationEmail.resendconfirmation_order('8000004770');
                    ResendAllConfirmationEmail.resendconfirmation_delivery('8000004770');
                   // ResendAllConfirmationEmail.resendconfirmation_delivery('8000003749');
                    //ResendAllConfirmationEmail.resendconfirmation_order('1438916353');
                    //ResendAllConfirmationEmail.resendconfirmation_delivery('8000003748');
                    //ResendAllConfirmationEmail.resendconfirmation_order('1438916352');
                   // ResendAllConfirmationEmail.resendDelConfFromOrderDetail(thisOrder.id);
                   ResendAllConfirmationEmail.resendOrdConfFromOrderDetail('8000004770');
                     
                Test.stopTest();
            }
        
    }
    
    public class ResendOrderConfirmationNoError implements WebServiceMock {
    
   public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

        // Create response element from the autogenerated class.
        // Populate response element.
        // Add response element to the response parameter, as follows:
        idexxiComSapxiPocbeaCustomer360.MessagesReturn responseElement = new idexxiComSapxiPocbeaCustomer360.MessagesReturn();
        responseElement.IsError = false;
            
        
        
        idexxiComSapxiPocbeaCustomer360.Messages message = new idexxiComSapxiPocbeaCustomer360.Messages();
        message.MessageType = 'Success';
        message.MessageID = 'M1';
        message.MessageNumber = 'M1';
        message.Message = 'No Error';
        responseElement.Messages = new List<idexxiComSapxiPocbeaCustomer360.Messages>();
        responseElement.Messages.add(message);
        
        response.put('response_x', responseElement); 
   }
   }
   
   public class ResendOrderConfirmationPendingOutput implements WebServiceMock {
    
   public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

        // Create response element from the autogenerated class.
        // Populate response element.
        // Add response element to the response parameter, as follows:
        idexxiComSapxiPocbeaCustomer360.MessagesReturn responseElement = new idexxiComSapxiPocbeaCustomer360.MessagesReturn();
        //responseElement.IsError = false;
            
        
        
        idexxiComSapxiPocbeaCustomer360.Messages message = new idexxiComSapxiPocbeaCustomer360.Messages();
        message.MessageType = 'Success';
        message.MessageID = 'M1';
        message.MessageNumber = 'M1';
        message.Message = 'Output is already pending for document 8000004770';
        responseElement.Messages = new List<idexxiComSapxiPocbeaCustomer360.Messages>();
        responseElement.Messages.add(message);
        
        response.put('response_x', responseElement); 
   }
}

public class ResendOrderConfirmationNoOutput implements WebServiceMock {
    
   public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

        // Create response element from the autogenerated class.
        // Populate response element.
        // Add response element to the response parameter, as follows:
        idexxiComSapxiPocbeaCustomer360.MessagesReturn responseElement = new idexxiComSapxiPocbeaCustomer360.MessagesReturn();
        //responseElement.IsError = false;
            
        
        
        idexxiComSapxiPocbeaCustomer360.Messages message = new idexxiComSapxiPocbeaCustomer360.Messages();
        message.MessageType = 'Success';
        message.MessageID = 'M1';
        message.MessageNumber = 'M1';
        message.Message = 'No output exists for document 8000004770';
        responseElement.Messages = new List<idexxiComSapxiPocbeaCustomer360.Messages>();
        responseElement.Messages.add(message);
        
        response.put('response_x', responseElement); 
   }
}

public class ResendOrderConfirmationTAOrder implements WebServiceMock {
    
   public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

        // Create response element from the autogenerated class.
        // Populate response element.
        // Add response element to the response parameter, as follows:
        idexxiComSapxiPocbeaCustomer360.MessagesReturn responseElement = new idexxiComSapxiPocbeaCustomer360.MessagesReturn();
        //responseElement.IsError = false;
            
        
        
        idexxiComSapxiPocbeaCustomer360.Messages message = new idexxiComSapxiPocbeaCustomer360.Messages();
        message.MessageType = 'Success';
        message.MessageID = 'M1';
        message.MessageNumber = 'M1';
        message.Message = 'Order confirmation can only be sent for TA order type';
        responseElement.Messages = new List<idexxiComSapxiPocbeaCustomer360.Messages>();
        responseElement.Messages.add(message);
        
        response.put('response_x', responseElement); 
   }
}

public class ResendOrderConfirmationNoObject implements WebServiceMock {
    
   public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

        // Create response element from the autogenerated class.
        // Populate response element.
        // Add response element to the response parameter, as follows:
        idexxiComSapxiPocbeaCustomer360.MessagesReturn responseElement = new idexxiComSapxiPocbeaCustomer360.MessagesReturn();
        //responseElement.IsError = false;
            
        
        
        idexxiComSapxiPocbeaCustomer360.Messages message = new idexxiComSapxiPocbeaCustomer360.Messages();
        message.MessageType = 'Success';
        message.MessageID = 'M1';
        message.MessageNumber = 'M1';
        message.Message = 'Object key 8000004770 not found';
        responseElement.Messages = new List<idexxiComSapxiPocbeaCustomer360.Messages>();
        responseElement.Messages.add(message);
        
        response.put('response_x', responseElement); 
   }
}

public class ResendOrderConfirmationNoObjectOnly implements WebServiceMock {
    
   public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

        // Create response element from the autogenerated class.
        // Populate response element.
        // Add response element to the response parameter, as follows:
        idexxiComSapxiPocbeaCustomer360.MessagesReturn responseElement = new idexxiComSapxiPocbeaCustomer360.MessagesReturn();
        //responseElement.IsError = false;
            
        
        
        idexxiComSapxiPocbeaCustomer360.Messages message = new idexxiComSapxiPocbeaCustomer360.Messages();
        message.MessageType = 'Success';
        message.MessageID = 'M1';
        message.MessageNumber = 'M1';
        message.Message = 'Object key  not found';
        responseElement.Messages = new List<idexxiComSapxiPocbeaCustomer360.Messages>();
        responseElement.Messages.add(message);
        
        response.put('response_x', responseElement); 
   }
}
}