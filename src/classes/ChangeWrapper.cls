public class ChangeWrapper extends SAPOrderWrapper {

    public override SAPOrder execute(SAPOrder order) {
        
        OrderChangeRequest.SalesOrderChangeRequest request;
        OrderChangeResponse.SalesOrderChangeResponse response;
		SAPOrder changedOrder;
        
        //transform SAPOrder into request
        try {
        	request = OrderChangeRequest.transform(order); 
        } catch(Exception e) {
            System.debug('Error transforming SAPOrder to OrderChangeRequest.');
            System.debug(e.getCause());
            System.debug(e.getMessage());
            
        	throw e;    
        }
        
        //call service
        try {
            OrderChangeService service = new OrderChangeService();
            
            if (!Test.isRunningTest()) {       
                response = service.ChangeOrder(request);
            } else {
                response = OrderTestDataFactory.createChangeResponse();
            }    
        } catch(Exception e) {
            System.debug('Error calling Order Change Service.');
            System.debug(e.getCause());
            System.debug(e.getMessage());
            
        	throw e;             
        }
        
        //transform response into new SAPOrder
        try {
        	changedOrder = OrderChangeResponse.transform(order, response);                
        } catch(Exception e) {
            System.debug('Error transforming OrderChangeResponse to SAPOrder.');
            System.debug(e.getCause());
            System.debug(e.getMessage());
            
        	throw e;              
        }
        
        return changedOrder;
    }
    
        
}