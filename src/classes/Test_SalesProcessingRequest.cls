@isTest
private class Test_SalesProcessingRequest {
	static testMethod void  returnOrderSimulateRequestPositive() {
		User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
		system.runAs(adminUser){
				Account thisAccount = new Account();
                thisAccount = CreateTestClassData.createAccountTypeWithCurrency(Label.RT_Customer,Label.USD);
                
                //Insert Products 
                Product2 testProduct = new Product2(Name='SNAP PRO STARTER KIT', Family='test', SAP_MATERIAL_NUMBER__c = '22345');
                insert testProduct;
                
                Pricebook2 priceBook = CreateTestClassData.reusablePricebook2('IDEXX Standard Price Book');
                insert priceBook;
                
                Id pricebookId = Test.getStandardPricebookId();
                //Standard price book Entry
                PricebookEntry stdpriceBookEntry = CreateTestClassData.reusablePriceBookEntry(testProduct.Id, pricebookId,1000);
                stdpriceBookEntry.UseStandardPrice = false;
                insert stdpriceBookEntry;
                
                //Set Shipping and Billing Address to the account
                thisAccount = CreateTestClassData.setBillingAddress(thisAccount);
                thisAccount = CreateTestClassData.setShippingAddress(thisAccount);
                update thisAccount;
                
                //Create Order and associate it to Account and Standard Pricebook
            	Order thisOrder = CreateTestClassData.createOrder(thisAccount,pricebookId, thisAccount.CurrencyISOCode);
            	// set the order with the test data 
            	thisOrder.PONumber = '22456';
            	thisOrder.Promo_Code__c = 'TestCode';
            	thisOrder.Ship_Method__c = 'Test Method';
            	thisOrder.Points_Redeemed__c = 20;
            	thisOrder.DiscountType__c = 'Percent - %';
            	update thisOrder;
            	
            	List<OrderItem> top10Orders = new List<OrderItem>();
            	List<PricebookEntry> priceBookEntryList = new List<PricebookEntry>{stdpriceBookEntry};
	            top10Orders = CreateTestClassData.createTop10Products(thisOrder,priceBookEntryList);
	            
	            List<OrderCreation.OliResult> OLItems = new  List<OrderCreation.OliResult>();
	            OLItems.add(CreateTestClassData.returnOLIElements(top10Orders[0]));
	            
	            List<OrderLineItems.OliResult> olItemsNew = new List<OrderLineItems.OliResult>();
				for(OrderCreation.OliResult oli : OLItems){
					OrderLineItems.OliResult olR = new OrderLineItems.OliResult();
					olR.orderItem = oli.orderItem;
        			olR.prodMatNo = oli.prodMatNo;
        			olR.currencyISOCode = oli.currencyISOCode;
        			olItemsNew.add(olR);
				}
                    
                Test.startTest();
                // initialize contoller
                SalesProcessingRequest requestService = new SalesProcessingRequest();
                requestService.returnOrderSimulateRequest(thisAccount,thisOrder,OLItems);
                requestService.returnOrderSimulateRequestLPD(thisAccount,thisOrder,olItemsNew, 'USD');
                Test.stopTest();
                
		}
	}
	static testMethod void  returnOrderSimulateRequestNegative() {
		
		Test.startTest();
			SalesProcessingRequest requestService = new SalesProcessingRequest();
		 	requestService.returnOrderSimulateRequest(null,null,null);
		 	requestService.returnOrderSimulateRequestLPD(new Account(),null,null, null);
		Test.stopTest();
	}
	
	static testMethod void  returnOrderCreateRequestNegative() {
		
		Test.startTest();
			SalesProcessingRequest requestService = new SalesProcessingRequest();
		 	requestService.returnOrderCreateRequest(null,null,null);
		 	// to improve code coverage - wrappers invoke
		 	SalesProcessingRequest.CreditCard_element creditCard = new SalesProcessingRequest.CreditCard_element();
		 	creditCard.CreditCardDate = 'Test';
		 	creditCard.CreditCardName = 'Test';
		 	creditCard.CreditCardNumber = 'Test';
		 	creditCard.CreditCardType = 'Test';
		 	SalesProcessingRequest.SalesOrderDeleteRequest_Sync deleteReq = new SalesProcessingRequest.SalesOrderDeleteRequest_Sync();
		 	deleteReq.DocumentNumber = '1222';
		 	deleteReq.Language = 'EN';
		Test.stopTest();
	}
	
	//3.10.2017 HLK added
	static testMethod void  returnOrderCreateRequestLPDPositive() {
		//SalesProcessingRequest requestService = new SalesProcessingRequest();
		//requestService.returnOrderCreateRequestLPD(null,null,null,null);
		User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
		system.runAs(adminUser){
				Account thisAccount = new Account();
                thisAccount = CreateTestClassData.createAccountTypeWithCurrency(Label.RT_Customer,Label.USD);
                
                //Insert Products 
                Product2 testProduct = new Product2(Name='SNAP PRO STARTER KIT', Family='test', SAP_MATERIAL_NUMBER__c = '22345');
                insert testProduct;
                
                Pricebook2 priceBook = CreateTestClassData.reusablePricebook2('IDEXX Standard Price Book');
                insert priceBook;
                
                Id pricebookId = Test.getStandardPricebookId();
                //Standard price book Entry
                PricebookEntry stdpriceBookEntry = CreateTestClassData.reusablePriceBookEntry(testProduct.Id, pricebookId,1000);
                stdpriceBookEntry.UseStandardPrice = false;
                insert stdpriceBookEntry;
                
                //Set Shipping and Billing Address to the account
                thisAccount = CreateTestClassData.setBillingAddress(thisAccount);
                thisAccount = CreateTestClassData.setShippingAddress(thisAccount);
                update thisAccount;
                
                //Create Order and associate it to Account and Standard Pricebook
            	Order thisOrder = CreateTestClassData.createOrder(thisAccount,pricebookId, thisAccount.CurrencyISOCode);
            	// set the order with the test data 
            	thisOrder.PONumber = '22456';
            	thisOrder.Promo_Code__c = 'TestCode';
            	thisOrder.Ship_Method__c = 'Test Method';
            	thisOrder.Points_Redeemed__c = 20;
            	thisOrder.DiscountType__c = 'Percent - %';
            	thisOrder.Order_Type__c = 'Free Goods Order';
            	thisOrder.Free_Goods_Agent__c = 'Test';
            	thisOrder.Free_Goods_Cost_Center__c = 'Test';
            	thisOrder.Free_Goods_Reason__c = 'Test';
            	thisOrder.Discount__c = 10;
            	//3.15.2017 - had to comment as this freight condition picklist isn't in PROD yet...
            	//thisOrder.freight_condition__c = 'ZF10';
            	thisOrder.Billing_Block__c = 'Cancellation invoice';
            	thisOrder.DeliveryBlock__c = 'Overall Block';
            	thisOrder.Shipping_Label_Text__c = 'test test test'; 
            	thisOrder.CIG__c = 'x';
            	thisOrder.CUG__c = 'x';
            	update thisOrder;
            	
            	List<OrderItem> top10Orders = new List<OrderItem>();
            	List<PricebookEntry> priceBookEntryList = new List<PricebookEntry>{stdpriceBookEntry};
	            top10Orders = CreateTestClassData.createTop10Products(thisOrder,priceBookEntryList);
	            top10Orders[0].Free_Goods__c = true;
	            update top10Orders[0];
	            
	            List<OrderCreation.OliResult> OLItems = new  List<OrderCreation.OliResult>();
	            OrderCreation.OliResult resultVar = CreateTestClassData.returnOLIElements(top10Orders[0]);
	            resultVar.itemCategory = 'ZTOP';
	            OLItems.add(resultVar);
	            
	            List<OrderLineItems.OliResult> orderItemList = new List<OrderLineItems.OliResult>();
				for(OrderCreation.OliResult oli : OLItems){
					OrderLineItems.OliResult olR = new OrderLineItems.OliResult();
					olR.orderItem = oli.orderItem;
					olR.orderItem.Batch__c = 'x';
					olR.orderItem.Plant__c = 'x';
					olR.orderItem.Quantity = 1.0;
					olR.orderItem.Freight_Charge__c = 1.0;
        			olR.prodMatNo = oli.prodMatNo;
        			olR.currencyISOCode = oli.currencyISOCode;
        			olR.itemCategory = oli.itemCategory;
        			olR.itemNumber = '00010';
        			olR.ReferenceDocument = '11-11111-11';
        			olR.ReferenceDocumentItem = 'X';
        			olR.freightChanged = true;
        			olR.freightConditionStep = '1';
        			olR.freightConditionCount = '1';
        			orderItemList.add(olR);
				}
                    
                Test.startTest();
                SalesProcessingRequest requestService = new SalesProcessingRequest();
                String selectedSalesViewCurrency = 'USD';
                
                requestService.returnOrderCreateRequestLPD(thisAccount, thisOrder, orderItemList, selectedSalesViewCurrency);

                Test.stopTest();
		}

	} //END 3.10.2017
	
	static testMethod void  returnOrderSimulateRequestFreeOrder() {
		User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
		system.runAs(adminUser){
				Account thisAccount = new Account();
                thisAccount = CreateTestClassData.createAccountTypeWithCurrency(Label.RT_Customer,Label.USD);
                
                //Insert Products 
                Product2 testProduct = new Product2(Name='SNAP PRO STARTER KIT', Family='test', SAP_MATERIAL_NUMBER__c = '22345');
                insert testProduct;
                
                Pricebook2 priceBook = CreateTestClassData.reusablePricebook2('IDEXX Standard Price Book');
                insert priceBook;
                
                Id pricebookId = Test.getStandardPricebookId();
                //Standard price book Entry
                PricebookEntry stdpriceBookEntry = CreateTestClassData.reusablePriceBookEntry(testProduct.Id, pricebookId,1000);
                stdpriceBookEntry.UseStandardPrice = false;
                insert stdpriceBookEntry;
                
                //Set Shipping and Billing Address to the account
                thisAccount = CreateTestClassData.setBillingAddress(thisAccount);
                thisAccount = CreateTestClassData.setShippingAddress(thisAccount);
                update thisAccount;
                
                //Create Order and associate it to Account and Standard Pricebook
            	Order thisOrder = CreateTestClassData.createOrder(thisAccount,pricebookId, thisAccount.CurrencyISOCode);
            	// set the order with the test data 
            	thisOrder.PONumber = '22456';
            	thisOrder.Promo_Code__c = 'TestCode';
            	thisOrder.Ship_Method__c = 'Test Method';
            	thisOrder.Points_Redeemed__c = 20;
            	thisOrder.DiscountType__c = 'Percent - %';
            	thisOrder.Order_Type__c = 'Free Goods Order';
            	thisOrder.Free_Goods_Agent__c = 'Test';
            	thisOrder.Free_Goods_Cost_Center__c = 'Test';
            	thisOrder.Free_Goods_Reason__c = 'Test';
            	thisOrder.Discount__c = 10;
            	update thisOrder;
            	
            	List<OrderItem> top10Orders = new List<OrderItem>();
            	List<PricebookEntry> priceBookEntryList = new List<PricebookEntry>{stdpriceBookEntry};
	            top10Orders = CreateTestClassData.createTop10Products(thisOrder,priceBookEntryList);
	            top10Orders[0].Free_Goods__c = true;
	            update top10Orders[0];
	            
	            List<OrderCreation.OliResult> OLItems = new  List<OrderCreation.OliResult>();
	            OrderCreation.OliResult resultVar = CreateTestClassData.returnOLIElements(top10Orders[0]);
	            resultVar.itemCategory = 'ZTOP';
	            OLItems.add(resultVar);
	            
	            List<OrderLineItems.OliResult> olItemsNew = new List<OrderLineItems.OliResult>();
				for(OrderCreation.OliResult oli : OLItems){
					OrderLineItems.OliResult olR = new OrderLineItems.OliResult();
					olR.orderItem = oli.orderItem;
        			olR.prodMatNo = oli.prodMatNo;
        			olR.currencyISOCode = oli.currencyISOCode;
        			olR.itemCategory = oli.itemCategory;
        			olItemsNew.add(olR);
				}
                    
                Test.startTest();
                // initialize contoller
                SalesProcessingRequest requestService = new SalesProcessingRequest();
                requestService.returnOrderSimulateRequest(thisAccount,thisOrder,OLItems);
                
                //3.10.2017
                
                //3.15.2017 - had to comment as this freight condition picklist isn't in PROD yet...
                //thisOrder.freight_condition__c = 'ZF10';
                thisOrder.Billing_Block__c = 'Cancellation invoice';
            	thisOrder.DeliveryBlock__c = 'Overall Block';
            	update thisOrder;
                
                requestService.returnOrderSimulateRequestLPD(thisAccount,thisOrder,OLItemsNew, 'USD');
                Test.stopTest();
		}
	}
	
	static testMethod void  returnOrderSimulateRequestFreeOrder2() {
		User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
		system.runAs(adminUser){
				Account thisAccount = new Account();
                thisAccount = CreateTestClassData.createAccountTypeWithCurrency(Label.RT_Customer,Label.USD);
                
                //Insert Products 
                Product2 testProduct = new Product2(Name='SNAP PRO STARTER KIT', Family='test', SAP_MATERIAL_NUMBER__c = '22345');
                insert testProduct;
                
                Pricebook2 priceBook = CreateTestClassData.reusablePricebook2('IDEXX Standard Price Book');
                insert priceBook;
                
                Id pricebookId = Test.getStandardPricebookId();
                //Standard price book Entry
                PricebookEntry stdpriceBookEntry = CreateTestClassData.reusablePriceBookEntry(testProduct.Id, pricebookId,1000);
                stdpriceBookEntry.UseStandardPrice = false;
                insert stdpriceBookEntry;
                
                //Set Shipping and Billing Address to the account
                thisAccount = CreateTestClassData.setBillingAddress(thisAccount);
                thisAccount = CreateTestClassData.setShippingAddress(thisAccount);
                update thisAccount;
                
                //Create Order and associate it to Account and Standard Pricebook
            	Order thisOrder = CreateTestClassData.createOrder(thisAccount,pricebookId, thisAccount.CurrencyISOCode);
            	// set the order with the test data 
            	thisOrder.PONumber = '22456';
            	thisOrder.Promo_Code__c = 'TestCode';
            	thisOrder.Ship_Method__c = 'Test Method';
            	thisOrder.Points_Redeemed__c = 20;
            	thisOrder.DiscountType__c = 'Value - $';
            	thisOrder.Order_Type__c = 'Free Goods Order';
            	thisOrder.Free_Goods_Agent__c = 'Test';
            	thisOrder.Free_Goods_Cost_Center__c = 'Test';
            	thisOrder.Free_Goods_Reason__c = 'Test';
            	thisOrder.Discount__c = 10;
            	update thisOrder;
            	
            	List<OrderItem> top10Orders = new List<OrderItem>();
            	List<PricebookEntry> priceBookEntryList = new List<PricebookEntry>{stdpriceBookEntry};
	            top10Orders = CreateTestClassData.createTop10Products(thisOrder,priceBookEntryList);
	            top10Orders[0].Free_Goods__c = true;
	            top10Orders[0].Discount_Type__c = 'Value - $';
	            update top10Orders[0];
	            
	            List<OrderCreation.OliResult> OLItems = new  List<OrderCreation.OliResult>();
	            OrderCreation.OliResult resultVar = CreateTestClassData.returnOLIElements(top10Orders[0]);
	            resultVar.itemCategory = 'Test';
	            OLItems.add(resultVar);
	            
	            List<OrderLineItems.OliResult> olItemsNew = new List<OrderLineItems.OliResult>();
				for(OrderCreation.OliResult oli : OLItems){
					OrderLineItems.OliResult olR = new OrderLineItems.OliResult();
					olR.orderItem = oli.orderItem;
        			olR.prodMatNo = oli.prodMatNo;
        			olR.currencyISOCode = oli.currencyISOCode;
        			olR.itemCategory = oli.itemCategory ;
        			olItemsNew.add(olR);
				}
                    
                Test.startTest();
                // initialize contoller
                SalesProcessingRequest requestService = new SalesProcessingRequest();
                requestService.returnOrderSimulateRequest(thisAccount,thisOrder,OLItems);
                requestService.returnOrderSimulateRequestLPD(thisAccount,thisOrder,olItemsNew, 'USD');
                Test.stopTest();
		}
	}
	
	static testMethod void  returnOrderCreateRequestFreeOrder() {
		User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
		system.runAs(adminUser){
				Account thisAccount = new Account();
                thisAccount = CreateTestClassData.createAccountTypeWithCurrency(Label.RT_Customer,Label.USD);
                thisAccount.SAP_Customer_Number__c = '55543';
                update thisAccount;
                
                //Insert Products 
                Product2 testProduct = new Product2(Name='SNAP PRO STARTER KIT', Family='test', SAP_MATERIAL_NUMBER__c = '22345');
                insert testProduct;
                
                Pricebook2 priceBook = CreateTestClassData.reusablePricebook2('IDEXX Standard Price Book');
                insert priceBook;
                
                Id pricebookId = Test.getStandardPricebookId();
                //Standard price book Entry
                PricebookEntry stdpriceBookEntry = CreateTestClassData.reusablePriceBookEntry(testProduct.Id, pricebookId,1000);
                stdpriceBookEntry.UseStandardPrice = false;
                insert stdpriceBookEntry;
                
                //Set Shipping and Billing Address to the account
                thisAccount = CreateTestClassData.setBillingAddress(thisAccount);
                thisAccount = CreateTestClassData.setShippingAddress(thisAccount);
                update thisAccount;
                
                //Create Order and associate it to Account and Standard Pricebook
            	Order thisOrder = CreateTestClassData.createOrder(thisAccount,pricebookId, thisAccount.CurrencyISOCode);
            	// set the order with the test data 
            	thisOrder.PONumber = '22456';
            	thisOrder.Promo_Code__c = 'TestCode';
            	thisOrder.Ship_Method__c = 'Test Method';
            	thisOrder.Points_Redeemed__c = 20;
            	thisOrder.DiscountType__c = 'Percent - %';
            	thisOrder.Order_Type__c = 'Free Goods Order';
            	thisOrder.Free_Goods_Agent__c = 'Test';
            	thisOrder.Free_Goods_Cost_Center__c = 'Test';
            	thisOrder.Free_Goods_Reason__c = 'Test';
            	thisOrder.Discount__c = 10;
            	thisOrder.Shipping_Label_Text__c = 'Test';
            	update thisOrder;
            	
            	List<OrderItem> top10Orders = new List<OrderItem>();
            	List<PricebookEntry> priceBookEntryList = new List<PricebookEntry>{stdpriceBookEntry};
	            top10Orders = CreateTestClassData.createTop10Products(thisOrder,priceBookEntryList);
	            top10Orders[0].Free_Goods__c = true;
	            update top10Orders[0];
	            
	            List<OrderCreation.OliResult> OLItems = new  List<OrderCreation.OliResult>();
	            OrderCreation.OliResult resultVar = CreateTestClassData.returnOLIElements(top10Orders[0]);
	            resultVar.itemCategory = 'ZTOP';
	            resultVar.itemNumber = '1223';
	            OLItems.add(resultVar);
                    
                Test.startTest();
                // initialize contoller
                SalesProcessingRequest requestService = new SalesProcessingRequest();
                requestService.returnOrderCreateRequest(thisAccount,thisOrder,OLItems);
                Test.stopTest();
		}
	}
	static testMethod void  returnOrderCreateRequestFreeOrder2() {
		User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
		system.runAs(adminUser){
				Account thisAccount = new Account();
                thisAccount = CreateTestClassData.createAccountTypeWithCurrency(Label.RT_Customer,Label.USD);
                thisAccount.SAP_Customer_Number__c = '55543';
                update thisAccount;
                
                //Insert Products 
                Product2 testProduct = new Product2(Name='SNAP PRO STARTER KIT', Family='test', SAP_MATERIAL_NUMBER__c = '22345');
                insert testProduct;
                
                Pricebook2 priceBook = CreateTestClassData.reusablePricebook2('IDEXX Standard Price Book');
                insert priceBook;
                
                Id pricebookId = Test.getStandardPricebookId();
                //Standard price book Entry
                PricebookEntry stdpriceBookEntry = CreateTestClassData.reusablePriceBookEntry(testProduct.Id, pricebookId,1000);
                stdpriceBookEntry.UseStandardPrice = false;
                insert stdpriceBookEntry;
                
                //Set Shipping and Billing Address to the account
                thisAccount = CreateTestClassData.setBillingAddress(thisAccount);
                thisAccount = CreateTestClassData.setShippingAddress(thisAccount);
                update thisAccount;
                
                //Create Order and associate it to Account and Standard Pricebook
            	Order thisOrder = CreateTestClassData.createOrder(thisAccount,pricebookId, thisAccount.CurrencyISOCode);
            	// set the order with the test data 
            	thisOrder.PONumber = '22456';
            	thisOrder.Promo_Code__c = 'TestCode';
            	thisOrder.Ship_Method__c = 'Test Method';
            	thisOrder.Points_Redeemed__c = 20;
            	thisOrder.DiscountType__c =  'Value - $';
            	thisOrder.Order_Type__c = 'Free Goods Order';
            	thisOrder.Free_Goods_Agent__c = 'Test';
            	thisOrder.Free_Goods_Cost_Center__c = 'Test';
            	thisOrder.Free_Goods_Reason__c = 'Test';
            	thisOrder.Discount__c = 10;
            	thisOrder.Shipping_Label_Text__c = 'Test';
            	update thisOrder;
            	
            	List<OrderItem> top10Orders = new List<OrderItem>();
            	List<PricebookEntry> priceBookEntryList = new List<PricebookEntry>{stdpriceBookEntry};
	            top10Orders = CreateTestClassData.createTop10Products(thisOrder,priceBookEntryList);
	            top10Orders[0].Free_Goods__c = true;
	             top10Orders[0].Discount_Type__c = 'Value - $';
	            update top10Orders[0];
	            
	            List<OrderCreation.OliResult> OLItems = new  List<OrderCreation.OliResult>();
	            OrderCreation.OliResult resultVar = CreateTestClassData.returnOLIElements(top10Orders[0]);
	            resultVar.itemCategory = 'ZTOP';
	            resultVar.itemNumber = '1223';
	            OLItems.add(resultVar);
                    
                Test.startTest();
                // initialize contoller
                SalesProcessingRequest requestService = new SalesProcessingRequest();
                requestService.returnOrderCreateRequest(thisAccount,thisOrder,OLItems);
                Test.stopTest();
		}
	}
	
	static testMethod void remainingCoverage(){
		SalesProcessingRequest.Partners_element pElements = new SalesProcessingRequest.Partners_element(); 	
		SalesProcessingRequest req = new SalesProcessingRequest();
		req.populatePartners('', '', 'WE', new Order(), '');
	}
                        

}