/**
*       @author Aditya Sangawar
*       @date   06/01/2016
        @description   Test class for InlineIDEXXAssetsController
        Function:      Class creates the Competitor Asset & IDEXX Assets.
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
       Aditya Sangawar                   06/01/2016         Created
        
**/
@isTest(SeeAllData = False)
    public With Sharing class Test_InlineIDEXXAssetsController {
        
        
        /*
        * Method name     : Test_CreateIDEXXAssets
        * Description     : Create List of IDEXX Competative Asset 
        * Return Type     : void
        * Input Parameter : nil
        */
        
        static testMethod void Test_CreateCompetativeIDEXXAssets() {
                     
            list<Asset> lstAsset = CreateTestClassData.CreateAssetList(10);
            for(Asset tempAsset : lstAsset  ){
                tempAsset.IsCompetitorProduct = True;
                tempAsset.Is_Active__c  = False;
            }
            update lstAsset;        
            
            //Refer the account where controller is to be called.
            Test.StartTest();
                Account acc = [select id from Account where id =: lstAsset[0].AccountId];        
                ApexPages.StandardController controller = new ApexPages.StandardController(acc);
                InlineIDEXXAssetsController testcontructor = new InlineIDEXXAssetsController(controller);
                testcontructor.FetchAssets();
                list<Asset> LstAst = [select Is_Active__c from Asset where Is_Active__c =: lstAsset[0].Is_Active__c]; 
                System.assertEquals(LstAst[0].Is_Active__c, False);               
            Test.StopTest();    
        
        }  
        
                /*
        * Method name     : Test_CreateIDEXXAssets
        * Description     : Create List of IDEXX Asset 
        * Return Type     : void
        * Input Parameter : nil
        */
        
        static testMethod void Test_CreateIDEXXAssets() {
                     
        list<Asset> lstAsset = CreateTestClassData.CreateAssetList(10);
        for(Asset tempAsset : lstAsset  ){
            tempAsset.IsCompetitorProduct = False;
            tempAsset.Is_Active__c  = True;
        }
        update lstAsset;        
        
        //Refer the account where controller is to be called.
        Test.StartTest();
                Account acc = [select id from Account where id =: lstAsset[0].AccountId];        
                ApexPages.StandardController controller = new ApexPages.StandardController(acc);
                InlineIDEXXAssetsController testcontructor = new InlineIDEXXAssetsController(controller);
                testcontructor.FetchAssets();
                list<Asset> LstAst = new List<Asset>();
            try{
                LstAst = [select IsCompetitorProduct from Asset where IsCompetitorProduct =: lstAsset[0].IsCompetitorProduct]; 
            }
            catch(dmlException ex){
                System.debug('Error in soql query ' + ex.getCause());
                
            }          
            Test.StopTest();
        } 
       
    }