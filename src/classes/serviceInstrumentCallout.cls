public class serviceInstrumentCallout {
    
    public static List<Map<String,String>> equip{get;set;}
    public static List<Map<String,String>> details{get;set;}
    public static List<Map<String,String>> notes{get;set;}
    public static Set<String> eqIds{get;set;}
    
    private static String endpoint1 =
        WebServiceUtil.retreiveEndpoints(WebServiceUtil.ServiceEvents);

    
    public static void getEvents(String id){
        if(test.isRunningTest()){endpoint1 = '';}
        String SoapXMLBody;
        
        SoapXMLBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sap="http://idexx.com/services/SAPSFDCServiceEvents/">'+
            '<soapenv:Header/> <soapenv:Body><sap:Request><sapId>'+id+'</sapId></sap:Request> </soapenv:Body></soapenv:Envelope>';
        
        Http http = new Http();
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(endpoint1);
        req.setHeader('SOAPAction', '"http://idexx.com/services/SAPSFDCServiceEvents/ByCustomer"');
        req.setTimeout(120000);
        req.setHeader('sessionId', UserInfo.getSessionId());
        req.setBody(SoapXMLBody);
        HttpResponse resp = http.send(req);
        Dom.Document body = resp.getBodyDocument();
        Dom.XmlNode root = body.getRootElement();

        render(root);
        
        
        
    }
    public static void render(Dom.XmlNode root){    
        equip = new List<Map<String, String>>();
        details = new List<Map<String, String>>();
        notes = new List<Map<String, String>>();
        eqIds = new Set<String>();
        Map<String,String> noteMap;
        
        
        
        Dom.XMLNode Body= root.getChildElements()[1]; 
        Dom.XmlNode childEle = Body.getChildElement('Response','http://idexx.com/services/SAPSFDCServiceEvents/');
        Dom.XmlNode eqResp = childEle.getChildElement('getEquipmentResponse','http://idexx.com/services/SAPSFDCServiceEvents/');
        Dom.XmlNode eqDetails = childEle.getChildElement('getEquipmentDetailsResponses','http://idexx.com/services/SAPSFDCServiceEvents/');
        //Dom.XmlNode eqDetEle = eqDetails.getChildElements()[1];
        Dom.XmlNode eqNotes = childEle.getChildElement('getNotificationsResponses','http://idexx.com/services/SAPSFDCServiceEvents/');
        //Dom.XmlNode eqNotesEle = eqNotes.getChildElement('SAPNotificationGetDetailResponse','http://idexx.com/services/SAPSFDCServiceEvents/');
        
        for(Dom.XmlNode eq : eqResp.getChildElement('CustomerGetEquipmentResponse','http://idexx.com/services/SAPSFDCServiceEvents/').getChildren()){
            
            Map<String,String> eqMap = new Map<String,String>();
            eqMap.put(eq.getName(),eq.getText());
            if(eq.getName() == 'Equipment'){
                for(Dom.XmlNode e : eq.getChildElements()){
                    if(e.getName() == 'EquipmentNumber')eqIds.add(e.getText());
                    eqMap.put(e.getName(), e.getText());
                    if(e.getName()=='Contracts'){
                        eqMap.put(e.getName(),e.getText());
                        for(Dom.XmlNode e2 : e.getChildElements()){
                            eqMap.put(e2.getName(), e2.getText());
                            if(e2.getName()=='Configuration'){
                                eqMap.put(e2.getName(),e2.getText());
                            }
                        }
                        
                    }
                }
            }
            equip.add(eqMap);
        }

        for(Dom.XmlNode note : eqNotes.getChildren()){
            
            
            for(Dom.XmlNode noteEle : note.getChildren()){
                noteMap = new Map<String,String>();
                String taskCode = ' ';
                String activity = ' ';
                String status = ' ';
                for(Dom.XmlNode nt : noteEle.getChildren()){
                    
                    noteMap.put(nt.getName(), nt.getText());
                    
                    if(nt.getName() == 'Tasks'){
                        for(Dom.XmlNode n : nt.getChildElements()){
                            taskCode+= n.getText() + ' — ';
                            noteMap.put('TC', taskCode);
                            noteMap.put(n.getName(), n.getText());
                        }
                    }
                    if(nt.getName() == 'Status'){
                        for(Dom.XmlNode n : nt.getChildElements()){
                            status+= n.getText() + ' — ';
                            noteMap.put('stat',status);
                            noteMap.put(n.getName(), n.getText());
                        }
                        
                    }
                    if(nt.getName() == 'Activities'){
                        for(Dom.XmlNode n : nt.getChildElements()){
                            if(n.getName()=='CodeText'){
                                activity+= n.getText() + ' — ';
                            	noteMap.put('activity', activity);
                            }
                        }
                        
                    }
					if(!noteMap.containsKey('activity'))noteMap.put('activity','');
                   
                }
                //throw out incomplete values
                if(noteMap.containsKey('EquipmentDescription')&&noteMap.containsKey('ShortText')&&noteMap.containsKey('Number')&&noteMap.containsKey('SalesActivity')){
                    List <String>sorted = new List <String>();
                    sorted.addAll(noteMap.keySet());
                    
                    notes.add(noteMap);
                    
                }
                
            }
            
        }
        System.debug(equip.size());
        System.debug(notes.size());
        System.debug(details.size());
        System.debug(eqIds);
        
        
        
    }
    
    
}