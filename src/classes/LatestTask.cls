public class LatestTask
{ 
public Task task;

public string taskId;
public Set<Id> accContactIds = new Set<Id>();        
public Set<Id> cmpgnContactIds = new Set<Id>();
ApexPages.Standardcontroller controller;
//public List<Campaign> relatedCampaigns  {get;set;}
public Account thisAccount {get;set;}
//public String AccName {get;set;}
//public list<CampaignWrapper> CampaignWrapperList {get;set;}
private Task currentTaskRecord;
public Account currentAccountRecord;
public Contact currentContactRecord;
public ID accId;
//public Account acc {get;set;}
//Set<Id> selectedIdCamp = new Set<Id>();
//public List<CampaignMember> campMem= new List<CampaignMember>();
//public List<CampaignMember> updateConMem= new List<CampaignMember>(); 
//public static final String DECLINED_CM_STATUS =  'Declined'; 
//public static final String FULFILLED_CM_STATUS = 'Fulfilled';
//private final string Campaign_Activity_RecType  = Label.RT_CampaignActivity;
public boolean isWater{get;set;}

public LatestTask (ApexPages.StandardController controller)
{       
           Id contactId = ApexPages.currentPage().getParameters().get('who_id');
           Id accountId = ApexPages.currentPage().getParameters().get('what_id');
           isWater = false;
           String profileName=[Select Id,Name from Profile where Id=:userinfo.getProfileId()].Name;
           system.debug('profileName@@@@@@@@@@@@@@2222'+profileName);
           /*for(WaterProfiles__c setting : WaterProfiles__c.getAll().values()) 
           {
              if (setting.Profile_Name__c== profileName) {
                    isWater=true;
                }
        
              }*/
            this.task = (Task)controller.getRecord(); 
            this.task.ActivityDate = system.today();
            //this.task.Interaction_Origin__c = ''; // tkt 564 stated interaction origin should not contain any default value. hence changing the value from Inbound Phone Call to blank value.
            //this.task.RecordTypeId = ApexPages.currentPage().getParameters().get('RecordType');
            // this.task.Activity_Account__c= ApexPages.currentPage().getParameters().get('what_ID');
         
           // this.task.Top_200_Account__c = 'Yes';
           // this.task.Date_of_Activity__c = date.today();
            //this.task.order__c = ApexPages.currentPage().getParameters().get('orderId');
            User u = [Select Id From User Where id = :UserInfo.getUserId()];
            //Contact con = [Select Id From Contact Where accountId =:ApexPages.currentPage().getParameters().get('who_id')];
            //task.WhoId = con.Id;
            //fetch the related to field from the URL to display associated account on the Task
           // task.WhatId = ApexPages.currentPage().getParameters().get('what_id');
            task.OwnerId = u.Id;    
            this.controller = controller;
            //fetch all the contacts present on the Account
            if( !String.isBlank(ApexPages.currentPage().getParameters().get('what_id'))){
            System.debug('ACC' + ApexPages.currentPage().getParameters().get('what_id'));
                this.currentAccountRecord = [Select Id From Account Where Id = :ApexPages.currentPage().getParameters().get('what_id')];
                accId = currentAccountRecord.Id;
            }
            else{
            
                this.currentContactRecord = [SELECT Id, AccountID from Contact WHERE ID
                    =:ApexPages.currentPage().getParameters().get('who_id')];
                accId = currentContactRecord.AccountID;
            }
            
            task.WhatId = accId;
            if(!String.isBlank(ApexPages.currentPage().getParameters().get('who_id'))){
            task.whoId = ApexPages.currentPage().getParameters().get('who_id');
            }
                        
            thisAccount = new Account();
            for(Account acc : [select id,Name,(select Id,Name from Contacts )  From Account where id = :accId ])
            {    
                thisAccount  = acc;
           
                for(Contact cnt: acc.contacts){
                    accContactIds.add(cnt.Id);
                }
            }
                     
}  

public PageReference cancel(){
PageReference page;
page = new PageReference('/'+ task.WhatId );
page.setRedirect(true);
return page;
} 

public PageReference save() {
                
            insert task;    
            PageReference pr = new PageReference('/'+task.WhatId);
            return pr;
       
}

public PageReference saveNewEvent()
    { 
        PageReference pr; 
        try
        {
            controller.save(); 
            Schema.DescribeSObjectResult describeResult = controller.getRecord().getSObjectType().getDescribe(); 
            pr = new PageReference('/apex/NewEventWater?what_id='+ event.WhatId );
            pr.setRedirect(true); 
            return pr; 
        }
        catch(Exception e)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage())); return null; 
        } 
    }

public PageReference saveNewTask()
{ 
PageReference pr; 
try
{
    controller.save(); 
    Schema.DescribeSObjectResult describeResult = controller.getRecord().getSObjectType().getDescribe(); 
    pr = new PageReference('/apex/NewTaskWater?what_id='+ task.WhatId );
    pr.setRedirect(true); 
    return pr; 
}
catch(Exception e)
{
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage())); return null; 
} 
}

}