@isTest
public class InlineWaterSalesHistoryRevenue_Test {
    
    static testMethod void InlineWaterSalesHistoryRevenue_Test()
    {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User us = new User();
        us.FirstName = 'Test';
        us.LastName = 'User';
        us.Email='standardtestuser@testsamorg.com';
        us.Alias='standt';
        us.TimeZoneSidKey='America/Los_Angeles';
        us.CommunityNickname='sample';
        us.Username='standardtestuser@testsamorg.com';
        us.ProfileId = p.Id;
        us.LocaleSidKey='en_US';
        us.EmailEncodingKey='UTF-8';
        us.LanguageLocaleKey='en_US';
        insert us;
        
        system.debug('user::' + us);
        
        Account acc = new Account();
        
        acc.Name = 'Test Account';
        acc.CurrencyIsoCode = 'AUD';
        acc.BillingCity ='Chennai' ;
        acc.BillingCountry='india';
        acc.BillingLatitude=56.57577;
        acc.BillingLongitude=78.4546;
        acc.BillingPostalCode='600075';
        acc.BillingState='tamil nadu';
        acc.BillingStreet='water well street';  
        acc.ShippingCity=null;
        acc.ShippingCountry=null;
        acc.ShippingLatitude=null;
        acc.ShippingLongitude=null;
        acc.ShippingPostalCode=null;
        acc.ShippingState=null;
        acc.ShippingStreet=null;
        acc.Veterinary_License_Number__c='TIN';
        
        insert acc;
        
        Contact con = new Contact();
        con.FirstName = 'Test'; 
        con.LastName = 'Contact'; 
        con.AccountId = acc.Id; 
        con.Title = 'Test Title'; 
        con.MobilePhone = '1111111111'; 
        insert con;        
        
        Customer_Sales_History_Water__c sh = new Customer_Sales_History_Water__c();        
        sh.ownerId = us.Id;
        sh.Product_Family__c = 'test family';
        sh.Rev3_1_Quarter_Ago__c=1000;
        sh.Rev3_2_Quarter_Ago__c =300;
        sh.Rev3_3_Quarter_Ago__c =400;
        sh.Rev3_4_Quarter_Ago__c=500;
        insert sh;
        
        Test.startTest();
        ApexPages.StandardController std = new ApexPages.StandardController(acc);
        ApexPages.currentPage().getParameters().put('what_id',acc.Id);
        
        InlineWaterSalesHistoryRevenue rev = new InlineWaterSalesHistoryRevenue(std);  
         rev.setHeaderValues();
         rev.getproductrevenue();
         
       
    }
}