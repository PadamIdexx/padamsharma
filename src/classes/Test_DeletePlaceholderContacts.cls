/**
*       @author Vishal Negandhi
*       @date   16/10/2015   
        @description   Test Class with unit test scenarios to cover the DeletePlaceHolder class
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Vishal Negandhi             15/10/2015          Original Version
**/
@isTest 	
public With Sharing class Test_DeletePlaceholderContacts {
    static testMethod void testDeletePlaceHolderContacts(){
    	// Create new Account
        Account acc = CreateTestClassData.createCustomerAccount();
        
        //Populate Contact details 
        List<Contact> contacts = new List<Contact>();
        Contact con           = new Contact(); 
        con.AccountId         = acc.Id; 
        con.LastName          = 'Place Holder'; 
        con.CurrencyIsoCode   = 'USD'; 
        contacts.add(con); 
        
        Contact con2          = new Contact(); 
        con2.AccountId         = acc.Id; 
        con2.LastName          = 'Non Place Holder'; 
        con2.CurrencyIsoCode   = 'USD'; 
        contacts.add(con2); 
        if(Schema.sObjectType.Contact.isCreateable() && contacts != null && !contacts.isEmpty())
        insert contacts; 
        
        test.startTest();
        database.executeBatch(new DeletePlaceholderContacts());
        test.stopTest();
        List<Contact> placeHolderContacts = [Select Id From Contact WHERE LastName = 'Place Holder'];
        system.assert(placeHolderContacts.size() == 0, 'Created place holder contact should be deleted');
    }
    
    static testMethod void testScheduleDeletePlaceHolderContacts(){
    	String CRON_EXP = '0 0 0 15 3 ? 2022';
    	
    	test.startTest();
    	String jobId = System.schedule('Delete Placeholder Contacts',
                        CRON_EXP,
                        new DeletePlaceholderContacts());
    	
    	test.stopTest();
    	CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
      	// Verify the expressions are the same
      	System.assertEquals(CRON_EXP, ct.CronExpression, 'Scheduled class and crontrigger expressions should match');
    }
}