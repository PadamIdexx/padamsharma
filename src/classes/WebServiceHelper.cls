public class WebServiceHelper {

    public static final String ENVIRONMENT_PROPERTY_NAME = 'ENVIRONMENT';
    
    public static final String ENVIRONMENT_DEV = 'DEV';
	public static final String ENVIRONMENT_QA = 'QA';
	public static final String ENVIRONMENT_STAGE = 'STAGE';    
	public static final String ENVIRONMENT_PRODUCTION = 'PROD';  
    
    /*
     * Retrieve integration URL based on custom setting name.
     */ 
    public static String getServiceEndpoints(String serviceName){
       IDEXX_Integration_Service__c cs = IDEXX_Integration_Service__c.getInstance(serviceName);
       
        if (null == cs) {
            System.debug('Integration not found: ' + serviceName);
            return null;
        } 
       
		//Retrieve org       
        Organization currOrg = [Select IsSandbox,OrganizationType,InstanceName from Organization limit 1];
       
        System.debug('Integration Name: ' + serviceName);
        System.debug('Org Name: ' + currOrg);
       
        if(!Currorg.IsSandBox) { //Not a sandbox.  return the endpoint (which will be prod for non-sandboxes).
        	System.debug('Endpoint: ' + cs.Endpoint__c);            
            return cs.Endpoint__c;
             
        } else { //this is a sandbox so return the appropriate endpoint for the sandbox based on environment.
        
            //retrieve sandbox environment
            IDEXX_Integration_Service_Properties__c env = IDEXX_Integration_Service_Properties__c.getInstance(ENVIRONMENT_PROPERTY_NAME);
            
            if (ENVIRONMENT_STAGE.equalsIgnoreCase(env.TextVal__c)) {
	        	System.debug('Endpoint: ' + cs.Stage_Training_Sandbox__c);                            
                return cs.Stage_Training_Sandbox__c;                
            } else if (ENVIRONMENT_QA.equalsIgnoreCase(env.TextVal__c)) {
	        	System.debug('Endpoint: ' + cs.QA_Sandbox__c);                                            
                return cs.QA_Sandbox__c;                
            } else if (ENVIRONMENT_DEV.equalsIgnoreCase(env.TextVal__c)) {
	        	System.debug('Endpoint: ' + cs.Sandbox__c);                                                            
				return cs.Sandbox__c;
            }
       }

       return null;
    }
    
     /*
      * Retrieve IDEXX_User_ID__c for specified user.         
      */
      public static String getIDEXXNetworkIdForUser(String userId){
        String idexxUserId;
        if (null <> userId) {
        	idexxUserId = [select id, IDEXX_User_ID__c from user where id=:userId].IDEXX_User_Id__c;

        	//TODO: Figure out what to do in this scenario.
        	if (String.isEmpty(idexxUserId)) {
        		idexxUserId = 'POUSER1';  //this is the system user that the SAP web services run on behalf of in Beacon              
        	}
        }
            
  		return idexxUserId;
    }
    
    /*
     * Retrieve sessionId for the integration user.  This is need to make web service calls
     * back to IDEXX.
     */ 
    public static String getIntegrationSessionId() {
        String sessionId;
        
        APIUser__c creds = APIUser__c.getInstance('API USER');
        
        if(creds !=null){
            sessionId = Login.login(creds.Username__c,creds.Password__c+creds.Security_Token__c);
        } 
        
        return sessionId;
    }
        
        
}