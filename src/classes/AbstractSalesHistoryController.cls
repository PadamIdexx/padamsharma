/**
 * @author sconnolly
 * @description Base class for sales history that is extended for specific functionality.
 *
 *  Modification Log:
 * -----------------------------------------------------------------------------------------------------------------------------
 *  Developer                       Date               Description
 * -----------------------------------------------------------------------------------------------------------------------------
 *  Heather Kinney                  05/31/2016         US23018 - Merging R2.1 changes into R3.0 - Ref Lab and Practice Supplies.
 *  Heather Kinney                  06/01/2016         DE8392  - Fix for column month name repeat issue when month has 31 days.
 *
 */
public with sharing abstract class AbstractSalesHistoryController {

    public static final String DT_FORMAT = 'MMM yy';
    public static final Integer FIRST_DAY_OF_MONTH = 1; //DE8392

    protected ApexPages.StandardController controller {get;set;}
    public List<SalesHistoryWrapper>  CagKits {get;set;}
    public List<SalesHistoryWrapper>  CagConsumables {get;set;}
    public List<SalesHistoryWrapper>  CagLabServices {get;set;}
    public List<SalesHistoryWrapper> CAGTelemed {get;set;}
    public List<SalesHistoryWrapper>  CAGSerologyImmunology {get;set;}
    public List<SalesHistoryWrapper> Chemistry {get;set;}
    public List<SalesHistoryWrapper> Parasitology {get;set;}
    public List<SalesHistoryWrapper> CAGReferenceLabSupplies {get;set;}
    public List<SalesHistoryWrapper> CAGPracticeSupplies {get;set;}

    public List<SalesHistoryWrapper> CagKitsTotal {get;set;}
    public List<SalesHistoryWrapper> CagConsumablesTotal {get;set;}
    public List<SalesHistoryWrapper> CagLabServicesTotal {get;set;}
    public List<SalesHistoryWrapper> CAGPracticeSuppliesTotal {get;set;}
    public List<SalesHistoryWrapper> CagTotal {get;set;}
    public List<SalesHistoryWrapper> CagTelemedTotal {get;set;}
    public List<SalesHistoryWrapper> CagTRGTotal {get;set;}
    
    //  Practice Supplies - New wrapper class, lists, and map expressing CAG Practice Supplies in a 2nd level hierarchy.
    public class CAGPracticeSuppliesWrapper {
        public List<SalesHistoryWrapper> cshList {get;set;}
        public String productFamilyName {get;set;}

        public CAGPracticeSuppliesWrapper(SalesHistoryWrapper ps2) {
            productFamilyName = ps2.wData.Product_Family__c;
            cshList = new List<SalesHistoryWrapper> {ps2};
        }
    }
    public List<CAGPracticeSuppliesWrapper> cagPracSupplies {get;set;}
    private Map<String, CAGPracticeSuppliesWrapper> mapCAGPracSupplies = new Map<String, CAGPracticeSuppliesWrapper>();
    //END: US23018 - Ref Lab and Practice Supplies

    public static final String PG_RAPID_ASSAY = 'RAPID ASSAY';
    public static final String PG_CATALYST_SLIDES = 'CATALYST SLIDES';
    public static final String PG_CHEMISTRY = 'CHEMISTRY';
    public static final String PG_CLINICAL_PATHOLOGY = 'CLINICAL PATHOLOGY';
    public static final String PG_COAG_CONSUMABLES = 'COAG CONSUMABLES';
    public static final String PG_ENDOCRINOLOGY = 'ENDOCRINOLOGY';
    public static final String PG_ESOTERICS = 'ESOTERICS';
    public static final String PG_HEMATOLOGY = 'HEMATOLOGY';
    public static final String PG_HISTOPATHOLOGY = 'HISTOPATHOLOGY';
    public static final String PG_LASERCYTE_CONSUMABLES = 'LASERCYTE CONSUMABLES';
    public static final String PG_MICROBIOLOGY = 'MICROBIOLOGY';
    public static final String PG_MISCELLANEOUS_LABS = 'MISCELLANEOUS LABS';
    public static final String PG_MOLECULAR_DIAGNSOTICS = 'MOLECULAR DIAGNOSTICS';
    public static final String PG_PARASITOLOGY = 'PARASITOLOGY';
    public static final String PG_PROCYTE_CONSUMABLES = 'PROCYTE CONSUMABLES';
    public static final String PG_QUANTATIVE_ASSAY = 'QUANTATIVE ASSAY';
    public static final String PG_SEROLOGY_IMMUNOLOGY = 'SEROLOGY/IMMUNOLOGY';
    public static final String PG_UA_CONSUMABLES = 'UA CONSUMABLES';
    public static final String PG_VETAUTOREAD_TUBES = 'VETAUTOREAD TUBES';
    public static final String PG_VETLYTE_CONSUMABLES = 'VETLYTE CONSUMABLES';
    public static final String PG_VETSTAT_CONSUMABLES = 'VETSTAT CONSUMABLES';
    public static final String PG_VETTEST_CONSUMABLES = 'VETTEST CONSUMABLES';
    public static final String PG_SEDIVUE = 'SEDIVUE';
   // public static final String PG_FECALCHECK = 'FECAL CHECK';
    public static final String PG_PRACTICE_SUPPLIES = 'PRACTICE SUPPLIES'; //US23018 - Ref Lab and Practice Supplies
    public static final String PG_REFERENCE_LAB_SUPPLIES = 'REFERENCE LAB SUPPLIES'; //US23018 - Ref Lab and Practice Supplies

    public static final String PL_CHEMISTRY = 'CHEMISTRY';
    public static final String PL_HEMATOLOGY = 'HEMATOLOGY';
    public static final String PL_URINALYSIS = 'URINALYSIS';
    public static final String PL_TELEMED = 'TELEMED';
    public static final String PL_TRG = 'TRG';
    public static final String PL_SNAP = 'SNAP';
    public static final String PL_SNAP_BASED = 'SNAP-BASED Q-ASSAY';

    
    public static final String PF_CAG_SUMMARY = 'CAG SUMMARY';
    public static final String PF_CAG_KITS_TOTAL = 'CAG KITS TOTAL';
    public static final String PF_CAG_LABS_TOTAL = 'CAG LABS TOTAL';
    public static final String PF_CAG_TOTAL = 'CAG TOTAL';
    public static final String PF_CAG_TELEMED_TOTAL = 'CAG TELEMED TOTAL';
    
     public static final String PF_CAG_TRG_TOTAL = 'CAG TRG TOTAL';
    
    public static final String PF_CAG_CONSUMABLES_TOTAL = 'CAG CONSUMABLES TOTAL';
    public static final String PF_CAG_PRACTICE_SUPPLIES_TOTAL = 'CAG PRACTICE SUPPLIES TOTAL';
    public static final String PF_CAG_REF_LAB_SUPPLIES_TOTAL = 'CAG REF LAB SUPPLIES TOTAL';        // LAKSHMI ADDED THIS FOR DEF# DE9064
    public static final String PF_QUANT_ASSAY = 'QUANT ASSAY';
    

    public Boolean hasData                  {get;set;}
    public List<String> noRecordsFound      {get;set;}
    public Account currentAccountRecord {get;set;}

    protected String salesHistorySelect;

    public SalesHistoryUIData uiData {get;set;}

    public String totalsQuery;
    public String cagKitsQuery;
    public String cagConsumablesQuery;
    public String cagLabServicesQuery;
    public String cagTelemedQuery;
    public String cagTRGQuery;
    public String cagSerologyImmunologyQuery;
    public String chemistryQuery;
    public String ParasitologyQuery;
    public String cagReferenceLabSuppliesQuery; //US23018 - Ref Lab and Practice Supplies
    public String cagPracticeSuppliesQuery; //US23018 - Ref Lab and Practice Supplies

    //constuctor
    public AbstractSalesHistoryController(ApexPages.StandardController controller) {

        //Initializations of the local variables
        this.controller           = controller;

        initLists();

        // fetch the account details
        currentAccountRecord = (Account)controller.getRecord();
        if (currentAccountRecord<>null && currentAccountRecord.Id<>null) {
            currentAccountRecord = [Select Id, Name, SAP_CUSTOMER_NUMBER__c,SAP_Number_without_Leading_Zeros__c from Account where Id=:currentAccountRecord.Id];
            setupQueries(currentAccountRecord.SAP_CUSTOMER_NUMBER__c);

            //TODO: Should run this after page loads, not before.
            GroupProducts2();
        }

        setHeaderValues();
    }

    protected abstract void setupQueries(String sapId);

    protected void initLists() {
        CagKits = new List<SalesHistoryWrapper> ();
        CagConsumables = new List<SalesHistoryWrapper>();
        CagLabServices = new List<SalesHistoryWrapper>();
        CAGTelemed = new List<SalesHistoryWrapper>();
        CAGSerologyImmunology = new List<SalesHistoryWrapper>();
        Chemistry = new List<SalesHistoryWrapper>();
        Parasitology = new List<SalesHistoryWrapper>();
        CAGReferenceLabSupplies = new List<SalesHistoryWrapper>(); //US23018 - Ref Lab and Practice Supplies
        CAGPracticeSupplies = new List<SalesHistoryWrapper>(); //US23018 - Ref Lab and Practice Supplies
        cagPracSupplies = new List<CAGPracticeSuppliesWrapper>(); //US23018 - Ref Lab and Practice Supplies

        CagKitsTotal = new List<SalesHistoryWrapper>();
        CagConsumablesTotal = new List<SalesHistoryWrapper>();
        CagLabServicesTotal = new List<SalesHistoryWrapper>();
        CagTotal = new List<SalesHistoryWrapper>();
        CagTRGTotal = new List<SalesHistoryWrapper>();
        CagTelemedTotal = new List<SalesHistoryWrapper>();
        CAGPracticeSuppliesTotal = new List<SalesHistoryWrapper>();

        hasData = false;
        noRecordsFound = new List<String>();

    }

    public void GroupProducts2() {
        // Kits : fetch all sales history records for Kits
        for (Customer_Sales_History__c csh : database.query(this.cagKitsQuery)) {
            CagKits.add(new SalesHistoryWrapper(csh));
            hasData = true;
        }

        // Consumables
        for (Customer_Sales_History__c csh : database.query(cagConsumablesQuery)) {
            CagConsumables.add(new SalesHistoryWrapper(csh));
            hasData = true;
        }

        // Lab services
        for (Customer_Sales_History__c csh : database.query(this.cagLabServicesQuery)) {
            CagLabServices.add(new SalesHistoryWrapper(csh));
            hasData = true;
        }

        // TELEMED
        for (Customer_Sales_History__c csh : database.query(cagTelemedQuery)) {
            CAGTelemed.add(new SalesHistoryWrapper(csh));
            hasData = true;
        }
        
        
                // TRG
        for (Customer_Sales_History__c csh : database.query(cagTRGQuery)) {
            CAGTelemed.add(new SalesHistoryWrapper(csh));
            hasData = true;
        }

        // SEROLOGY/IMMUNOLOGY
        for (Customer_Sales_History__c csh : database.query(cagSerologyImmunologyQuery)) {
            CAGSerologyImmunology.add(new SalesHistoryWrapper(csh));
            hasData = true;
        }

        // CHEMISTRY
        for (Customer_Sales_History__c csh : database.query(chemistryQuery)) {
            Chemistry.add(new SalesHistoryWrapper(csh));
            hasData = true;
        }

               // PARASITOLOGY
         for (Customer_Sales_History__c csh : database.query(ParasitologyQuery)) {
            Parasitology.add(new SalesHistoryWrapper(csh));
            hasData = true;
         }
      
        //BEGIN: US23018 - Ref Lab and Practice Supplies
        //CAG REFERENCE LAB SUPPLIES
        if (cagReferenceLabSuppliesQuery != null) {
            for (Customer_Sales_History__c csh : database.query(cagReferenceLabSuppliesQuery)) {
                CAGReferenceLabSupplies.add(new SalesHistoryWrapper(csh));
            }
        }

        //CAG PRACTICE SUPPLIES
        if (cagPracticeSuppliesQuery != null) {
            for (Customer_Sales_History__c csh : database.query(cagPracticeSuppliesQuery)) {
                CAGPracticeSupplies.add(new SalesHistoryWrapper(csh));
                hasData = true;
            }
        }
        //.....iterate through CAGPracticeSupplies - and add contents to CAGPracticeSuppliesWrapper as we
        //.....need to express Practice Supplies in a three-level hierarchy: product group -> product family -> product name.
        if (CAGPracticeSupplies != null) {
            for (SalesHistoryWrapper ps2 : CAGPracticeSupplies) {
                if (mapCAGPracSupplies.get(ps2.wData.Product_Family__c) == null) {
                    mapCAGPracSupplies.put(ps2.wData.Product_Family__c, new CAGPracticeSuppliesWrapper(ps2));
                    cagPracSupplies.add(mapCAGPracSupplies.get(ps2.wData.Product_Family__c));
                } else {
                    mapCAGPracSupplies.get(ps2.wData.Product_Family__c).cshList.add(ps2);
                }
            }
        }
        //END: US23018 - Ref Lab and Practice Supplies

        // *********** TOTALS COMBINED IN A SINGLE QUERY **********
        for (Customer_Sales_History__c csh : database.query(totalsQuery)) {
            if (csh.PRODUCT_FAMILY__c == 'CAG KITS TOTAL') {
                CagKitsTotal.add(new SalesHistoryWrapper(csh));
            }

            if (csh.PRODUCT_FAMILY__c == 'CAG LABS TOTAL') {
                CagLabServicesTotal.add(new SalesHistoryWrapper(csh));
            }

            if (csh.PRODUCT_FAMILY__c == 'CAG TOTAL') {
                CagTotal.add(new SalesHistoryWrapper(csh));
            }

            if (csh.PRODUCT_FAMILY__c == 'CAG TELEMED TOTAL') {
                CagTelemedTotal.add(new SalesHistoryWrapper(csh));
            }

          if (csh.PRODUCT_FAMILY__c == 'CAG TRG TOTAL') {
                CagTRGTotal.add(new SalesHistoryWrapper(csh));
            }
            
            if (csh.PRODUCT_FAMILY__c == 'CAG CONSUMABLES TOTAL') {
                CagConsumablesTotal.add(new SalesHistoryWrapper(csh));
            }

            if (csh.PRODUCT_FAMILY__c == 'CAG PRACTICE SUPPLIES TOTAL') {
                CAGPracticeSuppliesTotal.add(new SalesHistoryWrapper(csh));
            }
            
           // LAKSHMI ADDED THIS FOR DEF# DE9064
            if (csh.PRODUCT_FAMILY__c == 'CAG REF LAB SUPPLIES TOTAL') {
                CAGPracticeSuppliesTotal.add(new SalesHistoryWrapper(csh));
            }
            hasData = true;
        }

        if (!hasData) {
            noRecordsFound = new List<String>{system.Label.No_records};
        }
    }

    protected virtual void setHeaderValues() {
        this.uiData = new SalesHistoryUIData();
    }

    private static final String LIMIT_CLAUSE = ' ORDER BY PRODUCT_FAMILY__c Asc LIMIT '
        + (Limits.getLimitQueryRows() - Limits.getQueryRows());

    //US23018 - Ref Lab and Practice Supplies
    // Due to the additional level of hierarchical display for Practice Supplies, we need to sort by Product Family,
    // then by Product Name.
    // 
    private static final String LIMIT_CLAUSE_WITH_ORDER_BY_PROD_FAMILY_AND_PROD_NAME = ' ORDER BY PRODUCT_FAMILY__c ASC,  PRODUCT_NAME__C ASC LIMIT '
        + (Limits.getLimitQueryRows() - Limits.getQueryRows());

    public static final String CAG_KITS_WHERE_CLAUSE =
        ' AND PRODUCT_GROUP__c = \'' + PG_RAPID_ASSAY
        + '\' AND PRODUCT_LINE__c = \'' + PL_SNAP
        + '\' AND PRODUCT_FAMILY__c != \'' + PF_CAG_SUMMARY + '\''
        + LIMIT_CLAUSE;

    public static final String CONSUMABLES_WHERE_CLAUSE =
        ' AND PRODUCT_LINE__c IN ('
        + '\'' + PL_CHEMISTRY 
        + '\', \'' + PL_SNAP_BASED 
        + '\', \'' + PL_HEMATOLOGY
        + '\', \'' + PL_URINALYSIS
        
        + '\') AND PRODUCT_GROUP__c IN ('
        + '\'' + PG_PROCYTE_CONSUMABLES
        + '\', \'' + PG_LASERCYTE_CONSUMABLES
        + '\', \'' + PG_VETAUTOREAD_TUBES
        + '\', \'' + PG_COAG_CONSUMABLES
        + '\', \'' + PG_UA_CONSUMABLES
        + '\', \'' + PG_CATALYST_SLIDES
        + '\', \'' + PG_VETTEST_CONSUMABLES
        + '\', \'' + PG_VETLYTE_CONSUMABLES
        + '\', \'' + PG_VETSTAT_CONSUMABLES
        + '\', \'' +PG_SEDIVUE 
      //  + '\', \'' +PG_FECALCHECK
        + '\', \'' + PG_QUANTATIVE_ASSAY
        + '\') AND PRODUCT_FAMILY__c != \'' + PF_CAG_SUMMARY + '\''
        + LIMIT_CLAUSE;

    public static final String LAB_SERVICES_WHERE_CLAUSE =
        ' AND PRODUCT_GROUP__c IN ('
        + '\'' + PG_ENDOCRINOLOGY + '\''
        + ', \'' + PG_MICROBIOLOGY+ '\''
        + ', \'' + PG_HISTOPATHOLOGY+ '\''
        + ', \'' + PG_CLINICAL_PATHOLOGY+ '\''
        + ', \'' + PG_ESOTERICS+ '\''
        + ', \'' + PG_HEMATOLOGY+ '\''
        + ', \'' + PG_MISCELLANEOUS_LABS+ '\''
        + ', \'' + PG_MOLECULAR_DIAGNSOTICS+ '\')'
        //+ ', \'' + PG_PARASITOLOGY+ '\')'
        + LIMIT_CLAUSE;

       
    public static final String TELEMED_WHERE_CLAUSE =
        ' AND PRODUCT_LINE__c = \'' + PL_TELEMED + '\''
        + LIMIT_CLAUSE;
    
       public static final String TRG_WHERE_CLAUSE =
        ' AND PRODUCT_FAMILY__c = \'' + PL_TRG + '\''
        + LIMIT_CLAUSE;

    public static final String SER_IMM_WHERE_CLAUSE =
        ' AND PRODUCT_GROUP__c = \'' + PG_SEROLOGY_IMMUNOLOGY+ '\''
        + LIMIT_CLAUSE;

      public static final String PARA_WHERE_CLAUSE =
      ' AND PRODUCT_GROUP__c = \'' + PG_PARASITOLOGY+ '\''
      + LIMIT_CLAUSE;
        
    public static final String CHEM_WHERE_CLAUSE =
        ' AND PRODUCT_GROUP__c = \'' + PG_CHEMISTRY + '\''
        + LIMIT_CLAUSE;

    //BEGIN: US23018 - Ref Lab and Practice Supplies
    public static final String REFERENCE_LAB_SUPPLIES_WHERE_CLAUSE =
        ' AND PRODUCT_GROUP__c = \'' + PG_REFERENCE_LAB_SUPPLIES + '\''
        + LIMIT_CLAUSE;

    public static final String PRACTICE_SUPPLIES_WHERE_CLAUSE =
        ' AND PRODUCT_GROUP__c = \'' + PG_PRACTICE_SUPPLIES + '\''
        + LIMIT_CLAUSE_WITH_ORDER_BY_PROD_FAMILY_AND_PROD_NAME;
    //END: US23018 - Ref Lab and Practice Supplies

    public static final String TOTALS_WHERE_CLAUSE = ' AND PRODUCT_FAMILY__c in ('
        + '\'' + PF_CAG_KITS_TOTAL + '\', '
        + '\'' + PF_CAG_LABS_TOTAL + '\', '
        + '\'' + PF_CAG_TOTAL + '\', '
        + '\'' + PF_CAG_TELEMED_TOTAL + '\', '
        + '\'' + PF_CAG_TRG_TOTAL + '\', '
        + '\'' + PF_CAG_CONSUMABLES_TOTAL + '\', '
         + '\'' + PF_CAG_REF_LAB_SUPPLIES_TOTAL + '\', '        // LAKSHMI ADDED THIS FOR DEF# DE9064
        + '\'' + PF_CAG_PRACTICE_SUPPLIES_TOTAL + '\')'
        + LIMIT_CLAUSE;

    public class SalesHistoryWrapper {
        public Customer_Sales_History__c wData {get; set;}

        public SalesHistoryWrapper(Customer_Sales_History__c row) {
            this.wData = row;
        }

        private String getDiffStyle(Decimal val1, Decimal val2) {
            String value='';
            if (val1 != null) {
                if (val1 > val2) {
                    value = 'growth';
                } else if (val1 < val2) {
                    value = 'loss';
                }
            }
            return value;
        }

        public String getQtdGrowthStyle() {
        // lakshmi changed from wData.QTD__c  to wData.QTD_Last_Year__c
            return getDiffStyle(wData.QTD_Last_Year__c,
                                0);
        }

        public Decimal getQtdLastDiff() {
            Decimal value;
            if (wData.QTD__c != null) {
                value = wData.QTD__c - wData.QTD_Last_Year__c;
            }
            return value;
        }

        public String getCurrentMonthGrowthStyle() {
            return getDiffStyle(wData.Rev3_Current_Month__c,
                                wData.Rev3_Current_Month_Year_Ago__c);
        }

        public String getLastMonthGrowthStyle() {
            return getDiffStyle(wData.Rev3_1_Month_Ago__c,
                                wData.Rev3_13_Months_Ago__c);
        }

        public String getTwoMonthsAgoGrowthStyle() {
            return getDiffStyle(wData.Rev3_2_Months_Ago__c,
                                wData.Rev3_14_Months_Ago__c);
        }

        public String getThreeMonthsAgoGrowthStyle() {
            return getDiffStyle(wData.Rev3_3_Months_Ago__c,
                                wData.Rev3_15_Months_Ago__c);
        }

        public String getFourMonthsAgoGrowthStyle() {
            return getDiffStyle(wData.Rev3_4_Months_Ago__c,
                                wData.Rev3_16_Months_Ago__c);
        }

        public String getFiveMonthsAgoGrowthStyle() {
            return getDiffStyle(wData.Rev3_5_Months_Ago__c,
                                wData.Rev3_17_Months_Ago__c);
        }

        public String getSixMonthsAgoGrowthStyle() {
            return getDiffStyle(wData.Rev3_6_Months_Ago__c,
                                wData.Rev3_18_Months_Ago__c);
        }

        public String getSevenMonthsAgoGrowthStyle() {
            return getDiffStyle(wData.Rev3_7_Months_Ago__c,
                                wData.Rev3_19_Months_Ago__c);
        }

        public String getEightMonthsAgoGrowthStyle() {
            return getDiffStyle(wData.Rev3_8_Months_Ago__c,
                                wData.Rev3_20_Months_Ago__c);
        }

        public String getNineMonthsAgoGrowthStyle() {
            return getDiffStyle(wData.Rev3_9_Months_Ago__c,
                                wData.Rev3_21_Months_Ago__c);
        }

        public String getTenMonthsAgoGrowthStyle() {
            return getDiffStyle(wData.Rev3_10_Months_Ago__c,
                                wData.Rev3_22_Months_Ago__c);
        }

        public String getElevenMonthsAgoGrowthStyle() {
            return getDiffStyle(wData.Rev3_11_Months_Ago__c,
                                wData.Rev3_23_Months_Ago__c);
        }

        public String getQtyYOYGrowthStyle() {
            String value;
            if (wData.Product_FAMILY__c != null && wData.L04_Quantity_YOY__C != null) {
                if (wData.L04_Quantity_YOY__c > 0) {
                    value = 'growth';
                } else if (wData.L04_Quantity_YOY__c < 0) {
                    value = 'loss';
                }
            } else {
                value = '';
            }
            return value;
        }

        public String getUnitsYOYGrowthStyle() {
            String value;
            // Sales History Units fields.
            if (wData.Product_FAMILY__c != null) {
                if (wData.L04_Units_YOY__c > 0) {
                    value = 'growth';
                } else if (wData.L04_Units_YOY__c < 0) {
                    value = 'loss';
                }
            } else {
                value = '';
            }
            return value;
        }
    }

    public class SalesHistoryUIData {
        // column headers
        public String MONTH_Current  {get;set;}
        public String MONTH_1_AGO    {get;set;}
        public String MONTH_2_AGO    {get;set;}
        public String MONTH_3_AGO    {get;set;}
        public String MONTH_4_AGO    {get;set;}
        public String MONTH_5_AGO    {get;set;}
        public String MONTH_6_AGO    {get;set;}
        public String MONTH_7_AGO    {get;set;}
        public String MONTH_8_AGO    {get;set;}
        public String MONTH_9_AGO    {get;set;}
        public String MONTH_10_AGO    {get;set;}
        public String MONTH_11_AGO    {get;set;}
        public String MONTH_12_AGO    {get;set;}
        public String MONTH_13_AGO    {get;set;}
        public String MONTH_14_AGO    {get;set;}
        public String MONTH_15_AGO    {get;set;}
        public String MONTH_16_AGO    {get;set;}
        public String MONTH_17_AGO    {get;set;}
        public String MONTH_18_AGO    {get;set;}
        public String MONTH_19_AGO    {get;set;}
        public String MONTH_20_AGO    {get;set;}
        public String MONTH_21_AGO    {get;set;}
        public String MONTH_22_AGO    {get;set;}
        public String MONTH_23_AGO    {get;set;}

        public SalesHistoryUIData() {
            // initialise all variables
            Date dToday = date.Today();
            // all this year headers
            DateTime dt = DateTime.newInstance(dToday.year(), dToday.month(),dToday.day());
            //DE8392  - Fix for column month name repeat issue when month has 31 days.
            // For the setting of the "MONTH_NN_AGO" vars, FIRST_DAY_OF_MONTH is now being used
            // as opposed to dToday.day(), which for those current months with 31 days, resulted
            // in a flip to that same month: May 31 (cur month); April 31 (prev month) which
            // flipped to that current month (May in this example).
            MONTH_CURRENT = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 1,FIRST_DAY_OF_MONTH);
            MONTH_1_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 2,FIRST_DAY_OF_MONTH);
            MONTH_2_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 3,FIRST_DAY_OF_MONTH);
            MONTH_3_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 4,FIRST_DAY_OF_MONTH);
            MONTH_4_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 5,FIRST_DAY_OF_MONTH);
            MONTH_5_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 6,FIRST_DAY_OF_MONTH);
            MONTH_6_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 7,FIRST_DAY_OF_MONTH);
            MONTH_7_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 8,FIRST_DAY_OF_MONTH);
            MONTH_8_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 9,FIRST_DAY_OF_MONTH);
            MONTH_9_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 10,FIRST_DAY_OF_MONTH);
            MONTH_10_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 11,FIRST_DAY_OF_MONTH);
            MONTH_11_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 12,FIRST_DAY_OF_MONTH);
            MONTH_12_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 13,FIRST_DAY_OF_MONTH);
            MONTH_13_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 14,FIRST_DAY_OF_MONTH);
            MONTH_14_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 15,FIRST_DAY_OF_MONTH);
            MONTH_15_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 16,FIRST_DAY_OF_MONTH);
            MONTH_16_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 17,FIRST_DAY_OF_MONTH);
            MONTH_17_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 18,FIRST_DAY_OF_MONTH);
            MONTH_18_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 19,FIRST_DAY_OF_MONTH);
            MONTH_19_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 20,FIRST_DAY_OF_MONTH);
            MONTH_20_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 21,FIRST_DAY_OF_MONTH);
            MONTH_21_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 22,FIRST_DAY_OF_MONTH);
            MONTH_22_AGO = dt.format(DT_FORMAT);
            dt = DateTime.newInstance(dToday.year(), dToday.month() - 23,FIRST_DAY_OF_MONTH);
            MONTH_23_AGO = dt.format(DT_FORMAT);

        }

    }

    public static final String[] SH_COMMON_FIELDS = new String[] {
        'Id',
        'PRODUCT_FAMILY__c',
        'PRODUCT_GROUP__c',
        'PRODUCT_LINE__C',
        'PRODUCT_NAME__c' //US23018 - Ref Lab and Practice Supplies; we need the product name in both views.
    };

    public static final String[] SH_REVENUE_FIELDS = new String[] {
        'L12YoY_Diff__c', 
        'L12_Revenue__c',
        'L12_YOY__c',
        'QTD_Last_Year__c',
        'QTD__c',
        'Rev3_10_Months_Ago__c',
        'Rev3_11_Months_Ago__c',
        'Rev3_12_Months_Ago__c',
        'Rev3_13_Months_Ago__c',
        'Rev3_14_Months_Ago__c',
        'Rev3_15_Months_Ago__c',
        'Rev3_16_Months_Ago__c',
        'Rev3_17_Months_Ago__c',
        'Rev3_18_Months_Ago__c',
        'Rev3_19_Months_Ago__c',
        'Rev3_1_Month_Ago__c',
        'Rev3_20_Months_Ago__c',
        'Rev3_21_Months_Ago__c',
        'Rev3_22_Months_Ago__c',
        'Rev3_23_Months_Ago__c',
        'Rev3_24_Months_Ago__c',
        'Rev3_2_Months_Ago__c',
        'Rev3_3_Months_Ago__c',
        'Rev3_4_Months_Ago__c',
        'Rev3_5_Months_Ago__c',
        'Rev3_6_Months_Ago__c',
        'Rev3_7_Months_Ago__c',
        'Rev3_8_Months_Ago__c',
        'Rev3_9_Months_Ago__c',
        'Rev3_Current_Month_Year_Ago__c',
        'Rev3_Current_Month__c',
        'YOY_10_Months_Ago__c',
        'YOY_11_Months_Ago__c',
        'YOY_1_Month_Ago__c',
        'YOY_2_Months_Ago__c',
        'YOY_3_Months_Ago__c',
        'YOY_4_Months_Ago__c',
        'YOY_5_Months_Ago__c',
        'YOY_6_Months_Ago__c',
        'YOY_7_Months_Ago__c',
        'YOY_8_Months_Ago__c',
        'YOY_9_Months_Ago__c',
        'YOY_CurrentMonth__c',
         'MonthYoY_Diff__c',
         'LastMonthYoY_Diff__c',
         'AverageMonthRev__c',
         'Last3MonthsRev__c',
         'Last3MonthsYOY__c',
         'Last3MonthsYOY_Diff__c',
         'Last6MonthsRev__c',
         'Last6MonthsYOY__c',
         'Last6MonthsYOY_Diff__c' ,
         'Last9MonthsRev__c',
         'Last9MonthsYOY__c',
          'Last9MonthsYOY_Diff__c',
          'QTDYOY_Diff__c',

        'Units_Current_Month__c',
        'Units_Current_Month_Year_Ago__c',
        'Units_1_Month_Ago__c',
        'Units_13_Months_Ago__c',
        'Units_2_Months_Ago__c',
        'Units_14_Months_Ago__c',
        'Units_3_Months_Ago__c',
        'Units_15_Months_Ago__c',
        'Units_4_Months_Ago__c',
        'Units_16_Months_Ago__c',
        'Units_5_Months_Ago__c',
        'Units_17_Months_Ago__c',
        'Units_6_Months_Ago__c',
        'Units_18_Months_Ago__c',
        'Units_7_Months_Ago__c',
        'Units_19_Months_Ago__c',
        'Units_8_Months_Ago__c',
        'Units_20_Months_Ago__c',
        'Units_9_Months_Ago__c',
        'Units_21_Months_Ago__c',
        'Units_10_Months_Ago__c',
        'Units_22_Months_Ago__c',
        'Units_11_Months_Ago__c',
        'Units_23_Months_Ago__c',
        'L04_Units__c',
        'L04_Units_YOY__c',
        'Units_Current_Month_YOY_Diff__c',
        'Unit_Last_Month_YOY__c',
        'Unit_Last_Month_YOY_Diff__c',
        'Last_12_Months_Units__c',
        'AverageMonthUnits__c',
        'Last12MonthUintsYOY__c',
        'Last12MonthsUnitsYOY_Diff__c',
        'Last3MonthsUnits__c',
        'Last3MonthsUnitsYOY__c',   
        'Last3MonthsUinitsYOY_Diff__c',
         'Last6MonthsUnits__c',
         'Last6MonthsUnitsYOY__c',
         'Last6MonthsUinitsYOY_Diff__c',
         'Last9MonthsUnits__c',
         'Last9MonthUintsYOY__c',
         'Last9MonthsUinitsYOY_Diff__c',
          'QTD_Units__c',
          'QTDUnitsYOY__c',
          'QTD_Units_YOY_Diff__c'
                 
    };

    public static final String[] SH_QUANTITY_FIELDS = new String[] {
        'Quantity_Current_Month__c',
        'Quantity_Current_Month_Year_Ago__c',      
        'Quantity_1_Month_Ago__c',
        'Quantity_13_Months_Ago__c',
        'Quantity_2_Months_Ago__c',
        'Quantity_14_Months_Ago__c',
        'Quantity_3_Months_Ago__c',
        'Quantity_15_Months_Ago__c',
        'Quantity_4_Months_Ago__c',
        'Quantity_16_Months_Ago__c',
        'Quantity_5_Months_Ago__c',
        'Quantity_17_Months_Ago__c',
        'Quantity_6_Months_Ago__c',
        'Quantity_18_Months_Ago__c',
        'Quantity_7_Months_Ago__c',
        'Quantity_19_Months_Ago__c',
        'Quantity_8_Months_Ago__c',
        'Quantity_20_Months_Ago__c',
        'Quantity_9_Months_Ago__c',
        'Quantity_21_Months_Ago__c',
        'Quantity_10_Months_Ago__c',
        'Quantity_22_Months_Ago__c',
        'Quantity_11_Months_Ago__c',
        'Quantity_23_Months_Ago__c',              
        'L04_Quantity__c',
        'L04_Quantity_1_Year_Ago__c',
        'L04_Quantity_YOY__c',
        'Units_Current_Month__c',
        'Units_Current_Month_Year_Ago__c',
        'Units_1_Month_Ago__c',
        'Units_13_Months_Ago__c',
        'Units_2_Months_Ago__c',
        'Units_14_Months_Ago__c',
        'Units_3_Months_Ago__c',
        'Units_15_Months_Ago__c',
        'Units_4_Months_Ago__c',
        'Units_16_Months_Ago__c',
        'Units_5_Months_Ago__c',
        'Units_17_Months_Ago__c',
        'Units_6_Months_Ago__c',
        'Units_18_Months_Ago__c',
        'Units_7_Months_Ago__c',
        'Units_19_Months_Ago__c',
        'Units_8_Months_Ago__c',
        'Units_20_Months_Ago__c',
        'Units_9_Months_Ago__c',
        'Units_21_Months_Ago__c',
        'Units_10_Months_Ago__c',
        'Units_22_Months_Ago__c',
        'Units_11_Months_Ago__c',
        'Units_23_Months_Ago__c',
        'L04_Units__c',
        'L04_Units_YOY__c',
        'Units_Current_Month_YOY_Diff__c',
        'Unit_Last_Month_YOY__c',
        'Unit_Last_Month_YOY_Diff__c',
        'Last_12_Months_Units__c',
        'AverageMonthUnits__c',
        'Last12MonthUintsYOY__c',
        'Last12MonthsUnitsYOY_Diff__c',
        'Last3MonthsUnits__c',
        'Last3MonthsUnitsYOY__c',   
        'Last3MonthsUinitsYOY_Diff__c',
         'Last6MonthsUnits__c',
         'Last6MonthsUnitsYOY__c',
         'Last6MonthsUinitsYOY_Diff__c',
         'Last9MonthsUnits__c',
         'Last9MonthUintsYOY__c',
         'Last9MonthsUinitsYOY_Diff__c',
          'QTD_Units__c',
          'QTDUnitsYOY__c',
          'QTD_Units_YOY_Diff__c'
       
        };

}