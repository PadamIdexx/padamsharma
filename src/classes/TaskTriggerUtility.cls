/* Class Name  : TaskTriggerUtility
 * Description   : Apex Handler for Trigger on Task
 * Created By    : Raushan Anand
 * Created On    : 10-Nov-2015
 *
 *  Modification Log :
 *  --------------------------------------------------------------------------------------
 *  * Developer                    Date                    Description
 *  * ------------------------------------------------------------------------------------                 
 *  * Raushan Anand              10-Nov-2015               Initial version.
 *  * Pooja Wade                 22-Dec-2015               TKT - 241
 *****************************************************************************************/
public with sharing class TaskTriggerUtility {
   
    //to update the Last Touch Date
    public static void updateLastTouchDate(List<Task> taskList){

     
       List<AccountShare> accShare = new List<AccountShare>();
       Map<Id,DateTime> accIds = new Map<Id,DateTime>();
        List<Account> accList = new List<Account>(); 
        Map<ID,Schema.RecordTypeInfo> rt_Map = Task.sObjectType.getDescribe().getRecordTypeInfosById();
        boolean isSuccessful = false;
        for(Task oTask : taskList){
            if(oTask.WhatId != null && String.valueOf(oTask.WhatId).startsWith('001') && (rt_map.get(oTask.recordTypeID).getName().containsIgnoreCase('Log An Interaction') || rt_map.get(oTask.recordTypeID).getName().containsIgnoreCase('Sales Activity'))){
                accIds.put(oTask.WhatId,System.now());
                if(oTask.Successful__c == true){
                    isSuccessful = true;
                }
            }
        }
      
     if(!accIds.isEmpty()){
            for(Account acc : [SELECT Id, My_Last_Touch_Date__c, successful__c FROM Account WHERE ID IN: accIds.Keyset()]){
                acc.My_Last_Touch_Date__c= accIds.get(acc.Id);
                acc.Successful__c = acc.Successful__c || isSuccessful  ? true : false;
                accList.add(acc);
            }
        }
        if(Account.sObjectType.getDescribe().isUpdateable() && (!accList.isEmpty())){
            UPDATE accList;
        }
    }
}