@isTest public class Test_DeleteWrapper {
    @isTest static void testPositive() {
        Test.startTest();
        try {
            SAPOrder order = OrderTestDataFactory.createSAPOrder();
            DeleteWrapper w = new DeleteWrapper();
            w.execute(order);
        } catch (Exception e) {
            System.debug('Got Exception which was expected.');
        }          
      
        Test.stopTest();			
    } 

    @isTest static void testBadRequest() {
        Test.startTest();
        try {
            DeleteWrapper w = new DeleteWrapper();
            w.execute(null);
        } catch (Exception e) {
            System.debug('Got Exception which was expected.');
        }          
      
        Test.stopTest();			
    } 
}