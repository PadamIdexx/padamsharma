public with sharing class LabOrdersControllerExtension {
    
    public List<Map<String,String>> resultList1{get;set;}
    public List<Map<String,String>> resultList2{get;set;}
    public static transient List<LabOrders.labOrder> result{get;set;}
    public List<String> limsIds{get;set;}
    public static transient List<String> ImageRes{get;set;}
    public static transient Map<String,String> guids{get;set;}
    public static String image{get;set;}
    public static String gId{get;set;}
    public static String errorMsg{get;set;}
    private boolean hasLynxx{get;set;}
    private boolean hasAntrim{get;set;}
    public List<String> lynxx = new List<String>();
    public Integer offset { get; set; }
 
     public static Date endDate{
        get{
            if(endDate == null){
                endDate = Date.today().addDays(-5);
            }
            return endDate;}
        set;
    }
    public static Date startDate{
        get{
            if(startDate == null){
                startDate = Date.today();
                startDate.format();
            }
            return startDate;}
        set;
    }
    
    public static transient Map<String, String> ordersMap{get;set;}
    public List<LabOrders.labOrder> orders{get;set;}
    
    public Account acc{get; set;}
    
    private final Account acct;
    
    public AsyncLabOrders.getLabOrderCollectionsResponse_elementFuture labOrdersFuture{get;set;}
    
    public LabOrdersControllerExtension(ApexPages.StandardController stdController){
        this.acct = (Account)stdController.getRecord();
        acc = acct;
        
    }
    
    public static LabOrders.labReportSearchRequest_element reqEle = new LabOrders.labReportSearchRequest_element();
    
    public boolean validate(){
        boolean valid = true;
        
        Integer dateTest = endDate.daysBetween(startDate);
        if(dateTest > 45 || dateTest < 0){
            valid = false;
            errorMsg = 'you must select a date range of 45 days or less';
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, +'you must select a date range of 45 days or less'));
            return valid;
        }
        List<Lims__c> lims = new List<Lims__c>();
      
        try{
            lims.addAll([SELECT Lims_Practice_ID__c, Account__c FROM Lims__c WHERE Account__c =: acc.id Limit : 2]);
            
            
      
            if(lims.size() != 0){
                for(Lims__c l : lims){
                    limsIds.add(l.Lims_Practice_ID__c);
                    system.debug('lims ids ' + limsIds);
                }
            }
            else{
                errorMsg = 'No records to display: No associated LIMS ID for this Account.';
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, +'No records to display: No associated LIMS ID for this Account.'));
                valid = false;
                return valid;
            }
            
        }
        
        catch(DmlException ex){
            errorMsg = ex.getMessage();
            System.debug('Error fetching lims ' + ex.getCause());
            ApexPages.addMessages(ex);
            valid = false;
            return valid;
        }
        
        return valid;
        
    }
    
    public Continuation startRequest(){
        orders = new List<LabOrders.labOrder>();        
        orders.clear();
        errorMsg = null;
        limsIds = new List<String>();
        DateTime st = startDate.addDays(1);
        DateTime et = endDate;
       
        if(!validate()){
            return null;
            
        }
        else{
            
            Integer Timeout = 90;
            Continuation cont = new Continuation(Timeout);
            cont.ContinuationMethod = 'processResponse';
            
            
            reqEle.customerCode = limsIds;
            reqEle.minimumOrderDate = et;
            reqEle.maximumOrderdate = st;
      
                  
            String[] contexts = new List<String>();
            contexts.add('1');
            contexts.add('16');
            reqEle.internalContextID = contexts;
            reqEle.maxRows = 150;
           
            try{
                AsyncLabOrders.AsyncLabOrderServiceSoapPort 
                    LabOrdersService =
                    new AsyncLabOrders.AsyncLabOrderServiceSoapPort();
                labOrdersFuture = new AsyncLabOrders.getLabOrderCollectionsResponse_elementFuture();
                  LabOrdersFuture = LabOrdersService.beginGetLabOrderCollections(cont, reqEle);
                
            }
            catch(exception e){
                errorMsg = 'Error fetching records: ' + e;
                return null;
            }
            return cont;
            
        }
        
    }

    public PageReference processResponse(){
        hasLynxx = false;
        hasAntrim = false;
        transient LabOrders.labOrderReport_element ele = labOrdersFuture.getValue();
        transient LabOrders.labOrders_element e = ele.labOrders;

        orders = new List<LabOrders.labOrder>(e.labOrder);
        resultList1 = new List<Map<String, String>>();
        resultList2 = new List<Map<String, String>>();
        
        if(orders.size() != 0){
            
            for(LabOrders.labOrder order: orders){
                
                ordersMap = new Map<String, String>();
                

                transient LabOrders.practice practice = order.practice;
                transient labOrders.patient patient = order.patient;
                transient labOrders.orderStatus status = order.orderStatus!=null?order.orderStatus:new labOrders.orderStatus();
                transient labOrders.species species = patient.species;
                transient labOrders.patientOwner owner = patient.ownerName;
                transient labOrders.lims lims = order.lims;
                transient labOrders.labOrderSource source = order.labOrderSource;
                transient labOrders.collections_element collections = order.collections;
                transient labOrders.contacts_element practEle = practice.contacts;           
                transient List<LabOrders.practiceContact> doctor = practEle.contact;
                transient LabOrders.collections_element resEle = new LabOrders.collections_element();
                transient List<LabOrders.results_element> resultEle = new List<LabOrders.results_element>();
                transient List<LabOrders.collection> res = new List<LabOrders.collection>();
                transient labOrders.specimen spec = order.specimen;
                DateTime dt = order.dateReceived;       
                Date thisDate = date.newInstance(dt.year(), dt.month(), dt.day());
                
                transient List<LabOrders.collection> collList = new List<LabOrders.collection>(collections.collection);
         
                if(status.code != null){
                    ordersMap.put('status', status.code);
                }else{
                    ordersMap.put('status', '');
                }
                ordersMap.put('species', species.name);
                String doc = '';
                for(LabOrders.practiceContact p : doctor){
                    doc+= p.surname + ' ';
                }
                ordersMap.put('doctor', doc);
                if(patient.name!=null){
                    ordersMap.put('name', patient.name);
                }else{
                    ordersMap.put('name', '');
                }
                ordersMap.put('remarks', order.remarks);
                ordersMap.put('reqText', order.requisitionNoteText);
                ordersMap.put('requisition', order.requisitionID);
                ordersMap.put('accession', order.accessionID);

                ordersMap.put('order date', String.valueOf(thisDate));

                if(owner != null){
                    ordersMap.put('owner', owner.lastName);
                }
                else{
                    ordersMap.put('owner', '');
                }
                ordersMap.put('lims', lims.fullname);
                ordersMap.put('limsActive', lims.activeStatusIndicator);                
                ordersMap.put('source', source.name);
                
                
                if(lims.fullname == 'LYNXX'){
                    if(order.accessionID != null){
                        lynxx.add(order.accessionID);
                    }
                    else{
                        lynxx.add(order.requisitionID);
                    }
                    hasLynxx = true;
                }

                if(lims.fullname == 'Antrim'){
                 //system.debug('antrim debug'+antrim);
                    System.debug(Logginglevel.INFO, 'Antrim ' + lims.fullname);
                    hasAntrim = true;
                }
                ordersMap.put('guid', '');
                
                String tests = '';
                String assays = '';
                String code = '';
                
                for(LabOrders.collection c : collList){
                    tests+= c.name + ' ';
                    resultEle.add(c.results);
                    System.debug('res ' + c.results);
                    if(c.collections != null && !test.isRunningTest()){
                        resEle = c.collections;
                        res.addAll(resEle.collection);
                        system.debug('Response'+res);
                        
                        for(LabOrders.collection r : res){
                            assays+= ' '+r.name + ' |';
                            code+= r.code + ' ';
                            
                            System.debug('col ' + r.code + ' ' + r.name);
                            system.debug('Response'+res);
                        }
                        
                        
                    }
                }
                ordersMap.put('assay', assays);
                ordersMap.put('code',code);
                ordersMap.put('tests', tests);
                system.debug('lims before loop ' + limsIds.get(0));
                if(practice.code == limsIds.get(0)){
                    
                    resultList1.add(ordersMap);
                    system.debug('result list 1'+resultList1);
                }   
                else{
                    resultList2.add(ordersMap);
              system.debug('result list 2'+resultList2);  }
                
            }
            system.debug('********COMPLETE LYNXX Description********'+lynxx);
            return null;
        }
        
        else{
            errorMsg = 'No Lab Orders Available for this Date Range';
            return null;
            
        }
        
    }
    public PageReference fetchGuids(){
        guids = new Map<String,String>();
        DateTime st = startDate.addDays(1);
        DateTime et = endDate;
        
        
        String one = st.format('yyyy-MM-dd\'T\'hh:mm:ss');
        String two = et.format('yyyy-MM-dd\'T\'hh:mm:ss');
        
        LabImagesCallout imgService = new LabImagesCallout();
         system.debug('***Lynx count***'+lynxx.size());
      system.debug('***Antrim count***'+limsIds.size());
        if(orders.size()!=0) 
       {
            
           if(hasLynxx){
                try{
                system.debug('******LYNXX/ACC value in labextension******'+lynxx);
                 //system.debug('******List LYNXX/ACC value in labextension******'+listlynxx);
                    guids.putAll(imgService.fetchLynx(lynxx));
                    system.debug('****Guids Value***'+guids);
                    
                    
                }
                catch(exception e){
                    System.debug(Logginglevel.DEBUG, 'Error fetching Lynxx Images' + e.getCause());
                    errorMsg = 'Error fetching Lynxx Images' + e.getCause();
                    if(!test.isRunningTest()){
                        return null;
                    }
                    
                }
            } 
            
            if(hasAntrim){
                try{
                    guids.putAll(imgService.fetchGuids(limsIds, one, two));
                    
                }
                catch(exception e){
                    System.debug(Logginglevel.DEBUG, 'Error fetching Antrim Images' + e.getCause());
                    errorMsg = 'Error fetching Antrim Images' + e.getCause();
                    if(!test.isRunningTest()){
                        return null;
                    }
                }
            }
            
            boolean match = false;
            system.debug('***Result list 2 ****'+resultList2);
            for(Map<String,String> i : resultList2){
                List <String> gKeys = new List<String>(guids.keySet());
                system.debug('***KEY SET resulst list 2***'+guids.keySet());
                for(String s : gKeys){
                    SYSTEM.debug('req ' + i.get('requisition') + 'guid ' + s);
                    String gList = '';
                    if(i.get('requisition') == s){
                        gList+=guids.get(s);
                        match = true;
                        i.put('guid', gList);
                    }
                    
                }
            }
            for(Map<String,String> i : resultList1){
            
            system.debug('***Result list 1 ****'+resultList1);
                List <String> gKeys = new List<String>(guids.keySet());
                system.debug('***KEY SET result list1***'+guids.keySet());
                for(String s : gKeys){
                    SYSTEM.debug('req ' + i.get('requisition') + 'guid ' + s);
                    String gList = '';
                    if(i.get('requisition') == s || i.get('accession') ==s){
                        gList+=guids.get(s);
                        match = true;
                        i.put('guid', gList);
                    }
                    
                }
            }
            if(!match)errorMsg = 'No images available for these records';
            return null;
        }
       
        else{    
            return null;
        } 
    }

    
    
}