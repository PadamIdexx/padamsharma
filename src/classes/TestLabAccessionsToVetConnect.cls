@isTest
public class TestLabAccessionsToVetConnect {

    static testMethod void LabAccessionsTest(){
        Account acc1=new Account(Name='TestLabAcesssionsVetConnect',SAP_Customer_Number__c ='12345');
        String vetJson = '{"sapId" : "12345", "active" : true, "activeUserCount" : 6, "relatedSapIds" : []}';
        
        Test.startTest();
          insert acc1;
        Test.stopTest();
        
        //system.assertEquals(acc1.id,acc1.id);
        
        pageRefMethod(acc1);
        
    }
    
    
    public static void pageRefMethod(Account acc){
        ApexPages.StandardController sc= new ApexPages.StandardController(acc);
        LabAccessionsToVetConnect lavplus = new LabAccessionsToVetConnect(sc);
        lavplus.onLoad();
    }
}