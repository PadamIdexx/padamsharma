global class ScheduleContractBatchJob implements Schedulable {
    global void execute(SchedulableContext sc) {
        ContractBatchJob con = new  ContractBatchJob();
        Database.executeBatch(con);
    }
}