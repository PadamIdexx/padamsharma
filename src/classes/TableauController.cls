/**
*      @author         Anudeep Gopagoni
*      @date           November 23 2015
*      @description    Controller class for Tableau Page. 
Supports all the below listed functionalities
1. When Tableau dashboards are launched from Account detail page, this class captures the AccountId from the URL
2. This class will query for all the SAP Number from the specific account record that the button is launched from
3.  
*      Modification Log: 
*      ---------------------------------------------------------------------
*      Developer                  Date                Description
*      ---------------------------------------------------------------------
*      Anudeep Gopagoni              Nov 1 2015           Initial Version
*/
public with sharing class TableauController {
    
    public Account acct {get;set;}
        //Initiate the controller class
    public TableauController(ApexPages.StandardController controller) {
        //Get the account Id from the URL that is generated on click of 'Launch Tableau' button.
        String acctId = System.currentPageReference().getParameters().get('acctid');
        //Query for the formula field which will contain only the SAP number but not the leading zeros
        if(acctId!=null) {

        acct = [SELECT SAP_Number_without_Leading_Zeros__c, SAP_Customer_Number__c FROM Account WHERE Id =:acctId];
        } else {
        
        PageReference ReturnPage = new PageReference('https://tableauq.idexx.com/t/americas/views/VDCCOMPASSAdmin/CustomerProfile?:embed=y&:tabs=n&:showVizHome=y&:toolbar=top');
        }
     
    }

}