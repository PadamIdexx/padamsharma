@isTest
public class Test_AsyncLabOrders_Mock implements WebServiceMock {

    public void doInvoke(
        Object stub, 
        Object request, 
        Map<String, Object> response,
        String endpoint, 
        String soapAction, 
        String requestName,
        String responseNS, 
        String responseName, 
        String responseType) {

            AsyncLaborders.getLabOrderCollectionsResponse_elementFuture respElement =
                new AsyncLabOrders.getLabOrderCollectionsResponse_elementFuture();
            
            response.put('response_x', respElement);
        }
}