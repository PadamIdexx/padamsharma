public class InlineWaterSalesHistoryQuantity{ 
    
    private  ApexPages.StandardController controller {get;set;}
    public static list<Customer_Sales_History_Water__c> CagKits {get;set;}
    public static list<Customer_Sales_History_Water__c> CagKitsTotal {get;set;}
    public static list<Customer_Sales_History_Water__c> CagConsumables {get;set;}
    public static list<Customer_Sales_History_Water__c> CagConsumablesTotal {get;set;}
    public static list<Customer_Sales_History_Water__c> CagLabServices {get;set;}
    public static list<Customer_Sales_History_Water__c> CagLabServicesTotal {get;set;}
    public static list<Customer_Sales_History_Water__c> CagTotal {get;set;}
    public static list<Customer_Sales_History_Water__c> CAGTelemed {get;set;}
    public static list<Customer_Sales_History_Water__c> CagTelemedTotal {get;set;}
    public static list<Customer_Sales_History_Water__c> CAGSerologyImmunology {get;set;}  
    public list<Customer_Sales_History_Water__c> getquantityrecords {get;set;}
    public static Boolean hasData {get;set;}
    public static List<String> noRecordsFound {get;set;}
    public string accountId {get;set;}
    static Account currentAccountRecord;
    
    public List<AggregateResult> producltList {get;set;}
    
    // column headers
    public String QUARTER_CURRENT    {get;set;}
    public String QUARTER_1_AGO  {get;set;}
    public String QUARTER_2_AGO    {get;set;}
    public String QUARTER_3_AGO    {get;set;}
    public String QUARTER_4_AGO    {get;set;}
    public String QUARTER_5_AGO    {get;set;}
    public String QUARTER_6_AGO    {get;set;}
    public String QUARTER_7_AGO    {get;set;}
    public String QUARTER_8_AGO    {get;set;}
    public String QUARTER_9_AGO    {get;set;}
    public String QUARTER_10_AGO    {get;set;}
    public String QUARTER_11_AGO    {get;set;}
    public String QUARTER_12_AGO    {get;set;}
    public String QUARTER_13_AGO    {get;set;}
    public String QUARTER_14_AGO    {get;set;}
    public String QUARTER_15_AGO    {get;set;}
    public String QUARTER_16_AGO    {get;set;}
    public String QUARTER_17_AGO    {get;set;}
    public String QUARTER_18_AGO    {get;set;}
    public String QUARTER_19_AGO    {get;set;}
    public String QUARTER_20_AGO    {get;set;}
    public String QUARTER_21_AGO    {get;set;}
    public String QUARTER_22_AGO    {get;set;}
    public String QUARTER_23_AGO    {get;set;}
    public integer year {get;set;}
    //constuctor 
    public InlineWaterSalesHistoryQuantity(ApexPages.StandardController controller){
        
        //accountId = ApexPages.currentPage().getParameters().get('what_id');
		
        currentAccountRecord = (Account)controller.getRecord();
        system.debug('currentAccountRecord::' +currentAccountRecord);
        if(currentAccountRecord<>null && currentAccountRecord.Id<>null)
            currentAccountRecord = [Select Id,SAP_CUSTOMER_NUMBER__c from Account where Id=:currentAccountRecord.Id];
           system.debug('**********currentAccountRecord:' + currentAccountRecord) ;
           accountId=currentAccountRecord.id;
        system.debug('accountId:'+accountId);
        this.controller = controller;  
        GroupProducts2();     
        getproductdata();
        setHeaderValues(); 
        getquantityvalues();
    }
    public void getquantityvalues (){
        getquantityrecords = [select Quantity_QTD__c, PRODUCT_FAMILY__c , Quantity_QTD_YOY__c,
                        Quantity_4_Quarter_Ago__c,Quantity_Q1_Diff_1_Year_Ago__c,
                        Quantity_8_Quarter_Ago__c,Quantity_Q1_Diff_2_Year_Ago__c,
                        Quantity_3_Quarter_Ago__c,Quantity_Q2_Diff_1_Year_Ago__c,
                        Quantity_7_Quarter_Ago__c,Quantity_Q2_Diff_2_Year_Ago__c, 
                        Quantity_2_Quarter_Ago__c,Quantity_Q3_Diff_1_Year_Ago__c,
                        Quantity_6_Quarter_Ago__c, Quantity_Q3_Diff_2_Year_Ago__c,
                        Quantity_1_Quarter_Ago__c,Quantity_5_Quarter_Ago__c,
                        Quantity_Q4_Diff_1_Year_Ago__c, Quantity_9_Quarter_Ago__c,
                        Quantity_10_Quarter_Ago__c,Quantity_11_Quarter_Ago__c,
                        Quantity_Q4_Diff_2_Year_Ago__c,Quantity_YTD__c,
                        Quantity_YTD_1_Year_Ago__c,Quantity_Year_Diff_1_Year_Ago__c,
                        Quantity_YTD_2_Year_Ago__c,Quantity_Year_Diff_2_Year_Ago__c,
                        Quantity_1_Year_Ago__c,Quantity_2_Year_Ago__c
                        from Customer_Sales_History_Water__c 
                        where Account__c = :accountId
                        and PRODUCT_FAMILY__c Not IN ('All Other','Total All Other')
                       order by  Sort_Order__c  asc ];
        system.debug('getquantityrecords:' +getquantityrecords.size());
    }
    
    public void setHeadervalues(){     
        
        // initialise all variables
        QUARTER_CURRENT    = '';
        QUARTER_1_AGO  = '';
        QUARTER_2_AGO    = '';
        QUARTER_3_AGO    = '';
        QUARTER_4_AGO    = '';
        QUARTER_5_AGO    = '';
        QUARTER_6_AGO    = '';
        QUARTER_7_AGO    = '';
        QUARTER_8_AGO    = '';
        QUARTER_9_AGO    = '';
        QUARTER_10_AGO    = '';
        QUARTER_11_AGO    = '';
        QUARTER_12_AGO    = '';
        QUARTER_13_AGO    = '';
        QUARTER_14_AGO    = '';
        QUARTER_15_AGO    = '';
        QUARTER_16_AGO    = '';
        QUARTER_17_AGO    = '';
        QUARTER_18_AGO    = '';
        QUARTER_19_AGO    = '';
        QUARTER_20_AGO    = '';
        QUARTER_21_AGO    = '';
        QUARTER_22_AGO    = '';
        QUARTER_23_AGO    = '';        
        Date d = System.today();
        Integer month = d.month();
        year = d.year();
        QUARTER_CURRENT = quarter(0,month,year);        
        QUARTER_1_AGO = quarter(1,month,year);        
        QUARTER_2_AGO = quarter(2,month,year);        
        QUARTER_3_AGO = quarter(3,month,year);        
        QUARTER_4_AGO = quarter(4,month,year);        
        QUARTER_5_AGO = quarter(5,month,year);        
        QUARTER_6_AGO = quarter(6,month,year);        
        QUARTER_7_AGO = quarter(7,month,year);        
        QUARTER_8_AGO = quarter(8,month,year);        
        QUARTER_9_AGO = quarter(9,month,year);        
        QUARTER_10_AGO = quarter(10,month,year);        
        QUARTER_11_AGO = quarter(11,month,year);
        
    }  
    
    public String quarter(Integer quarter, Integer month, Integer year)
    {
        Integer quarterNumber;        
        if(month == 1 ||month == 2|| month == 3)
        {
            quarterNumber = 1;  
        }
        else if(month == 4 ||month == 5 || month == 6)
        {
            quarterNumber = 2;  
        }
        else if(month == 7 ||month == 8 || month == 9)
        {
            quarterNumber = 3;  
        }
        else if(month == 10 ||month == 11 || month == 12)
        {
            quarterNumber = 4;  
        }
        Integer p = math.mod(quarter,4);
        if(quarterNumber<= p)
        {
            --year;
        } 
        quarterNumber = math.mod((quarterNumber-p+4),4);
        if(quarterNumber ==0)
        {
            quarterNumber =4;
        }
        return 'Q'+quarterNumber+' '+(year-(quarter/4)) ;
    }
    // @RemoteAction 
    public void GroupProducts2(){
        CagKits = new List<Customer_Sales_History_Water__c>();
        CagConsumables = new List<Customer_Sales_History_Water__c>();
        CagKitsTotal = new List<Customer_Sales_History_Water__c>();
        CagConsumablesTotal = new List<Customer_Sales_History_Water__c>();
        CagLabServices = new List<Customer_Sales_History_Water__c>();
        CagLabServicesTotal = new List<Customer_Sales_History_Water__c>();
        CagTotal = new List<Customer_Sales_History_Water__c>();
        CAGTelemed = new List<Customer_Sales_History_Water__c>();
        CagTelemedTotal = new List<Customer_Sales_History_Water__c>();
        CAGSerologyImmunology = new List<Customer_Sales_History_Water__c>();        
        hasData = false;
        noRecordsFound = new List<String>();
        
        
        //  1 - Kits : fetch all sales history records for Kits 
        for(Customer_Sales_History_Water__c csh : ([select   Id, PRODUCT_FAMILY__c,Account__c,
                                                    Quantity_QTD__c,Quantity_QTD_YOY__c,Quantity_4_Quarter_Ago__c,
                                                    Quantity_Q1_Diff_1_Year_Ago__c,Quantity_8_Quarter_Ago__c,
                                                    Quantity_Q1_Diff_2_Year_Ago__c,Quantity_3_Quarter_Ago__c,
                                                    Quantity_Q2_Diff_1_Year_Ago__c,Quantity_7_Quarter_Ago__c,
                                                    Quantity_Q2_Diff_2_Year_Ago__c, Quantity_2_Quarter_Ago__c,
                                                    Quantity_Q3_Diff_1_Year_Ago__c,Quantity_6_Quarter_Ago__c,
                                                    Quantity_Q3_Diff_2_Year_Ago__c,Quantity_1_Quarter_Ago__c,
                                                    Quantity_5_Quarter_Ago__c,Quantity_Q4_Diff_1_Year_Ago__c,
                                                    Quantity_9_Quarter_Ago__c,Quantity_Q4_Diff_2_Year_Ago__c,
                                                    Quantity_YTD__c,Quantity_YTD_1_Year_Ago__c,
                                                    Quantity_Year_Diff_1_Year_Ago__c,Quantity_YTD_2_Year_Ago__c,
                                                    Quantity_Year_Diff_2_Year_Ago__c,Quantity_1_Year_Ago__c                                                    
                                                    from Customer_Sales_History_Water__c 
                                                    where Account__c = :accountId  ]))
        {
            CagKits.add(csh);  
            hasData = true;
            system.debug('accountId'+ accountId);
        }       
        
        
        if(!hasData){
            noRecordsFound = new List<String>();
        }
        
    }
    
    public list<AggregateResult> getproductdata(){
        
        producltList = [select sum(Quantity_QTD__c) qntqtd , PRODUCT_FAMILY__c product, sum(Quantity_QTD_YOY__c) qtdyoy,
                        sum(Quantity_4_Quarter_Ago__c) qnty4qtrago,sum(Quantity_Q1_Diff_1_Year_Ago__c) qtddiffQ1,
                        sum(Quantity_8_Quarter_Ago__c) qnty8qtrago,sum(Quantity_Q1_Diff_2_Year_Ago__c) qtddiffq8,
                        sum(Quantity_3_Quarter_Ago__c) qnty3qtrsago,sum(Quantity_Q2_Diff_1_Year_Ago__c) qntyq2diff1yearago,
                        sum(Quantity_7_Quarter_Ago__c) qnty7qtrsago,sum(Quantity_Q2_Diff_2_Year_Ago__c) qntyq2diff2yearago, 
                        sum(Quantity_2_Quarter_Ago__c) qnty2qtrsago,sum(Quantity_Q3_Diff_1_Year_Ago__c) qntyq3diff1yearago,
                        sum(Quantity_6_Quarter_Ago__c) qnty6qtrsago, sum(Quantity_Q3_Diff_2_Year_Ago__c) qntyq3diff2yearago,
                        sum(Quantity_1_Quarter_Ago__c) qnty1qtrsago,sum(Quantity_5_Quarter_Ago__c) qnty5qtrsago,
                        sum(Quantity_Q4_Diff_1_Year_Ago__c) qntyq4diff1yearago, sum(Quantity_9_Quarter_Ago__c) qnty9qtrsago,
                        sum(Quantity_10_Quarter_Ago__c) qnty10qtrsago,sum(Quantity_11_Quarter_Ago__c) qnty11qtrsago,
                        sum(Quantity_Q4_Diff_2_Year_Ago__c) qntyq4diff2yearago,sum(Quantity_YTD__c) qntyytd,
                        sum(Quantity_YTD_1_Year_Ago__c) qntyytd1yrago,sum(Quantity_Year_Diff_1_Year_Ago__c) qntyyrdiff1yrago,
                        sum(Quantity_YTD_2_Year_Ago__c) qntytyd2yrago,sum(Quantity_Year_Diff_2_Year_Ago__c) qntyyrdiff2yrago,
                        sum(Quantity_1_Year_Ago__c) qnty1yrago,sum(Quantity_2_Year_Ago__c) qnty2yrago
                        from Customer_Sales_History_Water__c 
                        where Account__c = :accountId
                        and PRODUCT_FAMILY__c !='All Other'
                        group by PRODUCT_FAMILY__c
                        order by PRODUCT_FAMILY__c];
        system.debug('currentAccountRecord.id::'+accountId);
        system.debug('producltList::'+producltList);
        return producltList;
    }
}