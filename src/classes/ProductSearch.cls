/**
 * @author Heather Kinney
 * @description CMS Product Search implementation.
 *
 *  Modification Log:
 * -----------------------------------------------------------------------------------------------------------------------------
 *  Developer                       Date               Description
 * -----------------------------------------------------------------------------------------------------------------------------
 *  Heather Kinney                  08/08/2016         Initial creation.
 *
 */
public class ProductSearch {
    
    protected ApexPages.StandardController controller {get;set;}

    public static final String LABEL_SOQL_LIMIT_PRODUCTS       = Label.OrderProductSearchLimit;
    
    public static final String FLAG_IS_ACTIVE                  = 'isActive=true';
    public static final String FLAG_INCLUDE_IN_SEARCH          = 'Include_In_Search__c = true';
    
    public static final String OBJECT_PRODUCT                  = 'Product2';
    public static final String OBJECT_PRICE_BOOK_ENTRY         = 'PriceBookEntry';
    
    public static final String FIELD_LABEL_PRODUCT_DESCRIPTION = 'Product_Description__c';
    public static final String FIELD_VALUE_ID                  = 'Id';
    public static final String FIELD_PRODUCT_CODE              = 'ProductCode';
    public static final String FIELD_NAME                      = 'Name';
    public static final String FIELD_UNITS_OF_MEASURE          = 'CMS_Unit_of_Measure__c';
    public static final String FIELD_LONG_PRODUCT_DESCRIPTION  = 'CMS_Product_Description_Long__c';

    public static final String WHERE_PRODUCT2_INCLUDE_IN_SEARCH_TRUE = ' AND ' + OBJECT_PRODUCT +'.' + FLAG_INCLUDE_IN_SEARCH;
    
    public ProductSearch(ApexPages.StandardController controller) {
        this.controller = controller;
    }
    
    public ProductSearch() {}
    
   	/** 
   	 * This is the initial Product Search - a SOSL search against Product2. 
   	 * SOSL is employed because we want to search all the Product2 fields.
   	 * If we get back results, we will populate a List containing Product2 objects that were found.
   	 */
    @RemoteAction
    public static List<ProductSearchResultsUIData> getCMSSearchResults(String searchString, String priceBookName, String currencyIsoCode) {
        List<ProductSearchResultsUIData> productList = null;
        //Two-pronged Search approach.
        //1. First, a SOSL search against Product2 where the isActive and Include in Search flags are both TRUE:
        List<List<sObject>> searchObjects = [FIND :searchString + '*' IN ALL FIELDS 
                                             RETURNING Product2 (Id,Name,CMS_Product_Keywords__c,ProductCode 
                                             WHERE isActive=true AND Include_In_Search__c = true)];
                                                                
		productList = processResults(searchObjects, priceBookName, currencyIsoCode);
        
        return productList;
    }
    
 
	/*
	 * Search functinon specific to LPD NA.  They would like to see all products at this time (i.e. no LOB specific filtering).
	 */ 
    @RemoteAction
    public static List<ProductSearchResultsUIData> getCMSSearchResultsLPDNA(String searchString, String priceBookName, String currencyIsoCode) {
        List<ProductSearchResultsUIData> productList = null;
        //Two-pronged Search approach.
        //1. First, a SOSL search against Product2 where the isActive and Include in Search flags are both TRUE:
        List<List<sObject>> searchObjects = [FIND :searchString + '*' IN ALL FIELDS 
                                             RETURNING Product2 (Id,Name,CMS_Product_Keywords__c,ProductCode 
                                             WHERE isActive=true AND Include_In_Search__c = true)];
                                                                
        productList = processResults(searchObjects, priceBookName, currencyIsoCode);

        return productList;
    }    
    

   	/*
	 * Search functinon specific to LPD EMEA.  They would like to see all LOB specific search results.
	 */ 
    @RemoteAction
    public static List<ProductSearchResultsUIData> getCMSSearchResultsLPDEMEA(String searchString, String priceBookName, String currencyIsoCode) {
        List<ProductSearchResultsUIData> productList = null;
        //Two-pronged Search approach.
        //1. First, a SOSL search against Product2 where the isActive and Include in Search flags are both TRUE:
        List<List<sObject>> searchObjects = [FIND :searchString + '*' IN ALL FIELDS 
                                             RETURNING Product2 (Id,Name,CMS_Product_Keywords__c,ProductCode 
                                             WHERE isActive=true AND Include_In_Search__c = true 
                                             and material_division_group__c in ('DAIRY', 'PAS', 'WATER'))];
                                                                
        productList = processResults(searchObjects, priceBookName, currencyIsoCode);

        return productList;
    }    

	/*
	 * Search functinon specific to CAG.  They would like to see all products at this time (i.e. no LOB specific filtering).
	 */ 
    @RemoteAction
    public static List<ProductSearchResultsUIData> getCMSSearchResultsCAG(String searchString, String priceBookName, String currencyIsoCode) {
        List<ProductSearchResultsUIData> productList = null;
        //Two-pronged Search approach.
        //1. First, a SOSL search against Product2 where the isActive and Include in Search flags are both TRUE:
        List<List<sObject>> searchObjects = [FIND :searchString + '*' IN ALL FIELDS 
                                             RETURNING Product2 (Id,Name,CMS_Product_Keywords__c,ProductCode 
                                             WHERE isActive=true AND Include_In_Search__c = true 
                                             and material_division_group__c in ('IHD', 'LABS', 'ICS'))];
                                                                
        productList = processResults(searchObjects, priceBookName, currencyIsoCode);
        
        return productList;
    }    
    
    private static List<ProductSearchResultsUIData> processResults(List<List<sObject>> searchObjects, String priceBookName, String currencyIsoCode) {
        List<ProductSearchResultsUIData> productList = null;

        List<Product2> productResults = (List<Product2>)searchObjects[0];
        if (productResults != null) {
            Set<Id> product2Ids = new Set<Id>();
        	for (Product2 p2 : productResults ) {
            	product2Ids.add(p2.Id);
        	}
            String prdIdToBeSearched = getProductIdsToBeSearched(product2Ids);
            List<PriceBookEntry> pbeList = new List<PriceBookEntry>(); 
            //2. Secondly, if we have results, perform the Secondary Search - combining Product2 and PriceBookEntry:
            if (String.isNotBlank(prdIdToBeSearched)) { 
                String soqlQuery = constructQuery(priceBookName, prdIdToBeSearched, currencyIsoCode);
            	pbeList = (List<PriceBookEntry>) Database.Query(soqlQuery);
            	productList = generateProductSearchResults(pbeList);
            }
        }
        return productList;        
    }
    
    public static String getProductIdsToBeSearched(Set<Id> productResults) {
        String stringOfProductIds = '' ;   
        System.debug('productResults = ' + productResults);
        for (Id product : productResults) {          
            stringOfProductIds += '\'' + product + '\',';            
        }
        stringOfProductIds = stringOfProductIds.lastIndexOf(',') > 0 ? '(' + stringOfProductIds.substring(0,stringOfProductIds.lastIndexOf(',')) + ')' : stringOfProductIds; 
        return stringOfProductIds;
    }
    
    public static String constructQuery(String priceBookName, String prdIdToBeSearched, String currencyIsoCode) {
        //12.12.2016 - HLK added Item_Category_Group_Code__c as this wasn't getting passed for FOC products via search
        String soqlQuery = 'SELECT ' + FIELD_VALUE_ID + ',' + FIELD_LABEL_PRODUCT_DESCRIPTION + ', Product2.ProductCode, Product2.Name, ' +
                    'Product2.Description, Product2.CMS_Product_Description_Long__c, Product2.CMS_Unit_of_Measure__c, Product2.Batch_Mgmt_Requirement__c, ' +
            ' Product2.Item_Category_Group_Code__c FROM ' + OBJECT_PRICE_BOOK_ENTRY;
        String paramCondition = ' WHERE Pricebook2Id =\'' + priceBookName +  '\' AND CurrencyIsoCode = \'' + currencyIsoCode + '\' ';
        			paramCondition += String.isNotBlank(prdIdToBeSearched) ? ' AND Product2.Id IN ' + prdIdToBeSearched : '';
        String orderBy = ' ORDER by Product2.sap_material_number__c DESC';  
        String soqlCondition = paramCondition + orderBy;
        String soqlLimit = ' LIMIT '+ +Label.OrderProductSearchLimit;
        soqlQuery += soqlCondition + soqlLimit;
        System.debug('soqlQuery = ' + soqlQuery);
        return soqlQuery;
    }
    
    public static List<ProductSearchResultsUIData> generateProductSearchResults(List<PriceBookEntry> pbeList) {
        System.debug('In generateProductSearchResults - pbeList is:' + pbeList);
        List<ProductSearchResultsUIData> productSearchResultList =  new List<ProductSearchResultsUIData>();
        for (PriceBookEntry pbe : pbeList) {
            System.debug('each pbe is:'+pbe);
            ProductSearchResultsUIData productSearchResult = new ProductSearchResultsUIData();
            productSearchResult.priceBookEntryId = pbe.Id;
            productSearchResult.priceBookEntryDescription = pbe.Product_Description__c;
            productSearchResult.productName = pbe.Product2.Name;
            productSearchResult.productCode = pbe.Product2.ProductCode;
            String sanitizedLongProductDescription = '';
            if (pbe.product2.CMS_Product_Description_Long__c != null) {
                //Long descriptions may contain markup. This manipulation returns a sanitized version of the description:
            	sanitizedLongProductDescription = pbe.product2.CMS_Product_Description_Long__c.replaceAll('\\<.*?\\>', '');
                sanitizedLongProductDescription = sanitizedLongProductDescription.unescapeHtml4();
            }
            productSearchResult.productDescription = sanitizedLongProductDescription;
            productSearchResult.productUnitsOfMeasure = pbe.product2.CMS_Unit_of_Measure__c;
            productSearchResult.isBatchManagementRequired = pbe.product2.Batch_Mgmt_Requirement__c;
            productSearchResult.itemCategoryGroupCode = pbe.Product2.Item_Category_Group_Code__c; //12.12.2016
           	System.debug('productSearchResult = ' + productSearchResult); //12.12.2016
            productSearchResultList.add(productSearchResult);  
        }
        return productSearchResultList;
    }
    
    public class ProductSearchResultsUIData {
    
		public String priceBookEntryId          	{get;set;}
        public String priceBookEntryDescription 	{get;set;}
        public String productId                 	{get;set;}
        public String productName               	{get;set;}
        public String productCode               	{get;set;}
        public String productDescription        	{get;set;}
        public String productUnitsOfMeasure     	{get;set;}
        public Boolean isSelected               	{get;set;}
        public Boolean isBatchManagementRequired	{get;set;}
        public String itemCategoryGroupCode			{get;set;} //12.12.2016
        
        public ProductSearchResultsUIData(){
            priceBookEntryId = priceBookEntryDescription = productId = productName = productCode = productDescription = productUnitsOfMeasure = '';
            isSelected = false;
            isBatchManagementRequired = false;
            itemCategoryGroupCode = ''; //12.12.2016
        }
   
    }
    
}