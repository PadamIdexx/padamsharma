public class SAPOrder {
    
    //The profile of the user is needed for certain scenarios of order management.
    public ProfileDisplayWrapper userProfile {get;set;}
    
    //Update map to carry which fields are actually be modified for the change operation
    //Map key is field name, map value is update flag (I/D/U)
    public Map<String, String> updateMap {get;set;}
    
    public String documentType {get;set;}
    public String documentNumber {get;set;}
    public String customerId {get;set;} 
    public String salesOrg {get;set;}
    public String distributionChannel {get;set;}
	public String division {get;set;}
    public String incoterms {get;set;}
    public String paymentTerms {get;set;}
    public String billingBlock {get;set;}
    public String deliveryBlock {get;set;}
    public String orderReason {get;set;}
    public String poNumber {get;set;}
    public String poType {get;set;}
    public String salesPerson {get;set;}
    public Date requestedDeliveryDate {get;set;}
    public Boolean completeDelivery {get;set;}
    public String language {get;set;}

    public Boolean freeGoodsOrder {get;set;}
    
    //not currently used in Salesforce but in the SAP interface
    public String referenceDocument {get;set;}
    public String referenceDocumentCategory {get;set;}
    
    public Boolean rewardItemsEligible {get;set;}
    public String couponCode {get;set;}
    public String externalProposalNumber {get;set;}
    public String externalDocumentNumber {get;set;}
    
    public Boolean useRewardPoints {get;set;}
    public Decimal pointsRedeemed {get;set;}
	public String pointsRedeemedCurrency {get;set;}
    public String pointsConditionStep {get;set;}		//required for change
    public String pointsConditionCount {get;set;}		//required for change

    public String discountType {get;set;}
    public Decimal discountValue {get;set;}
    public String discountCurrency {get;set;}
	public String discountConditionStep {get;set;}	//required for change
    public String discountconditionCount {get;set;}	//required for change
   
    public String freightType {get;set;}
    public Decimal freightValue {get;set;}
    public String freightCurrency {get;set;}
    public String freightConditionStep {get;set;} //need for change
    public String freightConditionCount {get;set;} //need for change    
    
    public String headerText {get;set;}
    public String headerTextLanguage {get;set;}
    
    public String CIG {get;set;}
    public String CUG {get;set;}
    public String CIGCUGLanguage {get;set;}

	//Various status flags
    public Boolean isDelivered {get;set;}
    public Boolean isPartiallyDelivered {get;set;}
    public Boolean hasErrors {get;set;}
    public Boolean hasWarnings {get;set;}
    public Boolean hasShortDatedBatches {get;set;}
    public Boolean hasCreditLimitBlock {get;set;}
  
    public Decimal totalTax {get;set;}
    public Decimal totalShipping {get;set;}
    public Decimal grandTotal {get;set;}
    
    public Integer maxItemNumber {get;set;}
    
    public List<Item> items {get;set;}
    public List<Partner> partners {get;set;}
    public List<CreditCard> creditcards {get;set;}
    public List<Return_x> returns {get;set;}
    
    public SAPOrder() {
        //initialize totals to prime calculations
    	totalTax = 0.00;
        totalShipping = 0.00;
        grandTotal = 0.00;
        maxItemNumber = 0;
    }
    
    public class Item {
        public Item() {
	        //initialize totals to prime calculations
			totalPrice = 0.00;
            netSubTotal = 0.00;
            netPrice = 0.00;
            shippingCharges = 0.00;
            tax = 0.00;
            discount = 0.00;
            unitPrice = 0.00;
            vat = 0.00;

            totalTax = 0.00;
            total = 0.00;
        }
        
        //map of fields being modified used with change
        //Update map to carry which fields are actually be modified for the change operation
    	//Map key is field name, map value is update flag (I/D/U)
        public Map<String, String> updateMap {get;set;}

        public String itemNumber {get;set;}
        public String externalItemNumber {get;set;}
        public String material {get;set;}
        public String speedCode {get;set;}
        public String shortText {get;set;}
        public Decimal netValue {get;set;}
        public String currencyCode {get;set;}
        public String uom {get;set;}
        public Date deliveryDate {get;set;}		//for simulate, this is the requested delivery date
        public Decimal requestedQuantity {get;set;}
        public Decimal confirmedQuantity {get;set;}
        public String plant {get;set;}
        public String shippingPoint {get;set;}
        public String itemCategory {get;set;}
        public String higherLevelItem {get;set;}
        public String storageLocation {get;set;}
        public String materialGroup {get;set;}
        public Boolean freeGoods {get;set;}
		public String freeGoodsAgent {get;set;}  
		public String freeGoodsCostCenter {get;set;}        
		public String freeGoodsReason{get;set;}        
        
        public String itemText {get;set;}
        
        public Decimal totalPrice {get;set;}  		//subtotal1
        public Decimal netSubTotal {get;set;}		//subtotal2
        public Decimal netPrice {get;set;}			//subtotal3
        public Decimal shippingCharges {get;set;}	//subtotal4
        public Decimal tax {get;set;}				//subtotal5
		public Decimal discount {get;set;}			//subtotal6
        public Decimal unitPrice {get;set;}		//subtotal1/requested quantity
        public Decimal vat {get;set;}				//value added tax
 
        public Boolean freightChanged {get;set;}
        public Decimal freightRate {get;set;}  //user entered shipping rate
        public String freightCurrency {get;set;}
        public String freightConditionStep {get;set;} //need for change
        public String freightConditionCount {get;set;} //need for change
        
        //line item rollup totals
        public Decimal totalTax {get;set;}
        public Decimal total {get;set;}
        
        public Decimal points {get;set;}
        public String batch {get;set;}
        public String incoterms {get;set;}
        public Boolean batchedItem {get;set;}
        public Date batchExpirationDate {get;set;}
        public Boolean shortDatedBatch {get;set;}
        public Boolean isDelivered {get;set;} 
        
    	public String discountType {get;set;}
    	public Decimal discountValue {get;set;}
    	public String discountCurrency {get;set;}
	    public String discountConditionStep {get;set;}	//needed for change
	    public String discountConditionCount {get;set;}	//needed for change        
      
        public Boolean designatedBatch {get;set;}
        public Boolean useCustomerIncoterms {get;set;}
    }
    
    public class Partner {
        //map of fields being modified used with change 
        //Update map to carry which fields are actually be modified for the change operation
    	//Map key is field name, map value is update flag (I/D/U)
	    public Map<String, String> updateMap {get;set;}
        
        public Boolean onetimeShippingChange {get;set;}
        public String partnerType {get;set;}
        public String customer {get;set;}
        public String name {get;set;}
        public String name2 {get;set;}
        public String street {get;set;}
        public String country {get;set;}
        public String postalCode {get;set;}
        public String city {get;set;}
        public String district {get;set;}
        public String region {get;set;}
        public String transportationZone {get;set;}
        
    }
    
    //only appears in responses
    public class CreditCard {
        public String creditCardType {get;set;}
        public String creditCardNumber {get;set;}
        public Date creditCardDate {get;set;}
        public String creditCardName {get;set;}
    }
    
   
    //Return item is used with all order processing responses
    //except read.
    //
    //only appears in responses
    public class Return_x {
        public String MessageType {get;set;}
        public String MessageID {get;set;}
        public String MessageNumber {get;set;}
        public String updateFlag {get;set;}
        public String Message {get;set;}
    }
    
}