@isTest
public class TestRefTestCodeLookupExtension {
    public static List<LIMS_Test__c> lims = new List<LIMS_Test__c>();
    public static List<Account> accts = new List<Account>();
    
    @testSetup 
    static void createTestData(){
        
        accts = ([SELECT Id, Name FROM Account WHERE Name LIKE '%TestAccount%' ]);
        
        lims =  new List<Lims_Test__c> ([select  LIMS__c, Id,Test_Code__c, Species__c, Product__c,  Product__r.SAP_MATERIAL_NUMBER__c, Test_Name__c, Turnaround_Time__c,
                                           Test_Type__c, Interferences__c, Storage_Stability__c, Submission_Requirements__c,
                                           Min_Specimen_Requirements__c, Acceptable_Specimens__c, Spec_Protocol__c, Comments__c,
                                           Internal_Comments__c, Interpretation_Comments__c, Test_Category__c, (select Component_Test_Code__c,
                                                                                                                Component_Test_Name__c from LIMS_Test_Components__r) from LIMS_Test__c 
                                           where Test_Code__c =:  '1201' and LIMS__c =:  'Antrim' Order by Test_Code__c]); 
        
    }
    
    @isTest
    static void testPageWithAccount(){
        Account myAct = new Account();    
        Test_Data_Utility.createTestLims(10, 1);
        Test_Data_Utility.createTestAccounts(10, 0);
        
        createTestData();
        
        myAct = accts.get(0);
        
        PageReference page = new PageReference('/apex/productTestCodeAndPricing?id='+myAct.id);
        test.setCurrentPage(page);    
        ApexPages.StandardController stdAct = new
            ApexPages.StandardController(myAct);
        RefLabTestCodeLookupControllerExtension testCont =
            new RefLabTestCodeLookupControllerExtension(stdAct);
        
        test.startTest();
        /*
        RefLabTestCodeLookupControllerExtension.getResults('Test Code', 'Antrim', '1201');
        RefLabTestCodeLookupControllerExtension.getResults('Component Code', 'Antrim', '1201');
        RefLabTestCodeLookupControllerExtension.getResults('Display Name', 'Antrim', '1201');
        */
        // Below code added by Rama part of DE8584
        testCont.LIMS ='123';
        testCont.limsId ='123';
        String str = testcont.getLIMS();
                
        RefLabTestCodeLookupControllerExtension.getResultDisplayName(1,'Display Name', 'Antrim', '1201');
        RefLabTestCodeLookupControllerExtension.do_pagination(1,'Display Name', 'Antrim', '1201');  

        RefLabTestCodeLookupControllerExtension.do_pagination(2,'Display Name', 'Antrim', '1201');  

        RefLabTestCodeLookupControllerExtension.getResultDisplayName(1,'Component Code', 'Antrim', '1201');
        RefLabTestCodeLookupControllerExtension.do_pagination(1,'Component Code', 'Antrim', '1201');  
        RefLabTestCodeLookupControllerExtension.do_pagination(2,'Component Code', 'Antrim', '1201');  

        RefLabTestCodeLookupControllerExtension.getResultDisplayName(1,'Test Code', 'Antrim', '1201');
        RefLabTestCodeLookupControllerExtension.do_pagination(1,'Test Code', 'Antrim', '1201');  
        RefLabTestCodeLookupControllerExtension.do_pagination(2,'Test Code', 'Antrim', '1201');  

        test.stopTest();  
        
        
    }
    
}