public with sharing class OrderSimulateResponse {
    //Web Service call out related constants.
    //
    // SOAP Action
    public final static String SOAPACTION = 'http://sap.com/xi/WebService/soap1.1';
    
    // XML Namespace for this object
    // Salesforce representation of Namespace
    public final static String NAMESPACE = 'http://idexx.com/sappo/sales/salesforce';
    public final static String SF_NAMESPACE = 'idexxComSappoSalesSalesforce';
    
    // XML Namespace of types    
    // Salesforce representation of Namespace   
    public final static String XMLTYPE_NAMESPACE = 'http://idexx.com/sappo/global/types';
	public final static String SF_XMLTYPE_NAMESPACE = 'idexxComSappoGlobalTypes';
     
	// Name of Class that carries the payload.  There is no generic way to get this at this 
	// from the obect at runtime at this time.    
    public final static String PAYLOAD_CLASSNAME = 'SalesOrderSimulateResponse';
    
    public class SalesOrderSimulateResponse {
        public String RewardItemsEligible;
        
        public Items_element[] Items;
		public Conditions_element[] Conditions;
        public Return_element[] Return_x;

		private String[] RewardItemsEligible_type_info = new String[]{'RewardItemsEligible',NAMESPACE,null,'0','1','false'};            
        private String[] Items_type_info = new String[]{'Items',NAMESPACE,null,'0','-1','false'};            
        private String[] Conditions_type_info = new String[]{'Conditions',NAMESPACE,null,'0','-1','false'};       
        private String[] Return_x_type_info = new String[]{'Return',NAMESPACE,null,'0','-1','false'};            

            
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'RewardItemEligible','Items','Conditions','Returns_x'};
            
    }

    public class Items_element {
        public String ItemNumber;
        public String ExternalItemNumber;
        public String Material;
        public String SpeedCode;
        public String ShortText;
        public String NetValue;
        public String Currency_x;
        public String UoM;
        public String DeliveryDate;
        public String RequestedQuantity;
		public String ConfirmedQuantity;        
        public String Plant;
        public String ItemCategory;
		public String HigherLevelItem;
        public String StorageLocation;
		public String MaterialGroup;
        public String Subtotal1;
        public String Subtotal2;
        public String Subtotal3;
        public String Subtotal4;
        public String Subtotal5;
        public String Subtotal6;                
        public String Batch;
        public String Incoterms;
		public String BatchedItem;
        public String BatchExpirationDate;
        public String ShortDatedBatch;
        public String MaterialSalesText;
        
                
        private String[] ItemNumber_type_info = new String[]{'ItemNumber',NAMESPACE,null,'1','1','false'};
        private String[] ExternalItemNumber_type_info = new String[]{'ExternalItemNumber',NAMESPACE,null,'0','1','false'};
        private String[] Material_type_info = new String[]{'Material',NAMESPACE,null,'0','1','false'};
        private String[] SpeedCode_type_info = new String[]{'SpeedCode',NAMESPACE,null,'0','1','false'};
        private String[] ShortText_type_info = new String[]{'ShortText',NAMESPACE,null,'0','1','false'};
        private String[] NetValue_type_info = new String[]{'NetValue',NAMESPACE,null,'0','1','false'};
        private String[] Currency_x_type_info = new String[]{'Currency',NAMESPACE,null,'0','1','false'};
        private String[] UoM_type_info = new String[]{'UoM',NAMESPACE,null,'0','1','false'};
        private String[] DeliveryDate_type_info = new String[]{'DeliveryDate',NAMESPACE,null,'0','1','false'};            
        private String[] RequestedQuantity_type_info = new String[]{'RequestedQuantity',NAMESPACE,null,'0','1','false'};            
		private String[] ConfirmedQuantity_type_info = new String[]{'ConfirmedQuantity',NAMESPACE,null,'0','1','false'};                        
		private String[] Plant_type_info = new String[]{'Plant',NAMESPACE,null,'0','1','false'};                                    
		private String[] ItemCategory_type_info = new String[]{'ItemCategory',NAMESPACE,null,'0','1','false'};                                                
		private String[] HigherLevelItem_type_info = new String[]{'HigherLevelItem',NAMESPACE,null,'0','1','false'};                                                            
		private String[] StorageLocation_type_info = new String[]{'StorageLocation',NAMESPACE,null,'0','1','false'};                                                                        
		private String[] MaterialGroup_type_info = new String[]{'MaterialGroup',NAMESPACE,null,'0','1','false'};                                                                        
        private String[] Subtotal1_type_info = new String[]{'Subtotal1',NAMESPACE,null,'0','1','false'};                                                                                    
		private String[] Subtotal2_type_info = new String[]{'Subtotal2',NAMESPACE,null,'0','1','false'};                                                                                    
		private String[] Subtotal3_type_info = new String[]{'Subtotal3',NAMESPACE,null,'0','1','false'};                                                                                    
		private String[] Subtotal4_type_info = new String[]{'Subtotal4',NAMESPACE,null,'0','1','false'};                                                                                    
		private String[] Subtotal5_type_info = new String[]{'Subtotal5',NAMESPACE,null,'0','1','false'};                                                                                    
		private String[] Subtotal6_type_info = new String[]{'Subtotal6',NAMESPACE,null,'0','1','false'};                                                                                                
		private String[] Batch_type_info = new String[]{'Batch',NAMESPACE,null,'0','1','false'};                                                                                                
		private String[] Incoterms_type_info = new String[]{'Incoterms',NAMESPACE,null,'0','1','false'};                                                                                                            
		private String[] BatchedItem_type_info = new String[]{'BatchedItem',NAMESPACE,null,'0','1','false'};                                                                                                            
		private String[] BatchExpirationDate_type_info = new String[]{'BatchExpirationDate',NAMESPACE,null,'0','1','false'};                                                                                                            
		private String[] ShortDatedBatch_type_info = new String[]{'ShortDatedBatch',NAMESPACE,null,'0','1','false'};                                                                                                                        
        private String[] MaterialSalesText_type_info = new String[]{'MaterialSalesText',NAMESPACE,null,'0','-1','false'};                                                                                                                        
            
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'ItemNumber','ExternalItemNumber','Material','SpeedCode','ShortText','NetValue','Currency_x',
                                                                'UoM','DeliveryDate','RequestedQuantity','ConfirmedQuantity','Plant',
                                                                'ItemCategory','HigherLevelItem','StorageLocation','MaterialGroup','Subtotal1','Subtotal2',
                                                                'Subtotal3','Subtotal4','Subtotal5','Subtotal6','Batch','Incoterms','BatchedItem',
            													'BatchExpirationDate','ShortDatedBatch','MaterialSalesText'};
    }    
    
     public class Conditions_element {
        public String ItemNumber;
        public String ConditionStep;
        public String ConditionCount;
        public String ConditionType;
        public String ConditionRate;
        public String CalculationType;
        public String ConditionCategory;
        public String StatisticalIndicator;
        public String Currency_x;         
        public String ConditionValue;   
        public String ConditionClass;
        public String Description;
        public String ConditionIsInActive;
    
        private String[] ItemNumber_type_info = new String[]{'ItemNumber',NAMESPACE,null,'1','1','false'};
		private String[] ConditionStep_type_info = new String[]{'ConditionStep',NAMESPACE,null,'0','1','false'};
      	private String[] ConditionCount_type_info = new String[]{'ConditionCount',NAMESPACE,null,'0','1','false'};                                                
        private String[] ConditionType_type_info = new String[]{'ConditionType',NAMESPACE,null,'1','1','false'};
		private String[] ConditionRate_type_info = new String[]{'ConditionRate',NAMESPACE,null,'0','1','false'};            
		private String[] CalculationType_type_info = new String[]{'CalculationType',NAMESPACE,null,'0','1','false'};            
		private String[] ConditionCategory_type_info = new String[]{'ConditionCategory',NAMESPACE,null,'0','1','false'};            
		private String[] StatisticalIndicator_type_info = new String[]{'StatisticalIndicator',NAMESPACE,null,'0','1','false'};                        
        private String[] Currency_x_type_info = new String[]{'Currency',NAMESPACE,null,'0','1','false'};
        private String[] ConditionValue_type_info = new String[]{'ConditionValue',NAMESPACE,null,'0','1','false'};
        private String[] ConditionClass_type_info = new String[]{'ConditionClass',NAMESPACE,null,'0','1','false'};            
        private String[] Description_type_info = new String[]{'Description',NAMESPACE,null,'0','1','false'};
        private String[] ConditionIsInActive_type_info = new String[]{'ConditionIsInActive',NAMESPACE,null,'0','1','false'};
            
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'ItemNumber','ConditionStep','ConditionCount','ConditionType','ConditionRate','CalculationType','ConditionCategory',
            													'StatisticalIndicator','Currency_x','ConditionValue','ConditionClass','Description','ConditionIsInActive'};
    }    
    
    public class Return_element {
        public String MessageType;
        public String MessageID;
        public String MessageNumber;
        public String Message;
        
        private String[] MessageType_type_info = new String[]{'MessageType',NAMESPACE,null,'0','1','false'};
        private String[] MessageID_type_info = new String[]{'MessageID',NAMESPACE,null,'0','1','false'};
        private String[] MessageNumber_type_info = new String[]{'MessageNumber',NAMESPACE,null,'0','1','false'};
        private String[] Message_type_info = new String[]{'Message',NAMESPACE,null,'0','1','false'};
            
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'MessageType','MessageID','MessageNumber','Message'};
    }

    /*
     * Transformer - Construct an SAPOrder from a response object
     */     
    public static SAPOrder transform(SAPOrder order, OrderSimulateResponse.SalesOrderSimulateResponse response) {
        
        if (null != response.RewardItemsEligible) {
            order.rewardItemsEligible = SAPServiceDataHelper.getBooleanForSAPIndicator(response.RewardItemsEligible);
        }
        
		//apply changes to order - walk through items in reponse and compare to items in order
		//and apply changes accordingly
        if ((null <> response.items) && (response.items.size() > 0)){
            for (OrderSimulateResponse.Items_element i : response.items) {
                for (SAPOrder.Item oi : order.items) {
                    if (oi.itemNumber.equals(i.itemNumber)) {
                        oi.material = i.Material;
                        oi.speedCode = i.SpeedCode;
                        oi.shortText = i.ShortText;
                        oi.itemText = i.MaterialSalesText;
						oi.netValue = SAPServiceDataHelper.getDecimalForSAPNumber(i.NetValue,2);
						oi.currencyCode = i.currency_x;
                        oi.uom = i.UoM;
                        oi.deliveryDate = SAPServiceDataHelper.getDateForSAPDate(i.DeliveryDate);
                        oi.requestedQuantity = SAPServiceDataHelper.getDecimalForSAPNumber(i.RequestedQuantity,2);
                        oi.confirmedQuantity = SAPServiceDataHelper.getDecimalForSAPNumber(i.ConfirmedQuantity,2);
                        oi.plant = i.Plant;
                        oi.itemCategory = i.ItemCategory;
                        oi.higherLevelItem = i.HigherLevelItem;
                        oi.storageLocation = i.StorageLocation;
                        oi.materialGroup = i.MaterialGroup;
						oi.totalPrice = SAPServiceDataHelper.getDecimalForSAPNumber(i.SubTotal1,2);
						oi.netSubTotal = SAPServiceDataHelper.getDecimalForSAPNumber(i.SubTotal2,2);
						oi.netPrice = SAPServiceDataHelper.getDecimalForSAPNumber(i.SubTotal3,2);
						oi.shippingCharges = SAPServiceDataHelper.getDecimalForSAPNumber(i.SubTotal4,2);
						oi.tax = SAPServiceDataHelper.getDecimalForSAPNumber(i.SubTotal5,2);                        
						oi.discount = SAPServiceDataHelper.getDecimalForSAPNumber(i.SubTotal6,2);
                        
                        //unit price.  Calculate if requested quantity was > 0 only
                        if (oi.requestedQuantity > 0) {                
                            oi.unitPrice = oi.totalPrice / oi.requestedQuantity;
                            oi.unitPrice = oi.unitPrice.setScale(2);
                        }
                                                
                        oi.batch = i.Batch;
                        oi.batchedItem = SAPServiceDataHelper.getBooleanForSAPIndicator(i.batchedItem);
                        oi.batchExpirationDate = SAPServiceDataHelper.getDateForSAPDate(i.BatchExpirationDate);
                        oi.shortDatedBatch = SAPServiceDataHelper.getBooleanForSAPIndicator(i.ShortDatedBatch);

                        //process line item conditions
                        if ((null <> response.Conditions) && (response.Conditions.size() > 0)) {
                            for (OrderSimulateResponse.Conditions_element ic : response.Conditions) {
                                if (String.isNotBlank(ic.itemNumber) && ic.itemNumber.equals(oi.itemNumber)) {
                                    //VAT
                                    if (String.isNotBlank(ic.conditionType)
                                       && ic.conditionType.equals(SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.VAT_CONDITION))) {
                                        oi.vat = SAPServiceDataHelper.getDecimalForSAPNumber(ic.conditionValue,2) ;
                                    }
                                    
                                    //Points
                                    if (String.isNotBlank(ic.conditionType)
                                       && ic.conditionType.equals(SAPServiceDataHelper.getSAPCode(SAPServiceDataHelper.POINTS_REDEMPTION_CONDITION))) {
                                        oi.points = SAPServiceDataHelper.getDecimalForSAPNumber(ic.conditionValue,2) ;
                                    }                                   
                                }
                            }
                        }
                    
                        //pricing rollups
                        oi.totalTax = oi.tax + oi.vat;
                        oi.totalTax = Utils.formatCurrency(oi.totalTax, oi.currencyCode);
                        
                        oi.total = oi.totalPrice + oi.discount + oi.shippingCharges + oi.totalTax;
                        oi.total = Utils.formatCurrency(oi.total, oi.currencyCode);
                        
                        order.totalTax += oi.totalTax;
                        order.totalTax = Utils.formatCurrency(order.totalTax, oi.currencyCode);
                        
                        order.grandTotal += oi.total;
                        order.grandTotal = Utils.formatCurrency(order.grandTotal, oi.currencyCode);
                    }
                }
            }
        }        
        
       	//process return values
        if ((null <> response.return_x) && (response.return_x.size() > 0)) {
			List<SAPOrder.Return_x> returns = new List<SAPOrder.Return_x>();

            for (OrderSimulateResponse.Return_element r : response.return_x) {
                SAPOrder.Return_x return_x = new SAPOrder.Return_x();
                
                return_x.messageType = r.MessageType;
                return_x.messageId = r.MessageID;
                return_x.messageNumber = r.messageNumber;
                return_x.message = r.message;
                
                returns.add(return_x);
            }
            
            //replace whatever is there already
	        order.returns = returns;
        }
        
        return order;
    }
    
}