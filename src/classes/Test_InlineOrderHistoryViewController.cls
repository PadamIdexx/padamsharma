/* Class Name  : Test_InlineOrderHistoryViewController
 * Description  : Test Class with unit test scenarios to cover the InlineOrderHistoryViewController class
 * Created By  : Anudeep Gopagoni
 * Created On  : 10-07-2015
 
* Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer              Date                   Modification ID       Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Anudeep               10-07-2015                 10001                  Initial version
 * Sirisha               10-23-2015                 10002                  Modified to include new test methods
 Aditya Sangawar         01-29-2016                 10003                  Added new test methods ViewLABServicesTestCase & ViewLABServices1TestCase
                                           
*/
@isTest(seeAllData=false)
public class Test_InlineOrderHistoryViewController {
    static testMethod void ViewOrderHistoryPositiveTestCase()  {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = CreateTestClassData.reusableUser('System Administrator','admin');
        system.runAs(currentUser){
           // test data creation - test accounts 
          Account testAccount = CreateTestClassData.createCustomerAccount();
          Contact testContact = CreateTestClassData.reusableContact(testAccount.Id);
          insert testContact;
          testAccount.Primary_Contact__c = testContact.Id;
          testAccount.SAP_CUSTOMER_NUMBER__c = '0000001003';
          update testAccount;
          //test data creation - SalesView
          Salesview__c sV =  CreateTestClassData.CreateSalesview();
          // test data creation - partner Function;
          List<Partner_Function__c> pList = new List<Partner_Function__c>();
            for(Integer i=0;i<2;i++) {
               Partner_Function__c pF = new Partner_Function__c(Account__c=testAccount.Id,Partner_Account__c = testAccount.Id, Partner_Function__c = 'AG', Cust_SlsOrg_DistChan_Div_PartFunc_Ctr__c = String.valueOf(Math.random()).subString(3),Salesview__c = sV.Id );
               pList.add(pF);
           }
           insert pList;
         //set Mock response
         String sampleResponse = '(ORDER_HISTORY__x:{CreatedOn__c = 12011999, DocCurrency__c=USD, DisplayUrl=https://ps1w2.rt.informaticacloud.com/active-bpel/odata/v2/SFDCStaging/ORDER_HISTORY('+'\''+'%2C10%2C1439011254'+'\''+'), DIVISION__c=CP, ExternalId=,10,1439011254, Id=000000000000000AAA, TrackNum__c=TAN, SalesOrderItem__c =VETLAB})';
         Test.startTest();
         Test_ReusableMockResponse fakeResponse = new Test_ReusableMockResponse(200,'SUCCESS',sampleResponse, new map<String,String>{'Content-Type'=>'application/json'});
         Test.setMock(HttpCalloutMock.class, fakeResponse);
        
            //set the standard Controller to the test Account created 
            ApexPages.StandardController std = new ApexPages.StandardController(testAccount);
            InlineOrderHistoryViewController controllerInstance = new InlineOrderHistoryViewController(std);
            controllerInstance.getOptions();
            controllerInstance.partnerAccountsList.add(pList[0]); 
            controllerInstance.getOrderbyTypes(); 
            controllerInstance.selectedPartnerType = 'test'; 
            controllerInstance.typeOptions = new List<SelectOption>();
            controllerInstance.getTypes();
            //controllerInstance.getPartnerAccounts();
            controllerInstance.getOptionsForLabs();
            controllerInstance.getDate('12012011');
            controllerInstance.selectedType = 'AG';
            controllerInstance.formatThisOrderHistory(new Order_History__x(ShipTo__c = '12345678'));
             List<ORDER_HISTORY__x> tempData = new List<ORDER_HISTORY__x>();
             ORDER_HISTORY__x data1 = new ORDER_HISTORY__x();
             //data1.CreatedOn__c = '12011999';
             tempData.add(data1);
             controllerInstance.enableLabsHistListView = 'true';
             controllerInstance.reGroupOrders(tempData);
             Map<String,sObject> tempMap = new Map<String,sObject>();
             tempMap.put('12345678901', new ORDER_HISTORY__x(ShipTo__c = '12345678'));
             InlineOrderHistoryViewController.styleMap = tempMap;
             controllerInstance.processThisOrder('12345678901');
            //system.assert(controllerInstance.OrderHistLst.Size()>0);
        Test.stopTest();
       }
    }
    static testMethod void ViewOrderHistoryNegativeTestCase()  {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = CreateTestClassData.reusableUser('System Administrator','admin2');
        system.runAs(currentUser){
           // test data creation - test accounts 
          Account testAccount = CreateTestClassData.createCustomerAccount();
         //set Mock response
         Test_ReusableMockResponse fakeResponse = new Test_ReusableMockResponse(400,'FAILURE','Test', null);
         Test.setMock(HttpCalloutMock.class, fakeResponse);
         Test.startTest();
            //set the standard Controller to the test Account created 
            ApexPages.StandardController std = new ApexPages.StandardController(testAccount);
            InlineOrderHistoryViewController controllerInstance = new InlineOrderHistoryViewController(std);
           
        Test.stopTest();
       }
    }
    
    static testMethod void ViewLABServicesTestCase()  {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = CreateTestClassData.reusableUser('System Administrator','admin2');
        system.runAs(currentUser){
           // test data creation - test accounts 
         Account testAccount = CreateTestClassData.createCustomerAccount();
           // test data creation - SalesView                   
           
        Test.startTest();
            //set the standard Controller to the test Account created 
            ApexPages.StandardController std = new ApexPages.StandardController(testAccount);
            InlineOrderHistoryViewController controllerInstance = new InlineOrderHistoryViewController(std);
            //controllerInstance.partnerAccountsList.add(pF1);
            controllerInstance.enableLabsHistListView = 'false';
            controllerInstance.viewLabServices();
            controllerInstance.selectedType = 'WE';
            //controllerInstance.getPartnerAccounts();
        Test.stopTest();
       }
    }
    
    static testMethod void ViewLABServices1TestCase()  {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = CreateTestClassData.reusableUser('System Administrator','admin3');
        system.runAs(currentUser){
           // test data creation - test accounts 
          Account testAccount = CreateTestClassData.createCustomerAccount();
          testAccount.SAP_CUSTOMER_NUMBER__c = '0000001003';
          update testAccount;
        Salesview__c sV =  CreateTestClassData.CreateSalesview();
         Partner_Function__c pF1 = new Partner_Function__c(Account__c=testAccount.Id,Partner_Account__c = testAccount.Id, Partner_Function__c = 'WE', Cust_SlsOrg_DistChan_Div_PartFunc_Ctr__c = String.valueOf(Math.random()).subString(3),Salesview__c = sV.Id );
        Test.startTest();
            //set the standard Controller to the test Account created 
            ApexPages.StandardController std = new ApexPages.StandardController(testAccount);
            InlineOrderHistoryViewController controllerInstance = new InlineOrderHistoryViewController(std);
            controllerInstance.viewLabServices1();
            //controllerInstance.partnerAccountsList.add(pF1);

           
        Test.stopTest();
       }
    }
    


   
    static testMethod void reGroupOrdersTestCase()  {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = CreateTestClassData.reusableUser('System Administrator','admin42');
        system.runAs(currentUser){
            // test data creation - test accounts 
              Account testAccount = CreateTestClassData.createCustomerAccount();
              Contact testContact = CreateTestClassData.reusableContact(testAccount.Id);
              insert testContact;
              testAccount.Primary_Contact__c = testContact.Id;
              testAccount.SAP_CUSTOMER_NUMBER__c = '0000001003';
              update testAccount;
              
              
            Test.startTest();
                //set the standard Controller to the test Account created 
                ApexPages.StandardController std = new ApexPages.StandardController(testAccount);
                InlineOrderHistoryViewController controllerInstance = new InlineOrderHistoryViewController(std);
                
                ORDER_HISTORY__x test123  = new ORDER_HISTORY__x(CreatedOn__c = '12011999',SalesOrder__c ='USD',Price__c= 120,OrderQty__c = 19.99,ShipTo__c='12345678', PGIDate__c = '20140209');              
                controllerInstance.OrderHistLst =  new  List<ORDER_HISTORY__x>();                   
                controllerInstance.OrderHistLst.add(test123);               
                controllerInstance.reGroupOrders(controllerInstance.OrderHistLst);
                system.assert(controllerInstance.OrderHistLst.Size()>0);
            Test.stopTest();
        }
    }
    
    static testMethod void reGroupOrdersEVENOrdersTestCase()  {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = CreateTestClassData.reusableUser('System Administrator','admin42');
        system.runAs(currentUser){
            // test data creation - test accounts 
              Account testAccount = CreateTestClassData.createCustomerAccount();
              Contact testContact = CreateTestClassData.reusableContact(testAccount.Id);
              insert testContact;
              testAccount.Primary_Contact__c = testContact.Id;
              testAccount.SAP_CUSTOMER_NUMBER__c = '0000001003';
              update testAccount;
              
              
            Test.startTest();
                //set the standard Controller to the test Account created 
                ApexPages.StandardController std = new ApexPages.StandardController(testAccount);
                InlineOrderHistoryViewController controllerInstance = new InlineOrderHistoryViewController(std);
                //InlineOrderHistoryViewController.DisplayWrapper disWrap2 = new InlineOrderHistoryViewController.DisplayWrapper(); 
                //disWrap2.POTypeDesc = 'B2B';
                
                ORDER_HISTORY__x test123  = new ORDER_HISTORY__x(CreatedOn__c = '12011999',SalesOrder__c ='USD',Price__c= 120,OrderQty__c = 19.99,ShipTo__c='12345678', POType__c = 'B2B1', DelvCreatedOn__c = '20150311', PGIDate__c = '20140209');              
                ORDER_HISTORY__x test1234  = new ORDER_HISTORY__x(CreatedOn__c = '12011999',SalesOrder__c ='USD',Price__c= 120,OrderQty__c = 19.99,ShipTo__c='12345678', POType__c = 'CRM', DelvCreatedOn__c = '20140210', PGIDate__c = '20140209');              
                
                controllerInstance.OrderHistLst =  new  List<ORDER_HISTORY__x>();                   
                controllerInstance.OrderHistLst.add(test123);  
                controllerInstance.OrderHistLst.add(test1234);                 
                //InlineOrderHistoryViewController.ordWrapperList.add(disWrap2); 
                controllerInstance.reGroupOrders(controllerInstance.OrderHistLst);
                system.assert(controllerInstance.OrderHistLst.Size()>0);
            Test.stopTest();
        }
    }  
    
    
   static testMethod void reGroupOrdersODDOrdersTestCase(){
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = CreateTestClassData.reusableUser('System Administrator','admin42');
        system.runAs(currentUser){
            // test data creation - test accounts 
              Account testAccount = CreateTestClassData.createCustomerAccount();
              Contact testContact = CreateTestClassData.reusableContact(testAccount.Id);
              insert testContact;
              testAccount.Primary_Contact__c = testContact.Id;
              testAccount.SAP_CUSTOMER_NUMBER__c = '0000001003';
              update testAccount;
              
              
            Test.startTest();
                //set the standard Controller to the test Account created 
                ApexPages.StandardController std = new ApexPages.StandardController(testAccount);
                InlineOrderHistoryViewController controllerInstance = new InlineOrderHistoryViewController(std);
                //InlineOrderHistoryViewController.DisplayWrapper disWrap1 = new InlineOrderHistoryViewController.DisplayWrapper(); 
                //disWrap1.POTypeDesc = 'Beacon';
                ORDER_HISTORY__x test123  = new ORDER_HISTORY__x(CreatedOn__c = '12011999',SalesOrder__c ='14334',Price__c= 120,OrderQty__c = 19.99,ShipTo__c='12345678',POType__c = 'BEA', DelvCreatedOn__c = '20151223', PGIDate__c = '20140209');              
                ORDER_HISTORY__x test1234  = new ORDER_HISTORY__x(CreatedOn__c = '12011999',SalesOrder__c ='14335',Price__c= 122,OrderQty__c = 29.99,ShipTo__c='12345678',POType__c = 'B2B', DelvCreatedOn__c = '20152012', PGIDate__c = '20140209');              
                ORDER_HISTORY__x test12345  = new ORDER_HISTORY__x(CreatedOn__c = '12011999',SalesOrder__c ='14334',Price__c= 123,OrderQty__c = 39.99,ShipTo__c='12345678', POType__c = 'B2B1',DelvCreatedOn__c = '20140209', PGIDate__c = '20140209');              
                ORDER_HISTORY__x test123456  = new ORDER_HISTORY__x(CreatedOn__c = '12011999',SalesOrder__c ='14334',Price__c= 123,OrderQty__c = 39.99,ShipTo__c='12345678', POType__c = 'CRM',DelvCreatedOn__c = '20140308', PGIDate__c = '20140209');              
                controllerInstance.OrderHistLst =  new  List<ORDER_HISTORY__x>();                   
                controllerInstance.OrderHistLst.add(test123);  
                controllerInstance.OrderHistLst.add(test1234);
                controllerInstance.OrderHistLst.add(test12345);      
                controllerInstance.OrderHistLst.add(test123456);       
                //InlineOrderHistoryViewController.ordWrapperList.add(tmpOrder); 
                controllerInstance.enableLabsHistListView = 'true';
                system.currentPageReference().getParameters().put('dropDown', 'true');
                controllerInstance.reGroupOrders(controllerInstance.OrderHistLst);
                system.assert(controllerInstance.OrderHistLst.Size()>0);
            Test.stopTest();
        }
    }
    
    static testMethod void reGroupOrdersEVEN2OrdersTestCase()  {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = CreateTestClassData.reusableUser('System Administrator','admin42');
        system.runAs(currentUser){
            // test data creation - test accounts 
              Account testAccount = CreateTestClassData.createCustomerAccount();
              Contact testContact = CreateTestClassData.reusableContact(testAccount.Id);
              insert testContact;
              testAccount.Primary_Contact__c = testContact.Id;
              testAccount.SAP_CUSTOMER_NUMBER__c = '0000001003';
              update testAccount;
              
              
            Test.startTest();
                //set the standard Controller to the test Account created 
                ApexPages.StandardController std = new ApexPages.StandardController(testAccount);
                InlineOrderHistoryViewController controllerInstance = new InlineOrderHistoryViewController(std);
                
                ORDER_HISTORY__x test123  = new ORDER_HISTORY__x(CreatedOn__c = '20151012',SalesOrder__c ='1433410101',Price__c= 120,OrderQty__c = 19.99,ShipTo__c='12345678', POType__c = 'CRM', DelvCreatedOn__c = '20140209', PGIDate__c = '20140209');              
                ORDER_HISTORY__x test1234  = new ORDER_HISTORY__x(CreatedOn__c = '20151012',SalesOrder__c ='1433410101',Price__c= 120,OrderQty__c = 19.99,ShipTo__c='12345678',POType__c = 'BEA', DelvCreatedOn__c = '20140209', PGIDate__c = '20140209');              
                ORDER_HISTORY__x test12345  = new ORDER_HISTORY__x(CreatedOn__c = '20151012',SalesOrder__c ='1433410101',Price__c= 120,OrderQty__c = 19.99,ShipTo__c='12345678',POType__c = 'B2B', DelvCreatedOn__c = '20140209', PGIDate__c = '20140209');              
                ORDER_HISTORY__x test123456  = new ORDER_HISTORY__x(CreatedOn__c = '20151011',SalesOrder__c ='1433410102',Price__c= 120,OrderQty__c = 19.99,ShipTo__c='12345678',POType__c = 'B2B1', DelvCreatedOn__c = '20140209', PGIDate__c = '20140209');              
                ORDER_HISTORY__x test1234567 = new ORDER_HISTORY__x(CreatedOn__c = '20151011',SalesOrder__c ='1433410102',Price__c= 120,OrderQty__c = 19.99,ShipTo__c='12345678', POType__c = '', DelvCreatedOn__c = '20140209',PGIDate__c = '20140209');              
                ORDER_HISTORY__x test12345678 = new ORDER_HISTORY__x(CreatedOn__c = '20151013',SalesOrder__c ='1433410104',Price__c= 120,OrderQty__c = 19.99,ShipTo__c='12345678', POType__c = 'BAE', DelvCreatedOn__c = '20140209',PGIDate__c = '20140209');              
                controllerInstance.OrderHistLst =  new  List<ORDER_HISTORY__x>();                   
                controllerInstance.OrderHistLst.add(test123);  
                controllerInstance.OrderHistLst.add(test1234);
                controllerInstance.OrderHistLst.add(test12345);  
                controllerInstance.OrderHistLst.add(test123456);   
                controllerInstance.OrderHistLst.add(test1234567); 
                controllerInstance.OrderHistLst.add(test12345678);
                controllerInstance.errorMsg = 'No records found';       
                controllerInstance.enableLabsHistListView = 'true';     
                system.currentPageReference().getParameters().put('dropDown', null);
                controllerInstance.reGroupOrders(controllerInstance.OrderHistLst);
                system.assert(controllerInstance.OrderHistLst.Size()>0);
            Test.stopTest();
        }
    } 
}