public with sharing class OrderReadController {

    public SAPOrder theOrder {get;set;}
    public String message {get;set;}
    public Boolean hasOrder {get;set;}
    
    public ProfileDisplayWrapper userProfile;
    
    public OrderReadController(ApexPages.StandardController std) {
        hasOrder = false;
        String documentNumber = ApexPages.CurrentPage().getParameters().get('documentNumber');  
        ProfileDisplayWrapper profile = new ProfileDisplayWrapper();            
        
        //All for both retrieving based on a detail page button
        //as well as just going to the url with the documentNumber
        if (String.isNotBlank(documentNumber)) {
            try {
                SAPOrder o = new SAPOrder();
                o.userProfile = profile;
                o.documentNumber = documentNumber;
                o.language = 'EN';
                
                theOrder = OrderManager.getOrder(o); 
                hasOrder = true;
            } catch (Exception e) {
                System.debug('Error retrieving order.');
                message = 'Error retrieving order. \n' + e.getMessage();
                hasOrder = false;
            }         
        } else {
	        String id = ApexPages.CurrentPage().getParameters().get('id');
			Order ord = [Select AccountId, Id, Status, OrderReferenceNumber From Order WHERE Id =: id]; 
            documentNumber = ord.OrderReferenceNumber;                      
            
            try {
                if ((null != ord)
                        && (ord.Status.equalsIgnoreCase('Submitted To SAP'))) {
                    SAPOrder o = new SAPOrder();
                    o.userProfile = profile;
                    o.documentNumber = documentNumber;
                    o.language = 'EN';
                    
                    theOrder = OrderManager.getOrder(o); 
                    hasOrder = true;
                 } else if (ord.Status.equalsIgnoreCase('Deleted')) {
                     message = 'Order was deleted from SAP and cannot be retrieved.';
                     hasOrder = false;
                 } else if (ord.Status.equalsIgnoreCase('Draft')) {
                     message = 'Order not Submitted to SAP.';
                     hasOrder = false;
                 } else {
                     message = 'Order not found in SAP.';
                     hasOrder = false;
                 }
            } catch (Exception e) {
                System.debug('Error retrieving order.');
                System.debug(e.getMessage());
                System.debug(e.getStackTraceString());
                message = 'Error retrieving order. \n' + e.getMessage();
                hasOrder = false;
            }
        }
    } 
    
    
    
}