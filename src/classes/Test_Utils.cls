@isTest
public with sharing class Test_Utils {

	@isTest
	private static void testFormattedAddress(){
        Test.StartTest();

		String formattedAddress = Utils.formatAddress('Street', 'City', 'State', 'Country', 'Zip');
        System.assertEquals(formattedAddress, 'Street, City, State, Country, Zip');

        Test.StopTest();

	}

	@isTest
	private static void testAppendComma(){
        Test.StartTest();

		String commaAppended = Utils.appendComma('Before', 'After');
        System.assertEquals(commaAppended, 'After, Before');

        Test.StopTest();

	}

	@isTest
	private static void testRemoveLeadingZeros(){
        Test.StartTest();

		String sapId = '000001138';
        String strippedId = Utils.removeLeadingZeros(sapId);
        System.AssertEquals(strippedId, '1138');

        sapId = '1138';
        strippedId = Utils.removeLeadingZeros(sapId);
        System.AssertEquals(strippedId, '1138');

        sapId = '010101138';
        strippedId = Utils.removeLeadingZeros(sapId);
        System.AssertEquals(strippedId, '10101138');

        sapId = null;
        strippedId = Utils.removeLeadingZeros(sapId);
        System.AssertEquals(strippedId, null);


        Test.StopTest();

	}
    
    @isTest
	private static void testChunkStringAtWordBoundaries(){
        Test.StartTest();
		// public static List<String> chunkStringAtWordBoundaries(String inputString, String charToken, Integer desiredStringLength) {  
		String longString = 'Lather rinse repeat. And again... Lather rinse repeat. And again... Lather skjasdn jsadn kasdm kasdm askd nasd  knasd nasdjn asjdnl ksmdfmf asdasdasdsad qwekjqwne rinse repeat. And again... Lather rinse repeat. And again... Lather rinse repeat. And again... Lather rinse repeat. And again... Lather rinse repeat. And again... Lather rinse repeat. And again... Lather rinse repeat. And again... ';
		List<String> chunkResult = null; 
        chunkResult = Utils.chunkStringAtWordBoundaries(longString, ' ', 132);
        System.assertNotEquals(null, chunkResult);
        if (chunkResult != null) {
            for (String stringItem : chunkResult) {
               System.assertNotEquals(stringItem.length(), 132); 
            }
        }
        
        Test.StopTest();

	}
	
	//3.10.2017 HLK added
	@isTest
	private static void testFormatCurrency() {
	    Test.startTest();
	    Utils.formatCurrency(1.00, 'USD');
	    Test.stopTest();
	    
	}
	
    @isTest 
    private static void testFormatCurrencyNegative() {
	    Test.startTest();
	    Utils.formatCurrency(null, 'USD');
	    Utils.formatCurrency(1.00, null);        
	    Test.stopTest();        
    }
    
	@isTest
	private static void testformatCurrencyForLocaleNegative() {
	    
	    Test.StartTest();
	    

        Decimal t1 = Utils.formatCurrencyForLocale(null, '1234');
		System.assertEquals(t1, null);        
        
        Decimal t2 = Utils.formatCurrencyForLocale('en_US', null);
		System.assertEquals(t2, null);
        
        Decimal t3 = Utils.formatCurrencyForLocale(null, null);
        System.assertEquals(t3, null);

	    Test.stopTest();
	}
    
    @isTest
	private static void testformatCurrencyForLocale_en_US() {
	    
	    Test.StartTest();

        Decimal t1 = Utils.formatCurrencyForLocale('en_US', '1234');
        System.assertEquals(t1, 1234);

        Decimal t2 = Utils.formatCurrencyForLocale('en_US', '1,234');
        System.assertEquals(t2, 1234);

        Decimal t3 = Utils.formatCurrencyForLocale('en_US', '12.34');
        System.assertEquals(t3, 12.34);

	    Test.stopTest();
	}

    @isTest
	private static void testformatCurrencyForLocale_fr_FR() {
	    
	    Test.StartTest();

        Decimal t1 = Utils.formatCurrencyForLocale('fr_FR', '1234');
        System.assertEquals(t1, 1234);

        Decimal t2 = Utils.formatCurrencyForLocale('fr_FR', '1.234');
        System.assertEquals(t2, 1234);

        Decimal t3 = Utils.formatCurrencyForLocale('fr_FR', '12,34');
        System.assertEquals(t3, 12.34);


	    Test.stopTest();
	}
    
}