@isTest
private class Test_OrderReadController {

	private static testMethod void testOrderReadControllerDraftDocumentNumber() {
	    
	    Test.startTest();
        SAPOrder order = OrderTestDataFactory.createSAPOrder();
        Account customerAccount = new Account();
        customerAccount = CreateTestClassData.createCustomerAccount();
        
         // Insert Products
        Product2 testProduct = new Product2(Name='SNAP PRO STARTER KIT', Family='test', SAP_MATERIAL_NUMBER__c = '11235');
        insert testProduct;
        
        // Insert PriceBook
        Pricebook2 priceBook = CreateTestClassData.reusablePricebook2('IDEXX Standard Price Book');
        insert priceBook;
        
        // Fetch Standard PriceBookId
        Id pricebookId = Test.getStandardPricebookId();
        System.debug('PB2 Id:'+pricebookId);
        
        // Insert PriceBookEntry
        PricebookEntry stdpriceBookEntry = CreateTestClassData.reusablePriceBookEntry(testProduct.Id, pricebookId,1000);
        stdpriceBookEntry.UseStandardPrice = false;
        insert stdpriceBookEntry; 
        
        //Order
        Order thisOrder = CreateTestClassData.createOrder(customerAccount,pricebookId, customerAccount.CurrencyISOCode);
        
        System.currentPageReference().getParameters().put('documentNumber', thisOrder.OrderReferenceNumber); 
        
        OrderReadController controller = new OrderReadController(new ApexPages.StandardController(customerAccount));
        
        Test.stopTest();

	}


	private static testMethod void testOrderReadControllerDraftOrderId() {
	    
	    Test.startTest();
        SAPOrder order = OrderTestDataFactory.createSAPOrder();
        Account customerAccount = new Account();
        customerAccount = CreateTestClassData.createCustomerAccount();
        
         // Insert Products
        Product2 testProduct = new Product2(Name='SNAP PRO STARTER KIT', Family='test', SAP_MATERIAL_NUMBER__c = '11235');
        insert testProduct;
        
        // Insert PriceBook
        Pricebook2 priceBook = CreateTestClassData.reusablePricebook2('IDEXX Standard Price Book');
        insert priceBook;
        
        // Fetch Standard PriceBookId
        Id pricebookId = Test.getStandardPricebookId();
        System.debug('PB2 Id:'+pricebookId);
        
        // Insert PriceBookEntry
        PricebookEntry stdpriceBookEntry = CreateTestClassData.reusablePriceBookEntry(testProduct.Id, pricebookId,1000);
        stdpriceBookEntry.UseStandardPrice = false;
        insert stdpriceBookEntry; 
        
        //Order
        Order thisOrder = CreateTestClassData.createOrder(customerAccount,pricebookId, customerAccount.CurrencyISOCode);
        
        //System.currentPageReference().getParameters().put('documentNumber', thisOrder.OrderReferenceNumber); 
        
        System.currentPageReference().getParameters().put('id', thisOrder.Id); 
        OrderReadController controller = new OrderReadController(new ApexPages.StandardController(customerAccount));
        
        Test.stopTest();

	}
	
	private static testMethod void testOrderReadControllerSubmittedOrderId() {
	    
	    Test.startTest();
        SAPOrder order = OrderTestDataFactory.createSAPOrder();
        Account customerAccount = new Account();
        customerAccount = CreateTestClassData.createCustomerAccount();
        
         // Insert Products
        Product2 testProduct = new Product2(Name='SNAP PRO STARTER KIT', Family='test', SAP_MATERIAL_NUMBER__c = '11235');
        insert testProduct;
        
        // Insert PriceBook
        Pricebook2 priceBook = CreateTestClassData.reusablePricebook2('IDEXX Standard Price Book');
        insert priceBook;
        
        // Fetch Standard PriceBookId
        Id pricebookId = Test.getStandardPricebookId();
        System.debug('PB2 Id:'+pricebookId);
        
        // Insert PriceBookEntry
        PricebookEntry stdpriceBookEntry = CreateTestClassData.reusablePriceBookEntry(testProduct.Id, pricebookId,1000);
        stdpriceBookEntry.UseStandardPrice = false;
        insert stdpriceBookEntry; 
        
        //Order
        Order thisOrder = CreateTestClassData.createOrder(customerAccount,pricebookId, customerAccount.CurrencyISOCode);
        thisOrder.Status = 'Submitted To SAP';
        upsert thisOrder;

        System.currentPageReference().getParameters().put('id', thisOrder.Id); 
        OrderReadController controller = new OrderReadController(new ApexPages.StandardController(customerAccount));
        
        Test.stopTest();

	}

    private static testMethod void testOrderReadControllerDeletedOrderId() {
	    
	    Test.startTest();
        SAPOrder order = OrderTestDataFactory.createSAPOrder();
        Account customerAccount = new Account();
        customerAccount = CreateTestClassData.createCustomerAccount();
        
         // Insert Products
        Product2 testProduct = new Product2(Name='SNAP PRO STARTER KIT', Family='test', SAP_MATERIAL_NUMBER__c = '11235');
        insert testProduct;
        
        // Insert PriceBook
        Pricebook2 priceBook = CreateTestClassData.reusablePricebook2('IDEXX Standard Price Book');
        insert priceBook;
        
        // Fetch Standard PriceBookId
        Id pricebookId = Test.getStandardPricebookId();
        System.debug('PB2 Id:'+pricebookId);
        
        // Insert PriceBookEntry
        PricebookEntry stdpriceBookEntry = CreateTestClassData.reusablePriceBookEntry(testProduct.Id, pricebookId,1000);
        stdpriceBookEntry.UseStandardPrice = false;
        insert stdpriceBookEntry; 
        
        //Order
        Order thisOrder = CreateTestClassData.createOrder(customerAccount,pricebookId, customerAccount.CurrencyISOCode);
        thisOrder.Status = 'Deleted';
        upsert thisOrder;

        System.currentPageReference().getParameters().put('id', thisOrder.Id); 
        OrderReadController controller = new OrderReadController(new ApexPages.StandardController(customerAccount));
        
        Test.stopTest();

	}
	
	
    private static testMethod void testOrderReadControllerBlockedOrderId() {
	    
	    Test.startTest();
        SAPOrder order = OrderTestDataFactory.createSAPOrder();
        Account customerAccount = new Account();
        customerAccount = CreateTestClassData.createCustomerAccount();
        
         // Insert Products
        Product2 testProduct = new Product2(Name='SNAP PRO STARTER KIT', Family='test', SAP_MATERIAL_NUMBER__c = '11235');
        insert testProduct;
        
        // Insert PriceBook
        Pricebook2 priceBook = CreateTestClassData.reusablePricebook2('IDEXX Standard Price Book');
        insert priceBook;
        
        // Fetch Standard PriceBookId
        Id pricebookId = Test.getStandardPricebookId();
        System.debug('PB2 Id:'+pricebookId);
        
        // Insert PriceBookEntry
        PricebookEntry stdpriceBookEntry = CreateTestClassData.reusablePriceBookEntry(testProduct.Id, pricebookId,1000);
        stdpriceBookEntry.UseStandardPrice = false;
        insert stdpriceBookEntry; 
        
        //Order
        Order thisOrder = CreateTestClassData.createOrder(customerAccount,pricebookId, customerAccount.CurrencyISOCode);
        thisOrder.Status = 'Blocked';
        upsert thisOrder;
        
        System.currentPageReference().getParameters().put('id', thisOrder.Id); 
        OrderReadController controller = new OrderReadController(new ApexPages.StandardController(customerAccount));
        
        Test.stopTest();

	}

}