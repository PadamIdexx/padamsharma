@isTest
public class Test_LatestTask{
    static testMethod void Test_LatestTask()
    {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User us = new User();
        us.FirstName = 'Test';
        us.LastName = 'User';
        us.Email='standardtestuser@testsamorg.com';
        us.Alias='standt';
        us.TimeZoneSidKey='America/Los_Angeles';
        us.CommunityNickname='sample';
        us.Username='standardtestuser@testsamorg.com';
        us.ProfileId = p.Id;
        us.LocaleSidKey='en_US';
        us.EmailEncodingKey='UTF-8';
        us.LanguageLocaleKey='en_US';
        insert us;
        
        system.debug('user::' + us);
        
        Account acc = new Account();

        acc.Name = 'Test Account';
        acc.CurrencyIsoCode = 'AUD';
        acc.BillingCity ='Chennai' ;
        acc.BillingCountry='india';
        acc.BillingLatitude=56.57577;
        acc.BillingLongitude=78.4546;
        acc.BillingPostalCode='600075';
        acc.BillingState='tamil nadu';
        acc.BillingStreet='water well street';  
        acc.ShippingCity=null;
        acc.ShippingCountry=null;
        acc.ShippingLatitude=null;
        acc.ShippingLongitude=null;
        acc.ShippingPostalCode=null;
        acc.ShippingState=null;
        acc.ShippingStreet=null;
        acc.Veterinary_License_Number__c='TIN';
        
        // add whatever other fields are required for account creation here
        insert acc;
        
        Contact con = new Contact();
        con.FirstName = 'Test'; // this assumes firstname is a required field on your contact records
        con.LastName = 'Contact'; // this assumes lastname is a required field on your contact records
        con.AccountId = acc.Id; // this assumes account is a required field on your contact records
        con.Title = 'Test Title'; // this is to validate your trigger
        con.MobilePhone = '1111111111'; // this is to validate your trigger
        // add whatever other fields are required for contact creation here
        insert con;
        
        Task u = new Task();
        u.ownerId = us.Id; // you've now successfully created your test user, time to use it on your new task
        u.whoId = con.Id; // you've now successfully created your test contact, time to use it on your new task
        u.Subject = 'Run Test Trigger';
        u.Status = 'Not Started';
        u.Priority = 'Normal';
        insert u;
        
         Test.startTest();
            ApexPages.StandardController std = new ApexPages.StandardController(u);
            ApexPages.currentPage().getParameters().put('what_id',acc.Id);
            LatestTask tsk = new LatestTask(std);
            tsk.cancel();
            tsk.saveNewTask();
            tsk.SaveNewEvent();
            //tsk.Save();
            Test.stopTest();
       
        
    }
    
}