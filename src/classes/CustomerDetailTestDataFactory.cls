@isTest
public class CustomerDetailTestDataFactory {

     public static CustomerDetailRequest.CUSTOMER_GETDETAIL_Request createRequest() {
    	CustomerDetailRequest.CUSTOMER_GETDETAIL_Request request = new CustomerDetailRequest.CUSTOMER_GETDETAIL_Request();
        
        request.CUSTOMERNO = '0000001138';
        request.LANGUAGE = 'EN';
        
        return request;
    }
    
    public static CustomerDetailResponse.CUSTOMER_GETDETAIL_Response createResponse() {
        CustomerDetailResponse.CUSTOMER_GETDETAIL_Response response = new CustomerDetailResponse.CUSTOMER_GETDETAIL_Response();
        
        CustomerDetailResponse.CUSTOMERADDRESS_element addr = new CustomerDetailResponse.CUSTOMERADDRESS_element();        
        List<CustomerDetailResponse.FAX_element> faxes = new List<CustomerDetailResponse.FAX_element>();
        CustomerDetailResponse.FAX_element fax = new CustomerDetailResponse.FAX_element();
        faxes.add(fax);
        addr.fax = faxes;        
        response.CUSTOMERADDRESS = addr;
        
        CustomerDetailResponse.CUSTOMERGENERALDETAIL_element generalDetail = new CustomerDetailResponse.CUSTOMERGENERALDETAIL_element();
        response.CUSTOMERGENERALDETAIL = generalDetail;
        
        List<CustomerDetailResponse.CUSTOMERCOMPANYDETAIL_element> companyInfo = new List<CustomerDetailResponse.CUSTOMERCOMPANYDETAIL_element>();
        CustomerDetailResponse.CUSTOMERCOMPANYDETAIL_element ci = new CustomerDetailResponse.CUSTOMERCOMPANYDETAIL_element();
        companyInfo.add(ci);        
        response.CUSTOMERCOMPANYDETAIL = companyInfo;

        List<CustomerDetailResponse.COMMENTS_element> comments = new List<CustomerDetailResponse.COMMENTS_element>();
        CustomerDetailResponse.COMMENTS_element comment = new CustomerDetailResponse.COMMENTS_element();
        comments.add(comment);
        response.COMMENTS = comments;
        
        List<CustomerDetailResponse.FLYER_MESSAGES_element> flyers = new List<CustomerDetailResponse.FLYER_MESSAGES_element>();
        CustomerDetailResponse.FLYER_MESSAGES_element fm = new CustomerDetailResponse.FLYER_MESSAGES_element();
        flyers.add(fm);
        response.FLYER_MESSAGES = flyers;        

        List<CustomerDetailResponse.PARTNERS_element> partners = new List<CustomerDetailResponse.PARTNERS_element>();
        CustomerDetailResponse.PARTNERS_element p = new CustomerDetailResponse.PARTNERS_element();
        partners.add(p);
        response.PARTNERS = partners;   
        
        CustomerDetailResponse.RETURN_element return_x = new CustomerDetailResponse.RETURN_element();
        response.RETURN_x = return_x;
        
        CustomerDetailResponse.CUSTOMERBANKDETAIL_element bank = new CustomerDetailResponse.CUSTOMERBANKDETAIL_element();        
        List<CustomerDetailResponse.item_element> b_items = new List<CustomerDetailResponse.item_element>();
        CustomerDetailResponse.item_element bi = new CustomerDetailResponse.item_element(); 
        b_items.add(bi);
        bank.item = b_items;
        response.CUSTOMERBANKDETAIL = bank;
                
        List<CustomerDetailResponse.SALES_DATA_element> sales = new List<CustomerDetailResponse.SALES_DATA_element>();
        CustomerDetailResponse.SALES_DATA_element s = new CustomerDetailResponse.SALES_DATA_element();
        
        //add some data for the payment terms tests     
        s.SALES_ORG = 'USS1';
        s.DIVISION = 'CP';
        s.PAYMENT_TERMS = 'ZP25';
        
        List<CustomerDetailResponse.SALES_TEXT_element> texts = new List<CustomerDetailResponse.SALES_TEXT_element>();
        CustomerDetailResponse.SALES_TEXT_element t = new CustomerDetailResponse.SALES_TEXT_element();
        texts.add(t);
        s.SALES_TEXT = texts;
        sales.add(s);
        response.SALES_DATA = sales; 
        
        List<CustomerDetailResponse.CREDIT_CONTROL_element> credit = new List<CustomerDetailResponse.CREDIT_CONTROL_element>();
        CustomerDetailResponse.CREDIT_CONTROL_element c = new CustomerDetailResponse.CREDIT_CONTROL_element();
        credit.add(c);
        response.CREDIT_CONTROL = credit; 

        List<CustomerDetailResponse.SOLD_TO_DETAIL_element> sold_to = new List<CustomerDetailResponse.SOLD_TO_DETAIL_element>();
        CustomerDetailResponse.SOLD_TO_DETAIL_element st = new CustomerDetailResponse.SOLD_TO_DETAIL_element();
        sold_to.add(st);
        response.SOLD_TO_DETAIL = sold_to;         
        
        return response;
        
    }
    
}