@isTest 
public class Test_SimulateWrapper {
    @isTest static void testPositive() {
        Test.startTest();
        try {
            SAPOrder order = OrderTestDataFactory.createSAPOrder();
            SimulateWrapper w = new SimulateWrapper();
            w.execute(order);
        } catch (Exception e) {
            System.debug('Got Exception which was expected.');
        }          
      
        Test.stopTest();			
    } 

    @isTest static void testBadRequest() {
        Test.startTest();
        try {
            SimulateWrapper w = new SimulateWrapper();
            w.execute(null);
        } catch (Exception e) {
            System.debug('Got Exception which was expected.');
        }          
      
        Test.stopTest();			
    }  
}