@isTest
Private class Test_WebServiceUtility {
	
		static testMethod void contactPersonQueryPositive() {
	        User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
	        system.runAs(currentUser){
	        	 Test.startTest();
		        	 ContactPersonProcessingRequest.ContactPersonQuery_Sync request = new ContactPersonProcessingRequest.ContactPersonQuery_Sync();
		        	 WebServiceUtil utility = new WebServiceUtil();
		        	 WebServiceUtil.contactPersonQueryresponse = CreateTestClassData.dummycontactpersonRead();
		        	 utility.contactPersonReadCall(request);
	        	 Test.StopTest();
	        }
		}
		static testMethod void contactPersonUpdateCallPositive() {
	        User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
	        system.runAs(currentUser){
	        	 Test.startTest();
		        	 ContactPersonProcessingRequest.ContactPersonUpdate_Sync request = new ContactPersonProcessingRequest.ContactPersonUpdate_Sync();
		        	 WebServiceUtil utility = new WebServiceUtil();
		        	 WebServiceUtil.contactPersonUpdateresponse = CreateTestClassData.dummyContactWriteUpdate();
		        	 utility.contactPersonUpdateCall(request);
	        	 Test.StopTest();
	        }
		}
		static testMethod void contactPersonAddCallPositive() {
	        User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
	        system.runAs(currentUser){
	        	 Test.startTest();
		        	 ContactPersonProcessingRequest.ContactPersonCreate_Sync  request = new ContactPersonProcessingRequest.ContactPersonCreate_Sync();
		        	 WebServiceUtil utility = new WebServiceUtil();
		        	 WebServiceUtil.contactPersonAddresponse = CreateTestClassData.dummyContactWriteUpdate();
		        	 utility.contactPersonAddCall(request);
	        	 Test.StopTest();
	        }
		}

		static testMethod void negativeGetParams(){
			Test.startTest();
				WebServiceUtil.retreiveParameters('SalesOrg1');
			Test.stopTest();
		}
		static testMethod void retrieveUserId() {
   	        User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
	        system.runAs(currentUser){
                Test.startTest();            
                WebServiceUtil.retrieveUserId(currentUser.Id);            
                Test.stopTest();
            }
        }
    
       	static testMethod void retrieveUserIdNoUserId() {
            //use system integration because it does not have an idexx_user_id defined for it
   	        User currentUser = CreateTestClassData.reusableUser('System Integration','TestU');
	        system.runAs(currentUser){
                Test.startTest();            
                WebServiceUtil.retrieveUserId(currentUser.Id);            
                Test.stopTest();
            }
        }

}