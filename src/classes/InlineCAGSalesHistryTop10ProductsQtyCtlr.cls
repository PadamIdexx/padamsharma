/**
*       @author Aditya Sangawar
*       @date   12/01/2015
        @description   This is VF Controller that fetches & Displays Top 5 Kits & Consumables 
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Aditya Sangawar                 12/01/2015         Created
        Aditya Sangawar                 14/01/2015         Modified From Top 5 to Top 10 and added the Avg/Month
**/

global class InlineCAGSalesHistryTop10ProductsQtyCtlr {
    private  ApexPages.StandardController                   controller                 {get;set;}//added an instance variable for the standard controller
    //public static list<Customer_Sales_History__c>               CAGSalesHistoryList {get;set;}
    public static list<Customer_Sales_History__c>               CagKits {get;set;}
    public static list<Customer_Sales_History__c>               CagConsumables {get;set;}
    public static list<Customer_Sales_History__c>               CagKitsAndConsumables {get;set;}
   
    public static Boolean hasData                               {get;set;}
    public static List<String> noRecordsFound                   {get;set;}
    
    static Account currentAccountRecord;
    
    public static String cagKitsFamilies {get;set;}
    public static String cagConsumableFamilies {get;set;}
    
    public static List<Integer> rank {get;set;}
    Transient Integer i=0;
    
    // column headers
  public String MONTH_CURRENT  {get;set;}
  public String MONTH_1_AGO    {get;set;}
  public String MONTH_2_AGO    {get;set;}
  public String MONTH_3_AGO    {get;set;}
 // public String MONTH_4_AGO    {get;set;}
 
    //constuctor 
    public InlineCAGSalesHistryTop10ProductsQtyCtlr (ApexPages.StandardController controller){
        
        //Initializations of the local variables
        this.controller           = controller;  
       // errorMsg                  = '';
                // fetch the account details 
        currentAccountRecord = (Account)controller.getRecord();
        if(currentAccountRecord<>null && currentAccountRecord.Id<>null)
            currentAccountRecord = [Select Id,SAP_CUSTOMER_NUMBER__c from Account where Id=:currentAccountRecord.Id];
            GroupProducts2();
        setHeaderValues();
    }
    
    private void setHeadervalues(){
    // initialise all variables
    MONTH_CURRENT    = '';
    MONTH_1_AGO  = '';
    MONTH_2_AGO    = '';
    MONTH_3_AGO    = '';
    }  
    
   @RemoteAction 
   global Static string GroupProducts2(){

        if(currentAccountRecord.SAP_CUSTOMER_NUMBER__c <> null)
        CagKits = new List<Customer_Sales_History__c>();
        CagConsumables = new List<Customer_Sales_History__c>();
        CagKitsAndConsumables = new List<Customer_Sales_History__c>();
        hasData = false;
        noRecordsFound = new List<String>();
        cagKitsFamilies = '';
        cagConsumableFamilies = '';
        Transient Integer i=0;
        rank = new List<Integer>();
                    
        // 3 - Kits & Consumables : fetch all sales history records for Kits & Consumables      
       for(Customer_Sales_History__c csh : ([select   Id, PRODUCT_FAMILY__c, 
                Quantity_Current_Month__c,Quantity_1_Month_Ago__c,
                Quantity_2_Months_Ago__c,Quantity_3_Months_Ago__c,Top5KitsandConsumables__c,Top5KitsandConsumablesAvg__c
 
                    from Customer_Sales_History__c 
                    where SAP_CUSTOMER_NUMBER__c =:currentAccountRecord.SAP_CUSTOMER_NUMBER__c
                    AND PRODUCT_LINE__c IN ('CHEMISTRY', 'HEMATOLOGY', 'SNAP')
                    AND PRODUCT_GROUP__c IN ('PROCYTE CONSUMABLES', 'LASERCYTE CONSUMABLES', 'VETAUTOREAD TUBES', 'COAG CONSUMABLES',
                    'UA CONSUMABLES', 'CATALYST SLIDES', 'VETTEST CONSUMABLES', 'VETLYTE CONSUMABLES', 'VETSTAT CONSUMABLES', 'QUANTATIVE ASSAY','RAPID ASSAY')
                    AND PRODUCT_FAMILY__c != 'CAG SUMMARY'                  
                    ORDER BY Top5KitsandConsumables__c Desc LIMIT 10])){
                    CagKitsAndConsumables.add(csh);   
                    hasData = true;
                    }
                    
                    /* if(cagKitsFamilies == '')
                        cagKitsFamilies = csh.PRODUCT_FAMILY__c;
                    else
                        cagKitsFamilies += ', ' + csh.PRODUCT_FAMILY__c; 
                    } */

        if(!hasData){
        noRecordsFound = new List<String>{system.Label.No_records};
        }
        return 'Success';
    }

}