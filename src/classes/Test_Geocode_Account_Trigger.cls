@isTest(seeAllData=false)
public class Test_Geocode_Account_Trigger {
//public with sharing class Test_AccountTriggerHandler{

    static testMethod void testGeoCodeTrigger() {

        Test.startTest();

        List<Account> accList =  new List<Account>();

        Test.setMock(HttpCalloutMock.class,
                     new Test_GeocodeAddressServiceMock());

        Account testAccount = CreateTestClassData.createCustomerAccount();
        testAccount.Name = 'account1';
        testAccount.ShippingStreet = '1 Idexx Dr';
        testAccount.ShippingCity = 'Westbrook';
        testAccount.ShippingState = 'Maine';
        testAccount.ShippingCountry = 'United States';
        testAccount.ShippingPostalCode = '04092';
        accList.add(testAccount);

        Account testAccount2 = CreateTestClassData.createCustomerAccount();
        testAccount2.Name = 'account2';
        testAccount2.ShippingStreet = '60 Court St';
        testAccount2.ShippingCity = 'Auburn';
        testAccount2.ShippingState = 'Maine';
        testAccount2.ShippingCountry = 'United States';
        testAccount2.ShippingPostalCode = '04210';
        accList.add(testAccount2);
        UPSERT accList;

        // Confirm that the geocode trigger fired?

        // Now disable the trigger and try again.
        DisableSettings__c disableSettings2 = DisableSettings__c.getInstance();
        disableSettings2.Disable_Triggers__c = true;
        upsert disableSettings2;

        testAccount2.ShippingCity = 'Portland';
        update testAccount2;

        // Now enable the trigger and try again.
        disableSettings2.Disable_Triggers__c = false;
        upsert disableSettings2;

        testAccount2.ShippingCity = 'South Portland';
        update testAccount2;

        Test.stopTest();
    }


}