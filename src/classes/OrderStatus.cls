/*
 * Object to represent the status of an order - delivered, 
 * partially delivered, or not delivered
 */
public class OrderStatus {

    public Boolean submittedToSAP = false;
    public Boolean delivered = false;
    public Boolean partiallyDelivered = false;
    public String sapOrderNumber;
    
    public OrderStatus() {
        sapOrderNumber = '';
    }
    
    public OrderStatus(String orderId) {        
        sapOrderNumber = orderId;
    }
        
}