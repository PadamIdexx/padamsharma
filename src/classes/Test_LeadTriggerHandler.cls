/* Class Name   : Test_LeadTriggerHandler
 * Description  : Test Class with unit test scenarios to cover the LeadTriggerHandler class
 * Created By   : Pooja Waade
 * Created On   : 10-22-2015 
 
* Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date                   Modification ID       Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Pooja Waade             10-22-2015              1000                   Initial version
 
 */

@isTest(seeAllData=false)
public with sharing class Test_LeadTriggerHandler{
   
   static testMethod void Test_beforeUpdate() {
       
       Lead newLead = new Lead();
       newLead.LastName = 'Test Lead 1000';
       newLead.Status = 'Open';
       newLead.CurrencyIsoCode = 'EUR';
       newLead.Company = 'Test Company';
       if(Schema.sObjectType.Lead.isCreateable()){
       if(newLead!=null){
       insert newLead;
       }
       }
       System.assert(newLead.id!=null);
       
       newLead.Status = 'Qualified';
       if(newLead!=null){
       update newLead;
       }
       System.assertEquals(newLead.Status,'Qualified'); 
   }

}