public class OrderReadService {
        public String endpoint_x = '';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x = 120000;

		//Namespace information for service all.
        private String[] ns_map_type_info = new String[]{OrderReadRequest.NAMESPACE, 
            								OrderReadRequest.SF_NAMESPACE, 
            								OrderReadRequest.XMLTYPE_NAMESPACE, 
            								OrderReadRequest.SF_XMLTYPE_NAMESPACE};

		public OrderReadService(){
            //Brute force approach to getting runtime classname to use for looking up the endpoint
            String thisName = String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));            
            String endPointURL = '';
            if (!Test.isRunningTest()) { //3.8.2017
                endPointURL = WebServiceHelper.getServiceEndpoints(thisName); 
            }
            endpoint_x = endPointURL!=null ? endPointURL : '';
           
            //set headers
            inputHttpHeaders_x = new Map<String, String>();
            
            String sessionId = '';
            if (!Test.isRunningTest()) { //3.8.2017
                sessionId = WebServiceHelper.getIntegrationSessionId();
            }
            inputHttpHeaders_x.put('sessionId', sessionId);
            
        }
    
    
        public OrderReadResponse.SalesOrderReadResponse getOrder(OrderReadRequest.SalesOrderReadRequest readRequest) {
            OrderReadRequest.SalesOrderReadRequest request_x = new OrderReadRequest.SalesOrderReadRequest();
            request_x= readRequest;
            OrderReadResponse.SalesOrderReadResponse response_x;
            Map<String, OrderReadResponse.SalesOrderReadResponse> response_map_x = new Map<String, OrderReadResponse.SalesOrderReadResponse>();
            response_map_x.put('response_x', response_x); 
            if (!Test.isRunningTest()) { //3.8.2017
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        OrderReadRequest.SOAPACTION,
                        OrderReadRequest.NAMESPACE,
                        OrderReadRequest.PAYLOAD_CLASSNAME,
                        OrderReadResponse.NAMESPACE,
                        OrderReadResponse.PAYLOAD_CLASSNAME,
                        OrderReadResponse.SalesOrderReadResponse.class.getName()}
                );
            }
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    
}