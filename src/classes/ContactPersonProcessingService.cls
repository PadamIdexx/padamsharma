/*
 * Service object for Contact Process.
 * 
 * This object servers as both a model object and XML marshalling intermediary.
 */ 
 public class ContactPersonProcessingService {
     
     public class ContactPersonProcessing_OutBindingQSPort{
     
     
     
     public String endpoint_x = '';
     public Map<String,String> inputHttpHeaders_x;
     public Map<String,String> outputHttpHeaders_x;
     public String clientCertName_x;
     public String clientCert_x;
     public String clientCertPasswd_x;
     public Integer timeout_x;
     private String[] ns_map_type_info = new String[]{'http://idexxi.com/sapxi/pocbea/customer360', 'ContactPersonProcessingService'};
    
     /*
         * @DefaultContructor
         * @Description  : This is to assign default values to most of the request parameters from the custom setting RemoteSiteSettings__c
         */
        public ContactPersonProcessing_OutBindingQSPort(){
            String endPointURL = WebServiceUtil.retreiveEndpoints(WebServiceUtil.CONTACT_PROCESSING);
            /*Map<String, String> parametersMap = WebServiceUtil.retreiveParameters(WebServiceUtil.CONTACT_PROCESSING);
            if(parametersMap<> null && !parametersMap.isEmpty()){
                
                /* Kevin Lafortune 1/5/2016
				 * The CONTACT_PROCESSING url has been updated but is not reflecting that update at runtime.                  
				 * I'm hardcoding this endpoint due to an issue with Custom Settings not refreshing themselves.  I
                 * am doing this in order to get this web service working again.  We will need to sweep back through in the                  
                 * morning to revert this change.
                 */
                //endpoint_x = 'https://devbeacon.idexx.com/beacon/sfdc/SAPOrderManagment/ProxyServices/ContactPersonProcessing';
                 /* Sirisha Kodi 1/6/2016
                 * Moved the endpoint URL to custom metadata type - IDEXX Parameters 
                 */    
                	/*endpoint_x = parametersMap.containsKey(WebServiceUtil.ENDPOINT)
                                ?parametersMap.get(WebServiceUtil.ENDPOINT)
                                :''; */
               // }
            endpoint_x = endPointURL!=null ? endPointURL : '';
            inputHttpHeaders_x = new Map<String, String>();
            inputHttpHeaders_x.put('sessionId', UserInfo.getSessionId());
        }
     public ContactPersonProcessingResponse.ContactPersonsQuery_Sync ContactPersonRead(ContactPersonProcessingRequest.ContactPersonQuery_Sync request){
            //ContactPersonProcessingRequest.ContactPersonQuery_Sync request = new ContactPersonProcessingRequest.ContactPersonQuery_Sync();
            //request.CustomerID = CustomerID;
            //request.ContactTypes = ContactTypes;
            ContactPersonProcessingResponse.ContactPersonsQuery_Sync response;
            system.debug('>>>>>>>>>>> REQUEST  ' + request);
             system.debug('>>>>>>>>>>> endpoint_x  ' + endpoint_x);
            Map<String, ContactPersonProcessingResponse.ContactPersonsQuery_Sync> response_map_x = new Map<String, ContactPersonProcessingResponse.ContactPersonsQuery_Sync>();
            response_map_x.put('response', response);
            WebServiceCallout.invoke(
              this,
              request,
              response_map_x,
              new String[]{endpoint_x,
              'http://sap.com/xi/WebService/soap1.1',
              ContactPersonProcessingRequest.NAMESPACE,
              ContactPersonProcessingRequest.SAP_SERVICE_WRAPPER,
              ContactPersonProcessingResponse.NAMESPACE,
              ContactPersonProcessingResponse.SAP_SERVICE_WRAPPER,
              'ContactPersonProcessingResponse.ContactPersonsQuery_Sync'}
            );
            system.debug('>>>>>>>>>>>>>>>>>>>response_map_x   ' + response_map_x);
            response = response_map_x.get('response_x');
            system.debug('>>>>>>>>>>>>>>>>>>>response   ' + response);
            
            return response;
     }
    
     
     public ContactPersonProcessingResponse.MessagesReturn ContactPersonUpdate(ContactPersonProcessingRequest.ContactPersonUpdate_Sync updateRequest) {
            ContactPersonProcessingRequest.ContactPersonUpdate_Sync request_x = new ContactPersonProcessingRequest.ContactPersonUpdate_Sync();
            request_x = updateRequest;
            ContactPersonProcessingResponse.MessagesReturn response_x;
            Map<String, ContactPersonProcessingResponse.MessagesReturn> response_map_x = new Map<String, ContactPersonProcessingResponse.MessagesReturn>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://sap.com/xi/WebService/soap1.1',
              ContactPersonProcessingRequest.NAMESPACE,
              'ContactPersonUpdateRequest',
              ContactPersonProcessingRequest.NAMESPACE,
              'ContactPersonUpdateResponse',
              'ContactPersonProcessingResponse.MessagesReturn'}
            );
            response_x = response_map_x.get('response_x');
            
            return response_x;
        }
     
     
     public ContactPersonProcessingResponse.MessagesReturn ContactPersonCreate(ContactPersonProcessingRequest.ContactPersonCreate_Sync createRequest) {
     //String CustomerID,String ContactType,String Department,String Lastname,String Firstname,String Sex,String Title,String Language,String LanguageISO,ContactPersonProcessingResponse.CommunicationsLite Communications
            ContactPersonProcessingRequest.ContactPersonCreate_Sync request_x = new ContactPersonProcessingRequest.ContactPersonCreate_Sync();
            request_x = createRequest;
            ContactPersonProcessingResponse.MessagesReturn response_x;
            Map<String, ContactPersonProcessingResponse.MessagesReturn> response_map_x = new Map<String, ContactPersonProcessingResponse.MessagesReturn>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://sap.com/xi/WebService/soap1.1',
              'http://idexxi.com/sapxi/pocbea/customer360',
              'ContactPersonCreateRequest',
              'http://idexxi.com/sapxi/pocbea/customer360',
              'ContactPersonCreateResponse',
              'ContactPersonProcessingResponse.MessagesReturn'}
            );
            response_x = response_map_x.get('response_x');
            
            return response_x;
            //return response_x;
        }
     }
 }