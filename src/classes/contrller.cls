public with sharing class contrller {
    Public id Current_Acc_Id;
    
    public contrller(ApexPages.StandardController controller) {
        Current_Acc_Id = controller.getRecord().id;
    }
    
    public List<Test_Mix_Lab_Price__c> getrelatedCustObjRecs(){
        List<Test_Mix_Lab_Price__c> conList = New List<Test_Mix_Lab_Price__c>();
        for(Account acc:[select id,name,(select name,id, CurrencyIsoCode,Material_Description__c,
                                                                Components_list__c,Quantity_Last_3_Months__c,
                                                                Quantity_Last_6_Months__c,Quantity_Last_12_Months__c,
                                                                Average_QTY_Month__c,List_Price__c,Discount_Amount__c,Customer_Price_Calc__c,
                                                                Discount_Percent__c, Special_Price__c from Customer_Lab_Price__r) from account where id=:Current_Acc_Id]){
            for(Test_Mix_Lab_Price__c con:acc.Customer_Lab_Price__r)
                conList.add(con); 
            system.debug('ConList Results  ' + ConList);
            
        }
        return conList;
    }
    
}