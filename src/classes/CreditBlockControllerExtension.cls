/*******************************************************************************************
 *      @author         Heather Kinney
 *      @date           7.6.2016
 *      @description    Controller class for the Credit Block controller extension.
 *      Modification Log: 
 *      ------------------------------------------------------------------------------------
 *      Developer                  Date                Description
 *      ------------------------------------------------------------------------------------
 *      Heather Kinney             7.6.2016           Initial Version
 */
global class CreditBlockControllerExtension {

    private final Account acct;

	public static CreditBlockService.CUSTOMER_GETPROFILE_Response response; //public so we can generate mock responses for testing. 
    public static Account accountToTest; //for testing.
    
    public transient String accountId                 {get;set;}
    public transient String errorMessageText          {get;set;}
    public transient String errorMessageDirectiveText {get;set;}

    static final String STATUS_CREDIT_BLOCK_UNKNOWN   = 'Unknown';
    static final String STATUS_CREDIT_BLOCK_NO        = 'No';
    static final String STATUS_CREDIT_BLOCK_YES       = 'Yes';
    
    //These next two constants informs the UI if it should refresh Account details.
    static final String REFRESH_CREDIT_BLOCK_NO       = 'No';
    static final String REFRESH_CREDIT_BLOCK_YES      = 'Yes';
    
    static final String BLOCKED_X                     = 'X';
    
    private static Map<String, String> creditControlAreasMap = null;

    public CreditBlockControllerExtension(ApexPages.StandardController stdController){
        this.acct = (Account)stdController.getRecord();
        this.accountId = this.acct.Id;
        
        errorMessageText = 	System.Label.Credit_Block_Error_Message;
        errorMessageDirectiveText = System.Label.Credit_Block_Error_Message_Directive;
    }
    
    @RemoteAction
    global static CreditBlockResultsWrapper getCreditBlockStatus(String sapId, String idForAccount) {
    	//System.debug('...in getCreditBlockStatus - passed in sapID is ' + sapId);    
    	CreditBlockResultsWrapper retVal;
        retVal = getCreditBlockStatusFromSAP(sapId, idForAccount);
        return retVal;
    }
    
    public static CreditBlockResultsWrapper getCreditBlockStatusFromSAP(String sapId, String idForAccount) {
        //System.debug('...in getCreditBlockStatusFromSAP');
        CreditBlockResultsWrapper creditBlockStatus;
        CreditBlockService.CUSTOMER_GETPROFILE_Request request   = new CreditBlockService.CUSTOMER_GETPROFILE_Request();
        request.CUSTOMERNO = sapId;

        CreditBlockService.HTTPS_Port service = new CreditBlockService.HTTPS_Port();
        if(!Test.isRunningTest()){
        	response = service.CustomerProfileRead(sapId);
        }

        if (response != null) {
            //System.debug('========> getCreditBlockStatusFromSAP - response is = ' + response);
            creditBlockStatus = processCreditBlockResponse(response, idForAccount);
        }
        return creditBlockStatus;
    }
    
    public static CreditBlockResultsWrapper processCreditBlockResponse(CreditBlockService.CUSTOMER_GETPROFILE_Response responseFromService, String idForAccount) {
        //System.debug('...in processCreditBlockResponse');
        CreditBlockResultsWrapper resultsWrapper = new CreditBlockResultsWrapper();
        resultsWrapper.creditBlockStatus = STATUS_CREDIT_BLOCK_UNKNOWN;
        resultsWrapper.refreshAccountDetails = REFRESH_CREDIT_BLOCK_NO;
        
        boolean hasProcessedAtLeastOneKKBERLineItem = false;
        boolean haltProcessingDueToCreditBlockYes = false;  //allows us to short circuit proceessing, if we detect a programmatic update was made via updateAccountCreditBlockIndicator.
        String creditBlockStatus = STATUS_CREDIT_BLOCK_UNKNOWN;
        
        CreditBlockService.CREDIT_DETAILS_element creditDetails = responseFromService.CREDIT_DETAILS;
        
        if ((creditDetails != null) && (creditDetails.item != null)) {
            //System.debug('...in processCreditBlockResponse - we have credit details - ' + creditDetails);
            //We may have the possibility of multiple CREDIT_DETAILS_Item_element items being returned in creditDetails...
            //in order to determine which items being returned are legitimately on credit block, we will look at a custom 
            //metadata type (Idexx Parameters - Credit_Control_Block_Check) and only check for a BLOCKED line item if the 
            //incoming values from SAP match those in the Credit_Control_Block_Check. This logic is contained in the call
            //to isIncomingCreditControlAreaToBeValidated.
            
            for (CreditBlockService.CREDIT_DETAILS_Item_element creditDetailsLineItem : creditDetails.item){
                if (!haltProcessingDueToCreditBlockYes) {
                    if ((creditDetailsLineItem != null) && (creditDetailsLineItem.KKBER != null)) {
                        hasProcessedAtLeastOneKKBERLineItem = true;
                        //System.debug('iterating through creditDetailsLineItem...' + creditDetailsLineItem);
                        if (creditDetailsLineItem.BLOCKED != null) {
                            //System.debug('found a line item with creditDetailsLineItem.KKBER of ' + creditDetailsLineItem.KKBER + ' and creditDetailsLineItem.BLOCKED of ' + creditDetailsLineItem.BLOCKED);
                            String creditControlAreaString = String.valueOf(creditDetailsLineItem.KKBER);
                            if (isIncomingCreditControlAreaToBeValidated(creditControlAreaString)) {
                                if (creditDetailsLineItem.BLOCKED.equalsIgnoreCase(BLOCKED_X)) {
                                    //System.debug('....in KKBER with BLOCKED_X.');
                                    haltProcessingDueToCreditBlockYes = true;
                                    creditBlockStatus = STATUS_CREDIT_BLOCK_YES;
                                }  //if BLOCKED = X...
                            } //if credit control area needs to be validated...
                        } //if creditDetailsLineItem.BLOCKED != null check...
                    } //if (creditDetailsLineItem != null) && (creditDetailsLineItem.KKBER != null) check...
                } //if haltProcessingDueToCreditBlockYes...
            } //for loop - processing each credit details element...  
            
            if (hasProcessedAtLeastOneKKBERLineItem) {
                //System.debug('............. out of the loop ---- creditBlockStatus is ' + creditBlockStatus);
                //we know we've processed at least one legitimate line item that may - or may not - be credit blocked...
                if (creditBlockStatus.equalsIgnoreCase(STATUS_CREDIT_BLOCK_UNKNOWN)) {
                    //we did not encounter a credit blocked item incoming from SAP. Will set credit block status to NO.
                	creditBlockStatus = STATUS_CREDIT_BLOCK_NO;    
                }
                resultsWrapper.creditBlockStatus = creditBlockStatus; 
                boolean refreshUI = updateAccountCreditBlockIndicator(idForAccount, creditBlockStatus);
                if (refreshUI) {
                	resultsWrapper.refreshAccountDetails = REFRESH_CREDIT_BLOCK_YES;    
                }
            } //hasProcessedAtLeastOneKKBERLineItem check...
 
        } //if ((creditDetails != null) && (creditDetails.item != null)) check...
        
        return resultsWrapper;
    }
    
    public static boolean updateAccountCreditBlockIndicator(String idForAccount, String statusOfCreditBlockFromSAP) {
        //System.debug('in updateAccountCreditBlockIndicator...');
        boolean wasAccountCreditBlockFlagUpdated = false; //flip to true if an update was made to the account's credit block status. 
        if (idForAccount != null) {
            Account accountToUpdate;
            try {
                if(Test.isRunningTest()){
                    accountToUpdate = accountToTest;
                } else {
            		accountToUpdate = [SELECT Order_Block_Indicator__c FROM Account WHERE Id =:idForAccount];
                }
            } catch (Exception ex) {
                SystemLoggingService.log(ex);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Generic_Error_Message));
            }
            
            try {
                if (accountToUpdate != null) {
                    String existingOrderBlockIndicator = accountToUpdate.Order_Block_Indicator__c;
                    System.debug('existingOrderBlockIndicator is ' + existingOrderBlockIndicator);
                    System.debug('statusOfCreditBlockFromSAP  is ' + statusOfCreditBlockFromSAP);
                    if (existingOrderBlockIndicator.equalsIgnoreCase(statusOfCreditBlockFromSAP)) {
                        //Don't do anything - no update required as the indicator is the same...
                    } else { 
                        //We have detected a change...will need to update the Account's Order_Block_Indicator__c
                        accountToUpdate.Order_Block_Indicator__c = statusOfCreditBlockFromSAP; 
                        update accountToUpdate;
                        wasAccountCreditBlockFlagUpdated = true;
                        //System.debug('accountToUpdate.Order_Block_Indicator__c is now ' + accountToUpdate.Order_Block_Indicator__c);
                    }  
                }
            } catch (Exception ex1) {
                SystemLoggingService.log(ex1);
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Generic_Error_Message));    
            }  
        }

        return wasAccountCreditBlockFlagUpdated;
    }
    
    public void setResponse(CreditBlockService.CUSTOMER_GETPROFILE_Response resp) {
    	response = resp;  
    }
    
    public void setAccountToUpdate(Account testAccount) {
    	accountToTest = testAccount;    
    }
    
    
    private static void buildCreditControlAreasToCheck() {
        Map<String, String> parametersMap = new Map<String, String>();  
        parametersMap = webserviceUtil.retreiveParameters(WebServiceUtil.CREDIT_CONTROL_CHECK);
        
        if (parametersMap != null) {
        	String stringListOfCreditControlAreas = parametersMap.containsKey(WebServiceUtil.CREDITCONTROLBLOCKCHECK) 
                	? parametersMap.get(WebServiceUtil.CREDITCONTROLBLOCKCHECK) : null;
            if (stringListOfCreditControlAreas != null) {
                creditControlAreasMap = new Map<String, String>();
            	//see if we have only one value, or a comma separated list of values (which we'll iterate)...
                if (stringListOfCreditControlAreas.contains(',')) {
                	String[] strArrOfValues = stringListOfCreditControlAreas.split(',');
                    for (integer i=0;i<strArrOfValues.size();i++) {
                       String mapVal = strArrOfValues[i];
                       creditControlAreasMap.put(mapVal, mapVal);
                    }
                } else {
                	creditControlAreasMap.put(stringListOfCreditControlAreas,stringListOfCreditControlAreas);	    
                }	    
            }
        }
    }
    
    private static boolean isIncomingCreditControlAreaToBeValidated(String creditControlAreaKey) {
        // bump incoming value against the already-built creditControlAreasMap
        boolean retVal = false; 

        if (creditControlAreasMap == null)
            buildCreditControlAreasToCheck();
        
        if (creditControlAreasMap != null) {
            retVal= creditControlAreasMap.containsKey(creditControlAreaKey) ? true : false;
        }
        return retVal;
    }
    
    global class CreditBlockResultsWrapper { 
        String creditBlockStatus;
        String refreshAccountDetails;
    }

}