@isTest
public class TestLabImagesControllerExtension {
    
    public static testMethod void testLabImagesContAntrim(){
        
        PageReference testPage = new PageReference('/apex/labImages');
        testPage.getParameters().put('guid','12344555');
        testPage.getParameters().put('reqId','abcd1234');
        testPage.getParameters().put('type','Antrim');
        
        
        Test.setCurrentPage(testPage);
        
        Test_genericMock mock = new Test_GenericMock();
        mock.callType = 'images';
        
        LabImagesController cont = new LabImagesController();
        
        Test.setMock(HttpCalloutMock.class, mock);
        LabImagesCallout call = new LabImagesCallout();
        test.startTest();
        
        LabImagesController.request();
        
        test.stopTest();
        
    }
    public static testMethod void testLabImagesContLynxx(){
        
        PageReference testPage = new PageReference('/apex/labImages');
        testPage.getParameters().put('guid','12344555');
        testPage.getParameters().put('reqId','abcd1234');
        testPage.getParameters().put('type','Lynxx');
        
        
        Test.setCurrentPage(testPage);
        Test_genericMock mock = new Test_GenericMock();
        mock.callType = 'images';
        
        LabImagesController cont = new LabImagesController();
        
        Test.setMock(HttpCalloutMock.class, mock);
        LabImagesCallout call = new LabImagesCallout();
        test.startTest();
        LabImagesController.request();
        String xmlString = '<Image>imageString</Image>';
        XmlStreamReader xsr = new XmlStreamReader(xmlString);
        
        LabImagesController.ParseImg(xsr);
        test.stopTest();
        
    }
    
}