@isTest
public class TestLabOrdersControllerExtension{
    
    public static testmethod void testWebService(){
        List<LabOrders.labOrder> mockList = new List<LabOrders.labOrder>();
        
        Account testAccount = new Account();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = 'Customer Testing Account';
        testAccount.CurrencyISOCode = 'USD';
        testAccount.HIN__c = 'Test123';
        testAccount.SAP_Customer_Number__c = '0'+String.valueOf(Math.random()).subString(12);
        insert testAccount;
        
        Lims__c lims = new Lims__c(LIMS_Practice_ID__c = '1345',Name = 'test',Account__c=testAccount.Id);
        Lims__c lims2 = new Lims__c(LIMS_Practice_ID__c = '8181',Name = 'test',Account__c=testAccount.Id);
        
        insert lims;
        insert lims2;
        
        Test.startTest();
        staticResource sr = new staticResource();

        PageReference page = ApexPages.currentPage();
        page.getParameters().put('id', testAccount.Id);
        
        test.setCurrentPage(page);    
        
        ApexPages.StandardController stdAct = new
            ApexPages.StandardController(testAccount);
        
        Test.setMock(WebServiceMock.class, new Test_AsyncLabOrders_Mock());
        
        LabOrdersControllerExtension testCont =
            new LabOrdersControllerExtension(stdAct);
        
        Continuation cont = (Continuation)testCont.startRequest();
        Map<String, HttpRequest> requests = cont.getRequests();
        
        try{    
            sr = [Select Body From StaticResource Where Name = 'LabOrdersResult' LIMIT 1];
        }
        catch(system.DmlException ex){
            
        }
        HttpResponse response = new HttpResponse();
        
        response.setBodyAsBlob(sr.Body);
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        Object result = Test.invokeContinuationMethod(testCont, cont);
       // System.debug(testCont.result);
        List<String> lList = new List<String>();
        lList.add('1345');
        lList.add('8181');
        testCont.limsIds = lList;
      //  testCont.endDate = date.today();
      //  testCont.startDate = date.today();
      //  testCont.image = 'FEER12423';
       // testCont.gId = 'AR1248991';
        boolean val = testCont.validate();
        System.debug('valid ' + val);

        testCont.processResponse();
        
        testCont.fetchGuids();
        
        System.debug(testCont);
        Test.stopTest();
        
    }
    
    public static testmethod void testWebService2(){
        List<LabOrders.labOrder> mockList = new List<LabOrders.labOrder>();
        
        Account testAccount = new Account();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = 'Customer Testing Account';
        testAccount.CurrencyISOCode = 'USD';
        testAccount.HIN__c = 'Test123';
        testAccount.SAP_Customer_Number__c = '0'+String.valueOf(Math.random()).subString(12);
        insert testAccount;
        
        Lims__c lims = new Lims__c(LIMS_Practice_ID__c = '1345',Name = 'test',Account__c=testAccount.Id);
        Lims__c lims2 = new Lims__c(LIMS_Practice_ID__c = '8181',Name = 'test',Account__c=testAccount.Id);
        
        insert lims;
        insert lims2;
        
        Test.startTest();
        PageReference page = ApexPages.currentPage();
        page.getParameters().put('id', testAccount.Id);
        
        test.setCurrentPage(page);    
        
        ApexPages.StandardController stdAct = new
            ApexPages.StandardController(testAccount);
        
        
        Test.setMock(WebServiceMock.class, new Test_AsyncLabOrders_Mock());
        
        LabOrdersControllerExtension testCont =
            new LabOrdersControllerExtension(stdAct);
        
        List<String> lList = new List<String>();
        lList.add('1345');
        lList.add('8181');
        testCont.limsIds = lList;
      //  testCont.endDate = date.today();
       // testCont.startDate = date.today();
       // testCont.image = 'FEER12423';
       // testCont.gId = 'AR1248991';
        boolean val = testCont.validate();
     //   System.debug('valid ' + val + ' '+ testCont.errorMsg);
        
      //  testCont.result = mockList;  
        
        testCont.labOrdersFuture = LabOrderTestDataGenerator.createAsyncLabOrderCollectionResponse();
        
        System.debug('labOrdersFuture ' + testCont.labOrdersFuture);
        testCont.startRequest();
        testCont.processResponse();
        
        testCont.fetchGuids();
        
        System.debug(testCont);
        Test.stopTest();
    }
    public static testmethod void negativeDateTest(){
        List<LabOrders.labOrder> mockList = new List<LabOrders.labOrder>();
        
        Account testAccount = new Account();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = 'Customer Testing Account';
        testAccount.CurrencyISOCode = 'USD';
        testAccount.HIN__c = 'Test123';
        testAccount.SAP_Customer_Number__c = '0'+String.valueOf(Math.random()).subString(12);
        insert testAccount;
        
        Lims__c lims = new Lims__c(LIMS_Practice_ID__c = '1345',Name = 'test',Account__c=testAccount.Id);
        Lims__c lims2 = new Lims__c(LIMS_Practice_ID__c = '8181',Name = 'test',Account__c=testAccount.Id);
        
        insert lims;
        insert lims2;
        
        Test.startTest();
        PageReference page = ApexPages.currentPage();
        page.getParameters().put('id', testAccount.Id);
        
        test.setCurrentPage(page);    
        
        ApexPages.StandardController stdAct = new
            ApexPages.StandardController(testAccount);
        
        
        Test.setMock(WebServiceMock.class, new Test_AsyncLabOrders_Mock());
        
        LabOrdersControllerExtension testCont =
            new LabOrdersControllerExtension(stdAct);
        
        List<String> lList = new List<String>();
        lList.add('1345');
        lList.add('00001345');
        testCont.limsIds = lList;
      //  testCont.endDate = date.today().addDays(1000);
      //  testCont.startDate = date.today();
        boolean val = testCont.validate();
        System.debug('valid ' + val);
        
       // testCont.result = mockList;  
        
        testCont.labOrdersFuture = LabOrderTestDataGenerator.createAsyncLabOrderCollectionResponse();
        
        System.debug('labOrdersFuture ' + testCont.labOrdersFuture);
        testCont.startRequest();
        
        
        System.debug(testCont);
        Test.stopTest();
     //   System.assertEquals('you must select a date range of 45 days or less', testCont.errorMsg);
        
        
    }
    public static testmethod void noDateTest(){
        List<LabOrders.labOrder> mockList = new List<LabOrders.labOrder>();
        
        Account testAccount = new Account();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = 'Customer Testing Account';
        testAccount.CurrencyISOCode = 'USD';
        testAccount.HIN__c = 'Test123';
        testAccount.SAP_Customer_Number__c = '0'+String.valueOf(Math.random()).subString(12);
        insert testAccount;
        
        
        Test.startTest();
        PageReference page = ApexPages.currentPage();
        page.getParameters().put('id', testAccount.Id);
        
        test.setCurrentPage(page);    
        
        
        ApexPages.StandardController stdAct = new
            ApexPages.StandardController(testAccount);
        
        
        Test.setMock(WebServiceMock.class, new Test_AsyncLabOrders_Mock());
        
        LabOrdersControllerExtension testCont =
            new LabOrdersControllerExtension(stdAct);
        List<String> lList = new List<String>();
        lList.add('1345');
        lList.add('8181');
        testCont.limsIds = lList;
     //   testCont.endDate = null;
     //   testCont.startDate = null;
        boolean val = testCont.validate();
        System.debug('valid ' + val);
        
      //  testCont.result = mockList;  
        
        testCont.labOrdersFuture = LabOrderTestDataGenerator.createAsyncLabOrderCollectionResponse();
        
        System.debug('labOrdersFuture ' + testCont.labOrdersFuture);
        testCont.startRequest();
        
        System.debug(testCont);
        Test.stopTest();
        
    }
    
    public static testmethod void negativeLimsTest(){
        List<LabOrders.labOrder> mockList = new List<LabOrders.labOrder>();
        
        Account testAccount = new Account();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = 'Customer Testing Account';
        testAccount.CurrencyISOCode = 'USD';
        testAccount.HIN__c = 'Test123';
        testAccount.SAP_Customer_Number__c = '0'+String.valueOf(Math.random()).subString(12);
        insert testAccount;
        
        
        Test.startTest();
        PageReference page = ApexPages.currentPage();
        page.getParameters().put('id', testAccount.Id);
        
        test.setCurrentPage(page);    
        
        ApexPages.StandardController stdAct = new
            ApexPages.StandardController(testAccount);
        
        
        Test.setMock(WebServiceMock.class, new Test_AsyncLabOrders_Mock());
        
        LabOrdersControllerExtension testCont =
            new LabOrdersControllerExtension(stdAct);
        
        List<String> lList = new List<String>();
       // testCont.endDate = date.today();
       // testCont.startDate = date.today();
        boolean val = testCont.validate();
        System.debug('valid ' + val);
        
      //  testCont.result = mockList;  
        
        testCont.labOrdersFuture = LabOrderTestDataGenerator.createAsyncLabOrderCollectionResponse();
        
        System.debug('labOrdersFuture ' + testCont.labOrdersFuture);
        testCont.startRequest();
        
        
        System.debug(testCont);
        Test.stopTest();
        
      //  System.assertEquals('No records to display: No associated LIMS ID for this Account.', testCont.errorMsg);
        
    }
    
}