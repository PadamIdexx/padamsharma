/* Class Name  : Test_ContactCTITriggerHandler
 * Description : Test class with unit tests that covers the ContactCTITriggerHandler class.
 * Created By  : Heather Kinney
 * Created On  : 01-20-2016
 *
 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer              Date                   Modification ID       Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Heather Kinney         01-22-2016             n/a                   Initial version
 *                                          
*/
@isTest(SeeAllData = False)     
public With Sharing class Test_ContactCTITriggerHandler {
    
    static testMethod void testSanitizePhoneNumber() {
        //Salesforce stores phone numbers with embedded formatting/vanity characters.
        //This test excercises the sanitization routine.
        
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = getCurrentUser();
        system.runAs(currentUser){
            
            test.startTest();
            
            String testPhoneNumberWithFormatting = '(207) 775-4321';
           
            ContactCTITriggerHandler contactCTIHandler = new ContactCTITriggerHandler();
            String sanitizedPhoneNumber = contactCTIHandler.sanitizePhoneNumber(testPhoneNumberWithFormatting);
            
            System.debug('sanitizedPhoneNumber = ' + sanitizedPhoneNumber);
            
            System.assert(true, sanitizedPhoneNumber.isNumeric());
            
            test.stopTest();
            
        }
        
    }
    
    static testMethod void testSanitizeContactPhoneNumbers() {
        //This test will exercise formatting of phone numbers on the Contact object.
        
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = getCurrentUser();
        system.runAs(currentUser){
        
            test.startTest();
            
            Account acc = createCustomerAccount('Test0', 'test0', '1111111113');          
            Contact contact = new Contact();
            contact.FirstName = 'TestCTIFName';
            contact.LastName = 'TestCTILName';
            contact.AccountId = acc.Id;

            
            contact.AssistantPhone = '(207) 775-4319';
            contact.Business_Phone__c = '(207) 775-4320';
            contact.HomePhone = '(207) 775-4321';
            contact.MobilePhone = '(207) 775-4322';
            contact.OtherPhone = '(207) 775-4323';
            contact.Phone = '(207) 775-4324';
            contact.Phone__c = '(207) 775-4325';

            ContactCTITriggerHandler contactCTIHandler = new ContactCTITriggerHandler();
            contactCTIHandler.sanitizeContactPhoneNumbers(contact); 
            
            System.debug('sanitized assistantPhone = ' + contactCTIHandler.sanitizedNums.assistantPhone);
            System.debug('sanitized businessPhone = ' + contactCTIHandler.sanitizedNums.businessPhone);
            System.debug('sanitized homePhone = ' + contactCTIHandler.sanitizedNums.homePhone);
            System.debug('sanitized mobilePhone = ' + contactCTIHandler.sanitizedNums.mobilePhone);
            System.debug('sanitized otherPhone = ' + contactCTIHandler.sanitizedNums.otherPhone);
            System.debug('sanitized phone = ' + contactCTIHandler.sanitizedNums.phone);
            System.debug('sanitized phoneCustom = ' + contactCTIHandler.sanitizedNums.phoneCustom);
            
            System.assertNotEquals(0, contactCTIHandler.sanitizedNums.assistantPhone.length());
            System.assert(true, contactCTIHandler.sanitizedNums.assistantPhone.isNumeric());
            
            System.assertNotEquals(0, contactCTIHandler.sanitizedNums.businessPhone.length());
            System.assert(true, contactCTIHandler.sanitizedNums.businessPhone.isNumeric());
            
            System.assertNotEquals(0, contactCTIHandler.sanitizedNums.homePhone.length());
            System.assert(true, contactCTIHandler.sanitizedNums.homePhone.isNumeric());
            
            System.assertNotEquals(0, contactCTIHandler.sanitizedNums.mobilePhone.length());
            System.assert(true, contactCTIHandler.sanitizedNums.mobilePhone.isNumeric());
            
            System.assertNotEquals(0, contactCTIHandler.sanitizedNums.otherPhone.length());
            System.assert(true, contactCTIHandler.sanitizedNums.otherPhone.isNumeric());
            
            System.assertNotEquals(0, contactCTIHandler.sanitizedNums.phone.length());
            System.assert(true, contactCTIHandler.sanitizedNums.phone.isNumeric());
            
            System.assertNotEquals(0, contactCTIHandler.sanitizedNums.phoneCustom.length());
            System.assert(true, contactCTIHandler.sanitizedNums.phoneCustom.isNumeric());
            
            test.stopTest();    
        }
        
    
    }
    
    static testMethod void testOnAfterInsert() {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = getCurrentUser();
        system.runAs(currentUser){
            
            test.startTest();
            
            Account acc = createCustomerAccount('Test1', 'test1', '1111111111'); 
            Contact existingContact = createExistingContact('LNtest-1', acc);
            List<Contact> contacts = createContactListContainingOneContact(existingContact);
            
            Phone_Lookup__c phoneLookup = createOnePhoneLookupRecord('2077754321', acc);
            upsert phoneLookup;
            
            ContactCTITriggerHandler contactCTIHandler = new ContactCTITriggerHandler();
            
            contactCTIHandler.onAfterInsert(contacts);
            
            test.stopTest();
        }
        
    
    }
    
    static testMethod void testOnAfterUpdate() {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = getCurrentUser();
        system.runAs(currentUser){
            
            test.startTest();
            
            Account acc = createCustomerAccount('Test1', 'test1', '1111111112');
            Contact existingContact = createExistingContact('LNtest-2', acc);
            insert existingContact;
            
            Contact contact2 = createExistingContact('LNTest-6', acc);
            insert contact2;
            List<Contact> contacts = createContactListContainingOneContact(contact2);
            
            Phone_Lookup__c phoneLookup = createOnePhoneLookupRecord('2077750000', acc);
            upsert phoneLookup;

            ContactCTITriggerHandler contactCTIHandler = new ContactCTITriggerHandler();
            
            Map<Id,Contact> oldMapContacts = new Map<Id,Contact>();
            oldMapContacts.put(existingContact.Id, existingContact);

            contactCTIHandler.onAfterUpdate(contacts, oldMapContacts);
            
            test.stopTest();
        }
 
    }
    
    static testMethod void testAssembleSoftDeleteRecords() {
    //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = getCurrentUser();
        system.runAs(currentUser){
            
            test.startTest();
            
            Account acc = createCustomerAccount('Test1', 'test1', '1111111112');
            Contact contact1 = createExistingContact('LNTest-9', acc);
            insert contact1;
            
            Contact contact2 = createExistingContact('LNTest-3', acc);
            insert contact2;
                
            ContactCTITriggerHandler contactCTIHandler = new ContactCTITriggerHandler();

            contactCTIHandler.assembleSoftDeleteRecords(contact1, contact2);
            
            test.stopTest();
        }   
    
    }
    
    static testMethod void testAssembleSoftDeleteRecords1() {
    //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = getCurrentUser();
        system.runAs(currentUser){
            
            test.startTest();
            
            Account acc = createCustomerAccount('Test1', 'test1', '1111111112');
            //Contact contact1 = createExistingContact('LNTest-9', acc);
            //insert contact1;
            Contact contact1 = new Contact();
            Contact contact2 = createExistingContact('LNTest-3', acc);
            insert contact2;
                
            ContactCTITriggerHandler contactCTIHandler = new ContactCTITriggerHandler();

            contactCTIHandler.assembleSoftDeleteRecords(contact1, contact2);
            
            test.stopTest();
        }   
    
    }
    
    static testMethod void testPerformPhoneLookupDelete() {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = getCurrentUser();
        system.runAs(currentUser){
  
            test.startTest();
            
            //onBeforeDelete
            
            Account acc = createCustomerAccount('Test1', 'test1', '1111111112');
            Contact existingContact = createExistingContact('LNtest-2', acc);
            insert existingContact;
            
            Contact contact2 = createExistingContact('LNTest-6', acc);
            insert contact2;
            List<Contact> contacts = createContactListContainingOneContact(contact2);
            
            Phone_Lookup__c phoneLookup = createOnePhoneLookupRecord('2077750000', acc);
            upsert phoneLookup;

            ContactCTITriggerHandler contactCTIHandler = new ContactCTITriggerHandler();
            
            Map<Id,Contact> oldMapContacts = new Map<Id,Contact>();
            oldMapContacts.put(existingContact.Id, existingContact);

            contactCTIHandler.onBeforeDelete(oldMapContacts);
            
            
            test.stopTest();
        }
    }
    
    
    private static Account createCustomerAccount(String name, String hin, String sapId) {     
        Account testAccount = new Account();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = name;
        testAccount.CurrencyISOCode = 'USD';
        testAccount.HIN__c = hin;
        testAccount.SAP_Customer_Number__c = sapId;
        insert testAccount;
        return testAccount;
    }
    
    private static User getCurrentUser() {
        //User currentUser = CreateTestClassData.reusableUser('System Administrator','admin2');
        User currentUser = CreateTestClassData.reusableUser('Inside Sales Rep-CAG','Caleb-Bi');
        return currentUser;
    }
    
    private static Phone_Lookup__c createOnePhoneLookupRecord(String phoneNumber, Account acc) {

        Phone_Lookup__c phoneLookup = new Phone_Lookup__c();
            
        phoneLookup.Account__c = acc.Id; 
        phoneLookup.Is_Active__c = true;
        phoneLookup.Match_Number__c = phoneNumber.substring(0,6);
        phoneLookup.Phone_Number__c = phoneNumber;
        phoneLookup.SAP_Customer_Number__c = acc.SAP_Customer_Number__c;
        
        return phoneLookup;
    
    }
    
    private static Contact createExistingContact(String lastName, Account acc) {
        //This represents a contact that already existed. Will be used to test an update scenario, where we need
        //to possibly soft-delete a row in Phone_Lookup__c.
          
        Contact contact = new Contact();
        contact.FirstName = 'TestCTIFName-1';
        contact.LastName = lastName;
        contact.AccountId = acc.Id;

        contact.AssistantPhone = '(207) 775-4320';
        contact.Business_Phone__c = '(207) 775-4321';
        contact.HomePhone = '(207) 775-4322';
        contact.MobilePhone = '(207) 775-4323';
        contact.OtherPhone = '(207) 775-4324';
        contact.Phone = '(207) 775-4325';
        contact.Phone__c = '(207) 775-4326';
 
        return contact;   
    
    }
    
    private static List<Contact> createContactListContainingOneContact(Contact contact) {
        
        //This method creates one contact and puts it in a list.
        // required to exercise the logic in the companion Handler class, which from a trigger - passes in a List of Contacts.

        List<Contact> contacts = new List<Contact>();
        contacts.add(contact);
        
        return contacts;

    }
}