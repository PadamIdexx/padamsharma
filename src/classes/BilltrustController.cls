public with sharing class BilltrustController {

    private static final String BT_CLIENT_GUID = retrieveSettings('BT_CLIENT_GUID');
    private static final String BT_SSO_URL = retrieveSettings('BT_SSO_URL');
    private static final String BT_ENCRYPT_URL = retrieveSettings('BT_ENCRYPT_URL');
    private static final String BT_ENCRYPT_KEY = retrieveSettings('BT_ENCRYPT_KEY');
    private static final String BT_STATEMENT_URL = retrieveSettings('BT_STATEMENT_URL');
    private static final String BT_SALES_USER_NAME = retrieveSettings('BT_SALES_USER_NAME');
    private static final String BT_SALES_FIRST_NAME = retrieveSettings('BT_SALES_FIRST_NAME');
    private static final String BT_SALES_LAST_NAME = retrieveSettings('BT_SALES_LAST_NAME');
    private static final String BT_SALES_GROUP = retrieveSettings('BT_SALES_GROUP');
    private static final String BT_SALES_EMAIL = retrieveSettings('BT_SALES_EMAIL');

    public String billtrustURL {get; set;}
    public String sapId {get; set;}
    private Account currentAccountRecord;

    public BilltrustController(ApexPages.StandardController controller) {
        currentAccountRecord = (Account)controller.getRecord();
        if (currentAccountRecord <> null && currentAccountRecord.Id <> null) {
            currentAccountRecord = [select Id, SAP_Number_without_Leading_Zeros__c from account where Id = :currentAccountRecord.Id];
            this.sapId = currentAccountRecord.SAP_Number_without_Leading_Zeros__c;
        } else {
            this.sapId = ApexPages.CurrentPage().getParameters().get('sapId');
        }
        createBilltrustURL();
    }

    private void createBilltrustURL() {
        //TODO: Move to custom settings
        String btData = 'EA=' + BT_SALES_EMAIL;
        if (!String.isEmpty(BT_SALES_USER_NAME)) {
            btData += ';UN=' + BT_SALES_USER_NAME;
        }
        if (!String.isEmpty(BT_SALES_FIRST_NAME)) {
            btData += ';FN=' + BT_SALES_FIRST_NAME;
        }
        if (!String.isEmpty(BT_SALES_LAST_NAME)) {
            btData += ';LN=' + BT_SALES_LAST_NAME;
        }
        btData += ';GP=' + BT_SALES_GROUP;

        //Timestamp is good for one hour from the time of single sign on.
        DateTime dt = DateTime.now();
        String dateStr = dt.format('yyyy-MM-dd HH:mm:ss',
                                   'America/New_York');
        btData += ';TS=' + dateStr;

        btData = encrypt(btData);

        String url = BT_SSO_URL + '?BTDATA="' + btData + '"&CG=' + BT_CLIENT_GUID + '&ETYPE=1';
        this.billtrustURL = url;
    }

    public String encrypt(String str) {
        HttpRequest req = new HttpRequest();

        req.setEndpoint(BT_ENCRYPT_URL);
        req.setHeader('Content-Type', 'application/xml');
        req.setHeader('sessionId', UserInfo.getSessionId());

        req.setMethod('POST');
        req.setBody('<TransferBean><key>' + BT_ENCRYPT_KEY + '</key><lstRequests><invoiceNumber>N/A</invoiceNumber><requestUri>' + str + '</requestUri></lstRequests></TransferBean>');

        Http http = new Http();
        HTTPResponse res = http.send(req);

        String body = res.getBody();

        JSONParser parser = JSON.createParser(body);
        String encryptedString = '';
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                (parser.getText() == 'requestUri')) {
                // Get the value.
                parser.nextToken();
                encryptedString = parser.getText();
            }
        }
        return encryptedString;
    }

    public static String retrieveSettings(String sIntegratioName) {
        Billtrust_Settings__c cs = Billtrust_Settings__c.getInstance(sIntegratioName);
        if (cs != null) {
            return cs.Value__c;
        } else {
            return null;
        }
    }
}