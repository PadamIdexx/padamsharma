@isTest
public class LabOrderTestDataGenerator {
    
    public static LabOrders.labReportSearchRequest_element createLabOrdersRequest() {
        LabOrders.labReportSearchRequest_element req = new LabOrders.labReportSearchRequest_element();
        
        String[] cust = new List<String>();
        cust.add('0000001345');
        
        req.customerCode = cust;
        DateTime dt = Datetime.newInstance(2015, 12, 01);
        req.minimumOrderDate = dt;
        
        String[] contexts = new List<String>();
        contexts.add('1');
        
        req.internalContextID = contexts;
        req.startRow = 1;
        req.maxRows = 20;
        req.ascending = false;
        
        return req;
    }
    
    
    public static LabOrders.getLabOrderCollectionsResponse_element createLabOrderCollectionResponse() {
        
        LabOrders.getLabOrderCollectionsResponse_element resp = new LabOrders.getLabOrderCollectionsResponse_element();
        
        resp.labOrderReport = getLabOrderReportElement();  
        
        return resp;
    }
    
    public static AsyncLabOrders.getLabOrderCollectionsResponse_elementFuture createAsyncLabOrderCollectionResponse() {
        
        AsyncLabOrders.getLabOrderCollectionsResponse_elementFuture resp = new AsyncLabOrders.getLabOrderCollectionsResponse_elementFuture();

        return resp;
    }

    public static LabOrders.getFilteredOrderCountResponse_element createFilteredOrderCountResponse() {
        LabOrders.getFilteredOrderCountResponse_element resp = new LabOrders.getFilteredOrderCountResponse_element();
        
        resp.return_x = Long.valueOf('20');
        
        return resp;
    }
    
    public static LabOrders.getLabOrderDetailsResponse_element createLabOrderDetailsResponse() {
        
        LabOrders.getLabOrderDetailsResponse_element resp = new LabOrders.getLabOrderDetailsResponse_element();        
        
        resp.labOrderReport = getLabOrderReportElement();  
        
        return resp;
    }
    
    /*
* Helper method used by all lab orders mock response elements
*/ 
    private static LabOrders.labOrderReport_element getLabOrderReportElement() {
        LabOrders.labOrderReport_element l = new LabOrders.labOrderReport_element();
        
        LabOrders.labOrders_element o = new LabOrders.labOrders_element();
        
        LabOrders.vendor v = new LabOrders.vendor();
        v.vendorCode='IDEXX';
        v.vendorName='IDEXX Reference Laboratories, Inc.';
        
        l.vendor = v;
        l.labOrders = o;
        
        LabOrders.labOrder[] orders = new List<LabOrders.labOrder>();
        
        LabOrders.labOrder theOrder = new LabOrders.labOrder();
        LabOrders.labOrder theOrder2 = new LabOrders.labOrder();
        theOrder.internalOrderId = '205767955';
        theOrder.dateReceived = datetime.now();
  		theOrder2.internalOrderId = '205767955';
        theOrder2.dateReceived = datetime.now();
        //LIMS
        LabOrders.lims lims = new LabOrders.lims();                
        lims.internalLimsID = '1';
        lims.fullName = 'Antrim';
        lims.shortName = 'Antrim';        
        //LYNXX
        LabOrders.lims lims2 = new LabOrders.lims();                
        lims2.internalLimsID = '16';
        lims2.fullName = 'LYNXX';
        lims2.shortName = 'LYNXX';  
        theOrder.lims = lims;
        theOrder2.lims = lims2;
        //Context        
        LabOrders.context c = new LabOrders.context();
        
        c.internalContextID = '1';
        c.code = 'Antrim';
        c.name = 'Antrim';
        c.activeStatusIndicator = 'Y';
        LabOrders.context c2 = new LabOrders.context();
        //LYNXX
        c2.internalContextID = '16';
        c2.code = 'LYNXX';
        c2.name = 'LYNXX';
        c2.activeStatusIndicator = 'Y';
        theOrder.context = c;        
        theOrder2.context = c2;   
        //Lab
        LabOrders.lab lab = new LabOrders.lab();
        lab.internalLabID = '47';
        lab.labCode = 'L';
        
        theOrder.lab = lab;
        theOrder2.lab = lab;
        
        //Lab Order Source
        LabOrders.labOrderSource source = new LabOrders.labOrderSource();
        source.internalOrderSourceApplicationID = '-1';
        source.internalOrderSourceID = '-1';
        source.internalOrderTrackingID = '-1';
        source.trackingCode = 'N/A';
        source.description = 'Not Applicable';
        source.name ='NA';
        
        theOrder.labOrderSource = source;
        theOrder2.labOrderSource = source;
        
        //patient
        theOrder.patient = getPatient();
        theOrder2.patient = getPatient();
        
        //practice
        theOrder.practice = getPractice1();        
        theOrder2.practice = getPractice2();
        
        //specimen
        LabOrders.specimen specimen = new LabOrders.specimen();
        specimen.collectionDate = Datetime.newInstance(2015, 12, 01);
        specimen.handler = 'HOA';
        
        theOrder.specimen = specimen;
        theOrder2.specimen = specimen;
        
        //lab orders collection
        theOrder.collections = getLabOrdersCollection();
        theOrder2.collections = getLabOrdersCollection();
        
        orders.add(theOrder);
        orders.add(theOrder2);
        
        o.labOrder = orders;
        
        return l;
    }
    
    
    private static LabOrders.patient getPatient() {
        LabOrders.patient p = new LabOrders.patient();
        p.internalPatientID = '99999';
        p.name = 'FLUFFY';        
        
        LabOrders.patientOwner po = new LabOrders.patientOwner();
        po.lastName = 'SMITH';        
        
        p.ownerName= po;
        
        LabOrders.species spec = new LabOrders.species();
        spec.code = 'CANINE';
        spec.name = 'CANINE';
        
        p.species = spec;
        
        LabOrders.breed b = new LabOrders.breed();
        b.name = 'COCKAPOO';
        
        p.breed = b;
        
        LabOrders.gender g = new LabOrders.gender();
        g.code = 'F';
        g.name = 'FEMALE';
        
        p.gender = g;
        
        LabOrders.age a = new LabOrders.age();
        a.numericAge = 5;
        a.ageUOM = 'Y';
        
        p.age = a;
        
        return p;
    }
    
    private static LabOrders.practice getPractice1() {
        LabOrders.practice p = new LabOrders.practice();
        
        p.internalPracticeID = '123';
        p.code = '1345';
        p.sapCustomerNumber = '0000023284';
        p.name = 'THE PET CLINIC - HAWAII';
        
        String[] addresses = new List<String>();
        
        LabOrders.addresses_element a = new LabOrders.addresses_element();        
        p.addresses = a;
        
        LabOrders.phones_element phones = new LabOrders.phones_element();
        p.phones = phones;
        
        LabOrders.contacts_element c = new LabOrders.contacts_element();
        List<LabOrders.practiceContact> pList = new List<LabOrders.practiceContact>();
        LabOrders.practiceContact pCon = new LabOrders.practiceContact();
        pCon.surname = 'MisterTester';
        pList.add(pCon);
        
        c.contact = pList;
        
        p.contacts = c;
        
        return p;
    }
      private static LabOrders.practice getPractice2() {
        LabOrders.practice p = new LabOrders.practice();
        
        p.internalPracticeID = '123';
        p.code = '8181';
        p.sapCustomerNumber = '0000023284';
        p.name = 'THE PET CLINIC - HAWAII';
        
        String[] addresses = new List<String>();
        
        LabOrders.addresses_element a = new LabOrders.addresses_element();        
        p.addresses = a;
        
        LabOrders.phones_element phones = new LabOrders.phones_element();
        p.phones = phones;
        
        LabOrders.contacts_element c = new LabOrders.contacts_element();
        List<LabOrders.practiceContact> pList = new List<LabOrders.practiceContact>();
        LabOrders.practiceContact pCon = new LabOrders.practiceContact();
        pCon.surname = 'MisterTester';
        pList.add(pCon);
        
        c.contact = pList;
        
        p.contacts = c;
        
        return p;
    }
    
    
    private static LabOrders.collections_element getLabOrdersCollection() {
        
        LabOrders.collections_element collection = new LabOrders.collections_element();
        
        LabOrders.collection[] c = new List<LabOrders.collection>();
        LabOrders.collection col = new LabOrders.collection();
        
        LabOrders.collections_element subCol = new LabOrders.collections_element();
        
        col.collections = subCol;
        
        LabOrders.results_element results = new LabOrders.results_element();
        
        LabOrders.result[] rList = new List<LabOrders.result>();        
        
        LabOrders.result r = new LabOrders.result();
        
        LabOrders.assay assay = new LabOrders.assay();
        
        r.assay = assay;
        
        LabOrders.testStatus tStatus = new LabOrders.testStatus();
        r.status = tStatus;
        
        LabOrders.testResultAbnormal ab = new LabOrders.testResultAbnormal();
        r.resultAbnormal = ab;
        
        LabOrders.resultRemarks_element remarks = new LabOrders.resultRemarks_element();
        r.resultRemarks = remarks;
        
        rList.add(r);       
        
        col.results = results;
        
        c.add(col);
        
        collection.collection = c;
        
        return collection;
    }
    
}