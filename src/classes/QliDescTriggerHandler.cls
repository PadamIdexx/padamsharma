// Apex class to add Product Name from QLI to Quote (Description field)

public with sharing class QliDescTriggerHandler {

  public static void ProcessProductNames(Set<ID> QuoteIds){

    // holds a map of the Quote id and comma separated Names to build
    Map<Id, String> QuoteDescMap = new Map<Id, String>();

    // get ALL of the Product Names for all affected Quotes so we can build
    List<QuoteLineItem> QuoteRegions = [select id,UnitPrice, Description,Quote.Id,Product2.Name,Product2.Product_Category__c,Product2.Is_Instrument__c 
       from QuoteLineItem 
      where QuoteId IN :QuoteIds order by   Product2.Is_Instrument__c DESC,Product2.Product_Category__c DESC];//order by Is Instrument First and then  Product2.Name

    for (QuoteLineItem qli : QuoteRegions) {
      if (!QuoteDescMap.containsKey(qli.Quote.id)) {
        // if the key (Quote) doesn't exist, add it with region name
        QuoteDescMap.put(qli.Quote.Id,qli.Product2.Name);
      } else {
        // if the key (Quote) already exist, add ", Product-name"
        QuoteDescMap.put(qli.Quote.Id,QuoteDescMap.get(qli.Quote.Id) + 
          ',' + qli.Product2.Name);
      }
    }

    // get the Quote that were affected
    List<Quote> Quotes= [select id,Description from Quote where Id IN :QuoteIds];

    // add the comma separated list of Products
    for (Quote q : Quotes)
      q.Description = QuoteDescMap.get(q.id);
      Database.SaveResult[] srList = Database.insert(Quotes, false);

    // update the Quotes
    update Quotes;

  }  
}