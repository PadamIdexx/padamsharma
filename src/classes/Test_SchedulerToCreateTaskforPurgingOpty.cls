/* Class Name   : Test_SchedulerToCreateTaskforPurgingOpty
 * Description  : Test Class with unit test scenarios to cover the SchedulerToCreateTaskforPurgingOpty class
 * Created By   : Pooja Wade
 * Created On   : 08-31-2015 
 
* Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                Date                   Modification ID       Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Pooja Wade             08-31-2015                1000                 Initial version
 */
@isTest
public with sharing class Test_SchedulerToCreateTaskforPurgingOpty {

   
   static testMethod void testExecute() {
   
    String CRONstr = '0 0 0 L MAR,JUN,SEP,DEC ? *';
    //Set the Current User for Test by passing the Profile Name and Alias for User.
    
    User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
   // System.debug('@@@@' + currentUser);
  //  System.debug('####' + currentUser.id);
  // System.assert(currentUser.id!=null);
    /*Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
      User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='standarduseridexx@testorg.com');*/
      
      system.runAs(currentUser){
        
        Test.startTest();
        Account testAccount = CreateTestClassData.createCustomerAccount();
        //System.assert(testAccount.id!=null);
     /* Account acc = new Account();
      acc.Name = 'test Acc';
      acc.CurrencyIsoCode = 'USD';
      if (Schema.sObjectType.Account.isCreateable()) {
           insert acc;
      }*/
      
      Opportunity opty = new Opportunity();
      opty.Name = 'test opty';
      opty.AccountID = testAccount.id;
      opty.CloseDate = date.today();
      opty.CurrencyIsoCode = 'USD';
      opty.StageName = 'Qualifying';
      //opty.ownerid = u.id;
      if(opty!=null){
      if (Schema.sObjectType.Opportunity.isCreateable()) {
           insert opty;
      }
      }
     //System.assert(opty.id!=null);
      

      // Schedule the test job
      
      

     String jobId = System.schedule('SchedulerToCreateTaskforPurgingOpty', CRONstr ,  new SchedulerToCreateTaskforPurgingOpty());
    
      // Get the information from the CronTrigger API object
      if(jobId!=null){
     CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
      FROM CronTrigger WHERE id = :jobId];

    //System.assertEquals(CRONstr, ct.CronExpression);
    }
   Test.stopTest();


}
   }
}