@RestResource(urlMapping = '/cpqquotes')
global with sharing class CPQQuotesResource {
    @HttpPost
    global static createOrUpdateQuoteResponse doPost(CreateOrUpdateQuote createOrUpdateQuote) {
        CreateOrUpdateQuoteResponse_element elemen = new CreateOrUpdateQuoteResponse_element();

        try {
                if (null != createOrUpdateQuote && null != createOrUpdateQuote.quote && null != createOrUpdateQuote.quote.OpportunityId && null != createOrUpdateQuote.quote.TransactionId) {
                //List<Quote> quoteList = [SELECT Transaction_ID__c, Quote_Number__c, Quote_Type__c, Description, Status, GrandTotal, Regional_Mgr_Amount__c, Quote_Submitted_Date__c, Quote_Expiration_Date__c, Quote_Approval_Date__c, Program_Name__c, (select Quantity, ListPrice, TotalPrice, Product2.SAP_MATERIAL_NUMBER__c, Product2.Name  from QuoteLineItems) FROM Quote where Quote.Opportunity.Id = :createOrUpdateQuote.quote.OpportunityId ];
                // Objects to load data into salesforce
                List <QuoteLineItem> cpqlineItems = new List <QuoteLineItem> ();
                List <Product2> cpqProducts = new List <Product2> ();
                Quote cpqQuote = new Quote();
                
                // to delete after successful upsert new data    
                List <QuoteLineItem> existingItems = new List <QuoteLineItem> ();
    
                Set <String> materials = new Set <String> ();
    
                if (null != createOrUpdateQuote.quote.QuoteItems && null != createOrUpdateQuote.quote.QuoteItems.QuoteItem && createOrUpdateQuote.quote.QuoteItems.QuoteItem.size() > 0) {
                    for (QuoteItem currQuoteItem: createOrUpdateQuote.quote.QuoteItems.QuoteItem) {
                        materials.add(currQuoteItem.MaterialNumber);
                    }
                }
                
                //locate opportunity for this quote
                Opportunity o = [Select opp.id, opp.pricebook2id, opp.CurrencyIsoCode, opp.account.CurrencyIsoCode, opp.account.Country__c from Opportunity opp WHERE id = :createOrUpdateQuote.quote.OpportunityId];
                
                List <PricebookEntry> dbPricebookEntries;

                if (String.isNotBlank(o.account.Country__c) && (o.account.Country__c == 'CANADA')) {
                    //Use the information from the opportunity to locate pricebooks to use for the opportunity
                    dbPricebookEntries = [SELECT bookEntry.Id, bookEntry.productcode, bookEntry.Pricebook2Id, bookEntry.Product2Id 
                                            FROM PricebookEntry bookEntry 
                                          where bookEntry.Pricebook2Id 
                                            in (select Id FROM Pricebook2 where Name = :CPQQuotesResource.retreiveProperty('CPQ-DEFAULT_PRICE_BOOK') and isActive = true) 
                                            and bookEntry.Product2Id in (select Id FROM Product2) 
                                            and bookEntry.CurrencyIsoCode = :o.CurrencyIsoCode 
                                            and bookEntry.productcode in :materials];

                    
                } else { 
                    //for non-Canada, do what we do now to limit regression
                    //TODO: Have all CPQ quotes use the above approach
                    dbPricebookEntries = [SELECT bookEntry.Id, bookEntry.productcode, bookEntry.Pricebook2Id, bookEntry.Product2Id 
                                          FROM PricebookEntry bookEntry 
                                          where bookEntry.Pricebook2Id 
                                            in (select Id FROM Pricebook2 where Name = :CPQQuotesResource.retreiveProperty('CPQ-DEFAULT_PRICE_BOOK') and isActive = true) 
                                          and bookEntry.Product2Id in (select Id FROM Product2) 
                                          and bookEntry.CurrencyIsoCode = :createOrUpdateQuote.quote.CurrencyCode
                                          and bookEntry.productcode in :materials];
                }
                    
                Map <String, PricebookEntry> myPricebookEntries = new Map < String, PricebookEntry > ();
                
                for (PricebookEntry currPricebookEntry: dbPricebookEntries) {
                    myPricebookEntries.put(currPricebookEntry.ProductCode, currPricebookEntry);
                }
                cpqQuote.Name = createOrUpdateQuote.quote.OpportunityId;                          
                cpqQuote.CPQQuoteName__c = createOrUpdateQuote.quote.OpportunityId; 
                cpqQuote.OpportunityId = createOrUpdateQuote.quote.OpportunityId;
                cpqQuote.Transaction_ID__c = createOrUpdateQuote.quote.TransactionId;
                cpqQuote.Quote_Number__c = createOrUpdateQuote.quote.QuoteNumber;
                cpqQuote.Quote_Type__c = createOrUpdateQuote.quote.QuoteType;
               
                //cpqQuote.Description = createOrUpdateQuote.quote.QuoteDescription;
                
                cpqQuote.CPQ_Quote_Description__c = createOrUpdateQuote.quote.QuoteDescription;
                
                cpqQuote.Status = createOrUpdateQuote.quote.QuoteStatus;
                cpqQuote.CPQ_Quote_Total__c = createOrUpdateQuote.quote.QuoteAmount;
                PricebookEntry cpqQuotePricebookEntry = dbPricebookEntries.get(0);
                cpqQuote.Pricebook2Id = cpqQuotePricebookEntry.Pricebook2Id;
                //field is not writable
                //cpqQuote.GrandTotal = createOrUpdateQuote.quote.QuoteAmount;
    
                cpqQuote.Regional_Mgr_Amount__c = createOrUpdateQuote.quote.RegionalMgrAmount;
                DateTime tempQuoteSubmittedDate = createOrUpdateQuote.quote.QuoteSubmittedDate;
                if (null != tempQuoteSubmittedDate) {
                    cpqQuote.Quote_Submitted_Date__c = tempQuoteSubmittedDate.date();
                }
                DateTime tempQuoteExpirationDate = createOrUpdateQuote.quote.QuoteExpirationDate;
                if (null != tempQuoteExpirationDate) {
                    cpqQuote.Quote_Expiration_Date__c = tempQuoteExpirationDate.date();
                }
                DateTime tempQuoteApprovalDate = createOrUpdateQuote.quote.QuoteApprovalDate;
                if (null != tempQuoteApprovalDate) {
                    cpqQuote.Quote_Approval_Date__c = tempQuoteApprovalDate.date();
                }
                String mergedProgramName = '';
                if (null != createOrUpdateQuote.quote.Programs && null != createOrUpdateQuote.quote.Programs.Program && createOrUpdateQuote.quote.Programs.Program.size() > 0) {
                    for (CreateOrUpdateQuote_Program currprogram: createOrUpdateQuote.quote.Programs.Program) {
                        if (null != currprogram.ProgramName) {
                            if (mergedProgramName == '') {
                                mergedProgramName += currprogram.ProgramName;
                            } else {
                                mergedProgramName += ',' + currprogram.ProgramName;
                            }
                        }
                    }
    
                }
    
                cpqQuote.Program_Name__c = mergedProgramName;
    
                upsert cpqQuote Transaction_ID__c;
    
                if (null != createOrUpdateQuote.quote.QuoteItems && null != createOrUpdateQuote.quote.QuoteItems.QuoteItem && createOrUpdateQuote.quote.QuoteItems.QuoteItem.size() > 0) {
                    //Quantity, ListPrice, TotalPrice, Product2.SAP_MATERIAL_NUMBER__c, Product2.Name 
                    existingItems = [select Id from QuoteLineItem where QuoteLineItem.Quote.Id =: cpqQuote.Id];
                    for (QuoteItem currQuoteItem: createOrUpdateQuote.quote.QuoteItems.QuoteItem) {
                        QuoteLineItem cpqlineItem = new QuoteLineItem();
    
                        PricebookEntry cpqPricebookEntry = myPricebookEntries.get(currQuoteItem.MaterialNumber);
                        cpqlineItem.PricebookEntryId = cpqPricebookEntry.Id;
                        cpqlineItem.Product2Id = cpqPricebookEntry.Product2Id;
                        cpqlineItem.QuoteId = cpqQuote.Id;
                        cpqlineItem.Quantity = currQuoteItem.Quantity;
                        cpqlineItem.UnitPrice = currQuoteItem.UnitAmount;
                        //fields are not writable
                        //cpqlineItem.ListPrice = currQuoteItem.UnitAmount;
                        //cpqlineItem.TotalPrice = currQuoteItem.TotalAmount;
    
                        cpqlineItems.add(cpqlineItem);
                    }
                }
    
                upsert cpqlineItems;
                delete existingItems;
                elemen.Status = 'SUCCESS';
                elemen.Description = 'Upsert was successful ';
            } else {
    
                elemen.Status = 'FAILED';
                elemen.Description = 'Missing Opportunity Id or CPQ Quote Transaction Id';
            }
        } catch (Exception e) {
            elemen.Status = 'FAILED';
            elemen.Description = e.getMessage();       
        }
        createOrUpdateQuoteResponse myresponse = new createOrUpdateQuoteResponse();
        myresponse.CreateOrUpdateQuoteResponse = elemen;

        return myresponse;
    }

    global class CreateOrUpdateQuote {
        webService CreateOrUpdateQuote_Quote quote {get;set;}
    }

    global class CreateOrUpdateQuoteResponse_element {
        webService String Status {get;set;}
        webService String Description {get;set;}
    }
    global class CreateOrUpdateQuote_Quote {
        webService String OpportunityId {get;set;}
        webService String TransactionId {get;set;}
        webService String QuoteNumber {get;set;}
        webService String QuoteType {get;set;}
        webService String QuoteDescription {get;set;}
        webService String QuoteStatus {get;set;} 
        webService Decimal QuoteAmount {get;set;}
        webService Decimal RegionalMgrAmount {get;set;}
        webService String CurrencyCode {get;set;}
        webService DateTime QuoteCreatedDate {get;set;}
        webService String QuoteCreatedBy {get;set;}
        webService DateTime QuoteLastModifiedDate {get;set;}
        webService String QuoteLastModifiedBy {get;set;}
        webService DateTime QuoteSubmittedDate {get;set;}
        webService DateTime QuoteExpirationDate {get;set;}
        webService DateTime QuoteApprovalDate {get;set;}
        webService QuoteItems_element QuoteItems {get;set;}
        webService Programs_element Programs {get;set;}
    }
    global class QuoteItems_element {
        webService QuoteItem[] QuoteItem {get;set;}
    }
   
    global class CreateOrUpdateQuote_Program {
        webService String ProgramName {get;set;}
    }
    
    global class createOrUpdateQuoteResponse {
        webService CreateOrUpdateQuoteResponse_element CreateOrUpdateQuoteResponse {get;set;}
    }

    global class QuoteItem {
        webService String MaterialNumber {get;set;}
        webService String MaterialDescription {get;set;}
        webService Integer Quantity {get;set;}
        webService Decimal UnitAmount {get;set;}
        webService Decimal TotalAmount {get;set;}
    }
    global class Programs_element {
        webService CreateOrUpdateQuote_Program[] Program {get;set;}

    }
    
    private static String retreiveProperty(String sIntegratioName) {
        IDEXX_Integration_Service_Properties__c prop = IDEXX_Integration_Service_Properties__c.getInstance(sIntegratioName);
        if(prop!=null)
            return prop.TextVal__c;
        else
            return null;
    }
}