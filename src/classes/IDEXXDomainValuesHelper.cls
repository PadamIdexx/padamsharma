public class IDEXXDomainValuesHelper {

    //helper method to fetch pricing description for payment terms 
    public static String getPricingInformation(String code, String typeOfDomain){
        
        String description = '';
        
        List<IDEXX_Domain_Values__c> domainVal = new List<IDEXX_Domain_Values__c>();
        try{
            domainVal = [SELECT Code__c, Description__c, Active__c, Name FROM IDEXX_Domain_Values__c WHERE Name =: typeOfDomain AND Active__c =: true AND Code__c =: code LIMIT 1];
            
        }
        catch(DmlException ex){
            System.debug(System.LoggingLevel.DEBUG, ex.getCause());
        }
        if(domainVal.size() > 0){
            description = domainVal.get(0).Description__c;
        }
        return description;
        
    }
    
    public static String getDomainValueInformationIgnoringActiveFlag(String code, String typeOfDomain){
        
        String description = '';
        
        List<IDEXX_Domain_Values__c> domainVal = new List<IDEXX_Domain_Values__c>();
        try{
            domainVal = [SELECT Code__c, Description__c, Active__c, Name FROM IDEXX_Domain_Values__c WHERE Name =: typeOfDomain AND Code__c =: code LIMIT 1];
            
        }
        catch(DmlException ex){
            System.debug(System.LoggingLevel.DEBUG, ex.getCause());
        }
        if(domainVal.size() > 0){
            description = domainVal.get(0).Description__c;
        }
        return description;
        
    }
    	
    
}