/*
 * Helper class for interogating a users profile to determine various things about the profile.
 * 
 * For example, is this a CAG profile?  Is it EMEA?  Etc.
 */ 
public class UserHelper {

    private String profileName;
    
    public UserHelper() {
		this(UserInfo.getProfileId());       
    }
    
     public UserHelper(String profileId) {
		profileName = [SELECT Id,Name From Profile where Id =:profileId].Name;       
    }
    
    public Boolean isCAG() {
        if (String.isNotBlank(profileName)
            && (profileName.indexOfIgnoreCase('CAG') >= 0 )) {
                return true;
        }
        
        return false;
    }
    
    public Boolean isLPD(){
        if (String.isNotBlank(profileName)
            && (profileName.indexOfIgnoreCase('LPD') >= 0)) {
                return true;
        }        
        return false;
    }

    public Boolean isWater(){
        if (String.isNotBlank(profileName)
            && (profileName.indexOfIgnoreCase('Water') >= 0)) {
                return true;
        }        
        return false;
    }

    
    public Boolean isEMEA() {
        if (String.isNotBlank(profileName)
            && (profileName.indexOfIgnoreCase('EMEA') >= 0)) {
                return true;
        }     
        return false;
    }
    
    public Boolean isNA() {
        if (String.isNotBlank(profileName)
            && (profileName.indexOfIgnoreCase('EMEA') < 0)) {
                return true;
        }             
        return false;
    }
    
    public Boolean isSystemAdministrator() {
                if (String.isNotBlank(profileName)
            && (profileName.indexOfIgnoreCase('System Administrator') >= 0)) {
                return true;
        }             
        return false;
    }
    
    public Boolean doesUserHaveProfile(String profile) {
            if (String.isNotBlank(profile)
            && (profileName.indexOfIgnoreCase(profile) >= 0)) {
                return true;
        }             
        return false; 
    }
    
}