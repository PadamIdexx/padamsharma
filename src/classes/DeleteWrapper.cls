public class DeleteWrapper extends SAPOrderWrapper {
	public override SAPOrder execute(SAPOrder order) {

        OrderDeleteRequest.SalesOrderDeleteRequest request;        
        OrderDeleteResponse.SalesOrderDeleteResponse response;
		SAPOrder deleteOrder;
        
        //transform SAPOrder to request        
        try {
        	request = OrderDeleteRequest.transform(order); 
        } catch(Exception e) {
            System.debug('Error transforming SAPOrder to OrderDeleteRequest.');
            System.debug(e.getCause());
            System.debug(e.getMessage());
            
        	throw e;    
        } 
        
        //call service
        try {
            OrderDeleteService service = new OrderDeleteService();
            
            if (!Test.isRunningTest()) {
                response = service.deleteOrder(request);            
            } else {
                response = OrderTestDataFactory.createDeleteResponse();
            }
        } catch(Exception e) {
            System.debug('Error calling Order Delete Service.');
            System.debug(e.getCause());
            System.debug(e.getMessage());
            
            throw e;             
        }               
        
        //transform response into new SAPOrder
        try {
	        deleteOrder = OrderDeleteResponse.transform(order, response);  
        } catch(Exception e) {
            System.debug('Error transforming OrderDeleteResponse to SAPOrder.');
            System.debug(e.getCause());
            System.debug(e.getMessage());
            
        	throw e;              
        }        
        
        return deleteOrder;
    }
}