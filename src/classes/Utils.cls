public with sharing class Utils {

    /**
     * Returns a comma-deliminated string of the address components, ignoring
     * any empty values.
     */
    public static String formatAddress(String pStreet,
                                       String pCity,
                                       String pState,
                                       String pCountry,
                                       String pZip) {
        String formattedAddress = ' ';
        formattedAddress = appendComma(pStreet, formattedAddress);
        formattedAddress = appendComma(pCity, formattedAddress);
        formattedAddress = appendComma(pState, formattedAddress);
        formattedAddress = appendComma(pCountry, formattedAddress);
        formattedAddress = appendComma(pZip, formattedAddress);

        return formattedAddress;
    }

    /**
     * Append a string to another string.
     * If pAppend is empty, it will just return pAppendTo.
     * If pAppendTo is empty, it will just return pAppend.
     * If neither is empty, it will return pAppendTo, pAppend.
     */
    public static String appendComma(String pAppend, String pAppendTo) {
        String newStr = pAppendTo;
        if (String.isNotBlank(pAppend)) {
            if (String.isNotBlank(pAppendTo)) {
                newStr = newStr + ', ' + pAppend;
            } else {
                newStr = pAppend;
            }
        }
        return newStr;
    }

    public static String removeLeadingZeros(String sapNumber) {
        String accSAPNo = sapNumber;
        if (String.isNotBlank(sapNumber)) {
            accSAPNo = sapNumber.replaceFirst('^0+', '');
        }
        return accSAPNo;
    }
    
    /**
     * Converts a String to a List of Strings that are split by a single character token. The length of each String
     * being added to the returned List of String is limited to the value passed in desiredStringLength. 
     * 
     * The result is a List of Strings that are chunked by charToken, not to exceed the integer value desiredStringLength for each.
     * Usage example: Given a very long paragraph, split the input String by word (single char space); returning a List of Strings.
     */
    public static List<String> chunkStringAtWordBoundaries(String inputString, String charToken, Integer desiredStringLength) {  
        String[] arrString = inputString.split('' + charToken + '\\s*');
        List<String> retList = new List<String>();
        String temp = '';
        for (integer i=0; i< arrString.size(); i++) {
            System.debug('iterating item ' + i + ' ...value = ' + arrString[i]);
            if (temp.length() + arrString[i].length()+1 <= desiredStringLength) {
                temp = temp + arrString[i] + ' ';
            } else {
                retList.add(temp);
                temp = arrString[i] + ' ';    
            }
        }
        
        if (temp.length() > 0) {
            retList.add(temp);
        }
        return retList;
    }
    
    /*
     * Helper to format an amount based on the scale of its currency
     */ 
    public static Decimal formatCurrency(Decimal amount, String curr) {

        if (null == amount) {
            System.debug('formatCurrency().  Amount cannot be null.');
            return amount;
        }
        
        if (String.isBlank(curr)) {
            System.debug('formatCurrency().  Currency code is required.  Defaulting to 2 decimal places.');
            return amount.setScale(2);
        }
        
		Decimal formattedAmount = amount;
        
        CurrencyType c = [SELECT DecimalPlaces FROM CurrencyType WHERE IsoCode =:curr];
        
        if (null != c) {
            formattedAmount = amount.setScale(c.DecimalPlaces);
        } 
        
        return formattedAmount;
    }
    
    
    /**
     * Helper function to handle converting formatted string currency to its corresponding
     * decimal value.
     * 
     * This function is intended to address a lack of locale currency handling in Salesforce.
     */
    public static Decimal formatCurrencyForLocale(String locale, String amount) {
        Decimal formattedAmount = null;
        
        if (String.isBlank(locale)
            || String.isBlank(amount)) {
            return null;
        }
        
        if (locale.equalsIgnoreCase('fr_FR')
            || locale.equalsIgnoreCase('it_IT')
            || locale.equalsIgnoreCase('es_ES')
            || locale.equalsIgnoreCase('nl_NL')) {
            
            //Strip out the periods (.)
            if (amount.contains('.')) {
                amount = amount.remove('.');
            }
            
            //replace commas (,) with periods (.) to make it a valid decimal
            if (amount.contains(',')) {
                amount = amount.replace(',', '.');
            }
            
            formattedAmount = Decimal.valueOf(amount);
            
        
        } else {  //default behavior (i.e. en_US, en_GB, and other that use us style currency formats)
        
            //Strip out commas (,)
            if (amount.contains(',')) {
                amount = amount.remove(',');
            }
            
        
            formattedAmount = Decimal.valueOf(amount);
        }
        
        return formattedAmount;
        
        
    }
}