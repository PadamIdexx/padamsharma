@isTest
public class Test_IDEXXDomainValuesHelper {
    
    public static Integer numElements{get;set;}
    
    @testSetup 
    static void createTestData(){
        Boolean setFlag = true;
        List<IDEXX_Domain_Values__c> domainVals = new List<IDEXX_Domain_Values__c>();
        for(Integer i=0; i<numElements; i++){
            IDEXX_Domain_Values__c domainVal = new IDEXX_Domain_Values__c();
            domainVal.Name = 'Payment Terms';
            domainVal.Active__c = setFlag;
			domainVal.Description__c = 'Test description ' + i;
			domainVal.Code__c = 'Z1'+i;  
            domainVal.CurrencyIsoCode = 'USD';
            setFlag = false;
            domainVals.add(domainVal);
        }
        insert domainVals;   
    }
    
    @testSetup 
    static void createTestDataIgnoringActiveFlag(){
        Boolean setFlag = false;
        List<IDEXX_Domain_Values__c> domainVals = new List<IDEXX_Domain_Values__c>();
        for(Integer i=0; i<numElements; i++){
            IDEXX_Domain_Values__c domainVal = new IDEXX_Domain_Values__c();
            domainVal.Name = 'Ship Method';
            domainVal.Active__c = setFlag;
			domainVal.Description__c = 'Test ignore active flag description ' + i;
			domainVal.Code__c = 'FP'+i;  
            domainVal.CurrencyIsoCode = 'USD';
            setFlag = false;
            domainVals.add(domainVal);
        }
        insert domainVals;   
    }

    public static testmethod void TestGetPricingInfoPositive(){
        numElements = 1;
        createTestData();
        String testOne = IDEXXDomainValuesHelper.getPricingInformation('Z10', 'Payment Terms');
        System.assertEquals('Test description 0', testOne);
        
    }
    public static testmethod void TestGetPricingInfoNegative(){
        numElements = 2;
        createTestData();
        String testTwo = '';
        try{
        	testTwo = IDEXXDomainValuesHelper.getPricingInformation('Z11', 'Payment Terms');
        }
        catch(DMLexception ex){
            system.debug('ex ' + ex.getDmlMessage(0));
            System.assertNotEquals('Test description 1', testTwo);
        }   
    }
    
    public static testmethod void TestGetShipMethodInfoIgnoringActiveFlagPositive(){
        numElements = 1;
        createTestDataIgnoringActiveFlag();
        String testThree = IDEXXDomainValuesHelper.getDomainValueInformationIgnoringActiveFlag('FP0', 'Ship Method');
        System.assertEquals('Test ignore active flag description 0', testThree);
        
    }
    public static testmethod void TestGetShipMethodInfoIgnoringActiveFlagNegative(){
        numElements = 2;
        createTestDataIgnoringActiveFlag();
        String testThree = '';
        try{
        	testThree = IDEXXDomainValuesHelper.getDomainValueInformationIgnoringActiveFlag('FP1', 'Ship Method');
        }
        catch(DMLexception ex){
            system.debug('ex ' + ex.getDmlMessage(0));
            System.assertNotEquals('Test ignore active flag description 1', testThree);
        }   
    }
}