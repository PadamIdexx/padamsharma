/* Trigger Name  : AccountSalesviewTrigger
* Description   : Apex Handler Class to update Doctor Discount based on Price Group of Salesview
* Created By    : Aditya Sangawar
* Created On    : 17-Nov-2015
*
*  Modification Log :
*  --------------------------------------------------------------------------------------
*  * Developer                    Date                    Description
*  * ------------------------------------------------------------------------------------                 
*  * Aditya Sangawar              17-Nov-2015               Initial version.
*    Aditya Sangawar              03-Dec-2015               Modified version (changed Trigger event from Salesview to AccountSalesview)
*    Aditya Sangawar              03-Dec-2015               Modified version (Added description along with Values on the Doctor Discount)
*    Aditya Sangawar              18-Feb-2016               Modified version ((Instead of Salesview Price_Group__c the Account_Salesview__c Price_Group__c is used now - TKT 543 )
*    Robin Fortney                10-Aug-2016               Modified to use Price_Group_Dr_Discount__c for Doctor Discount and modified query to be more specific     
*     
*****************************************************************************************/

public with sharing class AccountSalesviewTriggerHandler{
    public static void UpdateDoctorDiscountonPriceGroupInsert(List < Account_Salesview__c > svListInsert, List < Account_Salesview__c > svListUpdate, List < Account_Salesview__c > svListDelete, Map < Id, Account_Salesview__c > svOldMap) {
        
        List < Account > lstAccUpdate = new List < Account > ();
        //List < AggregateResult > ar = new List < AggregateResult > ();
        List < Account_Salesview__c > salesviewrecords = new List < Account_Salesview__c > ();
        Set < Id > SetIds = new Set < Id > ();
        
        if (svListInsert != null) {
            for (Account_Salesview__c childObj: svListInsert) {
                
                SetIds.add(childObj.Account__c);
            }
        } else if (svListUpdate != null) {
            for (Account_Salesview__c childObj: svListUpdate) {
                //if (childObj.Price_Group__c != svOldMap.get(childObj.Id).Price_Group__c) {
                setIds.add(childObj.Account__c); 
                //}
            }
        } else if (svListDelete != null) {
            for (Account_Salesview__c childObj: svListDelete) {
                setIds.add(childObj.Account__c);
            }
        }
        
        if (!setIds.isEmpty()) {
            Map < String, String > accounttoSv = new Map < String, String > ();
            Map < String, String > PriceGroupDescription = new Map < String, String > ();
            String temp = '';
            for(SalesView_PriceGroup_Value__mdt PriceGroupMetadata: [SELECT Id,Label,MasterLabel,QualifiedApiName,PriceGroup_Type__c From SalesView_PriceGroup_Value__mdt LIMIT 1000 ]){
                PriceGroupDescription.put(PriceGroupMetadata.MasterLabel, PriceGroupMetadata.PriceGroup_Type__c);
            }    
            
            for( Account_Salesview__c sv : [select Account__c, Price_Group_Dr_Discount__c from Account_Salesview__c where Account__c != null AND Price_Group_Dr_Discount__c != null AND SalesView__r.Sales_Division__c = 'VS' AND SalesView__r.Sales_Organization_Code__c = 'USS3'AND Account__c in : setIds]  ){
                
                if (!accounttoSv.containsKey(sv.Account__c)) { //add new record 
                    //if(!PriceGroupDescription.containsKey(tempDrDiscountKey)){
                    if(!PriceGroupDescription.containsKey(sv.Price_Group_Dr_Discount__c)){
                        //accountToSv.put(sv.Account__c, sv.Price_Group__c );//+ '-' + 'No Description'); 
                    }else{
                        //accountToSv.put(tempAcct, PriceGroupDescription.get(tempDrDiscountKey));
                        accountToSv.put(sv.Account__c, PriceGroupDescription.get(sv.Price_Group_Dr_Discount__c));//sv.Price_Group__c + '-' +PriceGroupDescription.get(sv.Price_Group__c));
                    }
                } else {
                    //temp = accountToSv.get(tempAcct); //add new record
                    temp = accountToSv.get(sv.Account__c); //add new record
                    //if(!PriceGroupDescription.containsKey(tempDrDiscountKey)){
                    if(!PriceGroupDescription.containsKey(sv.Price_Group_Dr_Discount__c)){
                        // accountToSv.put(sv.Account__c,temp + '\r\n' + sv.Price_Group__c);// + '-' + 'No Description');   
                    }else{
                        //accountToSv.put(tempAcct,temp + '\r\n' + PriceGroupDescription.get(tempDrDiscountKey));//sv.Price_Group__c + '-' +PriceGroupDescription.get(sv.Price_Group__c));
                        accountToSv.put(sv.Account__c,temp + '\r\n' + PriceGroupDescription.get(sv.Price_Group_Dr_Discount__c));//sv.Price_Group__c + '-' +PriceGroupDescription.get(sv.Price_Group__c));
                    }             
                }
            }
            
            // Update the account field Doctor_Discount__c with all these Price_Group__c values as Comma seperated values
            for (Account acc: [select id, Doctor_Discount__c from account where id in : accountToSv.keyset()]) {
                acc.Doctor_Discount__c = accounttosv.get(acc.Id);
                lstAccUpdate.add(acc);
            }
            
            // Delete the account field Doctor_Discount__c with all these Price_Group__c values as Comma seperated values
            for (Account acc: [Select ID, Doctor_Discount__c From Account WHERE ID IN: setIds AND ID NOT IN: accountToSv.keyset() AND IsDeleted = false]) {
                acc.Doctor_Discount__c = '';
                lstAccUpdate.add(acc);
            } 
            if(!lstAccUpdate.isempty()){
                update lstAccUpdate;
            }
        }
    }
    /*//Manoj for updating Territory Id in Account from SalesView object
    public static void UpdateSalesViewDistrictToAccount(List < Account_Salesview__c > svListInsert, List < Account_Salesview__c > svListUpdate, List < Account_Salesview__c > svListDelete, Map < Id, Account_Salesview__c > svOldMap) {
        
        List < Account > lstAccUpdate = new List < Account > ();
        List < Account_Salesview__c > salesviewrecords = new List < Account_Salesview__c > ();
        String iWaterRecTypeId = '';
        try{
            if(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Water')!=null){
                iWaterRecTypeId = string.isNotEmpty(string.valueOf(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Water').getRecordTypeId())) ? string.valueOf(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Water').getRecordTypeId()) : '';  
                system.debug('iWaterRecTypeId:: '+iWaterRecTypeId);
            }
        }
        catch(DmlException ex){
            system.debug('error fetching water record type ' + ex.getCause());
        }
        Map <Id, List <Account_Salesview__c>> mpAccSalesViewRecs = new Map <Id, List <Account_Salesview__c>>();
        Map <Id, Account_Salesview__c> mpAccSalesView_INSUPD = new Map <Id, Account_Salesview__c>();
        Map <Id, Account_Salesview__c> mpAccSalesView_DEL = new Map <Id, Account_Salesview__c>();
        Set < Id > SetAccIds = new Set < Id > ();
        system.debug('svListInsert ::'+svListInsert);
        if(svListInsert != null) {
            for (Account_Salesview__c childObj: svListInsert) {
                SetAccIds.add(childObj.Account__c);
                mpAccSalesView_INSUPD.put(childObj.Account__c, childObj);
            }
            system.debug('SetAccIds::' +SetAccIds);
            system.debug('mpAccSalesView_INSUPD::' +mpAccSalesView_INSUPD);
        } else if (svListUpdate != null) {
            for (Account_Salesview__c childObj: svListUpdate) {
                SetAccIds.add(childObj.Account__c); 
                mpAccSalesView_INSUPD.put(childObj.Account__c, childObj);
            }
        } else if (svListDelete != null) {
            for (Account_Salesview__c childObj: svListDelete) {
                SetAccIds.add(childObj.Account__c);
                mpAccSalesView_DEL.put(childObj.Account__c, childObj);
            }
        }
        
        if (!SetAccIds.isEmpty()) {
            for(Account_Salesview__c asv : [Select Id, Sales_District__c, Account__c From Account_Salesview__c Where Account__c IN: SetAccIds AND IsDeleted = false]) {
                
                if(mpAccSalesViewRecs.containsKey(asv.Account__c)) {
                    
                    List <Account_Salesview__c> tmpList = mpAccSalesViewRecs.get(asv.Account__c);
                    tmpList.add(asv);
                    mpAccSalesViewRecs.put(asv.Account__c, tmpList);
                } else {
                    List <Account_Salesview__c> tmpList = new List <Account_Salesview__c>();
                    tmpList.add(asv);
                    mpAccSalesViewRecs.put(asv.Account__c, tmpList);
                }
            }
            Map <Id, String> mpAccDistVal = new Map <Id, String>();
            if(iWaterRecTypeId != ''){
                for (Account acc: [Select ID, Territory_ID__c From Account WHERE ID IN: SetAccIds AND RecordTypeID =: Id.valueOf(iWaterRecTypeId) AND IsDeleted = false]) {
                    
                    acc.Territory_ID__c = '';
                    
                    if(mpAccSalesView_INSUPD.get(acc.Id) != null) {
                        string strTerrVal = '';
                        for(Account_Salesview__c asv : mpAccSalesViewRecs.get(acc.Id)) {
                            
                            if((asv.Id != mpAccSalesView_INSUPD.get(acc.Id).Id) && asv.Sales_District__c != null && asv.Sales_District__c != '') {
                                strTerrVal +=  asv.Sales_District__c + ';';
                            }
                            mpAccDistVal.put(acc.Id, strTerrVal);
                            system.debug('strTerrVal::'+strTerrVal);
                        }
                        if(mpAccDistVal.get(acc.Id) != null && mpAccDistVal.get(acc.Id) != '') {
                            acc.Territory_ID__c = mpAccDistVal.get(acc.Id) + ';';
                        } 
                        System.debug('Territory_ID__c:: '+acc.Territory_ID__c);
                        if(mpAccSalesView_INSUPD.get(acc.Id).Sales_District__c != null && mpAccSalesView_INSUPD.get(acc.Id).Sales_District__c != null) {
                            acc.Territory_ID__c += mpAccSalesView_INSUPD.get(acc.Id).Sales_District__c;
                        }
                        acc.Territory_ID__c = acc.Territory_ID__c.replaceAll(';;', ';');
                        if(acc.Territory_ID__c.lastIndexOf(';') == acc.Territory_ID__c.length()) {
                            acc.Territory_ID__c = acc.Territory_ID__c.subString(0, acc.Territory_ID__c.length() -1);
                        }
                    }
                    if(mpAccSalesView_DEL.get(acc.Id) != null && mpAccSalesViewRecs.get(acc.Id) != null) {
                        string strTerrVal = '';
                        for(Account_Salesview__c asv : mpAccSalesViewRecs.get(acc.Id)) {
                            
                            if((asv.Id != mpAccSalesView_DEL.get(acc.Id).Id) && asv.Sales_District__c != null && asv.Sales_District__c != '') {
                                strTerrVal +=  asv.Sales_District__c + ';';
                            }
                            mpAccDistVal.put(acc.Id, strTerrVal);
                        }
                        if(mpAccDistVal.get(acc.Id) != null && mpAccDistVal.get(acc.Id) != '') {
                            acc.Territory_ID__c = mpAccDistVal.get(acc.Id) + ';';
                        } 
                        acc.Territory_ID__c = acc.Territory_ID__c.replaceAll(';;', ';');
                        if(acc.Territory_ID__c.lastIndexOf(';') == acc.Territory_ID__c.length()) {
                            acc.Territory_ID__c = acc.Territory_ID__c.subString(0, acc.Territory_ID__c.length() -1);
                        }
                    }
                    lstAccUpdate.add(acc);
                }
            }
        }
        if(!lstAccUpdate.isempty()){
            update lstAccUpdate;
        }
    }*/
    
    //Added by Nagendra to populate the Freight Terms field on the Account object.............  
    public static void populateTheFreightTermsFieldOnTheAccount( Account_Salesview__c[] lstSalesViews ) 
    {
        map<Id, list<string>> mapAccount = new map<Id, list<string>>();
        set<Id> SetAccIds = new set<Id>();
        for( Account_Salesview__c obj : lstSalesViews ){
            if( obj.Account__c != null )
            {
                setAccIds.add(obj.Account__c);
            }
        }
        for( Account_Salesview__c obj : [Select Incoterms1__c, Account__c FROM Account_Salesview__c WHERE Account__c IN : setAccIds AND IsDeleted = false] ){
            if( obj.Account__c != null )
            {
                string[] lstIncoterms = new string[]{};
                    if( mapAccount.containsKey(obj.Account__c) )
                    lstIncoterms = mapAccount.get(obj.Account__c);
                lstIncoterms.add( obj.Incoterms1__c );
                mapAccount.put(obj.Account__c, lstIncoterms);   
            }
        }
        Account[] lstAccounts = new Account[]{};
            for( Account objAcc : [Select Id, Freight_Terms__c FROM Account WHERE RecordType.Name = 'Water' AND Id IN :mapAccount.keyset() AND IsDeleted = false] ){ 
                objAcc.Freight_Terms__c = string.JOIN(mapAccount.get( objAcc.Id ), ';');
                lstAccounts.add( objAcc );  
            }
        if( !lstAccounts.isEmpty() )
        {
            update lstAccounts;
        }
    }
    
   /* public static void populateIsWaterFieldOnTheAccount( Account_Salesview__c[] lstSalesViews )
    {
        set<Id> SetAccIds = new set<Id>();
        map<Id, id> mapAccount = new map<Id, id>();
        map<Id, id> mapAccount1 = new map<Id, id>();
        list<Account_Salesview__c> iswaterlist1 = new list<Account_Salesview__c>();
        for( Account_Salesview__c acsv : lstSalesViews )
        {
            if( acsv.Account__c != null )
            {
                setAccIds.add(acsv.Account__c);
            }
        }
        iswaterlist1 = [select id,AccountDivision__c, Marked_for_Deletion_Flag__c, Account__c FROM Account_Salesview__c WHERE  Account__c IN : setAccIds 
                        and AccountDivision__c ='CE' and Marked_for_Deletion_Flag__c =FALSE];
        system.debug('iswaterlist1 ' + iswaterlist1.size());
        for( Account_Salesview__c obj : iswaterlist1 )
        {
            if(iswaterlist1.size()>0 )
            {
                mapAccount.put(obj.Account__c,obj.Account__c);
            }
            
        }
        
        Account[] lstAccounts = new Account[]{};
            for( Account objAcc : [Select Id, isWater__c FROM Account WHERE RecordType.Name = 'Water' AND Id IN :mapAccount.keyset() AND IsDeleted = false] )   
        {    
            system.debug('Iswater:'+objAcc.isWater__c);
            objAcc.isWater__c = true;
            system.debug('Iswater:'+objAcc.isWater__c);
            lstAccounts.add(objAcc);  
        }
        if( !lstAccounts.isEmpty() )
        {
            update lstAccounts;
        }
        
    }
    
    public static void unpopulateIsWaterFieldOnTheAccount( Account_Salesview__c[] lstSalesViews1 )
    {
        set<Id> SetAccIds1 = new set<Id>();
        map<Id, id> mapAccount1 = new map<Id, id>();
        list<Account_Salesview__c> iswaterlist = new list<Account_Salesview__c>();
        list<Account_Salesview__c> iswaterlistall = new list<Account_Salesview__c>();
        for( Account_Salesview__c acsv1 : lstSalesViews1 )
        {
            if( acsv1.Account__c != null )
            {
                setAccIds1.add(acsv1.Account__c);
            }
        }
        iswaterlist = [select id,AccountDivision__c, Marked_for_Deletion_Flag__c, Account__c FROM Account_Salesview__c WHERE  Account__c IN : setAccIds1 
                       and (AccountDivision__c !='CE' or Marked_for_Deletion_Flag__c = TRUE)];
        iswaterlistall = [select id,AccountDivision__c, Marked_for_Deletion_Flag__c, Account__c FROM Account_Salesview__c WHERE  Account__c IN : setAccIds1 AND IsDeleted = false];
        
        system.debug('iswaterlist ' + iswaterlist.size());
        system.debug('iswaterlist ' + iswaterlistall.size());
        
        for( Account_Salesview__c obj1 :  iswaterlist )
        {
            system.debug('iswaterlist ' + iswaterlist.size());   
            system.debug('iswaterlist ' + iswaterlistall.size());   
            if(iswaterlist.size()==iswaterlistall.size())
            {
                system.debug('iswaterlist ' + iswaterlist.size()); 
                mapAccount1.put(obj1.Account__c,obj1.Account__c);
            }
            
            
        }
        Account[] lstAccounts1 = new Account[]{};
            for( Account objAcc1 : [Select Id, isWater__c FROM Account WHERE  Id IN :mapAccount1.keyset() AND IsDeleted = false] )   
        {            
            system.debug('Iswater:'+objAcc1.isWater__c);
            objAcc1.isWater__c = false;
            lstAccounts1.add(objAcc1);  
        }
        if( !lstAccounts1.isEmpty() )
        {
            update lstAccounts1;
        }
        
    }*/
}