public with sharing class This2weeksAppointments {

    public List<Event> lstEvent {get;set;}
    public This2weeksAppointments()
    {
    
        Date startOfWeek = System.today();
        Date endOfWeek = startOfWeek.addDays(14);

        System.debug('--startOfWeek-------->'+startOfWeek);
        System.debug('--endOfWeek-------->'+endOfWeek);
        
        lstEvent  = new List<Event>();
       // lstEvent = [select id,subject,ActivityDate,AccountId,Description,Location from Event where ActivityDate >= :startOfWeek AND ActivityDate <= :endOfWeek];
               lstEvent = [select id,subject,Account.Name,ActivityDate,AccountId,Description,Location,StartDateTime,EndDateTime from Event where ActivityDate >= :startOfWeek AND ActivityDate <= :endOfWeek and ownerid =:userinfo.getuserid() ORDER BY ActivityDate];
    }
}