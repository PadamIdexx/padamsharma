global with sharing class QuickQuoteExtension
{
    public QuickQuoteExtension(ApexPages.StandardController controller)
    {
       
    }
       
    @RemoteAction
    global static List<PriceBook2> getPricebooks() {
        List<PriceBook2> pbs = [SELECT Id, Name FROM PriceBook2 LIMIT 100];
        return pbs;
    }
   
  /*  @RemoteAction
     global static List<ProductWithPrice> getProducts(ID priceBookId) {
        List<PriceBookEntry > entries =  [SELECT UnitPrice, Product2ID FROM PriceBookEntry WHERE Pricebook2ID = :priceBookId AND IsActive = true];
       
        Map<ID,Decimal> priceMap = new Map<ID,Decimal>();
        Map<ID, ID> entryMap = new Map<ID,ID>();{
        for (PricebookEntry e : entries) {
            priceMap.put(e.product2ID,e.UnitPrice);
            entryMap.put(e.product2ID,e.ID);
        }
       
        List<Product2> products = [SELECT Id, Name FROM Product2 WHERE ID IN :priceMap.keySet() ORDER BY Name ASC];
     */  
      /*  List<ProductWithPrice> toReturn = new List<ProductWithPrice>();
        for (Product2 p : products)
        {
            ProductWithPrice one = new ProductWithPrice();
            one.ProductId = p.Id;
            one.Name = p.Name;
            one.UnitPrice = priceMap.get(p.Id);
            toReturn.add(one);
        }
       
        return toReturn;  */ 
  //  }
   
 /*  @RemoteAction
    global static ID saveQuote(Quote quote, List<QuickQuoteLineItem> lineItems) {
       
        insert quote;
       
        ID qId = quote.Id;
        List<QuoteLineItem> quoteLines = new List<QuoteLineItem>();
       
        for(QuickQuoteLineItem l:lineItems) {
            QuoteLineItem quoteLine = new QuoteLineItem();
           
            quoteLine.QuoteId = qId;
            quoteLine.Product2Id = l.Product.ProductId;
            PricebookEntry pbe = [SELECT Id FROM PriceBookEntry WHERE Pricebook2ID = :l.PriceBook AND Product2ID = :l.Product.ProductId];
            quoteLine.PricebookEntryId = pbe.Id;
            quoteLine.Discount = l.Discount;
            quoteLine.UnitPrice = l.Product.UnitPrice;
            quoteLine.Quantity = l.Quantity;
           
            quoteLines.add(quoteLine);
        }
       
        insert quoteLines;
        return qId;
    }
   */ 
}