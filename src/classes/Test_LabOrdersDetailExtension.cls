@isTest
public class Test_LabOrdersDetailExtension {

    public static testmethod void testWebService(){
        List<LabOrders.labOrder> mockList = new List<LabOrders.labOrder>();
        
        Account testAccount = new Account();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = 'Customer Testing Account';
        testAccount.CurrencyISOCode = 'USD';
        testAccount.HIN__c = 'Test123';
        testAccount.SAP_Customer_Number__c = '0'+String.valueOf(Math.random()).subString(12);
        insert testAccount;
        
        Lims__c lims = new Lims__c(LIMS_Practice_ID__c = '1345',Name = 'test',Account__c=testAccount.Id);
        Lims__c lims2 = new Lims__c(LIMS_Practice_ID__c = '8181',Name = 'test',Account__c=testAccount.Id);
        
        insert lims;
        insert lims2;
        
        Test.startTest();
        staticResource sr = new staticResource();
        
        
        PageReference page = ApexPages.currentPage();
        page.getParameters().put('id', testAccount.Id);
        page.getParameters().put('det','{status=P,%20species=CANINE,%20doctor=BANK,BLOOD%20,%20name=SHYLA,%20remarks=null,%20reqText=null,%20requisition=23525473,%20accession=7700492522,%20order%20date=2016-06-29,%20owner=CARNEY,%20lims=LYNXX,%20limsActive=Y,%20source=LabREXX,%20guid=,%20assay=%20CHEM%2025%20w/%20SDMA%20|%20CBC%20COMPREHENSIVE%20|%20LAB%204Dx%20PLUS%20|%20BRUCELLA%20CANIS%20(IFA)%20|%20BABESIA%20SP%20by%20RealPCR%20|%20BABESIA%20CANIS%20TITER%20|,%20code=111%20300%207244%20702%202633%20832%20,%20tests=HEALTHCHEK%20+4DX+BAB+BRUC%20}');
        
        test.setCurrentPage(page);    
        
        ApexPages.StandardController stdAct = new
            ApexPages.StandardController(testAccount);
        
        Test.setMock(WebServiceMock.class, new Test_AsyncLabOrders_Mock());
        
        LabOrdersDetailExtension testCont =
            new LabOrdersDetailExtension(stdAct);
        
        Continuation cont = (Continuation)testCont.detailRequest();
        Map<String, HttpRequest> requests = cont.getRequests();
        
        try{    
            sr = [Select Body From StaticResource Where Name = 'LabOrdersDetailsResponse' LIMIT 1];
        }
        catch(system.DmlException ex){
            
        }
        HttpResponse response = new HttpResponse();
        
        response.setBodyAsBlob(sr.Body);
        String requestLabel = requests.keyset().iterator().next();
        Test.setContinuationResponse(requestLabel, response);
        Object result = Test.invokeContinuationMethod(testCont, cont);
        System.debug(testCont.result);


        Test.stopTest();
        
    }


}