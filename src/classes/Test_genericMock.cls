@isTest
global class Test_genericMock implements HttpCalloutMock {
    staticResource sr = new staticResource();
    
    public String callType{get;set;}
    
    global HTTPResponse respond(HTTPRequest req) {
        try{
            if(callType == 'service'){
            sr = [Select Body From StaticResource Where Name = 'serviceInstrumentsResponse' LIMIT 1];
            }
            if(callType == 'images'){
                  sr = [Select Body From StaticResource Where Name = 'labImagesResponse' LIMIT 1];
            }
        }
        catch(exception ex){
            
            system.debug('error '+ ex.getCause());
        }
        
        String s = sr.body.toString();
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(s);
        res.setStatusCode(200);
        return res;
    }
}