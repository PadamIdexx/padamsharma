/* Class Name   : InlineCampaignsRelatedListController
 * Description  : Controller class to the InlineCampaignsRelatedList VF page. 
                  Is invoked when the Account record is viewed in the UI.
                  pulls all the campaigns related to the current account 
                  (All the campaigns having campaign members as the contacts to the Account record) and displays it on the Account detail page.
 * Created By   : Sirisha Kodi
 * Created On   : 09-07-2015
 
 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                        Date                     Modification ID              Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Sirisha Kodi                    09-07-2015                1000                     Initial version
 * Sirisha Kodi                    09-22-2015                1001                     Added Null Check at Line 53
 * Vishal Negandhi                 09-23-2015                1002                     Added a field Status in SOQL on line  57
 * Sirisha Kodi                    11-05-2015                1003                     Added an additional filter criteria as per the US-3139
 * Rama Dhulipala                  01-06-2017                1004                     Added an additional SOQL as per the US26573 for ISR profiles
                                                                                                                                                             
*/

public with sharing class InlineCampaignsRelatedListController 
{
    // campaign records
    public List<CampaignMember>                relatedCampaigns   {get;set;}
    public List<CampaignMember>                relatedCampaignsList   {get;set;}

    // List to store the custom message
    public List<String>                  noRecordsFound     {get;set;}
    // the current Account record
    public Account acc {get;set;}   
    
    private Account                      currentAccountRecord;
    //default constructor
    
    static final String DECLINED_CM_STATUS =  'Declined'; 
    static final String FULFILLED_CM_STATUS = 'Fulfilled';
    private integer counter=0;  //keeps track of the offset
    private integer list_size=20; //sets the page size or number of rows
    public integer total_size; //used to show user the total size of the list
    
    public Boolean isAdmin {get; set;}
    Map<String,Profile_Name__c> mapProfileName ;
    public ApexPages.StandardSetController con{get; set;} 
    public List<CampaignMember> lstRelatedCampaigns {get;set;}
    
    public InlineCampaignsRelatedListController(ApexPages.StandardController controller)
    {
    
        //Initializations
        this.currentAccountRecord = [Select Id, Primary_Contact__c From Account Where Id =: controller.getId()];
        relatedCampaigns          = new List<CampaignMember>();
        relatedCampaignsList      = new List<CampaignMember>();

        
        total_size = [select count() from CampaignMember where Contact.AccountId =:currentAccountRecord.Id]; //set the total size in the constructor 
        isAdmin = false;
        
        lstRelatedCampaigns = new List<CampaignMember>();
        
        // added below code to get All ISR profile name from custom setting
        User usrObj = [select profile.Name from User where id =:UserInfo.getUserId() ];
        mapProfileName = new Map<String,Profile_Name__c>();
        For (Profile_Name__c profileName : Profile_Name__c.getall().Values() )
        {
            mapProfileName.put(profileName.name ,profileName);
        }
        
        if( mapProfileName.containsKey(usrObj.profile.Name) )
        {
            isAdmin = false;
        }
        else
        {
            isAdmin = true;
        }
    }
    
    /*
    * Method name     : loadCampaigns
    * Description     : pulls all the campaign records that contains campaign members (contact) related to the current Account
    * Return Type     : void
    * Input Parameter : nil
    * Modification ID : 1001
    */
    public void loadCampaigns()
    {
        
        //A relationship query to pull the campaign records based on a filter condition on the campaign member record
        if(currentAccountRecord.Id<>null)
        //MOD 1003 : Enhancement Request: This VF Page should only include the Campaigns that are Active and where the Campaign Member Status is not Rejected or Fulfilled
        //MOD 1004 : Enhancement Request: Additional SOQLquery added with filters (Business Unit = 'CAG' and Type = 'ISC Initiative'
        if(relatedCampaigns.isEmpty())
        {
        
            // Below code is used to ISR profile
            if( isAdmin == false )
            {
                String businessType = Label.CampaignBusinessUnit;
                List<String> lstBusinessType = businessType.split(',');
                
                /*
                    relatedCampaigns = [Select CampaignId, Status,
                                           Campaign.Name, Campaign.Status, Campaign.StartDate, Campaign.Type 
                                           from CampaignMember
                                           where Contact.AccountId =:currentAccountRecord.Id
                                           and status!=: DECLINED_CM_STATUS and status!=: FULFILLED_CM_STATUS AND Campaign.isActive = true  
                                           and Campaign.Business_Unit__c in :lstBusinessType
                                           ORDER BY Campaign.Type,Campaign.StartDate DESC
                                           LIMIT : Limits.getLimitQueryRows()- Limits.getQueryRows()];
                
                */
                
                List<CampaignMember> TempRelatedCampaigns = [Select CampaignId, Status,
                                           Campaign.Name, Campaign.Status, Campaign.StartDate, Campaign.Type 
                                           from CampaignMember
                                           where Contact.AccountId =:currentAccountRecord.Id
                                           and status!=: DECLINED_CM_STATUS and status!=: FULFILLED_CM_STATUS AND Campaign.isActive = true  
                                           and Campaign.Business_Unit__c in :lstBusinessType
                                           ORDER BY Campaign.Type,Campaign.StartDate DESC
                                           LIMIT : Limits.getLimitQueryRows()- Limits.getQueryRows()];
                
                List<CampaignMember> listISCInitiative    = new List<CampaignMember>();
                List<CampaignMember> listNonISCInitiative = new List<CampaignMember>();
                
                for( CampaignMember cm : TempRelatedCampaigns )
                {
                    if(cm.Campaign.Type == 'ISC Initiative' )
                    {
                        listISCInitiative.add(cm);
                    }
                    else
                    {
                        listNonISCInitiative.add(cm);
                    }
                }
                
                if(listISCInitiative.size() > 0 )
                {
                    relatedCampaigns.addAll(listISCInitiative);
                }    
                if(listNonISCInitiative.size() > 0 )
                {
                    relatedCampaigns.addAll(listNonISCInitiative);
                }
            }
            else
            {
                relatedCampaigns = [Select CampaignId, Status,
                                            Campaign.Name, Campaign.Status, Campaign.StartDate, Campaign.Type 
                                            from CampaignMember
                                            where Contact.AccountId =:currentAccountRecord.Id
                                            and status!=: DECLINED_CM_STATUS and status!=: FULFILLED_CM_STATUS AND Campaign.isActive = true  ORDER BY     Campaign.StartDate DESC
                                            LIMIT : Limits.getLimitQueryRows()- Limits.getQueryRows()];
                                           
                                            System.debug('limits.getLimitQueryRows()= ' + limits.getLimitQueryRows());
                                            System.debug('limits.getQueryRows()= ' + limits.getQueryRows());
            }
        }
        
        // if there are no records, add the message to the list
        if(relatedCampaigns.isEmpty())
            noRecordsFound = new List<String>{system.Label.No_records};
    }


}