/* Class Name  : ResendAllConfirmationEmail
* Description : Controller class to trigger order deletion based on order reference number available on Order
* Created By  : Anudeep Gopagoni
* Created On  : 11-17-2015
*
* Modification Log:  
* --------------------------------------------------------------------------------------------------------------------------------------
* Developer              Date                   Modification ID       Description 
* ---------------------------------------------------------------------------------------------------------------------------------------
* Anudeep Gopagoni      11-17-2015               n/a                   Initial Version
* Anudeep Gopagoni      12-29-2015               1001                  Modified code to handle Callout Exception
*                                          
*/
global class ResendAllConfirmationEmail{
    Static String MessageType; 
    public static String[] cd;
    Public static List<Order> ordList = new List<Order>();  
    Public static List<Order> ordId = new List<Order>(); 
    Public static List<Order> ordId1 = new List<Order>();
    Public static List<Order> ordId2 = new List<Order>();
    public static List<ORDER_HISTORY__x > accList = new List<ORDER_HISTORY__x >(); 
    public static List<Id> cagordList = new List<Id>();
    Static String OrderRefNum = '';
    Static String DeliverNum = ''; 
    webService static String resendconfirmation_order(String OrderId) {
        OrderRefNum = OrderId;
        idexxiComSapxiPocbeaCustomer360.HTTPS_Port  ob = new idexxiComSapxiPocbeaCustomer360.HTTPS_Port();
        idexxiComSapxiPocbeaCustomer360.MessagesReturn  returntype = new idexxiComSapxiPocbeaCustomer360.MessagesReturn(); 
        try {
                returntype = ob.OrderConfirmationResend(OrderRefNum,'EN','test');
        }catch(Exception e) {
            MessageType = 'Failure';
        }
        String s = String.valueOf(returnType);
           System.debug('response is'+ s);
        cd = s.split(',');
        String K = '(Messages:[Message=Output is already pending for document'+ ' '+OrderRefNum;
        String J = OrderRefNum.LeftPad(10, '0');
        String L = 'Messages=(Messages:[Message=Object key' + ' ' + J +' '+'not found'; 
        String M = 'Messages=(Messages:[Message=No output exists for document' + ' ' +OrderRefNum;
        String N = 'Messages=(Messages:[Message=Order confirmation can only be sent for TA order type';
        for(Integer i=0;i<cd.size();i++)
        {
            if(cd[i].contains('IsError=false'))
            { 
                MessageType = 'success'; 
                break; 
                
            }else if (cd[i].contains(L))
                
            {
                MessageType = 'Failure2';
            }
            else if (cd[i].contains(K))
            {
                MessageType = 'Failure6';
            }
            else if (cd[i].contains(M))
            {
                MessageType = 'Failure3';
            }
            else if (cd[i].contains(N))
            {
                MessageType = 'Failure11';
            }
        }
        return MessageType;
        return null;
    }
    webService static String resendconfirmation_delivery(String DNum) {
        if(DNum!='') {
            DeliverNum = DNum;
        }else {
            MessageType = 'FailureDelivery';
        }
        idexxiComSapxiPocbeaCustomer360.HTTPS_Port  ob = new idexxiComSapxiPocbeaCustomer360.HTTPS_Port();
        idexxiComSapxiPocbeaCustomer360.MessagesReturn  returntype = new idexxiComSapxiPocbeaCustomer360.MessagesReturn(); 
        try{
                returntype = ob.DeliveryConfirmationResend(DeliverNum,'EN','test');
        }catch(Exception e) {
            MessageType = 'Failure';
        }
        String s = String.valueOf(returnType);
        cd = s.split(',');
        String N = 'Messages=(Messages:[Message=Output is already pending for document'+ ' ' +DeliverNum;
        String A = 'Messages=(Messages:[Message=Object key  not found';        
        for(Integer i=0;i<cd.size();i++)
        {
            if(cd[i].contains('IsError=false'))
            { 
                MessageType = 'success'; 
                break; 
                
            }else if(cd[i].contains(N)) 
            {
                MessageType = 'Pending';
            }else if(cd[i].contains(A)) {
                MessageType = 'ObjectKeyFailure';
            }
            
        }
        return MessageType;
        return null;
    }
    webService static String resendOrdConfFromOrderDetail(String OrderId) {
        if(OrderId!='' && OrderId!=null) {
            ordId2 = [Select OrderReferenceNumber FROM Order where Id=:OrderId LIMIT 1]; 
            if(ordId2.size()>0) {
                OrderRefNum = String.valueOf(ordId2[0].OrderReferenceNumber);
            }else {
                MessageType = 'Failure1';
            }
            idexxiComSapxiPocbeaCustomer360.HTTPS_Port  ob = new idexxiComSapxiPocbeaCustomer360.HTTPS_Port();
            idexxiComSapxiPocbeaCustomer360.MessagesReturn  returntype = new idexxiComSapxiPocbeaCustomer360.MessagesReturn(); 
            try {
                if(OrderRefNum!=null && OrderRefNum!='') {
                        returntype = ob.OrderConfirmationResend(OrderRefNum,'EN','test');
                }
            }catch(Exception e) {
                MessageType = 'Failure';
            }
            String s = String.valueOf(returnType);
             cd = s.split(',');
            String K = '(Messages:[Message=Output is already pending for document'+ ' '+OrderRefNum;
            String J = OrderRefNum.LeftPad(10, '0');
            String L = 'Messages=(Messages:[Message=Object key' + ' ' + J +' '+'not found'; 
            String M = 'Messages=(Messages:[Message=No output exists for document' + ' ' +OrderRefNum;
            String N = 'Messages=(Messages:[Message=Order confirmation can only be sent for TA order type';
            for(Integer i=0;i<cd.size();i++)
            {
                if(cd[i].contains('IsError=false'))
                { 
                    MessageType = 'success'; 
                    break; 
                }else if (cd[i].contains(L))
                    
                {
                    MessageType = 'Failure2';
                }
                else if (cd[i].contains(K))
                {
                    MessageType = 'Failure6';
                }
                else if (cd[i].contains(M))
                {
                    MessageType = 'Failure3';
                }
                else if (cd[i].contains(N))
                {
                    MessageType = 'Failure11';
                }
                
            }
        }
        return MessageType;
        return null;
    }
    webService static String resendDelConfFromOrderDetail(String OrderId) {
        ordId1 = [Select OrderReferenceNumber, AccountId  FROM Order where Id=:OrderId]; 
        try {
            if(!Test.isRunningTest()) {
                accList = [Select Delivery__c, SalesOrder__c from ORDER_HISTORY__x where SalesOrder__c=:ordId1[0].OrderReferenceNumber and SoldTo__c =:ordId1[0].AccountId LIMIT 1];
            } else {
                accList = new List<ORDER_HISTORY__X>{new ORDER_HISTORY__x(Delivery__c= '8000004770', SalesOrder__c = '1438916353')};
            }
        }
        catch(Exception e) {
            MessageType = 'Failure';
        }
        if(accList.size() > 0) {
            if(accList!=null && accList.size() > 0) {
                if(accList[0].Delivery__c!='') {
                    DeliverNum = String.valueOf(accList[0].Delivery__c);
                }else {
                    MessageType = 'FailureDelivery';
                }
            }else {
                MessageType = 'Failure4';
            }
        } else {
            MessageType = 'Failure5';
        }
        idexxiComSapxiPocbeaCustomer360.HTTPS_Port  ob = new idexxiComSapxiPocbeaCustomer360.HTTPS_Port();
        idexxiComSapxiPocbeaCustomer360.MessagesReturn  returntype = new idexxiComSapxiPocbeaCustomer360.MessagesReturn(); 
        try{
                returntype = ob.DeliveryConfirmationResend(DeliverNum,'EN','test');
        }catch(Exception e) {
            MessageType = 'Failure';
        }
        String s = String.valueOf(returnType);
        cd = s.split(',');
        String N = 'Messages=(Messages:[Message=Output is already pending for document'+ ' ' +DeliverNum;
        String A = 'Messages=(Messages:[Message=Object key  not found';
        for(Integer i=0;i<cd.size();i++)
        {
            if(cd[i].contains('IsError=false'))
            { 
                MessageType = 'success'; 
                break; 
                
            }else if(cd[i].contains(N)) 
            {
                MessageType = 'Pending';
            }else if(cd[i].contains(A)) {
                MessageType = 'ObjectKeyFailure';
            }     
        }
        return MessageType;
        return null;
    }
}