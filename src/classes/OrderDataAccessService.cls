/*
 * DAO class for accessing Order related data in Salesforce and ORDER_HISTORY__x.
 */ 
public class OrderDataAccessService {
    
    /*
     * Retrieve Order History record(s) by the SAP Order Id.
     */ 
    public static List<ORDER_HISTORY__x> getOrderById(String orderId) {
	    List<ORDER_HISTORY__x> orderHistLst;
        
        //Since this is OData based data, there is no other easier way to test this than to mock up results
        if (!Test.isRunningTest()) {        
        	orderHistLst = [SELECT SalesOrder__c, SalesOrderItem__c, Delivery__c,Division__c,CreatedOn__c , DelvCreatedOn__c, TrackNum__c,
                                    PONumber__c, Material__c, CreatedBy__c, MaterialDesc__c,OrderQty__c,
                                    BatchNum__c , CarrierType__c, POType__c, Price__c,Plant__c,ShipTo__c, PGIDate__c,
                                    IncoTerm1__c,POTypeDesc__c,SalesDocTyp__c,SDocTypDesc__c FROM ORDER_HISTORY__x WHERE SalesOrder__c = :orderId ];
        } else {
            
            //Mock up one record for testing purposes
            orderHistLst = new List<ORDER_HISTORY__x>();
            ORDER_HISTORY__x test  = new ORDER_HISTORY__x(SalesOrder__c = orderId, CreatedOn__c = '12011999',Price__c= 120,OrderQty__c = 19.99,ShipTo__c='12345678',POType__c = 'BEA', Delivery__c = '123456',DelvCreatedOn__c = '20151223', PGIDate__c = '20140209');              
            orderHistLst.add(test);            
            
        }
        
        return orderHistLst;
    }
    
    /*
     * Obtain status (delivered, partially delivered, or not deliveved) from Order History for
     * a give order.
     */ 
    public OrderStatus getStatusOfOrder(String orderId) {
        
        OrderStatus status = new OrderStatus(orderId);        
        
        List<ORDER_HISTORY__x> orderHistLst = getOrderById(orderId);
        
        Integer deliveryCount = 0;
        
        if ((orderHistLst != null) && (orderHistLst.size() > 0)) {
            
            status.submittedToSAP = true;
            
            for (ORDER_HISTORY__x hist : orderHistLst) {            
                if (hist.Delivery__c != null) {            
            		deliveryCount++;
                }
            }
            
            if (orderHistLst.size() == deliveryCount) {
                status.delivered = true;
            } else {
                if (deliveryCount > 0) {
                    status.partiallyDelivered = true;
                } 
            }        	    
        } 
        
        return status;
        
    }
}