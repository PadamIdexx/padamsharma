/* Class Name   : Test_AccountTriggerHandler
 * Description  : Test Class with unit test scenarios to cover the AccountTriggerHandler class
 * Created By   : Anudeep Gopagoni
 * Created On   : 10-22-2015
 
* Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer              Date                   Modification ID       Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Anudeep Gopagoni           10-22-2015             1000                   Initial version
 * Pooja Wade                 04-01-2016             1001                   For updates made during R2
   Aditya Sangawar            02-02-2016             1002                   Added testMethod caseCreationOtherCurrency()  
   Heather Kinney             3.15.2017                                     For a successful PROD deployment of code & tests, commented
                                                                            the method that tests geocode ("testGeoCodeTrigger"), and 
                                                                            added additional population of TIN__c and Veterinary_License_Number__c fields
                                                                            - documented with 3.15.2017 datestamp for clarity. Also adding a 
                                                                            BillingCountry in the method "caseCreationOtherCurrency" to avoid a 
                                                                            Apex test class compilation error. It was blank before.
   Heather Kinney             4.12.2017                                     Added ShippingCountry to account creation since it will be a required field.
*/
@isTest(seeAllData=false)
public with sharing class Test_AccountTriggerHandler{
  
    static testMethod void validateCurrencyUpdationScenario1() {
        List<Region_Currency_Mapping__mdt> currencyMapList= [SELECT MasterLabel,Currency_ISO_Code__c FROM Region_Currency_Mapping__mdt LIMIT 1];
        List<Account> accList =  new List<Account>();
        List<Account> accListupdate = new List<Account>(); 
        List<Contact> conListupdate = new List<Contact>(); 
        
        if(!currencyMapList.isEmpty()){
            String sId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
            Account testAccount = new Account();
            testAccount.RecordTypeId = sId;
            testAccount.Name = 'Customer Testing Account';
            testAccount.BillingCountryCode  = currencyMapList[0].MasterLabel; 
            testAccount.HIN__c = '9382098'; //3.15.2017 added
            testAccount.TIN__c = '9809809'; //3.15.2017 added
            testAccount.Veterinary_License_Number__c = '87987'; //3.15.2017 added
            accList.add(testAccount);
            
            Account testAccount2 = new Account();
            testAccount2.RecordTypeId = sId;
            testAccount2.BillingCountryCode ='SY';
            testAccount2.Name = 'Customer Testing No Country';
            testAccount.HIN__c = '9382098'; //3.15.2017 added
            testAccount.TIN__c = '9809809'; //3.15.2017 added
            testAccount.Veterinary_License_Number__c = '87987'; //3.15.2017 added
            accList.add(testAccount2);
            INSERT accList;
            List<Account> accInserted = [SELECT Id, CurrencyIsoCode, Name FROM Account WHERE Name = 'Customer Testing Account' LIMIT 1];
            System.assertEquals(currencyMapList[0].Currency_ISO_Code__c, accInserted[0].CurrencyIsoCode, 'Currency ISO Code don\'t match');
            
            
            //List<Contact> contQuery = [SELECT Id from Contact where AccountId!=null LIMIT 1];
            //Assign a primary contact the Account
            //Create a test contact
            Contact testContact = CreateTestClassData.reusableContact(testAccount.Id);
            Contact testContact2 = CreateTestClassData.reusableContact(testAccount2.Id);
            conListupdate.add(testContact);
            conListupdate.add(testContact2);
            insert conListupdate; 
            
            testAccount.BillingCountryCode = 'CH'; 
            testAccount.Primary_Contact__c = testContact.Id;
            accListupdate.add(testAccount); 
            testAccount2.BillingCountryCode='SY';
            testAccount2.Primary_Contact__c = testContact2.Id;
            accListupdate.add(testAccount2); 
            update accListupdate; 
            List<Account> accUpdated = [SELECT Id, CurrencyIsoCode, Name FROM Account WHERE Name = 'Customer Testing Account' LIMIT 1];
            System.assertEquals(accUpdated[0].CurrencyIsoCode, 'CHF', 'Currency Matches'); 
   
        }
    }
    
    //adding for R2
    static testMethod void caseCreation(){
      Profile p = [SELECT Id FROM Profile WHERE Name='Inside Sales Rep-CAG']; 
      User u = new User(Alias = 'ISR-CAG', Email='isr@test.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='isrcag@testorg.com');
      
      System.runAs(u){
        String CAG_PROSPECT_RECTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CAG Prospect').getRecordTypeId();
        Account testAccount = new Account();
        testAccount.RecordTypeId = CAG_PROSPECT_RECTYPEID;
        testAccount.Name = 'Customer Testing Account';
        testAccount.BillingCountry = 'United States';
        testAccount.BillingStreet = 'test Street';
        testAccount.BillingCity ='testCity';
        testAccount.BillingState = 'Alabama';
        testAccount.BillingPostalCode = '100456';
        testAccount.Type = 'Prospect';
        testAccount.Account_Type_Description__c = 'Bottler';
        testAccount.Customer_Size_Description__c = 'Small';
        testAccount.CurrencyIsoCode = 'USD';
        testAccount.Phone = '36853';
        testAccount.HIN__c ='A100';
        testAccount.TIN__c = '9809809'; //3.15.2017 added
        testAccount.Veterinary_License_Number__c = '87987'; //3.15.2017 added
        testAccount.ShippingCountry = 'United States';
        insert testAccount;
          
      }
    
    }

     //adding for R2
    static testMethod void caseCreationOtherCurrency(){
      Profile p = [SELECT Id FROM Profile WHERE Name='Inside Sales Rep-CAG']; 
      User u = new User(Alias = 'ISR-CAG', Email='isr@test.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='isrcag@testorg.com');
      
      System.runAs(u){
        String CAG_PROSPECT_RECTYPEID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CAG Prospect').getRecordTypeId();
        Account testAccount = new Account();
        testAccount.RecordTypeId = CAG_PROSPECT_RECTYPEID;
        testAccount.Name = 'Customer Testing Account';
        //3.15.2017 commented out original line below that was sending a blank BillingCountry. Resulted in an Apex test class execution
        //          error up in PROD when validating.
        //          System.DmlException: Insert failed. First exception on row 0; first error: FIELD_INTEGRITY_EXCEPTION, 
        //          A country/territory must be specified before specifying a state value for field: [BillingState] 
        //          Stack Trace: Class.Test_AccountTriggerHandler.caseCreationOtherCurrency: line 127, column 1
        //testAccount.BillingCountry = '';
        testAccount.BillingCountry = 'United States'; //3.15.2017 added - a non-blank billing country
        testAccount.BillingStreet = 'test Street';
        testAccount.BillingCity ='testCity';
        testAccount.BillingState = 'Alabama';
        testAccount.BillingPostalCode = '100456';
        testAccount.Type = 'Prospect';
        testAccount.Account_Type_Description__c = 'Bottler';
        testAccount.Customer_Size_Description__c = 'Small';
        testAccount.CurrencyIsoCode = 'INR';
        testAccount.Phone = '36853';
        testAccount.HIN__c ='A100';
        testAccount.TIN__c = '9809809'; //3.15.2017 added
        testAccount.Veterinary_License_Number__c = '87987'; //3.15.2017 added
        testAccount.ShippingCountry = 'United States';
        insert testAccount;
          
      }
    
    }
    
    //3.15.2017 - commented out this method since we currently don't have geocoding active in PROD. This may change in
    //            the future, but for now the below method is commented.
    /*
    static testMethod void testGeoCodeTrigger() {

        Test.startTest();

        List<Account> accList =  new List<Account>();

        Test.setMock(HttpCalloutMock.class,
                     new Test_GeocodeAddressServiceMock());

        Account testAccount = CreateTestClassData.createCustomerAccount();
        testAccount.Name = 'account1';
        testAccount.ShippingStreet = '1 Idexx Dr';
        testAccount.ShippingCity = 'Westbrook';
        testAccount.ShippingState = 'Maine';
        testAccount.ShippingCountry = 'United States';
        testAccount.ShippingPostalCode = '04092';
        accList.add(testAccount);

        Account testAccount2 = CreateTestClassData.createCustomerAccount();
        testAccount2.Name = 'account2';
        testAccount2.ShippingStreet = '60 Court St';
        testAccount2.ShippingCity = 'Auburn';
        testAccount2.ShippingState = 'Maine';
        testAccount2.ShippingCountry = 'United States';
        testAccount2.ShippingPostalCode = '04210';
        accList.add(testAccount2);
        UPSERT accList;

        // Confirm that the geocode trigger fired?

        // Now disable the trigger and try again.
        DisableSettings__c disableSettings2 = DisableSettings__c.getInstance();
        disableSettings2.Disable_Triggers__c = true;
        upsert disableSettings2;

        testAccount2.ShippingCity = 'Portland';
        update testAccount2;

        // Now enable the trigger and try again.
        disableSettings2.Disable_Triggers__c = false;
        upsert disableSettings2;

        testAccount2.ShippingCity = 'South Portland';
        update testAccount2;

        Test.stopTest();
    }
    */

}