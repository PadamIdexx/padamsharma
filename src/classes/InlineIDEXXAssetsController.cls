/**
*       @author Aditya Sangawar
*       @date   14/12/2015  
        @description   This class is used to display the Assets (Competitor Asset & IDEXX Asset) as two seperate inline VF pages. 
        Function:      Class used to display the Competitor Asset & IDEXX Asset that will appear in the related list of an account. 
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
       Aditya Sangawar                   14/12/2015         Created
       Aditya Sangawar                   13/01/2016         Modified - Added Criteria "Is_Active__c == true"
       Annie Magdziarz                   13/01/2016         Modified - Removed Criteria "Is_Active__c == true" from Competitor Assets
       Mansi Maini                       21/01/2016         Modified - Added Criteria 'IsCompetitorProduct = FALSE' for Idexx Assets
**/
public with sharing class InlineIDEXXAssetsController {
    
    
    private  ApexPages.StandardController                   controller                 {get;set;}//added an instance varaible for the standard controller                                        errorMsg                   {get;set;}// List to store the custom message
    public   Account                                         currentAccountRecord;// local variable that points to the account record 
    public   List<Asset> AssetList {get;set;}
    public   List<Asset> lstAssetCompetitive {get;set;}
    public   List<Asset> lstAssetIDEXX {get;set;}
    private string AccountId;
    
    //constuctor 
   public InlineIDEXXAssetsController(ApexPages.StandardController controller){  
        //Initializations of the local variables
       this.controller = controller;  
       AssetList  = new List<Asset>();
       lstAssetCompetitive = new List<Asset>();
       lstAssetIDEXX = new List<Asset>();
       AccountId = '';
       AccountId = controller.getId();
       //FetchAssets(controller.getId());
    }
    
     /*
    * Method name     : FetchAssets
    * Description     : The Method fetches and stores all the assets records
    * Return Type     : void
    * Input Parameter : nil
    */
   public void FetchAssets(){
    try{
          // if(currentAccountRecord.SAP_CUSTOMER_NUMBER__c <> null && !Test.isRunningTest())
           //Query for all the Asset records related to the current account object
         for(Asset AsVar : [select Asset.AccountId,SAP_CUSTOMER_NUMBER__c,Name,ID__c,RecordType.DeveloperName,Asset_Type__c,Asset_Category__c,Description,SerialNumber,Coverage_Agreement__c,Coverage_End_Date__c,Vendor__c,IsCompetitorProduct,Open_Issue__c,Is_Active__c
                                                from Asset WHERE AccountId = :AccountId]){
                             // where SAP_CUSTOMER_NUMBER__c =:currentAccountRecord.SAP_CUSTOMER_NUMBER__c LIMIT : Limits.getLimitQueryRows()- Limits.getQueryRows()]);
            if(AsVar.IsCompetitorProduct == true && AsVar.IsCompetitorProduct != null){
            //if((AsVar.IsCompetitorProduct == true  && AsVar.Is_Active__c == true) || (AsVar.IsCompetitorProduct == true  && AsVar.Is_Active__c == false)  ){}
               
                lstAssetCompetitive.add(AsVar);   
                system.debug('@@@@ Print Competative Asset ' + lstAssetCompetitive);      
            }
            else{
                //if(AsVar.IsCompetitorProduct != true && AsVar.Is_Active__c != false){
                if((AsVar.IsCompetitorProduct == false && AsVar.Is_Active__c == true) || (AsVar.IsCompetitorProduct == false && AsVar.Is_Active__c == null)){
                
                system.debug('@@@@123 Print IDEXX Asset AsVar ' + AsVar);
                lstAssetIDEXX.add(AsVar);
                 system.debug('@@@@123 Print IDEXX Asset lstAssetIDEXX' + lstAssetIDEXX);
                }
            }
             
          }      
    }catch(Exception ex){
                system.debug('*** exception details'+ex.getStackTraceString());
                //errorMsg = TECHNICAL_DETAILS+ex.getMessage();
            }
    }
}