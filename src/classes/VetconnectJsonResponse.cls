/************
Wrapper class to parse json response
************/
public class VetconnectJsonResponse {

    public class RelatedSapIds1 {
    }

    public String sapId;
    public Boolean active;
    public Integer activeUserCount;
    public List<RelatedSapIds1> relatedSapIds1;

    
    public static VetconnectJsonResponse parse(String json) {   
        return (VetconnectJsonResponse) System.JSON.deserialize(json, VetconnectJsonResponse.class);
    }
}