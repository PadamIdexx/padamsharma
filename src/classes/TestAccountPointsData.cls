/**
*       @author Vishal Negandhi
*       @date   13/01/2016   
        @description    Test class for AccountPointsData
        Function: This test class will test the functionality of AccountPointsData.
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Vishal Negandhi               13/01/2016          Original 
        Vishal Negandhi               15/01/2016          Modified
        
**/
@isTest(SeeAllData = False)
public with sharing class TestAccountPointsData {
    
    
    @isTest
    private static void withParentRecord(){
    
        Test.startTest(); 
        
       // System.debug('@@'+testParentChild);
        //System.assertEquals(testParentChild[0].ExternalId, '1001');
        
        String sampleResponse = '{"d":{"results":[{"__metadata": {"type":"sfdcservices","uri":"https://qareporting.idexx.com/idexxui/services/sfdc/sfdcservices.xsodata/POINT_TO_LEASE(\'194877680830839571\')"},"PTL":"194877680830839571","SOLDTO":"0000169115","SHIPTO":"0000012676","SALESORG":"USS1","DISCHNL":"00","DIVISION":"CP","CONTRACT":"0030085635","ITEM":"000010","PONUM":"25130420","POINTS":"541.26","CURRENCY":"USD"}]}}';
        Test_ReusableMockResponse fakeResponse = new Test_ReusableMockResponse(200,'SUCCESS',sampleResponse, new map<String,String>{'Content-Type'=>'application/json'});
        Test.setMock(HttpCalloutMock.class, fakeResponse); 
        // Account testAccount = [Select Id From Account WHERE Name = 'Customer Testing Account'];
        AccountPointsData pData = new AccountPointsData('123456');
        pData.parentRecord.add(new PARENT_CHILD__x(ChildCustomer__c='123456', ChildExclusion__c = 'A', ParentCustomer__c = '67890'));
        pData.expiryDate.add(new BANK_EXP__x(Customer__c = '67890', AvailBalance__c = 5, ExpiryDate__c = '11012099', ExpiredBal__c = 5));
        pData.fetchAccountPoints();
        Test.stopTest();
       
    }
    
    @isTest
    private static void withoutParent(){
       Test.startTest(); 
        
       // System.debug('@@'+testParentChild);
        //System.assertEquals(testParentChild[0].ExternalId, '1001');
        
        String sampleResponse = '{"d":{"results":[{"__metadata": {"type":"sfdcservices","uri":"https://qareporting.idexx.com/idexxui/services/sfdc/sfdcservices.xsodata/POINT_TO_LEASE(\'194877680830839571\')"},"PTL":"194877680830839571","SOLDTO":"0000169115","SHIPTO":"0000012676","SALESORG":"USS1","DISCHNL":"00","DIVISION":"CP","CONTRACT":"0030085635","ITEM":"000010","PONUM":"25130420","POINTS":"541.26","CURRENCY":"USD"}]}}';
        Test_ReusableMockResponse fakeResponse = new Test_ReusableMockResponse(200,'SUCCESS',sampleResponse, new map<String,String>{'Content-Type'=>'application/json'});
        Test.setMock(HttpCalloutMock.class, fakeResponse); 
        // Account testAccount = [Select Id From Account WHERE Name = 'Customer Testing Account'];
        AccountPointsData pData = new AccountPointsData('123456');
        //pData.parentRecord.add(new PARENT_CHILD__x(ChildCustomer__c='123456', ChildExclusion__c = 'A', ParentCustomer__c = '67890'));
        pData.expiryDate.add(new BANK_EXP__x(Customer__c = '67890', AvailBalance__c = 5, ExpiryDate__c = '11012099', ExpiredBal__c = 5));
        pData.fetchAccountPoints();
        Test.stopTest();
    }
    
   @isTest
    private static void method3(){
    
    
        Account acc = CreateTestClassData.createCustomerAccount();
        ApexPages.StandardController std = new ApexPages.StandardController(acc);
     AccountPointsData pData = new AccountPointsData(std);
     
    
    } 
}