@isTest
private class Test_VCPlus_LOB_Callout {
    public static List<Id> ids = new List<Id>();
    public static List<Account> accts = new List<Account>();
    
    
    @testSetup 
    static void createTestData(){
        
        accts = ([SELECT Id, Name FROM Account WHERE Name LIKE '%TestAccount%' ]);
        
    }
    
    @isTest
    static void testUpdate(){
        
        Test.setMock(HttpCalloutMock.class, new MockVetConnectPlusResponse());  
        
        User thisUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        Test_Data_Utility.createTestAssets(10, 2);
        
        createTestData();
        
        System.runAs(thisUser){
            Test.startTest();
            VCPlus_lob_callout.session = system.UserInfo.getSessionId();
            VCPlus_LOB_Callout.endpoint1 = 'https://devbeacon.idexx.com/beacon/sfdc/ApplicationServicesVCPLUS/ProxyServices/CommonTokenUtilVcPlusPS';
            VCPlus_LOB_Callout.custId = '?sapid=15613';  
            
            VCPLus_LOB_Callout myBatchObject = new VCPLus_LOB_Callout();

            Database.executeBatch(myBatchObject,20);
            
            
            System.debug('accounts ' + accts);
            
            Test.stopTest();            
            
        }
        
        
    }
    @isTest
    static void testUpdateBulk(){
        
        Test.setMock(HttpCalloutMock.class, new MockVetConnectPlusResponse());  
        
        User thisUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        Test_Data_Utility.createTestAssets(100, 2);
        
        createTestData();
		VCPlus_lob_callout.session = system.UserInfo.getSessionId();
        VCPlus_LOB_Callout.endpoint1 = 'https://devbeacon.idexx.com/beacon/sfdc/ApplicationServicesVCPLUS/ProxyServices/CommonTokenUtilVcPlusPS';
        VCPlus_LOB_Callout.custId = '?sapid=15613';
        VCPLus_LOB_Callout myBatchObject = new VCPLus_LOB_Callout();
        
        Test.startTest();
        Database.executeBatch(myBatchObject,200);
        
        System.debug('bulk accounts ' + accts.size());
        
        
        Test.stopTest();            
        
        
        
        
    }
    
    
    
    
}