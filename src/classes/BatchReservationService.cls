/**
 * @author Heather Kinney
 * @description 	Communicates with the SAP HANA endpoint BATCH_RESERVATIONS via a Named Credential and 
 * 					return BatchReservation information.
 *              
 *
 *  Modification Log:
 * -----------------------------------------------------------------------------------------------------------------------------
 *  Developer                       Date               Description
 * -----------------------------------------------------------------------------------------------------------------------------
 *  Heather Kinney                  08/17/2016         Initial creation.
 *
 */
public class BatchReservationService {
    //This is a Named Credential endpoint for the SAP HANA endpoint. Although it is named "PointsToLease" 
    //it's in fact agnostic and just represents the endpoint connection.
    String serviceEndPoint = 'callout:PointsToLease';
    
    //Path for BATCH_RESERVATIONS on SAP HANA. SAP_ID in this string resplaced at runtime. 
    String pathWithCountryCode 		= '/BATCH_RESERVATIONS?$filter=(Sold_To%20eq%20%27SAP_ID%27+or+Sold_To%20eq%20%27CountryCustomer%27)&(MAT)&$format=json';
    String pathWithoutCountryCode	= '/BATCH_RESERVATIONS?$filter=Sold_To%20eq%20%27SAP_ID%27&(MAT)&$format=json';  
    String pathWithJustSAPId 		= '/BATCH_RESERVATIONS?$filter=Sold_To%20eq%20%27SAP_ID%27&$format=json';  
    
	String endpoint = '';
    
    public static HttpResponse response;	//public so we can generate mock responses for testing.

    public List<BatchReservation> getBatchReservationForCustomer(String customerId) {
    	endpoint = serviceEndPoint + pathWithJustSAPId;
        endpoint = endpoint.replace('SAP_ID', customerId);
        System.debug('BatchReservationService.getBatchReservationForCustomer(String customerId) - endpoint - after all substitutions: ' + endpoint);  
        return sendRequestAndParse();
    }
    
    public List<BatchReservation> getBatchReservationForCustomer(String customerId, String countryCust, String materialNumber) {
        if (countryCust!='') {
            endpoint = serviceEndPoint + pathWithCountryCode;
            endpoint = endpoint.replace('CountryCustomer',countryCust); 
        } else {
            endpoint = serviceEndPoint + pathWithoutCountryCode;
        }
        endpoint = endpoint.replace('SAP_ID', customerId);
        endpoint = endpoint.replace('MAT', materialNumber);
        System.debug('BatchReservationService.getBatchReservationForCustomer(String customerId, String countryCust, String materialNumber) - endpoint - after all substitutions: ' + endpoint);

        return sendRequestAndParse();
    }
    
    private List<BatchReservation> sendRequestAndParse() {
    	
        List<BatchReservation> batchReservationsList = null;
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint(endpoint);        
        req.setMethod('GET');
        req.setHeader('sessionId', UserInfo.getSessionId());
        
        HttpResponse res;
        
        try {
            if (!Test.isRunningTest()) {
                res = h.send(req);
            } else { //we are in a test - obtain the response from a prepopulated object set in test mode.
            	res = response; 
            }
            
            System.debug('sendRequestAndParse - response: ' + res.toString());
            System.debug('sendRequestAndParse - status code: ' + res.getStatusCode());
            
            if (res.getStatusCode() == 200) {
                System.debug('getBatchReservationForCustomer(): ' + res.getBody());
                batchReservationsList = parseBatchReservation(res.getBody());        
                System.debug('batchReservationsList: ' + batchReservationsList);
            } else { //a non 200 status code represents a communication issue with the HANA endpoint for BATCH_RESERVATIONS.
                System.debug('BatchReservationService - sendRequestAndParse. Non-200 HTTP status code detected.');
                batchReservationsList = null;
                if (SystemLoggingService.validateInsertion()) {
                    List<String> integrationLogs = new List<String>();
                    integrationLogs.add(SystemLoggingService.concatenateFields('httpRequest', 'BatchReservationService', 'sendRequestAndParse', req.getBody(), 'BATCH_RESERVATIONS HANA endpoint'));
                    integrationLogs.add(SystemLoggingService.concatenateFields('httpResponse', 'BatchReservationService', 'sendRequestAndParse', res.getBody(), 'BATCH_RESERVATIONS HANA endpoint'));
                    systemLoggingService.logRequestResponse(integrationLogs, true);
                }
            }
        } catch (Exception e) {
            System.debug('BatchReservationService - sendRequestAndParse. Exception detected: ' + e.getCause() + ' ' + e.getMessage());
        	List<String> integrationLogs = new List<String>();
            integrationLogs.add(SystemLoggingService.concatenateFields('httpRequest', 'BatchReservationService', 'sendRequestAndParse', e.getCause() + ' ' + e.getMessage() , 'BATCH_RESERVATIONS HANA endpoint'));
            systemLoggingService.logRequestResponse(integrationLogs, true);    
        }
        
        return batchReservationsList; 
    }
    
    public void setResponse(HttpResponse httpResponse) {
        response = httpResponse;
    }
    
    
    public List<BatchReservation> parseBatchReservation(String body) {         
        List<BatchReservation> batchReservationList = new List<BatchReservation>();        
        //CURRENCY is a reserved word coming back from the HANA JSON response and cannot be used. Replaced so that it can be properly parsed to the object.
        body = body.replace('CURRENCY', 'curr');
        JSONParser parser = JSON.createParser(body);
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.START_ARRAY)) {
                while (parser.nextToken() != null) {
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                        BatchReservation p = (BatchReservation)parser.readValueAs(BatchReservation.class);
                        batchReservationList.add(p);
                    }
                } //next token !nulll...        
            }
        }
        return batchReservationList;
    }
    
    
    //Class to represent Batch Reservation data.
    public class BatchReservation {
        public String Sold_To {get;set;}
        public String Ship_To {get;set;}
        public String reservation {get;set;}
        public String Line_Item {get;set;}
        public String material {get;set;}
        public String batch {get;set;}
        public Integer availableQuantity {get;set;}
        public String Start_Date;
        public String End_Date;
        public String status {get;set;}
        public String Sales_Org;
        public String Dist_Channel;
        public String division;
        public String plant;
        public Integer Reserved_Qty {get;set;}
        public Integer Consumed_Qty {get;set;}

        public BatchReservation(){
            Sold_To = ''; 
            Ship_To = ''; 
            reservation = ''; 
            Line_Item = ''; 
            availableQuantity=0;
            material = ''; 
            batch = ''; 
            Start_Date = ''; 
            End_Date = ''; 
            status = ''; 
            Sales_Org = ''; 
            Dist_Channel = ''; 
            division = ''; 
            plant = ''; 
            Reserved_Qty = 0; 
            Consumed_Qty = 0; 
        }
        
        public BatchReservation(String sold, String ship, String res, String lItem, String mat, String batch, 
                                String sDate, String eDate, String status, String sOrg, String dChan, String div, String plant, Integer rQty, Integer cQty) {
         	Sold_To = sold;
         	Ship_To = ship;
         	reservation = res;
         	Line_Item = lItem;
         	material = mat;
         	batch = batch;
         	Start_Date = sDate;
         	End_Date = eDate;
         	status = status;
         	Sales_Org = sOrg;
         	Dist_Channel = dChan;
         	division = div;
         	plant = plant;
         	Reserved_Qty = rQty;
         	Consumed_Qty = cQty;
        }
        
    }
 
    
}