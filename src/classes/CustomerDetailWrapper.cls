/*
 * This class is a wrapper for the response from the SAP Customer Detail Service.
 * 
 * The long-term plan would be to add additional accessor methods for things in CustomerDetailResponse
 * as needs arise.
 */ 
public class CustomerDetailWrapper {
    private CustomerDetailResponse.CUSTOMER_GETDETAIL_Response detail {get;set;}
    
    public CustomerDetailWrapper(CustomerDetailResponse.CUSTOMER_GETDETAIL_Response d) {
        detail = d;
    }    
    
    /*
     * Payment terms wrapper.  Returns payment terms based on sales org and division
     */ 
    public String getPaymentTerms(String salesOrg, String division) {
        String paymentTerms = '';
        
        //validate input
        if (String.isEmpty(salesOrg) 
            || String.isEmpty(division)) {
                return paymentTerms;
        }
    
        for (CustomerDetailResponse.SALES_DATA_element sd : detail.SALES_DATA) {
            if (salesOrg.equalsIgnoreCase(sd.SALES_ORG)
               		&& division.equalsIgnoreCase(sd.DIVISION) ) {
                paymentTerms = sd.PAYMENT_TERMS;
                break;
            }
        }
        
        return paymentTerms;
    }

    
}