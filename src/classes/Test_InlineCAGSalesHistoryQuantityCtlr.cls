/**
*       @author Aditya Sangawar
*       @date   22/12/2015
        @description   Test Class for covering the Customer Sales History 'QUANTITY' Visual force controller.
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Aditya Sangawar                 06/01/2015          Created
        Heather Kinney                  05/31/2016          US23018 - Merging R2.1 changes into R3.0 - Ref Lab and Practice Supplies
**/

@isTest(SeeAllData = False)
public With Sharing class Test_InlineCAGSalesHistoryQuantityCtlr {

    // works as create scenario
    static testMethod void Test_CreateSalesHistoryQuantity() {

        List<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);

        Account acc = [select id,name from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryQuantityController testcontructor = new InlineCAGSalesHistoryQuantityController(controller);
        System.assertEquals(acc.name, 'Customer Testing Account');
    }

    static testMethod void Test_CreateCAGKits() { // works as update scenario

        List<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
        for(Customer_Sales_History__c tempCSH : lstCSH){
            tempCSH.PRODUCT_GROUP__c = 'RAPID ASSAY';
            tempCSH.PRODUCT_LINE__c = 'SNAP';
            tempCSH.PRODUCT_FAMILY__c = 'CAG SUMMARY2';
        }
        update lstCSH;

        Test.startTest();
        Account acc = [select id from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryQuantityController testcontructor = new InlineCAGSalesHistoryQuantityController(controller);
        List<Customer_Sales_History__c> CSH = [select PRODUCT_GROUP__c from Customer_Sales_History__c where PRODUCT_GROUP__c =: lstCSH[0].PRODUCT_GROUP__c];
        System.assertEquals(CSH[0].PRODUCT_GROUP__c, 'RAPID ASSAY');
        Test.stopTest();
    }

    /* static testMethod void Test_CreatKitsTotalrow() {

         // works as update scenario
            List<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
            for(Customer_Sales_History__c tempCSH : lstCSH){
                tempCSH.PRODUCT_GROUP__c = 'RAPID ASSAY';
                tempCSH.PRODUCT_LINE__c = 'SNAP';
                tempCSH.PRODUCT_FAMILY__c = 'CAG KITS TOTAL';
            }
            update lstCSH;

            Account acc = [select id from Account where id =: lstCSH[0].Account__c];
            ApexPages.StandardController controller = new ApexPages.StandardController(acc);
            InlineCAGSalesHistoryQuantityController testcontructor = new InlineCAGSalesHistoryQuantityController(controller);

        }*/

    static testMethod void Test_ConsumablesTotal() {

        // works as update scenario
        List<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
        for(Customer_Sales_History__c tempCSH : lstCSH){
            tempCSH.PRODUCT_GROUP__c = 'RAPID ASSAY';
            tempCSH.PRODUCT_LINE__c = 'SNAP';
            tempCSH.PRODUCT_FAMILY__c = 'CAG CONSUMABLES TOTAL';
        }
        update lstCSH;

        Test.startTest();
        Account acc = [select id from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryQuantityController testcontructor = new InlineCAGSalesHistoryQuantityController(controller);
        List<Customer_Sales_History__c> CSH = [select PRODUCT_GROUP__c from Customer_Sales_History__c where PRODUCT_GROUP__c =: lstCSH[0].PRODUCT_GROUP__c];
        System.assertEquals(CSH[0].PRODUCT_GROUP__c, 'RAPID ASSAY');
        Test.stopTest();
    }

    static testMethod void Test_LabServices() {

        // works as update scenario
        List<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
        for(Customer_Sales_History__c tempCSH : lstCSH){
            tempCSH.PRODUCT_GROUP__c = 'RAPID ASSAY';
            tempCSH.PRODUCT_LINE__c = 'SNAP';
            tempCSH.PRODUCT_FAMILY__c = 'CAG CONSUMABLES TOTAL';
        }
        update lstCSH;

        Test.startTest();
        Account acc = [select id from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryQuantityController testcontructor = new InlineCAGSalesHistoryQuantityController(controller);
        List<Customer_Sales_History__c> CSH = [select PRODUCT_GROUP__c from Customer_Sales_History__c where PRODUCT_GROUP__c =: lstCSH[0].PRODUCT_GROUP__c];
        System.assertEquals(CSH[0].PRODUCT_GROUP__c, 'RAPID ASSAY');
        Test.stopTest();

    }

    static testMethod void Test_LabServicesTotal() {

        // works as update scenario
        List<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
        for(Customer_Sales_History__c tempCSH : lstCSH){
            tempCSH.PRODUCT_GROUP__c = 'RAPID ASSAY';
            tempCSH.PRODUCT_LINE__c = 'LAB SERVICES';
            tempCSH.PRODUCT_FAMILY__c = 'CAG TOTAL';
        }
        update lstCSH;

        Test.startTest();
        Account acc = [select id from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryQuantityController testcontructor = new InlineCAGSalesHistoryQuantityController(controller);
        List<Customer_Sales_History__c> CSH = [select PRODUCT_GROUP__c from Customer_Sales_History__c where PRODUCT_GROUP__c =: lstCSH[0].PRODUCT_GROUP__c];
        System.assertEquals(CSH[0].PRODUCT_GROUP__c, 'RAPID ASSAY');
        Test.stopTest();

    }

    //******* Newly added Methods ***************


    static testMethod void Test_Consumables() {

        // works as update scenario
        List<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
        for(Customer_Sales_History__c tempCSH : lstCSH){
            tempCSH.PRODUCT_GROUP__c = 'PROCYTE CONSUMABLES';
            tempCSH.PRODUCT_LINE__c =  'CHEMISTRY';
            tempCSH.PRODUCT_FAMILY__c = 'CAG TOTAL';
        }
        update lstCSH;

        Test.startTest();
        Account acc = [select id from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryQuantityController testcontructor = new InlineCAGSalesHistoryQuantityController(controller);
        List<Customer_Sales_History__c> CSH = [select PRODUCT_GROUP__c from Customer_Sales_History__c where PRODUCT_GROUP__c =: lstCSH[0].PRODUCT_GROUP__c];
        System.assertEquals(CSH[0].PRODUCT_GROUP__c, 'PROCYTE CONSUMABLES');
        Test.stopTest();

    }

    static testMethod void Test_LabServices2() {

        // works as update scenario
        List<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
        for(Customer_Sales_History__c tempCSH : lstCSH){
            tempCSH.PRODUCT_GROUP__c = 'ENDOCRINOLOGY';
            //tempCSH.PRODUCT_LINE__c =  'CHEMISTRY';
            //tempCSH.PRODUCT_FAMILY__c = 'CAG TOTAL';
        }
        update lstCSH;

        Test.startTest();
        Account acc = [select id from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryQuantityController testcontructor = new InlineCAGSalesHistoryQuantityController(controller);
        List<Customer_Sales_History__c> CSH = [select PRODUCT_GROUP__c from Customer_Sales_History__c where PRODUCT_GROUP__c =: lstCSH[0].PRODUCT_GROUP__c];
        System.assertEquals(CSH[0].PRODUCT_GROUP__c, 'ENDOCRINOLOGY');
        Test.stopTest();

    }

    static testMethod void Test_TeleMed() {

        // works as update scenario
        List<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
        for(Customer_Sales_History__c tempCSH : lstCSH){
            //tempCSH.PRODUCT_GROUP__c = 'ENDOCRINOLOGY';
            tempCSH.PRODUCT_LINE__c =  'TELEMED';
            //tempCSH.PRODUCT_FAMILY__c = 'CAG TOTAL';
        }
        update lstCSH;

        Test.startTest();
        Account acc = [select id from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryQuantityController testcontructor = new InlineCAGSalesHistoryQuantityController(controller);
        List<Customer_Sales_History__c> CSH = [select PRODUCT_LINE__c from Customer_Sales_History__c where PRODUCT_LINE__c =: lstCSH[0].PRODUCT_LINE__c];
        System.assertEquals(CSH[0].PRODUCT_LINE__c, 'TELEMED');
        Test.stopTest();

    }

    static testMethod void Test_SerologyImmunology() {

        // works as update scenario
        List<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
        for(Customer_Sales_History__c tempCSH : lstCSH){
            tempCSH.PRODUCT_GROUP__c = 'SEROLOGY/IMMUNOLOGY';
            //tempCSH.PRODUCT_LINE__c =  'CHEMISTRY';
            tempCSH.PRODUCT_FAMILY__c = 'Allergy';
        }
        update lstCSH;

        Test.startTest();
        Account acc = [select id from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryQuantityController testcontructor = new InlineCAGSalesHistoryQuantityController(controller);
        List<Customer_Sales_History__c> CSH = [select PRODUCT_GROUP__c from Customer_Sales_History__c where PRODUCT_GROUP__c =: lstCSH[0].PRODUCT_GROUP__c];
        System.assertEquals(CSH[0].PRODUCT_GROUP__c, 'SEROLOGY/IMMUNOLOGY');
        Test.stopTest();

    }

    //BEGIN - US23018 Ref Lab and Practice Supplies
    static testMethod void Test_CAGRefLabSupplies() {
        // works as update scenario
        List<Customer_Sales_History__c> lstCSH = CreateTestClassData.CreateCustomerSalesHistoryList(10);
        for(Customer_Sales_History__c tempCSH : lstCSH){
            tempCSH.PRODUCT_GROUP__c = 'REFERENCE LAB SUPPLIES';
            tempCSH.PRODUCT_FAMILY__c = 'LAB SERV/ACCESSORIES';
            tempCSH.PRODUCT_NAME__c = 'LAVENDER-TOP TUBE - 2ML, EDTA';
        }
        update lstCSH;

        Test.startTest();
        System.debug('lstCSH === > ' + lstCSH);
        Account acc = [select id from Account where id =: lstCSH[0].Account__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        InlineCAGSalesHistoryRevenueController testConstructor = new InlineCAGSalesHistoryRevenueController(controller);
        List<Customer_Sales_History__c> CSH = [select PRODUCT_GROUP__c from Customer_Sales_History__c where PRODUCT_GROUP__c =: lstCSH[0].PRODUCT_GROUP__c];
        System.debug('at this location?');
        System.debug('CSH = > ' + CSH);
        Test.stopTest();
    }

    //END - US23018 Ref Lab and Practice Supplies

}