@isTest
public class Test_CustomerPricingControllerExtension {
    public static List<Account> accts = new List<Account>();
    @testSetup 
    static void createTestData(){
        
        accts = ([SELECT Id, Name FROM Account WHERE Name LIKE '%TestAccount%' ]);
        
    }
    
    @isTest static void TestSimulatesync() {
        Test_Data_Utility.createTestAccounts(1, 0);
        SYSTEM.debug('dml 0 ' + Limits.getDMLStatements());
        IDEXX_Integration_Service_Properties__c customsettings = new IDEXX_Integration_Service_Properties__c();
        customsettings.Name = 'CUST_PRICE_INVOKE_SYNC';
        customsettings.TextVal__c = 'true';
        insert customsettings;
        SYSTEM.debug('dml 1 ' + Limits.getDMLStatements());
        createTestData();
        //Account 
        Account thisAccount = accts.get(0);
        
        List<String> devisionList = new List<String>();
        List<Salesview__c> salesview = new List<Salesview__c>();
        List<Account_Salesview__c> acctSalesView = new List<Account_Salesview__c>();
        List<Partner_Function__c> partner = new List<Partner_Function__c>();
        devisionList.add('CP');
        devisionList.add('VS');
        devisionList.add('VB');
        devisionList.add('VR');
        devisionList.add('CH');
        devisionList.add('VC');
        devisionList.add('CL');
        devisionList.add('BR');
        devisionList.add('XX'); 
        for(String devision :devisionList) {
            
            Salesview__c tempSV = new Salesview__c(Name = 'US Sales Org 1 / Customer POD', CurrencyIsoCode = 'USD', Sales_Organization_Code__c = 'USS1', Distribution_Channel_Code__c = '00', Sales_Division__c = devision, SalesOrg_DistChann_Division__c='USS100'+devision);
            salesview.add(tempSV);

        }
        insert salesview;
        integer i = 0;
        for(SalesView__c s : salesview){
            String devision = devisionList.get(i);       
                Account_Salesview__c  tempAccView = new Account_Salesview__c(Account__c = thisAccount.Id, SalesView__c = s.id, Name = 'USS1 / 00 / '+devision+' / 0000001003', CurrencyIsoCode = 'USD', SalesOrg_DistChann_Div_Cust__c = 'USS100'+devision+'0000001003', Incoterms1__c = '250' , SAP_Customer_Number__c = '0000001003', Marked_for_Deletion_Flag__c = false, IDEXX_Region_Code__c = 'USA' );
                acctSalesView.add(tempAccView);     
                        
            Partner_Function__c tempP = new Partner_Function__c(Name = '0000001003 / USS1 / '+devision+' / AG / 0000001003',Account__c = thisAccount.Id, Partner_Account__c = thisAccount.Id, Sales_Organization__c = 'USS1', Distribution_Channel__c = '00', Division__c = devision, Default_Partner__c = 'N',Partner_Function__c='AG', Partner_Function_Desc__c = 'Sold-to party', Salesview__c=s.Id, Cust_SlsOrg_DistChan_Div_PartFunc_Ctr__c = '0000001003USS100'+devision+'AG0' );
            partner.add(tempP);
            i++;
        }
        insert acctSalesView;
        insert partner;        
        Product2 product = new Product2(Family = 'Test', Name = 'SNAP PRO STARTER KIT', ProductCode = '99-0005630', Description = 'SNAP PRO STARTER KIT', IsActive = true, CurrencyIsoCode = 'USD', Base_Unit_of_Measure_Desc__c = 'EACH', Base_Unit_of_Measure__c = 'EA', Batch_Mgmt_Requirement__c = false, General_Item_Category_Group_Desc__c = 'Service w/o delivery', General_Item_Category_Group__c = 'LEIS', Gross_Weight__c = '0.000000000000000', Hierarchy_Family__c = 'UNALLOCATED', Hierarchy_Line__c = 'UNALLOCATED', Hierarchy_Name__c = 'UNALLOCATED', Hierarchy_Type__c = 'UNALLOCATED', Industry_Sector_Desc__c = 'PHARMACEUTICALS', Industry_Sector__c = 'P', Item_Category_Group_Code__c = 'ZTOP', Item_Category_Group_Desc__c = 'TOP LEVEL ASSEMBLY', Material_Division_Desc__c = 'POD IMMUNOASSAY', Material_Division_Group__c = 'IHD', Material_Division__c = 'VV', Material_Group_Desc__c = 'SNAP PRO', Material_Group__c = 'SNAP PRO', Material_Type_Desc__c = 'NON-STOCK MATERIAL', Material_Type__c = 'NLAG', Net_Weight__c = '0.000000000000000', Product_Hierarchy__c = 'AG', Quantity_Per_Package__c = '1.000000000000000', SAP_MATERIAL_NUMBER__c = '99-0001634', Weight_Unit_Desc__c = 'KILOGRAM', Weight_Unit__c = 'KG');
        insert product;
        SYSTEM.debug('dml 5 ' + Limits.getDMLStatements());
        System.debug('product'+product.Id);
        
        Id pricebookId = Test.getStandardPricebookId();
        Pricebook2 priceBook = new Pricebook2(Name = 'IDEXX Standard Price Book', isActive=true);
        insert priceBook;
        
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        
        PricebookEntry stdpriceBookEntry = new PriceBookEntry(Pricebook2Id = pricebookId, Product2Id = product.Id, CurrencyIsoCode = 'USD', UnitPrice = 10000.0, IsActive = true, PriceBook_SAPMatNr__c = 'Standard Price Book99-0001634USD');
        pricebookEntries.add(stdpriceBookEntry);
        
        PricebookEntry priceBookEntry = new PriceBookEntry(Pricebook2Id = priceBook.Id, Product2Id = product.Id, CurrencyIsoCode = 'USD', UnitPrice = 2000.0, IsActive = true, PriceBook_SAPMatNr__c = 'IDEXX Standard Price Book99-0001634USD');
        pricebookEntries.add(priceBookEntry);
        
        insert pricebookEntries;
        
        // initialize contoller
        CustomerPricingControllerExtension controller = new CustomerPricingControllerExtension(new ApexPages.StandardController(thisAccount));
        
        Test.startTest();
        controller.productSearch();
        controller.searchString = '99-0001634';
        Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = product.Id;
        Test.setFixedSearchResults(fixedSearchResults); 
        controller.productSearch();
        List<CustomerPricingControllerExtension.ProductsWithPriceHolder> currentResults = controller.results;
        for(CustomerPricingControllerExtension.ProductsWithPriceHolder cuentResult : currentResults) {
            cuentResult.isSelected = true;                                              
        }
        TestCodeSalesProcessingSalesforce.SalesOrderSimulateResponse_Sync myresponse = new TestCodeSalesProcessingSalesforce.SalesOrderSimulateResponse_Sync();
        List<TestCodeSalesProcessingSalesforce.ResponseItems_element> responseItemsList = new List<TestCodeSalesProcessingSalesforce.ResponseItems_element>();
        TestCodeSalesProcessingSalesforce.ResponseItems_element ResponseItem = new TestCodeSalesProcessingSalesforce.ResponseItems_element();
        ResponseItem.ItemNumber  = '000010';
        ResponseItem.ExternalItemNumber  = '1'; 
        ResponseItem.Material  = '99-0001634';
        ResponseItem.SpeedCode  = '99-0001634';
        ResponseItem.ShortText  = 'SNAP PRO STARTER KIT';
        ResponseItem.Currency_x  = 'USD';
        ResponseItem.UoM  = 'EA'; 
        ResponseItem.DeliveryDate  = '2016-05-06'; 
        ResponseItem.RequestedQuantity  = '1.0'; 
        ResponseItem.ConfirmedQuantity  = '1.0'; 
        ResponseItem.Plant  = 'USPB'; 
        ResponseItem.ItemCategory  = 'ZBOM';
        ResponseItem.MaterialGroup  = 'SNAP PRO';
        ResponseItem.Subtotal1  = '2000.0'; 
        ResponseItem.Subtotal2  = '1760.0';
        ResponseItem.Subtotal3  = '1780.0';
        ResponseItem.Subtotal4  = '20.0'; 
        ResponseItem.Subtotal5  = '106.8'; 
        ResponseItem.Subtotal6  = '-240.0';
        responseItemsList.add(ResponseItem);
        myresponse.ResponseItems = responseItemsList;
        controller.syncResponse = myresponse;
        controller.priceItems = product.Id;
        controller.getPrice();
        // controller.priceItems = product.Id;
        
        controller.showPrice();
        SYSTEM.debug('dml 6 ' + Limits.getDMLStatements());
        Test.stopTest();    
    }
    
    @isTest static void TestSimulateAsync() {
        Test_Data_Utility.createTestAccounts(1, 0);
        IDEXX_Integration_Service_Properties__c customsettings = new IDEXX_Integration_Service_Properties__c();
        customsettings.Name = 'CUST_PRICE_INVOKE_SYNC';
        customsettings.TextVal__c = 'false';
        insert customsettings;
        SYSTEM.debug('dml 7 ' + Limits.getDMLStatements());
        createTestData();
        //Account 
        SYSTEM.debug('dml 8 ' + Limits.getDMLStatements());
        Account thisAccount = accts.get(0);
        List<String> devisionList = new List<String>();
        List<Salesview__c> salesview = new List<Salesview__c>();
        List<Account_Salesview__c> acctSalesView= new List<Account_Salesview__c>();
        List<Partner_Function__c> partner = new List<Partner_Function__c>();
        
        devisionList.add('CP');
        devisionList.add('VS');
        devisionList.add('VB');
        devisionList.add('VR');
        devisionList.add('CH');
        devisionList.add('VC');
        devisionList.add('CL');
        devisionList.add('BR');
        devisionList.add('XX'); 
        for(String devision :devisionList) {
            
            Salesview__c tempSV = new Salesview__c(Name = 'US Sales Org 1 / Customer POD', CurrencyIsoCode = 'USD', Sales_Organization_Code__c = 'USS1', Distribution_Channel_Code__c = '00', Sales_Division__c = devision, SalesOrg_DistChann_Division__c='USS100'+devision);
            salesview.add(tempSV);

        }
        insert salesview;
        integer i = 0;
        for(SalesView__c s : salesview){
            String devision = devisionList.get(i);       
                Account_Salesview__c  tempAccView = new Account_Salesview__c(Account__c = thisAccount.Id, SalesView__c = s.id, Name = 'USS1 / 00 / '+devision+' / 0000001003', CurrencyIsoCode = 'USD', SalesOrg_DistChann_Div_Cust__c = 'USS100'+devision+'0000001003', Incoterms1__c = '250' , SAP_Customer_Number__c = '0000001003', Marked_for_Deletion_Flag__c = false, IDEXX_Region_Code__c = 'USA' );
                acctSalesView.add(tempAccView);     
                        
            Partner_Function__c tempP = new Partner_Function__c(Name = '0000001003 / USS1 / '+devision+' / AG / 0000001003',Account__c = thisAccount.Id, Partner_Account__c = thisAccount.Id, Sales_Organization__c = 'USS1', Distribution_Channel__c = '00', Division__c = devision, Default_Partner__c = 'N',Partner_Function__c='AG', Partner_Function_Desc__c = 'Sold-to party', Salesview__c=s.Id, Cust_SlsOrg_DistChan_Div_PartFunc_Ctr__c = '0000001003USS100'+devision+'AG0' );
            partner.add(tempP);
            i++;
        }
        insert acctSalesView;
        insert partner;   
        SYSTEM.debug('dml 11 ' + Limits.getDMLStatements());
        Product2 product = new Product2(Family = 'Test', Name = 'SNAP PRO STARTER KIT', ProductCode = '99-0001634', Description = 'SNAP PRO STARTER KIT', IsActive = true, CurrencyIsoCode = 'USD', Base_Unit_of_Measure_Desc__c = 'EACH', Base_Unit_of_Measure__c = 'EA', Batch_Mgmt_Requirement__c = false, General_Item_Category_Group_Desc__c = 'Service w/o delivery', General_Item_Category_Group__c = 'LEIS', Gross_Weight__c = '0.000000000000000', Hierarchy_Family__c = 'UNALLOCATED', Hierarchy_Line__c = 'UNALLOCATED', Hierarchy_Name__c = 'UNALLOCATED', Hierarchy_Type__c = 'UNALLOCATED', Industry_Sector_Desc__c = 'PHARMACEUTICALS', Industry_Sector__c = 'P', Item_Category_Group_Code__c = 'ZTOP', Item_Category_Group_Desc__c = 'TOP LEVEL ASSEMBLY',  Material_Division_Desc__c = 'POD IMMUNOASSAY', Material_Division_Group__c = 'IHD', Material_Division__c = 'VV', Material_Group_Desc__c = 'SNAP PRO', Material_Group__c = 'SNAP PRO', Material_Type_Desc__c = 'NON-STOCK MATERIAL', Material_Type__c = 'NLAG', Net_Weight__c = '0.000000000000000', Product_Hierarchy__c = 'AG', Quantity_Per_Package__c = '1.000000000000000', SAP_MATERIAL_NUMBER__c = '99-0001634', Weight_Unit_Desc__c = 'KILOGRAM', Weight_Unit__c = 'KG');
        insert product;
        SYSTEM.debug('dml 12 ' + Limits.getDMLStatements());
        System.debug('product'+product.Id);
        
        Id pricebookId = Test.getStandardPricebookId();
        Pricebook2 priceBook = new Pricebook2(Name = 'IDEXX Standard Price Book', isActive=true);
        insert priceBook;
        SYSTEM.debug('dml 13 ' + Limits.getDMLStatements());
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        
        PricebookEntry stdpriceBookEntry = new PriceBookEntry(Pricebook2Id = pricebookId, Product2Id = product.Id, CurrencyIsoCode = 'USD', UnitPrice = 10000.0, IsActive = true, PriceBook_SAPMatNr__c = 'Standard Price Book99-0001634USD');
        pricebookEntries.add(stdpriceBookEntry);
        
        
        
        PricebookEntry priceBookEntry = new PriceBookEntry(Pricebook2Id = priceBook.Id, Product2Id = product.Id, CurrencyIsoCode = 'USD', UnitPrice = 2000.0, IsActive = true, PriceBook_SAPMatNr__c = 'IDEXX Standard Price Book99-0001634USD');
        pricebookEntries.add(priceBookEntry);
        
        insert pricebookEntries;
        SYSTEM.debug('dml 14 ' + Limits.getDMLStatements());
        // initialize contoller
        CustomerPricingControllerExtension controller = new CustomerPricingControllerExtension(new ApexPages.StandardController(thisAccount));
        
        Test.startTest();
        controller.productSearch();
        controller.searchString = '99-0001634';
        Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = product.Id;
        Test.setFixedSearchResults(fixedSearchResults); 
        controller.productSearch();
        List<CustomerPricingControllerExtension.ProductsWithPriceHolder> currentResults = controller.results;
        for(CustomerPricingControllerExtension.ProductsWithPriceHolder cuentResult : currentResults) {
            cuentResult.isSelected = true;                                              
        }
        Continuation conti = (Continuation)controller.showPrice();
        
        if (null != conti) {
            Map<String, HttpRequest> requests = conti.getRequests();
            System.assertEquals(requests.size(), 1);
            
            HttpResponse response = new HttpResponse();
            response.setBody('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' 
                             +'   <SOAP:Header xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"/>' 
                             +'   <SOAP:Body xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/">' 
                             +'      <sal:SalesOrderSimulateResponse xmlns:sal="http://idexx.com/sappo/sales/salesforce">' 
                             +'         <ResponseItems>' 
                             +'            <ItemNumber xmlns:ns1="http://idexx.com/sappo/sales/salesforce">000010</ItemNumber>' 
                             +'            <ExternalItemNumber xmlns:ns1="http://idexx.com/sappo/sales/salesforce">1</ExternalItemNumber>' 
                             +'            <Material xmlns:ns1="http://idexx.com/sappo/sales/salesforce">99-0001634</Material>' 
                             +'            <SpeedCode xmlns:ns1="http://idexx.com/sappo/sales/salesforce">99-0001634</SpeedCode>' 
                             +'            <ShortText xmlns:ns1="http://idexx.com/sappo/sales/salesforce">SNAP PRO STARTER KIT</ShortText>' 
                             +'            <Currency xmlns:ns1="http://idexx.com/sappo/sales/salesforce">USD</Currency>' 
                             +'            <UoM xmlns:ns1="http://idexx.com/sappo/sales/salesforce">EA</UoM>' 
                             +'            <DeliveryDate xmlns:ns1="http://idexx.com/sappo/sales/salesforce">2016-05-06</DeliveryDate>' 
                             +'            <RequestedQuantity xmlns:ns1="http://idexx.com/sappo/sales/salesforce">1.0</RequestedQuantity>' 
                             +'            <ConfirmedQuantity xmlns:ns1="http://idexx.com/sappo/sales/salesforce">1.0</ConfirmedQuantity>' 
                             +'            <Plant xmlns:ns1="http://idexx.com/sappo/sales/salesforce">USPB</Plant>' 
                             +'            <ItemCategory xmlns:ns1="http://idexx.com/sappo/sales/salesforce">ZBOM</ItemCategory>' 
                             +'            <MaterialGroup xmlns:ns1="http://idexx.com/sappo/sales/salesforce">SNAP PRO</MaterialGroup>' 
                             +'            <Subtotal1 xmlns:ns1="http://idexx.com/sappo/sales/salesforce">2000.0</Subtotal1>' 
                             +'            <Subtotal2 xmlns:ns1="http://idexx.com/sappo/sales/salesforce">1760.0</Subtotal2>' 
                             +'            <Subtotal3 xmlns:ns1="http://idexx.com/sappo/sales/salesforce">1780.0</Subtotal3>' 
                             +'            <Subtotal4 xmlns:ns1="http://idexx.com/sappo/sales/salesforce">20.0</Subtotal4>' 
                             +'            <Subtotal5 xmlns:ns1="http://idexx.com/sappo/sales/salesforce">106.8</Subtotal5>' 
                             +'            <Subtotal6 xmlns:ns1="http://idexx.com/sappo/sales/salesforce">-240.0</Subtotal6>' 
                             +'         </ResponseItems>' 
                             +'      </sal:SalesOrderSimulateResponse>' 
                             +'   </SOAP:Body>' 
                             +'</soapenv:Envelope>');
            String requestLabel = requests.keySet().iterator().next();
            Test.setContinuationResponse(requestLabel, response);
            Test.invokeContinuationMethod(controller, conti);
        }
        SYSTEM.debug('dml 15 ' + Limits.getDMLStatements());
        Test.stopTest();    
    }
    
    @isTest static void TestSimulateAsyncNoResults() {
        Test_Data_Utility.createTestAccounts(1, 0);
        IDEXX_Integration_Service_Properties__c customsettings = new IDEXX_Integration_Service_Properties__c();
        customsettings.Name = 'CUST_PRICE_INVOKE_SYNC';
        customsettings.TextVal__c = 'false';
        insert customsettings;
        
        createTestData();
        //Account 
        Account thisAccount = accts.get(0);
        
        Salesview__c salesview = new Salesview__c(Name = 'US Sales Org 1 / Customer POD', CurrencyIsoCode = 'USD', Sales_Organization_Code__c = 'USS1', Distribution_Channel_Code__c = '00', Sales_Division__c = 'CP', SalesOrg_DistChann_Division__c='USS100CP');
        insert salesview;
        
        Account_Salesview__c acctSalesView = new Account_Salesview__c(Account__c = thisAccount.Id, SalesView__c = salesview.Id,Name = 'USS1 / 00 / CP / 0000001003', CurrencyIsoCode = 'USD', SalesOrg_DistChann_Div_Cust__c = 'USS100CP0000001003', Incoterms1__c = '250' , SAP_Customer_Number__c = '0000001003', Marked_for_Deletion_Flag__c = false, IDEXX_Region_Code__c = 'USA' );
        insert acctSalesView;
        
        Partner_Function__c partner = new Partner_Function__c(Name = '0000001003 / USS1 / CP / AG / 0000001003',Account__c = thisAccount.Id, Partner_Account__c = thisAccount.Id, Sales_Organization__c = 'USS1', Distribution_Channel__c = '00', Division__c = 'CP', Default_Partner__c = 'N',Partner_Function__c='AG', Partner_Function_Desc__c = 'Sold-to party', Salesview__c=salesview.Id, Cust_SlsOrg_DistChan_Div_PartFunc_Ctr__c = '0000001003USS100CPAG0' );
        insert partner;
        
        
        Product2 product = new Product2(Family = 'Test', Name = 'SNAP PRO STARTER KIT', ProductCode = '99-0001634', Description = 'SNAP PRO STARTER KIT', IsActive = true, CurrencyIsoCode = 'USD', Base_Unit_of_Measure_Desc__c = 'EACH', Base_Unit_of_Measure__c = 'EA', Batch_Mgmt_Requirement__c = false, General_Item_Category_Group_Desc__c = 'Service w/o delivery', General_Item_Category_Group__c = 'LEIS', Gross_Weight__c = '0.000000000000000', Hierarchy_Family__c = 'UNALLOCATED', Hierarchy_Line__c = 'UNALLOCATED', Hierarchy_Name__c = 'UNALLOCATED', Hierarchy_Type__c = 'UNALLOCATED', Industry_Sector_Desc__c = 'PHARMACEUTICALS', Industry_Sector__c = 'P', Item_Category_Group_Code__c = 'ZTOP', Item_Category_Group_Desc__c = 'TOP LEVEL ASSEMBLY', Material_Division_Desc__c = 'POD IMMUNOASSAY', Material_Division_Group__c = 'IHD', Material_Division__c = 'VV', Material_Group_Desc__c = 'SNAP PRO', Material_Group__c = 'SNAP PRO', Material_Type_Desc__c = 'NON-STOCK MATERIAL', Material_Type__c = 'NLAG', Net_Weight__c = '0.000000000000000', Product_Hierarchy__c = 'AG', Quantity_Per_Package__c = '1.000000000000000', SAP_MATERIAL_NUMBER__c = '99-0001634', Weight_Unit_Desc__c = 'KILOGRAM', Weight_Unit__c = 'KG');
        insert product;

        // DE8799----Added by Rama        
        Product_Salesview__c psv = new Product_Salesview__c();
        psv.name ='Test';
        psv.Product__c = product.id;
        psv.Product_Division__c = '00';
        psv.Sales_Division__c = 'CP';
        psv.Sales_Organization__c = 'USS1';
        psv.SalesView__c = salesview.id;
        psv.SalesOrg_DistChann_SlsDiv_Product__c = '00cpuss1';
        insert psv; 
        //END
        
        System.debug('product'+product.Id);
        
        Id pricebookId = Test.getStandardPricebookId();
        Pricebook2 priceBook = new Pricebook2(Name = 'IDEXX Standard Price Book', isActive=true);
        insert priceBook;
        
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        
        PricebookEntry stdpriceBookEntry = new PriceBookEntry(Pricebook2Id = pricebookId, Product2Id = product.Id, CurrencyIsoCode = 'USD', UnitPrice = 10000.0, IsActive = true, PriceBook_SAPMatNr__c = 'Standard Price Book99-0001634USD');
        pricebookEntries.add(stdpriceBookEntry);
        
        
        
        PricebookEntry priceBookEntry = new PriceBookEntry(Pricebook2Id = priceBook.Id, Product2Id = product.Id, CurrencyIsoCode = 'USD', UnitPrice = 2000.0, IsActive = true, PriceBook_SAPMatNr__c = 'IDEXX Standard Price Book99-0001634USD');
        pricebookEntries.add(priceBookEntry);
        
        insert pricebookEntries;
        
        // initialize contoller
        CustomerPricingControllerExtension controller = new CustomerPricingControllerExtension(new ApexPages.StandardController(thisAccount));
        
        
        Test.startTest();
        controller.productSearch();
        controller.searchString = '99-0001634';
        Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = product.Id;
        Test.setFixedSearchResults(fixedSearchResults); 
        controller.productSearch();
        List<CustomerPricingControllerExtension.ProductsWithPriceHolder> currentResults = controller.results;
        for(CustomerPricingControllerExtension.ProductsWithPriceHolder cuentResult : currentResults) {
            cuentResult.isSelected = true;                                              
        }
        Continuation conti = (Continuation)controller.showPrice();
        
        if (null != conti) {
            Map<String, HttpRequest> requests = conti.getRequests();
            System.assertEquals(requests.size(), 1);
            
            HttpResponse response = new HttpResponse();
            response.setBody(' <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' 
                             +'    <SOAP:Header xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"/>' 
                             +'    <SOAP:Body xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/">' 
                             +'       <sal:SalesOrderSimulateResponse xmlns:sal="http://idexx.com/sappo/sales/salesforce">' 
                             +'          <Return>' 
                             +'             <MessageType xmlns:ns1="http://idexx.com/sappo/sales/salesforce">E</MessageType>' 
                             +'             <MessageID xmlns:ns1="http://idexx.com/sappo/sales/salesforce">CZ</MessageID>' 
                             +'             <MessageNumber xmlns:ns1="http://idexx.com/sappo/sales/salesforce">115</MessageNumber>' 
                             +'             <Message xmlns:ns1="http://idexx.com/sappo/sales/salesforce">Sales area USS1 00 zz does not exist</Message>' 
                             +'          </Return>' 
                             +'       </sal:SalesOrderSimulateResponse>' 
                             +'    </SOAP:Body>' 
                             +' </soapenv:Envelope>');
            String requestLabel = requests.keySet().iterator().next();
            Test.setContinuationResponse(requestLabel, response);
            Test.invokeContinuationMethod(controller, conti);
        }
        
        Test.stopTest();    
    }
    
    @isTest static void TestSimulateAsyncNoErrorMsg() {
        Test_Data_Utility.createTestAccounts(1, 0);
        IDEXX_Integration_Service_Properties__c customsettings = new IDEXX_Integration_Service_Properties__c();
        customsettings.Name = 'CUST_PRICE_INVOKE_SYNC';
        customsettings.TextVal__c = 'false';
        insert customsettings;
        
        createTestData();
        //Account 
        Account thisAccount = accts.get(0);
        Salesview__c salesview = new Salesview__c(Name = 'US Sales Org 1 / Customer POD', CurrencyIsoCode = 'USD', Sales_Organization_Code__c = 'USS1', Distribution_Channel_Code__c = '00', Sales_Division__c = 'CP', SalesOrg_DistChann_Division__c='USS100CP');
        insert salesview;
        
        Account_Salesview__c acctSalesView = new Account_Salesview__c(Account__c = thisAccount.Id, SalesView__c = salesview.Id,Name = 'USS1 / 00 / CP / 0000001003', CurrencyIsoCode = 'USD', SalesOrg_DistChann_Div_Cust__c = 'USS100CP0000001003', Incoterms1__c = '250' , SAP_Customer_Number__c = '0000001003', Marked_for_Deletion_Flag__c = false, IDEXX_Region_Code__c = 'USA' );
        insert acctSalesView;
        
        Partner_Function__c partner = new Partner_Function__c(Name = '0000001003 / USS1 / CP / AG / 0000001003',Account__c = thisAccount.Id, Partner_Account__c = thisAccount.Id, Sales_Organization__c = 'USS1', Distribution_Channel__c = '00', Division__c = 'CP', Default_Partner__c = 'N',Partner_Function__c='AG', Partner_Function_Desc__c = 'Sold-to party', Salesview__c=salesview.Id, Cust_SlsOrg_DistChan_Div_PartFunc_Ctr__c = '0000001003USS100CPAG0' );
        insert partner;
        
        
        Product2 product = new Product2(Family = 'Test', Name = 'SNAP PRO STARTER KIT', ProductCode = '99-0001634', Description = 'SNAP PRO STARTER KIT', IsActive = true, CurrencyIsoCode = 'USD', Base_Unit_of_Measure_Desc__c = 'EACH', Base_Unit_of_Measure__c = 'EA', Batch_Mgmt_Requirement__c = false, General_Item_Category_Group_Desc__c = 'Service w/o delivery', General_Item_Category_Group__c = 'LEIS', Gross_Weight__c = '0.000000000000000', Hierarchy_Family__c = 'UNALLOCATED', Hierarchy_Line__c = 'UNALLOCATED', Hierarchy_Name__c = 'UNALLOCATED', Hierarchy_Type__c = 'UNALLOCATED', Industry_Sector_Desc__c = 'PHARMACEUTICALS', Industry_Sector__c = 'P', Item_Category_Group_Code__c = 'ZTOP', Item_Category_Group_Desc__c = 'TOP LEVEL ASSEMBLY', Material_Division_Desc__c = 'POD IMMUNOASSAY', Material_Division_Group__c = 'IHD', Material_Division__c = 'VV', Material_Group_Desc__c = 'SNAP PRO', Material_Group__c = 'SNAP PRO', Material_Type_Desc__c = 'NON-STOCK MATERIAL', Material_Type__c = 'NLAG', Net_Weight__c = '0.000000000000000', Product_Hierarchy__c = 'AG', Quantity_Per_Package__c = '1.000000000000000', SAP_MATERIAL_NUMBER__c = '99-0001634', Weight_Unit_Desc__c = 'KILOGRAM', Weight_Unit__c = 'KG');
        insert product;
        System.debug('product'+product.Id);
        
        Id pricebookId = Test.getStandardPricebookId();
        Pricebook2 priceBook = new Pricebook2(Name = 'IDEXX Standard Price Book', isActive=true);
        insert priceBook;
        
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        
        PricebookEntry stdpriceBookEntry = new PriceBookEntry(Pricebook2Id = pricebookId, Product2Id = product.Id, CurrencyIsoCode = 'USD', UnitPrice = 10000.0, IsActive = true, PriceBook_SAPMatNr__c = 'Standard Price Book99-0001634USD');
        pricebookEntries.add(stdpriceBookEntry);
        
        
        
        PricebookEntry priceBookEntry = new PriceBookEntry(Pricebook2Id = priceBook.Id, Product2Id = product.Id, CurrencyIsoCode = 'USD', UnitPrice = 2000.0, IsActive = true, PriceBook_SAPMatNr__c = 'IDEXX Standard Price Book99-0001634USD');
        pricebookEntries.add(priceBookEntry);
        
        insert pricebookEntries;
        
        // initialize contoller
        CustomerPricingControllerExtension controller = new CustomerPricingControllerExtension(new ApexPages.StandardController(thisAccount));
        
        
        Test.startTest();
        controller.productSearch();
        controller.searchString = '99-0001634';
        Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = product.Id;
        Test.setFixedSearchResults(fixedSearchResults); 
        controller.productSearch();
        List<CustomerPricingControllerExtension.ProductsWithPriceHolder> currentResults = controller.results;
        for(CustomerPricingControllerExtension.ProductsWithPriceHolder cuentResult : currentResults) {
            cuentResult.isSelected = true;                                              
        }
        Continuation conti = (Continuation)controller.showPrice();
        
        if (null != conti) {
            Map<String, HttpRequest> requests = conti.getRequests();
            System.assertEquals(requests.size(), 1);
            
            HttpResponse response = new HttpResponse();
            response.setBody(' <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' 
                             +'    <SOAP:Header xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"/>' 
                             +'    <SOAP:Body xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/">' 
                             +'       <sal:SalesOrderSimulateResponse xmlns:sal="http://idexx.com/sappo/sales/salesforce">' 
                             +'       </sal:SalesOrderSimulateResponse>' 
                             +'    </SOAP:Body>' 
                             +' </soapenv:Envelope>');
            String requestLabel = requests.keySet().iterator().next();
            Test.setContinuationResponse(requestLabel, response);
            Test.invokeContinuationMethod(controller, conti);
        }
        
        Test.stopTest();    
    }
    
    @isTest static void TestSimulateAsyncNoResponse() {
        Test_Data_Utility.createTestAccounts(1, 0);
        IDEXX_Integration_Service_Properties__c customsettings = new IDEXX_Integration_Service_Properties__c();
        customsettings.Name = 'CUST_PRICE_INVOKE_SYNC';
        customsettings.TextVal__c = 'false';
        insert customsettings;
        
        createTestData();
        //Account 
        Account thisAccount = accts.get(0);
        Salesview__c salesview = new Salesview__c(Name = 'US Sales Org 1 / Customer POD', CurrencyIsoCode = 'USD', Sales_Organization_Code__c = 'USS1', Distribution_Channel_Code__c = '00', Sales_Division__c = 'CP', SalesOrg_DistChann_Division__c='USS100CP');
        insert salesview;
        
        Account_Salesview__c acctSalesView = new Account_Salesview__c(Account__c = thisAccount.Id, SalesView__c = salesview.Id,Name = 'USS1 / 00 / CP / 0000001003', CurrencyIsoCode = 'USD', SalesOrg_DistChann_Div_Cust__c = 'USS100CP0000001003', Incoterms1__c = '250' , SAP_Customer_Number__c = '0000001003', Marked_for_Deletion_Flag__c = false, IDEXX_Region_Code__c = 'USA' );
        insert acctSalesView;
        
        Partner_Function__c partner = new Partner_Function__c(Name = '0000001003 / USS1 / CP / AG / 0000001003',Account__c = thisAccount.Id, Partner_Account__c = thisAccount.Id, Sales_Organization__c = 'USS1', Distribution_Channel__c = '00', Division__c = 'CP', Default_Partner__c = 'N',Partner_Function__c='AG', Partner_Function_Desc__c = 'Sold-to party', Salesview__c=salesview.Id, Cust_SlsOrg_DistChan_Div_PartFunc_Ctr__c = '0000001003USS100CPAG0' );
        insert partner;
        
        
        Product2 product = new Product2(Family = 'Test', Name = 'SNAP PRO STARTER KIT', ProductCode = '99-0001634', Description = 'SNAP PRO STARTER KIT', IsActive = true, CurrencyIsoCode = 'USD', Base_Unit_of_Measure_Desc__c = 'EACH', Base_Unit_of_Measure__c = 'EA', Batch_Mgmt_Requirement__c = false, General_Item_Category_Group_Desc__c = 'Service w/o delivery', General_Item_Category_Group__c = 'LEIS', Gross_Weight__c = '0.000000000000000', Hierarchy_Family__c = 'UNALLOCATED', Hierarchy_Line__c = 'UNALLOCATED', Hierarchy_Name__c = 'UNALLOCATED', Hierarchy_Type__c = 'UNALLOCATED', Industry_Sector_Desc__c = 'PHARMACEUTICALS', Industry_Sector__c = 'P', Item_Category_Group_Code__c = 'ZTOP', Item_Category_Group_Desc__c = 'TOP LEVEL ASSEMBLY', Material_Division_Desc__c = 'POD IMMUNOASSAY', Material_Division_Group__c = 'IHD', Material_Division__c = 'VV', Material_Group_Desc__c = 'SNAP PRO', Material_Group__c = 'SNAP PRO', Material_Type_Desc__c = 'NON-STOCK MATERIAL', Material_Type__c = 'NLAG', Net_Weight__c = '0.000000000000000', Product_Hierarchy__c = 'AG', Quantity_Per_Package__c = '1.000000000000000', SAP_MATERIAL_NUMBER__c = '99-0001634', Weight_Unit_Desc__c = 'KILOGRAM', Weight_Unit__c = 'KG');
        insert product;
        System.debug('product'+product.Id);
        
        Id pricebookId = Test.getStandardPricebookId();
        Pricebook2 priceBook = new Pricebook2(Name = 'IDEXX Standard Price Book', isActive=true);
        insert priceBook;
        
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        
        PricebookEntry stdpriceBookEntry = new PriceBookEntry(Pricebook2Id = pricebookId, Product2Id = product.Id, CurrencyIsoCode = 'USD', UnitPrice = 10000.0, IsActive = true, PriceBook_SAPMatNr__c = 'Standard Price Book99-0001634USD');
        pricebookEntries.add(stdpriceBookEntry);
        
        
        
        PricebookEntry priceBookEntry = new PriceBookEntry(Pricebook2Id = priceBook.Id, Product2Id = product.Id, CurrencyIsoCode = 'USD', UnitPrice = 2000.0, IsActive = true, PriceBook_SAPMatNr__c = 'IDEXX Standard Price Book99-0001634USD');
        pricebookEntries.add(priceBookEntry);
        
        insert pricebookEntries;
        
        // initialize contoller
        CustomerPricingControllerExtension controller = new CustomerPricingControllerExtension(new ApexPages.StandardController(thisAccount));
        
        
        Test.startTest();
        controller.productSearch();
        controller.searchString = '99-0001634';
        Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = product.Id;
        Test.setFixedSearchResults(fixedSearchResults); 
        controller.productSearch();
        List<CustomerPricingControllerExtension.ProductsWithPriceHolder> currentResults = controller.results;
        for(CustomerPricingControllerExtension.ProductsWithPriceHolder cuentResult : currentResults) {
            cuentResult.isSelected = true;                                              
        }
        Continuation conti = (Continuation)controller.showPrice();
        
        if (null != conti) {
            Map<String, HttpRequest> requests = conti.getRequests();
            System.assertEquals(requests.size(), 1);
            
            HttpResponse response = new HttpResponse();
            response.setBody(' <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' 
                             +'    <SOAP:Header xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"/>' 
                             +'    <SOAP:Body xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/">' 
                             +'    </SOAP:Body>' 
                             +' </soapenv:Envelope>');
            String requestLabel = requests.keySet().iterator().next();
            Test.setContinuationResponse(requestLabel, response);
            Test.invokeContinuationMethod(controller, conti);
        }
        
        //controller.priceItems = product.Id;
                        
        Test.stopTest();    
    }
}