@isTest
private class WFM_UtilsTest {

    @testSetup
    private static void createData() {
        WFM_Utils.setUserPermissions([select id from User where id = :UserInfo.getUserId()]);
        
        List<Territory2Type> listTerritoryTypes = [
            SELECT Id, MasterLabel
            FROM    Territory2Type
            WHERE  DeveloperName = 'CAG_Territory_Type'
        ];
        Id pricebookId = Test.getStandardPricebookId();
        Territory2Model terrModel;
        Territory2 newTerritory;
        Territory2 newTerritoryParent;
        List<User> listOfUsers = new List<User>();
        System.runAs(new User(Id = UserInfo.getUserId())) {
            for(Integer i=0; i<5; i++) {
                listOfUsers.add(CreateTestClassData.reusableUser('System Administrator', 'tusr' + i));
            }
            insert listOfUsers;
            
            terrModel = [Select id, DeveloperName, Name from Territory2Model where State = 'Active'];
            newTerritoryParent = new Territory2(
                DeveloperName = 'Test_Parent_Territory',
                Name = 'Test Parent Territory',
                Territory2ModelId = terrModel.Id,
                Territory2TypeId = listTerritoryTypes[0].Id
            );
            insert newTerritoryParent;
            
            Group grp=new Group(
                Type='Queue',
                DeveloperName = 'Test_Parent_Territory',
                Name = 'Test Parent Territory'
            );
            insert grp;
                        
            QueueSObject caseQueue=new QueueSObject(
                SOBjectType='Case',
                QueueId=grp.Id
                );
            insert caseQueue;
            
            newTerritory = new Territory2(
                DeveloperName = 'Test_Territory',
                Name = 'Test Territory',
                Territory2ModelId = terrModel.Id,
                Territory2TypeId = listTerritoryTypes[0].Id,
                ParentTerritory2 = newTerritoryParent
            );
            insert newTerritory;

            List<UserTerritory2Association> listUserTerritory = new List<UserTerritory2Association>();
            for(User usr : listOfUsers){
                UserTerritory2Association usrTerritory = new UserTerritory2Association(
                    UserId = usr.Id, 
                    Territory2Id = newTerritory.Id,
                    RoleInTerritory2 = WFM_Constants.USER_TERRITORY_ROLE_FSR_DR
                );
                if(usr == listOfUsers[0]){
                    usrTerritory.RoleInTerritory2 = WFM_Constants.USER_TERRITORY_ROLE_FSR_DX;
                }

                listUserTerritory.add(usrTerritory);
            }
            insert listUserTerritory;
        }

        Account acc = new Account(
            Name = 'Account_B',
            SAP_Customer_Number__c = '909092',
            CurrencyIsoCode = 'USD',
            ShippingCountryCode = 'US',
            ShippingStateCode = 'PA'
        );
        Account acc2 = acc.clone();
        acc2.Name = 'Account_C';
        acc2.SAP_Customer_Number__c = '0909093';
        insert new List<Account>{acc, acc2};

        ObjectTerritory2Association acctTerritory = new ObjectTerritory2Association(
            ObjectId = acc.Id, 
            Territory2Id = newTerritory.Id,
            AssociationCause = 'Territory2Manual'
        );
        insert acctTerritory;

        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.CurrencyIsoCode = acc.CurrencyIsoCode;
        opp.StageName = '4 commitment';
        opp.Name = 'Test Opp ';
        opp.CloseDate = Date.today();
        insert opp;

        Quote quote = new Quote(
            Name = 'QuoteName',
            Quote_Number__c = '121213',
            OpportunityId = opp.Id,
            Pricebook2Id = pricebookId,
            Pricing_Start_Date__c = Date.today(),
            Pricing_Expiration_Date__c = Date.today().addDays(2),
            Status = WFM_Constants.QUOTE_STATUS_ACCEPTED
        );
        insert quote;

        List<Product2> listProducts = new List<Product2>();
        for(Integer i=0; i<5; i++){
            listProducts.add(new Product2(
                Name = 'ProductName' + i,
                SAP_MATERIAL_NUMBER__c = String.valueOf(i + 1000),
                IsActive = true,
                Include_in_Install_Work_Order__c = true
            ));
        }
        insert listProducts;

        List<PricebookEntry> listPBEs = new List<PricebookEntry>();
        Map<Id, PricebookEntry> mapPBEByProductId = new Map<Id, PricebookEntry>();
        for(Product2 product : listProducts){
            PricebookEntry pbe = new PricebookEntry(
                Pricebook2Id = pricebookId,
                Product2Id = product.Id,
                UnitPrice = 500,
                PriceBook_SAPMatNr__c = product.SAP_MATERIAL_NUMBER__c,
                IsActive = true
            );
            listPBEs.add(pbe);
        }
        insert listPBEs;

        for(PricebookEntry pbe : listPBEs) {
            mapPBEByProductId.put(pbe.product2Id, pbe);
        }

        List<QuoteLineItem> listQLIs = new List<QuoteLineItem>();
        for(Product2 product : listProducts){
            QuoteLineItem qli = new QuoteLineItem(
                QuoteId = quote.Id,
                Product2Id = product.Id,
                PricebookEntryId = mapPBEByProductId.get(product.Id).Id,
                Quantity = 1,
                UnitPrice = 500
            );
            listQLIs.add(qli);
        }
        insert listQLIs;

        WorkOrder wo = new WorkOrder(
                RecordTypeId = WFM_Constants.WORK_ORDER_CRITICAL_RECORDTYPE_ID,
                AccountId = acc.Id,
                Subject = 'Test Class Subject',
                Duration = 2.0,         //US29036 - H.Kinney - 4.27.2017
                DurationType = 'Hours', //US29036 - H.Kinney - 4.27.2017
                Install_Time__c = 1,    //US29036 - H.Kinney - 4.27.2017
                Training_Time__c = 1,   //US29036 - H.Kinney - 4.27.2017
                Other_Time__c = 1       //US29036 - H.Kinney - 4.27.2017
        );
        List<WorkOrder> workOrders = new List<WorkOrder>{wo, wo.clone()};
        insert workOrders;

        WorkOrderLineItem woli = new WorkOrderLineItem(
            WorkOrderId = workOrders[0].Id,
            Product__c = listProducts[0].Id
        );
        WorkOrderLineItem woli2 = woli.clone();
        woli2.WorkOrderId = workOrders[1].Id;
        insert new List<WorkOrderLineItem>{woli, woli2};
    }
    
    static testMethod void retrieveSupervisorsTest() {
        Profile profile = [SELECT   id FROM Profile WHERE Name = 'System Administrator'];
        // Create supervisor role and user
        UserRole supervisorRole = new UserRole(
                Name = 'Level One',
                DeveloperName = 'LevelOne'
            );
        insert supervisorRole;

        User supervisor = new User(
                Email = 'WFM_supervisor@test.com',
                LastName = 'WFM_supervisor',
                UserRoleId = supervisorRole.Id,
                Username = 'wfmsupervisorForTest@test.com',
                Alias = 'superv',
                CommunityNickname = 'WFMsupervisorForTest',
                ProfileId = profile.Id,
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                TimeZoneSidKey = 'GMT',
                EmailEncodingKey = 'UTF-8'
            );


        // Create subordinate role
        UserRole roleOne = new UserRole(
                Name = 'Level Two',
                DeveloperName = 'LevelTwo',
                ParentRoleId = supervisorRole.Id
            );
        insert roleOne;
        User userOne = new User(
                Email = 'WFMuserOne@test.com',
                LastName = 'WFMuserOne',
                UserRoleId = roleOne.Id,
                Username = 'WFMuserOne@test.com',
                Alias = 'WFMusr1',
                CommunityNickname = 'WFMuserOne',
                ProfileId = profile.Id,
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                TimeZoneSidKey = 'GMT',
                EmailEncodingKey = 'UTF-8'
            );

        // Create subordinate 2 role
        UserRole roleTwo = new UserRole(
                Name = 'Level Three',
                DeveloperName = 'LevelThree',
                ParentRoleId = roleOne.Id
            );
        insert roleTwo;
        User userTwo = new User(
                Email = 'WFMuserTwo@test.com',
                LastName = 'WFMuserTwo',
                UserRoleId = roleTwo.Id,
                Username = 'WFMuserTwo@test.com',
                Alias = 'WFMUsr2',
                CommunityNickname = 'WFMuserTwo',
                ProfileId = profile.Id,
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                TimeZoneSidKey = 'GMT',
                EmailEncodingKey = 'UTF-8'
            );

        Test.startTest();
        insert new List<User>{supervisor, userOne, userTwo};
        Test.stopTest();
        // Retrieve the userTwo.
        List<User> listUserTwo = [
                SELECT  Id, UserRoleId, Name, Email
                FROM    User
                WHERE   Id = :userTwo.Id
                ];
        List<WFM_Utils.UserGroupWrapper> usersWrapped = new List<WFM_Utils.UserGroupWrapper>();
        for(User u : listUserTwo) {
            usersWrapped.add(new WFM_Utils.UserGroupWrapper(u));
        }

        Map<Id, List<User>> supervisorMap = WFM_Utils.retrieveSupervisors(usersWrapped);
        System.debug(supervisorMap);
        System.assertEquals(supervisorMap.size(), 1);
        System.assert(supervisorMap.containsKey(userTwo.Id));
        System.assertEquals(supervisorMap.get(userTwo.Id).size(), 1);
        System.assertEquals(supervisorMap.get(userTwo.Id).get(0).Id, userOne.Id);

        List<User> listUserOne = [
                SELECT  Id, UserRoleId, Name, Email
                FROM    User
                WHERE   Id = :userOne.Id
                ];

        usersWrapped.clear();
        for(User u : listUserOne) {
            usersWrapped.add(new WFM_Utils.UserGroupWrapper(u));
        }

        supervisorMap = WFM_Utils.retrieveSupervisors(usersWrapped);
        System.debug(supervisorMap);
        System.assertEquals(supervisorMap.size(), 1);
        System.assert(supervisorMap.containsKey(userOne.Id));
        System.assertEquals(supervisorMap.get(userOne.Id).size(), 1);
        System.assertEquals(supervisorMap.get(userOne.Id).get(0).Id, supervisor.Id);

    }
    
    static testMethod void buildWorkOrderChangeEmailTest() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User usr = new User(Alias = 'fsladmn', Email='wfmsysadmin@wfmtestorg.com', 
        EmailEncodingKey='UTF-8', LastName='Admin', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, FirstName = 'WFM Test sys admin',
        TimeZoneSidKey='America/Los_Angeles', UserName='fsladmin@fsltestorg.com');
        insert usr;
        WFM_Utils.UserGroupWrapper usrWrapped = new WFM_Utils.UserGroupWrapper(usr);

        // Create account
        Account account = new Account(
                Name = 'Account_A',
                SAP_Customer_Number__c = '909091',
                ShippingCountryCode = 'US',
                ShippingStateCode = 'PA'
            );
        insert account;

        List<Product2> listProducts = [SELECT Id, Name, SAP_MATERIAL_NUMBER__c FROM Product2];

        // Create an associated work order
        WorkOrder wo = new WorkOrder(
                RecordTypeId = WFM_Constants.WORK_ORDER_CRITICAL_RECORDTYPE_ID,
                AccountId = account.Id,
                Subject = 'Test Class Subject'
            );
        Test.startTest();
        insert wo;

        wo = [
                SELECT Id, RecordType.Name, Account.SAP_Customer_Number__c, WorkOrderNumber, Status, Account.Name, LastModifiedBy.Name
                FROM    WorkOrder
                WHERE  Id = :wo.Id
                LIMIT 1
            ];
        // Create related wo line items
        WorkOrderLineItem woli = new WorkOrderLineItem(
                WorkOrderId = wo.Id,
                Product__c = listProducts.get(1).Id
            );
        insert woli;

        woli = [
                SELECT Id, Product__r.Name
                FROM    WorkOrderLineItem
                WHERE  Id = :woli.Id
                LIMIT 1
            ];
        Test.stopTest();

        Messaging.SingleEmailMessage email = WFM_Utils.buildWorkOrderChangeEmail(wo, usrWrapped, usrWrapped, 'Account_A', new List<WorkOrderLineItem>{woli});

        System.assertEquals(email.subject, 'Critical Work Order Re-Assignment Account_A / 909091 / ' + wo.WorkOrderNumber);
    }
    
    static testMethod void test_getMapOfUsersFromUserIds() {
        Set<Id> userIds = new Set<Id>();
        Group gr = [SELECT Id FROM Group LIMIT 1];
        User usr = [SELECT Id FROM USER LIMIT 1];
        userIds.add(gr.Id);
        userIds.add(usr.Id);

        Test.startTest();
        Map<Id, WFM_Utils.UserGroupWrapper> userId2UserGroup = WFM_Utils.getMapOfUsersFromUserIds(userIds);
        userIds.remove(gr.Id);
        Map<Id, WFM_Utils.UserGroupWrapper> userId2UserGroup2 = WFM_Utils.getMapOfUsersFromUserIds(userIds);
        Test.stopTest();

        System.assert(userId2UserGroup.containsKey(gr.Id), 'Map contains group');
        System.assert(userId2UserGroup.containsKey(usr.Id), 'Map contains created user');
        System.assert(userId2UserGroup2.containsKey(usr.Id), 'Map contains created user');
        System.assert(!userId2UserGroup2.containsKey(gr.Id), 'Map does not contain group');
    }

    static testMethod void test_buildWorkOrderStatusChanged() {
        List<WorkOrder> workOrders = [
            SELECT Id, RecordType.Name, Account.SAP_Customer_Number__c, WorkOrderNumber, Status, Account.Name, 
                LastModifiedBy.Name, Account_Name__c, Cancellation_Reason__c
            FROM WorkOrder
        ];

        List<WorkOrderLineItem> woLineItems = [SELECT Id FROM WorkOrderLineItem WHERE WorkOrderId IN: workOrders];
        for(WorkOrderLineItem woli : woLineItems) {
            woli.Status = WFM_Constants.WORK_ORDER_LINE_ITEM_COMPLETED;
        }
        update woLineItems;

        for(WorkOrder wo : workOrders) {
            String status = wo.Id == workOrders[0].Id ? WFM_Constants.WORK_ORDER_STATUS_COMPLETED :
                WFM_Constants.WORK_ORDER_STATUS_CANCELED;
            wo.RecordTypeId = WFM_Constants.WORK_ORDER_INSTALL_RECORDTYPE_ID;
            wo.Reason_for_Visit__c = WFM_Constants.WORK_ORDER_REASON_NEW_SYSTEM;
            wo.Install_Time__c = 2;
            wo.Status = status;
            wo.Payment_Type__c = WFM_Constants.WORK_ORDER_PAYMENT_TYPE_CASH;
            wo.Cancellation_Notes__c = 'Test cancel';
            wo.Cancellation_Reason__c = 'Credit';
        }

        update workOrders;

        Test.startTest();
        Messaging.SingleEmailMessage email = WFM_Utils.buildWorkOrderStatusChanged(workOrders[0], new List<WorkOrderLineItem>());
        Messaging.SingleEmailMessage email2 = WFM_Utils.buildWorkOrderStatusChanged(workOrders[1], new List<WorkOrderLineItem>());
        Test.stopTest();

        System.assertEquals(workOrders[0].RecordType.Name + ' Work Order Completion: ' + 
            workOrders[0].Account_Name__c + ' / ' + workOrders[0].Account.SAP_Customer_Number__c + ' / ' + workOrders[0].WorkOrderNumber,
            email.subject,
            'Email message should be generated with subject for work order completion');
        System.assertEquals(workOrders[1].RecordType.Name + ' Work Order Cancellation: ' + 
            workOrders[1].Account_Name__c + ' / ' + workOrders[1].Account.SAP_Customer_Number__c + ' / ' + workOrders[1].WorkOrderNumber,
            email2.subject,
            'Email message should be generated with subject for work order cancellation');
    }

    static testMethod void test_buildChangedInstallationDateNotification() {
        List<WorkOrder> workOrders = [
            SELECT Id, RecordType.Name, Account.SAP_Customer_Number__c, WorkOrderNumber, Status, Account.Name, 
                LastModifiedBy.Name, Account_Name__c, Cancellation_Reason__c, Opportunity__r.CloseDate, Opportunity__r.Name
            FROM WorkOrder
        ];

        List<ServiceAppointment> serviceAppointments = [SELECT Id, SchedStartTime FROM ServiceAppointment WHERE ParentRecordId = :workOrders[0].Id];

        Test.startTest();
        Messaging.SingleEmailMessage email = WFM_Utils.buildChangedInstallationDateNotification(workOrders[0], serviceAppointments[0], new List<WorkOrderLineItem>());
        Test.stopTest();

        System.assertEquals('Install Date Change for SAP Account ' + workOrders[0].Account.SAP_Customer_Number__c + 
            ' / Opportunity ' + workOrders[0].Opportunity__r.Name,
            email.subject,
            'Email message should be generated with subject for install date change');
    }

    static testMethod void test_buildNoResourceNotification() {
        List<WorkOrder> workOrders = [
            SELECT Id, RecordType.Name, Account.SAP_Customer_Number__c, WorkOrderNumber, Status, Account.Name, 
                LastModifiedBy.Name, Account_Name__c, Cancellation_Reason__c, Opportunity__r.CloseDate, Opportunity__r.Name,
                Opportunity__c, Owner.Name
            FROM WorkOrder
        ];

        Test.startTest();
        Messaging.SingleEmailMessage email = WFM_Utils.buildNoResourceNotification(workOrders[0]);
        Test.stopTest();

        System.assertEquals('Unable to assign preferred resource: ' + workOrders[0].WorkOrderNumber + '/' + 
            workOrders[0].Account.SAP_Customer_Number__c  + '/' + workOrders[0].Owner.Name,
            email.subject,
            'Email message should be generated with subject for no preferred resource');
    }

    static testMethod void test_getWorkOrdersForTop15() {
        List<WorkOrder> workOrders = [SELECT Id, Due_Date__c FROM WorkOrder];
        List<Account> accs = [SELECT Id FROM Account];
        
        for(WorkOrder wo : workOrders) {
            DateTime dt = wo.Id == workOrders[0].Id ? WFM_Utils.getStartCurrentSemester() : WFM_Utils.getMidSemester();
            wo.Due_Date__c = dt;
        }
        update workOrders;

        Test.startTest();
        List<WorkOrder> newWorkOrders = WFM_Utils.getWorkOrdersForTop15(accs[0], workOrders, UserInfo.getUserId());
        Test.stopTest();

        System.assertEquals(2, newWorkOrders.size(), 'Two new Work Orders should be created');

        for(WorkOrder wo : newWorkOrders) {
            DateTime dt = WFM_Utils.getMidSemester().addDays(-1);
            DateTime dt2 = WFM_Utils.getEndCurrentSemester();
            System.assert(wo.Due_Date__c == dt || wo.Due_Date__c == dt2, 
                'Due date is before second quarter start or is second quarter end');
        }
    }

    static testMethod void test_getWorkOrdersForTop100() {
        List<Account> accs = [SELECT Id FROM Account];

        Test.startTest();
        List<WorkOrder> newWorkOrders = WFM_Utils.getWorkOrdersForTop100(accs[0], null, UserInfo.getUserId());
        Test.stopTest();

        System.assertEquals(1, newWorkOrders.size(), 'One new Work Order should be created');
        System.assertEquals(WFM_Utils.getEndCurrentSemester(), newWorkOrders[0].Due_Date__c,
            'Due date is end of current semester');
    }

    static testMethod void test_retrieveAssociatedFSRs() {
        Map<Id, Account> accs = new Map<Id, Account>([SELECT Id FROM Account]);
        List<Id> accIds = new List<Id>(accs.keySet());
        List<User> users = [SELECT Id FROM User WHERE Alias LIKE 'tusr_'];

        Test.startTest();
        Map<Id, List<Id>> accId2Fsrs = WFM_Utils.retrieveAssociatedFSRs(accIds, new List<String>{WFM_Constants.USER_TERRITORY_ROLE_FSR_DR,
                WFM_Constants.USER_TERRITORY_ROLE_FSR_DX, WFM_Constants.USER_TERRITORY_ROLE_FSR_DX_SUP});
        Test.stopTest();

    }

    static testMethod void test_retrieveProductsFromWorkOrders() {
        Map<Id, WorkOrder> id2WorkOrder = new Map<Id, WorkOrder>([SELECT Id, Due_Date__c FROM WorkOrder]);

        Test.startTest();
        Map<Id, List<WorkOrderLineItem>> workOrderId2Woli = WFM_Utils.retrieveProductsFromWorkOrders(id2WorkOrder.keySet());
        Test.stopTest();

        List<WorkOrderLineItem> woLineItems = new List<WorkOrderLineItem>();
        for(Id woId : workOrderId2Woli.keySet()) {
            woLineItems.addAll(workOrderId2Woli.get(woId));
        }

        System.assertEquals(2, woLineItems.size(), 'There should be 2 work order line items retrieved');
    }

    static testMethod void test_getMapAccountsBySAPAccountNumber() {
        List<Account> accs = [SELECT Id, SAP_Customer_Number__c, SAP_Number_without_Leading_Zeros__c FROM Account];
        Set<String> sapAccNumber = new Set<String>{'909092', '909093'};

        Test.startTest();
        Map<String, Account> sapNum2Account = WFM_Utils.getMapAccountsBySAPAccountNumber(sapAccNumber);
        Test.stopTest();

        System.assert(!sapNum2Account.isEmpty(), 'Accounts were retrived correctly');
        for(String sapNum : sapAccNumber) {
            System.assert(sapNum2Account.containsKey(sapNum), 'Map should contain sap account number w/o leading zeros');
            System.assertNotEquals(null, sapNum2Account.get(sapNum), 'Account should be correctly put in map');
        }
    }

    static testMethod void test_createServiceWorkOrderFromEmailValuesMap() {
        String sapAccNum = '909092';
        List<Account> accs = [SELECT Id FROM Account WHERE SAP_Customer_Number__c = :sapAccNum];
        Map<String, String> mapEmailValues = new Map<String, String>();
        mapEmailValues.put('SAP_Account_Number', sapAccNum);
        mapEmailValues.put('Line_of_Business', WFM_Constants.WORK_ORDER_PRODUCT_TYPE_DX);
        mapEmailValues.put('Estimated_Installation_Date', Date.today().format());
        mapEmailValues.put('Subject', 'Test subject');
        mapEmailValues.put('Body', 'Test body');

        Test.startTest();
        WorkOrder wo = WFM_Utils.createServiceWorkOrderFromEmailValuesMap(mapEmailValues);
        Test.stopTest();

        System.assertEquals(WFM_Constants.WORK_ORDER_PRODUCT_TYPE_DX, wo.Product_Type__c, 'Product type should be correctly set');
        System.assertEquals(Date.today(), wo.Due_Date__c.date(), 'Due date should be correctly set to today');
        System.assertEquals('Test subject', wo.Subject, 'Subject should be correctly set');
        System.assertEquals('Test body', wo.Description, 'Description should be correctly taken from body field');
        System.assertEquals(accs[0].Id, wo.AccountId, 'Account should be correctly set based on SAP account number');
    }

    static testMethod void test_createOperatingHoursForAccounts() {
        Map<Id, Account> accs = new Map<Id, Account>([
            SELECT Id, Name, Monday__c, Tuesday__c, Wednesday__c, Thursday__c, Friday__c, Saturday__c, Sunday__c,
                OperatingHoursId, Monday_To__c, Tuesday_To__c, Wednesday_To__c, Thursday_To__c, Friday_To__c,
                Saturday_To__c, Sunday_To__c
            FROM Account
        ]);

        List<Account> accs2 = accs.values().deepClone(true, true, true);
        String startTime = '3:00 PM';
        String endTime = '4:00 PM';

        Test.startTest();
        WFM_Utils.createOperatingHoursForAccounts(accs2, accs);
        accs2[0].Monday__c = startTime;
        accs2[0].Monday_To__c = endTime;
        WFM_Utils.createOperatingHoursForAccounts(accs2, accs);
        Test.stopTest();

        accs = new Map<Id, Account>([
            SELECT Id, Name, Monday__c, Tuesday__c, Wednesday__c, Thursday__c, Friday__c, Saturday__c, Sunday__c,
                OperatingHoursId, Monday_To__c, Tuesday_To__c, Wednesday_To__c, Thursday_To__c, Friday_To__c,
                Saturday_To__c, Sunday_To__c
            FROM Account
        ]);

        Map<Id, Id> accId2OperatingHourId = new Map<Id, Id>();
        for(Account a : accs.values()) {
            accId2OperatingHourId.put(a.Id, a.OperatingHoursId);
        }

        List<TimeSlot> timeSlots = [
            SELECT Id, StartTime, EndTime, DayOfWeek, OperatingHoursId
            FROM Timeslot
            WHERE OperatingHoursId IN :accId2OperatingHourId.values()
        ];

        System.assert(!timeSlots.isEmpty(), 'Time slots should be retrieved correctly');

        TimeSlot modifiedTimeSlot;
        for(TimeSlot ts : timeSlots) {
            if(accId2OperatingHourId.get(accs2[0].Id) == ts.OperatingHoursId && ts.DayOfWeek == 'Monday') {
                modifiedTimeSlot = ts;
            }
        }
        System.assertEquals(WFM_Utils.convertStringToTime(startTime), modifiedTimeSlot.StartTime, 'Start time should be changed');
        System.assertEquals(WFM_Utils.convertStringToTime(endTime), modifiedTimeSlot.EndTime, 'End time should be changed');
    }
    
    static testMethod void test_createOnboardingWorkOrders(){
        Account acc=[select Id from Account limit 1];
        
        Case obCase=new Case(
            AccountId=acc.Id,
            Business__c='New',
            Status='Approved',
            Subject='[WFM] TEST',
            IDEXX_Projected_Monthly_Net_Invoice__c=1,
            Need_By_Date__c=DateTime.now(),
            RecordTypeId=WFM_Constants.CASE_CAG_DEAL_SHAPING_REQUEST_RECORDTYPE_ID
            );
            
        insert obCase;
        }
             
    static testMethod void test_createInstallWorkOrders() {
        Map<Id, Opportunity> opps = new Map<Id, Opportunity>([SELECT Id, StageName, AccountId, CloseDate FROM Opportunity]);
        List<Opportunity> newOpps = opps.values().deepClone(true, true, true);
        newOpps[0].StageName = WFM_Constants.OPPORTUNITY_STAGE_COMMITTED;

        Test.startTest();
        WFM_Utils.createInstallWorkOrders(newOpps, opps);
        Test.stopTest();

        List<WorkOrder> workOrders = [
            SELECT Id, RecordTypeId, Priority, Due_Date__c
            FROM WorkOrder
            WHERE Opportunity__c = :newOpps[0].Id
        ];

        List<WorkOrderLineItem> listWOLIs = [
            SELECT Id, RecordTypeId, Activity__c, Automatically_Created__c
            FROM WorkOrderLineItem
            WHERE WorkOrderId IN :workOrders
        ];

        System.assert(!workOrders.isEmpty(), 'Work Orders should be created for Opportunity');
        for(WorkOrder wo : workOrders) {
            System.assertEquals(WFM_Constants.WORK_ORDER_INSTALL_RECORDTYPE_ID, wo.RecordTypeId, 'Record type should be Install');
            System.assertEquals(WFM_Constants.WORK_ORDER_PRIORITY_HIGH, wo.Priority, 'Priority should be set to high');
            //System.assertEquals(newOpps[0].CloseDate, wo.Due_Date__c, 'Due date should be set to Opportunity closed date');
        }
        System.assert(!listWOLIs.isEmpty(), 'Work Order Line Items should be created for Opportunity');
        for(WorkOrderLineItem woli : listWOLIs) {
            System.assertEquals(WFM_Constants.WORK_ORDER_LINE_ITEM_INSTALL_RECORDTYPE_ID, woli.RecordTypeId, 'Record type should be Install');
            System.assertEquals(WFM_Constants.WORK_ORDER_LINE_ITEM_ACTIVITY_INSTALL_TRAINING, woli.Activity__c, 'Activity should be Install');
            System.assert(woli.Automatically_Created__c, 'Work Order Line Item should have automatically created flag set to true');
        }

    }
}