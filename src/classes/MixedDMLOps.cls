public without sharing class MixedDMLOps {
    // DML UPDATE operation - Uncomment if needed
    //public static Database.SaveResult[] up (Sobject[] objs) {
    //    Database.Saveresult[] updateRes;
    //    if (Test.isRunningTest()) {
    //        System.runAs(new User(Id = Userinfo.getUserId())) {
    //            updateRes = database.update(objs);
    //        }
    //    } else {
    //        updateRes = database.update(objs);
    //    }
    //    return updateRes;
    //}

    // DML DELETE - Uncomment if needed
    //public static Database.DeleteResult[] del (Sobject[] objs) {
    //    Database.DeleteResult[] delRes;
    //    if (Test.isRunningTest()) {
    //        System.runAs(new User(Id = Userinfo.getUserId())) {
    //            delRes = database.delete(objs);
    //        }
    //    } else {
    //        delRes = database.delete(objs);
    //    }
    //    return delRes;
    //}

    // DML INSERT
    public static Database.Saveresult[] ins (Sobject[] objs) {
        Database.Saveresult[] res;
        if (Test.isRunningTest()) {
            System.runAs(new User(Id = Userinfo.getUserId())) {
                res = database.insert(objs);
            }
        } else {
            res = database.insert(objs);
        }
        return res;
    }

    public static Database.Saveresult ins (Sobject obj) {
        Database.Saveresult res;
        if (Test.isRunningTest()) {
            System.runAs(new User(Id = Userinfo.getUserId())) {
                res = database.insert(obj);
            }
        } else {
            res = database.insert(obj);
        }
        return res;
    }

}