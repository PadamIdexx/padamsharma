/**
 * @author Heather Kinney
 * @description Batch and Plant implementation and wrapper classes.
 *
 *  Modification Log:
 * -----------------------------------------------------------------------------------------------------------------------------
 *  Developer                       Date               Description
 * -----------------------------------------------------------------------------------------------------------------------------
 *  Heather Kinney                  08/16/2016         Initial creation.
 *
 */
public class BatchAndPlant { 
    
    public static MaterialProcessingService.DT_STOCK_OVERVIEW_Response materialStockResponse; 	//public so we can generate mock responses for testing.
    public static List<BatchReservationService.BatchReservation> batchReservations; 			//public so we can generate mock responses for testing.
    
    public List<WrapperPlant> listWrapperPlant = new List<WrapperPlant>();
    public List<WrapperBatch> listWrapperBatch = new List<WrapperBatch>();
    
   	public static Boolean isResponseNull				{get;set;}
    public static String errorMsg						{get;set;}

    public WrapperPlantAndBatch plantAndBatchInfo       {get;set;}
    
    public Map<String, WrapperReservationAndQuantity> mapReservationsAndQuantities	{get;set;}
    
    private boolean isBatchReservationCalloutError; //set if we detect a non-200 error (aka NULL) coming out of the call to the BatchReservationService (SAP HANA)
    
    public BatchAndPlant() { 
        listWrapperPlant = new List<WrapperPlant>();
        listWrapperBatch = new List<WrapperBatch>(); 
        plantAndBatchInfo = new WrapperPlantAndBatch();
        plantAndBatchInfo.hasError = false;
		plantAndBatchInfo.errorMessage = '';
        isResponseNull = false;
        isBatchReservationCalloutError = false;
    }
    
    //3.22.2017 
    //Called from OrderCreationLPD.saveOrder method
    public Map<String, WrapperPlant> loadStockInfoForMultipleMaterials(String gCustomer, String gCountry, String gsalesOrg, 
                String distributionChannel, String gDivision, List<WrapperBatch> materialItems) {
        //System.debug('>>>>>> materialItems = ' + materialItems);                    
        List<WrapperPlant> retPlantInfo = new List<WrapperPlant>();
        Map<String, WrapperPlant> plantInfoMap = new Map<String, WrapperPlant>();
        
        try {
            String distributionChannelFormatted  = distributionChannel.replaceAll('(?i)[^a-z0-9]', '');
             MaterialProcessingService.DT_STOCK_OVERVIEW_Response stockOverViewResponse = 
                    getStockForMultipleMaterials(gCustomer, gCountry, gsalesOrg, distributionChannelFormatted, gDivision, materialItems);
            if (stockOverViewResponse != null) {
                if (stockOverViewResponse.STOCK != null) {
                    //process the return of STOCK...
                    for (MaterialProcessingService.STOCK_element stockElement: stockOverViewResponse.STOCK) {  
                        if (stockElement.PLANTS != null) {
                            for (MaterialProcessingService.PLANTS_element plantElement: stockElement.PLANTS) {
                                WrapperPlant wp = new WrapperPlant();
                                wp.Plant = plantElement.PLANT;
                                wp.commodity = plantElement.COMMODITY_CODE;
                                wp.countryOfOrigin = plantElement.COUNTRY_OF_ORIGIN;
                                wp.productMatNo  = stockElement.MATERIAL; 
                                //System.debug('>>>>>> wp = ' + wp);  
                                //US29008 - 5.23.2017 HKinney - commented out the check and dependency of populating the plantInfoMap based on BATCHES.
                                //          in the case of needing commodity code and country of origin (for quoting, in particular) we are not
                                //          dependent on batches. In fact, we may have some material line items come across with "Select Batch"/"On Backorder"
                                //          but we'll ALWAYS have PLANTS.
                                String mapKey = wp.productMatNo + ':' + wp.Plant;
                                //System.debug('>>>>>> mapKey = ' + mapKey);                                 
                                if (!plantInfoMap.containsKey(mapKey)) {
                                    //System.debug('>>>>>> adding wp ' + wp + ' via mapKey ' + mapKey + ' to plantInfoMap...');                                      
                                    plantInfoMap.put(mapKey, wp);    
                                } 
                                /*
                                if (plantElement.BATCHES != null) {
                                    System.debug('in BatchAndPlant - have BATCHES data...');
                                    for (MaterialProcessingService.BATCHES_element batchElement: plantElement.BATCHES) {
                                        wp.Batch = batchElement.BATCH;
                                        String mapKey = wp.productMatNo + ':' + wp.Batch;
                                        if (!plantInfoMap.containsKey(mapKey)) {
                                            plantInfoMap.put(mapKey, wp);    
                                        } 
                                    } //for - iterate over plantElement.BATCHES...
                                } //if - plantElement.BATCHES != null...
                                */
                            } //for - iterate over stockElement.PLANTS...
                        } //if stockElement.PLANTS != null...
                    } //for - iterate over stockOverViewResponse.STOCK...
                } //if stockOverViewResponse.STOCK != null...
            } //if stockOverViewResponse != null...
            
        } catch (Exception e) {
            System.debug(LoggingLevel.ERROR,'error in loadStockInfoForMultipleMaterials - ' + e.getMessage());            
        }   
        return plantInfoMap;
    }
       
    //Called from OrderCreationLPD.loadPlants method (called from UI/js call callLoadPlantsforBatch)
    public WrapperPlantAndBatch loadStockInfo(String gCustomer, String gCountry, String gsalesOrg, String distributionChannel, String gDivision, 
    	String gPlant, String gProdMat, String ExtItemNo, String gBatch){
    
        mapReservationsAndQuantities = loadReservations(gCustomer);
            
        if (!isBatchReservationCalloutError) {                                                                            
            String distributionChannelFormatted  = distributionChannel.replaceAll('(?i)[^a-z0-9]', '');
                                                                                           
            MaterialProcessingService.DT_STOCK_OVERVIEW_Response stockOverViewResponse = 
                getStock(gCustomer, gCountry, gsalesOrg, distributionChannelFormatted, gDivision, gPlant, gProdMat, gBatch); 
            
            if (stockOverViewResponse != null) {
                System.debug('stockOverViewResponse is not null: ' + stockOverViewResponse); 
                if (stockOverViewResponse.STOCK != null) {
                    System.debug('in BatchAndPlant - have STOCK overview data...');
                    listWrapperPlant = new List<WrapperPlant>();
                    listWrapperBatch = new List<WrapperBatch>();
                    //process the return of STOCK...
                    for (MaterialProcessingService.STOCK_element stockElement: stockOverViewResponse.STOCK) {    
                        //iterate through the STOCK elelemts returned. We may have only one for the material number at hand...
                        if (stockElement.PLANTS != null) {
                            System.debug('in BatchAndPlant - have PLANTS data...');
                            for (MaterialProcessingService.PLANTS_element plantElement: stockElement.PLANTS) {
                                WrapperPlant wp = new WrapperPlant();
                                wp.Plant = plantElement.PLANT;
                                wp.Available_Qty = plantElement.AVAILABLE_QTY;
                                wp.Allocated_Qty = plantElement.ALLOCATED_QTY;
                                wp.commodity = plantElement.COMMODITY_CODE;
                                wp.countryOfOrigin = plantElement.COUNTRY_OF_ORIGIN;
                                wp.productMatNo  = gProdMat;
                                wp.ExternalItemNo = ExtItemNo;
                                listWrapperPlant.add(wp); //list of Plant
                                //Logic to display message 'Stock List Not Found' when no plant information is returned from SAP
                                if (ListWrapperPlant.isEmpty()) {
                                    setNotFoundDataVariables();
                                }
                                if (plantElement.BATCHES != null) {
                                    System.debug('in BatchAndPlant - have BATCHES data...');
                                    for (MaterialProcessingService.BATCHES_element batchElement: plantElement.BATCHES) {
                                        WrapperBatch wpbatch = new WrapperBatch();
                                        wpbatch.Batch = batchElement.BATCH;
                                        wpbatch.Plant = wp.Plant;
                                        wpbatch.Store_loc= batchElement.STORE_LOC;
                                        wpbatch.Available_Qty= batchElement.AVAILABLE_QTY;
                                        wpbatch.exp_date = batchElement.EXPIRATION_DATE;
                                        wpbatch.productMatNo  = gProdMat;
                                        wpbatch.ExternalItemNo = ExtItemNo; 
                                        if ((wpbatch.Batch != null) && (wpbatch.Batch.length() > 0)) {
                                        	WrapperReservationAndQuantity resAndQty = findReservationAndQuantityByBatch(wpbatch.Batch);
                                            if (resAndQty != null) {
                                            	wpbatch.RemainingQty = resAndQty.availableQty; 
                                                wpbatch.wrapreservation = resAndQty.reservation;
                                            }
                                        }
                                        listWrapperBatch.add(wpbatch); //list of Batch
                                        //Logic to display message 'Stock List Not Found' when no batch information is returned from SAP
                                        if(listWrapperBatch.isEmpty()) {
                                            setNotFoundDataVariables();
                                        }
                                    }    
                                } else { //plantElement.BATCHES is NULL...
                                    setNotFoundDataVariables();
                                } //if the PLANTS has BATCHES...  	                                
                            } //for-loop to process all the PLANT elements...     
                        } else { //The STOCK has NO PLANTS...
                            setNotFoundDataVariables();    
                        } //if the STOCK has PLANTS...
                    } //for-loop to process all the STOCK elements 
                } else { //the response has NO STOCK elements
                    setNotFoundDataVariables(); 
                } //if the response has STOCK...
               	 plantAndBatchInfo = new WrapperPlantAndBatch(listWrapperPlant,listWrapperBatch); 
            } else { //callout to BatchReservations SAP HANA endpoint resulted in non 200 HTTP status:
            	setNotFoundDataVariables();
                //setting of plantAndBatchInfo.hasError and plantAndBatchInfo.errorMessage 
            } //isBatchReservationCalloutError check...
            
            //TODO: handle any return_x items coming across, as they'd indicate anything out of the norm for the display of batch and plant info?!?
            //      CONFIRM THIS!!!
            /*
            boolean hasReturnXItem = false;
            if (stockOverViewResponse.Return_x != null) {
            	setNotFoundDataVariables();
            	hasReturnXItem = true;    
            }
			*/
            
        }                                
        //plantAndBatchInfo = new WrapperPlantAndBatch(listWrapperPlant,listWrapperBatch); 
        return plantAndBatchInfo;                                          
    }
    
    public void setNotFoundDataVariables() {
    	isResponseNull = true; 
        errorMsg = 'Stock List Not Found';
    }
         
    public MaterialProcessingService.DT_STOCK_OVERVIEW_Response getStock(String gCustomer, String gCountry, 
    		String gsalesOrg, String gDistChan, String gDivision, String gPlant, String gProdMat, String gBatch) {

        MaterialProcessingService.DT_STOCK_OVERVIEW_Response response;
        
        MaterialProcessingService.DT_STOCK_OVERVIEW_Request request = new MaterialProcessingService.DT_STOCK_OVERVIEW_Request();
        MaterialProcessingService.ITEMS_element[] ITEMS = new MaterialProcessingService.ITEMS_element[]{};     
        MaterialProcessingService.ITEMS_element itemElement = new MaterialProcessingService.ITEMS_element();
        
        itemElement.CUSTOMER = gCustomer; //NB: we should always have customer!
        if (gCountry != null && gCountry.length() > 0) {
        	itemElement.COUNTRY = gCountry;
        }
        if (gsalesOrg != null && gsalesOrg.length() > 0) {
            itemElement.SALES_ORG = gsalesOrg;
        }
        if (gDistChan != null && gDistChan.length() > 0) {
            itemElement.DIST_CHAN = gDistChan;
        }
        if (gDivision != null && gDivision.length() > 0) {
            itemElement.DIVISION = gDivision;
        }
        if (gProdMat != null && gProdMat.length() > 0) {
            itemElement.MATERIAL = gProdMat;
        }
        if (gBatch != null && gBatch.length() > 0) {
            if (!gBatch.equalsIgnoreCase('Select Batch')) {
                itemElement.BATCH = gBatch;
            }
        }
        if (gPlant != null && gPlant.length() > 0) {
            if (!gPlant.equalsIgnoreCase('Select Batch')) {
                itemElement.PLANT = gPlant;
            }
        }
        
        ITEMS.add(itemElement);
        MaterialProcessingService.HTTPS_Port service = new MaterialProcessingService.HTTPS_Port();
        //System.debug('in BatchAndPlant.getStock - about to call service.StockQuantityQuery - with ITEMS: ' + ITEMS);  
        
        if(!Test.isRunningTest()){
        	response = service.StockQuantityQuery(ITEMS);
        } else {
            //we are in a test - obtain the response from a prepopulated object set in test mode.
            response = materialStockResponse;
        }
        return response;
    }
    
    //3.22.2017 - added
    //Unlike getStock, this method will accept a List of WrapperBatch wich will contain the material, batch, and associate plant. 
    //Used when we need to hand off data from the Order controller to the Quote controller.
    public MaterialProcessingService.DT_STOCK_OVERVIEW_Response getStockForMultipleMaterials(String gCustomer, String gCountry, 
    		String gsalesOrg, String gDistChan, String gDivision, List<WrapperBatch> materialItems) {

System.debug(LoggingLevel.ERROR,'in getStockForMultipleMaterials...');

        MaterialProcessingService.DT_STOCK_OVERVIEW_Response response;
        String EMPTY_ELEMENT = '';
        MaterialProcessingService.DT_STOCK_OVERVIEW_Request request = new MaterialProcessingService.DT_STOCK_OVERVIEW_Request();
        MaterialProcessingService.ITEMS_element[] ITEMS = new MaterialProcessingService.ITEMS_element[]{};     
        
        try {
            for (WrapperBatch wrapperBatch: materialItems) { 
                boolean hasBatch = false; //US29008
                
                MaterialProcessingService.ITEMS_element itemElement = new MaterialProcessingService.ITEMS_element(); //US29008 - moved from outside for loop to here...
                itemElement.CUSTOMER = gCustomer; //NB: we should always have customer! //US29008 - moved from outside for loop to here...
        
                if (gCountry != null && gCountry.length() > 0) {
                	itemElement.COUNTRY = gCountry;
                }
                if (gsalesOrg != null && gsalesOrg.length() > 0) {
                    itemElement.SALES_ORG = gsalesOrg;
                }
                if (gDistChan != null && gDistChan.length() > 0) {
                    itemElement.DIST_CHAN = gDistChan;
                }
                if (gDivision != null && gDivision.length() > 0) {
                    itemElement.DIVISION = gDivision;
                }
                if (wrapperBatch.productMatNo != null && wrapperBatch.productMatNo.length() > 0) {
                    itemElement.MATERIAL = wrapperBatch.productMatNo;
                }
                if (wrapperBatch.Batch != null && wrapperBatch.Batch.length() > 0) {
                    if ((!wrapperBatch.Batch.equalsIgnoreCase('Select Batch')) || (!wrapperBatch.Batch.equalsIgnoreCase(OrderCreationLPD.ORDER_LINE_ITEM_BATCH_ON_BACKORDER))) { //US29008 - added check for backorder string
System.debug('>>>>>> setting itemElement.BATCH  to ' + wrapperBatch.Batch);                    
                        itemElement.BATCH = wrapperBatch.Batch;
                        hasBatch = true;
                    }
                }
                if (!hasBatch) {
                    itemElement.BATCH = EMPTY_ELEMENT;
                }
                if (wrapperBatch.Plant != null && wrapperBatch.Plant.length() > 0) {
                    if (!wrapperBatch.Plant.equalsIgnoreCase('Select Plant')) {
                        itemElement.PLANT = wrapperBatch.Plant;
                    }
                }
System.debug('>>>>>> itemElement = ' + itemElement);                
                ITEMS.add(itemElement);
            } //end - iterating materialItems
            
            MaterialProcessingService.HTTPS_Port service = new MaterialProcessingService.HTTPS_Port();
System.debug(LoggingLevel.ERROR,'in BatchAndPlant.getStock - about to call service.StockQuantityQuery - with ITEMS: ' + ITEMS);  
            
            if(!Test.isRunningTest()){
            	response = service.StockQuantityQuery(ITEMS);
            } else {
                //we are in a test - obtain the response from a prepopulated object set in test mode.
                response = materialStockResponse;
            }
        } catch (Exception e) {
System.debug(LoggingLevel.ERROR,'getStockForMultipleMaterials error = e ' + e.getMessage());            
        }
        
System.debug(LoggingLevel.ERROR,'getStockForMultipleMaterials = ' + response);     
        return response;
    }

    public WrapperReservationAndQuantity findReservationAndQuantityByBatch(String batch) {
        WrapperReservationAndQuantity retWrapperResAndQty = null;
        if (mapReservationsAndQuantities.containsKey(batch)) {
        	retWrapperResAndQty = mapReservationsAndQuantities.get(batch);
        }
        return retWrapperResAndQty;
    }
    
    //Get reservations using the BatchReservations HANA endpoint via a HTTP GET.
    public Map<String, WrapperReservationAndQuantity> loadReservations(String gCustomer) {  
        //Need this method to set the following which are used later on when processing the MaterialProcessingService/StockOverview response:
        // - reservation
        // - reservationBatch
        // - availableQty (reserved quantity - consumed quantity)
        Map<String, WrapperReservationAndQuantity> mapReservationsAndQuantities = null;
        BatchReservationService batchReservationService = new BatchReservationService();
        List<BatchReservationService.BatchReservation> batchReservationsList;
        
        if(!Test.isRunningTest()){
            System.debug('right before callout to batchReservationService.getBatchReservationForCustomer for customer ' + gCustomer);
        	batchReservationsList = batchReservationService.getBatchReservationForCustomer(gCustomer);
            if (batchReservationsList == null) {
                isBatchReservationCalloutError = true;
            	plantAndBatchInfo.hasError = true;
        		plantAndBatchInfo.errorMessage = 'Unable to obtain Batch Reservation data.';
            }
        } else { //we are in a test - obtain the list from a prepopulated list of batch reservations
            batchReservationsList = batchReservations;
        }
        if (!isBatchReservationCalloutError) {
            mapReservationsAndQuantities = new Map<String, WrapperReservationAndQuantity>();
            Iterator<BatchReservationService.BatchReservation> batchReservationsListIterator = batchReservationsList.iterator();
            while (batchReservationsListIterator.hasNext()) {
                BatchReservationService.BatchReservation batchReservation = (BatchReservationService.BatchReservation)batchReservationsListIterator.next();
                System.debug('in while loop....');
                String reservation = String.valueof(batchReservation.reservation);
                String reservationBatch = String.valueof(batchReservation.batch);
                if (batchReservation.Reserved_Qty == null) {
                    batchReservation.Reserved_Qty = 0;
                }
                if (batchReservation.Consumed_Qty == null) {
                    batchReservation.Consumed_Qty = 0; 
                }
                Integer availableQty = Integer.valueOf(batchReservation.Reserved_Qty - batchReservation.Consumed_Qty);
                WrapperReservationAndQuantity resAndQty = new WrapperReservationAndQuantity();
                resAndQty.batch = reservationBatch;
                resAndQty.reservation = reservation;
                resAndQty.availableQty = availableQty;
                mapReservationsAndQuantities.put(reservationBatch, resAndQty);
            }
        }
        return mapReservationsAndQuantities;
    }

    
    public void setResponse(MaterialProcessingService.DT_STOCK_OVERVIEW_Response resp) {
        materialStockResponse = resp;  
    }
    
    public void setBatchReservationsList(List<BatchReservationService.BatchReservation> batchResList) {
        batchReservations = batchResList;
    }
    
  
    public class WrapperReservationAndQuantity {
        public String batch				{get;set;}
        public String reservation		{get;set;}
        public Integer availableQty		{get;set;}
        public WrapperReservationAndQuantity() {
            batch = '';
            reservation = '';
            availableQty = 0;
        }
    }
    
    public class WrapperPlant {
        public String Plant				{get;set;}
        public String Available_Qty		{get;set;}
        public String Allocated_Qty		{get;set;}
        public String commodity 		{get;set;}
        public String countryOfOrigin	{get;set;}
        public String productMatNo		{get;set;}
        public String ExternalItemNo	{get;set;}
        public String Batch             {get;set;} //3.22.2017 added
        public WrapperPlant(){
            Plant = '';
            Available_Qty = '';
            Allocated_Qty = '';
            commodity = '';
            countryOfOrigin = '';
            productMatNo = ''; 
            ExternalItemNo = '';
            Batch = '';
        }
    }
        
    public class WrapperBatch {
        public String Batch				{get;set;}
        public String Plant				{get;set;}
        public String Store_loc			{get;set;}
        public String Available_Qty		{get;set;}
        public String ExternalItemNo	{get;set;}
        public Date exp_date 			{get;set;}
        public String wrapreservation	{get;set;}
        public Integer RemainingQty		{get;set;}
        public String productMatNo		{get;set;}
        public WrapperBatch(){
            Batch= '';
            Plant='';
            Store_loc = '';
            Available_Qty = '';
            exp_date = system.today();
            wrapreservation = '';
            productMatNo = ''; 
            RemainingQty = 0; 
            ExternalItemNo = '';  
        }
    }
     
    //Wrapper class to return one plant and batch list
    public class WrapperPlantAndBatch {  
        public boolean hasError						{get;set;}
        public String errorMessage					{get;set;}
        public List<WrapperPlant> listWrapperPlant	{get;set;}
        public List<WrapperBatch> listWrapperBatch	{get;set;}
        public WrapperPlantAndBatch(){
            listWrapperPlant = new List<WrapperPlant>();
            listWrapperBatch = new List<WrapperBatch>();
        }
        public WrapperPlantAndBatch(List<wrapperPlant> plantWrapper, List<wrapperBatch> batchWrapper){
            listWrapperPlant = plantWrapper;
            listWrapperBatch = batchWrapper;
        }
    }

}