@isTest
public class OrderTestDataFactory {

    public static OrderChangeRequest.SalesOrderChangeRequest createChangeRequest() {
            SAPOrder order = OrderTestDataFactory.createSAPOrderForUpdateHeaderInfo();
        OrderChangeRequest.SalesOrderChangeRequest request = OrderChangeRequest.transform(order);
        
        return request;
    }    

    public static OrderChangeResponse.SalesOrderChangeResponse createChangeResponse() {
        OrderChangeResponse.SalesOrderChangeResponse response = new OrderChangeResponse.SalesOrderChangeResponse();
        
        List<OrderChangeResponse.Items_element> items = new List<OrderChangeResponse.Items_element>();
        OrderChangeResponse.Items_element it = new OrderChangeResponse.Items_element();
        
        items.add(it);
        response.Items = items;
        
        List<OrderChangeResponse.Return_element> return_x = new List<OrderChangeResponse.Return_element>();

        OrderChangeResponse.Return_element r = new OrderChangeResponse.Return_element();
        r.MessageType = 'W';  
        r.Message ='Warning';
        return_x.add(r);
        
        OrderChangeResponse.Return_element r2 = new OrderChangeResponse.Return_element();
        r2.MessageType = 'E';
        r2.Message ='Error';        
        return_x.add(r2);

        OrderChangeResponse.Return_element r3 = new OrderChangeResponse.Return_element();
        r3.MessageType = 'I';
        r3.Message ='Short dated batch';        
        return_x.add(r3);
        
        OrderChangeResponse.Return_element r4 = new OrderChangeResponse.Return_element();
        r4.MessageType = 'W';
        r4.Message = 'Warning';
        r4.MessageID ='ZPOC';        
        return_x.add(r4);   
        
        response.Return_x = return_x;
        
        return response;
        
    }    

    public static OrderReadRequest.SalesOrderReadRequest createReadRequest() {
        SAPOrder order = OrderTestDataFactory.createSAPOrder();
        OrderReadRequest.SalesOrderReadRequest request = OrderReadRequest.transform(order);
        
        return request;
    }   

    public static OrderReadResponse.SalesOrderReadResponse createReadResponse() {
        OrderReadResponse.SalesOrderReadResponse response = new OrderReadResponse.SalesOrderReadResponse();
        
        //3.8.2017 - header condition
        //000000
        
        List<OrderReadResponse.Items_element> items = new List<OrderReadResponse.Items_element>();
        OrderReadResponse.Items_element it = new OrderReadResponse.Items_element();      
        it.ItemNumber = '000010';
        items.add(it);
        response.Items = items;
        
        List<OrderReadResponse.Conditions_element> conditions = new List<OrderReadResponse.Conditions_element>();
        //3.8.2017 - adding POINTS condition
        OrderReadResponse.Conditions_element cPoints = new OrderReadResponse.Conditions_element();
        cPoints.ConditionType = 'ZPDH'; //points
        cPoints.ConditionValue = '20'; //
        cPoints.Currency_x = 'USD';
        cPoints.ConditionStep = '1';
        cPoints.ConditionCount = '1';
        conditions.add(cPoints);
        
        //3.8.2017 - adding VALUE DISCOUNT condition
        OrderReadResponse.Conditions_element cDiscountsValue = new OrderReadResponse.Conditions_element();
        cDiscountsValue.ConditionType = 'RB00'; //value discount
        cDiscountsValue.ConditionValue = '20'; //
        cDiscountsValue.ConditionRate = '20';
        cDiscountsValue.Currency_x = 'USD';
        cDiscountsValue.ConditionStep = '2';
        cDiscountsValue.ConditionCount = '2';
        conditions.add(cDiscountsValue);
        
        //3.8.2017 - adding PERCENTAGE DISCOUNT condition
        OrderReadResponse.Conditions_element cDiscountsPercentage = new OrderReadResponse.Conditions_element();
        cDiscountsPercentage.ConditionType = 'RA01'; //percentage discount
        cDiscountsPercentage.ConditionValue = '20'; //
        cDiscountsPercentage.ConditionRate = '20';
        cDiscountsPercentage.Currency_x = 'USD';
        cDiscountsPercentage.ConditionStep = '3';
        cDiscountsPercentage.ConditionCount = '3';
        conditions.add(cDiscountsPercentage);
        
        //3.8.2017 - adding ZF10 FREIGHT condition
        OrderReadResponse.Conditions_element cFreightZF10 = new OrderReadResponse.Conditions_element();
        cFreightZF10.ConditionType = 'ZF10'; //Freight
        cFreightZF10.ConditionValue = '20'; //
        cFreightZF10.ConditionRate = '20';
        cFreightZF10.Currency_x = 'USD';
        cFreightZF10.ConditionStep = '4';
        cFreightZF10.ConditionCount = '4';
        conditions.add(cFreightZF10);
        
        
        response.Conditions = conditions;
        
        List<OrderReadResponse.Partners_element> partners = new List<OrderReadResponse.Partners_element>();
        OrderReadResponse.Partners_element p = new OrderReadResponse.Partners_element();
        partners.add(p);
        response.Partners = partners;
        
        List<OrderReadResponse.Texts_element> texts = new List<OrderReadResponse.Texts_element>();
        OrderReadResponse.Texts_element t = new OrderReadResponse.Texts_element();
        texts.add(t);
        response.Texts = texts;

        return response;
        
    }    

    public static OrderDeleteRequest.SalesOrderDeleteRequest createDeleteRequest() {
            SAPOrder order = OrderTestDataFactory.createSAPOrder();
        OrderDeleteRequest.SalesOrderDeleteRequest request = OrderDeleteRequest.transform(order);
        
        return request;
    }     

    public static OrderDeleteResponse.SalesOrderDeleteResponse createDeleteResponse() {
        OrderDeleteResponse.SalesOrderDeleteResponse response = new OrderDeleteResponse.SalesOrderDeleteResponse();
       
        List<OrderDeleteResponse.Return_element> return_x = new List<OrderDeleteResponse.Return_element>();
        
        OrderDeleteResponse.Return_element r = new OrderDeleteResponse.Return_element();
        r.MessageType = 'W';  
        r.Message ='Warning';
        return_x.add(r);
        
        OrderDeleteResponse.Return_element r2 = new OrderDeleteResponse.Return_element();
        r2.MessageType = 'E';
        r2.Message ='Error';        
        return_x.add(r2);

        OrderDeleteResponse.Return_element r3 = new OrderDeleteResponse.Return_element();
        r3.MessageType = 'I';
        r3.Message ='Short dated batch';        
        return_x.add(r3);
        
        OrderDeleteResponse.Return_element r4 = new OrderDeleteResponse.Return_element();
        r4.MessageType = 'W';
        r4.Message = 'Warning';
        r4.MessageID ='ZPOC';        
        return_x.add(r4);            
        
        response.Return_x = return_x;
        
        return response;
        
    }  

    public static OrderSimulateRequest.SalesOrderSimulateRequest createSimulateRequest() {
            SAPOrder order = OrderTestDataFactory.createSAPOrder();
        OrderSimulateRequest.SalesOrderSimulateRequest request = OrderSimulateRequest.transform(order);
        
        return request;
    }    

    public static OrderSimulateResponse.SalesOrderSimulateResponse createSimulateResponse() {
        OrderSimulateResponse.SalesOrderSimulateResponse response = new OrderSimulateResponse.SalesOrderSimulateResponse();
        
        List<OrderSimulateResponse.Items_element> items = new List<OrderSimulateResponse.Items_element>();
        OrderSimulateResponse.Items_element it = new OrderSimulateResponse.Items_element();
        it.itemNumber = '000010';
        items.add(it);
        response.Items = items;
        
        List<OrderSimulateResponse.Conditions_element> conditions = new List<OrderSimulateResponse.Conditions_element>();
        OrderSimulateResponse.Conditions_element c = new OrderSimulateResponse.Conditions_element();
        conditions.add(c);
        response.Conditions = conditions;
        
        List<OrderSimulateResponse.Return_element> return_x = new List<OrderSimulateResponse.Return_element>();
        
        OrderSimulateResponse.Return_element r = new OrderSimulateResponse.Return_element();
        r.MessageType = 'W';  
        r.Message ='Warning';
        return_x.add(r);
        
        OrderSimulateResponse.Return_element r2 = new OrderSimulateResponse.Return_element();
        r2.MessageType = 'E';
        r2.Message ='Error';        
        return_x.add(r2);

        OrderSimulateResponse.Return_element r3 = new OrderSimulateResponse.Return_element();
        r3.MessageType = 'I';
        r3.Message ='Short dated batch';        
        return_x.add(r3);
        
        OrderSimulateResponse.Return_element r4 = new OrderSimulateResponse.Return_element();
        r4.MessageType = 'W';
        r4.Message = 'Warning';
        r4.MessageID ='ZPOC';        
        return_x.add(r4);        
       
        response.Return_x = return_x;
        
        return response;
        
    }    

    public static OrderCreateRequest.SalesOrderCreateRequest createCreateRequest() {
            SAPOrder order = OrderTestDataFactory.createSAPOrder();
            ProfileDisplayWrapper userProfile = new ProfileDisplayWrapper(); //3.10.2017 HLK - added profile to order.
        order.userProfile = userProfile;
        OrderCreateRequest.SalesOrderCreateRequest request = OrderCreateRequest.transform(order);
        
        return request;
    }    

    public static OrderCreateResponse.SalesOrderCreateResponse createCreateResponse() {
        OrderCreateResponse.SalesOrderCreateResponse response = new OrderCreateResponse.SalesOrderCreateResponse();
        
        List<OrderCreateResponse.Items_element> items = new List<OrderCreateResponse.Items_element>();
        OrderCreateResponse.Items_element it = new OrderCreateResponse.Items_element();
        
        items.add(it);
        response.Items = items;
        
        List<OrderCreateResponse.Return_element> return_x = new List<OrderCreateResponse.Return_element>();

        OrderCreateResponse.Return_element r = new OrderCreateResponse.Return_element();
        r.MessageType = 'W';  
        r.Message ='Warning';
        return_x.add(r);
        
        OrderCreateResponse.Return_element r2 = new OrderCreateResponse.Return_element();
        r2.MessageType = 'E';
        r2.Message ='Error';        
        return_x.add(r2);

        OrderCreateResponse.Return_element r3 = new OrderCreateResponse.Return_element();
        r3.MessageType = 'I';
        r3.Message ='Short dated batch';     
        return_x.add(r3);
        
        OrderCreateResponse.Return_element r4 = new OrderCreateResponse.Return_element();
        r4.MessageType = 'W';
        r4.Message = 'Warning';
        r4.MessageID ='ZPOC';        
        return_x.add(r4);        
       
        response.Return_x = return_x;
        
        return response;
        
    } 
    
    public static SAPOrder createSAPOrder() {
        SAPOrder order = new SAPOrder();

        order.deliveryBlock = '999999999';
        order.customerId = '0000016242';
        order.language = 'EN';
        order.salesOrg = 'USS1';
        order.division = 'CP';
        order.distributionChannel = '00';
        order.poNumber = '12345-Test';
        order.incoterms = '250';
        order.billingBlock = 'XX';
        order.couponCode = 'xx';

        order.discountType = 'Percent - %';
        order.discountValue = 10.00;
        order.discountCurrency  = 'USD';
        
        order.pointsRedeemed = 10.00;
        order.pointsRedeemedCurrency = 'USD';

        order.headerText = 'This is a test';
        order.headerTextLanguage = 'EN';

        order.cig = 'CIG';
        order.cug = 'CUG';
        order.cigcuglanguage = 'EN';
        
        order.completeDelivery = false; //3.8.2017
        
        order.freightType = 'X'; //3.10.2017 HLK added
        order.freightValue = 1.00; //3.10.2017 HLK added
        order.freightCurrency = 'USD'; //3.10.2017 HLK added
        
        order.requestedDeliveryDate = Date.newInstance(2017, 7, 8); //3.10.2017 HLK added
        order.freeGoodsOrder = false; //3.10.2017 HLK added
        order.referenceDocument = 'x'; //3.10.2017 HLK added
        order.referenceDocumentCategory = 'x'; //3.10.2017 HLK added
        order.rewardItemsEligible = false; //3.10.2017 HLK added
        order.externalProposalNumber = 'x'; //3.10.2017 HLK added

        order.isDelivered = false;  //3.10.2017 HLK added
        order.isPartiallyDelivered = true;  //3.10.2017 HLK added
        order.hasErrors = false;  //3.10.2017 HLK added
        order.hasWarnings = false;  //3.10.2017 HLK added
        order.hasShortDatedBatches = true;  //3.10.2017 HLK added
        order.hasCreditLimitBlock = false;  //3.10.2017 HLK added

        
        List<SAPOrder.Item> items = new List<SAPOrder.Item>();
        
        SAPOrder.Item i = new SAPOrder.Item();           
            i.itemNumber = '000010';
            i.externalItemNumber = '1'; //3.8.2017
        i.material = '99-99999-99';
        i.higherLevelItem = '99-99999-99'; //3.10.2017 HLK added
        i.itemText = 'x'; //3.10.2017 HLK added
        i.speedCode = 'ABC';
        i.shortText = 'test test test';
        i.batch = '1234';
        i.storageLocation = 'x';
        i.requestedQuantity = 1.00;
        i.itemCategory = 'x';
        i.netValue = Decimal.valueOf('1.23');
        i.currencyCode = 'USD';
        i.uom = 'x';
        i.deliveryDate = Date.newInstance(2017, 7, 8);
        i.requestedQuantity = 1.00;
        i.confirmedQuantity = 1.00;
        i.plant = 'x';
        i.shippingPoint = 'x';
        i.itemCategory = 'x';
        i.storageLocation = 'x';
            i.materialGroup = 'x';
        i.totalPrice = 1.00;
        i.netSubTotal = 1.00;
        i.netPrice = 1.00;
        i.shippingCharges = 1.00;
        i.freightChanged = false;
        i.tax = 1.00;
        i.discount = 1.00;
        i.discountType = 'Percent - %';
        i.discountValue = 10.00;
        i.discountCurrency  = 'USD';        
        i.batch = 'x';
        i.incoterms = 'x';
        i.batchedItem = true;
        i.batchExpirationDate = Date.newInstance(2017, 7, 8);
        i.isDelivered = true;   
        i.freightRate = 1.0;
        items.add(i);
        
        SAPOrder.Item i2 = new SAPOrder.Item();           
            i2.itemNumber = '000020';
            i2.externalItemNumber = '2'; //3.8.2017
        i2.material = '11-11111-11';
        i2.higherLevelItem = '11-11111-11'; //3.10.2017 HLK added
        i2.itemText = 'x'; //3.10.2017 HLK added
        i2.speedCode = 'ABC';
        i2.shortText = 'test test test';
        i2.batch = '1234';
        i2.storageLocation = 'x';
        i2.requestedQuantity = 1.00;
        i2.itemCategory = 'x';
        i2.netValue = Decimal.valueOf('1.23');
        i2.currencyCode = 'USD';
        i2.uom = 'x';
        i2.deliveryDate = Date.newInstance(2017, 7, 8);
        i2.requestedQuantity = 1.00;
        i2.confirmedQuantity = 1.00;
        i2.plant = 'x';
        i2.shippingPoint = 'x';
        i2.itemCategory = 'x';
        i2.storageLocation = 'x';
            i2.materialGroup = 'x';
        i2.totalPrice = 1.00;
        i2.netSubTotal = 1.00;
        i2.netPrice = 1.00;
        i2.shippingCharges = 1.00;
        i2.freightChanged = false;        
        i2.tax = 1.00;
        i2.discount = 1.00;
        i2.batch = 'x';
        i2.incoterms = 'x';
        i2.batchedItem = true;
        i2.batchExpirationDate = Date.newInstance(2017, 7, 8);
        i2.isDelivered = true;
        
        i2.freeGoods = true;
        i2.freeGoodsAgent = 'xxxx';
        i2.freeGoodsReason = 'xxxx';
        i2.freeGoodsCostCenter = 'xxxx';
        
        items.add(i2);
        
        //line item with a discount - value 
        //added 3.10.2017
        SAPOrder.Item i3 = new SAPOrder.Item();           
            i3.itemNumber = '000030';
            i3.externalItemNumber = '3'; //3.8.2017
        i3.material = '99-99999-98';
        i3.higherLevelItem = '99-99999-98'; //3.10.2017 HLK added
        i3.itemText = 'x'; //3.10.2017 HLK added
        i3.speedCode = 'ABC';
        i3.shortText = 'test test test';
        i3.batch = '1234';
        i3.storageLocation = 'x';
        i3.requestedQuantity = 1.00;
        i3.itemCategory = 'x';
        i3.netValue = Decimal.valueOf('1.23');
        i3.currencyCode = 'USD';
        i3.uom = 'x';
        i3.deliveryDate = Date.newInstance(2017, 7, 8);
        i3.requestedQuantity = 1.00;
        i3.confirmedQuantity = 1.00;
        i3.plant = 'x';
        i3.shippingPoint = 'x';
        i3.itemCategory = 'x';
        i3.storageLocation = 'x';
            i3.materialGroup = 'x';
        i3.totalPrice = 1.00;
        i3.netSubTotal = 1.00;
        i3.netPrice = 1.00;
        i3.shippingCharges = 1.00;
        i3.freightChanged = false;
        i3.tax = 1.00;
        i3.discount = 1.00;
        i3.discountType = 'Value';
        i3.discountValue = 10.00;
        i3.discountCurrency  = 'USD';        
        i3.batch = 'x';
        i3.incoterms = 'x';
        i3.batchedItem = true;
        i3.batchExpirationDate = Date.newInstance(2017, 7, 8);
        i3.shortDatedBatch = true; //3.10.2017 HLK added
        i3.isDelivered = true;   
        i3.freightRate = 1.0;
        i3.points = 10.0; //3.10.2017 HLK added
        items.add(i3);
        
        order.items = items;

        List<SAPOrder.Partner> partners = new List<SAPOrder.Partner>();
        
        SAPOrder.Partner p = new SAPOrder.Partner();
        
        p.onetimeShippingChange = false;
        p.partnerType = 'x';
        p.customer = '0000016242';
        p.name = 'x';
        p.name2 = 'x';
        p.street = 'x';
        p.country = 'x';
        p.postalCode = 'x';
        p.city = 'x';
        p.district = 'x';
        p.region = 'x';
        p.transportationZone = 'x';
        
        partners.add(p);
        order.partners = partners;     
        
        
        List<SAPOrder.CreditCard> cards = new List<SAPOrder.CreditCard>();
        
        SAPOrder.CreditCard cc = new SAPOrder.CreditCard();
        cc.creditCardType = 'VISA';
        cc.creditCardNumber = '1111222233334444';
        cc.creditCardDate = Date.newInstance(2016, 7, 8);
        cc.creditCardName = 'x';
        
        cards.add(cc);
        order.creditcards = cards;   
        
        //3.15.2017 added to resolve apex text class execution failures.
        ProfileDisplayWrapper userProfile = new ProfileDisplayWrapper(); //3.10.2017 HLK - added profile to order.
        order.userProfile = userProfile;

    
        return order;
    }

    public static SAPOrder createSAPOrderForUpdateHeaderInfo() {
        SAPOrder order = createSAPOrder();

            order.updateMap = new Map<String, String>();
        order.updateMap.put('incoterms', 'U');
        order.updateMap.put('deliveryBlock', 'U');
            order.updateMap.put('billingBlock', 'U');        
            order.updateMap.put('poNumber', 'U');                
            order.updateMap.put('couponCode','U');    
        order.updateMap.put('discountType','U');
        
        order.updateMap.put('CIG','U');
        order.updateMap.put('CUG','U');
            order.updateMap.put('headerText','U');   

        order.updateMap.put('pointsRedeemed', 'U');
        
        order.updateMap.put('freightType', 'U'); //3.10.2017 HLK added
        
        return order;
    }

    public static SAPOrder createSAPOrderForDeleteHeaderInfo() {
        SAPOrder order = createSAPOrder();
         
        order.updateMap = new Map<String, String>();
        order.updateMap.put('incoterms', 'D');
        order.updateMap.put('deliveryBlock', 'D');
            order.updateMap.put('billingBlock', 'D');        
            order.updateMap.put('poNumber', 'D');                
            order.updateMap.put('couponCode','D'); 
        order.updateMap.put('discountType','D');
        
        order.updateMap.put('CIG','D');
        order.updateMap.put('CUG','D');
            order.updateMap.put('headerText','D');        
        
        order.updateMap.put('pointsRedeemed', 'D');
       
        return order;
    }

    public static SAPOrder createSAPOrderForUpdateItem() {
        SAPOrder order = createSAPOrder();
         
        order.updateMap = new Map<String, String>();
        
        order.updateMap.put('items', 'U');

        order.items.get(0).updateMap = new Map<String, String>();
        order.items.get(0).updateMap.put('itemNumber', 'U');
            order.items.get(0).updateMap.put('requestedQuantity', 'U');        
            order.items.get(0).updateMap.put('material', 'U');     
        order.items.get(0).updateMap.put('freeGoods', 'U');
            order.items.get(0).updateMap.put('freeGoodsAgent', 'U');        
            order.items.get(0).updateMap.put('freeGoodsCostCenter', 'U');        
            order.items.get(0).updateMap.put('freeGoodsReason', 'U');        
            order.items.get(0).updateMap.put('itemCategory', 'U');                
            order.items.get(0).updateMap.put('batch', 'U');                
            order.items.get(0).updateMap.put('plant', 'U');        
            order.items.get(0).updateMap.put('storageLocation', 'U');                
            order.items.get(0).updateMap.put('shippingPoint', 'U');                
            order.items.get(0).updateMap.put('designatedBatch', 'U');
            order.items.get(0).updateMap.put('incoterms', 'U');                
            order.items.get(0).updateMap.put('useCustomerIncoterms', 'U');     
            order.items.get(0).updateMap.put('requestedDeliveryDate', 'U');  
            order.items.get(0).updateMap.put('deliveryDate', 'U');  //3.10.2017 HLK added
            order.items.get(0).updateMap.put('discountType', 'U');
            order.items.get(0).updateMap.put('freightRate', 'U'); //3.10.2017 HLK added
            
      
        return order;
    }


    public static SAPOrder createSAPOrderForInsertItem() {
        SAPOrder order = createSAPOrder();
        
        order.updateMap = new Map<String, String>();
        
        order.updateMap.put('pointsRedeemed', 'I'); //3.10.2017 HLK added
        order.updateMap.put('discountType', 'I'); //3.10.2017 HLK added
        order.updateMap.put('freightType', 'I'); //3.10.2017 HLK added
        order.updateMap.put('CIG', 'I'); //3.10.2017 HLK added
        order.updateMap.put('CUG', 'I'); //3.10.2017 HLK added
        order.updateMap.put('items', 'I');

        order.items.get(0).updateMap = new Map<String, String>();
        order.items.get(0).updateMap.put('itemNumber', 'I');
            order.items.get(0).updateMap.put('requestedQuantity', 'I');        
            order.items.get(0).updateMap.put('material', 'I');     
        order.items.get(0).updateMap.put('freeGoods', 'I');
            order.items.get(0).updateMap.put('freeGoodsAgent', 'I');        
            order.items.get(0).updateMap.put('freeGoodsCostCenter', 'I');        
            order.items.get(0).updateMap.put('freeGoodsReason', 'I');        
            order.items.get(0).updateMap.put('itemCategory', 'I');                
            order.items.get(0).updateMap.put('batch', 'I');                
            order.items.get(0).updateMap.put('plant', 'I');        
            order.items.get(0).updateMap.put('storageLocation', 'I');                
            order.items.get(0).updateMap.put('shippingPoint', 'I');                
            order.items.get(0).updateMap.put('designatedBatch', 'I');
            order.items.get(0).updateMap.put('incoterms', 'I');                
            order.items.get(0).updateMap.put('useCustomerIncoterms', 'I');     
            order.items.get(0).updateMap.put('requestedDeliveryDate', 'I');  
            order.items.get(0).updateMap.put('deliveryDate', 'I');  //3.10.2017 HLK added
            order.items.get(0).updateMap.put('discountType', 'I');     
            order.items.get(0).updateMap.put('freightRate', 'I'); //3.10.2017 HLK added
            
      
        return order;
    }
    
    
      public static SAPOrder createSAPOrderForDeleteItem() {
        SAPOrder order = createSAPOrder();
         
        order.updateMap = new Map<String, String>();      
        order.updateMap.put('items', 'D');

        order.items.get(0).updateMap = new Map<String, String>();
            order.items.get(0).updateMap.put('itemNumber', 'D');
       
        
        return order;
    }  
    
    //3.10.2017 HLK added
    public static SAPOrder createSAPOrderForDeleteBatchOfItem() {
        SAPOrder order = createSAPOrder();
         
        order.updateMap = new Map<String, String>();      
        order.updateMap.put('items', 'U');

        order.items.get(0).updateMap = new Map<String, String>();
            order.items.get(0).updateMap.put('batch', 'D');
            order.items.get(0).updateMap.put('designatedBatch', 'D');

        return order;
    }  
    
    public static SAPOrder createSAPOrderForUpdatePartner() {
        SAPOrder order = createSAPOrder();
         
        order.updateMap = new Map<String, String>();
        
        order.updateMap.put('partners', 'U');

        order.partners.get(0).updateMap = new Map<String, String>();
        order.partners.get(0).updateMap.put('partnerType', 'U');
            order.partners.get(0).updateMap.put('customer', 'U');        
            order.partners.get(0).updateMap.put('name', 'U');        
            order.partners.get(0).updateMap.put('name2', 'U');        
            order.partners.get(0).updateMap.put('street', 'U');        
            order.partners.get(0).updateMap.put('country', 'U');                
            order.partners.get(0).updateMap.put('postalCode', 'U');                
            order.partners.get(0).updateMap.put('city', 'U');                
            order.partners.get(0).updateMap.put('district', 'U');                
            order.partners.get(0).updateMap.put('region', 'U');
      
        return order;
    }    
    
      public static SAPOrder createSAPOrderForDeletePartner() {
        SAPOrder order = createSAPOrder();
         
        order.updateMap = new Map<String, String>();      
        order.updateMap.put('partners', 'D');

        order.partners.get(0).updateMap = new Map<String, String>();
            order.partners.get(0).updateMap.put('partnerType', 'D');
       
        
        return order;
    }  
}