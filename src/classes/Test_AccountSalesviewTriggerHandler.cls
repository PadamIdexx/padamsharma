/**
*       @author Aditya Sangawar
*       @date   22/01/2015  
        @description   Test Class for covering the AccountSalesviewTriggerHandler.
         This will copy pricegroup from Salesview and put it in Doctor Discount field on account
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Aditya Sangawar                 22/01/2015          Created
                  
**/
@isTest(SeeAllData = False)
    public With Sharing class Test_AccountSalesviewTriggerHandler{
        
        static testMethod void testAccountSalesview_Create(){
    
            Account acc = CreateTestClassData.createCustomerAccount();
            acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Water').getRecordTypeId();
            update acc;
            Salesview__c SV = CreateTestClassData.CreateSalesview();
            
            Test.StartTest();            
            List < Account_Salesview__c > lstsalesviewrecords = new List < Account_Salesview__c > ();
            
                for(Integer i=1; i<=10;i++){
                    Account_Salesview__c tempSV = new Account_Salesview__c();
                    tempSV.Name = 'ABC';
                    tempSV.SalesOrg_DistChann_Div_Cust__c = 'idexx123'+i ;                  
                    tempSV.Account__c = acc.Id;
                    tempSV.SalesView__c =  SV.Id;    
                    tempSV.Price_Group__c = 'DD';
                    tempSV.Price_Group_Dr_Discount__c = 'DD';
                    lstsalesviewrecords.add(tempSV);
                }
            insert lstsalesviewrecords;
            system.assertequals(acc.name,'Customer Testing Account'); 
            Test.StopTest();
        }
        
        static testMethod void testAccountSalesview_Update(){
    
            Account acc = CreateTestClassData.createCustomerAccount();
            acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Water').getRecordTypeId();
            update acc;
            Salesview__c SV = CreateTestClassData.CreateSalesview();
            
            Test.StartTest();            
            List < Account_Salesview__c > lstsalesviewrecords = new List < Account_Salesview__c > ();           
            List < Account_Salesview__c > lstsalesviewrecordsupdate = new List < Account_Salesview__c > ();
            
            
                for(Integer i=1; i<=10;i++){
                    Account_Salesview__c tempSV = new Account_Salesview__c();
                    tempSV.Name = 'ABC';
                    tempSV.SalesOrg_DistChann_Div_Cust__c = 'idexx123'+i ;                  
                    tempSV.Account__c = acc.Id;
                    tempSV.SalesView__c =  SV.Id;                   
                    lstsalesviewrecords.add(tempSV);
                }
            insert lstsalesviewrecords; 
            Integer i = 0;
            for (Account_Salesview__c tempSV : lstsalesviewrecords){
                    
                    tempSV.Name = 'ABCD';
                    tempSV.SalesOrg_DistChann_Div_Cust__c = 'idexx1234'+i ;                  
                    tempSV.Account__c = acc.Id;
                    tempSV.SalesView__c =  SV.Id;                   
                    lstsalesviewrecordsupdate.add(tempSV);
                    i++;
                
            }
            update lstsalesviewrecordsupdate;
            system.assertequals(acc.name,'Customer Testing Account'); 
            Test.StopTest();
        }
        
        
        static testMethod void testAccountSalesview_Delete(){
    
            Account acc = CreateTestClassData.createCustomerAccount();
            //Account acc = CreateTestClassData.createCustomerAccount();
            acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Water').getRecordTypeId();
            update acc;
            
            Salesview__c SV = CreateTestClassData.CreateSalesview();
            
            Test.StartTest();            
            List < Account_Salesview__c > lstsalesviewrecords = new List < Account_Salesview__c > ();
            
                for(Integer i=1; i<=10;i++){
                    Account_Salesview__c tempSV = new Account_Salesview__c();
                    tempSV.Name = 'ABC';
                    tempSV.SalesOrg_DistChann_Div_Cust__c = 'idexx123'+i ;                  
                    tempSV.Account__c = acc.Id;
                    tempSV.SalesView__c =  SV.Id;                   
                    lstsalesviewrecords.add(tempSV);
                }
            insert lstsalesviewrecords; 
            delete lstsalesviewrecords;
            system.assertequals(acc.name,'Customer Testing Account'); 
            Test.StopTest();
        }
    }