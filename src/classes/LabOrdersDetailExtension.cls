public class LabOrdersDetailExtension {
    
    public static String resultDetails{get;set;}
    public List<LabOrders.labOrder> result{get;set;}
    public String errorMsg{get;set;}
    public static String newString{get;set;}
    public static String resultString{get;set;}
    public static transient String accId{get;set;}
    public LabOrders.labOrder orderObj{get;set;}
    public labOrders.collections_element collections{get;set;}
    public List<Map<String, String>> assayObj{get;set;}
    public List<LabOrders.labOrder> orders{get;set;}
    
    public Account acc{get; set;}
    
    private final Account acct;
    
    public AsyncLabOrders.getLabOrderDetailsResponse_elementFuture labOrdersFuture2{get;set;}
    
    public LabOrdersDetailExtension(ApexPages.StandardController stdController){
        this.acct = (Account)stdController.getRecord();
        acc = acct;
        
    }
    
    public static LabOrders.labReportSearchRequest_element reqEle = new LabOrders.labReportSearchRequest_element();

    public Continuation detailRequest(){
                
        errorMsg = '';

        accId = '';
        accId = ApexPages.currentPage().getParameters().get('accId');
        
        transient List <String> accIds = new List<String>();
        accIds.add(accId);
        
        Integer Timeout = 60;
        Continuation cont = new Continuation(Timeout);
        
        cont.ContinuationMethod = 'processResponse';
        
        reqEle.accessionId = accIds;
        
        String[] contexts = new List<String>();
        contexts.add('1');
        contexts.add('16');
        reqEle.internalContextID = contexts;
        
        try{
            AsyncLabOrders.AsyncLabOrderServiceSoapPort 
                LabOrdersService =
                new AsyncLabOrders.AsyncLabOrderServiceSoapPort();
            labOrdersFuture2 = new AsyncLabOrders.getLabOrderDetailsResponse_elementFuture();
            LabOrdersFuture2 = LabOrdersService.beginGetLabOrderDetails(cont, reqEle);
            
        }
        catch(exception e){
            errorMsg = 'Error fetching records: ' + e;
            return null;
        }
        return cont;
        
        
    }
    public PageReference processResponse(){
        resultDetails = getDetails();
        assayObj = new List<Map<String,String>>();
        
        transient LabOrders.labOrderReport_element ele = labOrdersFuture2.getValue();
        transient LabOrders.labOrders_element e = ele.labOrders;
        
        
        orders = new List<LabOrders.labOrder>(e.labOrder);
        
        System.debug(logginglevel.INFO, 'orders ' + orders.size());
        
        
        if(orders.size() != 0){

            resultString = JSON.serialize(orders,false);

            resultString = resultString.escapeJava();
            
            return null;
        }
        else{
            errorMsg = 'No Lab Orders Available for this Date Range';
            return null;
            
        }
        
    }
    
    public String getDetails(){
        
        resultDetails = '';
        transient String order = '';
        transient List <String> lString = new List<String>();
        newString = '';
        
        try{
            order = ApexPages.currentPage().getParameters().get('id');
            resultDetails = ApexPages.currentPage().getParameters().get('det');
            resultDetails = resultDetails.normalizeSpace();
            resultDetails = resultDetails.remove('"{');
            resultDetails = resultDetails.remove('}"');

            lString = resultDetails.split(', ');
            transient List<String> newList = new List<String>();
            newString+='{';
            for(String s: lString){
                s=s.deleteWhitespace();
                if(s.length() > 1){
                    s = s.trim();
                    s = s.replaceAll('=', ': ');
                    string r = s.substringBefore(':');
                    r = r.remove(' ');
                    r ='"'+r+'"';
                    s = s.replace(s.substringBefore(':'), r);
                    string t = s.substringAfter(':');
                    t = '"'+t+'"';
                    s = s.replace(s.substringAfter(':'), t);
                    
                    newList.add(s);
                    newString+= ' ' + s + ', ';
                }
                
            }
            newString+='}';
            newString = newString.replace(', }', '}');
            newString = newString.replace('{ ', '{');
            resultDetails = newString;
            lString = newList;
            return resultDetails;
            
        }
        catch(exception ex){
            System.debug('error with page param ' + ex.getCause());
            return null;
        }
        
        
    }
    
    
}