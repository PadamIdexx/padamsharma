@istest
public class Test_CPQQuotesResource {
    
    static testMethod void testDoPost() {
        
        IDEXX_Integration_Service_Properties__c customsettings = new IDEXX_Integration_Service_Properties__c();
        customsettings.Name = 'CPQ-DEFAULT_PRICE_BOOK';
		customsettings.TextVal__c = 'IDEXX Standard Price Book';
        insert customsettings;
        
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'Test Opportunity';
        testOpportunity.Start_Date__c = System.today();
        testOpportunity.CloseDate = System.today().addMonths(10);
        testOpportunity.stageName = '2 Discovery';
        insert testOpportunity;
        
        Product2 product = new Product2();
        product.Name = 'Test';
        product.ProductCode = '99-14732';
        product.SAP_MATERIAL_NUMBER__c = '99-14732';
        product.IsActive = true;
        insert product;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        Pricebook2 priceBook = new Pricebook2(Name = 'IDEXX Standard Price Book', isActive=true);
        insert priceBook;
        
        PricebookEntry pBkEntry = new PricebookEntry();
        pBkEntry.Pricebook2Id = pricebookId;
        pBkEntry.Product2Id = product.Id;
        pBkEntry.CurrencyIsoCode = 'USD';
        pBkEntry.UnitPrice = 10;
        pBkEntry.PriceBook_SAPMatNr__c = 'Standard Price Book99-14732USD';
        pBkEntry.IsActive = true;
        insert pBkEntry;        
        
        pBkEntry = new PricebookEntry();
        pBkEntry.Pricebook2Id = priceBook.Id;
        pBkEntry.Product2Id = product.Id;
        pBkEntry.CurrencyIsoCode = 'USD';
        pBkEntry.UnitPrice = 1000;
        pBkEntry.PriceBook_SAPMatNr__c = 'IDEXX Standard Price Book99-14732USD';
        pBkEntry.IsActive = true;
        insert pBkEntry;
        
        List <PricebookEntry> dbPricebookEntries1 = [SELECT bookEntry.Id, bookEntry.productcode, bookEntry.Pricebook2Id, bookEntry.Product2Id FROM PricebookEntry bookEntry where bookEntry.Pricebook2Id in (select Id FROM Pricebook2 where Name = 'IDEXX Standard Price Book' and isActive = true) and bookEntry.Product2Id in (select Id FROM Product2) and bookEntry.CurrencyIsoCode = 'USD' and bookEntry.productcode in ('99-14732')];    
        
        CPQQuotesResource.CreateOrUpdateQuote createOrUpdateQuote = new CPQQuotesResource.CreateOrUpdateQuote();
        CPQQuotesResource.CreateOrUpdateQuote_Quote quote = new CPQQuotesResource.CreateOrUpdateQuote_Quote();
        CPQQuotesResource.QuoteItems_element quoteItems = new CPQQuotesResource.QuoteItems_element();
        CPQQuotesResource.QuoteItem quoteItem = new CPQQuotesResource.QuoteItem();
        CPQQuotesResource.Programs_element programs = new CPQQuotesResource.Programs_element();
        
        List<CPQQuotesResource.CreateOrUpdateQuote_Program> programList = new List<CPQQuotesResource.CreateOrUpdateQuote_Program>();
        
        CPQQuotesResource.CreateOrUpdateQuote_Program program = new CPQQuotesResource.CreateOrUpdateQuote_Program();
        program.ProgramName = 'Test1';
        programList.add(program);
        program = new CPQQuotesResource.CreateOrUpdateQuote_Program();
        program.ProgramName = 'Test2';
        programList.add(program);
        programs.Program = programList;
        
        quoteItem.MaterialNumber = '99-14732';
        quoteItem.MaterialDescription = 'Catalyst Dx Analyzer';
        quoteItem.Quantity = 1;
        quoteItem.UnitAmount = 19995.0;
        quoteItem.TotalAmount = 22001.77;
        
        List<CPQQuotesResource.QuoteItem> quoteItemList = new List<CPQQuotesResource.QuoteItem>();
        quoteItemList.add(quoteItem);
        quoteItems.QuoteItem = quoteItemList;
        
        quote.OpportunityId = testOpportunity.Id;
        quote.TransactionId = '448654144';
        quote.QuoteNumber = '2016-61967-9';
        quote.QuoteType = 'Proposal';
        quote.QuoteDescription = '';
        quote.QuoteStatus = 'Approved';
        quote.QuoteAmount = 22720.29;
        quote.RegionalMgrAmount= 100.0;
        quote.CurrencyCode = 'USD';
        quote.QuoteCreatedDate = Datetime.valueOf('2016-04-15 14:11:00');
        quote.QuoteCreatedBy = '';
        quote.QuoteLastModifiedDate = Datetime.valueOf('2016-04-15 14:11:00');
        quote.QuoteLastModifiedBy = '';
        quote.QuoteSubmittedDate = Datetime.valueOf('2016-04-15 14:11:00');
        quote.QuoteExpirationDate = Datetime.valueOf('2016-04-15 14:11:00');
        quote.QuoteApprovalDate = Datetime.valueOf('2016-04-15 14:11:00');
        
        quote.QuoteItems = quoteItems;
        quote.Programs = programs;
                
        
        createOrUpdateQuote.quote = quote;
            
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/cpqquotes';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        CPQQuotesResource.createOrUpdateQuoteResponse response = CPQQuotesResource.dopost(createOrUpdateQuote);
        
        response = CPQQuotesResource.dopost(createOrUpdateQuote);
        
        System.debug('response'+response);
    }
    
    
    static testMethod void testDoPostCA() {
        
        IDEXX_Integration_Service_Properties__c customsettings = new IDEXX_Integration_Service_Properties__c();
        customsettings.Name = 'CPQ-DEFAULT_PRICE_BOOK';
		customsettings.TextVal__c = 'Standard Price Book';
        insert customsettings;
        
        
        Account testAccount = new Account();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = 'Test Canada Account';
        testAccount.CurrencyISOCode = 'CAD';
        testAccount.BillingCountry = 'CANADA';
        testAccount.ShippingCountry = 'CANADA';
        testAccount.HIN__c = 'Test123';
        testAccount.SAP_Customer_Number__c = '123456789';
        testAccount.Order_Email__c = 'test@email.com';
        insert testAccount;
        
        
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.AccountId = testAccount.id;
        testOpportunity.Name = 'Test Opportunity';
        testOpportunity.Start_Date__c = System.today();
        testOpportunity.CloseDate = System.today().addMonths(10);
        testOpportunity.stageName = '2 Discovery';
        insert testOpportunity;
        
        Product2 product = new Product2();
        product.Name = 'Test';
        product.ProductCode = '99-14732';
        product.SAP_MATERIAL_NUMBER__c = '99-14732';
        product.IsActive = true;
        insert product;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        Pricebook2 priceBook = new Pricebook2(Name = 'Standard Price Book', isActive=true);
        insert priceBook;
        
        PricebookEntry pBkEntry = new PricebookEntry();
        pBkEntry.Pricebook2Id = pricebookId;
        pBkEntry.Product2Id = product.Id;
        pBkEntry.CurrencyIsoCode = 'CAD';
        pBkEntry.UnitPrice = 10;
        pBkEntry.PriceBook_SAPMatNr__c = 'Standard Price Book99-14732USD';
        pBkEntry.IsActive = true;
        insert pBkEntry;        
       
        List <PricebookEntry> dbPricebookEntries1 = [SELECT bookEntry.Id, bookEntry.productcode, bookEntry.Pricebook2Id, bookEntry.Product2Id FROM PricebookEntry bookEntry where bookEntry.Pricebook2Id in (select Id FROM Pricebook2 where Name = 'IDEXX Standard Price Book' and isActive = true) and bookEntry.Product2Id in (select Id FROM Product2) and bookEntry.CurrencyIsoCode = 'USD' and bookEntry.productcode in ('99-14732')];    
        
        CPQQuotesResource.CreateOrUpdateQuote createOrUpdateQuote = new CPQQuotesResource.CreateOrUpdateQuote();
        CPQQuotesResource.CreateOrUpdateQuote_Quote quote = new CPQQuotesResource.CreateOrUpdateQuote_Quote();
        CPQQuotesResource.QuoteItems_element quoteItems = new CPQQuotesResource.QuoteItems_element();
        CPQQuotesResource.QuoteItem quoteItem = new CPQQuotesResource.QuoteItem();
        CPQQuotesResource.Programs_element programs = new CPQQuotesResource.Programs_element();
        
        List<CPQQuotesResource.CreateOrUpdateQuote_Program> programList = new List<CPQQuotesResource.CreateOrUpdateQuote_Program>();
        
        CPQQuotesResource.CreateOrUpdateQuote_Program program = new CPQQuotesResource.CreateOrUpdateQuote_Program();
        program.ProgramName = 'Test1';
        programList.add(program);
        program = new CPQQuotesResource.CreateOrUpdateQuote_Program();
        program.ProgramName = 'Test2';
        programList.add(program);
        programs.Program = programList;
        
        quoteItem.MaterialNumber = '99-14732';
        quoteItem.MaterialDescription = 'Catalyst Dx Analyzer';
        quoteItem.Quantity = 1;
        quoteItem.UnitAmount = 19995.0;
        quoteItem.TotalAmount = 22001.77;
        
        List<CPQQuotesResource.QuoteItem> quoteItemList = new List<CPQQuotesResource.QuoteItem>();
        quoteItemList.add(quoteItem);
        quoteItems.QuoteItem = quoteItemList;
        
        quote.OpportunityId = testOpportunity.Id;
        quote.TransactionId = '448654144';
        quote.QuoteNumber = '2016-61967-9';
        quote.QuoteType = 'Proposal';
        quote.QuoteDescription = '';
        quote.QuoteStatus = 'Approved';
        quote.QuoteAmount = 22720.29;
        quote.RegionalMgrAmount= 100.0;
        quote.CurrencyCode = 'CAD';
        quote.QuoteCreatedDate = Datetime.valueOf('2016-04-15 14:11:00');
        quote.QuoteCreatedBy = '';
        quote.QuoteLastModifiedDate = Datetime.valueOf('2016-04-15 14:11:00');
        quote.QuoteLastModifiedBy = '';
        quote.QuoteSubmittedDate = Datetime.valueOf('2016-04-15 14:11:00');
        quote.QuoteExpirationDate = Datetime.valueOf('2016-04-15 14:11:00');
        quote.QuoteApprovalDate = Datetime.valueOf('2016-04-15 14:11:00');
        
        quote.QuoteItems = quoteItems;
        quote.Programs = programs;
                
        
        createOrUpdateQuote.quote = quote;
            
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/cpqquotes';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        CPQQuotesResource.createOrUpdateQuoteResponse response = CPQQuotesResource.dopost(createOrUpdateQuote);
        
        response = CPQQuotesResource.dopost(createOrUpdateQuote);
        
        System.debug('response'+response);
    }    
    static testMethod void testFailedMessage() {
        
        IDEXX_Integration_Service_Properties__c customsettings = new IDEXX_Integration_Service_Properties__c();
        customsettings.Name = 'CPQ-DEFAULT_PRICE_BOOK';
		customsettings.TextVal__c = 'IDEXX Standard Price Book';
        insert customsettings;
        
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'Test Opportunity';
        testOpportunity.Start_Date__c = System.today();
        testOpportunity.CloseDate = System.today().addMonths(10);
        testOpportunity.stageName = '2 Discovery';
        insert testOpportunity;
        
        Product2 product = new Product2();
        product.Name = 'Test';
        product.ProductCode = '99-14732';
        product.SAP_MATERIAL_NUMBER__c = '99-14732';
        product.IsActive = true;
        insert product;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        Pricebook2 priceBook = new Pricebook2(Name = 'IDEXX Standard Price Book', isActive=true);
        insert priceBook;
        
        PricebookEntry pBkEntry = new PricebookEntry();
        pBkEntry.Pricebook2Id = pricebookId;
        pBkEntry.Product2Id = product.Id;
        pBkEntry.CurrencyIsoCode = 'USD';
        pBkEntry.UnitPrice = 10;
        pBkEntry.PriceBook_SAPMatNr__c = 'Standard Price Book99-14732USD';
        pBkEntry.IsActive = true;
        insert pBkEntry;        
        
        pBkEntry = new PricebookEntry();
        pBkEntry.Pricebook2Id = priceBook.Id;
        pBkEntry.Product2Id = product.Id;
        pBkEntry.CurrencyIsoCode = 'USD';
        pBkEntry.UnitPrice = 1000;
        pBkEntry.PriceBook_SAPMatNr__c = 'IDEXX Standard Price Book99-14732USD';
        pBkEntry.IsActive = true;
        insert pBkEntry;
        
        List <PricebookEntry> dbPricebookEntries1 = [SELECT bookEntry.Id, bookEntry.productcode, bookEntry.Pricebook2Id, bookEntry.Product2Id FROM PricebookEntry bookEntry where bookEntry.Pricebook2Id in (select Id FROM Pricebook2 where Name = 'IDEXX Standard Price Book' and isActive = true) and bookEntry.Product2Id in (select Id FROM Product2) and bookEntry.CurrencyIsoCode = 'USD' and bookEntry.productcode in ('99-14732')];    
        
        CPQQuotesResource.CreateOrUpdateQuote createOrUpdateQuote = new CPQQuotesResource.CreateOrUpdateQuote();
        CPQQuotesResource.CreateOrUpdateQuote_Quote quote = new CPQQuotesResource.CreateOrUpdateQuote_Quote();
        CPQQuotesResource.QuoteItems_element quoteItems = new CPQQuotesResource.QuoteItems_element();
        CPQQuotesResource.QuoteItem quoteItem = new CPQQuotesResource.QuoteItem();
        CPQQuotesResource.Programs_element programs = new CPQQuotesResource.Programs_element();
        
        
        Test.startTest();
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/cpqquotes';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        CPQQuotesResource.createOrUpdateQuoteResponse response = CPQQuotesResource.dopost(createOrUpdateQuote);
        
        response = CPQQuotesResource.dopost(createOrUpdateQuote);
        
        System.debug('response'+response);
    }
}