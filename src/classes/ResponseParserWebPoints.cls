/* Class Name   : ResponseParserWebPoints
 * Description  : Wrapper class used by webPointsIntegration class
 * Created By   : Srinivas Kaduluri
 * Created On   : 03-17-2017
 
 * Modification Log:  
 * --------------------------------------------------------------------------------------------------------------------------------------
 * Developer                        Date                     Modification ID              Description 
 * ---------------------------------------------------------------------------------------------------------------------------------------
 * Srinivas Kaduluri                03-17-2017                1000                     Initial version
*/
public class ResponseParserWebPoints {

    public String access_token;
    public string refresh_token;
    public string scope;
    public string id_token;
    public string token_type;
    public string expires_in;

       
    public static ResponseParserWebPoints parse(String jsonRes) {   
        return (ResponseParserWebPoints ) System.JSON.deserialize(jsonRes, ResponseParserWebPoints .class);
    }
}