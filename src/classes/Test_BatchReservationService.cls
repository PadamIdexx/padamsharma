/**************************************************************************************
Apex Class Name  : Test_BatchReservationService

Description - Test class for BatchReservationService callout

                   
* Developer         Date                   Description
* ----------------------------------------------------------------------------                 
* Aditya Sangawar     March 08 2016           Original Version
*************************************************************************************/

@isTest
public class Test_BatchReservationService {

    static testMethod void Test_BatchReservationService(){
        
        Account acc1 = CreateTestClassData.createCustomerAccount();
        BatchReservationService batchserviext = new BatchReservationService();
        String sampleResponse = '{"d":{"results":[{"__metadata": {"type":"idexxui.services.sfdc.sfdcservices.BATCH_RESERVATIONSType","uri":"____https://devreporting.idexx.com/idexxui/services/sfdc/sfdcservices.xsodata/BATCH_RESERVATIONS('+'\''+'242214407345813091'+'\''+')"},"BATCHRES":"242214407345813091","Sold_To":"0000001047","Ship_To":null,"Reservation":"0040095128","Line_Item":"000010","Material":"98-20590-00","Batch":"0404H","Start_Date":"20151001","End_Date":"20160930","Status":"B","Sales_Org":"USS1","Dist_Channel":"00","Division":"CP","Plant":"USP1","Reserved_Qty":"10","Consumed_QTY":"2"}]}}';
        
		Test_ReusableMockResponse fakeResponse = new Test_ReusableMockResponse(303,'SUCCESS',sampleResponse, new map<String,String>{'Content-Type'=>'application/json'});
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        
        batchserviext.getBatchReservationForCustomer('test','test','test');
        BatchReservationService.BatchReservation abc = new BatchReservationService.BatchReservation();
        BatchReservationService.BatchReservation abc2 = new BatchReservationService.BatchReservation('','','','','','','','','','','','','',0,0);
        
        // abc.BatchReservation();
        Test.stopTest();
        
    }
    
    static testMethod void testGetBatchReservationForCustomer() {
    	User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
            system.runAs(currentUser){
                Test.startTest();
                String sampleResponse = '{"d":{"results":[{"__metadata": {"type":"idexxui.services.sfdc.sfdcservices.BATCH_RESERVATIONSType","uri":"____https://devreporting.idexx.com/idexxui/services/sfdc/sfdcservices.xsodata/BATCH_RESERVATIONS('+'\''+'242214407345813091'+'\''+')"},"BATCHRES":"242214407345813091","Sold_To":"0000001047","Ship_To":null,"Reservation":"0040095128","Line_Item":"000010","Material":"98-20590-00","Batch":"0404H","Start_Date":"20151001","End_Date":"20160930","Status":"B","Sales_Org":"USS1","Dist_Channel":"00","Division":"CP","Plant":"USP1","Reserved_Qty":"10","Consumed_QTY":"2"}]}}';
        		HttpResponse httpResponse = new HttpResponse();
                httpResponse.setBody(sampleResponse);
                BatchReservationService batchReservationService = new BatchReservationService();
                batchReservationService.setResponse(httpResponse);
                batchReservationService.getBatchReservationForCustomer('9999999900'); //0000060483
                
                batchReservationService.parseBatchReservation(sampleResponse);
                
                Test.stopTest();
                
            }
        
    }

}