/**
 *    @author Aditya Sangawar
 *    @date   22/12/2015
 *    @description   This is VF Controller that fetches all the Customer Sales History 'REVENUE' Data and groups them as per Product Hierarchy.
 Modification Log:
 ------------------------------------------------------------------------------------
 Developer                       Date                Description
 ------------------------------------------------------------------------------------
 Aditya Sangawar                 22/12/2015          Created
 Aditya Sangawar                 03/02/2016          Modified - Optimised the no. of SOQL Queries
 Aditya Sangawar                 10/02/2016          TKT 506 - Added SEROLOGY/IMMUNOLOGY as an Enhancement
 Heather Kinney                  06/01/2016          DE8392  - Fix for column month name repeat issue when month has 31 days.
 Heather Kinney                  05/31/2016          US23018 - Merging R2.1 changes into R3.0 - Ref Lab and Practice Supplies
**/
public class InlineCAGSalesHistoryRevenueController extends AbstractSalesHistoryController {

    public static final String MMM_FORMAT = 'MMM';
    public static final Integer FIRST_DAY_OF_MONTH = 1; //DE8392

    //constuctor
    public InlineCAGSalesHistoryRevenueController(ApexPages.StandardController controller) {
        super(controller);
    }

    protected override void setHeaderValues() {
        super.setHeaderValues();

        Date dToday = date.Today();
        // all this year headers
        DateTime dt = DateTime.newInstance(dToday.year(), dToday.month(),dToday.day());
        //DE8392  - Fix for column month name repeat issue when month has 31 days.
        // For the setting of the "MONTH_NN_AGO" vars, FIRST_DAY_OF_MONTH is now being used
        // as opposed to dToday.day(), which for those current months with 31 days, resulted
        // in a flip to that same month: May 31 (cur month); April 31 (prev month) which
        // flipped to that current month (May in this example).
        dt = DateTime.newInstance(dToday.year(), dToday.month() - 12,FIRST_DAY_OF_MONTH);
        uiData.MONTH_12_AGO = dt.format(MMM_FORMAT);
        dt = DateTime.newInstance(dToday.year(), dToday.month() - 13,FIRST_DAY_OF_MONTH);
        uiData.MONTH_13_AGO = dt.format(MMM_FORMAT);
        dt = DateTime.newInstance(dToday.year(), dToday.month() - 14,FIRST_DAY_OF_MONTH);
        uiData.MONTH_14_AGO = dt.format(MMM_FORMAT);
        dt = DateTime.newInstance(dToday.year(), dToday.month() - 15,FIRST_DAY_OF_MONTH);
        uiData.MONTH_15_AGO = dt.format(MMM_FORMAT);
        dt = DateTime.newInstance(dToday.year(), dToday.month() - 16,FIRST_DAY_OF_MONTH);
        uiData.MONTH_16_AGO = dt.format(MMM_FORMAT);
        dt = DateTime.newInstance(dToday.year(), dToday.month() - 17,FIRST_DAY_OF_MONTH);
        uiData.MONTH_17_AGO = dt.format(MMM_FORMAT);
        dt = DateTime.newInstance(dToday.year(), dToday.month() - 18,FIRST_DAY_OF_MONTH);
        uiData.MONTH_18_AGO = dt.format(MMM_FORMAT);
        dt = DateTime.newInstance(dToday.year(), dToday.month() - 19,FIRST_DAY_OF_MONTH);
        uiData.MONTH_19_AGO = dt.format(MMM_FORMAT);
        dt = DateTime.newInstance(dToday.year(), dToday.month() - 20,FIRST_DAY_OF_MONTH);
        uiData.MONTH_20_AGO = dt.format(MMM_FORMAT);
        dt = DateTime.newInstance(dToday.year(), dToday.month() - 21,FIRST_DAY_OF_MONTH);
        uiData.MONTH_21_AGO = dt.format(MMM_FORMAT);
        dt = DateTime.newInstance(dToday.year(), dToday.month() - 22,FIRST_DAY_OF_MONTH);
        uiData.MONTH_22_AGO = dt.format(MMM_FORMAT);
        dt = DateTime.newInstance(dToday.year(), dToday.month() - 23,FIRST_DAY_OF_MONTH);
        uiData.MONTH_23_AGO = dt.format(MMM_FORMAT);

    }

    public override void setupQueries(String sapId) {

        // Assemble basic select query to pull in sales history based upon
        // specific criteria.
        String salesHistorySelect = 'SELECT ';
        salesHistorySelect += String.join(SH_COMMON_FIELDS, ', ') + ', ';
        salesHistorySelect += String.join(SH_REVENUE_FIELDS, ', ');
        salesHistorySelect += ' FROM Customer_Sales_History__c WHERE SAP_CUSTOMER_NUMBER__c = \''
            + sapId + '\'';

        // Kits : fetch all sales history records for Kits
        cagKitsQuery = salesHistorySelect + CAG_KITS_WHERE_CLAUSE;
        cagConsumablesQuery = salesHistorySelect + CONSUMABLES_WHERE_CLAUSE;
        cagLabServicesQuery = salesHistorySelect + LAB_SERVICES_WHERE_CLAUSE;
        cagTelemedQuery = salesHistorySelect + TELEMED_WHERE_CLAUSE;
        cagTRGQuery = salesHistorySelect + TRG_WHERE_CLAUSE;
        cagSerologyImmunologyQuery = salesHistorySelect + SER_IMM_WHERE_CLAUSE;
        chemistryQuery = salesHistorySelect + CHEM_WHERE_CLAUSE;
        ParasitologyQuery = salesHistorySelect + PARA_WHERE_CLAUSE;
        cagReferenceLabSuppliesQuery = salesHistorySelect + REFERENCE_LAB_SUPPLIES_WHERE_CLAUSE;
        cagPracticeSuppliesQuery = salesHistorySelect + PRACTICE_SUPPLIES_WHERE_CLAUSE;

        totalsQuery = salesHistorySelect + TOTALS_WHERE_CLAUSE;

    }

}