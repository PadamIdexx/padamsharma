/*
 * Wrapper class for User Profiles.
 * 
 * This is intended for use in VF pages when 
 * it is necessary to enable/disable things
 * based on profile.
 */ 
public class ProfileDisplayWrapper {
    public ProfileDisplayWrapper() {
        UserHelper up = new UserHelper();
        
        isCAG = up.isCAG();
        isLPD = up.isLPD();
        isWater = up.isWater();
        isNA = up.isNA();
        isEMEA = up.isEMEA();
    }
    
    public Boolean isCAG {get;set;}
    public Boolean isLPD {get;set;}
    public Boolean isWater {get;set;}
    public Boolean isEMEA {get;set;}
    public Boolean isNA {get;set;}
}