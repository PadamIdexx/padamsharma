global with sharing class CustomerPricingControllerExtension {
    public List<ProductsWithPriceHolder> results {get;set;}
    public String searchString {get;set;}
    public String selectedSalesAreaDetails {get; set;}
    public List<SelectOption> SalesAreaDetailsSelectOptions {get;set;}
    private Account account {get;set;}
    public String sapId {get;set;}
    private Product2 product2 {get;set;}
    private List<Account_Salesview__c> accountSalesViewList {get;set;}
    public Map<String, SalesAreaDetails> salesAreaDetailsMap {get;set;}
    private String sAPCustomerNumber;
    private Map<String, String> selectOptionsMap;
    public Boolean useSynchronousInvoke = true;
    public String priceItems{get;set;}
    public String res2 {get;set;}
    public String errorMsg {get;set;}

    public TestCodeSalesProcessingSalesforce.SalesOrderSimulateResponse_Sync syncResponse;
    public String selectedCountry{get;set;}

    public CustomerPricingControllerExtension(ApexPages.StandardController controller) {
        this.account = (Account) controller.getRecord();
        Product2 = new Product2();
        String validSAkey;
        priceItems = '';
        errorMsg = '';
        //List<SalesAreaDetails> salesAreaDetailsList = new List<SalesAreaDetails>();
        salesAreaDetailsMap = new Map <String, SalesAreaDetails>();
        List<String> partnerFilter = new List<String>();
        partnerFilter.add('ZC');

        for (Account selectedAccount: [select SAP_Customer_Number__c, (select Sales_Organization__c, Distribution_Channel__c, Division__c, Partner_Function__c, Partner_Function_Desc__c, Partner_Account__r.SAP_Customer_Number__c from Partner_Functions__r pf where Partner_Function__c NOT IN :partnerFilter ), (select SalesView__r.Sales_Organization_Code__c, SalesView__r.Distribution_Channel_Code__c, SalesView__r.Sales_Division__c, Incoterms1__c from AccountSalesviews__r where Marked_for_Deletion_Flag__c = FALSE AND IDEXX_Region_Code__c != NULL) from Account where Id =: this.account.Id]) {

     //   List<Account> lstAccount = CustomerPricingControllerWithoutSharing.getAccount(this.account,partnerFilter);
              
     //   for (Account selectedAccount: lstAccount ) {

            this.sapId = Utils.removeLeadingZeros(selectedAccount.SAP_Customer_Number__c);

            sAPCustomerNumber = selectedAccount.SAP_Customer_Number__c;
            List<Partner_Function__c> partnerFunctionList = selectedAccount.Partner_Functions__r;
            accountSalesViewList = selectedAccount.AccountSalesviews__r;

            //Map<String, List<String>> validPartnersMap = new Map<String, List<String>>();

            if (null != partnerFunctionList && partnerFunctionList.size() > 0) 
            {
                for (Partner_Function__c pf: partnerFunctionList) 
                {
                    validSAkey = pf.Sales_Organization__c + pf.Distribution_Channel__c + pf.Division__c;
                    if (salesAreaDetailsMap.containsKey(validSAkey)) 
                    {
                        salesAreaDetailsMap.get(validSAkey).partnerFunctions.add(pf.Partner_Function__c + '/' + pf.Partner_Account__r.SAP_Customer_Number__c);
                    } 
                    else 
                    {
                        SalesAreaDetails salesAreaDetails = new SalesAreaDetails();
                        List<String> partnerFunctionDescriptions = new List<String>();
                        partnerFunctionDescriptions.add(pf.Partner_Function__c + '/' + pf.Partner_Account__r.SAP_Customer_Number__c);
                        salesAreaDetails.partnerFunctions = partnerFunctionDescriptions;
                        salesAreaDetailsMap.put(validSAkey, salesAreaDetails);
                    }
                }

                if (null != accountSalesViewList && accountSalesViewList.size() > 0) 
                {
                    for (Account_Salesview__c accountSalesview: accountSalesViewList) 
                    {
                        validSAkey = accountSalesview.SalesView__r.Sales_Organization_Code__c + accountSalesview.SalesView__r.Distribution_Channel_Code__c + accountSalesview.SalesView__r.Sales_Division__c;
                        if (salesAreaDetailsMap.containsKey(validSAkey)) 
                        {
                            SalesAreaDetails salesAreaDetails = salesAreaDetailsMap.get(validSAkey);
                            salesAreaDetails.salesOrganizationCode = accountSalesview.SalesView__r.Sales_Organization_Code__c;
                            salesAreaDetails.distributionChannelCode = accountSalesview.SalesView__r.Distribution_Channel_Code__c;
                            salesAreaDetails.salesDivision = accountSalesview.SalesView__r.Sales_Division__c;
                            if (null != accountSalesview.Incoterms1__c) 
                            {
                                salesAreaDetails.incoterms1 = accountSalesview.Incoterms1__c;
                            } else 
                            {
                                salesAreaDetails.incoterms1 = '250';
                            }
                            salesAreaDetailsMap.put(validSAkey, salesAreaDetails);
                        }
                    }
                }
            }
        }
        SalesAreaDetailsSelectOptions = getSalesAreaDetailsSelectOptions();
        searchString = System.currentPageReference().getParameters().get('lksrch');
    }

    // performs the keyword search
    public PageReference productSearch() {
        if (ApexPages.hasMessages()) {
            ApexPages.getMessages().clear();
        }

        if (String.isNotBlank(searchString) && searchString.length() > 1) {

            results = performProductSearch(searchString);
        }

        return null;
    }
    
    

    public void getPrice() 
    {
        System.debug('--getPrice--------------->'+priceItems);
        System.debug('--res2--------------->'+res2);
        res2= null;
        
        if (ApexPages.hasMessages()) 
        {
            ApexPages.getMessages().clear();
        }
        List<String>prices = new List<String>();
        results = new List<ProductsWithPriceHolder>();
        if (priceItems.length() > 0) 
        {
            prices.addAll(priceItems.split(','));
            System.debug('prices ' + prices[0]);
            try 
            {
                List<Product2> prods = new List<Product2>([SELECT Id, SAP_MATERIAL_NUMBER__c FROM Product2 WHERE Id IN: prices]);
                
                System.debug(selectedCountry+'--selectedSalesAreaDetails------in getPrice------>'+selectedSalesAreaDetails);
                if(selectedCountry != null && selectedCountry !='')
                {
                    if( selectedCountry == 'Antrim' )
                    {
                        selectedSalesAreaDetails = 'US-Ref Labs';
                    }
                    else
                    {
                        selectedSalesAreaDetails = 'CA-Ref Labs';
                    }
                }
                System.debug('--selectedSalesAreaDetails------in getPrice------>'+selectedSalesAreaDetails);

                
                for(Product2 p : prods) 
                {
                    ProductsWithPriceHolder pwp = new ProductsWithPriceHolder();
                    pwp.product2 = p;
                    pwp.isSelected = true;
                    pwp.material = p.SAP_MATERIAL_NUMBER__c;
                    System.debug('products ********************************************************************' + pwp);
                    results.add(pwp);
                    if(selectedSalesAreaDetails.equals('CA-Ref Labs')) {
                        pwp = new ProductsWithPriceHolder();
                        pwp.product2 = p;
                        pwp.isSelected = true;
                        pwp.material = p.SAP_MATERIAL_NUMBER__c;
                        System.debug('products ********************************************************************' + pwp);
                        results.add(pwp);
                    }
                    
                    System.debug('results.size() ********************************************************************' + results.size());
                    
                }
                showPrice();

            } 
            catch(exception ex) 
            {
                System.debug('----130----------------------->'+ex);
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()+ ' ' +ex.getLineNumber());
                ApexPages.addMessage(myMsg);
            }
        } 
        else 
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,  'Please Choose At Least 1 Item');
            ApexPages.addMessage(myMsg);
            errorMsg = 'Please Choose At Least 1 Item';
            system.debug('------errorMsg ---------------------->'+errorMsg );
            //return null;
        }
            //return null;
        
    }

    private List<ProductsWithPriceHolder> performProductSearch(string searchString) 
    {
        List<ProductsWithPriceHolder> productsWithPriceHolderList = new List<ProductsWithPriceHolder>();
        List<List<sObject>> searchObjects;
        
        searchObjects = [FIND: searchString + '*'
                         IN ALL FIELDS RETURNING Product2(SAP_MATERIAL_NUMBER__c,id, CMS_Product_Units__c, CMS_Product_Units_Measure__c, Quantity_Per_Package__c, CMS_Product_Description_Long__c, ProductCode, Name WHERE isActive = true order by ProductCode DESC limit 25)
                        ];
                        
        List<Product2> product2List = ((List<Product2>) searchObjects[0]);

        // DE8799---Code added by Rama
        Set<Id> setProductID = new Set<ID>();
        if (null != product2List && product2List.size() > 0) 
        {
            for (Product2 myProduct2: product2List) 
            {
                setProductID.add(myProduct2.id);
            }
        }
        
        System.debug('--DE8799---selectedSalesAreaDetails------>'+selectedSalesAreaDetails);
        String countyCode;
        String recCode;

        if(selectedSalesAreaDetails != null && selectedSalesAreaDetails !='')
        {
            List<String> lstString = selectedSalesAreaDetails.split('-'); 
            countyCode = RevconvertDivision(lstString[0]);  
            recCode = RevconvertDivision(lstString[1]);  
        }    
        System.debug('--Rama---RevconvertDivision------>'+recCode );
        System.debug('-----salesAreaDetailsMap----->'+salesAreaDetailsMap );
        
        List<SalesAreaDetails> mysalesareas = salesAreaDetailsMap.values();
        String mySalesOrg = null;
        String myDivision = null;
        for(SalesAreaDetails mysalesarea : mysalesareas) 
        {
            if(null != mysalesarea &&  mysalesarea.salesDivision != null && mysalesarea.salesDivision.equals(recCode)) 
            {
                myDivision = mysalesarea.salesDivision;
                if( mysalesarea.salesOrganizationCode != null &&  mysalesarea.salesOrganizationCode.startsWith(countyCode) )
                {
                    mySalesOrg = mysalesarea.salesOrganizationCode;
                }
                System.debug('************ mysalesarea  ***********'+ mysalesarea.salesDivision );
                System.debug('************ mysalesarea  ***********'+ mysalesarea.salesOrganizationCode );
            }
        }
        
       System.debug('------myDivision-------------->'+myDivision);
       System.debug('------mySalesOrg-------------->'+mySalesOrg);
     
       List<Product_Salesview__c> lstProductSalesView = [select id,name,Product_Division__c ,Product__c,Product_code__c from Product_Salesview__c where Product__c in :setProductID AND Sales_Organization__c = :mySalesOrg  AND Sales_Division__c =:myDivision ];
        
       //List<Product_Salesview__c> lstProductSalesView = [select id,name,Product_Division__c ,Product__c,Product_code__c from Product_Salesview__c where Sales_Division__c= :recCode and Product__c in :setProductID AND Sales_Organization__c = :mySalesOrg  AND Sales_Division__c =:myDivision ];
        
        Map<String,Product_Salesview__c> mapProductCodeWiseSalesView = new Map<String,Product_Salesview__c>();
        
        for(Product_Salesview__c psv : lstProductSalesView)
        {
            mapProductCodeWiseSalesView.put(psv.Product__c , psv);
        }   
        System.debug('-----mapProductCodeWiseSalesView------>'+mapProductCodeWiseSalesView);    
        
        //DE8799- Change END.
        
        if (null != product2List && product2List.size() > 0) 
        {
            for (Product2 myProduct2: product2List) 
            {
                if(mapProductCodeWiseSalesView.containsKey( myProduct2.id ))
                {
                    ProductsWithPriceHolder productsWithPriceHolder = new ProductsWithPriceHolder();
                    productsWithPriceHolder.product2 = myProduct2;
                    productsWithPriceHolder.material = myProduct2.SAP_MATERIAL_NUMBER__c;
                    productsWithPriceHolder.inQuantity = 1;
                    if (product2List.size() == 1) 
                    {
                        productsWithPriceHolder.isSelected = true;
                    }
                    productsWithPriceHolderList.add(productsWithPriceHolder);
                }   
            }
        }
        return productsWithPriceHolderList;
    }

    /*
     * 
     * 
     */
    
    public Object showPrice() 
    {
        errorMsg = '';
        if (ApexPages.hasMessages()) {
            ApexPages.getMessages().clear();
        }
        try {

            if (null != selectedSalesAreaDetails) 
            {
                System.debug('-------------->'+selectedSalesAreaDetails);
                System.debug('----------selectOptionsMap---->'+selectOptionsMap);
                String DocumentType = 'TA';
                List<String> tempSelectedSalesAreaDetailsList = selectOptionsMap.get(selectedSalesAreaDetails).split('/');
                String SalesOrganization = tempSelectedSalesAreaDetailsList.get(0);
                String DistributionChannel = tempSelectedSalesAreaDetailsList.get(1);
                String Division = tempSelectedSalesAreaDetailsList.get(2);
                String PurchaseOrderType = 'CRM';
                String SalesPerson = UserInfo.getUserName().substring(0, 12);
                String Incoterms = salesAreaDetailsMap.get(SalesOrganization + DistributionChannel + Division).incoterms1;
                String PaymentTerms = null;
                String DeliveryBlock = null;
                String BillingBlock = null;
                String OrderReason = null;
                String CustomerGroup3 = null;
                String CustomerGroup4 = null;
                String CustomerGroup5 = null;
                String PurchaseOrderNumber = sAPCustomerNumber;
                TestCodeSalesProcessingSalesforce.OrderConditionsType OrderConditions = null;
                String ReferenceDocument = null;
                String ReferenceDocumentCategory = null;
                String CouponCode = null;
                String Customer = sAPCustomerNumber;
                String Language = 'EN';
                System.debug('------271--------->');
                TestCodeSalesProcessingSalesforce.Items_element[] Items = new List<TestCodeSalesProcessingSalesforce.Items_element>();
                Integer counter = 1;
                for (ProductsWithPriceHolder productsWithPriceHolder: results) 
                {
                    System.debug('--------productsWithPriceHolder.isSelected ---------->'+productsWithPriceHolder.isSelected );
                    System.debug('--------productsWithPriceHolder.product2.SAP_MATERIAL_NUMBER__c---------->'+productsWithPriceHolder.product2.SAP_MATERIAL_NUMBER__c);

                    if (productsWithPriceHolder.isSelected == true) 
                    {
                        TestCodeSalesProcessingSalesforce.Items_element item = new TestCodeSalesProcessingSalesforce.Items_element();
                        item.ExternalItemNumber = String.valueOf(counter);
                        item.Material = productsWithPriceHolder.product2.SAP_MATERIAL_NUMBER__c;
                        if (null == productsWithPriceHolder.inQuantity) {
                            item.RequestedQuantity = String.valueOf('1.000');
                        } else {
                            item.RequestedQuantity = String.valueOf(Decimal.valueOf(productsWithPriceHolder.inQuantity).setScale(3));
                        }

                        item.RequestedDeliveryDate = Datetime.now().format('yyyy-MM-dd');
                        Items.add(item);
                    }
                    counter++;
                }

                if (Items.size() <= 0) 
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select one or more items');
                    ApexPages.addMessage(myMsg);
                    System.debug('-----------------------errorMsg ------------>'+errorMsg );
                    errorMsg = 'Please Select an Item';
                    return null;
                }
                System.debug('-----------------------After Error------------>');
                TestCodeSalesProcessingSalesforce.Partners_element[] Partners = new List<TestCodeSalesProcessingSalesforce.Partners_element>();
                List<String> partnerFunctionsList = salesAreaDetailsMap.get(SalesOrganization + DistributionChannel + Division).partnerFunctions;

                for (String partnerFunction: partnerFunctionsList) 
                {
                    TestCodeSalesProcessingSalesforce.Partners_element partner = new TestCodeSalesProcessingSalesforce.Partners_element();
                    List<String> tempPfList = partnerFunction.split('/');
                    partner.PartnerType = tempPfList.get(0);
                    if (null != tempPfList.get(1)) {
                        partner.Customer = tempPfList.get(1);
                    } else {
                        partner.Customer = sAPCustomerNumber;
                    }

                    Partners.add(partner);
                }

                System.debug('------Rama--inside if--------->');
                System.debug('------Rama--inside if-Items-------->'+Items);
                System.debug('------Rama--inside if-SalesOrganization-------->'+SalesOrganization);
                System.debug('------Rama--inside if-DistributionChannel-------->'+DistributionChannel);
                System.debug('------Rama--inside if-Division-------->'+Division);
                
                TestCodeSalesProcessingSalesforce.HTTPS_Port stub = new TestCodeSalesProcessingSalesforce.HTTPS_Port();
                stub.timeout_x = 120000;
                syncResponse = stub.SalesOrderSimulate(DocumentType, SalesOrganization, DistributionChannel, Division, PurchaseOrderType, SalesPerson, Incoterms, PaymentTerms, DeliveryBlock, BillingBlock, OrderReason, CustomerGroup3, CustomerGroup4, CustomerGroup5, PurchaseOrderNumber, OrderConditions, ReferenceDocument, ReferenceDocumentCategory, CouponCode, Customer, Language, Items, Partners);
                ProcessResponse();

                return null;
            }

        } 
        catch (Exception ex) 
        {
            System.debug('----323--------------Exception-->'+ex);
            errorMsg = ex.getMessage();
            if(errorMsg.contains('IO Exception'))
            {
                //errorMsg = 'Please try After Some time ,'+errorMsg;
                errorMsg = 'Time out Request- Please try again.';
                ApexPages.addMessages(ex);
            }
            else
            {
                ApexPages.addMessages(ex);
            }
        }
        return null;
    }

    public Object ProcessResponse() 
    {
        System.debug('----Rama--DE8799------------syncResponse-->'+syncResponse );
        errorMsg = '';
        try {
            if (null != syncResponse) {
                List<TestCodeSalesProcessingSalesforce.ResponseItems_element> responseItems;
                if (null != syncResponse) {
                    responseItems = syncResponse.ResponseItems;
                }

                if (null != responseItems && responseItems.size() > 0) 
                {
                    for (TestCodeSalesProcessingSalesforce.ResponseItems_element responseItem: responseItems) 
                    {
                        for (Integer i = 0; i<results.size(); i++) 
                        {
                            system.debug('results in loop ---->' + results[i].material);
                            system.debug('responseItem.Materialin loop ---->' + responseItem.Material);
                            if (results[i].material == responseItem.Material) 
                            {
                                if (String.isNotBlank(responseItem.Currency_x)) {
                                    results[i].outCurrency = responseItem.Currency_x;
                                }
                                if (String.isNotBlank(responseItem.Subtotal2)) {
                                    results[i].price = String.valueOf(Decimal.valueOf(responseItem.Subtotal2).setScale(2, RoundingMode.UP));
                                } else {
                                    results[i].price = 'N/A';
                                }
                                if (String.isNotBlank(responseItem.Subtotal1)) {
                                    results[i].listPrice = String.valueOf(Decimal.valueOf(responseItem.Subtotal1).setScale(2, RoundingMode.UP));
                                    results[i].discount = Decimal.valueOf(results[i].price) - Decimal.valueOf(results[i].listPrice);
                                } else {
                                    results[i].price = 'N/A';
                                }
                            }
                            res2 = JSON.serializePretty(results);
                            SYSTEM.debug('results ' + responseItem.material + ' '+ results[0].listPrice);
                        }
                    }
                } else {
                    String consolidatedMsg = '';
                    List<TestCodeSalesProcessingSalesforce.Return_element> returnElementList;
                    if (null != syncResponse) {
                        returnElementList = syncResponse.Return_x;
                    } 
                    
                    if (null != returnElementList && returnElementList.size() > 0) {

                        for (TestCodeSalesProcessingSalesforce.Return_element returnElement: returnElementList) {
                            consolidatedMsg = consolidatedMsg + returnElement.Message + '\n';
                        }
                    }
                    if (String.isNotBlank(consolidatedMsg)) {
                        System.debug('--------------> ');
                        productSearch();
                        String myCustomMsg ='';
                        if(consolidatedMsg != null && consolidatedMsg.startsWith('No customer master record exists for customer null'))
                        {
                            myCustomMsg='Do not have Parent Account access';
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, myCustomMsg);
                            ApexPages.addMessage(myMsg);
                            errorMsg = myCustomMsg;
                        }
                        else
                        {
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, consolidatedMsg);
                            ApexPages.addMessage(myMsg);
                            errorMsg = consolidatedMsg;
                        }
                    } else {
                        productSearch();
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid item. Pricing not available.');
                        ApexPages.addMessage(myMsg);
                        errorMsg = 'Invalid item. Pricing not available.';
                    }

                    return null;
                }
            } else {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please narrow down search and try again');
                ApexPages.addMessage(myMsg);
                errorMsg =  'Please narrow down search and try again';
                return null;
            }
        } catch (Exception ex) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please check your selected line of business.');
            ApexPages.addMessage(myMsg);
            errorMsg =  'Please check your selected line of business.';
        }
        return null;
    }

    global class ProductsWithPriceHolder {
        public Product2 product2 {get;set;}
        public String material {get;set;}
        public String listPrice {get;set;}
        public Decimal discount {get;set;}
        public String price {get;set;}
        public Boolean isSelected {get;set;}
        public Integer inQuantity {get;set;}
        public String outCurrency {get;set;}
    }

    public class SalesAreaDetails {
        public String salesOrganizationCode {get;set;}
        public String distributionChannelCode {get;set;}
        public String salesDivision {get;set;}
        public String incoterms1 {get;set;}
        public List<String> partnerFunctions {get;set;}
    }

    public List<SelectOption> getSalesAreaDetailsSelectOptions () {
        selectOptionsMap = new Map<String,String>();
        List<SelectOption> options = new List<SelectOption>();
        if (null != salesAreaDetailsMap && salesAreaDetailsMap.size() > 0) {
            for (String key : salesAreaDetailsMap.keySet()) 
            {
                System.debug('----------------->'+key);
                SalesAreaDetails salesAreaDetails = salesAreaDetailsMap.get(key);
                System.debug('----------------->'+salesAreaDetails);
              
                if(salesAreaDetails.salesOrganizationCode != null && salesAreaDetails.salesDivision != null )
                {
                    String newKey = salesAreaDetails.salesOrganizationCode.substring(0, 2) +'-'+ convertDivision(salesAreaDetails.salesDivision);
                    selectOptionsMap.put(newKey , salesAreaDetails.salesOrganizationCode +'/'+ salesAreaDetails.distributionChannelCode +'/'+ salesAreaDetails.salesDivision);
                    if (newKey.contains('US') && newKey.contains('IHD')) {
                        selectedSalesAreaDetails = newKey;
                    }
                    if (null == selectedSalesAreaDetails && newKey.contains('IHD')) {
                        selectedSalesAreaDetails = newKey;
                    }
                    options.add(new SelectOption(newKey,newKey));
                }   
            }
        }
        return options;
    }

    private String convertDivision(String div) {
        if (null != div) {
            if ('CP'.equalsIgnoreCase(div)) {
                div = 'IHD';
            } else if ('VS'.equalsIgnoreCase(div)) {
                div = 'Ref Labs';
            } else if ('VB'.equalsIgnoreCase(div)) {
                div = 'IM';
            } else if ('VR'.equalsIgnoreCase(div)) {
                div = 'Digital';
            } else if ('CH'.equalsIgnoreCase(div)) {
                div = 'Pharmacy';
            } else if ('VC'.equalsIgnoreCase(div)) {
                div = 'CardioPet';
            } else if ('CL'.equalsIgnoreCase(div)) {
                div = 'Livestock';
            } else if ('BR'.equalsIgnoreCase(div)) {
                div = 'Pharma';
            } else if ('XX'.equalsIgnoreCase(div)) {
                div = 'Generic';
            }
        }
        return div;
    }

    private String RevconvertDivision(String div) {
        if (null != div) {
            if ('IHD'.equalsIgnoreCase(div)) {
                div = 'CP';
            } else if ('Ref Labs'.equalsIgnoreCase(div)) {
                div = 'VS';
            } else if ('IM'.equalsIgnoreCase(div)) {
                div = 'VB';
            } else if ('Digital'.equalsIgnoreCase(div)) {
                div = 'VR';
            } else if ('Pharmacy'.equalsIgnoreCase(div)) {
                div = 'CH';
            } else if ('CardioPet'.equalsIgnoreCase(div)) {
                div = 'VC';
            } else if ('Livestock'.equalsIgnoreCase(div)) {
                div = 'CL';
            } else if ('Pharma'.equalsIgnoreCase(div)) {
                div = 'BR';
            } else if ('Generic'.equalsIgnoreCase(div)) {
                div = 'XX';
            }
        }
        return div;
    }

}