/**
 *    @author Vishal Negandhi
 *    @date   18/12/2015
 *    @description   This is VF Controller that fetches all the Customer Sales History Quantity Data and groups them as per Product Hierarchy.
 Modification Log:
 ------------------------------------------------------------------------------------
 Developer                       Date                Description
 ------------------------------------------------------------------------------------
 Vishal Negandhi                  18/12/2015         Created
 Aditya Sangawar                  05/01/2015         Modified (Added Logic to make the page Asynchronous)
 Aditya Sangawar                  10/02/2016         TKT 506 - Added SEROLOGY/IMMUNOLOGY as an Enhancement
 Heather Kinney                   05/31/2016         US23018 - Merging R2.1 changes into R3.0 - Ref Lab and Practice Supplies
**/
public class InlineCAGSalesHistoryQuantityController extends AbstractSalesHistoryController {

    //constuctor
    public InlineCAGSalesHistoryQuantityController(ApexPages.StandardController controller) {
        super(controller);
    }

    public override void setupQueries(String sapId) {

        // Assemble basic select query to pull in sales history based upon
        // specific criteria.
        String salesHistorySelect = 'SELECT ';
        salesHistorySelect += String.join(SH_COMMON_FIELDS, ', ') + ', ';
        salesHistorySelect += String.join(SH_QUANTITY_FIELDS, ', '); 
        salesHistorySelect += ' FROM Customer_Sales_History__c WHERE SAP_CUSTOMER_NUMBER__c = \''
            + sapId + '\'';

        // Kits : fetch all sales history records for Kits
        cagKitsQuery = salesHistorySelect + CAG_KITS_WHERE_CLAUSE;
        cagConsumablesQuery = salesHistorySelect + CONSUMABLES_WHERE_CLAUSE;
        cagLabServicesQuery = salesHistorySelect + LAB_SERVICES_WHERE_CLAUSE;
        cagTelemedQuery = salesHistorySelect + TELEMED_WHERE_CLAUSE;
        cagTRGQuery = salesHistorySelect + TRG_WHERE_CLAUSE;
        cagSerologyImmunologyQuery = salesHistorySelect + SER_IMM_WHERE_CLAUSE;
        chemistryQuery = salesHistorySelect + CHEM_WHERE_CLAUSE;
        ParasitologyQuery = salesHistorySelect + PARA_WHERE_CLAUSE;
        cagReferenceLabSuppliesQuery = salesHistorySelect + REFERENCE_LAB_SUPPLIES_WHERE_CLAUSE;
        cagPracticeSuppliesQuery = salesHistorySelect + PRACTICE_SUPPLIES_WHERE_CLAUSE;
        totalsQuery = salesHistorySelect + TOTALS_WHERE_CLAUSE;    
        
    }
}