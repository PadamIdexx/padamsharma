@isTest
global class Test_WeeklyDomainWebserviceCalloutMock implements WebServiceMock {
    
   global void doInvoke(Object stub,Object request,Map<String, Object> response,String endpoint,String soapAction,String requestName,String responseNS,String responseName,String responseType) {
       
       WeeklyDomainValues.DT_WeeklyDomains_Response sampleResponse = new WeeklyDomainValues.DT_WeeklyDomains_Response();
       
       WeeklyDomainValues.SALES_INFO_element[] SALES_INFO = new List<WeeklyDomainValues.SALES_INFO_element>();
       
       WeeklyDomainValues.INCO_TERMS_element[] INCO_TERMS = new List<WeeklyDomainValues.INCO_TERMS_element>();
       WeeklyDomainValues.ORDER_REASONS_element[] ORDER_REASONS= new List<WeeklyDomainValues.ORDER_REASONS_element>();
       WeeklyDomainValues.BILLING_BLOCK_element[] BILLING_BLOCK= new List<WeeklyDomainValues.BILLING_BLOCK_element>();
       WeeklyDomainValues.DELIVERY_BLOCK_element[] DELIVERY_BLOCK = new List<WeeklyDomainValues.DELIVERY_BLOCK_element>();
       WeeklyDomainValues.DELIVERY_BLOCK_element delvBlock = new WeeklyDomainValues.DELIVERY_BLOCK_element();
       WeeklyDomainValues.PAYMENT_TERMS_element[] PAYMENT_TERMS = new List<WeeklyDomainValues.PAYMENT_TERMS_element>();
       WeeklyDomainValues.PAYMENT_TERMS_element paymentBlock = new WeeklyDomainValues.PAYMENT_TERMS_element();
       delvBlock.DELIVERY_BLOCK = 'Testing';
       delvBlock.DESCRIPTION = 'Testing desc';
       paymentBlock.PAYMENT_TERMS = '123';
       paymentBlock.DESCRIPTION = 'Testing desc';
       PAYMENT_TERMS.add(paymentBlock);
       /*
       String[] PAYMENT_TERMS = new List<String>();       
       String[] DELIVERY_BLOCK = new List<String>();
       PAYMENT_TERMS.add('test');
       PAYMENT_TERMS.add('test1');
       DELIVERY_BLOCK.add('test');
       DELIVERY_BLOCK.add('test1');
       */
       WeeklyDomainValues.INCO_TERMS_element incoTerm = new WeeklyDomainValues.INCO_TERMS_element();
       incoTerm.PART1 = 'TEST';
       incoTerm.PART1_DESCRIPTION= 'TEST DESC';
       INCO_TERMS.add(incoTerm);
       
       WeeklyDomainValues.INCO_TERMS_element incoTerm1 = new WeeklyDomainValues.INCO_TERMS_element();
       incoTerm1.PART1 = 'TEST1';
       incoTerm1.PART1_DESCRIPTION= 'TEST1 DESC';
       INCO_TERMS.add(incoTerm1);
       
       WeeklyDomainValues.ORDER_REASONS_element ordReason = new WeeklyDomainValues.ORDER_REASONS_element();
       ordReason.REASON='Test';
       ordReason.DESCRIPTION = 'Test Desc';
       ORDER_REASONS.add(ordReason);
       
       WeeklyDomainValues.ORDER_REASONS_element ordReason1 = new WeeklyDomainValues.ORDER_REASONS_element();
       ordReason1.REASON='Test1';
       ordReason1.DESCRIPTION = 'Test1 Desc';
       ORDER_REASONS.add(ordReason1);
       
       WeeklyDomainValues.BILLING_BLOCK_element billBlock = new  WeeklyDomainValues.BILLING_BLOCK_element();
       
       
       WeeklyDomainValues.SALES_INFO_element salesinfo = new WeeklyDomainValues.SALES_INFO_element();
       WeeklyDomainValues.SALES_INFO_element salesinfo1 = new WeeklyDomainValues.SALES_INFO_element();
       
       salesinfo.INCO_TERMS = INCO_TERMS ;
       salesinfo.ORDER_REASONS= ORDER_REASONS;
       salesinfo.BILLING_BLOCK=BILLING_BLOCK;
       salesinfo.DELIVERY_BLOCK=DELIVERY_BLOCK;
       salesinfo.PAYMENT_TERMS=PAYMENT_TERMS;
       SALES_INFO.add(salesinfo);
       
       salesinfo1.INCO_TERMS = INCO_TERMS ;
       salesinfo1.ORDER_REASONS= ORDER_REASONS;
       salesinfo1.BILLING_BLOCK=BILLING_BLOCK;
       salesinfo1.DELIVERY_BLOCK=DELIVERY_BLOCK;
       salesinfo1.PAYMENT_TERMS=PAYMENT_TERMS;
       SALES_INFO.add(salesinfo1);
       
       sampleResponse.SALES_INFO= SALES_INFO;
       response.put('response_x', sampleResponse ); 
       
       System.debug('########'+response.get('response_x'));
   }
}