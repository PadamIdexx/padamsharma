/* @Class Name   : QLITriggerHandler
 * @Description  : Trigger Handler class for Quote Line Item related Triggers.
 * @Created By   : Pooja Wade
 * @Created On   : 09/16/2015  
 * @Modification Log:  
 * --------------------------------------------------------------------------------------------------
 * @Developer                Date                   Description
 * --------------------------------------------------------------------------------------------------
 * @Pooja Wade              09/16/2015              Created
 * ---------------------------------------------------------------------------------------------------
*/
public class QLITriggerHandler{

    //method to be called when the trigger fires on insert
    public void onBeforeInsert(List<QuoteLineItem> newQLIs){
        updateDiscountonQLI(newQLIs);
    }
    
    //method to be called when the trigger fires on update
    public void onBeforeUpdate(List<QuoteLineItem> updatedQLIs){
        updateDiscountonQLI(updatedQLIs);
    }


    private void updateDiscountonQLI(List<QuoteLineItem> lstQLIs){
     
        
      for(QuoteLineItem discQLI  : lstQLIs){
      

                if(discQLI.Discount_Type__c != NULL && discQLI.Discount_Type__c == 'Percent'){            //if Discount Type = 'Percent'
                
                    discQLI.Discount = discQLI .Discount__c;
                }
                else if(discQLI .Discount_Type__c != NULL && discQLI.Discount_Type__c == 'Fixed'){         //if Discount Type = 'Fixed'
                     
                     discQLI .Discount = (discQLI.Discount__c*100)/discQLI.Subtotal;
                
                }
                else if(discQLI.Discount_Type__c == NULL){
                    
                    discQLI.Discount__c = NULL;
                    discQLI.Discount = NULL;
               }
        }

    }

}