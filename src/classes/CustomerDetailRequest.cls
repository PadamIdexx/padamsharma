/*
 * This class represents the request object for the SAP Customer Detail Service.
 */ 
public class CustomerDetailRequest {

    public class CUSTOMER_GETDETAIL_Request {
        public String COMPANYCODE;
        public String CUSTOMERNO;
        public String LANGUAGE;
        
        private String[] COMPANYCODE_type_info = new String[]{'COMPANYCODE','http://idexxi.com/sapxi/pocbea/customer360',null,'1','1','false'};
		private String[] CUSTOMERNO_type_info = new String[]{'CUSTOMERNO','http://idexxi.com/sapxi/pocbea/customer360',null,'0','1','false'};            
		private String[] LANGUAGE_type_info = new String[]{'LANGUAGE','http://idexxi.com/sapxi/pocbea/customer360',null,'0','1','false'};        
        
        private String[] apex_schema_type_info = new String[]{'http://idexxi.com/sapxi/pocbea/customer360','false','false'};
        private String[] field_order_type_info = new String[]{'COMPANYCODE','CUSTOMERNO','LANGUAGE'};
    }
    
}