public class CreateWrapper extends SAPOrderWrapper {
	public override SAPOrder execute(SAPOrder order) {

        OrderCreateRequest.SalesOrderCreateRequest request;        
        OrderCreateResponse.SalesOrderCreateResponse response;
        SAPOrder createdOrder;

        //transform SAPOrder to request
        try {        
        	request = OrderCreateRequest.transform(order); 
        } catch(Exception e) {
            System.debug('Error transforming SAPOrder to OrderCreateRequest.');
            System.debug(e.getCause());
            System.debug(e.getMessage());
            
        	throw e;    
        }  
        
        //call service
        try {
             OrderCreateService service = new OrderCreateService();

            if (!Test.isRunningTest()) {
                response = service.createOrder(request);            
            } else {
                response = OrderTestDataFactory.createCreateResponse();
            }
       } catch(Exception e) {
            System.debug('Error calling Order Create Service.');
            System.debug(e.getCause());
            System.debug(e.getMessage());
            
        	throw e;             
        }         

        //transform response into new SAPOrder
        try {
        	createdOrder = OrderCreateResponse.transform(order, response);  
        } catch(Exception e) {
            System.debug('Error transforming OrderCreateResponse to SAPOrder.');
            System.debug(e.getCause());
            System.debug(e.getMessage());
            
        	throw e;              
        }
        
        return createdOrder;
    }
}