/**
*       @author Hemanth Vanapalli
*       @date   25/09/2015   
        @description   Test Class with unit test scenarios to cover the RecordTypeSelectionController class
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Hemanth Vanapalli              07/10/2015          Modified Version
**/

@isTest(seeAllData=false)
private class Test_RecordTypeSelectionController {
    static testMethod void tenderUnitTest() {
        Account testAccount = new Account();
        String TENDER_RECTYPE = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.RT_Annuitized).getRecordTypeId();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = 'Test Account';
        testAccount.CurrencyIsoCode = 'USD';
        insert testAccount;        
        System.assert(testAccount.Id != null);
        Test.setCurrentPage(Page.RecordTypeSelection);
        System.currentPageReference().getParameters().put('accid',testAccount.Id);
        RecordTypeSelectionController controller = new RecordTypeSelectionController( new ApexPages.StandardController(testAccount));
        controller.redirectToPage();
        controller.redirectToThisRecordType();        
        controller.isSplitSelected = null;
        controller.redirectToThisRecordType();  
        controller.thisOpportunity.RecordTypeId = TENDER_RECTYPE;
        controller.redirectToThisRecordType();
    }
}