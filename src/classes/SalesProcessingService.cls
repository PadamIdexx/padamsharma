/****************************************************************************
* Apex Class Name    : SalesProcessingService
* Description        : A Service class which has the web service callout implementation
                       
                            1. SalesOrderCreate 
                            2. SalesOrderSimulate
                            3. SalesOrderChange
                        
* Modification Log   :
* ---------------------------------------------------------------------------
* Developer                   Date                   Description
* ---------------------------------------------------------------------------
* Sirisha Kodi              Nov 25 2015             Created
****************************************************************************/
public with sharing class SalesProcessingService{


        
        public String endpoint_x = '';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x = 120000;
        public static Map<String, String> parametersMap = new Map<String,String>();
        private String[] ns_map_type_info = new String[]{'http://idexx.com/sappo/sales/salesforce', 'idexxComSappoSalesSalesforce', 'http://idexx.com/sappo/global/types', 'idexxComSappoGlobalTypes'};
       
        /*
         * @DefaultContructor
         * @Description  : This is to assign default values to most of the request parameters from the custom setting RemoteSiteSettings__c
         * @Param        : SalesOrderSimulateRequest_Sync request structure 
         * @Return       : Returns SalesOrderSimulateResponse_Sync Response element
         */
        public SalesProcessingService(){
            String endPointURL = WebServiceUtil.retreiveEndpoints(WebServiceUtil.SALES_PROCESSING);
            parametersMap = WebServiceUtil.retreiveParameters(WebServiceUtil.ORDER_PROCESSING);
            if(parametersMap<> null && !parametersMap.isEmpty()){
                
                /* Kevin Lafortune 1/5/2016
                 * The ORDER_PROCESSING url has been updated but is not reflecting that update at runtime.                  
                 * I'm hardcoding this endpoint due to an issue with Custom Settings not refreshing themselves.  I
                 * am doing this in order to get this web service working again.  We will need to sweep back through in the                  
                 * morning to revert this change.
                 */
                 
                 /* Sirisha Kodi 1/6/2016
                 * Moved the endpoint URL to custom metadata type - IDEXX Parameters 
                 */
                
                //endpoint_x = 'https://devbeacon.idexx.com/beacon/sfdc/SAPOrderManagment/ProxyServices/SalesProcessing';
                 /*   
                endpoint_x = parametersMap.containsKey(WebServiceUtil.ENDPOINT)
                                ?parametersMap.get(WebServiceUtil.ENDPOINT)
                                :''; */
                }
            endpoint_x = endPointURL!=null ? endPointURL : '';
            inputHttpHeaders_x = new Map<String, String>();
            if(!System.isBatch()) {
                System.debug('*** Non-batch invocation scenario ***');
                System.debug('*** SessionId: ' + UserInfo.getSessionId());
                inputHttpHeaders_x.put('sessionId',UserInfo.getSessionId());   
            }
            else{
                System.debug('*** Batch invocation scenario ***');
                System.debug('*** SessionId: ' + UserInfo.getSessionId());

                APIUser__c creds = APIUser__c.getInstance('API USER');
                if(creds !=null){
                    String sessionId = Login.login(creds.Username__c,creds.Password__c+creds.Security_Token__c);
                    
                    System.debug('*** SessionId: ' + sessionId);

                    inputHttpHeaders_x.put('sessionId', sessionId);
                } 
            }
        }
        
        
        /*
         * @Method name  : SalesOrderSimulate
         * @Description  : This method makes the webservice callout to the Order Simulate Service
         * @Param        : SalesOrderSimulateRequest_Sync request structure 
         * @Return       : Returns SalesOrderSimulateResponse_Sync Response element
         */
        public SalesProcessingResponse.SalesOrderSimulateResponse_Sync SalesOrderSimulate(SalesProcessingRequest.SalesOrderSimulateRequest_Sync request) {
            
            SalesProcessingRequest.SalesOrderSimulateRequest_Sync request_x = new SalesProcessingRequest.SalesOrderSimulateRequest_Sync();
            request_x = request;
            system.debug('**** endpoint_x'+endpoint_x);
            system.debug('&&& request_x'+request_x);
            SalesProcessingResponse.SalesOrderSimulateResponse_Sync response_x;
            Map<String, SalesProcessingResponse.SalesOrderSimulateResponse_Sync> response_map_x = new Map<String, SalesProcessingResponse.SalesOrderSimulateResponse_Sync>();
            response_map_x.put('response_x', response_x);
            try {
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://sap.com/xi/WebService/soap1.1',
              'http://idexx.com/sappo/sales/salesforce',
              'SalesOrderSimulateRequest',
              'http://idexx.com/sappo/sales/salesforce',
              'SalesOrderSimulateResponse',
              'SalesProcessingResponse.SalesOrderSimulateResponse_Sync'}
            );
            } catch (Exception e) {
System.debug(LoggingLevel.ERROR, 'e - ' + e.getMessage() +  ' '  + e.getCause() + ' ' + e.getLineNumber() + ' ' + e.getStackTraceString() + ' ' + e.getTypeName());                
            }
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    
    
        /*
         * @Method name  : SalesOrderCreate
         * @Description  : This method makes the webservice callout to the Order Create Service
         * @Param        : SalesOrderCreateRequest_Sync request structure 
         * @Return       : Returns SalesOrderCreateResponse_Sync Response element
         */
        public SalesProcessingResponse.SalesOrderCreateResponse_Sync SalesOrderCreate(SalesProcessingRequest.SalesOrderCreateRequest_Sync request) {
            	System.debug('is invoked:');
            try{
                SalesProcessingRequest.SalesOrderCreateRequest_Sync request_x = new SalesProcessingRequest.SalesOrderCreateRequest_Sync();
                request_x = request;
                SalesProcessingResponse.SalesOrderCreateResponse_Sync response_x;
                Map<String, SalesProcessingResponse.SalesOrderCreateResponse_Sync> response_map_x = new Map<String, SalesProcessingResponse.SalesOrderCreateResponse_Sync>();
                response_map_x.put('response_x', response_x);
                WebServiceCallout.invoke(
                  this,
                  request_x,
                  response_map_x,
                  new String[]{endpoint_x,
                  'http://sap.com/xi/WebService/soap1.1',
                  'http://idexx.com/sappo/sales/salesforce',
                  'SalesOrderCreateRequest',
                  'http://idexx.com/sappo/sales/salesforce',
                  'SalesOrderCreateResponse',
                  'SalesProcessingResponse.SalesOrderCreateResponse_Sync'}
                );
                response_x = response_map_x.get('response_x');
                return response_x;
            }catch(CallOutException coe) {
                System.debug(' is CallOutException here'+coe.getStackTraceString());
                throw new OrderCreationException(coe.getMessage(), coe);
                System.debug(' is CallOutException here1'+coe.getStackTraceString());
                return null;
            }
            catch(Exception e) {
                System.debug(' is Exception here'+e.getStackTraceString());
                throw new OrderCreationException(e.getMessage(), e);
                System.debug(' is Exception here'+e.getStackTraceString());
                return null;
            }
        }
           
      

}