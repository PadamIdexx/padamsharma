/**
*      @author         Anudeep Gopagoni
*      @date           September 07 2015
*      @description    Test class for OrderTriggerUtility Class              
*       
*       Modification Log:
*       ---------------------------------------------------------------------
*       Developer                  Date                Description
*       ---------------------------------------------------------------------
*       Anudeep Gopagoni          07/09/2015           Initial Version   
*       Anudeep Gopagoni          23/10/2015           Changes based on review comments
*/
@isTest 
public with sharing class Test_OrderTriggerUtility {
    
    @testSetup static void testData() { 
        //Populate Customer/Prospect Account details
        // Create new Account
        Account acc = CreateTestClassData.createCustomerAccount();
        /*List<Account> accList = new List<Account>(); 
        Account acc           =  new Account(); 
        acc.Name              = 'Golden Straw'; 
        acc.CurrencyIsoCode   = 'USD'; 
        accList.add(acc); 
        if(!accList.isEmpty()) {
            insert acc;
        } */
        //Populate Contact details 
        List<Contact> conLst = new List<Contact>(); 
        if (Schema.sObjectType.Contact.fields.AccountId.isCreateable()) {         
            Contact con           = new Contact(); 
            con.AccountId         = acc.Id; 
            con.LastName          = 'test'; 
            con.CurrencyIsoCode   = 'USD'; 
            conLst.add(con);
            if(!conLst.isEmpty()) {
                insert con; 
            }
        }       
        //populate contract details 
        List<Contract> contractLst = new List<Contract>(); 
        Contract cont         = new Contract(); 
        cont.StartDate        = System.today(); 
        cont.AccountId        = acc.Id; 
        cont.Status           = 'Draft'; 
        cont.CurrencyIsoCode  = 'USD'; 
        cont.ContractTerm     = 12; 
        contractLst.add(cont);
        if(!contractLst.isEmpty()) {
            insert contractLst; 
        }
        // First, set up test price book entries.
        // Insert a test product.
        List<Product2> prodLst = New List<Product2>(); 
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware',SAP_MATERIAL_NUMBER__c='124234');
        prodLst.add(prod); 
        if(!prodLst.isEmpty()) {
            insert prodLst; 
        }
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry or the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        List<PricebookEntry> pricebookentryLst = new List<PricebookEntry>(); 
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true,currencyIsoCode = 'USD', PriceBook_SAPMatNr__c = '3');
        pricebookentryLst .add(standardprice); 
        if(!pricebookentryLst.isEmpty()) {
            insert pricebookentryLst;
        }
        //Populat Campaign details
        List<Campaign> campList = new List<Campaign>(); 
        Campaign camp = new Campaign(); 
        camp.Name = 'Test'; 
        camp.CurrencyIsoCode = 'USD';
        camp.IsActive=true;
        camp.Total_Order_Amount__c = 0;
        camp.Total_Order_Quantity__c = 0;
        camp.Num_Total_Order__c = 0;
        campList.add(camp); 
        if(!campList.isEmpty()) {
            insert campList; 
        } 
        System.debug('camp list is:'+campList);
        // Create a custom price book
        List<Pricebook2> pricebookLst = new List<Pricebook2>();
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        pricebookLst.add(customPB); 
        if(!pricebookLst.isEmpty()) {
            insert pricebookLst;  
        }
        // 2. Insert a price book entry with a custom price.
        List<PricebookEntry> pricebookentry2Lst = new List<PricebookEntry>(); 
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true, CurrencyIsoCode = 'USD', PriceBook_SAPMatNr__c = '2');
        pricebookentry2Lst.add(customPrice);
        if(!pricebookentry2Lst.isEmpty()) { 
            insert pricebookentry2Lst;
        } 
        //Populate Order details
        List<Order> ordLst = new List<Order>();
        Order ord             = new Order(); 
        ord.ContractId        = contractLst[0].Id; 
        ord.EffectiveDate     = System.today(); 
        ord.AccountId         = acc.Id; 
        ord.CurrencyIsoCode   = 'USD'; 
        ord.POC_Campaign__c   = camp.Id; 
        ord.Status            = 'Draft'; 
        ord.Pricebook2Id      = pricebookId;
        ordLst.add(ord);
        if(!ordLst.isEmpty()) {
            insert ordLst;    
        }
        // 3. Create Order Item to associate a product to an order
        if (Schema.sObjectType.OrderItem.isCreateable()) {
            List<OrderItem> orderitemLst = new List<OrderItem>(); 
            OrderItem ordItm = new OrderItem(); 
            ordItm.OrderId = ord.Id; 
            ordItm.PricebookEntryId = standardPrice.Id; 
            ordItm.Quantity = 12; 
            ordItm.UnitPrice = 20;
            orderitemLst.add(ordItm); 
            if(orderitemLst.size()>0) {
                insert orderitemLst; 
            }
            ordItm.UnitPrice = 40;
            update OrdItm;
            //delete OrdItm;
            
            OrderItem ordItm1 = new OrderItem(); 
            ordItm1.OrderId = ord.Id; 
            ordItm1.PricebookEntryId = standardPrice.Id; 
            ordItm1.Quantity = 12; 
            ordItm1.UnitPrice = 20;
            ordItm1.Campaign__c = camp.Id;
            insert ordItm1;
            ordItm1.UnitPrice = 30;
            update OrdItm1;
            
            OrderItem ordItm2 = new OrderItem(); 
            ordItm2.OrderId = ord.Id; 
            ordItm2.PricebookEntryId = standardPrice.Id; 
            ordItm2.Quantity = 12; 
            ordItm2.UnitPrice = 12;
            ordItm2.Campaign__c = camp.Id;
            insert ordItm2; 
        }
        List<OrderItem> lstItem = [SELECT Id FROM OrderItem where OrderId=:ord.Id];
        //System.assertEquals(2, lstItem.size());
    }  
    static testMethod void testOpportunityDeletion() {
        //Query for all the Opportunities that exist on Campaign
        List<Campaign> campLst = new List<campaign>([Select Id, Total_Order_Amount__c  FROM Campaign WHERE Name = 'Test' LIMIT 1]); 
        List<Order> ordLst1 = new List<Order>([Select Id, POC_Campaign__c, TotalAmount, ContractId ,EffectiveDate, OpportunityId,AccountId, Pricebook2Id,Status,CurrencyIsoCode  FROM Order LIMIT 10]);
        System.debug('test Camps:'+campLst);
        System.debug('test ordLst1:'+ordLst1);
        //Check if the Order Amount on Orders is reflected on Campaign
        //System.assertEquals(campLst[0].Total_Order_Amount__c , ordLst1[0].TotalAmount,'Total amount on the order should be reflected on the campaign');
        Test.startTest();
        if (Schema.sObjectType.Order.isDeletable()) {
            // Delete Opportunity
            //OrderTriggerUtility.counterCheckAfterDelete(ordLst1);
            if(!ordLst1.isEmpty()) {
                delete ordLst1;
            }
        } 
         List<Order> ordLst = [SELECT Id FROM Order limit 10];
        System.assertEquals(0, ordLst.size());
        Test.stopTest();                                          
    }
    /*
    static testMethod void buildOppCampOrderRelation() {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
        system.runAs(currentUser){
            // test data creation - test accounts 
            Account testAccount = CreateTestClassData.createCustomerAccount();
            //Test data - test Contact 
            Contact testContact = CreateTestClassData.reusableContact(testAccount.Id);
            insert testContact;
            
            //Test Campaign 
            Campaign testCampaign = CreateTestClassData.reusableCampaign();
            insert testCampaign;
            
            //Test Opportunity
            Opportunity testOpportunity = CreateTestClassData.createLPDOpportunity();
            testOpportunity.accountId = testAccount.Id;
            testOpportunity.CampaignId = testCampaign.Id;
            update testOpportunity;
            
            //Test CampaignMember
            CampaignMember testCM =  CreateTestClassData.reusableCampaignMember(testCampaign.Id, testContact.Id);
            insert testCM;
            
            Test.startTest();
            
            //insert order 
            Order testOrder = CreateTestClassData.reusableOrder(testOpportunity.Id,testAccount.Id);
            testOrder.Status  = 'Draft';
            insert testOrder;
            
            //Query for the order record and assert for the POC_Campaign__c field
            testOrder = [Select id, POC_Campaign__c from order where Id=:testOrder.Id];
            //assert for the campaign field
            system.assertEquals(testOrder.POC_Campaign__c, testCampaign.Id);
            Test.stopTest();
            
        }
    }
    static testMethod void updateCampaignOnOrder(){
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
        system.runAs(currentUser){
            // test data creation - test accounts 
            Account testAccount = CreateTestClassData.createCustomerAccount();
            //Test data - test Contact 
            Contact testContact = CreateTestClassData.reusableContact(testAccount.Id);
            insert testContact;
            
            //Test Campaign 
            Campaign testCampaign = CreateTestClassData.reusableCampaign();
            insert testCampaign;
            
            //Test Opportunity
            Opportunity testOpportunity = CreateTestClassData.createLPDOpportunity();
            testOpportunity.accountId = testAccount.Id;
            testOpportunity.CampaignId = testCampaign.Id;
            update testOpportunity;
            
            //Test CampaignMember
            CampaignMember testCM =  CreateTestClassData.reusableCampaignMember(testCampaign.Id, testContact.Id);
            insert testCM;
            
            Test.startTest();
            
            //insert order 
            Order testOrder = CreateTestClassData.reusableOrder(testOpportunity.Id,testAccount.Id);
            testOrder.Status  = 'Draft';
            //testOrder.TotalAmount = Double.valueOf(1000);
            insert testOrder;
            
            //Query for the order record and assert for the POC_Campaign__c field
            testCampaign = [Select id, Total_Order_Amount__c from Campaign where Id=:testCampaign.Id];
            //assert for the campaign field
            system.assertEquals(testCampaign.Total_Order_Amount__c, testOrder.TotalAmount);
            Test.stopTest();
            
        }
    }*/
}