@isTest
public with sharing class Test_AccountActivityHistoryController {
    
    @isTest(SeeAllData=true)
    static void AccountActivityHistoryController1(){
        Account testAccount = new Account();
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        testAccount.Name = 'Customer Testing Account';
        testAccount.CurrencyISOCode = 'USD';
        testAccount.HIN__c = 'Test123';
        testAccount.SAP_Customer_Number__c = '0'+String.valueOf(Math.random()).subString(12);
        insert testAccount;
        
        Opportunity opp1 = new Opportunity();
        opp1.StageName = '4 Commitment';
        opp1.AccountId = testAccount.Id;
        opp1.Name = 'testOpp1';
        opp1.CloseDate = Date.Today() + 5;
        insert opp1;
        
        Order ord1 = new Order();
        ord1.AccountId = testAccount.Id;
        ord1.OpportunityId = opp1.Id;
        ord1.EffectiveDate = date.today();
        ord1.Status = 'Draft';
        insert ord1;
        
        Case case1 = new Case();
        case1.AccountId = testAccount.Id;
        insert case1;
        
        Task ah = new Task();
        ah.WhatId = testAccount.Id;
        ah.ActivityDate = date.Today() - 15;
        ah.Status = 'Completed';
        insert ah;
        
        //Account acc1 = new Account();
        //acc1 = [Select Id From Account where Name = 'Customer Testing Account' LIMIT 1];
        
        ApexPages.StandardController std = new ApexPages.StandardController(testAccount);
        AccountActivityHistoryController actHistoryCtrl = new AccountActivityHistoryController(std);
        actHistoryCtrl.fetchAccountData();
    }
    
}