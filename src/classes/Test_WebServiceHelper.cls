@isTest
public class Test_WebServiceHelper {

    @isTest(SeeAllData=true) //see all is set on purpose
    static void getServiceEndpointsPositive() {
    	Test.startTest();
        
        String url = WebServiceHelper.getServiceEndpoints('OrderReadService');
        
        System.assertNotEquals(url, null);
        
        Test.stopTest();
    }
    
    @isTest
    static void getServiceEndpointsNegative() {
    	Test.startTest();
        
        String url = WebServiceHelper.getServiceEndpoints('OrderReadService');
        
        System.assertEquals(url, null);
        
        Test.stopTest();
    }


    @isTest
    static void getIDEXXNetworkIdForUserPositive() {
    	Test.startTest();
        
       	User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
	    System.runAs(currentUser){            
            String userId = WebServiceHelper.getIDEXXNetworkIdForUser(currentUser.Id);
            
            System.assertNotEquals(userId, null);
        }        
        Test.stopTest();
    }
    
    @isTest
    static void getIDEXXNetworkIdForUserNegative() {
    	Test.startTest();
        
        String userId = WebServiceHelper.getIDEXXNetworkIdForUser(null);
        
        System.assertEquals(userId, null);
        
        Test.stopTest();
    }    
    
    @isTest
    static void getIntegrationSessionIdPositive() {
    	Test.startTest();
        
       	User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
	    System.runAs(currentUser){            
            String userId = WebServiceHelper.getIntegrationSessionId();
            
            System.assertEquals(userId, null);
        }        
        Test.stopTest();
    }
    
}