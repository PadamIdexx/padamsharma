/**
*    @author         Anudeep Gopagoni
*    @date           12/07/2015   
     @description    This is a test class for WSDL generated class idexxiComSapxiPocbeaCustomer360 written for testing the resend order/delivery confirmation emails functionality
     Modification Log:
    ------------------------------------------------------------------------------------
    Developer                            Date                Description
    ------------------------------------------------------------------------------------
    Anudeep Gopagoni                   11/01/2015          Initial Version
	Heather Kinney                     3.10.2017           Modified - improving code coverage.
**/

@isTest public class Test_SalesOrderDelete {
 
    static testMethod void orderDeleteTest() {
        User adminUser = CreateTestClassData.reusableUser('System Administrator','Idexx');
        system.runAs(adminUser){
            Test.startTest();
            // Insert Customer Account
            Account customerAccount = new Account();
            customerAccount = CreateTestClassData.createCustomerAccount();
                        
            // Insert Products
            Product2 testProduct = new Product2(Name='SNAP PRO STARTER KIT', Family='test', SAP_MATERIAL_NUMBER__c = '11235');
            insert testProduct;
            
            // Insert PriceBook
            Pricebook2 priceBook = CreateTestClassData.reusablePricebook2('IDEXX Standard Price Book');
            insert priceBook;
            
            // Fetch Standard PriceBookId
            Id pricebookId = Test.getStandardPricebookId();
            System.debug('PB2 Id:'+pricebookId);
            // Insert PriceBookEntry
            PricebookEntry stdpriceBookEntry = CreateTestClassData.reusablePriceBookEntry(testProduct.Id, pricebookId,1000);
            stdpriceBookEntry.UseStandardPrice = false;
            insert stdpriceBookEntry;
           
            //Create Order and associate it to Account and Standard Pricebook
            Order thisOrder = CreateTestClassData.createOrder(customerAccount,pricebookId, customerAccount.CurrencyISOCode);
            
            String sampleResponse = '[Message=Standard Order 1439013417 deleted, MessageID=V1, MessageID_type_info=(MessageID, http://idexx.com/sappo/sales/salesforce, null, 0, 1, false), MessageNumber=008, MessageNumber_type_info=(MessageNumber, http://idexx.com/sappo/sales/salesforce, null, 0, 1, false), MessageType=S, MessageType_type_info=(MessageType, http://idexx.com/sappo/sales/salesforce, null, 0, 1, false), Message_type_info=(Message, http://idexx.com/sappo/sales/salesforce, null, 0, 1, false), apex_schema_type_info=(http://idexx.com/sappo/sales/salesforce, false, false), field_order_type_info=(MessageType, MessageID, MessageNumber, Message)]';
            Test_ReusableMockResponse fakeResponse = new Test_ReusableMockResponse(200,'SUCCESS',sampleResponse, new map<String,String>{'Content-Type'=>'application/json'});
            Test.setMock(HttpCalloutMock.class, fakeResponse);

            SalesOrderDelete.deleteSalesOrder(thisOrder.id);

            Test.stopTest();
 
        }
    }
    
}