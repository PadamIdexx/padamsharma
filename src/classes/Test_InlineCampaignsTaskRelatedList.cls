@isTest
public with sharing class Test_InlineCampaignsTaskRelatedList {
    static testMethod void Test_InlineCampaignsTaskRelatedList() {
        
            // Create common test accounts
              User currentUser = CreateTestClassData.reusableUser('System Administrator','TestU');
            system.runAs(currentUser){
            
            Account acc = CreateTestClassData.createCustomerAccount();
            
            Account acc2 = CreateTestClassData.createCustomerAccount();
            
            Contact cont = new Contact();
            cont.LastName = 'testContact1';
            cont.AccountId = acc.Id;
            insert cont;
            
            //RecordType rt = [select id,Name from RecordType where SobjectType='Task' and Name='Log_A_Call' Limit 1];
            
            acc.Primary_Contact__c = cont.Id;
            update acc;
            
            Campaign camp = new Campaign();
            camp.Name = 'testCampaign1';
            insert camp;
            
            CampaignMember cm = new CampaignMember();
            cm.ContactId = cont.Id;
            cm.CampaignId = camp.Id;
            cm.Status = 'Sent';
            insert cm;
            
            Task tsk = new task();
            tsk.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Log An Interaction').getRecordTypeId();
            tsk.Subject = 'Call';
            tsk.Priority = 'Normal';
            tsk.Status = 'Completed';
            tsk.OwnerId = currentUser.Id;
            tsk.WhatId = camp.Id;
            tsk.selectedcampaign__c = cm.id;
            insert tsk;
              
            Test.setCurrentPage(Page.InlineCampaignsTaskRelatedList);
            ApexPages.StandardController std = new ApexPages.StandardController(tsk);
           // InlineCampaignsRelatedListController cac = new InlineCampaignsRelatedListController(std);
           InlineCampaignsRelatedListTaskController cac =  new InlineCampaignsRelatedListTaskController(std);
            //std = new ApexPages.StandardController(acc2);
          //  cac = new InlineCampaignsRelatedListController(std);
            //cac = new InlineCampaignsRelatedListTaskController(std);
            
            //InlineCampaignsRelatedListTaskController TestVariable = new InlineCampaignsRelatedListTaskController(std);
            //TestVariable.loadCampaigns(); 
        
        }  
    }
}