@IsTest
public class Test_OrderDataAccessService {

    static testMethod void testGetStatusOfOrder()  {
        //Set the Current User for Test by passing the Profile Name and Alias for User.
        User currentUser = CreateTestClassData.reusableUser('System Administrator','admin2');
        system.runAs(currentUser){
           
             // test data creation - test accounts 
             Account testAccount = CreateTestClassData.createCustomerAccount();
                        
            Test.startTest();
                OrderDataAccessService svc = new OrderDataAccessService();
                
                OrderStatus status = svc.getStatusOfOrder('123456789');
                
                System.assert(status.delivered);
                
            Test.stopTest();
        }
    }
    
    static testMethod void testOrderStatus() {
        OrderStatus status1 = new OrderStatus();
        System.assert(!status1.submittedToSAP);
        System.assert(!status1.delivered);
        System.assert(!status1.partiallyDelivered);        
    }
        
}