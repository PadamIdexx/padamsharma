@isTest(SeeAllData=FALSE)
private with sharing class Test_BilltrustController {

    static {

        Billtrust_Settings__c setting = new Billtrust_Settings__c();
        setting.Name='BT_ENCRYPT_URL';
        setting.Value__c = 'https://devbeacon.idexx.com/beacon/sfdc/ApplicationServicesBTRUST/ProxyServices/BilltrustPS';
        upsert setting;

        setting = new Billtrust_Settings__c();
        setting.Name='BT_SSO_URL';
        setting.Value__c = 'https://secure.billtrust.com/ccsinglesignon.php';
        upsert setting;

        setting = new Billtrust_Settings__c();
        setting.Name='BT_STATEMENT_URL';
        setting.Value__c = 'https://secure.billtrust.com/cc90/ccResearch.php';
        upsert setting;

        setting = new Billtrust_Settings__c();
        setting.Name='BT_CLIENT_GUID';
        setting.Value__c = '02F35471-86E1-4d48-83D2-A6FE2905417D';
        upsert setting;

        setting = new Billtrust_Settings__c();
        setting.Name='BT_SALES_USER_NAME';
        setting.Value__c = 'crmsso';
        upsert setting;

        setting = new Billtrust_Settings__c();
        setting.Name='BT_SALES_FIRST_NAME';
        setting.Value__c = 'CRM';
        upsert setting;

        setting = new Billtrust_Settings__c();
        setting.Name='BT_SALES_LAST_NAME';
        setting.Value__c = 'SSO';
        upsert setting;

        setting = new Billtrust_Settings__c();
        setting.Name='BT_SALES_EMAIL';
        setting.Value__c = 'Beacon-Test@idexx.com';
        upsert setting;

        setting = new Billtrust_Settings__c();
        setting.Name='BT_SALES_GROUP';
        setting.Value__c = 'Standard User';
        upsert setting;

        setting = new Billtrust_Settings__c();
        setting.Name='BT_ENCRYPT_KEY';
        setting.Value__c = '3AE3572CDFE160C2D66A7AC6';
        upsert setting;
    }

    public static testMethod void testBilltrustController() {

        Test.startTest();

        Test.setMock(HttpCalloutMock.class,
                     new BilltrustEncryptServiceMockImpl());

        //create account
        Account testAccount = new Account();
        testAccount.SAP_Customer_Number__c = '0000016242';

        PageReference pageRef = Page.BillTrustStatement;
        pageRef.getParameters().put('sapId',testAccount.SAP_Customer_Number__c);
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController stdController = new ApexPages.StandardController(testAccount);
        BilltrustController controller = new BilltrustController(stdController);

        System.assert(controller.billtrustURL != null);
        System.debug('controller.billtrustURL='+controller.billtrustURL);

        Test.stopTest();
    }

    public static testMethod void testRetrieveSettings() {

        Test.startTest();

        String goodSetting = BilltrustController.retrieveSettings('BT_SALES_USER_NAME');
        System.assert('crmsso' == goodSetting);

        Test.stopTest();
    }

    public static testMethod void testRetrieveInvalidSettings() {

        Test.startTest();

        String badSetting = BilltrustController.retrieveSettings('xxx');
        System.assert(badSetting == null);

        Test.stopTest();
    }
}