/**
*       @author Vishal Negandhi
*       @date   16/10/2015   
        @description   Test Class with unit test scenarios to cover the ContactTriggerHandler class
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Vishal Negandhi             15/10/2015          Original Version
**/

@istest
private class Test_ContactTriggerHandler
{
    private static testmethod void method1()
    {
        Account acc = CreateTestClassData.createCustomerAccount();
        
        Contact con = CreateTestClassData.reusableContact(acc.id);
        con.firstname='Place Holder';
        insert con;
        
        acc.Primary_Contact__c = con.id;
        update acc;
        
        Campaign Camp = CreateTestClassData.createCampaign();
        CampaignMeMbEr CampMem =CreateTestClassData.createCampaignMembers(camp.id,con.id);
        
        Contact con2 = CreateTestClassData.createContact(acc.id);      
    }
}