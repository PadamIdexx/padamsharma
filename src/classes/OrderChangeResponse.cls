/*
 * OrderChangeResponse
 * 
 * This class represents the shape the Response for the SAP Sales Order Change 
 * operation in SalesProcessing_OutService as defined in Beacon OSB under
 * SAPOrderManagment.
 */ 
public with sharing class OrderChangeResponse {

    //Web Service call out related constants.
    //
    // SOAP Action
    public final static String SOAPACTION = 'http://sap.com/xi/WebService/soap1.1';
    
    // XML Namespace for this object
    // Salesforce representation of Namespace
    public final static String NAMESPACE = 'http://idexx.com/sappo/sales/salesforce';
    public final static String SF_NAMESPACE = 'idexxComSappoSalesSalesforce';
    
    // XML Namespace of types    
    // Salesforce representation of Namespace   
    public final static String XMLTYPE_NAMESPACE = 'http://idexx.com/sappo/global/types';
	public final static String SF_XMLTYPE_NAMESPACE = 'idexxComSappoGlobalTypes';
     
	// Name of Class that carries the payload.  There is no generic way to get this at this 
	// from the obect at runtime at this time. 
    public final static String PAYLOAD_CLASSNAME = 'SalesOrderChangeResponse';    
    
	public class SalesOrderChangeResponse {
        
        public Items_element[] Items;
        public Return_element[] Return_x;

        private String[] Items_type_info = new String[]{'Items',NAMESPACE,null,'0','-1','false'};            
        private String[] Return_x_type_info = new String[]{'Return',NAMESPACE,null,'0','-1','false'};            
                  
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'Items','Returns_x'};
    }
    
    public class Items_element {
        public String ItemNumber;
        public String RequestedQuantity;
        public String ConfirmedQuantity;
        
        private String[] ItemNumber_type_info = new String[]{'ItemNumber',NAMESPACE,null,'1','1','false'};
        private String[] RequestedQuantity_type_info = new String[]{'RequestedQuantity',NAMESPACE,null,'1','1','false'};
        private String[] ConfirmedQuantity_type_info = new String[]{'ConfirmedQuantity',NAMESPACE,null,'1','1','false'};            
        
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'ItemNumber','RequestedQuantity','ConfirmedQuantity'};
    }    
    
    public class Return_element {
        public String MessageType;
        public String MessageID;
        public String MessageNumber;
        public String Message;
        
        private String[] MessageType_type_info = new String[]{'MessageType',NAMESPACE,null,'0','1','false'};
        private String[] MessageID_type_info = new String[]{'MessageID',NAMESPACE,null,'0','1','false'};
        private String[] MessageNumber_type_info = new String[]{'MessageNumber',NAMESPACE,null,'0','1','false'};
        private String[] Message_type_info = new String[]{'Message',NAMESPACE,null,'0','1','false'};
            
        private String[] apex_schema_type_info = new String[]{NAMESPACE,'false','false'};
        private String[] field_order_type_info = new String[]{'MessageType','MessageID','MessageNumber','Message'};
    }
    
    
    /*
     * Transformer - Construct an SAPOrder from a response object
     */     
    public static SAPOrder transform(SAPOrder order, OrderChangeResponse.SalesOrderChangeResponse response) {
        
		//apply changes to order - walk through items in reponse and compare to items in order
		//and apply changes accordingly
        if ((null <> response.items) && (response.items.size() > 0)){
            for (OrderChangeResponse.Items_element i : response.items) {
                if (order.items != null) {
                    for (SAPOrder.Item oi : order.items) {
                        if (String.isNotBlank(oi.itemNumber) && oi.itemNumber.equals(i.itemNumber)) {
                            oi.requestedQuantity = SAPServiceDataHelper.getDecimalForSAPNumber(i.RequestedQuantity,2);
                            oi.confirmedQuantity = SAPServiceDataHelper.getDecimalForSAPNumber(i.ConfirmedQuantity,2);
                        }
                    }
                } 
            }
        }
		
		//process return values
        if ((null <> response.return_x) && (response.return_x.size() > 0)) {
			List<SAPOrder.Return_x> returns = new List<SAPOrder.Return_x>();

            for (OrderChangeResponse.Return_element r : response.return_x) {
                SAPOrder.Return_x return_x = new SAPOrder.Return_x();
                
                return_x.messageType = r.MessageType;
                return_x.messageId = r.MessageID;
                return_x.messageNumber = r.messageNumber;
                return_x.message = r.message;
                
                returns.add(return_x);
            }
            
            //replace whatever is there already
	        order.returns = returns;
        }
        
		//return modified order        
        return order;
    }    
    
}