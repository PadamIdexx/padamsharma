public with sharing class OrderDeleteService{

        public String endpoint_x = '';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x = 120000;

		//Namespace information for service all.
        private String[] ns_map_type_info = new String[]{OrderDeleteRequest.NAMESPACE, 
            								OrderDeleteRequest.SF_NAMESPACE, 
            								OrderDeleteRequest.XMLTYPE_NAMESPACE, 
            								OrderDeleteRequest.SF_XMLTYPE_NAMESPACE};
                                                
        public OrderDeleteService(){
            //Brute force approach to getting runtime classname to use for looking up the endpoint
            String thisName = String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));            
            String endPointURL = '';
            if (!Test.isRunningTest()) { //3.8.2017
                endPointURL = WebServiceHelper.getServiceEndpoints(thisName);    
            }
            endpoint_x = endPointURL!=null ? endPointURL : '';
           
            //set headers
            inputHttpHeaders_x = new Map<String, String>();
            
            String sessionId = '';
            if (!Test.isRunningTest()) { //3.8.2017
                sessionId = WebServiceHelper.getIntegrationSessionId();
            }
            inputHttpHeaders_x.put('sessionId', sessionId);
            
        }

    
        
        public OrderDeleteResponse.SalesOrderDeleteResponse deleteOrder(OrderDeleteRequest.SalesOrderDeleteRequest deleteRequest) {
            OrderDeleteRequest.SalesOrderDeleteRequest request_x = new OrderDeleteRequest.SalesOrderDeleteRequest();
            request_x= deleteRequest;
            OrderDeleteResponse.SalesOrderDeleteResponse response_x;
            Map<String, OrderDeleteResponse.SalesOrderDeleteResponse> response_map_x = new Map<String, OrderDeleteResponse.SalesOrderDeleteResponse>();
            response_map_x.put('response_x', response_x); 
            if (!Test.isRunningTest()) { //3.8.2017
                WebServiceCallout.invoke(
                  this,
                  request_x,
                  response_map_x,
                  new String[]{endpoint_x,
                        OrderDeleteRequest.SOAPACTION,
                        OrderDeleteRequest.NAMESPACE,
                        OrderDeleteRequest.PAYLOAD_CLASSNAME,
                        OrderDeleteResponse.NAMESPACE,
                        OrderDeleteResponse.PAYLOAD_CLASSNAME,
                        OrderDeleteResponse.SalesOrderDeleteResponse.class.getName()}
                );
            }
            response_x = response_map_x.get('response_x');
            return response_x;
        }

}