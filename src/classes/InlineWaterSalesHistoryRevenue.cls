public class InlineWaterSalesHistoryRevenue{ 
    
    private  ApexPages.StandardController controller  {get;set;}//added an instance variable for the standard controller
    //public static list<Customer_Sales_History_Water__c> CAGSalesHistoryList {get;set;}
    public static list<Customer_Sales_History_Water__c> CagKits {get;set;}    
    public static Boolean hasData  {get;set;}
    public static List<String> noRecordsFound {get;set;}
    static Account currentAccountRecord;
    public list<AggregateResult> somme ;
    public  list<AggregateResult> totRev;
    public list<AggregateResult> totQuant;
    public  list<AggregateResult> totQC;
    public  list<AggregateResult> totTest;
  //  Public List<wrapperClass> wpList{get; set;}
   // Public List<wrapperClass> wpList2{get; set;}
    public list<Customer_Sales_History_Water__c> recordslist {get;set;}
    // column headers
    public String QUARTER_CURRENT    {get;set;}
    public String QUARTER_1_AGO  {get;set;}
    public String QUARTER_2_AGO    {get;set;}
    public String QUARTER_3_AGO    {get;set;}
    public String QUARTER_4_AGO    {get;set;}
    public String QUARTER_5_AGO    {get;set;}
    public String QUARTER_6_AGO    {get;set;}
    public String QUARTER_7_AGO    {get;set;}
    public String QUARTER_8_AGO    {get;set;}
    public String QUARTER_9_AGO    {get;set;}
    public String QUARTER_10_AGO    {get;set;}
    public String QUARTER_11_AGO    {get;set;}
    public String QUARTER_12_AGO    {get;set;}
    public String QUARTER_13_AGO    {get;set;}
    public String QUARTER_14_AGO    {get;set;}
    public String QUARTER_15_AGO    {get;set;}
    public String QUARTER_16_AGO    {get;set;}
    public String QUARTER_17_AGO    {get;set;}
    public String QUARTER_18_AGO    {get;set;}
    public String QUARTER_19_AGO    {get;set;}
    public String QUARTER_20_AGO    {get;set;}
    public String QUARTER_21_AGO    {get;set;}
    public String QUARTER_22_AGO    {get;set;}
    public String QUARTER_23_AGO    {get;set;}
    public integer year {get;set;}
    public string accountId {get;set;}
    public List<AggregateResult> producltRev {get;set;}
    
    public InlineWaterSalesHistoryRevenue(ApexPages.StandardController controller){
        currentAccountRecord = (Account)controller.getRecord();
        if(currentAccountRecord<>null && currentAccountRecord.Id<>null)
            currentAccountRecord = [Select Id,SAP_CUSTOMER_NUMBER__c from Account where Id=:currentAccountRecord.Id];
        system.debug('**********currentAccountRecord:' + currentAccountRecord) ;
        recordslist = new list<Customer_Sales_History_Water__c>();
        accountId=currentAccountRecord.id;
        this.controller= controller;  
        getproductRev();
        GroupProducts2(currentAccountRecord.SAP_CUSTOMER_NUMBER__c);
        setHeaderValues();
        getproductrevenue();
    } 
    public void getproductrevenue(){
        system.debug('AccountID: '+accountId);
        recordslist= [select  Sort_Order__c ,PRODUCT_FAMILY__c,  Rev3_QTD__c,Rev3_QTD_YOY__c, 
                       Rev3_4_Quarter_Ago__c,Rev3_Q1_Diff_1_Year_Ago__c,
                       Rev3_8_Quarter_Ago__c,Rev3_Q1_Diff_2_Year_Ago__c,
                       Rev3_3_Quarter_Ago__c,
                       Rev3_Q2_Diff_1_Year_Ago__c,Rev3_7_Quarter_Ago__c, 
                       Rev3_Q2_Diff_2_Year_Ago__c, Rev3_2_Quarter_Ago__c,
                       Rev3_Q3_Diff_1_Year_Ago__c,Rev3_6_Quarter_Ago__c,
                       Rev3_Q3_Diff_2_Year_Ago__c,Rev3_1_Quarter_Ago__c,
                       Rev3_5_Quarter_Ago__c,Rev3_Q4_Diff_1_Year_Ago__c,
                       Rev3_9_Quarter_Ago__c,Rev3_Q4_Diff_2_Year_Ago__c,
                       Rev3_YTD__c,Rev3_YTD_1_Year_Ago__c,Rev3_10_Quarter_Ago__c,
                       Rev3_11_Quarter_Ago__c,Rev3_2_Year_Ago__c,
                       Rev3_Year_Diff_1_Year_Ago__c,Rev3_YTD_2_Year_Ago__c,
                       Rev3_Year_Diff_2_Year_Ago__c,Rev3_1_Year_Ago__c
                       from Customer_Sales_History_Water__c 
                       where Account__c = :accountId
                       and PRODUCT_FAMILY__c Not IN ('All Other','Total All Other')
                       order by  Sort_Order__c  asc ];
                    system.debug('recordlist:'+recordslist.size());
                    if(recordslist!=null&&recordslist.size()>0)
        {
            hasData = true;
        }
    }
    
    
    public list<AggregateResult> getproductRev(){
        
        producltRev = [select sum(Rev3_QTD__c) Rev3_QTD,sum(Rev3_QTD_YOY__c)Rev3_QTD_YOY, PRODUCT_FAMILY__c product,
                       sum(Rev3_4_Quarter_Ago__c)Rev3_4_Quarter_Ago,sum(Rev3_Q1_Diff_1_Year_Ago__c)Rev3_Q1_Diff_1_Year_Ago,
                       sum(Rev3_8_Quarter_Ago__c)Rev3_8_Quarter_Ago,sum(Rev3_Q1_Diff_2_Year_Ago__c)Rev3_Q1_Diff_2_Year_Ago,
                       sum(Rev3_3_Quarter_Ago__c)Rev3_3_Quarter_Ago,
                       sum(Rev3_Q2_Diff_1_Year_Ago__c)Rev3_Q2_Diff_1_Year_Ago,sum(Rev3_7_Quarter_Ago__c)Rev3_7_Quarter_Ago, 
                       sum(Rev3_Q2_Diff_2_Year_Ago__c)Rev3_Q2_Diff_2_Year_Ago, sum(Rev3_2_Quarter_Ago__c)Rev3_2_Quarter_Ago,
                       sum(Rev3_Q3_Diff_1_Year_Ago__c)Rev3_Q3_Diff_1_Year_Ago,sum(Rev3_6_Quarter_Ago__c)Rev3_6_Quarter_Ago,
                       sum(Rev3_Q3_Diff_2_Year_Ago__c)Rev3_Q3_Diff_2_Year_Ago,sum(Rev3_1_Quarter_Ago__c)Rev3_1_Quarter_Ago,
                       sum(Rev3_5_Quarter_Ago__c)Rev3_5_Quarter_Ago,sum(Rev3_Q4_Diff_1_Year_Ago__c)Rev3_Q4_Diff_1_Year_Ago,
                       sum(Rev3_9_Quarter_Ago__c)Rev3_9_Quarter_Ago,sum(Rev3_Q4_Diff_2_Year_Ago__c)Rev3_Q4_Diff_2_Year_Ago,
                       sum(Rev3_YTD__c)Rev3_YTD,sum(Rev3_YTD_1_Year_Ago__c)Rev3_YTD_1_Year_Ago,
                       sum(Rev3_2_Year_Ago__c)Rev3_2_Year_Ago,
                       sum(Rev3_10_Quarter_Ago__c)Rev3_10_Quarter_Ago,sum(Rev3_11_Quarter_Ago__c)Rev3_11_Quarter_Ago,
                       sum(Rev3_Year_Diff_1_Year_Ago__c)Rev3_Year_Diff_1_Year_Ago,sum(Rev3_YTD_2_Year_Ago__c)Rev3_YTD_2_Year_Ago,
                       sum(Rev3_Year_Diff_2_Year_Ago__c)Rev3_Year_Diff_2_Year_Ago,sum(Rev3_1_Year_Ago__c)Rev3_1_Year_Ago
                       from Customer_Sales_History_Water__c 
                       where Account__c = :accountId
                       and PRODUCT_FAMILY__c !='All Other'
                       group by PRODUCT_FAMILY__c 
                       order by PRODUCT_FAMILY__c ];
        
        return producltRev;
    }
    
    public void setHeadervalues(){
        // initialise all variables
        QUARTER_CURRENT    = '';
        QUARTER_1_AGO  = '';
        QUARTER_2_AGO    = '';
        QUARTER_3_AGO    = '';
        QUARTER_4_AGO    = '';
        QUARTER_5_AGO    = '';
        QUARTER_6_AGO    = '';
        QUARTER_7_AGO    = '';
        QUARTER_8_AGO    = '';
        QUARTER_9_AGO    = '';
        QUARTER_10_AGO    = '';
        QUARTER_11_AGO    = '';
        QUARTER_12_AGO    = '';
        QUARTER_13_AGO    = '';
        QUARTER_14_AGO    = '';
        QUARTER_15_AGO    = '';
        QUARTER_16_AGO    = '';
        QUARTER_17_AGO    = '';
        QUARTER_18_AGO    = '';
        QUARTER_19_AGO    = '';
        QUARTER_20_AGO    = '';
        QUARTER_21_AGO    = '';
        QUARTER_22_AGO    = '';
        QUARTER_23_AGO    = '';
        
        Date d = System.today();
        Integer month = d.month();
        year = d.year();
        QUARTER_CURRENT = quarter(0,month,year);
        
        QUARTER_1_AGO = quarter(1,month,year);
        
        QUARTER_2_AGO = quarter(2,month,year);
        
        QUARTER_3_AGO = quarter(3,month,year);
        
        QUARTER_4_AGO = quarter(4,month,year);
        
        QUARTER_5_AGO = quarter(5,month,year);
        
        QUARTER_6_AGO = quarter(6,month,year);
        
        QUARTER_7_AGO = quarter(7,month,year);
        
        QUARTER_8_AGO = quarter(8,month,year);
        
        QUARTER_9_AGO = quarter(9,month,year);
        
        QUARTER_10_AGO = quarter(10,month,year);
        
        QUARTER_11_AGO = quarter(11,month,year);
        
        /*QUARTER_12_AGO = quarter(11,month,year);
dt = DateTime.newInstance(dToday.year(), dToday.QUARTER() - 13,dToday.day());
QUARTER_13_AGO = dt.format('MMM yy');
dt = DateTime.newInstance(dToday.year(), dToday.QUARTER() - 14,dToday.day());
QUARTER_14_AGO = dt.format('MMM yy');
dt = DateTime.newInstance(dToday.year(), dToday.QUARTER() - 15,dToday.day());
QUARTER_15_AGO = dt.format('MMM yy');
dt = DateTime.newInstance(dToday.year(), dToday.QUARTER() - 16,dToday.day());
QUARTER_16_AGO = dt.format('MMM yy');
dt = DateTime.newInstance(dToday.year(), dToday.QUARTER() - 17,dToday.day());
QUARTER_17_AGO = dt.format('MMM yy');
dt = DateTime.newInstance(dToday.year(), dToday.QUARTER() - 18,dToday.day());
QUARTER_18_AGO = dt.format('MMM yy');
dt = DateTime.newInstance(dToday.year(), dToday.QUARTER() - 19,dToday.day());
QUARTER_19_AGO = dt.format('MMM yy');
dt = DateTime.newInstance(dToday.year(), dToday.QUARTER() - 20,dToday.day());
QUARTER_20_AGO = dt.format('MMM yy');
dt = DateTime.newInstance(dToday.year(), dToday.QUARTER() - 21,dToday.day());
QUARTER_21_AGO = dt.format('MMM yy');
dt = DateTime.newInstance(dToday.year(), dToday.QUARTER() - 22,dToday.day());
QUARTER_22_AGO = dt.format('MMM yy');
dt = DateTime.newInstance(dToday.year(), dToday.QUARTER() - 23,dToday.day());
QUARTER_23_AGO = dt.format('MMM yy'); */
        
        
    }  
    
    public String quarter(Integer quarter, Integer month, Integer year)
    {
        Integer quarterNumber;        
        if(month == 1 ||month == 2|| month == 3)
        {
            quarterNumber = 1;  
        }
        else if(month == 4 ||month == 5 || month == 6)
        {
            quarterNumber = 2;  
        }
        else if(month == 7 ||month == 8 || month == 9)
        {
            quarterNumber = 3;  
        }
        else if(month == 10 ||month == 11 || month == 12)
        {
            quarterNumber = 4;  
        }
        Integer p = math.mod(quarter,4);
        if(quarterNumber<= p)
        {
            --year;
        } 
        quarterNumber = math.mod((quarterNumber-p+4),4);
        if(quarterNumber ==0)
        {
            quarterNumber =4;
        }
        return 'Q'+quarterNumber+' '+(year-(quarter/4)) ;
    }
    public list<AggregateResult> getAggregateResultQuery(string SAPNumber,String prdFamily,String prdType,Boolean varList)
    {
        
        String strQuery = 'select   sum(Rev3_QTD__c) Rev3_QTD,sum(Rev3_QTD_YOY__c)Rev3_QTD_YOY,sum(Rev3_4_Quarter_Ago__c)Rev3_4_Quarter_Ago,'
            + 'sum(Rev3_Q1_Diff_1_Year_Ago__c)Rev3_Q1_Diff_1_Year_Ago,sum(Rev3_8_Quarter_Ago__c)Rev3_8_Quarter_Ago, '
            + 'sum(Rev3_Q1_Diff_2_Year_Ago__c)Rev3_Q1_Diff_2_Year_Ago,sum(Rev3_3_Quarter_Ago__c)Rev3_3_Quarter_Ago, '
            +'sum(Rev3_Q2_Diff_1_Year_Ago__c)Rev3_Q2_Diff_1_Year_Ago,sum(Rev3_7_Quarter_Ago__c)Rev3_7_Quarter_Ago, '
            +'sum(Rev3_Q2_Diff_2_Year_Ago__c)Rev3_Q2_Diff_2_Year_Ago, sum(Rev3_2_Quarter_Ago__c)Rev3_2_Quarter_Ago, '
            +'sum(Rev3_Q3_Diff_1_Year_Ago__c)Rev3_Q3_Diff_1_Year_Ago,sum(Rev3_6_Quarter_Ago__c)Rev3_6_Quarter_Ago, '
            +'sum(Rev3_Q3_Diff_2_Year_Ago__c)Rev3_Q3_Diff_2_Year_Ago,sum(Rev3_1_Quarter_Ago__c)Rev3_1_Quarter_Ago, '
            +'sum(Rev3_5_Quarter_Ago__c)Rev3_5_Quarter_Ago,sum(Rev3_Q4_Diff_1_Year_Ago__c)Rev3_Q4_Diff_1_Year_Ago, '
            +'sum(Rev3_9_Quarter_Ago__c)Rev3_9_Quarter_Ago,sum(Rev3_Q4_Diff_2_Year_Ago__c)Rev3_Q4_Diff_2_Year_Ago, '
            +'sum(Rev3_YTD__c)Rev3_YTD,sum(Rev3_YTD_1_Year_Ago__c)Rev3_YTD_1_Year_Ago, '
            +'sum(Rev3_Year_Diff_1_Year_Ago__c)Rev3_Year_Diff_1_Year_Ago,sum(Rev3_YTD_2_Year_Ago__c)Rev3_YTD_2_Year_Ago, '
            +'sum(Rev3_Year_Diff_2_Year_Ago__c)Rev3_Year_Diff_2_Year_Ago,sum(Rev3_1_Year_Ago__c)Rev3_1_Year_Ago '
            
            +'from Customer_Sales_History_Water__c where ' ;
        // +' SAP_CUSTOMER_NUMBER__c ='+SAPNumber +'AND';
        if(varList==true)
        {
            strQuery += ' PRODUCT_FAMILY__c IN (\'WATER/COLILERT\',\'WATER/QC\',\'WATER/TESTS\') ' ; 
        }
        if(prdFamily!=null&& prdFamily!='')
        {
            strQuery  += ' PRODUCT_FAMILY__c = '+prdFamily ;
        }
        if(prdType!=null && prdType!='')
        {   
            strQuery += ' AND PRODUCT_TYPE__c = '+prdType ;
        }
        
        strQuery += ' GROUP BY  PRODUCT_FAMILY__c';  
        //system.debug('@@@@@@@@@@@@@@'+Database.query(strQuery));                   
        return Database.query(strQuery);
    }
    // @RemoteAction 
    public void GroupProducts2(string SAPNumber){
        // MV_CUSTOMER_SALES_HISTORY__x lpdTestTotalrecord = new MV_CUSTOMER_SALES_HISTORY__x();
        //if(currentAccountRecord.SAP_CUSTOMER_NUMBER__c <> null)
        CagKits = new List<Customer_Sales_History_Water__c>();
        //List<String> varList = new List<String>{'WATER/COLILERT','WATER/QC','WATER/TESTS'};
        //wpList = new List<wrapperClass>();
        hasData = false;
        noRecordsFound = new List<String>();
        
        //public  list<AggregateResult> totRev;
        
        
        /*  String strQuery = 'select   sum(Rev3_QTD__c) Rev3_QTD,sum(Rev3_QTD_YOY__c)Rev3_QTD_YOY,sum(Rev3_4_Quarter_Ago__c)Rev3_4_Quarter_Ago,'
+ 'sum(Rev3_Q1_Diff_1_Year_Ago__c)Rev3_Q1_Diff_1_Year_Ago,sum(Rev3_8_Quarter_Ago__c)Rev3_8_Quarter_Ago, '
+ 'sum(Rev3_Q1_Diff_2_Year_Ago__c)Rev3_Q1_Diff_2_Year_Ago,sum(Rev3_3_Quarter_Ago__c)Rev3_3_Quarter_Ago, '
+'sum(Rev3_Q2_Diff_1_Year_Ago__c)Rev3_Q2_Diff_1_Year_Ago,sum(Rev3_7_Quarter_Ago__c)Rev3_7_Quarter_Ago, '
+'sum(Rev3_Q2_Diff_2_Year_Ago__c)Rev3_Q2_Diff_2_Year_Ago, sum(Rev3_2_Quarter_Ago__c)Rev3_2_Quarter_Ago, '
+'sum(Rev3_Q3_Diff_1_Year_Ago__c)Rev3_Q3_Diff_1_Year_Ago,sum(Rev3_6_Quarter_Ago__c)Rev3_6_Quarter_Ago, '
+'sum(Rev3_Q3_Diff_2_Year_Ago__c)Rev3_Q3_Diff_2_Year_Ago,sum(Rev3_1_Quarter_Ago__c)Rev3_1_Quarter_Ago, '
+'sum(Rev3_5_Quarter_Ago__c)Rev3_5_Quarter_Ago,sum(Rev3_Q4_Diff_1_Year_Ago__c)Rev3_Q4_Diff_1_Year_Ago, '
+'sum(Rev3_9_Quarter_Ago__c)Rev3_9_Quarter_Ago,sum(Rev3_Q4_Diff_2_Year_Ago__c)Rev3_Q4_Diff_2_Year_Ago, '
+'sum(Rev3_YTD__c)Rev3_YTD,sum(Rev3_YTD_1_Year_Ago__c)Rev3_YTD_1_Year_Ago, '
+'sum(Rev3_Year_Diff_1_Year_Ago__c)Rev3_Year_Diff_1_Year_Ago,sum(Rev3_YTD_2_Year_Ago__c)Rev3_YTD_2_Year_Ago, '
+'sum(Rev3_Year_Diff_2_Year_Ago__c)Rev3_Year_Diff_2_Year_Ago,sum(Rev3_1_Year_Ago__c)Rev3_1_Year_Ago '

+'from Customer_Sales_History_Water__c  '
// +' where SAP_CUSTOMER_NUMBER__c ='+SAPNumber 
+  ' where PRODUCT_FAMILY__c IN  (\'WATER/COLILERT\',\'WATER/QC\',\'WATER/TESTS\') '
+ ' GROUP BY  PRODUCT_FAMILY__c ';                     
somme = Database.query(strQuery); */
        
        totRev=getAggregateResultQuery(SAPNumber,'','',true);
        totQuant=getAggregateResultQuery(SAPNumber,'\'WATER/COLILERT\'','',false);
        totQC=getAggregateResultQuery(SAPNumber,'\'WATER/COLILERT\'','',false);
        for(AggregateResult ar : totRev )
        {
            
          //  wrapperClass wp1 = new wrapperClass(ar);
           // wp1.str='Total Revenue';
           // wpList.add(wp1);
            //system.debug('wpList::'+wpList);
        }
        for(AggregateResult ar : totQuant ){
            
            
            //wrapperClass wp2 = new wrapperClass(ar);
          //  wp2.str='Total Quantification';
            //wpList.add(wp2);
        }
        for(AggregateResult ar : totQC){
            
            
           // wrapperClass wp3 = new wrapperClass(ar);
           // wp3.str='Total QC';
           // wpList.add(wp3);
        }
        
        if(totRev!=null&&totRev.size()>0)
        {
            hasData = true;
        }
        if(totQuant!=null&&totQuant.size()>0)
        {
            hasData = true;
        }
        
        if(!hasData){
            noRecordsFound = new List<String>();
        }
    }
    /*public class wrapperClass{
        public String str {get;set;}
        
        public Decimal Rev3_QTD{get; set;}
        public  Decimal Rev3_QTD_YOY                 {get;set;}
        public  Decimal Rev3_4_Quarter_Ago           {get;set;}
        public  Decimal Rev3_Q1_Diff_1_Year_Ago      {get;set;}
        public  Decimal Rev3_8_Quarter_Ago           {get;set;}
        public  Decimal Rev3_Q1_Diff_2_Year_Ago      {get;set;}
        public  Decimal Rev3_3_Quarter_Ago           {get;set;}
        public  Decimal Rev3_Q2_Diff_1_Year_Ago      {get;set;}
        public  Decimal Rev3_7_Quarter_Ago           {get;set;}
        public  Decimal Rev3_Q2_Diff_2_Year_Ago      {get;set;}
        public  Decimal Rev3_2_Quarter_Ago           {get;set;}
        public  Decimal Rev3_Q3_Diff_1_Year_Ago      {get;set;}
        public  Decimal Rev3_6_Quarter_Ago           {get;set;}
        public  Decimal Rev3_Q3_Diff_2_Year_Ago      {get;set;}
        public  Decimal Rev3_1_Quarter_Ago           {get;set;}
        public  Decimal Rev3_5_Quarter_Ago           {get;set;}
        public  Decimal Rev3_Q4_Diff_1_Year_Ago      {get;set;}
        public  Decimal Rev3_9_Quarter_Ago           {get;set;}
        public  Decimal Rev3_Q4_Diff_2_Year_Ago      {get;set;}
        public  Decimal Rev3_YTD                     {get;set;}
        public  Decimal Rev3_YTD_1_Year_Ago          {get;set;}
        public  Decimal Rev3_Year_Diff_1_Year_Ago    {get;set;}
        public  Decimal Rev3_YTD_2_Year_Ago          {get;set;}
        public  Decimal Rev3_Year_Diff_2_Year_Ago    {get;set;}
        public  Decimal Rev3_1_Year_Ago           {get;set;}
        
        public wrapperClass(AggregateResult ar){
            
            
            
            Rev3_QTD= (Decimal) ar.get('Rev3_QTD');
            Rev3_QTD_YOY                = (Decimal)ar.get('Rev3_QTD_YOY');
            Rev3_4_Quarter_Ago          = (Decimal)ar.get('Rev3_4_Quarter_Ago');
            Rev3_Q1_Diff_1_Year_Ago     = (Decimal)ar.get('Rev3_Q1_Diff_1_Year_Ago');
            Rev3_8_Quarter_Ago          = (Decimal)ar.get('Rev3_8_Quarter_Ago');
            Rev3_Q1_Diff_2_Year_Ago     = (Decimal)ar.get('Rev3_Q1_Diff_2_Year_Ago');
            Rev3_3_Quarter_Ago          = (Decimal)ar.get('Rev3_3_Quarter_Ago');
            Rev3_Q2_Diff_1_Year_Ago     = (Decimal)ar.get('Rev3_Q2_Diff_1_Year_Ago');
            Rev3_7_Quarter_Ago          = (Decimal)ar.get('Rev3_7_Quarter_Ago');
            Rev3_Q2_Diff_2_Year_Ago     = (Decimal)ar.get('Rev3_Q2_Diff_2_Year_Ago');
            Rev3_2_Quarter_Ago          = (Decimal)ar.get('Rev3_2_Quarter_Ago');
            Rev3_Q3_Diff_1_Year_Ago     = (Decimal)ar.get('Rev3_Q3_Diff_1_Year_Ago');
            Rev3_6_Quarter_Ago          = (Decimal)ar.get('Rev3_6_Quarter_Ago');
            Rev3_Q3_Diff_2_Year_Ago     = (Decimal)ar.get('Rev3_Q3_Diff_2_Year_Ago');
            Rev3_1_Quarter_Ago          = (Decimal)ar.get('Rev3_1_Quarter_Ago');
            Rev3_5_Quarter_Ago          = (Decimal)ar.get('Rev3_5_Quarter_Ago');
            Rev3_Q4_Diff_1_Year_Ago     = (Decimal)ar.get('Rev3_Q4_Diff_1_Year_Ago');
            Rev3_9_Quarter_Ago          = (Decimal)ar.get('Rev3_9_Quarter_Ago');
            Rev3_Q4_Diff_2_Year_Ago     = (Decimal)ar.get('Rev3_Q4_Diff_2_Year_Ago');
            Rev3_YTD                    = (Decimal)ar.get('Rev3_YTD');
            Rev3_YTD_1_Year_Ago         = (Decimal)ar.get('Rev3_YTD_1_Year_Ago');
            Rev3_Year_Diff_1_Year_Ago   = (Decimal)ar.get('Rev3_Year_Diff_1_Year_Ago');
            Rev3_YTD_2_Year_Ago         = (Decimal)ar.get('Rev3_YTD_2_Year_Ago');
            Rev3_Year_Diff_2_Year_Ago   = (Decimal)ar.get('Rev3_Year_Diff_2_Year_Ago');
            Rev3_1_Year_Ago             = (Decimal)ar.get('Rev3_1_Year_Ago');
        }
    }*/
}