public class CustomerDetailManager {

    public static CustomerDetailWrapper getCustomerDetail(String id, String language) {
        
        try {           
            CustomerDetailRequest.CUSTOMER_GETDETAIL_Request req = createRequest(id, language);
                        
            CustomerDetailService service = new CustomerDetailService();
            
            CustomerDetailResponse.CUSTOMER_GETDETAIL_Response response;
                
            //if we are running a test, just create a mock response, othewise, call the service
            if(!Test.isRunningTest()){
            	response = service.getCustomerDetail(req);
            } else { 
                response = CustomerDetailTestdataFactory.createResponse();
            }
            
            CustomerDetailWrapper wrap = new CustomerDetailWrapper(response);
            
            return wrap;
            
        } catch (Exception e) {
            System.debug('Error occured retrieving Customer Detail from SAP: ' + e);
            System.debug('Cause: '+ e.getCause());
            System.debug('Message: ' + e.getMessage());
            
            //rethrow error so it can be handled by caller
            throw e;
        }
        
        return null;
    }
    
    @TestVisible
    private static CustomerDetailRequest.CUSTOMER_GETDETAIL_Request createRequest(String id, String language) {
    	CustomerDetailRequest.CUSTOMER_GETDETAIL_Request req = new CustomerDetailRequest.CUSTOMER_GETDETAIL_Request();
        req.CUSTOMERNO = id;
        req.LANGUAGE = language;
        
        return req;
    }
}