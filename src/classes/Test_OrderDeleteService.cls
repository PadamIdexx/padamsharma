@isTest
public with sharing class Test_OrderDeleteService {

    @isTest static void SalesOrderDeletePositive() {
        Test.startTest();
        OrderDeleteRequest.SalesOrderDeleteRequest request = OrderTestDataFactory.createDeleteRequest();
        OrderDeleteResponse.SalesOrderDeleteResponse mockResponse = OrderTestDataFactory.createDeleteResponse();
        
        Test.setMock(WebServiceMock.class, new Test_SOAPReusableMockResponse(mockResponse));
        
        OrderDeleteService service = new OrderDeleteService();
        
        service.deleteOrder(request);
        
        SAPOrder order = OrderTestDataFactory.createSAPOrder();
        order = OrderDeleteResponse.transform(order, mockResponse);

        Test.stopTest();
        
    }
}