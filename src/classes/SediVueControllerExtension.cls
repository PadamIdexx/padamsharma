/**************************************************************************************
* Apex Class Name  : SediVueControllerExtension
*
*
* Developer               Date                 Description
* ------------------------------------------------------------------------------------                 
* Heather Kinney          7.8.2016             Original version.
*
*
*************************************************************************************/

global class SediVueControllerExtension {
    
    private final Account acct;
     
    public static SediVueAutoReplenishmentService.SubscriptionResponse syncResponse;          //public so we can generate mock responses for testing. 
    public static SediVueAutoReplenishmentService.SubUpdResp_element[] syncUpdateResponse;    //public so we can generate mock responses for testing.
    public static Account accountToTest; //for testing.

    public static SediVueAutoReplenishmentService.SubDetailsReq_element reqEleDetails = new SediVueAutoReplenishmentService.SubDetailsReq_element();
    public static SediVueAutoReplenishmentService.SubUpdReq_element reqEleUpdate = new SediVueAutoReplenishmentService.SubUpdReq_element();
    
    public transient String sapId {get;set;}
    public transient boolean hideBanner {get;set;}
    public transient String errorMsg {get;set;} 
    
    public String errorMessageDirectiveText {get;set;}
    

    public SediVueControllerExtension(ApexPages.StandardController stdController){
        this.acct = (Account)stdController.getRecord();
        if ((ApexPages.currentPage().getParameters().get('hideBanner')) != null) {
            boolean hideBannerVal = Boolean.valueOf(ApexPages.currentPage().getParameters().get('hideBanner'));
            if ((hideBannerVal != null) && (hideBannerVal)) {
                hideBanner = true;
            }
        }
        errorMessageDirectiveText = System.Label.SediVue_Error_Message;
        getSapIdForAccount(this.acct.id);
    }
    
    private void getSapIdForAccount(Id accountId) {
        Account retAcct = [SELECT SAP_Number_without_Leading_Zeros__c FROM Account WHERE Id =: accountId];
        if (retAcct != null) {
            sapId = retAcct.SAP_Number_without_Leading_Zeros__c;
        }
    }

    //Called via UI update request...
    @RemoteAction
    global static boolean setData(String jsonUpdatePayload, String shipToSapId) {
        boolean retResult = false;
        retResult = processSetData(jsonUpdatePayload, shipToSapId);
        return retResult;
    }
    
    public static boolean processSetData(String jsonUpdatePayload, String shipToSapId) {
        boolean retVal = false;
        boolean isReorderPointchange = false;
        boolean isCurrentInventoryChange = false;

        SediVueResultWrapper wrapper = parseUpdateJsonRequest(jsonUpdatePayload);
        if (wrapper != null) { 
            //we will have either a request to update Current Inventory, or a request to update ReOrder Point.
            //and to save unnecessary updates; we will compare old vs. new bevore allowing the update to proceed.
            if ((wrapper.reOrderPoint.length() > 0) && (wrapper.newReOrderPoint.length() > 0)) {	
                if (wrapper.reOrderPoint != wrapper.newReOrderPoint) {
                    isReorderPointchange = true;
                }
            }
            if ((wrapper.currentInventory.length() > 0) && (wrapper.newCurrentInventory.length() > 0)) {	 
                if (wrapper.currentInventory != wrapper.newCurrentInventory) { 
                    isCurrentInventoryChange = true;
                }
            }
            
            SediVueAutoReplenishmentService.SubUpdReq_element subItemUpdate = new SediVueAutoReplenishmentService.SubUpdReq_element();
            subItemUpdate.ShipTo = shipToSapId;
            subItemUpdate.ProdReference = wrapper.materialNumber;
            if (isReorderPointchange) {
            	subItemUpdate.ReOrderQty = wrapper.newReOrderPoint;   
            }
            if (isCurrentInventoryChange) {
            	subItemUpdate.InventoryTot = wrapper.newCurrentinventory;    
            }
            SediVueAutoReplenishmentService.SubUpdReq_element[] SubUpdReq = new SediVueAutoReplenishmentService.SubUpdReq_element[]{};
            SubUpdReq.add(subItemUpdate);

        	SediVueAutoReplenishmentService.HTTPS_Port service = new SediVueAutoReplenishmentService.HTTPS_Port();
        	//if(!Test.isRunningTest()){
            if (SubUpdReq != null) {
            	syncUpdateResponse = service.UpdSubsc(SubUpdReq);
            }
        	//}
            
            if (syncUpdateResponse != null) {
            	retVal = processUpdateSubscriptionResponse(syncUpdateResponse, shipToSapId);
        	}
			
        }

        return retVal;
    }
    
    
    @RemoteAction
    global static List<SediVueResultWrapper> getResultsSync(String sapId) {
        List<SediVueResultWrapper> retResultsList;
        SediVueAutoReplenishmentService.SubscriptionRequest request = new SediVueAutoReplenishmentService.SubscriptionRequest();
        SediVueAutoReplenishmentService.SubDetailsReq_element[] SubDetailsReq = new SediVueAutoReplenishmentService.SubDetailsReq_element[]{};    
        SediVueAutoReplenishmentService.SubDetailsReq_element subDetailsElement = new SediVueAutoReplenishmentService.SubDetailsReq_element(); 
        subDetailsElement.ShipTo = sapId; 
        SubDetailsReq.add(subDetailsElement);     
        SediVueAutoReplenishmentService.HTTPS_Port service = new SediVueAutoReplenishmentService.HTTPS_Port();
        //if(!Test.isRunningTest()){
        syncResponse = service.GetDetails(SubDetailsReq);
        //}
        if (syncResponse != null) {
            retResultsList = processGetDetailsResponse(syncResponse, sapId);
        }
        return retResultsList;
    }
    
    public static boolean processUpdateSubscriptionResponse(SediVueAutoReplenishmentService.SubUpdResp_element[] updateResponse, String sapId) {
        boolean retVal = false;
        
        for (SediVueAutoReplenishmentService.SubUpdResp_element updateItem: updateResponse) {
            if (updateItem != null) {
                //ShipTo and ProdReference are sent back in updateItem, as well as a return_x object...
                SediVueAutoReplenishmentService.Return_element return_x = updateItem.Return_x;
                if ((return_x != null) && (return_x.Type_x != null)) {
                    if (return_x.Type_x.equalsIgnoreCase('S')) {
                        //NB: S is SUCCESS; per SAP team.
                        retVal = true;
                    } else {
                        //TODO:
                        //parse out the remainder of the return_x: Id, Number_x, and Message - and send this back to the UI
                        //in the case of a failure - "soft" error from SAP:
                        
                    }
                } //check to see if we have a non-null return object, and an id in that object
            } //updateItem != null 
        } //for
        
        return retVal;
    }


    
    public static List<SediVueResultWrapper> processGetDetailsResponse(SediVueAutoReplenishmentService.SubscriptionResponse getDetailsResponse, String sapId) {

        List<SediVueResultWrapper> retResultsList;
 
        if (getDetailsResponse.SubscriptionRespDetails != null) {
            SediVueAutoReplenishmentService.SubscriptionRespDetails_element[] subDetails = getDetailsResponse.SubscriptionRespDetails;
            retResultsList = buildResults(subDetails, sapId);
        } else if (getDetailsResponse.Return_x != null) {
            //Otherwise, we may have gotten back an "Unable to find any subscription details" response. If so,
            //it will be contained in the Return_x element.
            SediVueAutoReplenishmentService.Return_element Return_x = new SediVueAutoReplenishmentService.Return_element();
            Return_x = getDetailsResponse.Return_x;
            SediVueResultWrapper resultWrapper = new SediVueResultWrapper(); 
            SediVueResultMessage resultMessage = new SediVueResultMessage();
            if (Return_x != null) {
                if (Return_x.Type_x != null) {
                    resultMessage.Type_x = Return_x.Type_x;   
                }
                if (Return_x.Id != null) {
                    resultMessage.Id = Return_x.Id;   
                }
                if (Return_x.Number_x != null) {
                    resultMessage.Number_x = Return_x.Number_x;   
                }
                if (Return_x.Message != null) {
                    resultMessage.Message = Return_x.Message;   
                }
            }
            resultWrapper.messageReturn = resultMessage;
            retResultsList = new List<SediVueResultWrapper>();
            retResultsList.add(resultWrapper);
        }
        return retResultsList;  
    }
    
    private static List<SediVueResultWrapper> buildResults(SediVueAutoReplenishmentService.SubscriptionRespDetails_element[] subDetails, String sapId) {
        List<SediVueResultWrapper> retResultsList;
        
        if (subDetails != null) {
          retResultsList = new List<SediVueResultWrapper>(); 
          for (SediVueAutoReplenishmentService.SubscriptionRespDetails_element sediVueItem : subDetails) {    
            if (sediVueItem != null) {
                SediVueResultWrapper resultWrapper = new SediVueResultWrapper();
                resultWrapper.shipTo = sapId;   
                if (sediVueItem.ProdReference != null) {
                    resultWrapper.materialNumber = sediVueItem.ProdReference;   
                }
                if (sediVueItem.ProdDescription != null) {
                    resultWrapper.description = sediVueItem.ProdDescription;   
                }
                if (sediVueItem.InventoryTot != null) {
                    resultWrapper.currentInventory = formatNumericDataFromSAP(sediVueItem.InventoryTot);   
                }
                if (sediVueItem.ReOrderQty != null) {
                    resultWrapper.reOrderPoint = formatNumericDataFromSAP(sediVueItem.ReOrderQty);   
                }
                if (sediVueItem.PendDelvDate != null) {
                    resultWrapper.pendingDeliveryDate = sediVueItem.PendDelvDate;   
                }
                if (sediVueItem.PendOrder != null) {
                    resultWrapper.pendingOrderId = sediVueItem.PendOrder;   
                }
                if (sediVueItem.PendDelvItem != null) {
                    resultWrapper.pendingDeliveryNumber = sediVueItem.PendDelivery;
                }
                if (sediVueItem.PendOrder != null) {
                    resultWrapper.pendingTrackingCarrier = sediVueItem.PendDelvCarrier;   
                }
                if (sediVueItem.PendOrder != null) {
                    resultWrapper.pendingTrackingNumber = sediVueItem.PendDelvTrack;   
                }
                if (sediVueItem.LastDelvDate != null) {
                    resultWrapper.lastDeliveryDate = sediVueItem.LastDelvDate;   
                }
                if (sediVueItem.LastOrder != null) {
                    resultWrapper.lastOrderId = sediVueItem.LastOrder;   
                }
                if (sediVueItem.LastDelvItem != null) {
                    resultWrapper.lastDeliveryNumber = sediVueItem.LastDelivery;
                }
                if (sediVueItem.LastDelvCarrier != null) {
                    resultWrapper.lastTrackingCarrier = sediVueItem.LastDelvCarrier;   
                }
                if (sediVueItem.LastDelvTrack != null) {
                    resultWrapper.lastTrackingNumber = sediVueItem.LastDelvTrack;   
                }
        
                retResultsList.add(resultWrapper);
                
            }
          }
        }
        return retResultsList;  
    }
   
    private static String formatNumericDataFromSAP(String valueFromSAP) {
        String retVal;
        if (valueFromSAP.indexOf('.') > -1) {
			String[] arrValueFromSAP = valueFromSAP.split('\\.');
            if ((arrValueFromSAP!= null) && (arrValueFromSAP[0] != null)) {
                retVal = arrValueFromSAP[0];
            }
        } 
        return retVal;
    }

    private static SediVueResultWrapper parseUpdateJsonRequest(String request) {

		//serializes the incoming json request into a SediVueResultWrapper object        
        SediVueResultWrapper retObj;
        boolean hasParsedObject = false;

        JSONParser parser = JSON.createParser(request);
        while ((parser.nextToken() != null) && (!hasParsedObject)) {
            if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                SediVueResultWrapper wrapper = (SediVueResultWrapper)parser.readValueAs(SediVueResultWrapper.class);
        		hasParsedObject = true;
                retObj = wrapper;
            }    
        }
        return retObj;
    }

    public void setResponse(SediVueAutoReplenishmentService.SubscriptionResponse resp) {
        syncResponse = resp;  
    }
    
    public void setUpdateResponse(SediVueAutoReplenishmentService.SubUpdResp_element[] updResp) {
        syncUpdateResponse = updResp;
    }
    
    global class SediVueResultMessage {
        public String Type_x {get;set;}
        public String Id {get;set;}
        public String Number_x {get;set;}
        public String Message {get;set;} 
    }
    
    global class SediVueResultWrapper {
        public String shipTo {get;set;}
        public String materialNumber {get;set;}
        public String description {get;set;}
        public String currentInventory {get;set;}
        public String newCurrentInventory {get;set;}
        public String reOrderPoint {get;set;}
        public String newReOrderPoint {get;set;}
        public String pendingDeliveryDate {get;set;}
        public String pendingOrderId {get;set;}
        public String pendingDeliveryNumber {get;set;}
        public String pendingTrackingNumber {get;set;}
        public String pendingTrackingCarrier {get;set;}
        public String lastDeliveryDate {get;set;}
        public String lastOrderId {get;set;}
        public String lastDeliveryNumber {get;set;}
        public String lastTrackingNumber {get;set;}
        public String lastTrackingCarrier {get;set;}
        public SediVueResultMessage messageReturn {get;set;} 
        
        public SediVueResultWrapper() {
        	initializeValues();   
        }
        
        private void initializeValues() { 
            shipTo = '';
        	materialNumber = '';
            description = '';
            currentInventory = '';
            newCurrentinventory = '';
            reOrderPoint = '';
            newReOrderPoint = '';
            pendingDeliveryDate = '';
            pendingDeliveryNumber = '';
            pendingOrderId = '';
            pendingDeliveryNumber = '';
            pendingTrackingNumber = '';
            pendingTrackingCarrier = '';
            lastDeliveryDate = '';
            lastDeliveryNumber = '';
            lastTrackingNumber = '';
            lastTrackingCarrier = '';
        }        
    }
    

}