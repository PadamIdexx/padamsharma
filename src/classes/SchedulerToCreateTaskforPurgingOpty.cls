/**
*       @author Pooja Wade
*       @date   31/08/2015   
        @description   Scheduler class for Opportunities which have their close date in this quarter but have not yet been closed.
        Function:      It alerts the owners of opportunities that the end date is in this quarter but the status of the opportunity has not yet been closed.
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        ------------------------------------------------------------------------------------
        Pooja Wade                  11/09/2015          Modified Version
**/

global with sharing class SchedulerToCreateTaskforPurgingOpty implements Schedulable{
    // public static String CRON_EXP = '0 0 0 L 12/3 MAR,JUN,SEP,DEC ? *';
   
    global void Execute(SchedulableContext ctx){
 
    List<Task> lstTask = new List<Task>();
    Task t = new Task();
    List<String> toEmailIds = new List<String>(); 
    String body = ' ';
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    String senderDisplayName = 'No-Reply Salesforce';

    // Step 0: Create a master list to hold the emails we'll send
    List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();

    List<Opportunity> o = [SELECT ID,CloseDate, StageName , Name, OwnerID,CurrencyIsoCode, Owner.Email, Owner.Name from Opportunity where StageName NOT IN ('Closed Won' , 'Closed Lost') and CloseDate = THIS_QUARTER];
    
   // if(o.size() > 0){
   if(!o.isEmpty()){
        for(Opportunity opty : o){
         //   Task t = new Task();
            t.Subject = 'As the Close Date of Opportunity coincides with end of Quarter, Please close the Opportunity or extend Close Date for the Opportunity';
            t.OwnerID = opty.OwnerID;
            t.Status = 'Open';
            t.ActivityDate = Date.TODAY();
            t.Priority = 'High';
            t.CurrencyIsoCode = opty.CurrencyIsoCode;
            t.WhatID = opty.ID;
            // t.recordtypeID = '012g0000000DUKR';
            lstTask.add(t);
        }
    }
  
     
    // if(lstTask.size() > 0){
    if(!lstTask.isEmpty()){
        // Database.DMLOptions  dmlo = new Database.DMLOptions();
        // Database.DMLOptions dmlOptions = new Database.DMLOptions();'
        //dmlo.EmailHeader.TriggerUserEmail = TRUE;
        // dmlo.EmailHeader.TriggerUserEmail = TRUE; 
        // database.insert(lstTask, dmlo);
        
        if (Schema.sObjectType.Task.isCreateable()) {
       insert lstTask;
     }
       
        
        
        
        for(Task tsk : [Select Id, Owner.Email, Owner.Name, What.Name From Task Where Id IN : lstTask]){
            // Step 1: Create a new Email
           // Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    
            // Step 2: Set list of people who should get the email
          //  List<String> toEmailIds = new List<String>();
            toEmailIds.add(tsk.Owner.Email);
            mail.setToAddresses(toEmailIds);
    
            // Step 3: Set who the email is sent from
            //mail.setReplyTo('sirdavid@bankofnigeria.com');
            mail.setSenderDisplayName(senderDisplayName);
    
            // (Optional) Set list of people who should be CC'ed
            //List<String> ccTo = new List<String>();
            //ccTo.add('business@bankofnigeria.com');
            //mail.setCcAddresses(ccTo);
            
            // Step 4. Set email contents - you can use variables!
            mail.setSubject('As the Close Date of Opportunity coincides with end of Quarter, Please close the Opportunity or extend Close Date for the Opportunity');
            body = 'Dear ' + tsk.Owner.Name + ', <br/><br/>';
            body += 'A New Task has been assigned to you. <br/>';
            body += 'Subject: As the Close Date of Opportunity coincides with end of Quarter, Please close the Opportunity or extend Close Date for the Opportunity. <br/>';
            body += 'Opportunity: ' + tsk.What.Name + ' <br/>';
            body += 'Due Date: ' + Date.Today() + ' <br/>';
            body += 'Priority: High' + ' <br/><br/>';
            body += 'For More Details, click the following link: '  + '<br/><br/>';
            body += URL.getSalesforceBaseUrl().toExternalForm() + '/' + tsk.id;
            mail.setHtmlBody(body);
    
            // Step 5. Add your email to the master list
            mails.add(mail);
        }
            
        // Step 6: Send all emails in the master list
        Messaging.sendEmail(mails);
    }
  }
         
}