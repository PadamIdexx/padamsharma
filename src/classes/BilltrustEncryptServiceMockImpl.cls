@isTest
global class BilltrustEncryptServiceMockImpl implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {

        String jsonResp = '{"key":"","lstRequests":{"invoiceNumber":"invo123","requestUri":"gdBZp5EIppODM4erNb8etg%3D%3D"}}';
        HTTPResponse resp = new HTTPResponse();
        resp.setBody(jsonResp);
        return resp;
    }
}