/**************************************************************************************
Apex Class Name  : Test_idexxPointsExtension

                   
* Developer         Date                   Description
* ----------------------------------------------------------------------------                 
* Pooja Wade      Jan 29 2016           Original Version
*************************************************************************************/

@isTest
public class Test_idexxPointsExtension{


   static testMethod void Test_idexxPointsExtension(){
   
    Account acc1 = CreateTestClassData.createCustomerAccount();
    System.debug('Acc' + acc1);   
    ApexPages.StandardController std = new ApexPages.StandardController(acc1);
    ApexPages.currentPage().getParameters().put('id',acc1.Id);
    idexxPointsExtension idexxExt = new idexxPointsExtension(std);
    idexxExt.acc = new List<Account>();
    idexxExt.acc.add(acc1);
    idexxExt.sapCustNo = '1494';
    idexxExt.endpointURL = 'https://beacon.idexx.com/beacon/commonauth/enterprise/security/signon-beacon-points-proxy';
    String sampleResponse = 'testing the sample';
    Test_ReusableMockResponse fakeResponse = new Test_ReusableMockResponse(303,'SUCCESS',sampleResponse, new map<String,String>{'Content-Type'=>'application/json'});
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, fakeResponse);
    
    idexxExt.fetchData();
    Test.stopTest();
       
   }

}